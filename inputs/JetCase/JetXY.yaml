%YAML 1.2
# LBMS INPUT FILE.
# For reference on the YAML markup language:
#   http://www.yaml.org/refcard.html

JobName: JetXY

#-------------------------------------------------------
Mesh:
    Coarse  : [80,40,1]
    Fine    : [80,40,1]
    Length  : [0.8,0.4,0]
#-------------------------------------------------------

#-------------------------------------------------------
PoKiTT:
    CanteraInputFile : air.cti
    CanteraInputGroup: air
    temperature         : {name: temperature }
    Viscosity           : {name: viscosity   }
    ThermalConductivity : {name: conductivity}
#-------------------------------------------------------
  
#-------------------------------------------------------
TimeStepper:
    Timestep      : 2.0e-6
    EndTime       : 2.0e-5
    CheckpointTime: 2.0e-5
    MonitorTime   : 2.0e-5
    TimeIntegrator: SSPRK3

#-------------------------------------------------------

#-------------------------------------------------------
Density : {NameTag: {name: density,  state: STATE_N}}
#-------------------------------------------------------
  
#-------------------------------------------------------
BasicExpression:
  
  - type: VOLUME
    TaskList: initialization
    NameTag: {name: x_velocity, state: STATE_NONE}
    DoubleTanhFunction:
      NameTag: {name: Y_Vol, state: STATE_NONE}
      midpointUp   : 0.195
      midpointDown : 0.205
      width        : 0.006
      amplitude    : 1.0
  
  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: x_velocity_Jet, state: STATE_NONE}
    DoubleTanhFunction:
      NameTag: {name: Y_Vol, state: STATE_NONE}
      midpointUp   : 0.195
      midpointDown : 0.205
      width        : 0.006
      amplitude    : 1.0
                    
  - type: VOLUME
    TaskList: initialization
    NameTag: {name: y_velocity, state: STATE_NONE}
    Constant: 0.0

  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: y_velocity_Jet, state: STATE_NONE}
    Constant: 0.0

  - type: VOLUME
    TaskList: initialization
    NameTag: {name: temperature, state: STATE_NONE} 
    Constant: 300.0

  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: temperature_Jet, state: STATE_NONE}
    Constant: 300.0

  - type: VOLUME
    TaskList: initialization
    NameTag: {name: pressure, state: STATE_NONE}
    Constant: 101325.0

  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: pressure_Jet, state: STATE_NONE}
    Constant: 101325.0

  - type: VOLUME
    TaskList: initialization
    NameTag: {name: O2, state: STATE_NONE}
    Constant: 0.21
    
  - type: VOLUME
    TaskList: initialization
    NameTag: {name: N2, state: STATE_NONE}
    Constant: 0.79

  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: O2, state: STATE_NONE}
    Constant: 0.21
    
  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: N2, state: STATE_NONE}
    Constant: 0.79

  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: O2_Jet, state: STATE_NONE}
    Constant: 0.21
    
  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: N2_Jet, state: STATE_NONE}
    Constant: 0.79

  - type: VOLUME
    TaskList: initialization
    NameTag: {name: viscosity, state: STATE_NONE}
    Constant: 5.0e-4
 
  - type: VOLUME
    TaskList: advance_solution
    NameTag: {name: viscosity, state: STATE_NONE}
    Constant: 5.0e-4
    
#-------------------------------------------------------

#-------------------------------------------------------
Geometries:
  - Shape : Plane
    Face  : XMINUS
    Center: [0,0,0]  # only the x-coordinate is important for the x-plane 
    label : plane 
    
  - Shape : Plane    
    Face  : XPLUS
    Center: [0.8,0,0]  # only the x-coordinate is important for the x-plane 
    label : xPlusFace 
    
  - Shape: Circle 
    Face  : XMINUS
    Center: [0,0.2,0]
    Radius: 0.05
    label : Jetless   

  - Shape: Circle 
    Face  : XMINUS
    Center: [0,0.2,0]
    Radius: 0.05
    label : Jet

  - Shape : Complement  # Plane without the jet
    shape1: plane
    shape2: Jetless
    label : xMinusFace

  - Shape : Plane   
    Face  : YMINUS
    Center: [0,0,0]  # only the y-coordinate is important for the y-plane 
    label : yMinusFace 
    
  - Shape : Plane
    Face  : YPLUS
    Center: [0,0.4,0]  # only the y-coordinate is important for the y-plane 
    label : yPlusFace 
          
#-------------------------------------------------------
#-------------------------------------------------------
BoundaryCondition:
#========= Jet Boundary Conditions =========
#----- Primitive Variables -----

  - label: Jet
    Geometry: Jet
    BCType: HardInflow

#---- NSCBC ----
#--X--
  - label: xPlusBC
    Geometry : xPlusFace
    BCType: NonreflectingFlow
    FarFieldPressure: 101325.0
    
  - label:  xMinusBC
    Geometry : xMinusFace
    BCType: NonreflectingFlow
    FarFieldPressure: 101325.0
   
#--Y--
  - label: yPlusBC
    Geometry : yPlusFace
    BCType: NonreflectingFlow
    FarFieldPressure: 101325.0
    
  - label:  yMinusBC
    Geometry : yMinusFace
    BCType: NonreflectingFlow
    FarFieldPressure: 101325.0
    
#-------------------------------------------------------      

#-------------------------------------------------------
MomentumEquations:
  - X-Momentum
  - Y-Momentum
  - Advection  
#-------------------------------------------------------

#-------------------------------------------------------
DensityEquation: {}
#-------------------------------------------------------

#-------------------------------------------------------
ScalarTransportEquation:
  - EnergyEquation: {Advection}
#-------------------------------------------------------

#-------------------------------------------------------     
# matlab visualization
Matlab:
  - NameTag: {name: pressure,          state: STATE_NONE}
  - NameTag: {name: temperature,       state: STATE_NONE}
  - NameTag: {name: x_velocity,        state: STATE_NONE}
  - NameTag: {name: y_velocity,        state: STATE_NONE} 


#IDX-Visualization:
#  - field: {type: Volume, NameTag: {name: pressure,    state: STATE_NONE}}
