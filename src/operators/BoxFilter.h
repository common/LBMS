/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

#ifndef BoxFilter_h
#define BoxFilter_h

/**
 *  \file   BoxFilter.h
 *  \date   Jan 6, 2017
 *  \author Derek Cline
 */

#include <expression/Expression.h>
#include <mpi/Environment.h>
#include <spatialops/structured/IntVec.h>

namespace LBMS{

  //====================================================================

  template< typename FieldT >
  class BoxFilter
    : public Expr::Expression<FieldT>
  {

    DECLARE_FIELDS( FieldT, field_ )

    BoxFilter( const Expr::Tag& fieldTag );

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag fieldT_;
    public:
      Builder( const Expr::Tag& result,
               const Expr::Tag& fieldTag )
      : ExpressionBuilder(result),
        fieldT_( fieldTag ) {}
      Expr::ExpressionBase* build() const
      { return new BoxFilter<FieldT>(fieldT_); }
    };

    void evaluate();

  private:
    Direction dir_;
    int ratio_;
    int filterRange_;
    int numOfPoints_;
    SpatialOps::IntVec extent_;
  };


  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################


  template< typename FieldT >
  BoxFilter<FieldT>::
  BoxFilter( const Expr::Tag& fieldTag )
    : Expr::Expression<FieldT>()
  {
    using SpatialOps::IntVec;
    field_ = this->template create_field_request<FieldT>( fieldTag );

    dir_ = direction<typename FieldT::Location::Bundle>();

    //There is no reason to filter in directions perpendicular to the bundle
    //as we are only interested in filtering precommunication fields.
    const BundlePtr bundle = Environment::bundle( dir_ );

    extent_ = bundle->npts();

    const double epsilon = 10*std::numeric_limits<double>::epsilon();

    ratio_ = bundle->npts(dir_) / bundle->ncoarse(dir_);
    filterRange_ = std::ceil( (ratio_ / 2.0) - epsilon ) - 1;
    numOfPoints_ = 2.0 * filterRange_ + 1.0;
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  BoxFilter<FieldT>::evaluate()
  {
    FieldT& result = this->value();

    const FieldT& field = field_->field_ref();

    result <<= field;

    SpatialOps::IntVec location(0,0,0);
    location[ dir_ ] = 0;

    for( size_t k = 0; k != extent_[get_perp2(dir_)]; ++k ){
      for( size_t j = 0; j != extent_[get_perp1(dir_)]; ++j ){
        location[get_perp1(dir_)] = j;
        location[get_perp2(dir_)] = k;

              FieldT resultLine = get_line_field( result, location );
        const FieldT fieldLine  = get_line_field( field,  location );

        //get_line_field will lop off ghost cells, so interior_begin() will actually return
        //the second interior point.
        typename FieldT::iterator       resultIter = resultLine.begin() + filterRange_;
        typename FieldT::const_iterator fieldIter  = fieldLine. begin() + filterRange_;
        for( int i = filterRange_; i != extent_[dir_]-filterRange_; ++i ){
          *resultIter = 0.0;
          for( int filterIter = -filterRange_; filterIter <= filterRange_; ++filterIter ){
            *resultIter = *resultIter + *(fieldIter+filterIter);
          }//end filtering
          *resultIter = *resultIter / numOfPoints_;
          ++resultIter; ++fieldIter;
        }//end parallel
      }//end for perp2
    }//end for perp2
  }

  //--------------------------------------------------------------------

}//end namespace

#endif // BoxFilter_h
