#ifndef OneSidedConvectionDiv_h
#define OneSidedConvectionDiv_h

#include <expression/Expression.h>

namespace LBMS{



  //====================================================================

  template< typename FieldT, typename DivT >
  class OneSidedConvection
    : public Expr::Expression<FieldT>
  {

    DECLARE_FIELDS( FieldT, phi_, vel_ )

    OneSidedConvection( const SpatialOps::SpatialMask<FieldT>& mask,
                        const Expr::Tag& phiTag,
                        const Expr::Tag& velTag );

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag phiT_;
      const Expr::Tag velT_;
      const SpatialOps::SpatialMask<FieldT> mask_;
    public:
      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const Expr::Tag& phiTag,
               const Expr::Tag& velTag )
      : ExpressionBuilder(result),
        mask_(mask),
        phiT_( phiTag ),
        velT_( velTag ){}
      Expr::ExpressionBase* build() const
      { return new OneSidedConvection<FieldT,DivT>(mask_,phiT_,velT_); }
    };

    void evaluate();
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  private:
    const DivT* divOp_;
    const SpatialOps::SpatialMask<FieldT> mask_;
  };


  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################


  template< typename FieldT, typename DivT >
  OneSidedConvection<FieldT,DivT>::
  OneSidedConvection( const SpatialOps::SpatialMask<FieldT>& mask,
                      const Expr::Tag& phiTag,
                      const Expr::Tag& velTag )
    : Expr::Expression<FieldT>(),
      mask_(mask)
  {
    phi_ = this->template create_field_request<FieldT>( phiTag );
    vel_ = this->template create_field_request<FieldT>( velTag );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename DivT >
  void
  OneSidedConvection<FieldT,DivT>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    divOp_ = opDB.retrieve_operator<DivT>();
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename DivT >
  void
  OneSidedConvection<FieldT,DivT>::
  evaluate()
  {
    FieldT& result = this->value();
    const FieldT& phi = phi_->field_ref();
    const FieldT& vel = vel_->field_ref();

    masked_assign( mask_, result, result -1.0 * (*divOp_)( phi*vel ) );
  }

  //--------------------------------------------------------------------

}//end namespace

#endif // OneSidedConvectionDiv_h
