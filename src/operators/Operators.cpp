#include "Operators.h"
#include <operators/InterpFineToCoarse.h>
#include <operators/InterpCoarseToFine.h>

#include <lbms/Mesh.h>
#include <lbms/Direction.h>

//--- SpatialOps Headers ---//
#include <spatialops/OperatorDatabase.h>
#include <spatialops/structured/stencil/StencilBuilder.h>
#include <spatialops/Nebo.h>

using namespace SpatialOps;

using SpatialOps::IntVec;
using SpatialOps::FaceTypes;

#include <lbms/Coordinate.h>
#include <lbms/Bundle.h>

namespace LBMS{


#define BUILD_BASIC_OPS( DIR, VOL )    \
{                               \
  typedef OperatorTypeBuilder<Interpolant, VOL, FaceTypes<VOL>::XFace >::type VolInterpXT; \
  typedef OperatorTypeBuilder<Interpolant, VOL, FaceTypes<VOL>::YFace >::type VolInterpYT; \
  typedef OperatorTypeBuilder<Interpolant, VOL, FaceTypes<VOL>::ZFace >::type VolInterpZT; \
  typedef OperatorTypeBuilder<Interpolant, FaceTypes<VOL>::XFace, VOL >::type XInterpVolT; \
  typedef OperatorTypeBuilder<Interpolant, FaceTypes<VOL>::YFace, VOL >::type YInterpVolT; \
  typedef OperatorTypeBuilder<Interpolant, FaceTypes<VOL>::ZFace, VOL >::type ZInterpVolT; \
  typedef OperatorTypeBuilder<Gradient   , VOL, FaceTypes<VOL>::XFace >::type GradXT;      \
  typedef OperatorTypeBuilder<Gradient   , VOL, FaceTypes<VOL>::YFace >::type GradYT;      \
  typedef OperatorTypeBuilder<Gradient   , VOL, FaceTypes<VOL>::ZFace >::type GradZT;      \
  typedef OperatorTypeBuilder<Divergence , FaceTypes<VOL>::XFace, VOL >::type DivXT;       \
  typedef OperatorTypeBuilder<Divergence , FaceTypes<VOL>::YFace, VOL >::type DivYT;       \
  typedef OperatorTypeBuilder<Divergence , FaceTypes<VOL>::ZFace, VOL >::type DivZT;       \
  opDB.register_new_operator( new VolInterpXT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new VolInterpYT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new VolInterpZT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new XInterpVolT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new YInterpVolT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new ZInterpVolT( SpatialOps::build_two_point_coef_collection( 0.5, 0.5 ) ) );    \
  opDB.register_new_operator( new GradXT( SpatialOps::build_two_point_coef_collection( -1.0/dx, 1.0/dx ) ) );  \
  opDB.register_new_operator( new GradYT( SpatialOps::build_two_point_coef_collection( -1.0/dy, 1.0/dy ) ) );  \
  opDB.register_new_operator( new GradZT( SpatialOps::build_two_point_coef_collection( -1.0/dz, 1.0/dz ) ) );  \
  opDB.register_new_operator( new DivXT(  SpatialOps::build_two_point_coef_collection( -1.0/dx, 1.0/dx ) ) );  \
  opDB.register_new_operator( new DivYT(  SpatialOps::build_two_point_coef_collection( -1.0/dy, 1.0/dy ) ) );  \
  opDB.register_new_operator( new DivZT(  SpatialOps::build_two_point_coef_collection( -1.0/dz, 1.0/dz ) ) );  \
  typedef NativeFieldSelector<DIR> VolTypes;                                             \
  typedef FaceFieldSelector<DIR> FaceTypes;                                              \
  opDB.register_new_operator( new InterpFineToCoarse< FaceTypes::ParallelT>(*bundle) );  \
  opDB.register_new_operator( new InterpFineToCoarse< FaceTypes::Perp1T   >(*bundle) );  \
  opDB.register_new_operator( new InterpFineToCoarse< FaceTypes::Perp2T   >(*bundle) );  \
  opDB.register_new_operator( new InterpCoarseToFine< FaceTypes::ParallelT>(*bundle) );  \
  opDB.register_new_operator( new InterpCoarseToFine< FaceTypes::Perp1T   >(*bundle) );  \
  opDB.register_new_operator( new InterpCoarseToFine< FaceTypes::Perp2T   >(*bundle) );  \
  opDB.register_new_operator( new InterpFineToCoarse< VolTypes::VolT      >(*bundle) );  \
  opDB.register_new_operator( new InterpCoarseToFine< VolTypes::VolT      >(*bundle) );  \
  opDB.register_new_bc_operator<VolInterpXT>(); \
  opDB.register_new_bc_operator<VolInterpYT>(); \
  opDB.register_new_bc_operator<VolInterpZT>(); \
  opDB.register_new_bc_operator<XInterpVolT>(); \
  opDB.register_new_bc_operator<YInterpVolT>(); \
  opDB.register_new_bc_operator<ZInterpVolT>(); \
  opDB.register_new_bc_operator<GradXT>();      \
  opDB.register_new_bc_operator<GradYT>();      \
  opDB.register_new_bc_operator<GradZT>();      \
  opDB.register_new_bc_operator<DivXT>();       \
  opDB.register_new_bc_operator<DivYT>();       \
  opDB.register_new_bc_operator<DivZT>();       \
}

  void
  build_operators( const BundlePtr& bundle )
  {
    SpatialOps::OperatorDatabase& opDB = bundle->operator_database();

    const double dx = bundle->spacing(XDIR);
    const double dy = bundle->spacing(YDIR);
    const double dz = bundle->spacing(ZDIR);

    switch( bundle->dir() ){
    case XDIR: BUILD_BASIC_OPS( SpatialOps::XDIR, XVolField ); break;
    case YDIR: BUILD_BASIC_OPS( SpatialOps::YDIR, YVolField ); break;
    case ZDIR: BUILD_BASIC_OPS( SpatialOps::ZDIR, ZVolField ); break;
    case NODIR: assert(false); break;
    }

    //One sided, 2 pt coefficients:
    NeboStencilCoefCollection<2> coefHalfDx2Plus   = build_two_point_coef_collection( -1.0/dx,  1.0/dx );
    NeboStencilCoefCollection<2> coefHalfDx2Minus  = build_two_point_coef_collection(  1.0/dx, -1.0/dx );
    NeboStencilCoefCollection<2> coefHalfDy2Plus   = build_two_point_coef_collection( -1.0/dy,  1.0/dy );
    NeboStencilCoefCollection<2> coefHalfDy2Minus  = build_two_point_coef_collection(  1.0/dy, -1.0/dy );
    NeboStencilCoefCollection<2> coefHalfDz2Plus   = build_two_point_coef_collection( -1.0/dz,  1.0/dz );
    NeboStencilCoefCollection<2> coefHalfDz2Minus  = build_two_point_coef_collection(  1.0/dz, -1.0/dz );

    //One sided, 3 pt coefficients:
    NeboStencilCoefCollection<3> coefHalfDx3Plus   = build_three_point_coef_collection( -1.5/dx,  2.0/dx, -0.5/dx );
    NeboStencilCoefCollection<3> coefHalfDx3Minus  = build_three_point_coef_collection(  1.5/dx, -2.0/dx,  0.5/dx );
    NeboStencilCoefCollection<3> coefHalfDy3Plus   = build_three_point_coef_collection( -1.5/dy,  2.0/dy, -0.5/dy );
    NeboStencilCoefCollection<3> coefHalfDy3Minus  = build_three_point_coef_collection(  1.5/dy, -2.0/dy,  0.5/dy );
    NeboStencilCoefCollection<3> coefHalfDz3Plus   = build_three_point_coef_collection( -1.5/dz,  2.0/dz, -0.5/dz );
    NeboStencilCoefCollection<3> coefHalfDz3Minus  = build_three_point_coef_collection(  1.5/dz, -2.0/dz,  0.5/dz );

    //Extrapolent coefficients
    NeboStencilCoefCollection<3> extrapCoef = build_three_point_coef_collection( 0.0, 2.0, -1.0 );

    //Filtering operators
    {
      typedef SpatialOps::FilterSelector<SpatialOps::XDIR>::ThreePtFilterT X3FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::YDIR>::ThreePtFilterT Y3FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::ZDIR>::ThreePtFilterT Z3FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::XDIR>::FivePtFilterT  X5FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::YDIR>::FivePtFilterT  Y5FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::ZDIR>::FivePtFilterT  Z5FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::XDIR>::SevenPtFilterT X7FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::YDIR>::SevenPtFilterT Y7FilterT;
      typedef SpatialOps::FilterSelector<SpatialOps::ZDIR>::SevenPtFilterT Z7FilterT;

      const double alpha3 =  0.25;     ///< 3 pt filter coefficient
      const double alpha5 = -0.0625;   ///< 5 pt filter coefficient
      const double alpha7 =  0.015625; ///< 7 pt filter coefficient

      opDB.register_new_operator( new X3FilterT( build_three_point_coef_collection( 1.0*alpha3, 1+(-2.0*alpha3), 1.0*alpha3 ) ) );
      opDB.register_new_operator( new Y3FilterT( build_three_point_coef_collection( 1.0*alpha3, 1+(-2.0*alpha3), 1.0*alpha3 ) ) );
      opDB.register_new_operator( new Z3FilterT( build_three_point_coef_collection( 1.0*alpha3, 1+(-2.0*alpha3), 1.0*alpha3 ) ) );
      opDB.register_new_operator( new X5FilterT( build_five_point_coef_collection(  1.0*alpha5, -4.0*alpha5, 1+(6.0*alpha5), -4.0*alpha5, 1.0*alpha5 ) ) );
      opDB.register_new_operator( new Y5FilterT( build_five_point_coef_collection(  1.0*alpha5, -4.0*alpha5, 1+(6.0*alpha5), -4.0*alpha5, 1.0*alpha5 ) ) );
      opDB.register_new_operator( new Z5FilterT( build_five_point_coef_collection(  1.0*alpha5, -4.0*alpha5, 1+(6.0*alpha5), -4.0*alpha5, 1.0*alpha5 ) ) );
      opDB.register_new_operator( new X7FilterT( build_seven_point_coef_collection( 1.0*alpha7, -6.0*alpha7, 15.0*alpha7, 1+(-20.0*alpha7), 15.0*alpha7, -6.0*alpha7, 1.0*alpha7 ) ) );
      opDB.register_new_operator( new Y7FilterT( build_seven_point_coef_collection( 1.0*alpha7, -6.0*alpha7, 15.0*alpha7, 1+(-20.0*alpha7), 15.0*alpha7, -6.0*alpha7, 1.0*alpha7 ) ) );
      opDB.register_new_operator( new Z7FilterT( build_seven_point_coef_collection( 1.0*alpha7, -6.0*alpha7, 15.0*alpha7, 1+(-20.0*alpha7), 15.0*alpha7, -6.0*alpha7, 1.0*alpha7 ) ) );
    }

    // special operators
    {
      // X-Bundle
      opDB.register_new_operator<SpatialOps::X2YInterpX>( new SpatialOps::X2YInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::X2ZInterpX>( new SpatialOps::X2ZInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2XInterpX>( new SpatialOps::Y2XInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2ZInterpX>( new SpatialOps::Y2ZInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2XInterpX>( new SpatialOps::Z2XInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2YInterpX>( new SpatialOps::Z2YInterpX( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );

      {
        typedef UnitTriplet<SpatialOps::XDIR>::type  XPlus;
        typedef UnitTriplet<SpatialOps::YDIR>::type  YPlus;
        typedef UnitTriplet<SpatialOps::ZDIR>::type  ZPlus;
        typedef XPlus::Negate            XMinus;
        typedef YPlus::Negate            YMinus;
        typedef ZPlus::Negate            ZMinus;

        build_one_sided_gradients<SpatialOps::OneSidedStencil2<XPlus > >( opDB, coefHalfDx2Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil2<XMinus> >( opDB, coefHalfDx2Minus );
        build_one_sided_gradients<SpatialOps::OneSidedStencil2<YPlus > >( opDB, coefHalfDy2Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil2<YMinus> >( opDB, coefHalfDy2Minus );
        build_one_sided_gradients<SpatialOps::OneSidedStencil2<ZPlus > >( opDB, coefHalfDz2Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil2<ZMinus> >( opDB, coefHalfDz2Minus );

        build_one_sided_gradients<SpatialOps::OneSidedStencil3<XPlus > >( opDB, coefHalfDx3Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil3<XMinus> >( opDB, coefHalfDx3Minus );
        build_one_sided_gradients<SpatialOps::OneSidedStencil3<YPlus > >( opDB, coefHalfDy3Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil3<YMinus> >( opDB, coefHalfDy3Minus );
        build_one_sided_gradients<SpatialOps::OneSidedStencil3<ZPlus > >( opDB, coefHalfDz3Plus  );
        build_one_sided_gradients<SpatialOps::OneSidedStencil3<ZMinus> >( opDB, coefHalfDz3Minus );

        build_extrapolants<XPlus >( opDB, extrapCoef );
        build_extrapolants<XMinus>( opDB, extrapCoef );
        build_extrapolants<YPlus >( opDB, extrapCoef );
        build_extrapolants<YMinus>( opDB, extrapCoef );
        build_extrapolants<ZPlus >( opDB, extrapCoef );
        build_extrapolants<ZMinus>( opDB, extrapCoef );
      }

      // Y-Bundle
      opDB.register_new_operator<SpatialOps::X2YInterpY>( new SpatialOps::X2YInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::X2ZInterpY>( new SpatialOps::X2ZInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2XInterpY>( new SpatialOps::Y2XInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2ZInterpY>( new SpatialOps::Y2ZInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2XInterpY>( new SpatialOps::Z2XInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2YInterpY>( new SpatialOps::Z2YInterpY( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );

      // Z-Bundle
      opDB.register_new_operator<SpatialOps::X2YInterpZ>( new SpatialOps::X2YInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::X2ZInterpZ>( new SpatialOps::X2ZInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2XInterpZ>( new SpatialOps::Y2XInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Y2ZInterpZ>( new SpatialOps::Y2ZInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2XInterpZ>( new SpatialOps::Z2XInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
      opDB.register_new_operator<SpatialOps::Z2YInterpZ>( new SpatialOps::Z2YInterpZ( SpatialOps::build_four_point_coef_collection(.25,0.25,0.25,0.25) ) );
    }
  }

  //One Sided Gradient Stencil Builder
  template< typename FieldT, typename StencilT, typename CoefCollection >
  void build_one_sided_gradients( OperatorDatabase& opdb,
                                  const CoefCollection& coefs )
  {
    typedef typename OneSidedOpTypeBuilder<Gradient,StencilT,FieldT>::type  GradOp;
    opdb.register_new_operator( new GradOp( coefs ) );
  }
  template< typename StencilT, typename CoefCollection >
  void build_one_sided_gradients( OperatorDatabase& opdb,
                                  const CoefCollection& coefs )
  {
    build_one_sided_gradients<LBMS::XVolField,StencilT,CoefCollection>( opdb, coefs );
    build_one_sided_gradients<LBMS::YVolField,StencilT,CoefCollection>( opdb, coefs );
    build_one_sided_gradients<LBMS::ZVolField,StencilT,CoefCollection>( opdb, coefs );
  }

  //Extrapolation Stencil Builder
  template< typename FieldT, typename IndexTripletT, typename CoefCollection >
  void build_extrapolants( OperatorDatabase& opdb,
                           const CoefCollection& coefs )
  {
    typedef typename OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<IndexTripletT>,FieldT>::type  ExtrapOp;
    opdb.register_new_operator( new ExtrapOp( coefs ) );
  }
  template< typename IndexTripletT, typename CoefCollection >
  void build_extrapolants( OperatorDatabase& opdb,
                           const CoefCollection& coefs )
  {
    build_extrapolants<LBMS::XVolField,  IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::XSurfXField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::XSurfYField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::XSurfZField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::YVolField,  IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::YSurfXField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::YSurfYField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::YSurfZField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::ZVolField,  IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::ZSurfXField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::ZSurfYField,IndexTripletT,CoefCollection>( opdb, coefs );
    build_extrapolants<LBMS::ZSurfZField,IndexTripletT,CoefCollection>( opdb, coefs );
  }


} // namespace LBMS
