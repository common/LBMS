/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <boost/foreach.hpp>

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>

#ifndef Extrapolation_h
#define Extrapolation_h

namespace LBMS{

  /**
   *  \class Extrapolation
   *  \author Derek Cline
   *  \date   Feb, 2016
   *
   *  \brief Provides 3 Point Extrapolation for volume field terms.  A mask point will be shifted in
   *         the direction of the extrapolation.  For example, if a mask point is on the first interior
   *         cell, phi_1, and an XMinus-Direction is given, then this expression will apply:
   *         \f$\phi_0=2\cdot\phi_1-\phi_2 \f$
   *
   *         Currently applies to volume fields
   *         We shift the mask, as NSCBC mask points are first interior cells, though we wish to
   *         extrapolate to the ghost cell to calculate valid fluxes across the boundary.
   *
   */
  template< typename FieldT, typename OpT >
  class Extrapolation : public Expr::Expression<FieldT>
  {
  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const SpatialOps::SpatialMask<FieldT> mask_;
      const LBMS::Direction dir_;
      const SpatialOps::BCSide side_;
    public:

      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const LBMS::Direction& dir,
               const SpatialOps::BCSide& side );

      Expr::ExpressionBase* build() const;
    };

    void evaluate();
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  private:

    Extrapolation( const SpatialOps::SpatialMask<FieldT>& mask,
                   const LBMS::Direction& dir,
                   const SpatialOps::BCSide& side );

    const OpT* extrapOp_;
    const SpatialOps::SpatialMask<FieldT> mask_;
    const LBMS::Direction dir_;
    const SpatialOps::BCSide side_;

    SpatialOps::SpatialMask<FieldT>* shiftedMask_;
  };



  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT, typename OpT >
  Extrapolation<FieldT,OpT>::Extrapolation( const SpatialOps::SpatialMask<FieldT>& mask,
                                            const LBMS::Direction& dir,
                                            const SpatialOps::BCSide& side )
    : Expr::Expression<FieldT>(),
      mask_(mask),
      dir_(dir),
      side_(side)
  {
    //This will shift the mask to the appropriate cell for extrapolation.
    SpatialOps::IntVec shift(0,0,0);

    switch( dir ){
      case SpatialOps::XDIR::value: shift = shift + SpatialOps::IntVec(1,0,0); break;
      case SpatialOps::YDIR::value: shift = shift + SpatialOps::IntVec(0,1,0); break;
      case SpatialOps::ZDIR::value: shift = shift + SpatialOps::IntVec(0,0,1); break;
      case SpatialOps::NODIR::value:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "Invalid direction in Extrapolation expression" << std::endl;
        throw std::runtime_error( msg.str() );
        break;
    }

    if( side == SpatialOps::MINUS_SIDE ) shift = shift * SpatialOps::IntVec( -1,-1,-1 );

    std::vector<SpatialOps::IntVec> tempVec( (mask.points()).size() );

    size_t i=0;
    BOOST_FOREACH( const SpatialOps::IntVec& v, mask.points() ){
      tempVec[i++] = v + shift;
    }

    //Essentially copies everything from the previous mask but
    //uses the shifted points instead.
    shiftedMask_ = new SpatialOps::SpatialMask<FieldT>( mask.window_with_ghost(),
                                                        mask.boundary_info(),
                                                        mask.get_ghost_data(),
                                                        tempVec );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void Extrapolation<FieldT,OpT>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    extrapOp_ = opDB.retrieve_operator<OpT>();
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void Extrapolation<FieldT,OpT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();

    SpatFldPtr<FieldT> tempField = SpatialFieldStore::get<FieldT>( result );
    *tempField <<= result;

    masked_assign( *shiftedMask_, *tempField, 0.0 );
    masked_assign( *shiftedMask_, result, (*extrapOp_)(*tempField) );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  Extrapolation<FieldT,OpT>::Builder::
  Builder( const Expr::Tag& result,
           const SpatialOps::SpatialMask<FieldT>& mask,
           const LBMS::Direction& dir,
           const SpatialOps::BCSide& side )
    : ExpressionBuilder(result),
      mask_(mask),
      dir_(dir),
      side_(side)
  {}

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  Expr::ExpressionBase*
  Extrapolation<FieldT,OpT>::Builder::build() const
  { return new Extrapolation<FieldT,OpT>( mask_, dir_, side_ ); }

  /**
   *  \class FluxExtrapolation
   *  \author Derek Cline
   *  \date   Feb, 2016
   *
   *  \brief Provides 3 Point FluxExtrapolation for face field terms.  A mask point will be shifted in
   *         the direction of the FluxExtrapolation.  For example, if a mask point is on the first interior
   *         cell, phi_1, and an XMinus-Direction is given, then this expression will apply:
   *         \f$\phi_0=2\cdot\phi_1-\phi_2 \f$
   *
   *         Currently only applies to face fields.
   *         For NSCBC boundaries, the mask points should cross the face of the boundary (i.e. NOT ghost faces)
   *
   */
  template< typename FieldT, typename OpT >
  class FluxExtrapolation : public Expr::Expression<FieldT>
  {
  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const SpatialOps::SpatialMask<FieldT> mask_;
    public:

      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask);

      Expr::ExpressionBase* build() const;
    };
    void evaluate();
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  private:
    FluxExtrapolation( const SpatialOps::SpatialMask<FieldT>& mask );

    const OpT* extrapOp_;
    const SpatialOps::SpatialMask<FieldT> mask_;
  };


  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT, typename OpT >
  FluxExtrapolation<FieldT,OpT>::FluxExtrapolation( const SpatialOps::SpatialMask<FieldT>& mask )
    : Expr::Expression<FieldT>(),
      mask_(mask)
  {}

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void FluxExtrapolation<FieldT,OpT>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    extrapOp_ = opDB.retrieve_operator<OpT>();
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void FluxExtrapolation<FieldT,OpT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();

    SpatFldPtr<FieldT> tempField = SpatialFieldStore::get<FieldT>( result );
    *tempField <<= result;

    masked_assign( mask_, *tempField, 0.0 );
    masked_assign( mask_, result, (*extrapOp_)(*tempField) );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  FluxExtrapolation<FieldT,OpT>::Builder::
  Builder( const Expr::Tag& result,
           const SpatialOps::SpatialMask<FieldT>& mask )
    : ExpressionBuilder(result),
      mask_(mask)
  {}

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  Expr::ExpressionBase*
  FluxExtrapolation<FieldT,OpT>::Builder::build() const
  { return new FluxExtrapolation<FieldT,OpT>( mask_ ); }

}//end namespace

#endif // Extrapolation_h
