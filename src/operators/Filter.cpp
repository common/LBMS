/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Filter.h"
#include <lbms/Bundle.h>

#include <expression/ExprLib.h>

using SpatialOps::IntVec;

namespace LBMS{

  template< typename DirT >
  Filter<DirT>::Filter( const LBMS::BundlePtr& bundle,
                        const Expr::TagSet& tags )
  : bundle_         ( bundle                                                                 ),
    tags_           ( tags                                                                   ),
    fml_            ( *bundle_->get_field_manager_list()                                     ),
    threePtFilterOp_( *(bundle_->operator_database()).template retrieve_operator<Filter3T>() ),
    fivePtFilterOp_ ( *(bundle_->operator_database()).template retrieve_operator<Filter5T>() ),
    sevenPtFilterOp_( *(bundle_->operator_database()).template retrieve_operator<Filter7T>() ),
    newGhost_       ( 3 )  //See Filter.h for explanation
  {
    //----------------------------------------------
    //Setting up the masks
    const bool do1( bundle_->npts( LBMS::XDIR ) > 1 );
    const bool do2( bundle_->npts( LBMS::YDIR ) > 1 );
    const bool do3( bundle_->npts( LBMS::ZDIR ) > 1 );

    SpatialOps::IntVec offset(SpatialOps::IntVec(0,0,0)); //offset of starting location
    SpatialOps::IntVec extent = bundle_->npts() - SpatialOps::IntVec(1,1,1);   //extent of the field without ghosts

    ghostMaskSet_.clear();
    edgeMaskSet_.clear();
    exteriorMaskSet_.clear();

    //Adding back in the ghost cells for the appropriate cases
    //We filter ghost cells only in perpendicular directions but these only exist
    //if the dimension exists
    if( do1 && bundle_->dir() != LBMS::XDIR ) offset = offset - IntVec(1,0,0);
    if( do2 && bundle_->dir() != LBMS::YDIR ) offset = offset - IntVec(0,1,0);
    if( do3 && bundle_->dir() != LBMS::ZDIR ) offset = offset - IntVec(0,0,1);

    //Adding back in the ghost cells for the appropriate cases
    if( do1 && bundle_->dir() != LBMS::XDIR ) extent = extent + IntVec(1,0,0);
    if( do2 && bundle_->dir() != LBMS::YDIR ) extent = extent + IntVec(0,1,0);
    if( do3 && bundle_->dir() != LBMS::ZDIR ) extent = extent + IntVec(0,0,1);

    //Shifting if we are not periodic (i.e. ghost cells are not valid)
    const IntVec minusGhostShift( IntVec( (bundle_->get_boundary_type(XMINUS) == PeriodicBoundary ? 0 : 1),
                                          (bundle_->get_boundary_type(YMINUS) == PeriodicBoundary ? 0 : 1),
                                          (bundle_->get_boundary_type(ZMINUS) == PeriodicBoundary ? 0 : 1) ) );

    //We can have a processor boundary on one side and a periodic boundary on the other
    const IntVec plusGhostShift( IntVec( (bundle_->get_boundary_type(XPLUS) == PeriodicBoundary ? 0 : 1),
                                         (bundle_->get_boundary_type(YPLUS) == PeriodicBoundary ? 0 : 1),
                                         (bundle_->get_boundary_type(ZPLUS) == PeriodicBoundary ? 0 : 1) ) );

    //Only shift points if that dimension exists
    if( do1 ){ extent[0] = extent[0] - plusGhostShift[0]; offset[0] = offset[0] + minusGhostShift[0]; }
    if( do2 ){ extent[1] = extent[1] - plusGhostShift[1]; offset[1] = offset[1] + minusGhostShift[1]; }
    if( do3 ){ extent[2] = extent[2] - plusGhostShift[2]; offset[2] = offset[2] + minusGhostShift[2]; }

    //Calculating mask sets along the Z face
    for(int i=offset[0];i<=extent[0];++i){
      for(int j=offset[1];j<=extent[1];++j){
        if( bundle_->dir() == LBMS::ZDIR ){
          ghostMaskSet_.push_back   ( IntVec(i,j,offset[2]-1) );
          edgeMaskSet_.push_back    ( IntVec(i,j,offset[2])   );
          exteriorMaskSet_.push_back( IntVec(i,j,offset[2]+1) );
          exteriorMaskSet_.push_back( IntVec(i,j,extent[2]-1) );
          edgeMaskSet_.push_back    ( IntVec(i,j,extent[2])   );
          ghostMaskSet_.push_back   ( IntVec(i,j,extent[2]+1) );
        }
      }
      //Calculating mask sets along the Y face
      for(int k=offset[2];k<=extent[2];++k){
        if( bundle_->dir() == LBMS::YDIR ){
          ghostMaskSet_.push_back   ( IntVec(i,offset[1]-1,k) );
          edgeMaskSet_.push_back    ( IntVec(i,offset[1],k)   );
          exteriorMaskSet_.push_back( IntVec(i,offset[1]+1,k) );
          exteriorMaskSet_.push_back( IntVec(i,extent[1]-1,k) );
          edgeMaskSet_.push_back    ( IntVec(i,extent[1],k)   );
          ghostMaskSet_.push_back   ( IntVec(i,extent[1]+1,k) );
        }
      }
    }
    //Calculating mask sets along the X face
    for(int k=offset[2];k<=extent[2];++k){
      for(int j=offset[1];j<=extent[1];++j){
        if( bundle_->dir() == LBMS::XDIR ){
          ghostMaskSet_.push_back   ( IntVec(offset[0]-1,j,k) );
          edgeMaskSet_.push_back    ( IntVec(offset[0],j,k)   );
          exteriorMaskSet_.push_back( IntVec(offset[0]+1,j,k) );
          exteriorMaskSet_.push_back( IntVec(extent[0]-1,j,k) );
          edgeMaskSet_.push_back    ( IntVec(extent[0],j,k)   );
          ghostMaskSet_.push_back   ( IntVec(extent[0]+1,j,k) );
        }
      }
    }

    FieldMgr& fm = fml_.field_manager<FieldT>();

    for( typename Expr::TagSet::const_iterator i = tags_.begin(); i!=tags_.end(); ++i ){
      //dac This if statement allows for the use of "timeStepper->get_variables( varNames );" on
      //any number of processors.  This ensures that we filter all of the solution variables together
      //so that there are no inconsistencies.
      if( fm.has_field(*i) ){
        //Field for mask creation
        typedef typename LBMS::NativeFieldSelector<DirT>::VolT FieldT;
        const FieldT& fieldForMaskCreation = fm.field_ref(*i);

        //Mask creation
        exteriorMask_ = new SpatialOps::SpatialMask<FieldT>( fieldForMaskCreation, exteriorMaskSet_ );
        edgeMask_     = new SpatialOps::SpatialMask<FieldT>( fieldForMaskCreation, edgeMaskSet_     );
        ghostMask_    = new SpatialOps::SpatialMask<FieldT>( fieldForMaskCreation, ghostMaskSet_    );

        //Memory window creation
        const SpatialOps::MemoryWindow& mw = fieldForMaskCreation.window_with_ghost();
        const SpatialOps::GhostData& ghost = fieldForMaskCreation.get_ghost_data();
        const IntVec newExtent = mw.extent() - ghost.get_plus() - ghost.get_minus() +
                                             newGhost_.get_plus() + newGhost_.get_minus();

        newMemoryWindow_ = new SpatialOps::MemoryWindow( newExtent, IntVec(0,0,0), newExtent);

        break;
      }
    }
  }


  //--------------------------------------------------------------------------------------------
  template< typename DirT >
  Filter<DirT>::~Filter()
  {
    delete ghostMask_;
    delete edgeMask_;
    delete exteriorMask_;
    delete newMemoryWindow_;
  }

  //--------------------------------------------------------------------------------------------
  template< typename DirT >
  void Filter<DirT>::apply_filter()
  {
    using namespace SpatialOps;

    FieldMgr& fm = fml_.field_manager<FieldT>();

    for( typename Expr::TagSet::const_iterator ifld = tags_.begin(); ifld!=tags_.end(); ++ifld ){
      //dac This if statement allows for the use of "timeStepper->get_variables( varNames );" on
      //any number of processors.  This ensures that we filter all of the solution variables together
      //so that there are no inconsistencies.
      if( fm.has_field(*ifld) ){

        FieldT& field = fm.field_ref(*ifld);
        SpatFldPtr<FieldT> tmp = SpatialFieldStore::get_from_window<FieldT>( *newMemoryWindow_, field.boundary_info(), newGhost_, field.active_device_index() );

        *tmp <<= field;
        tmp->reset_valid_ghosts( newGhost_ );

        field <<= cond( *ghostMask_,                          *tmp   )
                      ( *edgeMask_,     ( threePtFilterOp_ )( *tmp ) )
                      ( *exteriorMask_, ( fivePtFilterOp_ ) ( *tmp ) )
                      (                 ( sevenPtFilterOp_ )( *tmp ) );
      }
    }
  }

  //=================================================
  template class Filter<SpatialOps::XDIR>;
  template class Filter<SpatialOps::YDIR>;
  template class Filter<SpatialOps::ZDIR>;
  //=================================================


}//end namespace
