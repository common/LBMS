#ifndef Filter_h
#define Filter_h

#include <expression/Expression.h>
#include <operators/Operators.h>
#include <spatialops/Nebo.h>
#include <spatialops/OperatorDatabase.h>
#include <spatialops/structured/IntVec.h>

namespace LBMS{

  /**
   *  \class Filter
   *  \author Derek Cline
   *  \date   March 3, 2015
   *
   *  \brief Provides filtering for every variable fed into the constructor.
   *         apply_filter() should be called between time steps.  Fields will need to be
   *         locked (solution variables are automatically locked however).
   *
   *  \param bundle Pointer to the bundle that the fields live on
   *  \param TagSet Set of tags that will be filtered.  Any tag requiring filtering can
   *         be fed into this tag.  However, for triplet mapping, we wish to filter
   *         solution variables.
   *
   */
  template< typename DirT >
  class Filter
  {
  public:
    Filter( const LBMS::BundlePtr& bundle,
            const Expr::TagSet& );

    ~Filter();

    void apply_filter();

  private:

    const LBMS::BundlePtr bundle_;

    typedef typename SpatialOps::FilterSelector<DirT>::ThreePtFilterT  Filter3T;
    typedef typename SpatialOps::FilterSelector<DirT>::FivePtFilterT   Filter5T;
    typedef typename SpatialOps::FilterSelector<DirT>::SevenPtFilterT  Filter7T;

    typedef typename LBMS::NativeFieldSelector<DirT>::VolT FieldT;
    typedef typename Expr::FieldMgrSelector<FieldT>::type FieldMgr;

    const Filter3T& threePtFilterOp_;
    const Filter5T& fivePtFilterOp_;
    const Filter7T& sevenPtFilterOp_;

    //This will need to be as large as the largest stencil used in filtering.
    //For example, if we wish to use a seven point filter, we need at least 3 ghost cells.
    const SpatialOps::GhostData newGhost_;

    Expr::FieldManagerList& fml_;

    std::vector<SpatialOps::IntVec> exteriorMaskSet_, edgeMaskSet_, ghostMaskSet_;

    SpatialOps::SpatialMask<FieldT>* exteriorMask_;
    SpatialOps::SpatialMask<FieldT>* edgeMask_;
    SpatialOps::SpatialMask<FieldT>* ghostMask_;

    SpatialOps::MemoryWindow* newMemoryWindow_;

    const Expr::TagSet tags_;
  };


}//end namespace
#endif // Filter_h

