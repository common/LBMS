/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>
#include <spatialops/SpatialOpsDefs.h>

#ifndef NSCBCDiffFluxTreatment_h
#define NSCBCDiffFluxTreatment_h

namespace LBMS{

  /**
   *  \class NSCBCDiffFluxTreatment
   *  \author Derek Cline
   *  \date   April, 2016
   *
   *  \brief Provides 3 Point NSCBCDiffFluxTreatment for diffusive fluxterms.  A mask point will be shifted in
   *         the direction of the NSCBCDiffFluxTreatment.  For example, if a mask point is on the first interior
   *         cell, phi_1, and an XMinus-Direction is given, then this expression will apply:
   *         \f$\phi_0=2\cdot\phi_1-\phi_2 \f$
   *
   *         Ensure that Neumann zero conditions are applied prior to use
   *
   *
   */
  template< typename FieldT, typename OpT >
  class NSCBCDiffFluxTreatment : public Expr::Expression<FieldT>
  {
  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag velT_;
      const SpatialOps::SpatialMask<FieldT> mask_;
      const SpatialOps::BCSide side_;
      int isNormalStress_;
    public:

      /**
       *  Provides 3 Point NSCBCDiffFluxTreatment for volume field terms.
       *  This will take a diffusive flux field and its boundary mask that
       *  has ALREADY had its boundary flux set to a neumann zero condition
       *  and will replace that neumann zero flux with an extrapolant in the
       *  appropriate case.  For outflow, we extrapolate the normal stress (e.g.
       *  tau_yy) and zero the other stresses.  For an inflow, we zero the normal
       *  stress and extrapolate the other fluxes.  Other fluxes include
       *  species diffusion, energy diffusion and perpendicular stresses).
       *
       *  @param result The resulting field with the extrapolated value
       *  @param velTag The velocity field (should be a face field, use advecting velocity)
       *  @param side The plus or minus side associated with the domain boundary
       *  @param isNormalStress_ This will be -1 for any term that is
       *         not \f$\tau_\alpha_\alpha \f$ and 1 for \f$\tau_\alpha_\alpha \f$
       */

      Builder( const Expr::Tag& result,
               const Expr::Tag& velTag,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const SpatialOps::BCSide& side,
               int isNormalStress );

      Expr::ExpressionBase* build() const;
    };

    void evaluate();
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  private:

    NSCBCDiffFluxTreatment( const Expr::Tag& velTag,
                            const SpatialOps::SpatialMask<FieldT>& mask,
                            const SpatialOps::BCSide& side,
                            int isNormalStress );

    const OpT* extrapOp_;
    const Expr::Tag velT_;
    const SpatialOps::SpatialMask<FieldT> mask_;
    const SpatialOps::BCSide side_;
    int isNormalStress_;

    DECLARE_FIELDS( FieldT, vel_ )
  };



  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT, typename OpT >
  NSCBCDiffFluxTreatment<FieldT,OpT>::NSCBCDiffFluxTreatment( const Expr::Tag& velTag,
                                                              const SpatialOps::SpatialMask<FieldT>& mask,
                                                              const SpatialOps::BCSide& side,
                                                              int isNormalStress )
    : Expr::Expression<FieldT>(),
      velT_(velTag),
      mask_(mask),
      side_(side)
  {
    //Ensure that we calculate "outflow" at the minus side correctly
    if( side_ == SpatialOps::MINUS_SIDE ){
      isNormalStress_ = -1.0 * isNormalStress;
    }
    else if( side_ == SpatialOps::PLUS_SIDE  ){
      isNormalStress_ = isNormalStress;
    }
    else{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid direction in MomentumEquation NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
    }

    vel_ = this->template create_field_request<FieldT>( velTag );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void NSCBCDiffFluxTreatment<FieldT,OpT>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    extrapOp_ = opDB.retrieve_operator<OpT>();
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  void NSCBCDiffFluxTreatment<FieldT,OpT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& vel = vel_->field_ref();

    SpatFldPtr<FieldT> tempField  = SpatialFieldStore::get<FieldT>( result );
    SpatFldPtr<FieldT> tempField2 = SpatialFieldStore::get<FieldT>( result );

    *tempField  <<= result;
    *tempField2 <<= result;

    masked_assign( mask_, *tempField2, (*extrapOp_)(*tempField) );

    //Cond statements seem to struggle with operators AND masks.
    *tempField2 <<= cond( (isNormalStress_*vel) > 0.0, *tempField2 )
                        (                              *tempField  );

    masked_assign( mask_, result, *tempField2 );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  NSCBCDiffFluxTreatment<FieldT,OpT>::Builder::
  Builder( const Expr::Tag& result,
           const Expr::Tag& velTag,
           const SpatialOps::SpatialMask<FieldT>& mask,
           const SpatialOps::BCSide& side,
           int isNormalStress )
    : ExpressionBuilder(result),
      velT_( velTag ),
      mask_(mask),
      side_(side),
      isNormalStress_(isNormalStress)
  {}

  //--------------------------------------------------------------------

  template< typename FieldT, typename OpT >
  Expr::ExpressionBase*
  NSCBCDiffFluxTreatment<FieldT,OpT>::Builder::build() const
  {
    return new NSCBCDiffFluxTreatment<FieldT,OpT>( velT_, mask_, side_, isNormalStress_ );
  }

}//end namespace

#endif // NSCBCDiffFluxTreatment_h

