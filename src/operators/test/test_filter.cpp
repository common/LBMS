/*
 * test_flux.cpp
 *
 *  Created on: Feb, something, I forget
 *      Author: Derek Cline
 */

#include <fields/Checkpoint.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <operators/Filter.h>
#include <operators/Operators.h>
#include <mpi/Environment.h>
#include <fields/Fields.h>
#include <test/TestHelper.h>
#include <boost/program_options.hpp>
#include <expression/ExprLib.h>

namespace po=boost::program_options;

using namespace Expr;
using namespace LBMS;
using namespace std;
using SpatialOps::IntVec;

int main( int iarg, char* carg[] )
{
  const int nghost = 1;
  IntVec nCoarse, nFine;
  LBMS::Coordinate length;
  bool doVisualization;
  std::string name;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(20), "Fine grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(20), "Fine grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(20), "Fine grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(10),"Coarse grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(10),"Coarse grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(10),"Coarse grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "name", po::value<std::string>(&name)->default_value(""),"checkpoint dir tag")
      ( "visualization", po::value<bool>(&doVisualization)->default_value(false),"output?");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }// end scope

  try{

    Environment::setup(iarg,carg);

    const MeshPtr mesh( new Mesh(nCoarse,nFine,length,true,true,true) ); // assumes periodic
    Environment::set_topology( mesh );

    const BundlePtr xbundle = Environment::bundle(LBMS::XDIR);
    const BundlePtr ybundle = Environment::bundle(LBMS::YDIR);
    const BundlePtr zbundle = Environment::bundle(LBMS::ZDIR);

    BundlePtrVec bundles;
    bundles.push_back( xbundle ); build_operators( xbundle );
    bundles.push_back( ybundle ); build_operators( ybundle );
    bundles.push_back( zbundle ); build_operators( zbundle );

    Expr::FMLMap fmls;

    BOOST_FOREACH( BundlePtr b, bundles ){ fmls[b->id()] = b->get_field_manager_list(); }

    LBMS::Filter<SpatialOps::XDIR> *xfilter = NULL;
    LBMS::Filter<SpatialOps::YDIR> *yfilter = NULL;
    LBMS::Filter<SpatialOps::ZDIR> *zfilter = NULL;

    FieldManagerList& fmlx = *xbundle->get_field_manager_list();
    FieldManagerList& fmly = *ybundle->get_field_manager_list();
    FieldManagerList& fmlz = *zbundle->get_field_manager_list();

    FieldMgrSelector<XVolField>::type& xvolfm = fmlx.field_manager<XVolField>();
    FieldMgrSelector<YVolField>::type& yvolfm = fmly.field_manager<YVolField>();
    FieldMgrSelector<ZVolField>::type& zvolfm = fmlz.field_manager<ZVolField>();

    TagSet tags, filterTags;

    const Tag phixTag  ( "phi_XBundle", STATE_N    );
    const Tag phiyTag  ( "phi_YBundle", STATE_N    );
    const Tag phizTag  ( "phi_ZBundle", STATE_N    );
    const Tag xcoordTag( "coordx",      STATE_NONE );
    const Tag ycoordTag( "coordy",      STATE_NONE );
    const Tag zcoordTag( "coordz",      STATE_NONE );

    //tags for checkpoint writing
    filterTags.insert( phixTag ); filterTags.insert( phiyTag ); filterTags.insert( phizTag );
    tags.insert( xcoordTag ); tags.insert( ycoordTag ); tags.insert( zcoordTag );

    BOOST_FOREACH( const Expr::Tag& tag, tags ){
      xvolfm.register_field( tag, nghost );
      yvolfm.register_field( tag, nghost );
      zvolfm.register_field( tag, nghost );
    }

    xvolfm.register_field( phixTag, nghost );
    yvolfm.register_field( phiyTag, nghost );
    zvolfm.register_field( phizTag, nghost );

    xvolfm.allocate_fields( boost::cref(xbundle->get_field_info() ) );
    yvolfm.allocate_fields( boost::cref(ybundle->get_field_info() ) );
    zvolfm.allocate_fields( boost::cref(zbundle->get_field_info() ) );

    XVolField& phix    = xvolfm.field_ref(phixTag);
    XVolField& xcoordx = xvolfm.field_ref(xcoordTag);
    XVolField& xcoordy = xvolfm.field_ref(ycoordTag);
    XVolField& xcoordz = xvolfm.field_ref(zcoordTag);

    YVolField& phiy    = yvolfm.field_ref(phiyTag);
    YVolField& ycoordx = yvolfm.field_ref(xcoordTag);
    YVolField& ycoordy = yvolfm.field_ref(ycoordTag);
    YVolField& ycoordz = yvolfm.field_ref(zcoordTag);

    ZVolField& phiz    = zvolfm.field_ref(phizTag);
    ZVolField& zcoordx = zvolfm.field_ref(xcoordTag);
    ZVolField& zcoordy = zvolfm.field_ref(ycoordTag);
    ZVolField& zcoordz = zvolfm.field_ref(zcoordTag);

    LBMS::set_coord_values( xcoordx, xcoordy, xcoordz, xbundle );
    LBMS::set_coord_values( ycoordx, ycoordy, ycoordz, ybundle );
    LBMS::set_coord_values( zcoordx, zcoordy, zcoordz, zbundle );

    phix <<= sin( xcoordx*(24.0) );
    phiy <<= sin( ycoordy*(24.0) );
    phiz <<= sin( zcoordz*(24.0) );

    if( doVisualization ){
      LBMS::Checkpoint visualization( bundles, filterTags, fmls );
      visualization.write_checkpoint( "filtering_prior" );
    }

    BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
      switch( b->dir() ){
        case LBMS::XDIR: xfilter = new LBMS::Filter<SpatialOps::XDIR>( b, filterTags ); break;
        case LBMS::YDIR: yfilter = new LBMS::Filter<SpatialOps::YDIR>( b, filterTags ); break;
        case LBMS::ZDIR: zfilter = new LBMS::Filter<SpatialOps::ZDIR>( b, filterTags ); break;
        case LBMS::NODIR: break; //supresses warnings
      }
    }

    assert( xfilter != NULL ); xfilter->apply_filter();
    assert( yfilter != NULL ); yfilter->apply_filter();
    assert( zfilter != NULL ); zfilter->apply_filter();

    delete xfilter;
    delete yfilter;
    delete zfilter;

    if( doVisualization ){
      LBMS::Checkpoint visualization( bundles, filterTags, fmls );
      visualization.write_checkpoint( "filtering_post" );
    }

    TestHelper status;
    const double precision = 1000 * std::numeric_limits<double>::epsilon();

    XVolField::const_iterator i = phix.begin();
    status( ( abs(*(i+32)  + 0.889535679334613) < precision ), "x direction filtering");
    status( ( abs(*(i+154) + 0.564642473395035) < precision ), "x direction filtering");
    status( ( abs(*(i+730) + 0.843330458222179) < precision ), "x direction filtering");

    YVolField::const_iterator j = phiy.begin();
    status( ( abs(*(j+32)  - 0.874859030025637) < precision ), "y direction filtering");
    status( ( abs(*(j+154) - 0.913113152664518) < precision ), "y direction filtering");
    status( ( abs(*(j+730) + 0.238969941470928) < precision ), "y direction filtering");

    ZVolField::const_iterator k = phiz.begin();
    status( ( abs(*(k+32)  + 0.564642473395035) < precision ), "z direction filtering");
    status( ( abs(*(k+154) - 0.384622526068308) < precision ), "z direction filtering");
    status( ( abs(*(k+730) + 0.747721368600835) < precision ), "z direction filtering");

    if( status.ok() ){
      return 0; // for successful exit.
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << endl;
  }
  return -1;
}
