#include <operators/InterpFineToCoarse.h>
#include <operators/InterpCoarseToFine.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>

#include <test/TestHelper.h>

#include <spatialops/Nebo.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/FieldManager.h>

#include <boost/program_options.hpp>
namespace po=boost::program_options;

#include <limits>
#include <cmath>

using SpatialOps::IntVec;
using namespace LBMS;
using namespace std;

//#define DUMP_DIAGNOSTICS

const Expr::Tag xft ("xf", Expr::STATE_NONE), yft ("yf", Expr::STATE_NONE), zft ("zf", Expr::STATE_NONE),
                xct ("xc", Expr::STATE_NONE), yct ("yc", Expr::STATE_NONE), zct ("zc", Expr::STATE_NONE),
                xcet("xce",Expr::STATE_NONE), ycet("yce",Expr::STATE_NONE), zcet("zce",Expr::STATE_NONE),
                xfet("xfe",Expr::STATE_NONE), yfet("yfe",Expr::STATE_NONE), zfet("zfe",Expr::STATE_NONE);


template< typename T >
void setup_fields( Expr::LBMSFieldManager<T>& fm,
                   const BundlePtr bundle,
                   const bool isCoarse )
{
  const int nghost = 1;
  if( isCoarse ){
    fm.register_field(xct, nghost);  fm.register_field(xcet, nghost);
    fm.register_field(yct, nghost);  fm.register_field(ycet, nghost);
    fm.register_field(zct, nghost);  fm.register_field(zcet, nghost);
  }
  else{
    fm.register_field(xft, nghost);  fm.register_field(xfet, nghost);
    fm.register_field(yft, nghost);  fm.register_field(yfet, nghost);
    fm.register_field(zft, nghost);  fm.register_field(zfet, nghost);
  }

  fm.allocate_fields( boost::cref(bundle->get_field_info()) );

  if( isCoarse ){
    set_coord_values<T>( fm.field_ref(xct ), fm.field_ref(yct ), fm.field_ref(zct ), bundle );
    set_coord_values<T>( fm.field_ref(xcet), fm.field_ref(ycet), fm.field_ref(zcet), bundle );
  }
  else{
    set_coord_values<T>( fm.field_ref(xft ), fm.field_ref(yft ), fm.field_ref(zft ), bundle );
    set_coord_values<T>( fm.field_ref(xfet), fm.field_ref(yfet), fm.field_ref(zfet), bundle );
  }
}

template< typename FieldT >
bool compare_results( const Direction dir,
                      const FieldT& f1,
                      const FieldT& f2 )
{
  SpatialOps::MemoryWindow mw = f1.window_without_ghost();

  const int fieldLocDir = int(FieldT::Location::FaceDir::value);

  // on non-domain boundaries (periodic & processor boundaries) we require an extra valid ghost cell on the + side.
  if( dir == fieldLocDir && !f1.boundary_info().has_bc(dir,SpatialOps::PLUS_SIDE) ){
    mw.extent(dir) += f1.get_ghost_data().get_plus(dir);
  }

  const FieldT f1part( mw, f1 );
  const FieldT f2part( mw, f2 );

  return SpatialOps::field_equal_abs( f1part, f2part, 10*std::numeric_limits<double>::epsilon() );
}

template< typename SrcT >
bool test_fine_to_coarse( const Mesh& mesh )
{
  const BundlePtr bundle = mesh.bundle( direction<typename SrcT::Location::Bundle>() );
  typedef typename InterpFineToCoarse<SrcT>::DestT DestT;
  const InterpFineToCoarse<SrcT> interpf2c( *bundle );

  Expr::LBMSFieldManager<SrcT > srcfm;
  Expr::LBMSFieldManager<DestT> destfm;

  setup_fields<SrcT >( srcfm,  bundle, false );
  setup_fields<DestT>( destfm, bundle, true  );

  SrcT&  xf =  srcfm.field_ref(xft);
  SrcT&  yf =  srcfm.field_ref(yft);
  SrcT&  zf =  srcfm.field_ref(zft);
  DestT& xc = destfm.field_ref(xct);
  DestT& yc = destfm.field_ref(yct);
  DestT& zc = destfm.field_ref(zct);

  xc <<= -99;  interpf2c.apply_to_field(xf,xc);
  yc <<= -99;  interpf2c.apply_to_field(yf,yc);
  zc <<= -99;  interpf2c.apply_to_field(zf,zc);

  DestT& xce = destfm.field_ref(xcet);
  DestT& yce = destfm.field_ref(ycet);
  DestT& zce = destfm.field_ref(zcet);

  set_coord_values<DestT>( xce, yce, zce, bundle );

# ifdef DUMP_DIAGNOSTICS
  write_matlab( xf, "xfine"  , true );
  write_matlab( yf, "yfine"  , true );
  write_matlab( zf, "zfine"  , true );
  write_matlab( xc, "xcoarse", true );
  write_matlab( yc, "ycoarse", true );
  write_matlab( zc, "zcoarse", true );
  write_matlab( xce, "xce", true );
  write_matlab( yce, "yce", true );
  write_matlab( zce, "zce", true );
# endif

  TestHelper status(false);

  status( compare_results( bundle->dir(), xce, xc ), "X values" );
  status( compare_results( bundle->dir(), yce, yc ), "Y values" );
  status( compare_results( bundle->dir(), zce, zc ), "Z values" );

  return status.ok();
}

template<typename DestT>
bool test_coarse_to_fine( const Mesh& mesh )
{
  typedef InterpCoarseToFine<DestT> InterpC2F;
  typedef typename InterpC2F::SrcT    SrcT;

  typedef InterpFineToCoarse<DestT> InterpF2C;

  const BundlePtr bundle = mesh.bundle( direction<typename DestT::Location::Bundle>() );

  const InterpC2F interpc2f( *bundle );
  const InterpF2C interpf2c( *bundle );

  Expr::LBMSFieldManager<SrcT >  srcfm;
  Expr::LBMSFieldManager<DestT> destfm;

  setup_fields<SrcT >( srcfm,  bundle, true  );
  setup_fields<DestT>( destfm, bundle, false );

  SrcT&        xc =  srcfm.field_ref(xct);
  SrcT&        yc =  srcfm.field_ref(yct);
  SrcT&        zc =  srcfm.field_ref(zct);
  const SrcT& xce = srcfm.field_ref(xcet);
  const SrcT& yce = srcfm.field_ref(ycet);
  const SrcT& zce = srcfm.field_ref(zcet);

  DestT& xf = destfm.field_ref(xft);
  DestT& yf = destfm.field_ref(yft);
  DestT& zf = destfm.field_ref(zft);

  xf <<= -99.9;  interpc2f.apply_to_field( xc, xf );
  yf <<= -99.9;  interpc2f.apply_to_field( yc, yf );
  zf <<= -99.9;  interpc2f.apply_to_field( zc, zf );

  const DestT& xfe = destfm.field_ref(xfet);
  const DestT& yfe = destfm.field_ref(yfet);
  const DestT& zfe = destfm.field_ref(zfet);

# ifdef DUMP_DIAGNOSTICS
  write_matlab( xf, "xfine"  , false );
  write_matlab( yf, "yfine"  , false );
  write_matlab( zf, "zfine"  , false );
  write_matlab( xc, "xcoarse", false );
  write_matlab( yc, "ycoarse", false );
  write_matlab( zc, "zcoarse", false );
  write_matlab( xfe, "xfe", false );
  write_matlab( yfe, "yfe", false );
  write_matlab( zfe, "zfe", false );
# endif

  TestHelper status(false);

  // Ensure that the round-trip preserves results properly.
  // Since we are testing the f2c operator separately,
  // this should provide robust testing of the c2f operator.
  xc <<= -9.9;
  interpf2c.apply_to_field( xf, xc );
  status( field_max_interior( abs(xce-xc) ) < 1e-14, "round trip x" );

  {
    // jcs this exposed a bug in SpatialOps so I am leaving it here for good measure
    SpatialOps::SpatFldPtr<SrcT> c = SpatialOps::SpatialFieldStore::get<SrcT>(xf);
    status( c->window_with_ghost() == xf.window_with_ghost(), "window" );
  }

  yc <<= -9.9;
  interpf2c.apply_to_field( yf, yc );
  status( field_max_interior( abs(yce-yc) ) < 1e-14, "round trip y" );

  zc <<= -9.9;
  interpf2c.apply_to_field( zf, zc );
  status( field_max_interior( abs(zce-zc) ) < 1e-14, "round trip z" );

  return status.ok();
}

//===================================================================

bool driver( const IntVec& nCoarse, const IntVec& nFine,
             const LBMS::Coordinate& length,
             const bool periX, const bool periY, const bool periZ )
{
  using SpatialOps::IntVec;
  const LBMS::Mesh mesh( nCoarse, nFine, length, periX, periY, periZ );

  TestHelper status(true);

  if( nFine[0]>1 ){
    status( test_fine_to_coarse<  XVolField>(mesh), "fine->coarse XVolField"   );
    status( test_fine_to_coarse<XSurfXField>(mesh), "fine->coarse XSurfXField" );
    status( test_fine_to_coarse<XSurfYField>(mesh), "fine->coarse XSurfYField" );
    status( test_fine_to_coarse<XSurfZField>(mesh), "fine->coarse XSurfZField" );

    status( test_coarse_to_fine<  XVolField>(mesh), "coarse->fine XVolField"   );
    status( test_coarse_to_fine<XSurfXField>(mesh), "coarse->fine XSurfXField" );
    status( test_coarse_to_fine<XSurfYField>(mesh), "coarse->fine XSurfYField" );
    status( test_coarse_to_fine<XSurfZField>(mesh), "coarse->fine XSurfZField" );
  }
  if( nFine[1]>1 ){
    status( test_fine_to_coarse<  YVolField>(mesh), "fine->coarse YVolField"   );
    status( test_fine_to_coarse<YSurfXField>(mesh), "fine->coarse YSurfXField" );
    status( test_fine_to_coarse<YSurfYField>(mesh), "fine->coarse YSurfYField" );
    status( test_fine_to_coarse<YSurfZField>(mesh), "fine->coarse YSurfZField" );

    status( test_coarse_to_fine<  YVolField>(mesh), "coarse->fine YVolField"   );
    status( test_coarse_to_fine<YSurfXField>(mesh), "coarse->fine YSurfXField" );
    status( test_coarse_to_fine<YSurfYField>(mesh), "coarse->fine YSurfYField" );
    status( test_coarse_to_fine<YSurfZField>(mesh), "coarse->fine YSurfZField" );
  }
  if( nFine[2]>1 ){
    status( test_fine_to_coarse<  ZVolField>(mesh), "fine->coarse ZVolField"   );
    status( test_fine_to_coarse<ZSurfXField>(mesh), "fine->coarse ZSurfXField" );
    status( test_fine_to_coarse<ZSurfYField>(mesh), "fine->coarse ZSurfYField" );
    status( test_fine_to_coarse<ZSurfZField>(mesh), "fine->coarse ZSurfZField" );

    status( test_coarse_to_fine<  ZVolField>(mesh), "coarse->fine ZVolField"   );
    status( test_coarse_to_fine<ZSurfXField>(mesh), "coarse->fine ZSurfXField" );
    status( test_coarse_to_fine<ZSurfYField>(mesh), "coarse->fine ZSurfYField" );
    status( test_coarse_to_fine<ZSurfZField>(mesh), "coarse->fine ZSurfZField" );
  }
  return status.ok();
}

//===================================================================

int main( int iarg, char* carg[] )
{
  IntVec nCoarse, nFine;
  LBMS::Coordinate length;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(50), "Fine (ODT) grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(50), "Fine (ODT) grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(50), "Fine (ODT) grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(5),"Coarse (LES) grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(5),"Coarse (LES) grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(5),"Coarse (LES) grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
    return -1;
  }

  TestHelper status(true);

  try{
    status( driver(nCoarse,nFine,length, true, true, true), "Periodic xyz" );
    status( driver(nCoarse,nFine,length, true,false, true), "Periodic x z" );
    status( driver(nCoarse,nFine,length, true, true,false), "Periodic xy " );
    status( driver(nCoarse,nFine,length,false, true, true), "Periodic  yz" );
    status( driver(nCoarse,nFine,length, true,false,false), "Periodic x  " );
    status( driver(nCoarse,nFine,length,false, true,false), "Periodic  y " );
    status( driver(nCoarse,nFine,length,false,false, true), "Periodic   z" );
    status( driver(nCoarse,nFine,length,false,false,false), "non-Periodic" );

    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;
}

//===================================================================
