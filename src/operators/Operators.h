#ifndef LBMSOperators_h
#define LBMSOperators_h

#include <spatialops/structured/stencil/FVStaggeredBCOp.h>
#include <spatialops/structured/stencil/FVStaggeredOperatorTypes.h>
#include <spatialops/structured/stencil/OneSidedOperatorTypes.h>

#include <fields/Fields.h>
#include <lbms/Bundle_fwd.h>

#include <operators/InterpFineToCoarse.h>
#include <operators/InterpCoarseToFine.h>

#include <spatialops/structured/stencil/StencilBuilder.h>
#include <spatialops/Nebo.h>

/**
 *  \file  Operators.h
 *  \brief Defines operators for use in LBMS.
 */

namespace SpatialOps{
  class OperatorDatabase;   // forward declaration
}

namespace SpatialOps{
  template<typename Op, typename StencilT, typename FieldT, typename Offset>
  struct OneSidedOpTypeBuilder;   // forward declaration
}

namespace LBMS{
  /**
   *  \brief Construct all of the required LBMS operators on the given
   *         Bundle and place them in the OperatorDatabase on that Bundle.
   */
  void build_operators( const BundlePtr& b );

  template< typename FieldT, typename StencilT, typename CoefCollection >
  void build_one_sided_gradients( SpatialOps::OperatorDatabase& opdb,
                                  const CoefCollection& coefs );
  template< typename StencilT, typename CoefCollection >
  void build_one_sided_gradients( SpatialOps::OperatorDatabase& opdb,
                                  const CoefCollection& coefs );

  template< typename FieldT, typename StencilT, typename CoefCollection >
  void build_extrapolants( SpatialOps::OperatorDatabase& opdb,
                           const CoefCollection& coefs );
  template< typename StencilT, typename CoefCollection >
  void build_extrapolants( SpatialOps::OperatorDatabase& opdb,
                           const CoefCollection& coefs );
} // namespace LBMS


namespace SpatialOps{

#define FINE2COARSE( FINET )                           \
  template<> struct OperatorTypeBuilder                \
         < SpatialOps::Interpolant,                    \
           FINET,                                      \
           LBMS::CoarseFieldSelector<FINET>::type >    \
  {                                                    \
    typedef LBMS::InterpFineToCoarse<FINET> type;      \
  };

#define COARSE2FINE( FINET )                           \
  template<> struct OperatorTypeBuilder                \
         < SpatialOps::Interpolant,                    \
           LBMS::CoarseFieldSelector<FINET>::type,     \
           FINET >                                     \
  {                                                    \
    typedef LBMS::InterpCoarseToFine<FINET> type;      \
  };

  template< typename DirT > struct FilterSelector;

  //Typedefs for one sided stencils (these specify the direction and size)
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::XDIR>::type         > X2PlusT;
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::XDIR>::type::Negate > X2MinusT;
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::YDIR>::type         > Y2PlusT;
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::YDIR>::type::Negate > Y2MinusT;
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type         > Z2PlusT;
  typedef SpatialOps::OneSidedStencil2< SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type::Negate > Z2MinusT;

  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::XDIR>::type         > X3PlusT;
  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::XDIR>::type::Negate > X3MinusT;
  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::YDIR>::type         > Y3PlusT;
  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::YDIR>::type::Negate > Y3MinusT;
  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type         > Z3PlusT;
  typedef SpatialOps::OneSidedStencil3< SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type::Negate > Z3MinusT;

  //Typedefs for one sided stencils
  typedef SpatialOps::UnitTriplet<XDIR>::type  XPlus;
  typedef SpatialOps::UnitTriplet<YDIR>::type  YPlus;
  typedef SpatialOps::UnitTriplet<ZDIR>::type  ZPlus;
  typedef XPlus::Negate XMinus;
  typedef YPlus::Negate YMinus;
  typedef ZPlus::Negate ZMinus;

  /** \addtogroup XBundle
   * @{
   */
  BASIC_OPTYPE_BUILDER( LBMS::XVolField )

  COARSE2FINE( LBMS::XSurfXField )
  COARSE2FINE( LBMS::XSurfYField )
  COARSE2FINE( LBMS::XSurfZField )

  FINE2COARSE( LBMS::XSurfXField )
  FINE2COARSE( LBMS::XSurfYField )
  FINE2COARSE( LBMS::XSurfZField )

  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfXField, LBMS::XSurfYField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfXField, LBMS::XSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfYField, LBMS::XSurfXField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfZField, LBMS::XSurfXField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfYField, LBMS::XSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::XSurfZField, LBMS::XSurfYField )
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfXField, LBMS::XSurfYField >::type  X2YInterpX; ///< Interpolate from an x-face to a y-face on an x-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfXField, LBMS::XSurfZField >::type  X2ZInterpX; ///< Interpolate from an x-face to a z-face on an x-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfYField, LBMS::XSurfXField >::type  Y2XInterpX; ///< Interpolate from an y-face to a x-face on an x-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfZField, LBMS::XSurfXField >::type  Z2XInterpX; ///< Interpolate from an z-face to a x-face on an x-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfYField, LBMS::XSurfZField >::type  Y2ZInterpX; ///< Interpolate from an y-face to a z-face on an x-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::XSurfZField, LBMS::XSurfYField >::type  Z2YInterpX; ///< Interpolate from an z-face to a y-face on an x-bundle

  //Points for X direction stencils for filtering
  typedef IndexTriplet<-3, 0, 0> XThirdLower;
  typedef IndexTriplet<-2, 0, 0> XSecondLower;
  typedef IndexTriplet<-1, 0, 0> XFirstLower;
  typedef IndexTriplet< 1, 0, 0> XFirstUpper;
  typedef IndexTriplet< 2, 0, 0> XSecondUpper;
  typedef IndexTriplet< 3, 0, 0> XThirdUpper;
  typedef IndexTriplet< 0, 0, 0> Mid;

  //3, 5 and 7 point stencils (NOT staggered, i.e. the source and dest fields will be the same)
  typedef NeboStencilPointCollection<XFirstLower, NeboNil>::AddPoint<Mid>::Result::AddPoint<XFirstUpper>::Result  ThreePtXCollection;

  typedef NeboStencilPointCollection<XSecondLower, NeboNil>::AddPoint<XFirstLower>::Result::AddPoint<Mid>::Result
          ::AddPoint<XFirstUpper>::Result::AddPoint<XSecondUpper>::Result                                         FivePtXCollection;

  typedef NeboStencilPointCollection<XThirdLower, NeboNil>::AddPoint<XSecondLower>::Result
          ::AddPoint<XFirstLower>::Result::AddPoint<Mid>::Result::AddPoint<XFirstUpper>::Result
          ::AddPoint<XSecondUpper>::Result::AddPoint<XThirdUpper>::Result                                         SevenPtXCollection;

  //Filter operations
  template<> struct FilterSelector<SpatialOps::XDIR>{
    typedef NeboStencilBuilder<Filter, ThreePtXCollection, LBMS::XVolField, LBMS::XVolField> ThreePtFilterT;
    typedef NeboStencilBuilder<Filter, FivePtXCollection,  LBMS::XVolField, LBMS::XVolField> FivePtFilterT;
    typedef NeboStencilBuilder<Filter, SevenPtXCollection, LBMS::XVolField, LBMS::XVolField> SevenPtFilterT;
  };

  /**@}*/

  /** \addtogroup YBundle
   * @{
   */
  BASIC_OPTYPE_BUILDER( LBMS::YVolField )

  COARSE2FINE( LBMS::YSurfXField )
  COARSE2FINE( LBMS::YSurfYField )
  COARSE2FINE( LBMS::YSurfZField )

  FINE2COARSE( LBMS::YSurfXField )
  FINE2COARSE( LBMS::YSurfYField )
  FINE2COARSE( LBMS::YSurfZField )

  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfYField, LBMS::YSurfXField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfYField, LBMS::YSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfXField, LBMS::YSurfYField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfZField, LBMS::YSurfYField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfXField, LBMS::YSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::YSurfZField, LBMS::YSurfXField )
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfYField, LBMS::YSurfXField >::type  Y2XInterpY; ///< Interpolate from the y-face to the x-face on the y-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfYField, LBMS::YSurfZField >::type  Y2ZInterpY; ///< Interpolate from the y-face to the z-face on the y-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfXField, LBMS::YSurfYField >::type  X2YInterpY; ///< Interpolate from the x-face to the y-face on the y-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfZField, LBMS::YSurfYField >::type  Z2YInterpY; ///< Interpolate from the z-face to the y-face on the y-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfXField, LBMS::YSurfZField >::type  X2ZInterpY; ///< Interpolate from the x-face to the z-face on the y-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::YSurfZField, LBMS::YSurfXField >::type  Z2XInterpY; ///< Interpolate from the z-face to the x-face on the y-bundle

  //Points for Y direction stencils for filtering
  typedef IndexTriplet<0, -3, 0> YThirdLower;
  typedef IndexTriplet<0, -2, 0> YSecondLower;
  typedef IndexTriplet<0, -1, 0> YFirstLower;
  typedef IndexTriplet<0,  1, 0> YFirstUpper;
  typedef IndexTriplet<0,  2, 0> YSecondUpper;
  typedef IndexTriplet<0,  3, 0> YThirdUpper;
  typedef IndexTriplet<0,  0, 0> Mid;

  //3, 5 and 7 point stencils (NOT staggered, i.e. the source and dest fields will be the same)
  typedef NeboStencilPointCollection<YFirstLower, NeboNil>::AddPoint<Mid>::Result::AddPoint<YFirstUpper>::Result  ThreePtYCollection;

  typedef NeboStencilPointCollection<YSecondLower, NeboNil>::AddPoint<YFirstLower>::Result::AddPoint<Mid>::Result
          ::AddPoint<YFirstUpper>::Result::AddPoint<YSecondUpper>::Result                                         FivePtYCollection;

  typedef NeboStencilPointCollection<YThirdLower, NeboNil>::AddPoint<YSecondLower>::Result
          ::AddPoint<YFirstLower>::Result::AddPoint<Mid>::Result::AddPoint<YFirstUpper>::Result
          ::AddPoint<YSecondUpper>::Result::AddPoint<YThirdUpper>::Result                                         SevenPtYCollection;

  //Filter operations
  template<> struct FilterSelector<SpatialOps::YDIR>{
    typedef NeboStencilBuilder<Filter, ThreePtYCollection, LBMS::YVolField, LBMS::YVolField> ThreePtFilterT;
    typedef NeboStencilBuilder<Filter, FivePtYCollection,  LBMS::YVolField, LBMS::YVolField> FivePtFilterT;
    typedef NeboStencilBuilder<Filter, SevenPtYCollection, LBMS::YVolField, LBMS::YVolField> SevenPtFilterT;
  };

  /**@}*/

  /** \addtogroup ZBundle
   * @{
   */
  BASIC_OPTYPE_BUILDER( LBMS::ZVolField )

  COARSE2FINE( LBMS::ZSurfXField )
  COARSE2FINE( LBMS::ZSurfYField )
  COARSE2FINE( LBMS::ZSurfZField )

  FINE2COARSE( LBMS::ZSurfXField )
  FINE2COARSE( LBMS::ZSurfYField )
  FINE2COARSE( LBMS::ZSurfZField )

  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfZField, LBMS::ZSurfXField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfZField, LBMS::ZSurfYField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfXField, LBMS::ZSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfYField, LBMS::ZSurfZField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfXField, LBMS::ZSurfYField )
  OP_BUILDER( Stencil4Collection, SpatialOps::Interpolant, LBMS::ZSurfYField, LBMS::ZSurfXField )
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfZField, LBMS::ZSurfXField >::type  Z2XInterpZ; ///< Interpolate from the z-face to the x-face on the z-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfZField, LBMS::ZSurfYField >::type  Z2YInterpZ; ///< Interpolate from the z-face to the y-face on the z-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfXField, LBMS::ZSurfZField >::type  X2ZInterpZ; ///< Interpolate from the x-face to the z-face on the z-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfYField, LBMS::ZSurfZField >::type  Y2ZInterpZ; ///< Interpolate from the y-face to the z-face on the z-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfXField, LBMS::ZSurfYField >::type  X2YInterpZ; ///< Interpolate from the x-face to the y-face on the z-bundle
  typedef OperatorTypeBuilder< SpatialOps::Interpolant, LBMS::ZSurfYField, LBMS::ZSurfXField >::type  Y2XInterpZ; ///< Interpolate from the y-face to the x-face on the z-bundle

  //Points for Z direction stencils for filtering
  typedef IndexTriplet<0, 0, -3> ZThirdLower;
  typedef IndexTriplet<0, 0, -2> ZSecondLower;
  typedef IndexTriplet<0, 0, -1> ZFirstLower;
  typedef IndexTriplet<0, 0,  1> ZFirstUpper;
  typedef IndexTriplet<0, 0,  2> ZSecondUpper;
  typedef IndexTriplet<0, 0,  3> ZThirdUpper;
  typedef IndexTriplet<0, 0,  0> Mid;

  //3, 5 and 7 point stencils (NOT staggered, i.e. the source and dest fields will be the same)
  typedef NeboStencilPointCollection<ZFirstLower, NeboNil>::AddPoint<Mid>::Result::AddPoint<ZFirstUpper>::Result  ThreePtZCollection;

  typedef NeboStencilPointCollection<ZSecondLower, NeboNil>::AddPoint<ZFirstLower>::Result::AddPoint<Mid>::Result
          ::AddPoint<ZFirstUpper>::Result::AddPoint<ZSecondUpper>::Result                                         FivePtZCollection;

  typedef NeboStencilPointCollection<ZThirdLower, NeboNil>::AddPoint<ZSecondLower>::Result
          ::AddPoint<ZFirstLower>::Result::AddPoint<Mid>::Result::AddPoint<ZFirstUpper>::Result
          ::AddPoint<ZSecondUpper>::Result::AddPoint<ZThirdUpper>::Result                                        SevenPtZCollection;

  //Filter operations
  template<> struct FilterSelector<SpatialOps::ZDIR>{
    typedef NeboStencilBuilder<Filter, ThreePtZCollection, LBMS::ZVolField, LBMS::ZVolField> ThreePtFilterT;
    typedef NeboStencilBuilder<Filter, FivePtZCollection,  LBMS::ZVolField, LBMS::ZVolField> FivePtFilterT;
    typedef NeboStencilBuilder<Filter, SevenPtZCollection, LBMS::ZVolField, LBMS::ZVolField> SevenPtFilterT;
  };

  template<> struct BCOpTypes< LBMS::XSurfXField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::XSurfXField, LBMS::XVolField>::type> DirichletX;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::XSurfXField, LBMS::XVolField>::type> NeumannX;
  };
  template<> struct BCOpTypes< LBMS::XSurfYField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::XSurfYField, LBMS::XVolField>::type> DirichletY;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::XSurfYField, LBMS::XVolField>::type> NeumannY;
  };
  template<> struct BCOpTypes< LBMS::XSurfZField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::XSurfZField, LBMS::XVolField>::type> DirichletZ;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::XSurfZField, LBMS::XVolField>::type> NeumannZ;
  };

  template<> struct BCOpTypes< LBMS::YSurfXField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::YSurfXField, LBMS::YVolField>::type> DirichletX;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::YSurfXField, LBMS::YVolField>::type> NeumannX;
  };
  template<> struct BCOpTypes< LBMS::YSurfYField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::YSurfYField, LBMS::YVolField>::type> DirichletY;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::YSurfYField, LBMS::YVolField>::type> NeumannY;
  };
  template<> struct BCOpTypes< LBMS::YSurfZField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::YSurfZField, LBMS::YVolField>::type> DirichletZ;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::YSurfZField, LBMS::YVolField>::type> NeumannZ;
  };

  template<> struct BCOpTypes< LBMS::ZSurfXField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::ZSurfXField, LBMS::ZVolField>::type> DirichletX;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::ZSurfXField, LBMS::ZVolField>::type> NeumannX;
  };
  template<> struct BCOpTypes< LBMS::ZSurfYField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::ZSurfYField, LBMS::ZVolField>::type> DirichletY;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::ZSurfYField, LBMS::ZVolField>::type> NeumannY;
  };
  template<> struct BCOpTypes< LBMS::ZSurfZField >{
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Interpolant, LBMS::ZSurfZField, LBMS::ZVolField >::type> DirichletZ;
    typedef NeboBoundaryConditionBuilder<OperatorTypeBuilder<SpatialOps::Divergence,  LBMS::ZSurfZField, LBMS::ZVolField >::type> NeumannZ;
  };

  /**@}*/

BUILD_ONE_SIDED_STENCILS( LBMS::XVolField )
BUILD_ONE_SIDED_STENCILS( LBMS::YVolField )
BUILD_ONE_SIDED_STENCILS( LBMS::ZVolField )


#define BUILD_EXTRAPOLANT_OP( Op, I1, I2, I3, FieldT )                                                                  \
  template<> struct OneSidedOpTypeBuilder<Op,OneSidedStencil3<IndexTriplet<I1,I2,I3> >,FieldT,IndexTriplet<0,0,0> >{ \
    typedef NeboStencilBuilder<Op,OneSidedStencil3<IndexTriplet<I1,I2,I3> >::StPtCollection, FieldT, FieldT>  type;     \
  };

#define BUILD_EXTRAPOLANTS( FieldT )                      \
  BUILD_EXTRAPOLANT_OP( Extrapolant,  1, 0, 0, FieldT )   \
  BUILD_EXTRAPOLANT_OP( Extrapolant, -1, 0, 0, FieldT )   \
  BUILD_EXTRAPOLANT_OP( Extrapolant,  0, 1, 0, FieldT )   \
  BUILD_EXTRAPOLANT_OP( Extrapolant,  0,-1, 0, FieldT )   \
  BUILD_EXTRAPOLANT_OP( Extrapolant,  0, 0, 1, FieldT )   \
  BUILD_EXTRAPOLANT_OP( Extrapolant,  0, 0,-1, FieldT )


BUILD_EXTRAPOLANTS( LBMS::XVolField   )
BUILD_EXTRAPOLANTS( LBMS::YVolField   )
BUILD_EXTRAPOLANTS( LBMS::ZVolField   )

//For now, we only need to extrapolate fluxes across a boundary
//Perpendicular fluxes are fully calculated after velocity volume
//fields are extrapolated.
BUILD_EXTRAPOLANTS( LBMS::XSurfXField )
BUILD_EXTRAPOLANTS( LBMS::XSurfYField )
BUILD_EXTRAPOLANTS( LBMS::XSurfZField )
BUILD_EXTRAPOLANTS( LBMS::YSurfXField )
BUILD_EXTRAPOLANTS( LBMS::YSurfYField )
BUILD_EXTRAPOLANTS( LBMS::YSurfZField )
BUILD_EXTRAPOLANTS( LBMS::ZSurfXField )
BUILD_EXTRAPOLANTS( LBMS::ZSurfYField )
BUILD_EXTRAPOLANTS( LBMS::ZSurfZField )

} // namespace SpatialOps

#endif // LBMSOperators_h
