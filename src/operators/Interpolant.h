#ifndef LBMSInterpolant_h
#define LBMSInterpolant_h

#include <spatialops/SpatialOpsDefs.h>  // defines SpatialOps::Interpolant

#include <fields/Fields.h>
#include <lbms/Mesh.h>

namespace LBMS{

  /**
   *  \class LinearInterpolant
   *  \author James C. Sutherland
   *  \date Aug, 2010
   *
   *  \brief Provides support for linear interpolation from volume to
   *         surface fields on uniform meshes in the direction
   *         associated with the line bundle.
   */
  template< typename SrcFieldT, typename DestFieldT >
  class LinearInterpolant
  {
    const Bundle& bundle_;
    const int nx_, ny_, nz_;
    const int nxd_, nyd_, nzd_;
    const int nxs_, nys_, nzs_;

  public:

    // these are required typedefs for this to behave as a SpatialOperator
    typedef SpatialOps::Interpolant Type;
    typedef SrcFieldT SrcFieldType;
    typedef DestFieldT DestFieldType;

    typedef typename SrcFieldT::Ghost      SrcGhost;
    typedef typename SrcFieldT::Location   SrcLocation;

    typedef typename DestFieldT::Ghost     DestGhost;
    typedef typename DestFieldT::Location  DestLocation;


    LinearInterpolant( const Bundle& bundle )
      : bundle_( bundle ),
        nx_( bundle.npts(XDIR) ),
        ny_( bundle.npts(YDIR) ),
        nz_( bundle.npts(ZDIR) ),
        nxs_( LBMS::npts< SrcFieldT>( XDIR, bundle ) ),
        nys_( LBMS::npts< SrcFieldT>( YDIR, bundle ) ),
        nzs_( LBMS::npts< SrcFieldT>( ZDIR, bundle ) ),
        nxd_( LBMS::npts<DestFieldT>( XDIR, bundle ) ),
        nyd_( LBMS::npts<DestFieldT>( YDIR, bundle ) ),
        nzd_( LBMS::npts<DestFieldT>( ZDIR, bundle ) )
    {}

    void apply_to_field( const SrcFieldT& src, DestFieldT& dest ) const
    {
      IndexTriplet ijkDest = flat2ijk<DestFieldT>( 0, dest.ghost_data(), bundle_ );
      shift_dest_index( ijkDest );

      const int stride = calculate_stride();

      typename SrcFieldT::const_iterator isrc1=src.begin();
      typename SrcFieldT::const_iterator isrc2=isrc1+stride;
      typename DestFieldT::iterator idest=dest.begin();

      idest += ijk2flat<DestFieldT>(ijkDest,dest.ghost_data(),bundle_);

      const int ihi = upper_bound( XDIR );  // jcs this is all whacked for y-dir cases.
      const int jhi = upper_bound( YDIR );
      const int khi = upper_bound( ZDIR );

      // these indices are relative to the (-) side source (volume) field.
      for( int k=0; k<khi; ++k ){
        for( int j=0; j<jhi; ++j ){
          for( int i=0; i<ihi; ++i ){
            // assumes uniform mesh - interpolate to center of src points
            *idest = 0.5*( *isrc1 + *isrc2 );
//             std::cout << "(" << i << "," << j <<"," << k << ") : "
//                       << *isrc1 << ", " << *isrc2 << ", " << *idest << std::endl;
            ++isrc1; ++isrc2; ++idest;
          }
          isrc1 += post_increment_src_y ();
          isrc2 += post_increment_src_y ();
          idest += post_increment_dest_y();
        }
        isrc1 += post_increment_src_z ();
        isrc2 += post_increment_src_z ();
        idest += post_increment_dest_z();
      }
//       std::cout << std::endl;
    }

  private:

    inline int calculate_stride() const;
    inline size_t post_increment_src_y() const;
    inline size_t post_increment_dest_y() const;
    inline size_t post_increment_src_z() const;
    inline size_t post_increment_dest_z() const;
    inline void shift_dest_index( IndexTriplet& ijk ) const;
    inline int upper_bound( const Direction dir ) const;

  };

  template<> inline int LinearInterpolant<VolXField,XSurfXField>::upper_bound( const Direction dir ) const
  {
    int n = npts<VolXField>( dir, bundle_ );
    if( dir==XDIR & n>1 ) --n;
    return n;
  }
  template<> inline int LinearInterpolant<VolYField,YSurfYField>::upper_bound( const Direction dir ) const
  {
    int n = npts<VolYField>( dir, bundle_ );
    if( dir==YDIR & n>1 ) --n;
    return n;
  }
  template<> inline int LinearInterpolant<VolZField,ZSurfZField>::upper_bound( const Direction dir ) const
  {
    int n = npts<VolZField>( dir, bundle_ );
    if( dir==ZDIR & n>1 ) --n;
    return n;
  }

  template<> inline int LinearInterpolant<VolXField,XSurfXField>::calculate_stride() const { return 1;         }
  template<> inline int LinearInterpolant<VolYField,YSurfYField>::calculate_stride() const { return nxs_;      }
  template<> inline int LinearInterpolant<VolZField,ZSurfZField>::calculate_stride() const { return nxs_*nys_; }

  template<> inline size_t LinearInterpolant<VolXField,XSurfXField>::post_increment_src_y () const { return 1; }
  template<> inline size_t LinearInterpolant<VolXField,XSurfXField>::post_increment_dest_y() const { return 2; }

  template<> inline size_t LinearInterpolant<VolYField,YSurfYField>::post_increment_src_y () const { return 0; }
  template<> inline size_t LinearInterpolant<VolYField,YSurfYField>::post_increment_dest_y() const { return 0; }

  template<> inline size_t LinearInterpolant<VolZField,ZSurfZField>::post_increment_src_y () const { return 0; }
  template<> inline size_t LinearInterpolant<VolZField,ZSurfZField>::post_increment_dest_y() const { return 0; }

  template<> inline size_t LinearInterpolant<VolXField,XSurfXField>::post_increment_src_z () const { return 0; }
  template<> inline size_t LinearInterpolant<VolXField,XSurfXField>::post_increment_dest_z() const { return 0; }

  template<> inline size_t LinearInterpolant<VolYField,YSurfYField>::post_increment_src_z () const { return nxs_; }
  template<> inline size_t LinearInterpolant<VolYField,YSurfYField>::post_increment_dest_z() const { return 2*nxd_; }

  template<> inline size_t LinearInterpolant<VolZField,ZSurfZField>::post_increment_src_z () const { return 0; }
  template<> inline size_t LinearInterpolant<VolZField,ZSurfZField>::post_increment_dest_z() const { return 0; }

  template<> inline void LinearInterpolant<VolXField,XSurfXField>::shift_dest_index( IndexTriplet& ijk ) const
  {
    if( bundle_.npts(XDIR) >1 ) ijk.i += VolXField::Ghost::NGHOST - XSurfXField::Ghost::NGHOST +1;
    if( bundle_.npts(YDIR) >1 ) ijk.j += VolXField::Ghost::NGHOST - XSurfXField::Ghost::NGHOST;
    if( bundle_.npts(ZDIR) >1 ) ijk.k += VolXField::Ghost::NGHOST - XSurfXField::Ghost::NGHOST;
  }

  template<> inline void LinearInterpolant<VolYField,YSurfYField>::shift_dest_index( IndexTriplet& ijk ) const
  {
    if( bundle_.npts(XDIR) >1 ) ijk.i += VolYField::Ghost::NGHOST - YSurfYField::Ghost::NGHOST;
    if( bundle_.npts(YDIR) >1 ) ijk.j += VolYField::Ghost::NGHOST - YSurfYField::Ghost::NGHOST +1;
    if( bundle_.npts(ZDIR) >1 ) ijk.k += VolYField::Ghost::NGHOST - YSurfYField::Ghost::NGHOST;
  }

  template<> inline void LinearInterpolant<VolZField,ZSurfZField>::shift_dest_index( IndexTriplet& ijk ) const
  {
    if( bundle_.npts(XDIR) >1 ) ijk.i += VolZField::Ghost::NGHOST - ZSurfZField::Ghost::NGHOST;
    if( bundle_.npts(YDIR) >1 ) ijk.j += VolZField::Ghost::NGHOST - ZSurfZField::Ghost::NGHOST;
    if( bundle_.npts(ZDIR) >1 ) ijk.k += VolZField::Ghost::NGHOST - ZSurfZField::Ghost::NGHOST +1;
  }

} // namespace LBMS


#endif // LBMSInterpolant_h
