/**
 *  \file   InterpCoarseToFine.h
 *
 *  \date   Jun 16, 2012
 *  \author James C. Sutherland
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMS_InterpCoarseToFine_h
#define LBMS_InterpCoarseToFine_h

#include <lbms/Direction.h>
#include <fields/Fields.h>

namespace LBMS {

  /**
   *  \class  InterpCoarseToFine
   *  \author James C. Sutherland
   *  \date   September, 2012
   *
   *  \brief Interpolate from coarse to fine mesh (direct injection)
   *  \tparam DestT the destination (fine mesh) field type
   *
   *  Note that this operator should only be used on intensive quantities.
   */
  template< typename DestFieldT >
  class InterpCoarseToFine
  {
    const Direction dir_;
    const unsigned nxc_, nyc_, nzc_;
    const unsigned ratio_;

  public:
    typedef typename CoarseFieldSelector<DestFieldT>::type  SrcT;
    typedef DestFieldT DestT;

    // these typedefs provide compatibility with the SpatialOps interface
    typedef SpatialOps::Interpolant   type;
    typedef SrcT                      SrcFieldType;
    typedef DestT                     DestFieldType;

    InterpCoarseToFine( const Bundle& );
    ~InterpCoarseToFine();
    void apply_to_field( const SrcT&, DestT& ) const;
  };

} /* namespace LBMS */
#endif /* LBMS_InterpCoarseToFine_h */
