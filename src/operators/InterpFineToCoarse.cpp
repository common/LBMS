/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "InterpFineToCoarse.h"
#include <lbms/Bundle.h>

#ifdef NDEBUG
# define BOOST_DISABLE_ASSERTS
#endif
#include <boost/multi_array.hpp>

namespace LBMS{

  //-----------------------------------------------------------------

  template< typename SrcT >
  InterpFineToCoarse<SrcT>::
  InterpFineToCoarse( const Bundle& bundle )
  : nskip_( bundle.npts( bundle.dir() ) / bundle.ncoarse( bundle.dir() ) )
  {
    assert( direction<typename SrcT::Location::Bundle>() == bundle.dir() );
  }

  //-----------------------------------------------------------------

  template< typename SrcT >
  InterpFineToCoarse<SrcT>::~InterpFineToCoarse(){}

  //-----------------------------------------------------------------

  template<typename Loc> bool is_vol_cell(){ return false; }
  template<> bool is_vol_cell<SpatialOps::NODIR>(){ return true; }

  template< typename SrcT >
  void
  InterpFineToCoarse<SrcT>::
  apply_to_field( const SrcT& _src, DestT& _dest ) const
  {
    using SpatialOps::IntVec;

    typedef boost::multi_array_ref<typename SrcT::value_type,3>         Array;
    typedef boost::const_multi_array_ref<typename SrcT::value_type,3>   ConstArray;
    typedef typename Array::index                                       Index;
    typedef boost::multi_array_types::index_range                       Range;
    typedef typename      Array::template       array_view<3>::type     View;
    typedef typename ConstArray::template const_array_view<3>::type     ConstView;

    const Direction dir = direction<typename SrcT::Location::Bundle>();

    assert( _src.get_ghost_data().get_minus(XDIR) == _dest.get_ghost_data().get_minus(XDIR) );
    assert( _src.get_ghost_data().get_minus(YDIR) == _dest.get_ghost_data().get_minus(YDIR) );
    assert( _src.get_ghost_data().get_minus(ZDIR) == _dest.get_ghost_data().get_minus(ZDIR) );

    const bool isVol     = int(SrcT::Location::FaceDir::value) == int(NODIR);
    const bool isAligned = int(SrcT::Location::FaceDir::value) == int(dir);  // face fields on bundle direction

    const SpatialOps::MemoryWindow& wSrcNG  =  _src.window_without_ghost();
    const SpatialOps::MemoryWindow& wDestNG = _dest.window_without_ghost();
    const SpatialOps::MemoryWindow& wSrcG   =  _src.window_with_ghost();
    const SpatialOps::MemoryWindow& wDestG  = _dest.window_with_ghost();

    const boost::array<Index,3>  srcShape = {{ long( wSrcG.glob_dim(0)), long( wSrcG.glob_dim(1)), long( wSrcG.glob_dim(2)) }};
    const boost::array<Index,3> destShape = {{ long(wDestG.glob_dim(0)), long(wDestG.glob_dim(1)), long(wDestG.glob_dim(2)) }};

    ConstArray src(  _src.field_values(), srcShape , boost::fortran_storage_order() );
    Array     dest( _dest.field_values(), destShape, boost::fortran_storage_order() );

    IntVec sOffset = wSrcG.offset();  sOffset[dir] = wSrcNG.offset(dir) + (isAligned ? 0 : nskip_/2-1);
    IntVec sExtent = wSrcG.extent();  sExtent[dir] = wSrcNG.extent(dir) - (isAligned ? 0 : nskip_/2  );
    if( !isAligned && nskip_%2 !=0 ) ++sOffset[dir];

    IntVec dOffset = wDestG.offset();  dOffset[dir]=wDestNG.offset(dir);
    IntVec dExtent = wDestG.extent();  dExtent[dir]=wDestNG.extent(dir);

    //--- (+) side ghost cell in bundle-oriented direction (these require special treatment)
    if( isAligned ){
      const unsigned ng = _src.get_ghost_data().get_plus(dir);
      const unsigned slo = ng +  wSrcG.offset(dir) +  wSrcNG.extent(dir);
      const unsigned dlo = ng + wDestG.offset(dir) + wDestNG.extent(dir);

      // p => "plus side"
      const Range xsp( dir==XDIR ? slo : sOffset[0],  dir==XDIR ? slo+ng : sOffset[0]+sExtent[0] );
      const Range xdp( dir==XDIR ? dlo : dOffset[0],  dir==XDIR ? dlo+ng : dOffset[0]+dExtent[0] );

      const Range ysp( dir==YDIR ? slo : sOffset[1],  dir==YDIR ? slo+ng : sOffset[1]+sExtent[1] );
      const Range ydp( dir==YDIR ? dlo : dOffset[1],  dir==YDIR ? dlo+ng : dOffset[1]+dExtent[1] );

      const Range zsp( dir==ZDIR ? slo : sOffset[2],  dir==ZDIR ? slo+ng : sOffset[2]+sExtent[2] );
      const Range zdp( dir==ZDIR ? dlo : dOffset[2],  dir==ZDIR ? dlo+ng : dOffset[2]+dExtent[2] );

      ConstView sviewP =  src[ boost::indices[xsp][ysp][zsp] ];
      View      dviewP = dest[ boost::indices[xdp][ydp][zdp] ];

      typename ConstView::const_iterator is=sviewP.begin(), ise=sviewP.end();
      typename View::iterator id=dviewP.begin();
      for(; is!=ise; ++is, ++id ){
        *id=*is;
      }
    }

    const IntVec loIx(0,0,0);
    const IntVec hiIx = dExtent;
    IntVec sStride(1,1,1); sStride[dir] = nskip_;

    View destView = dest[ boost::indices[ Range( dOffset[0], dOffset[0]+dExtent[0] ) ]
                                        [ Range( dOffset[1], dOffset[1]+dExtent[1] ) ]
                                        [ Range( dOffset[2], dOffset[2]+dExtent[2] ) ] ];

    if( isVol ){
      sOffset[dir] = wSrcNG.offset(dir);
      sExtent[dir] = wSrcNG.extent(dir);

      //Stride by 1 to  hit every cell in a volume exchange
      ConstView srcView = src[ boost::indices[ Range( sOffset[0], sOffset[0]+sExtent[0], 1 ) ]
                                             [ Range( sOffset[1], sOffset[1]+sExtent[1], 1 ) ]
                                             [ Range( sOffset[2], sOffset[2]+sExtent[2], 1 ) ] ];
      // interpolation required
      ++sOffset[dir];
      ++sExtent[dir];

      for( Index k=loIx[2]; k<hiIx[2]; ++k ){
        for( Index j=loIx[1]; j<hiIx[1]; ++j ){
          for( Index i=loIx[0]; i<hiIx[0]; ++i ){
            double temp = 0.0;
            IntVec fineIndex(0,0,0), coarseIndex(0,0,0);
            for( size_t l=0; l !=nskip_; ++l ){
              coarseIndex = IntVec(i,j,k);
              fineIndex[0]=i; fineIndex[1]=j; fineIndex[2]=k;
              fineIndex[dir] = l + ( coarseIndex[dir] * nskip_ );
              temp = temp + srcView[ fineIndex[0] ][ fineIndex[1] ][ fineIndex[2] ];
            }
            destView[i][j][k] = temp / nskip_;
          }
        }
      }
    }
    else if( !isAligned && nskip_%2 == 0 ){
      ConstView srcView = src[ boost::indices[ Range( sOffset[0], sOffset[0]+sExtent[0], sStride[0] ) ]
                                             [ Range( sOffset[1], sOffset[1]+sExtent[1], sStride[1] ) ]
                                             [ Range( sOffset[2], sOffset[2]+sExtent[2], sStride[2] ) ] ];
      // interpolation required
      ++sOffset[dir];
      ++sExtent[dir];
      ConstView srcView2 = src[ boost::indices[ Range( sOffset[0], sOffset[0]+sExtent[0], sStride[0] ) ]
                                              [ Range( sOffset[1], sOffset[1]+sExtent[1], sStride[1] ) ]
                                              [ Range( sOffset[2], sOffset[2]+sExtent[2], sStride[2] ) ] ];
      for( Index k=loIx[2]; k<hiIx[2]; ++k ){
        for( Index j=loIx[1]; j<hiIx[1]; ++j ){
          for( Index i=loIx[0]; i<hiIx[0]; ++i ){
            destView[i][j][k] = 0.5*(srcView[i][j][k] + srcView2[i][j][k]);
          }
        }
      }
    }
    else{
      ConstView srcView = src[ boost::indices[ Range( sOffset[0], sOffset[0]+sExtent[0], sStride[0] ) ]
                                             [ Range( sOffset[1], sOffset[1]+sExtent[1], sStride[1] ) ]
                                             [ Range( sOffset[2], sOffset[2]+sExtent[2], sStride[2] ) ] ];
      // direct injection
      for( Index k=loIx[2]; k<hiIx[2]; ++k ){
        for( Index j=loIx[1]; j<hiIx[1]; ++j ){
          for( Index i=loIx[0]; i<hiIx[0]; ++i ){
            destView[i][j][k] = srcView[i][j][k];
          }
        }
      }
    }
  }

  //=================================================================
  // Explicit template instantiation
  template class InterpFineToCoarse<  XVolField>;
  template class InterpFineToCoarse<XSurfXField>;
  template class InterpFineToCoarse<XSurfYField>;
  template class InterpFineToCoarse<XSurfZField>;

  template class InterpFineToCoarse<  YVolField>;
  template class InterpFineToCoarse<YSurfXField>;
  template class InterpFineToCoarse<YSurfYField>;
  template class InterpFineToCoarse<YSurfZField>;

  template class InterpFineToCoarse<  ZVolField>;
  template class InterpFineToCoarse<ZSurfXField>;
  template class InterpFineToCoarse<ZSurfYField>;
  template class InterpFineToCoarse<ZSurfZField>;
  //=================================================================

} // namespace LBMS
