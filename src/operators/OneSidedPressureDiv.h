#ifndef OneSidedPressureDiv_h
#define OneSidedPressureDiv_h

#include <expression/Expression.h>

namespace LBMS{



  //====================================================================

  template< typename FieldT, typename DivT >
  class OneSidedPressure
    : public Expr::Expression<FieldT>
  {

    DECLARE_FIELDS( FieldT, pressure_ )

    OneSidedPressure( const SpatialOps::SpatialMask<FieldT>& mask,
                      const Expr::Tag& pressTag );

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag pressT_;
      const SpatialOps::SpatialMask<FieldT> mask_;
    public:
      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const Expr::Tag& pressureTag )
      : ExpressionBuilder(result),
        mask_(mask),
        pressT_( pressureTag ) {}
      Expr::ExpressionBase* build() const
      { return new OneSidedPressure<FieldT,DivT>(mask_,pressT_); }
    };

    void evaluate();
    void bind_operators( const SpatialOps::OperatorDatabase& opDB );

  private:
    const DivT* divOp_;
    const SpatialOps::SpatialMask<FieldT> mask_;
  };


  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################


  template< typename FieldT, typename DivT >
  OneSidedPressure<FieldT,DivT>::
  OneSidedPressure( const SpatialOps::SpatialMask<FieldT>& mask,
                    const Expr::Tag& pressTag )
    : Expr::Expression<FieldT>(),
      mask_(mask)
  {
    pressure_ = this->template create_field_request<FieldT>( pressTag );
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename DivT >
  void
  OneSidedPressure<FieldT,DivT>::
  bind_operators( const SpatialOps::OperatorDatabase& opDB )
  {
    divOp_ = opDB.retrieve_operator<DivT>();
  }

  //--------------------------------------------------------------------

  template< typename FieldT, typename DivT >
  void
  OneSidedPressure<FieldT,DivT>::
  evaluate()
  {
    FieldT& result = this->value();
    const FieldT& pressure = pressure_->field_ref();

    //Negative because we subtract out the gradient but add this as a "source term"
    masked_assign( mask_, result, -1.0 * (*divOp_)(pressure) );
  }

  //--------------------------------------------------------------------

}//end namespace

#endif // OneSidedPressureDiv_h
