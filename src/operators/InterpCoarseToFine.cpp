/**
 *  \file   InterpCoarseToFine.cpp
 *
 *  \date   Jun 16, 2012
 *  \author James C. Sutherland
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "InterpCoarseToFine.h"
#include <lbms/Bundle.h>

#ifdef NDEBUG
# define BOOST_DISABLE_ASSERTS
#endif
#include <boost/multi_array.hpp>

namespace LBMS {

  template< typename DestT >
  InterpCoarseToFine<DestT>::
  InterpCoarseToFine( const Bundle& bundle )
  : dir_( bundle.dir() ),
    nxc_( dir_ == XDIR ? bundle.ncoarse(dir_) : bundle.npts(XDIR) ),
    nyc_( dir_ == YDIR ? bundle.ncoarse(dir_) : bundle.npts(YDIR) ),
    nzc_( dir_ == ZDIR ? bundle.ncoarse(dir_) : bundle.npts(ZDIR) ),
    ratio_( bundle.npts(dir_) / bundle.ncoarse(dir_) )
  {
    assert( bundle.npts(dir_) % bundle.ncoarse(dir_) == 0 );
    assert( direction<typename DestT::Location::Bundle>() == dir_ );
  }

  template< typename DestT >
  InterpCoarseToFine<DestT>::~InterpCoarseToFine()
  {}

  template<typename DestT>
  void
  InterpCoarseToFine<DestT>::
  apply_to_field( const SrcT& _src, DestT& _dest ) const
  {
    using SpatialOps::IntVec;
    using SpatialOps::MemoryWindow;
    using SpatialOps::GhostData;

    typedef boost::multi_array_ref<typename SrcT::value_type,3>         Array;
    typedef boost::const_multi_array_ref<typename SrcT::value_type,3>   ConstArray;
    typedef typename Array::index                                       Index;
    typedef boost::multi_array_types::index_range                       Range;
    typedef typename      Array::template       array_view<3>::type     View;
    typedef typename ConstArray::template const_array_view<3>::type     ConstView;

    const Direction dir = direction<typename DestT::Location::Bundle>();

    const MemoryWindow& wSrcG  =  _src.window_with_ghost();
    const MemoryWindow& wDestG = _dest.window_with_ghost();

    const boost::array<Index,3>  srcShape = {{ long( wSrcG.glob_dim(0)), long( wSrcG.glob_dim(1)), long( wSrcG.glob_dim(2)) }};
    const boost::array<Index,3> destShape = {{ long(wDestG.glob_dim(0)), long(wDestG.glob_dim(1)), long(wDestG.glob_dim(2)) }};

    ConstArray src(  _src.field_values(), srcShape , boost::fortran_storage_order() );
    Array     dest( _dest.field_values(), destShape, boost::fortran_storage_order() );

    const IntVec& sOffset = wSrcG.offset();
    const IntVec& sExtent = wSrcG.extent();

    const IntVec& dOffset = wDestG.offset();
    const IntVec& dExtent = wDestG.extent();

    const IntVec sLoIx(0,0,0), dLoIx(0,0,0), dHiIx(dExtent);

    View destView = dest[ boost::indices[ Range( dOffset[0], dOffset[0]+dExtent[0] ) ]
                                        [ Range( dOffset[1], dOffset[1]+dExtent[1] ) ]
                                        [ Range( dOffset[2], dOffset[2]+dExtent[2] ) ] ];
    ConstView srcView = src[ boost::indices[ Range( sOffset[0], sOffset[0]+sExtent[0] ) ]
                                           [ Range( sOffset[1], sOffset[1]+sExtent[1] ) ]
                                           [ Range( sOffset[2], sOffset[2]+sExtent[2] ) ] ];

    switch( dir ){

      case XDIR:{
        assert( dExtent[1] == sExtent[1] );
        assert( dExtent[2] == sExtent[2] );
        if( sExtent[0] == 1 ){
          for( Index k=dLoIx[2]; k<dHiIx[2]; ++k ){
            for( Index j=dLoIx[1]; j<dHiIx[1]; ++j ){
              destView[0][j][k] = srcView[0][j][k];
            }
          }
          return;
        }

        assert( sExtent[0] > 1 );
        const size_t ngdm = _dest.get_ghost_data().get_minus(0);
        // j and k indices apply to both source and dest fields whereas the i index is different
        for(     Index k =dLoIx[2]; k <dHiIx[2]; ++k ){
          for(   Index j =dLoIx[1]; j <dHiIx[1]; ++j ){
            Index is = sLoIx[0];
            for( Index id=dLoIx[0]; id<dHiIx[0]; ++id ){
              destView[id][j][k] = srcView[is][j][k];
              if( (id-ngdm+1)%ratio_ == 0 ) ++is;
            }
          }
        }
        break;
      }

      case YDIR:{
        assert( dExtent[0] == sExtent[0] );
        assert( dExtent[2] == sExtent[2] );
        if( sExtent[1] == 1 ){
          for( Index k=dLoIx[2]; k<dHiIx[2]; ++k ){
            for( Index i=dLoIx[0]; i<dHiIx[0]; ++i ){
              destView[i][0][k] = srcView[i][0][k];
            }
          }
          return;
        }

        assert( sExtent[1] > 1 );
        // i and k indices apply to both source and dest fields whereas the j index is different
        const size_t ngdm = _dest.get_ghost_data().get_minus(1);
        for( Index k=dLoIx[2]; k<dHiIx[2]; ++k  ){
          Index js=sLoIx[1];
          for( Index jd=dLoIx[1]; jd<dHiIx[1]; ++jd ){
            for( Index i=dLoIx[0]; i<dHiIx[0]; ++i ){
              destView[i][jd][k] = srcView[i][js][k];
            }
            if( (jd-ngdm+1)%ratio_ == 0 ) ++js;
          }
        }
        break;
      }

      case ZDIR:{
        assert( dExtent[0] == sExtent[0] );
        assert( dExtent[1] == sExtent[1] );
        if( sExtent[2] == 1 ){
          for( Index j=dLoIx[1]; j<dHiIx[1]; ++j ){
            for( Index i=dLoIx[0]; i<dHiIx[0]; ++i ){
              destView[i][j][0] = srcView[i][j][0];
            }
          }
          return;
        }

        assert( sExtent[2] > 1 );
        // i and j indices apply to both source and dest fields whereas the k index is different
        const size_t ngdm = _dest.get_ghost_data().get_minus(2);
        Index ks=sLoIx[2];
        for(     Index kd=dLoIx[2]; kd<dHiIx[2]; ++kd ){
          for(   Index j =dLoIx[1]; j <dHiIx[1]; ++j ){
            for( Index i =dLoIx[0]; i <dHiIx[0]; ++i ){
              destView[i][j][kd] = srcView[i][j][ks];
            }
          }
          if( (kd-ngdm+1)%ratio_ == 0 ) ++ks;
        }
        break;
      }

      case NODIR:
        assert(false);
        break;
    }

    return;
  }

  //=================================================================
  // Explicit template instantiation
  template class InterpCoarseToFine<  XVolField>;
  template class InterpCoarseToFine<XSurfXField>;
  template class InterpCoarseToFine<YSurfXField>;
  template class InterpCoarseToFine<ZSurfXField>;

  template class InterpCoarseToFine<  YVolField>;
  template class InterpCoarseToFine<XSurfYField>;
  template class InterpCoarseToFine<YSurfYField>;
  template class InterpCoarseToFine<ZSurfYField>;

  template class InterpCoarseToFine<  ZVolField>;
  template class InterpCoarseToFine<XSurfZField>;
  template class InterpCoarseToFine<YSurfZField>;
  template class InterpCoarseToFine<ZSurfZField>;
  //=================================================================

} /* namespace LBMS */
