/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ScalarRHS.h"

//-- SpatialOps Includes --//
#include <spatialops/OperatorDatabase.h>

template< typename FieldT >
Expr::Tag
ScalarRHS<FieldT>::resolve_field_tag( const FieldSelector field,
                                      const FieldTagInfo& info )
{
  Expr::Tag tag;
  const FieldTagInfo::const_iterator ifld = info.find( field );
  if( ifld != info.end() ) tag = ifld->second;
  return tag;
}

void require( const Expr::Tag& tag, const std::string& str, const FieldTagInfo& fti )
{
  if( tag == Expr::Tag() ){
    std::ostringstream msg;
    msg << "ScalarRHS requires a specification for " << str << std::endl
        << "Specified information follows: \n"
        << fti << std::endl;
    throw std::runtime_error( msg.str() );
  }
}

//------------------------------------------------------------------

template< typename FieldT >
ScalarRHS<FieldT>::ScalarRHS( const FieldTagInfo& fieldTags,
                              Expr::TagList srcTags )
  : Expr::Expression<FieldT>(),

    convTagX_( ScalarRHS<FieldT>::resolve_field_tag( CONVECTIVE_FLUX_X, fieldTags ) ),
    convTagY_( ScalarRHS<FieldT>::resolve_field_tag( CONVECTIVE_FLUX_Y, fieldTags ) ),
    convTagZ_( ScalarRHS<FieldT>::resolve_field_tag( CONVECTIVE_FLUX_Z, fieldTags ) ),

    diffTagX_( ScalarRHS<FieldT>::resolve_field_tag( DIFFUSIVE_FLUX_X, fieldTags ) ),
    diffTagY_( ScalarRHS<FieldT>::resolve_field_tag( DIFFUSIVE_FLUX_Y, fieldTags ) ),
    diffTagZ_( ScalarRHS<FieldT>::resolve_field_tag( DIFFUSIVE_FLUX_Z, fieldTags ) ),

    haveConvection_( convTagX_ != Expr::Tag() || convTagY_ != Expr::Tag() || convTagZ_ != Expr::Tag() ),
    haveDiffusion_ ( diffTagX_ != Expr::Tag() || diffTagY_ != Expr::Tag() || diffTagZ_ != Expr::Tag() ),

    doXDir_( convTagX_ != Expr::Tag() || diffTagX_ != Expr::Tag() ),
    doYDir_( convTagY_ != Expr::Tag() || diffTagY_ != Expr::Tag() ),
    doZDir_( convTagZ_ != Expr::Tag() || diffTagZ_ != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  const Expr::Tag singleSrcTag = ScalarRHS<FieldT>::resolve_field_tag( SOURCE_TERM, fieldTags );
  if( singleSrcTag != Expr::Tag() ) srcTags.push_back( singleSrcTag );

  if( haveConvection_ ){
    if( doXDir_ )  require( convTagX_, "X-convective flux", fieldTags );
    if( doYDir_ )  require( convTagY_, "Y-convective flux", fieldTags );
    if( doZDir_ )  require( convTagZ_, "Z-convective flux", fieldTags );
    if( doXDir_ )  xConvFlux_ = this->template create_field_request<XFluxT>( convTagX_ );
    if( doYDir_ )  yConvFlux_ = this->template create_field_request<YFluxT>( convTagY_ );
    if( doZDir_ )  zConvFlux_ = this->template create_field_request<ZFluxT>( convTagZ_ );
  }

  if( haveDiffusion_ ){
    if( doXDir_ ) require( diffTagX_, "X-diffusive flux", fieldTags );
    if( doYDir_ ) require( diffTagY_, "Y-diffusive flux", fieldTags );
    if( doZDir_ ) require( diffTagZ_, "Z-diffusive flux", fieldTags );
    if( doXDir_ )  xDiffFlux_ = this->template create_field_request<XFluxT>( diffTagX_ );
    if( doYDir_ )  yDiffFlux_ = this->template create_field_request<YFluxT>( diffTagY_ );
    if( doZDir_ )  zDiffFlux_ = this->template create_field_request<ZFluxT>( diffTagZ_ );
  }

  if( !srcTags.empty() ) this->template create_field_vector_request<FieldT>( srcTags, srcTerm_ );
}

//------------------------------------------------------------------

template< typename FieldT >
void
ScalarRHS<FieldT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  if( doXDir_ )  divOpX_ = opDB.retrieve_operator<DivX>();
  if( doYDir_ )  divOpY_ = opDB.retrieve_operator<DivY>();
  if( doZDir_ )  divOpZ_ = opDB.retrieve_operator<DivZ>();
}

//------------------------------------------------------------------

template< typename FieldT >
void ScalarRHS<FieldT>::evaluate()
{
  using namespace SpatialOps;

  FieldT& rhs = this->value();

  // Optimize for the 3D case.  Otherwise, accumulate terms in.
  if( doXDir_ && doYDir_ && doZDir_ && haveConvection_ && haveDiffusion_ ){
    const XFluxT& xConvFlux = xConvFlux_->field_ref();
    const YFluxT& yConvFlux = yConvFlux_->field_ref();
    const ZFluxT& zConvFlux = zConvFlux_->field_ref();
    const XFluxT& xDiffFlux = xDiffFlux_->field_ref();
    const YFluxT& yDiffFlux = yDiffFlux_->field_ref();
    const ZFluxT& zDiffFlux = zDiffFlux_->field_ref();
    rhs <<= -(*divOpX_)( xConvFlux + xDiffFlux )
            -(*divOpY_)( yConvFlux + yDiffFlux )
            -(*divOpZ_)( zConvFlux + zDiffFlux );
  }
  else{
    // accumulate terms in - not quite as optimized...
    rhs <<= 0.0;

    if( doXDir_ ){
      if( haveConvection_ ) rhs <<= rhs - (*divOpX_)( xConvFlux_->field_ref() );
      if( haveDiffusion_  ) rhs <<= rhs - (*divOpX_)( xDiffFlux_->field_ref() );
    }
    if( doYDir_ ){
      if( haveConvection_ ) rhs <<= rhs - (*divOpY_)( yConvFlux_->field_ref() );
      if( haveDiffusion_  ) rhs <<= rhs - (*divOpY_)( yDiffFlux_->field_ref() );
    }
    if( doZDir_ ){
      if( haveConvection_ ) rhs <<= rhs - (*divOpZ_)( zConvFlux_->field_ref() );
      if( haveDiffusion_  ) rhs <<= rhs - (*divOpZ_)( zDiffFlux_->field_ref() );
    }
  }

  for( size_t i=0; i<srcTerm_.size(); ++i ){
    rhs <<= rhs + srcTerm_[i]->field_ref();
  }
}

//------------------------------------------------------------------

template< typename FieldT >
ScalarRHS<FieldT>::Builder::Builder( const Expr::Tag& result,
                                     const FieldTagInfo& fieldInfo,
                                     const std::vector<Expr::Tag>& sources )
  : ExpressionBuilder(result),
    info_( fieldInfo ),
    srcT_( sources   )
{}

//------------------------------------------------------------------

template< typename FieldT >
ScalarRHS<FieldT>::Builder::Builder( const Expr::Tag& result,
                                     const FieldTagInfo& fieldInfo )
  : ExpressionBuilder(result),
    info_( fieldInfo )
{}

//------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
ScalarRHS<FieldT>::Builder::build() const
{
  return new ScalarRHS<FieldT>( info_, srcT_ );
}
//------------------------------------------------------------------


//==========================================================================
// Explicit template instantiation for supported versions of this expression
#include <fields/Fields.h>
template class ScalarRHS< LBMS::XVolField >;
template class ScalarRHS< LBMS::YVolField >;
template class ScalarRHS< LBMS::ZVolField >;
//==========================================================================
