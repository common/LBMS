#ifndef FluxInterpolation_Expr_h
#define FluxInterpolation_Expr_h

#include <expression/Expression.h>
#include <operators/Operators.h>
#include <fields/Fields.h>

/**
 *  @class FluxInterpolation
 *  @author Derek A. Cline
 *  @brief interpolation from a flux field to a vol field or vol to flux
 */
template< typename DestFieldT,
          typename SrcFieldT >
class FluxInterpolation
 : public Expr::Expression<DestFieldT>
{
  DECLARE_FIELD( SrcFieldT, srcField_ )

  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, SrcFieldT, DestFieldT >::type  interpT;

  const interpT* interpOp_;

  FluxInterpolation( const Expr::Tag& srcFieldT );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag srcFieldT_;
    const SrcFieldT *srcField_;
  public:
    /**
     *  @brief Build a FluxInterpolation expression
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& srcFieldT )
      : ExpressionBuilder( resultTag ),
        srcFieldT_( srcFieldT )
    {}

    Expr::ExpressionBase* build() const{
      return new FluxInterpolation<DestFieldT,SrcFieldT>( srcFieldT_ );
    }
  };

  ~FluxInterpolation(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename DestFieldT, typename SrcFieldT >
FluxInterpolation<DestFieldT,SrcFieldT>::
FluxInterpolation( const Expr::Tag& srcFieldT )
: Expr::Expression<DestFieldT>()
{
  this->set_gpu_runnable(true);
  srcField_ = this->template create_field_request< SrcFieldT >( srcFieldT );
}

//--------------------------------------------------------------------

template< typename DestFieldT, typename SrcFieldT >
void
FluxInterpolation<DestFieldT,SrcFieldT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<interpT>();
}

//--------------------------------------------------------------------

template< typename DestFieldT, typename SrcFieldT >
void
FluxInterpolation<DestFieldT,SrcFieldT>::
evaluate()
{
  DestFieldT& result = this->value();
  result <<= (*interpOp_)( srcField_->field_ref() );
}

//--------------------------------------------------------------------


#endif // FluxInterpolation_Expr_h
