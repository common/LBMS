#ifndef StressNew_Expr_h
#define StressNew_Expr_h

#include <expression/Expression.h>

#include <fields/Fields.h>
#include <operators/Operators.h>

/**
 *  @class Stress
 *
 *  @brief  Calculates off-diagonal components of the stress tensor for use in AME.
 *
 *  The Stress expression calculates a single component of the stress tensor,
 *  \f[
 *    \tau_{ji} = -\mu\left(
 *               \frac{\partial u_{i}}{\partial x_{j}^{j}}
 *              +\frac{\partial u_{j}}{\partial x_{i}^{i}}\right)
 *              +\delta_{ij}\frac{2}{3}\mu
 *               \frac{\partial u_{k}}{\partial x_{k}^{k}},
 *  \f]
 *  where the first index indicates the CV face for the stress
 *  component and the second index indicates the direction of the
 *  stress. Considering only the off-diagonal components we have
 *  \f[
 *    \tau_{ji}=-\mu\left(\frac{\partial u_{i}}{\partial
 *    x_{j}^{j}}+\frac{\partial u_{j}}{\partial x_{i}^{i}}\right).
 *  \f]
 *
 *  There is a seperate class for calculation of the \link NormalStress \endlink.
 *
 *  @par Template Parameters
 *  This Expression is templated on the following types:
 *  <ul>
 *  <li> \b TauT  The type of field for this stress component.
 *  <li> \b velGradT The type of field associated with the off-direction velocity gradient.
 *  </ul>
 *  From these template parameters, the following operator types are deduced:
 *  <ul>
 *  <li> \b GradT The gradient operator to obtain the first velocity
 *       gradient, \f$\frac{\partial u_i}{\partial x_j}\f$ from \f$u\f$.
 *       on bundle \f$j\f$.
 *  <li> \b Grad2T The gradient operator to obtain the second velocity
 *       gradient, \f$\frac{\partial u_i}{\partial x_j}\f$ from \f$u\f$.
 *       on bundle \f$j\f$.
 *  <li> \b VelInterpT The interpolant operator to move the velocity
 *       gradient \f$\frac{\partial u_i}{\partial x_j}\f$ from bundle
 *       \f$i\f$ to bundle \f$j\f$.
 *  <li> \b ViscInterpT The interpolant operator in the \f$i\f$ direction
 *       to interpolate viscosity from the cell centers to the
 *       appropriate face.
 *  </ul>
 *
 *
 * \par Example 1
 *  Form \f$\tau_{xy} = -\mu \left(
 *  \frac{\partial u}{\partial y} + \frac{\partial v}{\partial x}
 *  \right) \f$ on an \f$x\f$ bundle for use in the y-momentum equation.
 *  We use \f$\frac{\partial v}{\partial x}\f$ from the \f$x\f$ bundle
 *  and \f$\frac{\partial u}{\partial y}\f$ from the \f$y\f$ bundle.  We
 *  would then need to interpolate \f$\frac{\partial u}{\partial y}\f$
 *  to the \f$x\f$ bundle.
 *  The following code snippet would form a builder for
 *  \f$\tau_{xy} = -\mu \left( \frac{\partial u}{\partial y} +
 *  \frac{\partial v}{\partial x} \right) \f$ on an \f$x\f$ bundle:
 *  \code{.cpp}
 *    typedef Stress<TauXY,YGradY>::Builder TauXYBuilder;
 *    TauXYBuilder tauxy( Expr::Tag( "v_xbundle", Expr::STATE_NONE ),
 *                        Expr::Tag( "u_ybundle", Expr::STATE_NONE ),
 *                        Expr::Tag( "viscosity_xbundle",  Expr::STATE_NONE ) );
 *  \endcode
 *  where
 * - \c TauXY is the type for the stress component \f$\tau_{xy}\f$ on the x-bundle
 * - \c YGradY is the location where \f$\frac{\partial u}{\partial y}\f$ is stored.
 *  This forms an expression to calculate \f$\tau_{xy}\f$ on an
 *  \f$x\f$ bundle.
 *
 *
 * \par Example 2
 *  Form \f$\tau_{xy} = -\mu\left( \frac{\partial u}{\partial y} +
 * \frac{\partial v}{\partial x} \right)\f$ on a \f$y\f$ bundle.
 *  \code{.cpp}
 *    typedef Stress<TauXY,XGradX>::Builder TauXYBuilder;
 *    TauXYBuilder tauxy( Expr::Tag( "u_ybundle", Expr::STATE_NONE ),
 *                        Expr::Tag( "v_xbundle", Expr::STATE_NONE ),
 *                        Expr::Tag( "viscosity_ybundle",  Expr::STATE_NONE ) );
 *  \endcode
 *  This results in an interpolant being applied to \f$\frac{\partial
 *  v}{\partial x}\f$, which is required since it will be calculated
 *  on the \f$x\f$ bundle and we need to move it to the \f$y\f$
 *  bundle.
 */
template< typename TauT >
class Stress
 : public Expr::Expression<TauT>
{
  typedef typename LBMS::NativeFieldSelector< typename TauT::Location::Bundle >::VolT      VolT;

  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, VolT, TauT >::type ViscInterpT;

  DECLARE_FIELD( VolT, visc_ )
  DECLARE_FIELDS( TauT, velGrad1_, velGrad2_ )

  const ViscInterpT* viscInterpOp_;

  Stress( const Expr::Tag& velGrad1T,
          const Expr::Tag& velGrad2T,
          const Expr::Tag& viscT );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag velGrad1T_, velGrad2T_, viscT_;
    const VolT *visc_;
    const TauT *velGrad1_, *velGrad2_;
  public:
    /**
     *  @brief Construct a builder for the Stress class.  The stress
     *  is defined as \f$\tau_{ij}=-2\mu \left( \frac{\partial
     *  u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i} \right) \f$
     *
     *  @param result the stress component of interest
     *
     *  @param velGrad1 For stress component \f$\tau_{ij}\f$, this is
     *  \f$\frac{\partial u_j}{\partial x_i}\f$.  This should already
     *  be located at the required face.
     *
     *  @param velGrad2 For stress component \f$\tau_{ij}\f$, this is
     *  \f$\frac{\partial u_i}{\partial x_j}\f$.  This will be
     *  interpolated to the appropriate face.  It should correspond to
     *  the Vel2GradT template parameter.
     *
     *  @param visc The viscosity (cell-centered) which will be
     *  interpolated to the required face.
     */
    Builder( const Expr::Tag& result,
             const Expr::Tag& velGrad1,
             const Expr::Tag& velGrad2,
             const Expr::Tag& visc )
      : ExpressionBuilder( result ),
        velGrad1T_( velGrad1 ),
        velGrad2T_( velGrad2 ),
        viscT_    ( visc )
    {}

    Expr::ExpressionBase* build() const{
      return new Stress<TauT>( velGrad1T_, velGrad2T_, viscT_ );
    }
  };

  ~Stress(){}

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename TauT >
Stress<TauT>::
Stress( const Expr::Tag& velGrad1T,
        const Expr::Tag& velGrad2T,
        const Expr::Tag& viscT )
  : Expr::Expression<TauT>()
{
  this->set_gpu_runnable(true);

  velGrad1_ = this->template create_field_request< TauT >( velGrad1T );
  velGrad2_ = this->template create_field_request< TauT >( velGrad2T );
  visc_     = this->template create_field_request< VolT >( viscT     );
}

//--------------------------------------------------------------------

template< typename TauT >
void
Stress<TauT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  viscInterpOp_ = opDB.retrieve_operator<ViscInterpT>();
}

//--------------------------------------------------------------------

template< typename TauT >
void
Stress<TauT>::
evaluate()
{
  using namespace SpatialOps;
  TauT& result = this->value();
  const TauT& velGrad1 = velGrad1_->field_ref();
  const TauT& velGrad2 = velGrad2_->field_ref();
  const VolT& visc     = visc_    ->field_ref();

  result <<= -( velGrad1 + velGrad2 ) * (*viscInterpOp_)(visc);
}

//--------------------------------------------------------------------


#endif // StressNew_Expr_h
