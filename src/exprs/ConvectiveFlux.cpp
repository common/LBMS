/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ConvectiveFlux.h"

#include <spatialops/OperatorDatabase.h>

//--------------------------------------------------------------------

template< typename FluxT >
ConvectiveFlux<FluxT>::
ConvectiveFlux( const Expr::Tag& velTag,
                const Expr::Tag& phiTag )
  : Expr::Expression<FluxT>()
{
  this->set_gpu_runnable(true);

  vel_ = this->template create_field_request<FluxT  >( velTag );
  phi_ = this->template create_field_request<ScalarT>( phiTag );
}

//--------------------------------------------------------------------

template< typename FluxT >
void
ConvectiveFlux<FluxT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename FluxT >
void
ConvectiveFlux<FluxT>::
evaluate()
{
  using namespace SpatialOps;
  const FluxT  & vel = vel_->field_ref();
  const ScalarT& phi = phi_->field_ref();
  this->value() <<= (*interpOp_)(phi) * vel;
}

//--------------------------------------------------------------------

template< typename FluxT >
ConvectiveFlux<FluxT>::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& velTag,
                  const Expr::Tag& phiTag )
  : ExpressionBuilder( resultTag ),
    velTag_( velTag ),
    phiTag_( phiTag )
{}

//--------------------------------------------------------------------

template< typename FluxT >
Expr::ExpressionBase*
ConvectiveFlux<FluxT>::
Builder::build() const
{
  return new ConvectiveFlux<FluxT>( velTag_,phiTag_ );
}

//--------------------------------------------------------------------

#define INSTANTIATE( VolT )                                         \
template class ConvectiveFlux<SpatialOps::FaceTypes<VolT>::XFace>;  \
template class ConvectiveFlux<SpatialOps::FaceTypes<VolT>::YFace>;  \
template class ConvectiveFlux<SpatialOps::FaceTypes<VolT>::ZFace>;

#include <operators/Operators.h>
#include <fields/Fields.h>
INSTANTIATE( LBMS::XVolField )
INSTANTIATE( LBMS::YVolField )
INSTANTIATE( LBMS::ZVolField )
