/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef Taylor_Vortex
#define Taylor_Vortex

#include <expression/Expression.h>

/**
 *  \class TaylorGreenVel3D
 *  \author Derek Cline, adapted from Wasatch_Taylor_Vortex by Tony Saad
 *  \date July, 2013
 *  \brief Implements the generalized Taylor-Green vortex three dimensional
velocity field. This is usually used as an initial condition for the velocity.
 *
 *  The taylor vortex velocity field in x direction is given as
 *  \f[
 *    u(x,y,z)= \frac{2}{\sqrt{3}} \sin(\theta + \frac{2\pi}{3}) \sin x \cos y \cos z
 *  \f]
 *  \f[
 *    v(x,y,z)= \frac{2}{\sqrt{3}} \sin(\theta - \frac{2\pi}{3}) \sin y \cos x \cos z
 *  \f]
 *  \f[
 *    w(x,y,z)= \frac{2}{\sqrt{3}} \sin(\theta) \sin z \cos x \cos y
 *  \f]
 *  where
 *   - \f$\theta\f$ is some angle specified by the user
 *  Note: that we implement only one expression for this velocity field. By merely
 *  shuffling the coordinates, we can generate all velocity components. This
 *  should be in processing the user input.
 *
 *  Citation:
 *    Brachet et. al., Small-scale structure of the Taylor-Green vortex,
 *    J. Fluid Mech, vol. 130, no. 41, p. 1452, 1983.
 */

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

namespace LBMS{

  template< typename FieldT >
  class TaylorGreenVel3D : public Expr::Expression<FieldT>
  {
  public:

    /**
     *  \brief Builds a Taylor Vortex velocity function
     */
    struct Builder : public Expr::ExpressionBuilder
    {
      Builder( const Expr::Tag& result, ///< the velocity
               const Expr::Tag& xTag,   ///< x coordinate
               const Expr::Tag& yTag,   ///< y coordinate
               const Expr::Tag& zTag,   ///< z
               const double angle=0.1   ///< Kinematic viscosity of the fluid
              );
      Expr::ExpressionBase* build() const;
    private:
      const double angle_;
      const Expr::Tag xt_, yt_, zt_;
    };

    void evaluate();

  private:

    TaylorGreenVel3D( const Expr::Tag& xTag,
                      const Expr::Tag& yTag,
                      const Expr::Tag& zTag,
                      const double angle );
    const double angle_;
    DECLARE_FIELDS( FieldT, x_, y_, z_ )
  };

  //====================================================================
  //
  //      IMPLEMENTATION
  //
  //====================================================================

  //--------------------------------------------------------------------

  template<typename FieldT>
  TaylorGreenVel3D<FieldT>::
  TaylorGreenVel3D( const Expr::Tag& xtag,
                    const Expr::Tag& ytag,
                    const Expr::Tag& ztag,
                    const double angle )
  : Expr::Expression<FieldT>(),
    angle_(angle)
  {
    x_ = this->template create_field_request< FieldT >( xtag );
    y_ = this->template create_field_request< FieldT >( ytag );
    z_ = this->template create_field_request< FieldT >( ztag );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  TaylorGreenVel3D<FieldT>::
  evaluate()
  {
    using namespace SpatialOps;
    FieldT& phi = this->value();
    const FieldT& x = x_->field_ref();
    const FieldT& y = y_->field_ref();
    const FieldT& z = z_->field_ref();
    phi <<= (2/sqrt(3.0))*sin(angle_) * sin(2.0*PI * x) * cos( 2.0*PI * y ) * cos(2.0*PI * z);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  TaylorGreenVel3D<FieldT>::Builder::
  Builder( const Expr::Tag& result,
           const Expr::Tag& xtag,
           const Expr::Tag& ytag,
           const Expr::Tag& ztag,
           const double angle)
  : ExpressionBuilder(result),
    angle_(angle),
    xt_( xtag ),
    yt_( ytag ),
    zt_( ztag )
  {}

  //--------------------------------------------------------------------

  template< typename FieldT >
  Expr::ExpressionBase*
  TaylorGreenVel3D<FieldT>::Builder::
  build() const
  {
    return new TaylorGreenVel3D<FieldT>( xt_, yt_, zt_, angle_ );
  }

  //--------------------------------------------------------------------

} //end namespace

#endif //Taylor_Vortex
