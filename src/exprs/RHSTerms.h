#ifndef RHSTerms_h
#define RHSTerms_h

#include <map>
#include <iomanip>

//-- ExprLib includes --//
#include <expression/Tag.h>

/**
 *  \enum FieldSelector
 *  \brief Use this enum to populate information in the FieldTagInfo type.
 */
enum FieldSelector{
  CONVECTIVE_FLUX_X,
  CONVECTIVE_FLUX_Y,
  CONVECTIVE_FLUX_Z,
  DIFFUSIVE_FLUX_X,
  DIFFUSIVE_FLUX_Y,
  DIFFUSIVE_FLUX_Z,
  SOURCE_TERM
};

/**
 * \todo currently we only allow one of each info type.  But there
 *       are cases where we may want multiple ones.  Example:
 *       diffusive terms in energy equation.  Expand this
 *       capability.
 */
typedef std::map< FieldSelector, Expr::Tag > FieldTagInfo; //< Defines a map to hold information on ExpressionIDs for the RHS.

inline std::string name( const FieldSelector fs ){
  switch( fs ){
    case CONVECTIVE_FLUX_X : return "X-convective flux";
    case CONVECTIVE_FLUX_Y : return "Y-convective flux";
    case CONVECTIVE_FLUX_Z : return "Z-convective flux";
    case DIFFUSIVE_FLUX_X  : return "X-diffusive flux";
    case DIFFUSIVE_FLUX_Y  : return "Y-diffusive flux";
    case DIFFUSIVE_FLUX_Z  : return "Z-diffusive flux";
    case SOURCE_TERM       : return "Source";
  }
}

template<typename T>
inline T& operator<<( T&t, const FieldSelector fs ){
  t << name( fs );
  return t;
}

template<typename T>
T& operator<<( T&t, const FieldTagInfo& fti ){
  for( typename FieldTagInfo::const_iterator i=fti.begin(); i!=fti.end(); ++i ){
    t << "  " << std::setw(17) << name(i->first) << " : " << i->second << std::endl;
  }
  return t;
}

#endif // RHSTerms_h
