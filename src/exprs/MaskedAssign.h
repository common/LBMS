/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <boost/foreach.hpp>

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>

#ifndef MaskedAssign_h
#define MaskedAssign_h

namespace LBMS{

  /**
   *  \class MaskedAssign
   *  \author Derek Cline
   *  \date   Feb, 2016
   *
   *  \brief Assigns a double to a field that is masked
   *
   *  \param mask the mask that covers points to be assigned
   *  \param value the value to be assigned
   *
   */
  template< typename FieldT >
  class MaskedAssign : public Expr::Expression<FieldT>
  {
  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const SpatialOps::SpatialMask<FieldT> mask_;
      const Expr::Tag tag_;
    public:

      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const Expr::Tag& tag );

      Expr::ExpressionBase* build() const;
    };

    void evaluate();
  private:
    MaskedAssign( const SpatialOps::SpatialMask<FieldT>& mask, const Expr::Tag& tag );

    const SpatialOps::SpatialMask<FieldT> mask_;
    DECLARE_FIELDS( FieldT, field_ )
  };

  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT >
  MaskedAssign<FieldT>::MaskedAssign( const SpatialOps::SpatialMask<FieldT>& mask,
                                      const Expr::Tag& tag )
    : Expr::Expression<FieldT>(), mask_(mask)
  {
    field_ = this->template create_field_request<FieldT>( tag );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void MaskedAssign<FieldT>::evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& field = field_->field_ref();
    masked_assign( mask_, result, field );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  MaskedAssign<FieldT>::Builder::
  Builder( const Expr::Tag& result,
           const SpatialOps::SpatialMask<FieldT>& mask,
           const Expr::Tag& tag )
    : ExpressionBuilder(result), mask_(mask), tag_(tag)
  {}

  //--------------------------------------------------------------------

  template< typename FieldT >
  Expr::ExpressionBase*
  MaskedAssign<FieldT>::Builder::build() const
  { return new MaskedAssign<FieldT>( mask_, tag_ ); }

}//end namespace

#endif // MaskedAssign_h
