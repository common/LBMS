/*O
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMS_BoundaryConditions_h
#define LBMS_BoundaryConditions_h

#include <spatialops/structured/stencil/FVStaggeredBCOp.h>
#include <spatialops/OperatorDatabase.h>

//--- LBMS Includes ---//
#include <lbms/Options.h>
#include <lbms/Bundle_fwd.h>
#include <operators/Operators.h>
#include <lbms/BCOptions.h>

#include <expression/Expression.h>
#include <expression/BoundaryConditionExpression.h>
namespace LBMS{


 template< typename FieldT, typename DirT >
 class LinearFunctionBC : public Expr::BoundaryConditionExpression<FieldT, DirT>
 {
   typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::BCValFieldT IVarT;

   DECLARE_FIELD( IVarT, x_ )
   const double a_, b_;

   LinearFunctionBC( typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask,
                     const BCOptions& bcOpts,
                     const Expr::Tag& indepVarTag,
                     const double slope,
                     const double intercept )
   : Expr::BoundaryConditionExpression<FieldT, DirT >( mask, bcOpts.bctype_, bcOpts.points.side() ),
     a_( slope ),
     b_( intercept )
   {
     x_ = this->template create_field_request<IVarT>( indepVarTag );
   }

 public:

   /**
    *  @brief Builds a LinearFunction Expression.
    */
   struct Builder : public Expr::ExpressionBuilder
   {
     /**
      * @param mask             The mask on which to apply the BC
      * @param bcOpts           The BCOptions
      * @param depVarTag        The dependent variable set by this function
      * @param indepVarTag      The independent variable to use for this function
      * @param slope            The slope of the line
      * @param intercept        The intercept of the line
      */
     Builder( typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask,
              const BCOptions& bcOpts,
              const Expr::Tag& depVarTag,
              const Expr::Tag& indepVarTag,
              const double slope,
              const double intercept )
     : Expr::ExpressionBuilder( depVarTag ),
       mask_(mask),
       opts_(bcOpts),
       a_( slope ),
       b_( intercept ),
       ivarTag_( indepVarTag )
     {}

     ~Builder(){}
     Expr::ExpressionBase* build() const{
       return new LinearFunctionBC<FieldT, DirT>( mask_, opts_, ivarTag_, a_, b_);
     }

   private:
     typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask_;
     const BCOptions opts_;
     const double a_, b_;
     const Expr::Tag ivarTag_;
   };

   void evaluate() {
     FieldT& f = this->value();
     const IVarT& x = x_->field_ref();
     switch( this->bcType_){
       case SpatialOps::NEUMANN  : (*this->neumannOp_  )(this->get_mask(), f, (x*a_ + b_), this->side_ == SpatialOps::MINUS_SIDE);  break;
       case SpatialOps::DIRICHLET: (*this->dirichletOp_)(this->get_mask(), f, (x*a_ + b_), this->side_ == SpatialOps::MINUS_SIDE);  break;
       default: assert(false);
     }
   }
 };

 //====================================================================

 /**
  *  @class GaussianFunctionBC, based on GaussianFunction
  *  @author James C. Sutherland, John Hutchins
  *  @date October, 2014
  *  @brief Implements a gaussian function of a single independent variable.
  *
  * The gaussian function is written as
  *  \f[
  *    f(x) = y_0 + a \exp\left( \frac{\left(x-x_0\right)^2 }{2\sigma^2} \right)
  *  \f]
  * where
  *  - \f$x_0\f$ is the mean (center of the gaussian)
  *  - \f$\sigma\f$ is the standard deviation (width of the gaussian)
  *  - \f$a\f$ is the amplitude of the gaussian
  *  - \f$y_0\f$ is the baseline value.
  */
 template< typename FieldT, typename DirT>
 class GaussianFunctionBC : public Expr::BoundaryConditionExpression<FieldT, DirT>
 {
   typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::BCValFieldT IVarT;

   const double a_, sigma_, mean_, yo_;
   DECLARE_FIELD( IVarT, x_ )

   GaussianFunctionBC( typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask,
                       const BCOptions& o,
                       const Expr::Tag& indepVarTag,
                       const double a,
                       const double stddev,
                       const double mean,
                       const double yo )
   :  Expr::BoundaryConditionExpression<FieldT, DirT >( mask, o.bctype_, o.points.side() ),
      a_( a ),
      sigma_( stddev ),
      mean_( mean ),
      yo_( yo )
   {
     x_ = this->template create_field_request<IVarT>( indepVarTag );
   }

 public:

   /**
    *  @brief Builds a GaussianFunction Boundary Condition Expression.
    */
   struct Builder : public Expr::ExpressionBuilder
   {
     Builder( typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask,
              const BCOptions& bcOpts,      ///< BC information
              const Expr::Tag& depVarTag,   ///< dependent variable tag
              const Expr::Tag& indepVarTag, ///< independent variable tag
              const double a,               ///< Amplitude of the Gaussian spike
              const double stddev,          ///< Standard deviation
              const double mean,            ///< Mean of the function
              const double yo=0.0           ///< baseline value
              )
     : Expr::ExpressionBuilder(depVarTag),
       mask_(mask),
       opts_(bcOpts),
       a_(a),
       sigma_(stddev),
       mean_(mean),
       yo_(yo),
       ivarTag_( indepVarTag )
     {}

     ~Builder(){}
     Expr::ExpressionBase* build() const{
       return new GaussianFunctionBC<FieldT, DirT>( mask_, opts_, ivarTag_, a_, sigma_, mean_, yo_ );
     }

   private:
     typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr mask_;
     const BCOptions opts_;
     const double a_, sigma_, mean_, yo_;
     const Expr::Tag ivarTag_;
   };

   void evaluate() {
     FieldT& f = this->value();
     const IVarT& x = x_->field_ref();
     const double denom = 1.0/(2.0*sigma_*sigma_);

     switch( this->bcType_){
       case SpatialOps::NEUMANN:
         (*this->neumannOp_)( this->get_mask(),
                              f,
                              yo_ + a_ * exp( -denom * (x-mean_)*(x-mean_) ),
                              this->side_ == SpatialOps::MINUS_SIDE );
         break;

       case SpatialOps::DIRICHLET:
         (*this->dirichletOp_)( this->get_mask(),
                                f,
                                yo_ + a_ * exp( -denom * (x-mean_)*(x-mean_) ),
                                this->side_ == SpatialOps::MINUS_SIDE );
         break;

       default:
         assert(false);
     }
   }
};

} // namespace LBMS


#endif // LBMS_BoundaryConditions_h
