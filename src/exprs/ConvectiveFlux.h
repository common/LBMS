/**
 * \file ConvectiveFlux.h
 * \author James C. Sutherland
 * \date June, 2012
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ConvectiveFlux_Expr_h
#define ConvectiveFlux_Expr_h

#include <expression/Expression.h>
#include <spatialops/structured/stencil/FVStaggeredOperatorTypes.h>

/**
 *  \class ConvectiveFlux
 */
template< typename FluxT >
class ConvectiveFlux
 : public Expr::Expression<FluxT>
{
  typedef typename SpatialOps::VolType<FluxT>::VolField  ScalarT;
  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, FluxT >::type InterpT;

  DECLARE_FIELD( FluxT,   vel_ )
  DECLARE_FIELD( ScalarT, phi_ )

  const InterpT* interpOp_;

  ConvectiveFlux( const Expr::Tag& velTag,
                  const Expr::Tag& phiTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag velTag_, phiTag_;
  public:
    /**
     *  @brief Build a ConvectiveFlux expression
     *  @param resultTag the tag for the value that this expression computes
     *  @param velTag the velocity to form the advective flux
     *  @param phiTag the scalar we are advecting
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& velTag,
             const Expr::Tag& phiTag );

    Expr::ExpressionBase* build() const;
  };

  ~ConvectiveFlux(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};

#endif // ConvectiveFlux_Expr_h
