#ifndef NormalStress_Expr_h
#define NormalStress_Expr_h

#include <expression/Expression.h>

#include <fields/Fields.h>

/**
 *  @class NormalStress
 *
 *  @author James C. Sutherland
 *
 *  @brief calculates normal components of the stress tensor,
 *  e.g. \f$\tau_{xx}=-2\mu \frac{\partial u}{\partial x} +
 *  \frac{2}{3}\mu\left( \frac{\partial u}{\partial x} +
 *  \frac{\partial v}{\partial y} + \frac{\partial w}{\partial z}
 *  \right)\f$
 *
 *  \par Example 1
 *  Form \f$\tau_{xx}\f$ on an x-bundle.
 *  \code{.cpp}
 *    typedef NormalStress<TauXX>::Builder TauXXBuilder;
 *    TauXXBuilder tauxx( Expr::Tag( "u_xbundle",          Expr::STATE_NONE ),
 *                        Expr::Tag( "dilatation_xbundle", Expr::STATE_NONE ),
 *                        Expr::Tag( "viscosity_xbundle",  Expr::STATE_NONE ) );
 *  \endcode
 */
template< typename TauT >
class NormalStress
 : public Expr::Expression<TauT>
{
  typedef typename LBMS::NativeFieldSelector<typename TauT::Location::Bundle>::VolT     VolT;
  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, VolT, TauT >::type  InterpT;

  DECLARE_FIELDS( VolT, visc_, dil_ )
  DECLARE_FIELD( TauT, vel_ )

  const InterpT* interpOp_;

  NormalStress( const Expr::Tag& vel,
                const Expr::Tag& dilatation,
                const Expr::Tag& visc );

public:
  /**
   *  @class Builder
   *  @brief Construct a NormalStress object.
   */
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag dvel_, dil_, visc_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& normalVel,
             const Expr::Tag& dilatation,
             const Expr::Tag& visc )
      : ExpressionBuilder( resultTag ),
        dvel_( normalVel  ),
        dil_ ( dilatation ),
        visc_( visc       )
    {}
    Expr::ExpressionBase* build() const{
      return new NormalStress<TauT>( dvel_, dil_, visc_ );
    }
  };

  ~NormalStress(){}

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename TauT >
NormalStress<TauT>::
NormalStress( const Expr::Tag& vel,
              const Expr::Tag& dil,
              const Expr::Tag& visc )
  : Expr::Expression<TauT>()
{
  this->set_gpu_runnable(true);

  vel_  = this->template create_field_request<TauT>( vel  );
  dil_  = this->template create_field_request<VolT>( dil  );
  visc_ = this->template create_field_request<VolT>( visc );
}

//--------------------------------------------------------------------

template< typename TauT >
void
NormalStress<TauT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename TauT >
void
NormalStress<TauT>::
evaluate()
{
  TauT& result = this->value();
  const TauT& vel  = vel_ ->field_ref();
  const VolT& dil  = dil_ ->field_ref();
  const VolT& visc = visc_->field_ref();
  const double oneThird = 1.0/3.0;
  result <<= 2.0 * (*interpOp_)(visc) * ( oneThird * (*interpOp_)(dil) - vel );
}

//--------------------------------------------------------------------


#endif // NormalStress_Expr_h
