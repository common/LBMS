/**
 *  \file   exprs/Coordinate.h
 *
 *  \author James C. Sutherland
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef Coordinate_Expr_h
#define Coordinate_Expr_h

#include <expression/Expression.h>
#include <lbms/Bundle.h>

/**
 *  \class Coordinates
 */
template< typename FieldT >
class Coordinates
 : public Expr::Expression<FieldT>
{
  const LBMS::BundlePtr bundle_;
  bool rebuild_;
  Coordinates( const LBMS::BundlePtr& b );
public:
  class Builder : public Expr::ExpressionBuilder
  {
    const LBMS::BundlePtr bundle_;
  public:
    /**
     *  @brief Build a Coordinate expression
     *  @param resultTags the tags for the x, y, and z coordinates
     *  @param b the bundle whos coordinates we are concerned with
     */
    Builder( const Expr::TagList& resultTags, const LBMS::BundlePtr& b ) : ExpressionBuilder( resultTags ), bundle_(b){}
    Expr::ExpressionBase* build() const{ return new Coordinates<FieldT>(bundle_); }
  };
  ~Coordinates(){}
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT >
Coordinates<FieldT>::
Coordinates( const LBMS::BundlePtr& b )
  : Expr::Expression<FieldT>(),
    bundle_( b ),
    rebuild_(true)
{}

//--------------------------------------------------------------------

template< typename FieldT >
void
Coordinates<FieldT>::
evaluate()
{
  if( !rebuild_ ) return;
  rebuild_ = false;

  typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
  assert( results.size() == 3 );
  set_coord_values( *results[0], *results[1], *results[2], bundle_ );
}

//--------------------------------------------------------------------

#endif // Coordinate_Expr_h
