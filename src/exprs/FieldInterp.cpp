/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/**
 * \file FieldInterp.cpp
 * \date Oct 24, 2012
 * \author James C. Sutherland
 */

#include "FieldInterp.h"

//-- SpatialOps includes --//
#include <spatialops/OperatorDatabase.h>


template< typename InterpT >
FieldInterp<InterpT>::FieldInterp( const Expr::Tag& srcFieldTag )
  : Expr::Expression<typename InterpT::DestFieldType>()
{
  srcField_ = this->template create_field_request< SrcT >( srcFieldTag );
}

//--------------------------------------------------------------------

template< typename InterpT >
void
FieldInterp<InterpT>::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename InterpT >
void
FieldInterp<InterpT>::evaluate()
{
  DestT& result = this->value();
  interp_->apply_to_field( srcField_->field_ref(), result );
}

//--------------------------------------------------------------------

template< typename InterpT >
FieldInterp<InterpT>::Builder::Builder( const Expr::Tag& resultTag,
                                        const Expr::Tag& srcFieldTag )
  : ExpressionBuilder( resultTag ),
    srcFieldTag_( srcFieldTag )
{}

//--------------------------------------------------------------------

template< typename InterpT >
Expr::ExpressionBase*
FieldInterp<InterpT>::Builder::build() const
{
  return new FieldInterp<InterpT>( srcFieldTag_ );
}


//==========================================================================
// Explicit template instantiation for supported versions of this expression
//
#include <operators/InterpCoarseToFine.h>
#include <operators/InterpFineToCoarse.h>

#define DECLARE( FIELDT, INTERP ) template class FieldInterp< INTERP<FIELDT> >;

#define DECLARE_VARIANTS( DIR )                                                \
  DECLARE( LBMS::FaceFieldSelector<DIR>::ParallelT, LBMS::InterpFineToCoarse ) \
  DECLARE( LBMS::FaceFieldSelector<DIR>::Perp1T,    LBMS::InterpFineToCoarse ) \
  DECLARE( LBMS::FaceFieldSelector<DIR>::Perp2T,    LBMS::InterpFineToCoarse ) \
  DECLARE( LBMS::NativeFieldSelector<DIR>::VolT,    LBMS::InterpFineToCoarse ) \
                                                                               \
  DECLARE( LBMS::FaceFieldSelector<DIR>::ParallelT, LBMS::InterpCoarseToFine ) \
  DECLARE( LBMS::FaceFieldSelector<DIR>::Perp1T,    LBMS::InterpCoarseToFine ) \
  DECLARE( LBMS::FaceFieldSelector<DIR>::Perp2T,    LBMS::InterpCoarseToFine ) \
  DECLARE( LBMS::NativeFieldSelector<DIR>::VolT,    LBMS::InterpCoarseToFine )

DECLARE_VARIANTS( SpatialOps::XDIR )
DECLARE_VARIANTS( SpatialOps::YDIR )
DECLARE_VARIANTS( SpatialOps::ZDIR )
//==========================================================================
