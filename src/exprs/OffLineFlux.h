#ifndef OffLineFlux_Expr_h
#define OffLineFlux_Expr_h

#include <expression/Expression.h>

#include <lbms/CrossLineFlux.h>
#include <lbms/Bundle.h>
#include <lbms/Direction.h>

/**
 *  \class OffLineFlux
 *  \author Derek A. Cline
 *  \date May, 2013
 *  \brief Creates a corrected offline flux
 */

template< typename FluxT,
          typename CoarseFluxT >
class OffLineFlux
 : public Expr::Expression<FluxT>
{
  DECLARE_FIELD( CoarseFluxT, coarse_ )
  DECLARE_FIELD( FluxT,       fine_   )

  OffLineFlux( const Expr::Tag& coarseTag,
               const Expr::Tag& fineTag,
               const LBMS::BundlePtr bundle );

  const LBMS::BundlePtr bundle_;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag coarseTag_, fineTag_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& coarseTag,
             const Expr::Tag& fineTag,
             const LBMS::BundlePtr bundle )
      : ExpressionBuilder( resultTag ),
        coarseTag_( coarseTag ),
        fineTag_  ( fineTag   ),
        bundle_   ( bundle    )
    {}
    ~Builder(){}
    const LBMS::BundlePtr bundle_;
    Expr::ExpressionBase* build() const{
      return new OffLineFlux<FluxT,CoarseFluxT>( coarseTag_, fineTag_, bundle_ );
    }

  };
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FluxT, typename CoarseFluxT >
OffLineFlux<FluxT,CoarseFluxT>::
OffLineFlux( const Expr::Tag& coarseTag,
             const Expr::Tag& fineTag,
             const LBMS::BundlePtr bundle )
  : Expr::Expression<FluxT>(),
    bundle_( bundle )
{
  coarse_ = this->template create_field_request<CoarseFluxT>( coarseTag );
  fine_   = this->template create_field_request<FluxT      >( fineTag   );
}

//--------------------------------------------------------------------

template< typename FluxT, typename CoarseFluxT >
void
OffLineFlux<FluxT,CoarseFluxT>::
evaluate()
{
  FluxT& result = this->value();

  const CoarseFluxT& coarse = coarse_->field_ref();
  const FluxT      & fine   = fine_  ->field_ref();

  LBMS::CrossLineFlux<FluxT, CoarseFluxT> *object = new LBMS::CrossLineFlux<FluxT, CoarseFluxT>(bundle_);
  object->create_synthetic_flux( fine, coarse, result );
  delete object;
}

//--------------------------------------------------------------------


#endif // OffLineFlux_Expr_h
