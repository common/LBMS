#ifndef OneSidedDivergence_Expr_h
#define OneSidedDivergence_Expr_h

/**
 *  \class OneSidedDivergence
 *  \author Derek A. Cline
 *  \date May, 2013
 *  \brief Creates a OneSidedDivergence
 */

template< typename DivT, typename FieldT >
class OneSidedDivergence
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELD( FieldT, phi_ )

  OneSidedDivergence( const SpatialOps::SpatialMask<FieldT>& mask, const Expr::Tag& phiTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag phiTag_;
    const SpatialOps::SpatialMask<FieldT> mask_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const Expr::Tag& phiTag )
      : ExpressionBuilder( result ),
        mask_  ( mask   ),
        phiTag_( phiTag )
    {}
    Expr::ExpressionBase* build() const{
      return new OneSidedDivergence<DivT,FieldT>( mask_,phiTag_ );
    }

  };

  ~OneSidedDivergence(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  private:
  const DivT* divOp_;
  const SpatialOps::SpatialMask<FieldT> mask_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename DivT, typename FieldT >
OneSidedDivergence<DivT,FieldT>::
OneSidedDivergence( const SpatialOps::SpatialMask<FieldT>& mask,
                    const Expr::Tag& phiTag )
  : Expr::Expression<FieldT>(),
    mask_( mask )
{
  this->set_gpu_runnable(true);
  phi_ = this->template create_field_request< FieldT >( phiTag );
}

//--------------------------------------------------------------------

template< typename DivT, typename FieldT >
void
OneSidedDivergence<DivT,FieldT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator< DivT >();
}

//--------------------------------------------------------------------

template< typename DivT, typename FieldT >
void
OneSidedDivergence<DivT,FieldT>::
evaluate()
{
  FieldT& result = this->value();
  const FieldT& phi = phi_->field_ref();
  masked_assign( mask_, result, (*divOp_)( phi ) );
}

//--------------------------------------------------------------------


#endif // OneSidedDivergence_Expr_h
