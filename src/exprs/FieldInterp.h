/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file FieldInterp.h
 *
 * \date Oct 24, 2012
 * \author James C. Sutherland
 */

#ifndef FieldInterp_Expr_h
#define FieldInterp_Expr_h

#include <expression/Expression.h>

/**
 *  \class FieldInterp
 *  \brief Interpolate a field.  Typically used for bundle exchanges.
 *
 *  \tparam InterpT - the type of interpolant operator to use.  This operator
 *   must define two public types: \c SrcFieldT and \c DestFieldT, which define
 *   the types of fields that the operator consumes and produces.
 */
template< typename InterpT >
class FieldInterp
: public Expr::Expression<typename InterpT::DestFieldType>
{
  typedef typename InterpT::SrcFieldType   SrcT;
  typedef typename InterpT::DestFieldType  DestT;

  DECLARE_FIELD( SrcT, srcField_ )
  const InterpT* interp_;

  FieldInterp( const Expr::Tag& srcFieldTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag srcFieldTag_;
  public:
    /**
     *  @brief Build a FieldInterp expression
     *  @param resultTag the tag for the field that we interpolate to
     *  @param srcFieldTag the tag for the field that we interpolate from
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& srcFieldTag );
    Expr::ExpressionBase* build() const;
  };

  ~FieldInterp(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};

#endif // FieldInterp_Expr_h
