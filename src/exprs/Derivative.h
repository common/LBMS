#ifndef Derivative_Expr_h
#define Derivative_Expr_h

/**
 *  \class Derivative
 *  \author Derek A. Cline
 *  \date May, 2013
 *  \brief Creates a derivative for velocity gradients
 */

template< typename FluxT,
          typename FieldT >
class Derivative
 : public Expr::Expression<FluxT>
{
  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Gradient, FieldT, FluxT >::type GradT;
  DECLARE_FIELD( FieldT, vel_ )
  const GradT* gradOp_;
  Derivative( const Expr::Tag& velTag );
public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag velTag_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& velTag )
      : ExpressionBuilder( resultTag ),
        velTag_( velTag )
    {}

    Expr::ExpressionBase* build() const{
      return new Derivative<FluxT,FieldT>( velTag_ );
    }
  };

  ~Derivative(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FluxT, typename FieldT >
Derivative<FluxT,FieldT>::
Derivative( const Expr::Tag& velTag )
  : Expr::Expression<FluxT>()
{
  this->set_gpu_runnable(true);
  vel_ = this->template create_field_request< FieldT >( velTag );
}

//--------------------------------------------------------------------

template< typename FluxT, typename FieldT >
void
Derivative<FluxT,FieldT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_ = opDB.retrieve_operator< GradT >();
}

//--------------------------------------------------------------------

template< typename FluxT, typename FieldT >
void
Derivative<FluxT,FieldT>::
evaluate()
{
  FluxT& result = this->value();
  result <<= (*gradOp_)( vel_->field_ref() );
}

//--------------------------------------------------------------------

#endif // Derivative_Expr_h
