/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <boost/foreach.hpp>

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>

#ifndef GhostAssign_h
#define GhostAssign_h

namespace LBMS{

  /**
   *  \class GhostAssign
   *  \author Derek Cline
   *  \date   Feb, 2016
   *
   *  \brief Assigns a double to a field that is Ghost
   *
   *  \param mask the mask that covers points interior to what will be assigned
   *  \param ghostTag a field to be assigned to the field in the ghost cells.
   *  \param dir the direction normal to the boundary, used to shift the mask to the ghost cells
   *  \param side plus for the extent and minus for the origin, used to shift the mask to the ghost cells
   *
   */
  template< typename FieldT >
  class GhostAssign : public Expr::Expression<FieldT>
  {
  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const SpatialOps::SpatialMask<FieldT> mask_;
      const LBMS::Direction dir_;
      const SpatialOps::BCSide side_;
      const Expr::Tag ghostTag_;
    public:

      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const LBMS::Direction& dir,
               const SpatialOps::BCSide& side,
               const Expr::Tag& ghostTag );

      Expr::ExpressionBase* build() const;
    };

    void evaluate();
  private:
    GhostAssign( const SpatialOps::SpatialMask<FieldT>& mask,
                 const LBMS::Direction& dir,
                 const SpatialOps::BCSide& side,
                 const Expr::Tag& ghostTag );

    const SpatialOps::SpatialMask<FieldT> mask_;
    const Expr::Tag ghostTag_;
    const LBMS::Direction dir_;
    const SpatialOps::BCSide side_;

    SpatialOps::SpatialMask<FieldT>* shiftedMask_;

    DECLARE_FIELDS( FieldT, ghostField_ )
  };

  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT >
  GhostAssign<FieldT>::GhostAssign( const SpatialOps::SpatialMask<FieldT>& mask,
                                    const LBMS::Direction& dir,
                                    const SpatialOps::BCSide& side,
                                    const Expr::Tag& ghostTag )
    : Expr::Expression<FieldT>(),
      mask_     ( mask     ),
      dir_      ( dir      ),
      side_     ( side     ),
      ghostTag_ ( ghostTag )
  {
    ghostField_ = this->template create_field_request<FieldT>( ghostTag );

    //This will shift the mask to the appropriate cell for extrapolation.
    SpatialOps::IntVec shift(0,0,0);

    switch( dir ){
      case SpatialOps::XDIR::value: shift = shift + SpatialOps::IntVec(1,0,0); break;
      case SpatialOps::YDIR::value: shift = shift + SpatialOps::IntVec(0,1,0); break;
      case SpatialOps::ZDIR::value: shift = shift + SpatialOps::IntVec(0,0,1); break;
      case SpatialOps::NODIR::value:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "Invalid direction in Extrapolation expression" << std::endl;
        throw std::runtime_error( msg.str() );
        break;
    }

    if( side == SpatialOps::MINUS_SIDE ) shift = shift * SpatialOps::IntVec( -1,-1,-1 );

    std::vector<SpatialOps::IntVec> tempVec( (mask.points()).size() );

    size_t i=0;
    BOOST_FOREACH( const SpatialOps::IntVec& v, mask.points() ){
      tempVec[i++] = v + shift;
    }

    //Essentially copies everything from the previous mask but
    //uses the shifted points instead.
    shiftedMask_ = new SpatialOps::SpatialMask<FieldT>( mask.window_with_ghost(),
                                                        mask.boundary_info(),
                                                        mask.get_ghost_data(),
                                                        tempVec );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void GhostAssign<FieldT>::evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& ghostField = ghostField_->field_ref();

    masked_assign( *shiftedMask_, result, ghostField );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  GhostAssign<FieldT>::Builder::
  Builder( const Expr::Tag& result,
           const SpatialOps::SpatialMask<FieldT>& mask,
           const LBMS::Direction& dir,
           const SpatialOps::BCSide& side,
           const Expr::Tag& ghostTag )
    : ExpressionBuilder(result),
      mask_    ( mask     ),
      dir_     ( dir      ),
      side_    ( side     ),
      ghostTag_( ghostTag )
  {}

  //--------------------------------------------------------------------

  template< typename FieldT >
  Expr::ExpressionBase*
  GhostAssign<FieldT>::Builder::build() const
  { return new GhostAssign<FieldT>( mask_, dir_, side_, ghostTag_ ); }

}//end namespace

#endif // GhostAssign_h
