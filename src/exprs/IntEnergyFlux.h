#ifndef IntEnergyFlux_h
#define IntEnergyFlux_h

#include <expression/Expression.h>

/**
 *  @class IntEnergyFlux
 *  @author James C. Sutherland
 *  @date   May, 2009
 *
 *  @brief Calculates the total diffusive flux for the total internal energy in a given direction.
 *
 * The total internal energy equation is given by
 * \f{eqnarray}
 *   \frac{\partial \rho e_0}{\partial t} &=&
 *      - \nabla\cdot ( p \mathbf{u} )
 *      - \nabla\cdot\mathbf{q}
 *      - \nabla\cdot ( \tau \cdot \mathbf{u} ) \nonumber \\
 *      &=& \nabla\cdot \left(
 *      - \rho e_0 \mathbf{u}
 *      - p \mathbf{u}
 *      - \mathbf{q}
 *      - \tau \cdot \mathbf{u}
 *        \right) \nonumber
 * \f}
 * where we have neglected body forces.
 * This expression calculates the total diffusive flux of internal energy for a
 * given direction.  Specifically for direction \f$i\f$, this calculates
 * \f[
 *    -\tau_{ij} u_j - p u_i -q_i
 * \f]
 *
 * See documentation of the \link IntEnergyFlux::Builder \endlink
 * class for more information on building this expression.
 *
 * \par Template Parameters
 * <ul>
 * <li> \b InterpT Operator type to interpolate from cell centers to cell faces where fluxes are formed.
 * </ul>
 */
template< typename InterpT >
class IntEnergyFlux
  : public Expr::Expression< typename InterpT::DestFieldType >
{
  typedef typename InterpT::SrcFieldType   ScalarT;  ///< Cell centered field type
  typedef typename InterpT::DestFieldType  FluxT;    ///< Face centered field type

  const bool do1_, do2_, do3_;

  DECLARE_FIELDS( ScalarT, pvol_, rhoE0_, vel1_, vel2_, vel3_ )

  DECLARE_FIELDS( FluxT, tau1_, tau2_, tau3_ , heatFlux_ )

  const InterpT* interpOp_;

  IntEnergyFlux( const Expr::Tag& pTag,     ///< Pressure at cell centers
                 const Expr::Tag& rhoE0Tag,
                 const Expr::Tag& tau1Tag,  ///< First  stress component
                 const Expr::Tag& tau2Tag,  ///< Second stress component
                 const Expr::Tag& tau3Tag,  ///< Third  stress component
                 const Expr::Tag& vel1Tag,  ///< First  velocity component
                 const Expr::Tag& vel2Tag,  ///< Second velocity component
                 const Expr::Tag& vel3Tag,  ///< Third  velocity component
                 const Expr::Tag& heatFluxTag );

public:

  /**
   *  @class Builder
   *  @brief Builds an IntEnergyFlux expression.  See documentation of
   *  IntEnergyFlux for more details.
   */
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pTag_,    re0Tag_,  hfTag_,
                    vel1Tag_, vel2Tag_, vel3Tag_,
                    tau1Tag_, tau2Tag_, tau3Tag_;

  public:
    Builder( const Expr::Tag& resultTag, ///< Result tag
             const Expr::Tag& pTag,       ///< Pressure (cell centered)
             const Expr::Tag& rhoE0Tag,   ///< Total internal energy (cell centered)
             const Expr::Tag& tau1Tag,    ///< First  stress component (at the bundle-direction face)
             const Expr::Tag& tau2Tag,    ///< Second stress component (at the bundle-direction face)
             const Expr::Tag& tau3Tag,    ///< Third  stress component (at the bundle-direction face)
             const Expr::Tag& vel1Tag,    ///< First  velocity component (cell centered)
             const Expr::Tag& vel2Tag,    ///< Second velocity component (cell centered)
             const Expr::Tag& vel3Tag,    ///< Third  velocity component (cell centered)
             const Expr::Tag& heatFluxTag ///< Heat Flux (at the bundle-direction face)
             );
    Expr::ExpressionBase* build() const;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

};

//====================================================================





//====================================================================
//
//                          Implementation
//
//====================================================================





//====================================================================

template<typename InterpT>
IntEnergyFlux<InterpT>::
IntEnergyFlux( const Expr::Tag& pTag,
               const Expr::Tag& rhoE0Tag,
               const Expr::Tag& tau1Tag,
               const Expr::Tag& tau2Tag,
               const Expr::Tag& tau3Tag,
               const Expr::Tag& vel1Tag,
               const Expr::Tag& vel2Tag,
               const Expr::Tag& vel3Tag,
               const Expr::Tag& heatFluxTag )
  : Expr::Expression<FluxT>( ),

    do1_( vel1Tag != Expr::Tag() ),
    do2_( vel2Tag != Expr::Tag() ),
    do3_( vel3Tag != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  assert( do1_ );

  pvol_     = this->template create_field_request<ScalarT>( pTag        );
  rhoE0_    = this->template create_field_request<ScalarT>( rhoE0Tag    );
  heatFlux_ = this->template create_field_request<FluxT  >( heatFluxTag );

  vel1_ = this->template create_field_request<ScalarT>( vel1Tag );
  tau1_ = this->template create_field_request<FluxT  >( tau1Tag );

  if( do2_ ){
    vel2_ = this->template create_field_request<ScalarT>( vel2Tag );
    tau2_ = this->template create_field_request<FluxT  >( tau2Tag );
  }
  if( do3_ ){
    vel3_ = this->template create_field_request<ScalarT>( vel3Tag );
    tau3_ = this->template create_field_request<FluxT  >( tau3Tag );
  }
}

//--------------------------------------------------------------------

template<typename InterpT>
void
IntEnergyFlux<InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template<typename InterpT>
void
IntEnergyFlux<InterpT>::
evaluate()
{
  using namespace SpatialOps;
  FluxT& flux = this->value();

  const ScalarT& pvol     = pvol_    ->field_ref();
  const ScalarT& rhoE0    = rhoE0_   ->field_ref();
  const FluxT  & heatFlux = heatFlux_->field_ref();

  // form the pressure work and heat flux terms.
  const ScalarT& vel1 = vel1_->field_ref();
  const FluxT  & tau1 = tau1_->field_ref();
  flux <<= (*interpOp_)(( pvol ) * ( vel1 )) + heatFlux + (*interpOp_)( vel1 ) * ( tau1 );

  if( do2_ ){
    const ScalarT& vel2 = vel2_->field_ref();
    const FluxT  & tau2 = tau2_->field_ref();
    flux <<= flux + (*interpOp_)( vel2 ) * tau2;
  }
  if( do3_ ){
    const ScalarT& vel3 = vel3_->field_ref();
    const FluxT  & tau3 = tau3_->field_ref();
    flux <<= flux + (*interpOp_)( vel3 ) * tau3;
  }
}

//--------------------------------------------------------------------

template<typename InterpT>
IntEnergyFlux<InterpT>::Builder::
Builder( const Expr::Tag& resultTag,
         const Expr::Tag& pTag,
         const Expr::Tag& rhoE0Tag,
         const Expr::Tag& tau1Tag,
         const Expr::Tag& tau2Tag,
         const Expr::Tag& tau3Tag,
         const Expr::Tag& vel1Tag,
         const Expr::Tag& vel2Tag,
         const Expr::Tag& vel3Tag,
         const Expr::Tag& heatFluxTag )
  : ExpressionBuilder( resultTag ),
    pTag_   ( pTag        ),
    re0Tag_ ( rhoE0Tag    ),
    hfTag_  ( heatFluxTag ),
    vel1Tag_( vel1Tag     ),  vel2Tag_( vel2Tag ),  vel3Tag_( vel3Tag ),
    tau1Tag_( tau1Tag     ),  tau2Tag_( tau2Tag ),  tau3Tag_( tau3Tag )
{
}

//--------------------------------------------------------------------

template<typename InterpT>
Expr::ExpressionBase*
IntEnergyFlux<InterpT>::Builder::
build() const
{
  return new IntEnergyFlux<InterpT>( pTag_,    re0Tag_,
                                     tau1Tag_, tau2Tag_, tau3Tag_,
                                     vel1Tag_, vel2Tag_, vel3Tag_,
                                     hfTag_ );
}

//--------------------------------------------------------------------
#endif // IntEnergyFlux_h
