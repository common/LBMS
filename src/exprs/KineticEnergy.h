#ifndef KineticEnergy_h
#define KineticEnergy_h

#include <expression/Expression.h>


/**
 *  \class KineticEnergy
 *  \author Derek Cline
 *  \date Nov, 2013
 *  \brief Implements an expression to solve for the Kinetic Energy
 *
 *  Note that perp1 and perp2 DO NOT NECESSARILY refer to the first and second
 *  perpendicular directions in the case of 2D.  In this case, perp1 always refers to
 *  the only perpendicular direction and perp2 always refers to the NULL direction.
 *
 */
template< typename FieldT >
class KineticEnergy
  : public Expr::Expression< FieldT >
{
  const bool doPerp1_, doPerp2_;

  DECLARE_FIELDS( FieldT, parVel_, perp1Vel_, perp2Vel_ )

  KineticEnergy( const Expr::Tag& parVelTag,
                 const Expr::Tag& perp1VelTag,
                 const Expr::Tag& perp2VelTag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag parVelTag_, perp1VelTag_, perp2VelTag_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& parVelTag,
             const Expr::Tag& perp1VelTag,
             const Expr::Tag& perp2VelTag )
      : ExpressionBuilder( resultTag ),
        parVelTag_(   parVelTag ),
        perp1VelTag_( perp1VelTag ),
        perp2VelTag_( perp2VelTag )
    {}

    Expr::ExpressionBase* build() const{
      return new KineticEnergy<FieldT>( parVelTag_, perp1VelTag_, perp2VelTag_ );
    }
  };

  void evaluate();

};

//====================================================================





//====================================================================
//
//                           Implementation
//
//====================================================================





//====================================================================

template< typename FieldT >
KineticEnergy<FieldT>::
KineticEnergy( const Expr::Tag& parVelTag,
               const Expr::Tag& perp1VelTag,
               const Expr::Tag& perp2VelTag )
  : Expr::Expression<FieldT>(),
    doPerp1_( perp1VelTag != Expr::Tag() ),
    doPerp2_( perp2VelTag != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  assert( parVelTag != Expr::Tag() );

  parVel_ = this->template create_field_request<FieldT>( parVelTag   );
  if( doPerp1_ ) perp1Vel_ = this->template create_field_request<FieldT>( perp1VelTag );
  if( doPerp2_ ) perp2Vel_ = this->template create_field_request<FieldT>( perp2VelTag );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
KineticEnergy<FieldT>::
evaluate()
{
  using namespace SpatialOps;
  FieldT& ke = this->value();

  const FieldT& parVel = parVel_->field_ref();

  if( doPerp1_ && doPerp2_ ){
    const FieldT& perp1Vel = perp1Vel_->field_ref();
    const FieldT& perp2Vel = perp2Vel_->field_ref();
    ke <<= 0.5 * ( parVel * parVel + perp1Vel * perp1Vel + perp2Vel * perp2Vel );
  }
  else if( doPerp1_ ){
    const FieldT& perp1Vel = perp1Vel_->field_ref();
    ke <<= 0.5 * ( parVel * parVel + perp1Vel * perp1Vel );
  }
  else{
    ke <<= 0.5 * ( parVel * parVel );
  }
}

//--------------------------------------------------------------------

#endif // KineticEnergy_h
