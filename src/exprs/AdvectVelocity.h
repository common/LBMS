#ifndef AdvectVelocity_h
#define AdvectVelocity_h

#include <expression/Expression.h>

//====================================================================

/**
 *  @class AdvectVelocity
 *  @author James C. Sutherland
 *  @date   September, 2008
 *
 *  @brief Calculates the advecting velocity for use in ODT.
 */

template< typename InterpT >
class AdvectVelocity
  : public Expr::Expression<typename InterpT::DestFieldType>
{
  typedef typename InterpT::DestFieldType AdvelT;
  typedef typename InterpT::SrcFieldType  VelT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag vT_;
  public:

    /**
     *  @param advelTag the result of this expression
     *  @param velTag The Expr::Tag that describes the transported
     *         velocity that is to be used in defining the advecting
     *         velocity.
     */
    Builder( const Expr::Tag& advelTag,
             const Expr::Tag& velTag );
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new AdvectVelocity<InterpT>(vT_); }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  AdvectVelocity( const Expr::Tag& velTag );

  DECLARE_FIELD( VelT, vel_ )
  const InterpT* interpOp_;
};



// ###################################################################
//
//                           Implementation
//
// ###################################################################


template< typename InterpT >
AdvectVelocity<InterpT>::
AdvectVelocity( const Expr::Tag& velTag )
  : Expr::Expression<AdvelT>()
{
  this->set_gpu_runnable(true);
  vel_ = this->template create_field_request<VelT>( velTag );
}

//--------------------------------------------------------------------

template< typename InterpT >
void
AdvectVelocity<InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename InterpT >
void
AdvectVelocity<InterpT>::
evaluate()
{
  this->value() <<= (*interpOp_)( vel_->field_ref() );
}

//--------------------------------------------------------------------

template< typename InterpT >
AdvectVelocity<InterpT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& velTag )
  : ExpressionBuilder(result),
    vT_(velTag)
{}

#endif // AdvectVelocity_h
