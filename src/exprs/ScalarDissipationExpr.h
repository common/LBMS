#ifndef ScalarDissipationExpr_h
#define ScalarDissipationExpr_h

#include <expression/Expression.h>

#include <spatialops/SpatialOpsTools.h>   // IsSameType
#include <boost/static_assert.hpp>

/**
 *  @class ScalarDissipationExpr
 *  @brief Calculates the scalar dissipation rate in 1D
 *
 *  Given the viscosity, mixture fraction and Schmidt number, this
 *  calculates
 *  \f[ \chi = D \left( \frac{\partial f}{\partial x} \right)^2 \f]
 *  with \f$ Sc = \mu / D \f$
 *
 *  \par Template Parameters
 *  <ul>
 *  <li> \b GradT - The gradient operator.
 *  <li> \b InterpT - the interpolant operator to move from the face to cell.
 *  </ul>
 */
template< typename GradT,
          typename InterpT >
class ScalarDissipationExpr
  : public Expr::Expression< typename GradT::SrcFieldType >
{
  typedef typename GradT::DestFieldType FluxT;
  typedef typename GradT::SrcFieldType  ScalarT;

  BOOST_STATIC_ASSERT( bool( SpatialOps::IsSameType< FluxT,
                             typename InterpT::SrcFieldType >::result ) );
  BOOST_STATIC_ASSERT( bool( SpatialOps::IsSameType< ScalarT,
                             typename InterpT::DestFieldType >::result ) );

  const double schmidt_;

  const GradT* gradOp_;
  const InterpT* interpOp_;

  DECLARE_FIELD( ScalarT, mixfr_, visc_ )

  ScalarDissipationExpr( const Expr::Tag mixfrTag,
                         const Expr::Tag viscTag,
                         const double schmidtNumber,
                         const Expr::ExpressionID& id,
                         const Expr::ExpressionRegistry& reg );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag mixfrTag_, viscTag_;
    const double sc_;
  public:
    Builder( const Expr::Tag mixfrTag,
             const Expr::Tag viscTag,
             const double schmidtNumber )
      : mixfrTag_( mixfrTag ),
        viscTag_ ( viscTag  ),
        sc_( schmidtNumber )
    {}

    Expr::ExpressionBase* build( const Expr::ExpressionID& id,
				 const Expr::ExpressionRegistry& reg ) const
    {
      return new ScalarDissipationExpr<GradT,InterpT>( mixfrTag_, viscTag_, sc_, id, reg );
    }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

};





// ###################################################################
//
//                           Implementation
//
// ###################################################################





template< typename GradT, typename InterpT >
ScalarDissipationExpr<GradT,InterpT>::
ScalarDissipationExpr( const Expr::Tag mixfrTag,
                       const Expr::Tag viscTag,
                       const double schmidtNumber,
                       const Expr::ExpressionID& id,
                       const Expr::ExpressionRegistry& reg )
    : Expr::Expression<ScalarT>( id, reg ),
      schmidt_ ( schmidtNumber )
{
  this->set_gpu_runnable(true);

  mixfr_ = this->template create_field_request<ScalarT>( mixfrTag );
  visc_  = this->template create_field_request<ScalarT>( viscTag  );
}

//--------------------------------------------------------------------

template< typename GradT, typename InterpT >
void
ScalarDissipationExpr<GradT,InterpT>::
evaluate()
{
  using namespace SpatialOps;
  ScalarT& chi = this->value();
  chi <<= *visc_ / schmidt_ * (*interpOp_)( pow( (*gradOp_)(*mixfr_), 2.0 ) );
}

//--------------------------------------------------------------------

template< typename GradT, typename InterpT >
void
ScalarDissipationExpr<GradT,InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------


#endif // ScalarDissipationExpr_h
