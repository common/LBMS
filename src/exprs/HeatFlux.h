#ifndef HeatFlux_h
#define HeatFlux_h

#include <expression/ExprLib.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper

/**
 *  @class HeatFlux
 *  @author James C. Sutherland
 *  @date   September, 2008
 *  @brief Calculates the heat flux term for the internal energy,
 *         enthalpy, or temperature equations.
 *  @tparam ScalarT the type for the temperature, thermal conductivity,
 *          and species enthalpy fields.
 *  @tparam FluxT the type for the flux field(s).
 *
 *  The heat flux is given as
 * \f[
 *     \mathbf{q} = -\lambda \nabla T + \sum_{i=1}^{n_s} h_i \mathbf{j}_i,
 * \f]
 * <ul>

 *  <li> \f$ \mathbf{q}_r = -\lambda \nabla T\f$ is the reduced heat
 *       flux vector, comprised of the Fourier heat flux only.

 *  <li> The term \f$ \sum_{i=1}^{n_s} h_i \mathbf{j}_i \f$ is due to
 *       species diffusion.  If all species diffusivities are equal
 *       (as is assumed with mixture fraction based models) then this
 *       term is identically zero.
 *  </ul>
 *
 *  The internal energy and enthalpy equations use \f$\mathbf{q}\f$
 *  while the temperature equation uses \f$\mathbf{q}_r\f$.
 */
template< typename ScalarT,
          typename FluxT >
class HeatFlux : public Expr::Expression<FluxT>
{
public:

  /**
   *  @class HeatFlux::Builder
   *  @brief The mechanism for building a HeatFlux object.
   */
  class Builder : public Expr::ExpressionBuilder
  {
    const bool doSpecies_;
    const Expr::Tag tcT_, tT_;
    const Expr::TagList spfT_, hTags_;
  public:
    /**
     *  Build the HeatFlux expression with \f$\mathbf{q} = -\lambda \nabla T\f$.
     *
     *  @param result the heat flux tag
     *  @param thermCondTag the thermal conductivity tag
     *  @param tempTag the temperature tag
     */
    Builder( const Expr::Tag& result,
             const Expr::Tag& thermCondTag,
             const Expr::Tag& tempTag );

    /**
     *  Build the HeatFlux expression with \f$\mathbf{q} = -\lambda
     *  \nabla T + \sum_{i=1}^{n_s} h_i \mathbf{j}_i\f$.
     *
     *  @param result the heat flux tag
     *  @param thermCondTag the thermal conductivity tag
     *  @param tempTag the temperature tag
     *  @param specFluxTags the tags for the species diffusive fluxes
     *  @param hTags the tags for the species enthalpies
     */
    Builder( const Expr::Tag& result,
             const Expr::Tag& thermCondTag,
             const Expr::Tag& tempTag,
             const Expr::TagList& specFluxTags,
             const Expr::TagList& hTags );

    Expr::ExpressionBase* build() const;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

private:

  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Gradient,   ScalarT,FluxT>::type  GradT;
  typedef typename SpatialOps::OperatorTypeBuilder<SpatialOps::Interpolant,ScalarT,FluxT>::type  InterpT;

  typedef std::vector<const FluxT*  >   SpecFluxT;
  typedef std::vector<const ScalarT*>   SpecT;

  HeatFlux( const Expr::Tag& thermCondTag,
            const Expr::Tag& tempTag );

  HeatFlux( const Expr::Tag& thermCondTag,
            const Expr::Tag& tempTag,
            const Expr::TagList& specFluxTag,
            const Expr::TagList& hTags);

  const bool doSpecies_;
  const int nspec_;

  const GradT*   gradOp_;
  const InterpT* interpOp_;

  DECLARE_FIELDS( ScalarT, temp_, lambda_ )
  DECLARE_VECTOR_OF_FIELDS( FluxT, specFluxes_ )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, h_ )
};



// ###################################################################
//
//                            Implementation
//
// ###################################################################


template< typename ScalarT, typename FluxT >
HeatFlux<ScalarT,FluxT>::
HeatFlux( const Expr::Tag& thermCondTag,
          const Expr::Tag& tempTag )
  : Expr::Expression<FluxT>(),
    doSpecies_( false ),
    nspec_( 0 )
{
  temp_   = this->template create_field_request<ScalarT>( tempTag      );
  lambda_ = this->template create_field_request<ScalarT>( thermCondTag );
}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
HeatFlux<ScalarT,FluxT>::
HeatFlux( const Expr::Tag& thermCondTag,
          const Expr::Tag& tempTag,
          const Expr::TagList& specFluxTags,
          const Expr::TagList& hTags )
  : Expr::Expression<FluxT>(),
    doSpecies_( true ),
    nspec_( CanteraObjects::number_species() )
{
  assert( nspec_ == specFluxTags.size() && nspec_ == hTags.size() );

  temp_   = this->template create_field_request<ScalarT>( tempTag      );
  lambda_ = this->template create_field_request<ScalarT>( thermCondTag );

  this->template create_field_vector_request<ScalarT>( hTags,        h_          );
  this->template create_field_vector_request<FluxT  >( specFluxTags, specFluxes_ );
}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
void
HeatFlux<ScalarT,FluxT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
void
HeatFlux<ScalarT,FluxT>::
evaluate()
{
  using namespace SpatialOps;
  FluxT& heatFlux = this->value();

  const ScalarT& temp   = temp_  ->field_ref();
  const ScalarT& lambda = lambda_->field_ref();

  // q = -\lambda \nabla T,  W/m^2
  heatFlux <<= - (*interpOp_)(lambda) * (*gradOp_)(temp);

  if( doSpecies_ ){ //     add in the term $\sum_{i=1}^{n_s} h_i \mathbf{j}_i$
    for( size_t n=0; n<nspec_; ++n ){
      const FluxT& spFlux = specFluxes_[n]->field_ref();
      const ScalarT& specEnth = h_[n]->field_ref();
      heatFlux <<= heatFlux + spFlux * (*interpOp_)(specEnth);
    }
  }
}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
HeatFlux<ScalarT,FluxT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& thermCondTag,
         const Expr::Tag& tempTag )
  : ExpressionBuilder(result),
    doSpecies_( false ),
    tcT_( thermCondTag ),
    tT_ ( tempTag      )
{}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
HeatFlux<ScalarT,FluxT>::Builder::
Builder( const Expr::Tag& result,
         const Expr::Tag& thermCondTag,
         const Expr::Tag& tempTag,
         const Expr::TagList& specFluxTags,
         const Expr::TagList& hTags )
  : ExpressionBuilder(result),
    doSpecies_( true ),
    tcT_  ( thermCondTag ),
    tT_   ( tempTag      ),
    spfT_ ( specFluxTags ),
    hTags_( hTags        )
{}

//--------------------------------------------------------------------

template< typename ScalarT, typename FluxT >
Expr::ExpressionBase*
HeatFlux<ScalarT,FluxT>::Builder::build() const
{
  if( doSpecies_ ) return new HeatFlux<ScalarT,FluxT>( tcT_, tT_, spfT_, hTags_ );
  return new HeatFlux<ScalarT,FluxT>( tcT_, tT_ );
}

//--------------------------------------------------------------------

#endif // HeatFlux_h
