#ifndef Dilatation_h
#define Dilatation_h

#include <expression/Expression.h>

template< typename Div1T,
          typename Div2T,
          typename Div3T >
class Dilatation
  : public Expr::Expression< typename Div1T::DestFieldType >
{
  typedef typename Div1T::DestFieldType  DilT;
  typedef typename Div1T::SrcFieldType   Vel1T;
  typedef typename Div2T::SrcFieldType   Vel2T;
  typedef typename Div3T::SrcFieldType   Vel3T;

  const bool do1_, do2_, do3_;

  DECLARE_FIELD( Vel1T, vel1_ )
  DECLARE_FIELD( Vel2T, vel2_ )
  DECLARE_FIELD( Vel3T, vel3_ )

  const Div1T* xDivOp_;
  const Div2T* yDivOp_;
  const Div3T* zDivOp_;

  Dilatation( const Expr::Tag& vel1Tag,
              const Expr::Tag& vel2Tag,
              const Expr::Tag& vel3Tag );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag vel1T_, vel2T_, vel3T_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& vel1Tag,
             const Expr::Tag& vel2Tag = Expr::Tag(),
             const Expr::Tag& vel3Tag = Expr::Tag() );

    Expr::ExpressionBase* build() const;
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

};

//====================================================================





// ###################################################################
//
//                           Implementation
//
// ###################################################################





//====================================================================

template< typename Div1T, typename Div2T, typename Div3T >
Dilatation<Div1T,Div2T,Div3T>::
Dilatation( const Expr::Tag& vel1Tag,
            const Expr::Tag& vel2Tag,
            const Expr::Tag& vel3Tag )
  : Expr::Expression<typename Div1T::DestFieldType>(),

    do1_( vel1Tag != Expr::Tag() ),
    do2_( vel2Tag != Expr::Tag() ),
    do3_( vel3Tag != Expr::Tag() )
{
  this->set_gpu_runnable(true);

  if( do1_ )  vel1_ = this->template create_field_request<Vel1T>( vel1Tag );
  if( do2_ )  vel2_ = this->template create_field_request<Vel2T>( vel2Tag );
  if( do3_ )  vel3_ = this->template create_field_request<Vel3T>( vel3Tag );
}

//--------------------------------------------------------------------

template< typename Div1T, typename Div2T, typename Div3T >
void
Dilatation<Div1T,Div2T,Div3T>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  if(do1_) xDivOp_ = opDB.retrieve_operator<Div1T>();
  if(do2_) yDivOp_ = opDB.retrieve_operator<Div2T>();
  if(do3_) zDivOp_ = opDB.retrieve_operator<Div3T>();
}

//--------------------------------------------------------------------

template< typename Div1T, typename Div2T, typename Div3T >
void
Dilatation<Div1T,Div2T,Div3T>::
evaluate()
{
  using namespace SpatialOps;
  DilT& dil = this->value();
  if( do1_ ){ dil <<=       (*xDivOp_)( vel1_->field_ref() ); }
  else      { dil <<= 0.0;                                    }
  if( do2_ ){ dil <<= dil + (*yDivOp_)( vel2_->field_ref() ); }
  if( do3_ ){ dil <<= dil + (*zDivOp_)( vel3_->field_ref() ); }
}

//--------------------------------------------------------------------

template< typename Div1T, typename Div2T, typename Div3T >
Dilatation<Div1T,Div2T,Div3T>::Builder::
Builder( const Expr::Tag& resultTag,
         const Expr::Tag& vel1Tag,
         const Expr::Tag& vel2Tag,
         const Expr::Tag& vel3Tag )
  : ExpressionBuilder( resultTag ),
    vel1T_( vel1Tag ),
    vel2T_( vel2Tag ),
    vel3T_( vel3Tag )
{}

//--------------------------------------------------------------------

template< typename Div1T, typename Div2T, typename Div3T >
Expr::ExpressionBase*
Dilatation<Div1T,Div2T,Div3T>::Builder::
build() const
{
  return new Dilatation<Div1T,Div2T,Div3T>(vel1T_,vel2T_,vel3T_);
}

//--------------------------------------------------------------------

#endif
