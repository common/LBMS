/*
 * The MIT License
 *
 * Copyright (c) 2015 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef DensityWeight_Expr_h
#define DensityWeight_Expr_h

#include <expression/Expression.h>

/**
 *  \class DensityWeight
 *  \brief Computes \f$\rho\phi\f$ from \f$\rho\f$ and \f$\phi\f$.
 */
template< typename FieldT >
class DensityWeight
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, phi_, density_ )

  DensityWeight( const Expr::Tag& phiTag,
                 const Expr::Tag& densityTag )
    : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable( true );
    phi_ = this->template create_field_request<FieldT>( phiTag );
    density_  = this->template create_field_request<FieldT>( densityTag  );
  }

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag phiTag_, densityTag_;
  public:
    /**
     *  @brief Build a DensityWeight expression
     *  @param resultTag the tag for the density weighted quantity (\f$\rho\phi\f$)
     *  @param phiTag the tag for the original (unweighted) quantity (\f$\phi\f$)
     *  @param densityTag the density (\f$\rho\f$)
     *  @param nghost (optional) specify the number of ghosts to compute on
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& phiTag,
             const Expr::Tag& densityTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
     : ExpressionBuilder( resultTag, nghost ),
       phiTag_    ( phiTag     ),
       densityTag_( densityTag )
    {}

    Expr::ExpressionBase* build() const{
      return new DensityWeight<FieldT>( phiTag_, densityTag_ );
    }
  };

  void evaluate(){
    this->value() <<= density_->field_ref() * phi_->field_ref();
  }
};

//--------------------------------------------------------------------

#endif // DensityWeight_Expr_h
