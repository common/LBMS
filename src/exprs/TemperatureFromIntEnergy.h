#ifndef TemperatureFromIntEnergy_h
#define TemperatureFromIntEnergy_h

#include <spatialops/structured/FieldHelper.h>  // write_matlab

#include <expression/Expression.h>

#include <vector>

//--- Cantera Headers ---//
#include <pokitt/CanteraObjects.h>
#include <cantera/IdealGasMix.h>

/**
 *  @class TemperatureFromE0
 *  @brief Calculate temperature given internal energy, composition, and kinetic energy.
 */
template< typename FieldT >
class TemperatureFromE0
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, e0_, ke_, rho_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, species_ )

  typedef typename FieldT::iterator       FieldIter;
  typedef typename FieldT::const_iterator ConstFieldIter;

  std::vector<double> ptSpec_;
  std::vector<ConstFieldIter> specIVec_;

  TemperatureFromE0( const Expr::Tag& e0Tag,
                     const Expr::Tag& keTag,
                     const Expr::Tag& rhoTag,
                     const Expr::TagList& yiTag );

public:

  ~TemperatureFromE0(){}

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag e0Tag_, keTag_, rhoTag_;
    const Expr::TagList yiTag_;
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag e0Tag,
             const Expr::Tag keTag,
             const Expr::Tag rhoTag,
             const Expr::TagList& yiTag )
      : ExpressionBuilder( resultTag),
        e0Tag_  ( e0Tag  ),
        keTag_  ( keTag  ),
        rhoTag_ ( rhoTag ),
        yiTag_  ( yiTag  )
    {}

    Expr::ExpressionBase* build() const{
      return new TemperatureFromE0<FieldT>( e0Tag_, keTag_, rhoTag_, yiTag_ );
    }
  };

  void evaluate();

};





// ###################################################################
//
//                           Implementation
//
// ###################################################################





template< typename FieldT>
TemperatureFromE0<FieldT>::
TemperatureFromE0( const Expr::Tag& e0Tag,
                   const Expr::Tag& keTag,
                   const Expr::Tag& rhoTag, 
                   const Expr::TagList& yiTag )
  : Expr::Expression<FieldT>()
{
  e0_   = this->template create_field_request<FieldT>( e0Tag  );
  ke_   = this->template create_field_request<FieldT>( keTag  );
  rho_  = this->template create_field_request<FieldT>( rhoTag );

  this->template create_field_vector_request<FieldT>( yiTag, species_ );
}

//--------------------------------------------------------------------

template< typename FieldT>
void
TemperatureFromE0<FieldT>::
evaluate()
{
  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();
  const int nspec = gas->nSpecies();
  ptSpec_.resize(nspec,0.0);

  FieldT& temp = this->value();

  FieldIter itemp = temp.interior_begin();
  const FieldIter itempe = temp.interior_end();

  ConstFieldIter ie0  = e0_  ->field_ref().interior_begin();
  ConstFieldIter ike  = ke_  ->field_ref().interior_begin();
  ConstFieldIter irho = rho_ ->field_ref().interior_begin();

  specIVec_.clear();
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec_.push_back( species_[i]->field_ref().interior_begin() );
  }

  // calculate temperature at each point
  for( ; itemp!=itempe; ++itemp, ++ie0, ++ike, ++irho ){

    // extract composition and pack it into a temporary buffer.
    typedef typename std::vector<ConstFieldIter>::iterator SpIter;
    SpIter isp=specIVec_.begin();
    const SpIter ispe=specIVec_.end();
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( ; isp!=ispe; ++ipt, ++isp ){
      *ipt = **isp;
    }

    // calculate the internal energy from the total internal energy
    const double intEnerg = *ie0 - *ike;

    // calculate the temperature from composition, internal energy, and pressure:
    try{
      // pressure doesn't matter here...
      gas->setState_TPY( *itemp, 101325, &ptSpec_[0] );
      gas->setState_UV( intEnerg, 1.0/(*irho), 1e-8 );
      *itemp = gas->temperature();
    }
    catch( Cantera::CanteraError& ){
      std::cout << "==================================================" << std::endl;
      std::cout << "Error in Temperature solve" << std::endl
                << "  E0 = " << *ie0 << std::endl
                << "  Rho = " << *irho << std::endl
                << "  KE = " << *ike << std::endl
                << "  e = " << intEnerg << std::endl
                << "  Tguess = " << *itemp << std::endl
                << "  Yi = ";
      double ysum=0;
      for( int i=0; i<nspec; ++i ){
	std::cout << ptSpec_[i] << " ";
	ysum += ptSpec_[i];
      }
      std::cout << std::endl << "  sum Yi = " << ysum << std::endl;

      std::cout << "--------------------------------------------------" << std::endl
                << "Locations in Temperature solve" << std::endl
                << "  E0 = " << ie0.location() << std::endl
                << "  Rho = " << irho.location() << std::endl
                << "  KE = " << ike.location() << std::endl
                << "  Yi = " ;

      for( isp=specIVec_.begin(); isp!=ispe; ++isp ){
        std::cout << (*isp).location() << ", ";
      }
      std::cout << std::endl << "==================================================" << std::endl;
      std::cout << std::endl;

      SpatialOps::write_matlab( temp, "T_fail" );
      SpatialOps::write_matlab( ke_->field_ref(), "ke_fail");
      SpatialOps::write_matlab( e0_->field_ref(), "e0_fail");
      for( size_t i=0; i<species_.size(); ++i ){
	const std::string spname2 = gas->speciesName(i) + "_fail";
        SpatialOps::write_matlab( species_[i]->field_ref(), spname2 );
      }

      Cantera::showErrors();

      throw std::runtime_error("Problems in Temperature expression.");
    }

    // increment iterators for species to the next point
    for( isp=specIVec_.begin(); isp!=ispe; ++isp ){
      ++(*isp);
    }
  }
  CanteraObjects::restore_gasmix( gas );
}

//--------------------------------------------------------------------

#endif
