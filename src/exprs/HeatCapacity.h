#ifndef HeatCapacity_h
#define HeatCapacity_h

#include <expression/ExprLib.h>

#include <pokitt/CanteraObjects.h> // include cantera wrapper
#include <cantera/transport.h>

//====================================================================

/**
 *  @class HeatCapacity
 *  @author Derek Cline
 *
 *  @brief Calculates the HeatCapacity(C_P) from Cantera using
 *  kinetic theory.  Units are J/kg/K.
 */
template< typename VolFieldTT >
class HeatCapacity
  : public Expr::Expression< VolFieldT >
{
    DECLARE_FIELDS( FieldT, cp_, temperature_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, yi_ )

    typedef typename FieldT::iterator       FieldIter;
    typedef typename FieldT::const_iterator ConstFieldIter;

    std::vector<double> ptSpec_;
    std::vector<ConstFieldIter> specIVec_;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag tT_, yiT_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& temperatureTag,
             const Expr::TagList& yiTag )
      : ExpressionBuilder(result),
        tT_ ( temperatureTag ),
        yiT_( yiTag          )
    {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{
      return new HeatCapacity( tT_, yiT_ );
    }
  };

  void evaluate();

protected:

  HeatCapacity( const Expr::Tag& temperatureTag,
                const Expr::TagList& yiTag );

  ~HeatCapacity(){}

  const Expr::Tag temperatureTag_;
  const Expr::TagList yiTag_;
};



//--------------------------Implementation---------------------------

HeatCapacity::HeatCapacity( const Expr::Tag& temperatureTag,
                            const Expr::TagList& yiTag )

  : Expr::Expression<FieldT>()
{
  temperature_ = this->template create_field_request<FieldT>( temperatureTag );
  this->template create_field_vector_request<FieldT>( yiTag, species_ );
}

//--------------------------------------------------------------------

void
HeatCapacity::evaluate()
{
  FieldT& cp = this->value();

  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();
  const int nspec = gas->nSpecies();
  ptSpec_.resize(nspec,0.0);

  FieldIter icp = cp.interior_begin();
  const FieldIter icpe = cp.interior_end();

  ConstFieldIter itemp  = temperature_->field_ref().interior_begin();

  const double press = gas->pressure();

  // loop over points to calculate heat capacity
  for( ; icp!=icpe; ++itemp, ++icp ){

    // extract composition and pack it into a temporary buffer.
    typedef typename std::vector<ConstFieldIter>::iterator SpIter;
    SpIter isp=specIVec_.begin();
    const SpIter ispe=specIVec_.end();
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( ; isp!=ispe; ++ipt, ++isp ){
      *ipt = **isp;
    }

    // calculate the heat capacity
    try{
      gas->setState_TPY( *itemp, press, &ptSpec_[0] );
      *icp = gas->cp_mass();
    }
    catch( Cantera::CanteraError ){
      cout << "Error in Heatcapacity" << std::endl
           << " T=" << *itemp << ", p=" << press << std::endl
           << " Yi=";
      double ysum=0;
      for( int i=0; i<nspec_; ++i ){
        std::cout << ptSpec_[i] << " ";
        ysum += ptSpec_[i];
      }
      cout << std::endl << "sum Yi = " << ysum << std::endl;
      throw std::runtime_error("Problems in Heatcapacity expression.");
    }

  } // loop over grid points

}

//--------------------------------------------------------------------


/**
 *  @class HeatCapacity_Cv
 *  @author Derek Cline
 *
 *  @brief Calculates the HeatCapacity_Cv(C_v) from Cantera using
 *  kinetic theory.  Units are J/kg/K.
 */
class HeatCapacity_Cv
: public Expr::Expression< VolFieldT >
{
  DECLARE_FIELDS( FieldT, cv_, temperature_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, yi_ )

  typedef typename FieldT::iterator       FieldIter;
  typedef typename FieldT::const_iterator ConstFieldIter;

  std::vector<double> ptSpec_;
  std::vector<ConstFieldIter> specIVec_;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag tT_, yiT_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& temperatureTag,
             const Expr::TagList& yiTag )
    : ExpressionBuilder(result),
      tT_ ( temperatureTag ),
      yiT_( yiTag          )
    {}

    Expr::ExpressionBase* build() const{
      return new HeatCapacity_Cv( tT_, yiT_ );
    }
  };

  void evaluate();
  void bind_fields( const Expr::FieldManagerList& fml );

  protected:

  HeatCapacity_Cv( const Expr::Tag& temperatureTag,
                   const Expr::TagList& yiTag );

  ~HeatCapacity_Cv(){}

  const Expr::Tag temperatureTag_;
  const Expr::TagList yiTag_;
};



//--------------------------Implementation---------------------------

HeatCapacity_Cv::HeatCapacity_Cv( const Expr::Tag& temperatureTag,
                                  const Expr::TagList& yiTag )
  : Expr::Expression<FieldT>()
{
  temperature_ = this->template create_field_request<FieldT>( temperatureTag );

  this->template create_field_vector_request<FieldT>( yiTag, species_ );
}

//--------------------------------------------------------------------

void
HeatCapacity_Cv::evaluate()
{
  FieldT& cv = this->value();

  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();
  const int nspec = gas->nSpecies();
  ptSpec_.resize(nspec,0.0);

  FieldIter icv = cv.interior_begin();
  const FieldIter icve = cv.interior_end();

  ConstFieldIter itemp  = temperature_->field_ref().interior_begin();

  const double press = gas->pressure();

  // loop over points to calculate heat capacity
  for( ; icv!=icve; ++itemp, ++icv ){

    // extract composition and pack it into a temporary buffer.
    typedef typename std::vector<ConstFieldIter>::iterator SpIter;
    SpIter isp=specIVec_.begin();
    const SpIter ispe=specIVec_.end();
    std::vector<double>::iterator ipt = ptSpec_.begin();
    for( ; isp!=ispe; ++ipt, ++isp ){
      *ipt = **isp;
    }

    // calculate the heat capacity
    try{
      gas->setState_TPY( *itemp, press, &ptSpec_[0] );
      *icv = gas->cv_mass();
    }
    catch( Cantera::CanteraError ){
      cout << "Error in Heatcapacity" << std::endl
           << " T=" << *itemp << ", p=" << press << std::endl
           << " Yi=";
      double ysum=0;
      for( int i=0; i<nspec_; ++i ){
        std::cout << ptSpec_[i] << " ";
        ysum += ptSpec_[i];
      }
      cout << std::endl << "sum Yi = " << ysum << std::endl;
      throw std::runtime_error("Problems in Heatcapacity expression.");
    }

  } // loop over grid points


}

//--------------------------------------------------------------------

#endif
