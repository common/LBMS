/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef GhostRhoPhi_h
#define GhostRhoPhi_h

#include <boost/foreach.hpp>

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>

namespace LBMS{

  /**
   *  \class GhostRhoPhi
   *  \author Derek Cline
   *  \date   Feb, 2016
   *
   *  \brief Multiplies a value by density and then assigns it to the ghost cells
   *         for use in a hard inflow boundary condition
   *
   *  \param mask the mask that covers points interior to what will be assigned
   *  \param dir the direction normal to the boundary, used to shift the mask to the ghost cells
   *  \param side plus for the extent and minus for the origin, used to shift the mask to the ghost cells
   */

  template< typename FieldT >
  class GhostRhoPhi : public Expr::Expression<FieldT>
  {
  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const SpatialOps::SpatialMask<FieldT> mask_;
      const LBMS::Direction dir_;
      const SpatialOps::BCSide side_;
      const Expr::Tag rhoTag_;
      const Expr::Tag phiTag_;
    public:

      Builder( const Expr::Tag& result,
               const SpatialOps::SpatialMask<FieldT>& mask,
               const LBMS::Direction& dir,
               const SpatialOps::BCSide& side,
               const Expr::Tag& rhoTag,
               const Expr::Tag& phiTag );

      Expr::ExpressionBase* build() const;
    };

    void evaluate();
  private:
    GhostRhoPhi( const SpatialOps::SpatialMask<FieldT>& mask,
                 const LBMS::Direction& dir,
                 const SpatialOps::BCSide& side,
                 const Expr::Tag& rhoTag,
                 const Expr::Tag& phiTag );

    const SpatialOps::SpatialMask<FieldT> mask_;
    const LBMS::Direction dir_;
    const SpatialOps::BCSide side_;

    const Expr::Tag rhoTag_;
    const Expr::Tag phiTag_;

    SpatialOps::SpatialMask<FieldT>* shiftedMask_;

    DECLARE_FIELDS( FieldT, rho_, phi_ )
  };

  // ###################################################################
  //
  //                            Implementation
  //
  // ###################################################################


  template< typename FieldT >
  GhostRhoPhi<FieldT>::GhostRhoPhi( const SpatialOps::SpatialMask<FieldT>& mask,
                                    const LBMS::Direction& dir,
                                    const SpatialOps::BCSide& side,
                                    const Expr::Tag& rhoTag,
                                    const Expr::Tag& phiTag )
    : Expr::Expression<FieldT>(),
      mask_  ( mask   ),
      dir_   ( dir    ),
      side_  ( side   ),
      rhoTag_( rhoTag ),
      phiTag_( phiTag )
  {
    rho_ = this->template create_field_request<FieldT>( rhoTag );
    phi_ = this->template create_field_request<FieldT>( phiTag );

    //This will shift the mask to the appropriate cell for extrapolation.
    SpatialOps::IntVec shift(0,0,0);

    switch( dir ){
      case SpatialOps::XDIR::value: shift = shift + SpatialOps::IntVec(1,0,0); break;
      case SpatialOps::YDIR::value: shift = shift + SpatialOps::IntVec(0,1,0); break;
      case SpatialOps::ZDIR::value: shift = shift + SpatialOps::IntVec(0,0,1); break;
      case SpatialOps::NODIR::value:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "Invalid direction in Extrapolation expression" << std::endl;
        throw std::runtime_error( msg.str() );
        break;
    }

    if( side == SpatialOps::MINUS_SIDE ) shift = shift * SpatialOps::IntVec( -1,-1,-1 );

    std::vector<SpatialOps::IntVec> tempVec( (mask.points()).size() );

    size_t i=0;
    BOOST_FOREACH( const SpatialOps::IntVec& v, mask.points() ){
      tempVec[i++] = v + shift;
    }

    //Essentially copies everything from the previous mask but
    //uses the shifted points instead.
    shiftedMask_ = new SpatialOps::SpatialMask<FieldT>( mask.window_with_ghost(),
                                                        mask.boundary_info(),
                                                        mask.get_ghost_data(),
                                                        tempVec );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void GhostRhoPhi<FieldT>::evaluate()
  {
    using namespace SpatialOps;

    const FieldT& rho = rho_->field_ref();
    const FieldT& phi = phi_->field_ref();

    FieldT& rhoPhi = this->value();

    masked_assign( *shiftedMask_, rhoPhi, rho * phi );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  GhostRhoPhi<FieldT>::Builder::
  Builder( const Expr::Tag& result,
           const SpatialOps::SpatialMask<FieldT>& mask,
           const LBMS::Direction& dir,
           const SpatialOps::BCSide& side,
           const Expr::Tag& rhoTag,
           const Expr::Tag& phiTag )
    : ExpressionBuilder(result),
      mask_  ( mask   ),
      dir_   ( dir    ),
      side_  ( side   ),
      rhoTag_( rhoTag ),
      phiTag_( phiTag )
  {}

  //--------------------------------------------------------------------

  template< typename FieldT >
  Expr::ExpressionBase*
  GhostRhoPhi<FieldT>::Builder::build() const
  { return new GhostRhoPhi<FieldT>( mask_, dir_, side_, rhoTag_, phiTag_ ); }

}//end namespace

#endif // GhostRhoPhi_h

