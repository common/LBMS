#ifndef Pressure_h
#define Pressure_h

#include <expression/Expression.h>


//====================================================================

template< typename InterpT >
class PressureInterp
  : public Expr::Expression<typename InterpT::DestFieldType>
{
  typedef typename InterpT::DestFieldType  DestT;
  typedef typename InterpT::SrcFieldType   SrcT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pressT_;
  public:
    Builder( const Expr::Tag& result, const Expr::Tag pressureTag )
    : ExpressionBuilder(result), pressT_( pressureTag ) {}
    Expr::ExpressionBase* build() const
    { return new PressureInterp<InterpT>(pressT_); }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  PressureInterp( const Expr::Tag pressTag );

  DECLARE_FIELD( SrcT, psrc_ )
  const InterpT* interpOp_;
};


//====================================================================


template< typename GradT >
class PressureGradient
  : public Expr::Expression<typename GradT::DestFieldType>
{
  typedef typename GradT::DestFieldType  PGradT;
  typedef typename GradT::SrcFieldType   ScalarT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag pressT_;
    const bool flipSign_;
  public:
    Builder( const Expr::Tag& result,
             const Expr::Tag& pressureTag,
             const bool flipSign=false )
    : ExpressionBuilder(result),
      pressT_( pressureTag ),
      flipSign_(flipSign)
    {}
    Expr::ExpressionBase* build() const{ return new PressureGradient<GradT>(pressT_,flipSign_); }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  PressureGradient( const Expr::Tag pressTag,
                    const bool flipSign );

  const bool flipSign_;
  DECLARE_FIELD( ScalarT, pressure_ )
  const GradT* gradOp_;

};


//====================================================================


template< typename DivT >
class PressureRHSTerm
  : public Expr::Expression<typename DivT::DestFieldType>
{
  typedef typename DivT::DestFieldType CellT;
  typedef typename DivT::SrcFieldType  FaceT;

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag facepressT_;
  public:
    Builder( const Expr::Tag& result, const Expr::Tag& facepressureTag )
      : ExpressionBuilder(result),
        facepressT_( facepressureTag )
    {}
    Expr::ExpressionBase* build() const{ return new PressureRHSTerm<DivT>(facepressT_); }
  };

  void evaluate();
  void bind_operators( const SpatialOps::OperatorDatabase& opDB );

protected:

  PressureRHSTerm( const Expr::Tag& facepressureTag );

  DECLARE_FIELD( CellT, pressgrad_ )
  DECLARE_FIELD( FaceT, facepress_ )
  const DivT*  divOp_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################


template< typename InterpT >
PressureInterp<InterpT>::
PressureInterp( const Expr::Tag pressTag )
  : Expr::Expression<DestT>()
{
  psrc_ = this->template create_field_request<SrcT >( pressTag );
}

//--------------------------------------------------------------------

template< typename InterpT >
void
PressureInterp<InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename InterpT >
void
PressureInterp<InterpT>::
evaluate()
{
  DestT& pdest = this->value();
  pdest <<= (*interpOp_)( psrc_->field_ref() );
}

//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------

template< typename GradT >
PressureGradient<GradT>::
PressureGradient( const Expr::Tag pressTag,
                  const bool flipSign )
  : Expr::Expression<PGradT>(),
    flipSign_( flipSign )
{
  this->set_gpu_runnable(true);
  pressure_ = this->template create_field_request<ScalarT>( pressTag );
}

//--------------------------------------------------------------------

template< typename GradT >
void
PressureGradient<GradT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_ = opDB.retrieve_operator<GradT>();
}

//--------------------------------------------------------------------

template< typename GradT >
void
PressureGradient<GradT>::
evaluate()
{
  using namespace SpatialOps;
  PGradT& gradp = this->value();
  const ScalarT& pressure = pressure_->field_ref();
  if( flipSign_ ) gradp <<= -(*gradOp_)(pressure);
  else            gradp <<=  (*gradOp_)(pressure);
}

//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------

template<typename DivT >
PressureRHSTerm<DivT>::
PressureRHSTerm( const Expr::Tag& facepressureTag )
  : Expr::Expression<CellT>()
{
  this->set_gpu_runnable(true);
  facepress_ = this->template create_field_request<FaceT>( facepressureTag );
}

//--------------------------------------------------------------------

template<typename DivT >
void
PressureRHSTerm<DivT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();
}

//--------------------------------------------------------------------

template<typename DivT >
void
PressureRHSTerm<DivT>::
evaluate()
{
  using namespace SpatialOps;
  CellT& momPTerm = this->value();
  momPTerm <<= -(*divOp_)(facepress_->field_ref());
}

//--------------------------------------------------------------------

#endif // Pressure_h
