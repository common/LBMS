/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "DiffusiveFlux.h"

//-- ExprLib includes --//
#include <expression/ExprLib.h>

//-- SpatialOps includes --//
#include <spatialops/OperatorDatabase.h>

template< typename FluxT >
DiffusiveFlux<FluxT>::DiffusiveFlux( const Expr::Tag& rhoTag,
                                     const Expr::Tag& phiTag,
                                     const Expr::Tag& coefTag )
  : Expr::Expression<FluxT>(),
    isConstCoef_( false ),
    coefVal_( 0.0 )
{
  this->set_gpu_runnable(true);

  phi_  = this->template create_field_request<ScalarT>( phiTag  );
  rho_  = this->template create_field_request<ScalarT>( rhoTag  );
  coef_ = this->template create_field_request<FluxT  >( coefTag );
}

//--------------------------------------------------------------------

template< typename FluxT >
DiffusiveFlux<FluxT>::DiffusiveFlux( const Expr::Tag& rhoTag,
                                     const Expr::Tag& phiTag,
                                     const double coef )
  : Expr::Expression<FluxT>(),
    isConstCoef_( true  ),
    coefVal_( coef )
{
  this->set_gpu_runnable(true);

  phi_ = this->template create_field_request<ScalarT>( phiTag );
  rho_ = this->template create_field_request<ScalarT>( rhoTag );
}

//--------------------------------------------------------------------

template< typename FluxT >
void
DiffusiveFlux<FluxT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename FluxT >
void
DiffusiveFlux<FluxT>::
evaluate()
{
  using namespace SpatialOps;
  FluxT& result = this->value();

  const ScalarT& phi = phi_->field_ref();
  const ScalarT& rho = rho_->field_ref();

  if( isConstCoef_ ){
    result <<= - (*interpOp_)(rho) * coefVal_ * (*gradOp_)(phi);
  }
  else{
    const FluxT& coef = coef_->field_ref();
    result <<= - (*interpOp_)(rho) * coef * (*gradOp_)(phi);
  }
}


//====================================================================


template< typename FluxT >
DiffusiveFlux2<FluxT>::
DiffusiveFlux2( const Expr::Tag& rhoTag,
                const Expr::Tag& phiTag,
                const Expr::Tag& coefTag )
  : Expr::Expression<FluxT>()
{
  this->set_gpu_runnable(true);

  rho_  = this->template create_field_request<ScalarT>( rhoTag  );
  phi_  = this->template create_field_request<ScalarT>( phiTag  );
  coef_ = this->template create_field_request<ScalarT>( coefTag );
}

//--------------------------------------------------------------------

template< typename FluxT >
void
DiffusiveFlux2<FluxT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  gradOp_   = opDB.retrieve_operator<GradT  >();
  interpOp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename FluxT >
void
DiffusiveFlux2<FluxT>::
evaluate()
{
  using namespace SpatialOps;
  const ScalarT rho  = rho_ ->field_ref();
  const ScalarT phi  = phi_ ->field_ref();
  const ScalarT coef = coef_->field_ref();
  this->value() <<= - (*interpOp_)( rho * coef ) * (*gradOp_)(phi);
}

//--------------------------------------------------------------------


//==========================================================================
// Explicit template instantiation for supported versions of this expression
//
#include <fields/Fields.h>
#include <operators/Operators.h>
#define DECLARE_DIFF_FLUX( VOL )                                      \
  template class DiffusiveFlux < SpatialOps::FaceTypes<VOL>::XFace >; \
  template class DiffusiveFlux < SpatialOps::FaceTypes<VOL>::YFace >; \
  template class DiffusiveFlux < SpatialOps::FaceTypes<VOL>::ZFace >; \
  template class DiffusiveFlux2< SpatialOps::FaceTypes<VOL>::XFace >; \
  template class DiffusiveFlux2< SpatialOps::FaceTypes<VOL>::YFace >; \
  template class DiffusiveFlux2< SpatialOps::FaceTypes<VOL>::ZFace >;
DECLARE_DIFF_FLUX( LBMS::XVolField );
DECLARE_DIFF_FLUX( LBMS::YVolField );
DECLARE_DIFF_FLUX( LBMS::ZVolField );
//
//==========================================================================
