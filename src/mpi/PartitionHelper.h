/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file PartitionHelper.h
 * \date Sep 17, 2013
 * \author John Hutchins
 */

#ifndef HELPER_H_
#define HELPER_H_
#include <spatialops/structured/Numeric3Vec.h>
#include <spatialops/structured/IntVec.h>

#include <mpi/Sieve.h>
#include <cmath>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <set>
#include <stdexcept>
#include <sstream>
#include <utility>

#include <boost/ref.hpp>

namespace LBMS {

  /**
   * \typedef ProcVec
   * \brief Simple way of expressing a collection of 3 size_t numbers
   */
  typedef SpatialOps::Numeric3Vec<size_t> ProcVec;

  /**
   * @brief checks a set to see if it contains the object.
   */
  template<typename T>
  inline bool contains(const std::set<T> s, T c)
  {
    return s.find(c) != s.end();
  }

  /**
   *  \ingroup Comm
   *  \class PartitionHelper
   *  \brief Given a number of processors and the coarse and fine points finds a valid process assignment
   *
   *  The PartitionHelper calculates the weighting given the fine and coarse grid points and determines
   *  how many processes should be assigned to each direction.
   */
  class PartitionHelper{
    typedef  SpatialOps::Numeric3Vec<float> WeightVec;

    const SpatialOps::IntVec nCoarse_;
    const SpatialOps::IntVec nFine_;
    const ProcVec maxs_;
    const size_t maxx_, maxy_, maxz_;
    const ProcVec maxDir_;
    const size_t maxxproc_, maxyproc_, maxzproc_;
    const ProcVec maxTwoDir_;
    const size_t maxproc_;
    WeightVec loading_;

    /**
     * @brief gets the next valid number from the set, above the in value.
     *
     * @param in        the value to be tested for
     * @param set       the set of possible numbers
     *
     * @return the next valid number found in the set that is greater (or equal to) in
     */
    size_t nearest_valid( const size_t in, const std::set<size_t> & set) const;

    /**
     *  @brief returns a valid processor assignment, given the maxs and a trial number
     *
     *  This distributes the processes to the directions so that each direction has a valid number of processes assigned to it;
     * which can make the result greater than the number of processes. 
     *
     *  @param trial    the suggested process splitting given the weighting
     *  @param maxs     the max number of processes for each coarse direction
     */
    ProcVec proc_assignment( ProcVec trial, const ProcVec & maxs, const size_t nproc_ ) const;

    // no copying
    PartitionHelper& operator=( const PartitionHelper& );
    PartitionHelper( const PartitionHelper& );

  public:

    /**
     * @brief Sets up the partition helper and calculates the weights.
     *
     *  @param nCoarse  the number of coarse grid points in each direction
     *  @param nFine    the number of fine grid points in each direction
     */
    PartitionHelper( const SpatialOps::IntVec& nCoarse, const SpatialOps::IntVec& nFine );

    /**
     * @brief Gives the set of possible process numbers for a given direction
     *
     * @param num1      the maximum number of processes possible for one of the coarse direction
     * @param num2      the maximum number of processes possible for the other coarse direction
     *
     * @return a set containing all posssible processes counts for this direction
     *
     */
    std::set<size_t> get_valid_nums( const size_t num1, const size_t num2 ) const;



    /**
     * @brief The helper function which takes in the number of processors, the number of coarse grid points, and the number of fine grid points
     * It turns these into a size_t 3 vec which is the processor counts for x,y,z.
     *
     *  @param nproc_   the process count to be checked for
     *  @param output   whether to output the distribution of the process
     *
     *  @return the division of the processes given the weighting and nproc_.
     */

    ProcVec check_proc_number( const size_t nproc_, const bool output = false ) const;
  };
}

#endif /* HELPER_H_ */
