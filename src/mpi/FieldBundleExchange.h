/* Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   FieldBundleExchange.h
 * \date   May 16, 2012
 *  \author James C. Sutherland
 */

#ifndef FIELDBUNDLEEXCHANGE_H_
#define FIELDBUNDLEEXCHANGE_H_

#include <lbms/Bundle_fwd.h>
#include <lbms/Direction.h>
#include <expression/ExprFwd.h>
#include <expression/Tag.h>
#include <expression/Poller.h>

#ifdef HAVE_MPI
#include <boost/mpi/request.hpp>
#endif

#include <spatialops/structured/IntVec.h>

namespace LBMS {

  /**
   * @brief sets up everything required to communicate ghost cells
   * @param factory the factory holding the expression that computes this field
   * @param b the bundle for the field
   * @param tag the field tag
   * @param f  the face of interest
   * @param destProcID the process ID for the destination
   */
  template<typename FieldT>
  void
  setup_ghost_exchange( Expr::ExpressionFactory& factory,
                        const BundlePtr b,
                        const Expr::Tag& tag,
                        const LBMS::Faces f,
                        const int destProcID );

  template<typename T> class FieldExchangeInfo; // forward


  /**
   *  \class  FieldBundleExchange
   *  \author James C. Sutherland
   *  \date   May 16, 2012
   *
   *  \brief Provides tools for exchanging data among fields.  This updates
   *         information between lines on a given bundle (ghost update).
   *  \ingroup Comm
   */
  template<typename FieldT>
  class FieldBundleExchange
  {
    const int xmID_, xpID_, ymID_, ypID_, zmID_, zpID_;
    const BundlePtr bundle_;
    const Expr::Tag fieldTag_;

  public:
    FieldBundleExchange( const BundlePtr bundle,
                         const Expr::Tag fieldTag,
                         Expr::ExpressionFactory& factory );
    ~FieldBundleExchange();
  };

  /**
   * \ingroup Comm
   * \fn  template<typename FieldT> void communicate_expression_result( const Expr::Tag&, const int, const BundlePtr, Expr::ExpressionFactory& );
   *
   * \brief request communication for the result of an expression to update
   *        ghost cells with neighboring lines in a bundle.

   * \param tag the identifier for the expression
   * \param bundle the bundle
   * \param factory the factory where the expression lives.
   *
   * \tparam FieldT the type of field that should be communicated.
   */
  template<typename FieldT>
  void
  communicate_expression_result( const Expr::Tag& tag,
                                 const BundlePtr bundle,
                                 Expr::ExpressionFactory& factory);


  /**
   * @brief Set up everything required to communicate a coarse field.
   * @param factory the ExpressionFactory used to register the expression
   * @param bundle the bundle that this field/expression is associated with
   * @param fieldTag the Tag identifying this expression
   * @param srcBundleDir the Direction of the source (originating) bundle
   * @param targetBundleDir the Direction of the destination (target) bundle
   * @ingroup Comm
   */
  template< typename FieldT >
  void
  communicate_coarse_expression_result( Expr::ExpressionFactory& factory,
                                        const BundlePtr bundle,
                                        const Expr::Tag& fieldTag,
                                        const LBMS::Direction srcBundleDir,
                                        const LBMS::Direction targetBundleDir );

  /**
   * \class CoarseFieldRecvHelper
   * \brief a helper class for receiving coarse fields from another bundle
   * \ingroup Comm
   * \tparam FieldT the type of coarse field of interest
   */
  template<typename FieldT>
  class CoarseFieldRecvHelper{
    const Expr::Tag fieldTag_;
    const Direction targetBundleDir_, srcBundleDir_;
    std::set<int> ids_;
    std::vector<FieldExchangeInfo<FieldT>*> info_;
    const Expr::Tag srcfieldTag_;
  public:
    CoarseFieldRecvHelper( const BundlePtr b, const Expr::Tag& t, const Direction srcBundleDir );
    ~CoarseFieldRecvHelper();
    /** \brief receive the field */
    void operator()( FieldT& field );
    /** \brief determine if the field is ready for receipt */
    bool is_ready( FieldT& field );
    static inline bool is_gpu_runnable(){ return false; }
  };

  /**
   * \class CoarseFieldSendHelper
   * \brief a helper class for sending coarse fields to other bundles
   * \ingroup Comm
   * \tparam FieldT the type of coarse field of interest
   */
  template<typename FieldT>
  class CoarseFieldSendHelper : public Expr::PollWorker{
    const BundlePtr bundle_;
    const Expr::Tag fieldTag_;
    const Direction dir_, dir1_, dir2_, srcBundleDir_, targetBundleDir_;
    std::set<int> ids1_, ids2_;
    std::vector<FieldExchangeInfo<FieldT>* > exchInfo1_, exchInfo2_;
    CoarseFieldSendHelper( const CoarseFieldSendHelper& ); // no copying
    CoarseFieldSendHelper& operator=( const CoarseFieldSendHelper& ); // no assignment
  public:
    /**
     * \param b the bundle
     * \param t the Tag for the field we are sending
     * \param srcBundleDir the direction associated with the bundle that the field we are sending originates from
     */
    CoarseFieldSendHelper( const BundlePtr b,
                           const Expr::Tag& t,
                           const LBMS::Direction srcBundleDir,
                           const LBMS::Direction targetBundleDir );
    ~CoarseFieldSendHelper();
    /** \brief send the field */
    void operator()( FieldT& field );
    bool operator()();
    static inline bool is_gpu_runnable(){ return false; }
  };

  /**
   * \class GhostFieldRecvHelper
   * \brief a helper class for receiving ghost cells
   * \ingroup Comm
   * \tparam FieldT the type of field that is being received
   */
  template<typename FieldT>
  class GhostFieldRecvHelper{
    const Expr::Tag fieldTag_;
    const Direction dir_;
    const LBMS::Faces face_;
    const int id_;
    FieldExchangeInfo<FieldT>* fInfo_;
    void set_fi( const FieldT& field );
  public:
    GhostFieldRecvHelper(const BundlePtr b, const Expr::Tag& t, const LBMS::Faces f, const int id);
    ~GhostFieldRecvHelper();
    /** \brief receive the field's ghost cells as appropriate */
    void operator()( FieldT& field );
    /** \brief query if a receipt is available */
    bool is_ready( FieldT& field );

    static inline bool is_gpu_runnable(){ return false; }
  };

  /**
   * \class GhostFieldSendHelper
   * \brief a helper class for sending ghost cells
   * \ingroup Comm
   * \tparam FieldT the type of field that is being sent
   *
   * This should be used as a non-blocking poll worker.  It manages the
   * nonblocking send buffers and ensures that the sends don't cause problem
   * with the MPI eager limit.
   */
  template<typename FieldT>
  class GhostFieldSendHelper : public Expr::PollWorker {
    FieldExchangeInfo<FieldT>* exchangeInfo_;
    const Expr::Tag fieldTag_;
    const Direction dir_;
    const LBMS::Faces face_;
    const int id_;
    const BundlePtr bundle_;
    GhostFieldSendHelper( const GhostFieldSendHelper& ); // no copy constructing
    GhostFieldSendHelper& operator=( const GhostFieldSendHelper& ); // no assignment
  public:
    GhostFieldSendHelper( const BundlePtr b,
                          const Expr::Tag& t,
                          const LBMS::Faces f,
                          const int id );
    ~GhostFieldSendHelper();
    /** \brief send the field's ghost cells as appropriate */
    void operator()( FieldT& field );

    // this interface is used for the poller
    bool operator()();

    static inline bool is_gpu_runnable(){ return false; }
  };

} /* namespace LBMS */

#endif /* FIELDBUNDLEEXCHANGE_H_ */
