/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 *  \file   FieldExchangeInfo.h
 *  \date   Aug 1, 2013
 *  \author "James C. Sutherland"
 */

#ifndef FIELDEXCHANGEINFO_H_
#define FIELDEXCHANGEINFO_H_

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/MemoryWindow.h>

#include <lbms/Direction.h>
#include <lbms/Coordinate.h>
#include <lbms/Bundle_fwd.h>

#include <mpi/Environment.h>
#include <limits>

#include <expression/Tag.h>

namespace LBMS{

  enum CommMode{ SEND, RECEIVE };

  unsigned long string_hash( const std::string& str );

  void clear_send_buffer();


  //Actually gets rid off all allocated communication buffers
  //Needed for the tests cases.
  void clear_all_buffers();
  /**
   * \class FieldExchangeInfo
   * \date August, 2013
   * \author James C. Sutherland
   * \brief Base class for exchanging information.
   * \ingroup Comm
   */
  template< typename FieldT >
  class FieldExchangeInfo
  {
  protected:
    const CommMode mode_;
    const int srcID_, destID_;
    const Direction srcBundleDir_;

    std::pair<int,int> fieldID_;
    bool hasSetFieldID_;

#   ifdef HAVE_MPI
    boost::mpi::request sendRequest_;
    boost::mpi::request receiveRequest_;
    bool resetRequest_;
#   endif

    const bool ghost_;

    virtual const FieldT get_src_field( const FieldT& srcField ) const =0;

    virtual FieldT get_dest_field( FieldT& destField ) const =0;

    virtual SpatialOps::IntVec
    origin_index( const CommMode mode,
                  const SpatialOps::MemoryWindow& ) const =0;

    virtual SpatialOps::IntVec glob_field_dim() const=0;

    virtual std::string get_tag_augment( const CommMode mode ) const{ return std::string(); }

    /**
     * @brief provides a unique field tag for MPI transfers
     * @param fieldNameTag
     * @param mw
     * @param id
     * @return a unique field identifier for MPI tags.
     */
    int field_tag( const Expr::Tag fieldNameTag,
                   const SpatialOps::MemoryWindow& mw,
                   const size_t id ) const;

    /**
     * @param fieldSlice  The windowed field that should be copied to the buffer
     * @param buffer the information copied from the field
     */
    void pack_buffer( const FieldT& fieldSlice,
                      std::vector<typename FieldT::value_type>& buffer ) const;

    /**
     * @param buffer the information to load into the field
     * @param fieldSlice  The windowed field that should be populated from the buffer
     */
    void unpack_buffer( const std::vector<typename FieldT::value_type>& buffer,
                        FieldT& fieldSlice ) const;

    /**
     * This is only meant to be constructed by derived classes...
     * @param mode Whether this is intended for send or receive
     * @param dir The bundle direction that the source field lives on
     * @param srcID The MPI rank of the process owning the source field
     * @param destID The MPI rank of the process owning the destination field
     * @param isGhostExchange true if the communication is happening on ghost cells rather than full volumes.
     */
    FieldExchangeInfo( const CommMode mode,
                       const Direction dir,
                       const int srcID,
                       const int destID,
                       const bool isGhostExchange );

    /**
     * @brief Send a field.
     * @param fieldNameTag the name of the field. This is used to provide a
     *        unique tag to identify the field in MPI.
     * @param field the field to send
     */
    void send( const Expr::Tag& fieldNameTag,
               const FieldT& field );

    /**
     * @brief Receive a field.
     * @param fieldNameTag the name of the field. This is used to provide a
     *        unique tag to identify the field in MPI.
     * @param field the field to receive
     */
    void receive( const Expr::Tag& fieldNameTag,
                  FieldT& field );

  public:

    virtual ~FieldExchangeInfo();

    /**
     * Send/Receive the field as appropriate.
     *
     * @param fieldNameTag the field to exchange
     * @param field the field values
     */
    void exchange( const Expr::Tag& fieldNameTag,
                   FieldT& field );


    /**
     * @brief should only be used by objects built in RECEIVE mode.
     * @param fieldNameTag the name of the field to query
     * @param field the field values
     * @return whether the field is ready for receipt or not.
     */
    bool is_recv_ready( const Expr::Tag fieldNameTag,
            FieldT& field );


    /**
     * Check to see if the send/receive is completed.
     * @return true if complete
     */
    bool check_status();

    /**
     * Block until the send/receive is ready
     */
    void wait();

    /**
     * \brief releases the buffer associated with the send.  This should
     * be done after the send has completed.
     */
    void release_buffer();

    inline int get_dest_id() const{return destID_;}
    inline int get_src_id () const{return srcID_; }

  };

  //=================================================================

  /**
   * \class GhostExchangeFieldInfo
   * \author James C. Sutherland
   * \date August, 2013
   * \brief Facilitates ghost exchange among parts of a bundle in a given direction (surface transfers)
   */
  template< typename FieldT >
  class GhostExchangeFieldInfo : public FieldExchangeInfo<FieldT>
  {
    const bool hasFieldInConstructor_;
    const Faces face_;
    const SpatialOps::IntVec nExtra_, nGhostMinus_, nGhostPlus_;
    int nShiftSrc_, nShiftDest_, nghost_;

    const FieldT get_src_field( const FieldT& srcField ) const;

    FieldT get_dest_field( FieldT& destField ) const;

    SpatialOps::IntVec origin_index( const CommMode mode, const SpatialOps::MemoryWindow& ) const;

    SpatialOps::IntVec glob_field_dim() const;

    std::string get_tag_augment( const CommMode mode ) const;

  public:

    /**
     * @brief Construct a GhostExchangeFieldInfo object
     * @param mode          indicates whether this is to be used in Send or Receive mode.
     * @param f           The field to exchange (required to get size information)
     * @param bundleDir   The direction that the bundle is oriented in
     * @param srcID       The MPI rank of the process owning the source field
     * @param destID      The MPI rank of the process owning the destination field
     * @param face        The face to be sent from the owning process.
     */
    GhostExchangeFieldInfo( const CommMode mode,
                            const FieldT& f,
                            const Direction bundleDir,
                            const int srcID,
                            const int destID,
                            const Faces face );

    GhostExchangeFieldInfo( const CommMode mode,
                            const Direction bundleDir,
                            const int srcID,
                            const int destID,
                            const Faces face );
  };

  //=================================================================

  /**
   * \class CoarseExchangeFieldInfo
   * \date August 2013
   * \author James C. Sutherland
   * \brief Provides tools to exchange coarse fields between bundles (volume
   *        transfers).  This is presently restricted to coarse surface fields.
   */
  template< typename FieldT >
  class CoarseExchangeFieldInfo : public FieldExchangeInfo<FieldT>
  {
    const Direction destBundleDir_;
    const SpatialOps::IntVec nGhostMinus_, nGhostPlus_;
    const SpatialOps::IntVec srcOrigin_, destOrigin_;
    SpatialOps::IntVec srcFieldStartIJK_,  destFieldStartIJK_;
    SpatialOps::IntVec srcFieldExtentIJK_, destFieldExtentIJK_;

    const FieldT get_src_field( const FieldT& srcField ) const;

    FieldT get_dest_field( FieldT& destField ) const;

    SpatialOps::IntVec origin_index( const CommMode mode, const SpatialOps::MemoryWindow& ) const;

    SpatialOps::IntVec glob_field_dim() const;

    std::string get_tag_augment( const CommMode mode ) const;

  public:

    /**
     * @brief Construct a CoarseExchangeFieldInfo object
     * @param mode          indicates whether this is to be used in Send or Receive mode.
     * @param f             the field of interest
     * @param srcBundleDir  The direction that the bundle is oriented in
     * @param srcID         The MPI rank of the process owning the source field
     * @param destBundleDir The direction that the bundle is oriented in
     * @param destID        The MPI rank of the process owning the destination field
     */
    CoarseExchangeFieldInfo( const CommMode mode,
                             const FieldT& f,
                             const Direction srcBundleDir,
                             const int srcID,
                             const Direction destBundleDir,
                             const int destID );

  };

} // namespace LBMS

#endif /* FIELDEXCHANGEINFO_H_ */
