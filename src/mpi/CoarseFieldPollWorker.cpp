/* Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file CoarseFieldPollWorker.cpp
 *  \date Dec 7, 2012
 *  \author James C. Sutherland
 */
#include "CoarseFieldPollWorker.h"

#include <mpi/Environment.h>
#include <mpi/FieldExchangeInfo.h>
#include <mpi/FieldBundleExchange.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>

#include <expression/Expression.h>

namespace LBMS{

  //-----------------------------------------------------------------

  template< typename FieldT >
  CoarseFieldPollWorker<FieldT>::
  CoarseFieldPollWorker( const Expr::Tag& fieldTag,
                         const BundlePtr b,
                         const Direction srcBundleDir )
  : Expr::PollWorker(),
    fieldTag_( fieldTag ),
    recv_( new CoarseFieldRecvHelper<FieldT>( b, fieldTag, srcBundleDir ) ),
    fml_( b->get_field_manager_list() )
  {}

  template< typename FieldT >
  CoarseFieldPollWorker<FieldT>::~CoarseFieldPollWorker()
  {
    delete recv_;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  bool
  CoarseFieldPollWorker<FieldT>::operator()( Expr::FieldManagerList* fml )
  {
    FieldT& field = fml_->field_ref<FieldT>( fieldTag_ );
    // check to see if the field is ready for receiving.
    if( !recv_->is_ready( field ) ) return false;
    (*recv_)( field );  // if we get here, we have a message ready, so we unpack it (fully receive it)
    return true;
  }

  //-----------------------------------------------------------------

  // Explicit template instantiation
#define DECLARE( T ) template class CoarseFieldPollWorker<T>;

  DECLARE( SpatialOps::SVolField   )
  DECLARE( SpatialOps::SSurfXField )
  DECLARE( SpatialOps::SSurfYField )
  DECLARE( SpatialOps::SSurfZField )

} // namespace LBMS
