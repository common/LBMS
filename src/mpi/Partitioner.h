/* Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/**
 *  \file   Partitioner.h
 *  \date   Jun 7, 2012
 *  \author James C. Sutherland
 */

#ifndef PARTITIONER_H_
#define PARTITIONER_H_

#include <spatialops/structured/IntVec.h>

#include <lbms/Mesh.h>
#include <mpi/NeighborIDs.h>
#include <mpi/PartitionHelper.h>

#include <boost/multi_array.hpp>

#include <map>

namespace LBMS {

  class BundlePartition;  // forward declaration

  /**
   *  \class  Partitioner
   *  \author James C. Sutherland
   *  \date   Jun 7, 2012
   *
   *  \brief Support for domain decomposition on the lattice
   *
   *  \todo  provide more robust decompositions to allow non-equal partitions
   */
  class Partitioner
  {
    typedef std::pair<unsigned,unsigned> ProcRange;

    const MeshPtr mesh_;
    const unsigned nproc_;

    ProcRange xrange_;  ///< process range for x bundles
    ProcRange yrange_;  ///< process range for y bundles
    ProcRange zrange_;  ///< process range for z bundles

    BundlePartition *xBundlePartition_, *yBundlePartition_, *zBundlePartition_;

    SpatialOps::IntVec bundleProcOffset_;
    SpatialOps::IntVec nProcX_, nProcY_, nProcZ_;


    // these are the processor ranks that own bundles in each of the directions
    std::set<int> xids_, yids_, zids_;
    PartitionHelper helper_;
    // no default constructor, no copying, no assignment
    Partitioner();
    Partitioner( const Partitioner& );
    Partitioner& operator=(const Partitioner&);

    /**
     *  \brief Determine the number of processors to allocate to each bundle
     *    direction.  Note that this is additive, so that it is possible that
     *    elements of this will be zero.
     *
     *  This sets the xrange_, yrange_ and zrange_ variables, which indicate the
     *  process id range associated with each global bundle.
     */
    void partition_bundles();

    std::set<int> owner_ids( const Direction dir, const int nproc ) const;

  public:

    /**
     * \brief construct a Partitioner
     * @param mesh the Mesh
     * @param nproc the number of processors to partition across
     */
    Partitioner( const MeshPtr mesh, const unsigned nproc );

    ~Partitioner();

    /**
     * \brief obtain the process ID that owns a given bundle
     * \param b a bundle
     * \return the process ID that owns the given bundle
     */
    int bundle_owner( const BundlePtr b ) const;

    /**
     * \param dir the direction of interest
     * \return the process ids that own bundles in the given direction.
     */
    const std::set<int>& get_owner_ids(const Direction dir) const;

    /**
     * \brief obtain the id 0-based for the given bundle direction
     * \param procid global process ID
     * \param dir the bundle direction
     * \return the local process ID for this bundle dir
     */
    int local_id( const unsigned procid, const Direction dir ) const;

    /**
     * \brief obtain the global process ID given the local ID for the bundle
     *        oriented in the given direction
     * \param localID the local process ID
     * \param dir the bundle direction
     * \return the global process ID
     */
    int global_id( const unsigned localID, const Direction dir ) const;

    /**
     * @param dir the bundle direction of interest
     * @return the processor decomposition (npx,npy,npz) for the given bundle direction.
     */
    SpatialOps::IntVec
    bundle_partition_pattern( const Direction dir ) const;

    /**
     * \brief obtain the bundles that are owned by this process
     * \param procid the process ID
     */
    BundlePtrVec get_my_bundles( const int procid ) const;

    /**
     * \brief obtain the bundle in the given direction owned by the given process
     * \param procid
     * \param dir the bundle direction direction
     */
    BundlePtr get_my_bundle ( const int procid, const Direction dir ) const;

    /**
     * @brief obtain the NeighborIDs for the given bundle
     * @param bundle the bundle of interest
     * @return the NeighborIDs describing the neighbors for this bundle
     */
    NeighborIDs neighbors( const BundlePtr bundle ) const;
    NeighborIDs neighbors( const Direction dir, const int procid ) const;

    /**
     * @param procid the process id of interest
     * @param dir the direction (bundle) of interest
     * @return the origin coordinate for this process in the given direction
     */
    Coordinate origin( const int procid, const Direction dir ) const;
    Coordinate origin_coarse( const Direction dir, const int procid ) const;

    /**
     * @param procid the process id of interest
     * @param dir the direction (bundle) of interest
     * @return the origin index for this process in the given direction
     */
    SpatialOps::IntVec origin_index( const int procid, const Direction dir ) const;
    SpatialOps::IntVec origin_index_coarse( const int procid, const Direction dir ) const;

    /**
     * @param dir the direction (bundle) of interest
     * @param procid the process id of interest
     * @return the end coordinate (+,+,+) for the given direction on the given process
     */
    Coordinate terminal_coarse( const Direction dir, const int procid ) const;

    SpatialOps::IntVec owned_size( const Direction dir, const int procid ) const;
    SpatialOps::IntVec owned_size_coarse( const Direction dir, const int procid ) const;
  };

} /* namespace LBMS */
#endif /* PARTITIONER_H_ */
