/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 *  \file   test_coarse_field_exchange.cpp
 *  \date   Jul 25, 2013
 *  \author "James C. Sutherland"
 */

#include <mpi/FieldBundleExchange.h>
#include <mpi/FieldExchangeInfo.h>
#include <mpi/Environment.h>
#include <lbms/Bundle.h>
#include <lbms/Mesh.h>

#include <test/TestHelper.h>

#include <expression/ExprLib.h>
#include <spatialops/Nebo.h>
#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/FieldHelper.h>

#include <boost/program_options.hpp>
#include <boost/static_assert.hpp>

#include <iostream>

using namespace LBMS;
using namespace std;
using SpatialOps::operator<<=;
using SpatialOps::IntVec;

namespace po = boost::program_options;

const Expr::Tag xface( "xface_Xbundle", Expr::STATE_NONE ),
                yface( "yface_Ybundle", Expr::STATE_NONE ),
                zface( "zface_Zbundle", Expr::STATE_NONE );

const Expr::Tag xvol(  "xvol_Xbundle",  Expr::STATE_NONE ),
                yvol(  "yvol_Ybundle",  Expr::STATE_NONE ),
                zvol(  "zvol_Zbundle",  Expr::STATE_NONE );
//===================================================================

const Expr::Tag& get_face_tag( const Direction dir )
{
  return select_from_dir(dir,xface,yface,zface);
}

const Expr::Tag& get_vol_tag( const Direction dir )
{
  return select_from_dir(dir,xvol,yvol,zvol);
}

//===================================================================

template< typename FT >
void setup_field( BundlePtr bundle, FT*& f, bool isSource )
{
  typename Expr::FieldMgrSelector<FT>::type& fm = bundle->get_field_manager_list()->field_manager<FT>();

  const bool isVol = int(FT::Location::FaceDir::value) == int(NODIR);
  const int nghost = 1;
  const Direction dir = bundle->dir();
  const Expr::Tag ft = (isVol) ? get_vol_tag ( dir ) : get_face_tag( dir );

  fm.register_field( ft, nghost );
  fm.allocate_fields( boost::cref( bundle->get_field_info() ) );

  f = &fm.field_ref( ft );
  *f <<= -999.99;
  if( isSource ){
    switch( dir ){
      case XDIR: SpatialOps::interior_assign(*f, 1.0);  break;
      case YDIR: SpatialOps::interior_assign(*f, 2.0);  break;
      case ZDIR: SpatialOps::interior_assign(*f, 3.0);  break;
      case NODIR: break;
    }
  }
  else{
    SpatialOps::interior_assign(*f, 0.0);
  }
}

//===================================================================

template< typename FieldT >
bool test_coarse_field_exchange()
{
  const bool verbose = false;

  typedef typename FaceFieldSelector<typename FieldT::Location::Bundle>::ParallelT FaceFieldT;
  typedef typename CoarseFieldSelector<FaceFieldT>::type CoarseFieldT;

  typedef typename CoarseFieldSelector<FieldT>::type CoarseVolumeFieldT;

  const Direction dir = direction<typename FieldT::Location::Bundle>();
  const Direction dir1 = get_perp1(dir);
  const Direction dir2 = get_perp2(dir);

  const BundlePtr srcBundle   = Environment::bundle(dir );
  const BundlePtr dest1Bundle = Environment::bundle(dir1);
  const BundlePtr dest2Bundle = Environment::bundle(dir2);

  // if we are 2D and the source bundle doesn't exist on any process, just return
  if( Environment::glob_dim(dir)[dir] == 1 ) return true;

  TestHelper status(false);

  CoarseFieldT *srcField;   // send this to other bundles
  CoarseFieldT *dest1Field; // receive this from other bundles
  CoarseFieldT *dest2Field; // receive this from other bundles

  CoarseVolumeFieldT *srcVolField;   // send this to other bundles
  CoarseVolumeFieldT *dest1VolField; // receive this from other bundles
  CoarseVolumeFieldT *dest2VolField; // receive this from other bundles

  const Expr::Tag fieldTag     = get_face_tag( dir );
  const Expr::Tag volFieldTag  = get_vol_tag ( dir );

# ifdef HAVE_MPI
  if( verbose ) begin_gate( Environment::world(), Environment::rank() );
# endif
  if( verbose ) std::cout << Environment::rank() << " field " << std::endl;

  if(   srcBundle ){ setup_field(   srcBundle,   srcField,    true  ); if( verbose ) SpatialOps::print_field(*  srcField,std::cout); }
  if( dest1Bundle ){ setup_field( dest1Bundle, dest1Field,    false ); if( verbose ) SpatialOps::print_field(*dest1Field,std::cout); }
  if( dest2Bundle ){ setup_field( dest2Bundle, dest2Field,    false ); if( verbose ) SpatialOps::print_field(*dest2Field,std::cout); }

  if(   srcBundle ){ setup_field(   srcBundle,   srcVolField, true  ); if( verbose ) SpatialOps::print_field(*  srcVolField,std::cout); }
  if( dest1Bundle ){ setup_field( dest1Bundle, dest1VolField, false ); if( verbose ) SpatialOps::print_field(*dest1VolField,std::cout); }
  if( dest2Bundle ){ setup_field( dest2Bundle, dest2VolField, false ); if( verbose ) SpatialOps::print_field(*dest2VolField,std::cout); }

# ifdef HAVE_MPI
  if( verbose ) end_gate( Environment::world(), Environment::rank() );
# endif

  if( srcBundle ){
    if( verbose ) std::cout << Environment::rank() << " sending field " << std::endl;
    CoarseFieldSendHelper<CoarseFieldT> faceSender( srcBundle, fieldTag, srcBundle->dir(), srcBundle->dir() );
    faceSender( *srcField );

    if( verbose ) std::cout << Environment::rank() << " sending vol field " << std::endl;
    CoarseFieldSendHelper<CoarseVolumeFieldT> volSender( srcBundle, volFieldTag, srcBundle->dir(), srcBundle->dir() );
    volSender( *srcVolField );
  }
# ifdef HAVE_MPI
  Environment::world().barrier();
# endif
  if( dest1Bundle ){
    if( verbose ) std::cout << Environment::rank() << " receiving field " << std::endl;
    CoarseFieldRecvHelper<CoarseFieldT      > recv   ( dest1Bundle, fieldTag,    dir );
    CoarseFieldRecvHelper<CoarseVolumeFieldT> recvVol( dest1Bundle, volFieldTag, dir );

    while(!recv.is_ready   (*dest1Field   )){}
    recv   ( *dest1Field    );
    while(!recvVol.is_ready(*dest1VolField)){}
    recvVol( *dest1VolField );

    const double val = select_from_dir( dir, 1.0, 2.0, 3.0 );
    for( typename CoarseFieldT::const_iterator i=dest1Field->interior_begin(); i!=dest1Field->interior_end(); ++i ){
      status( *i == val );
    }
    for( typename CoarseVolumeFieldT::const_iterator i=dest1VolField->interior_begin(); i!=dest1VolField->interior_end(); ++i ){
      status( *i == val );
    }
    if( verbose ) {
      std::cout << "\nDest1:\n";
      SpatialOps::print_field( *dest1Field,    std::cout );
      std::cout << "\nDest1Vol:\n";
      SpatialOps::print_field( *dest1VolField, std::cout );
    }
  }

  if( dest2Bundle ){
    if( verbose ) std::cout << Environment::rank() << " receiving field " << std::endl;
    CoarseFieldRecvHelper<CoarseFieldT      > recv   ( dest2Bundle, fieldTag,    dir );
    CoarseFieldRecvHelper<CoarseVolumeFieldT> recvVol( dest2Bundle, volFieldTag, dir );

    while(!recv.is_ready   (*dest2Field   )){}
    recv   ( *dest2Field    );
    while(!recvVol.is_ready(*dest2VolField)){}
    recvVol( *dest2VolField );

    const double val = select_from_dir( dir, 1.0, 2.0, 3.0 );
    for( typename CoarseFieldT::const_iterator i=dest2Field->interior_begin(); i!=dest2Field->interior_end(); ++i ){
      status( *i == val );
    }
    for( typename CoarseVolumeFieldT::const_iterator i=dest2VolField->interior_begin(); i!=dest2VolField->interior_end(); ++i ){
      status( *i == val );
    }
    if( verbose ){
      std::cout << "\nDest2:\n";
      SpatialOps::print_field( *dest2Field,    std::cout );
      std::cout << "\nDest2Vol:\n";
      SpatialOps::print_field( *dest2VolField, std::cout );
    }
  }
# ifdef HAVE_MPI
  Environment::world().barrier();
# endif
  LBMS::clear_all_buffers();
  return status.ok();
}

//===================================================================

bool test_driver( const IntVec nc,
                  IntVec nf,
                  const Coordinate len,
                  const bool peri[3] )
{

  for(int i = 0; i<3; i++)
  {
      if(nc[i]==1){nf[i]=1;}
  }

  const MeshPtr mesh( new Mesh( nc, nf, len, peri[0], peri[1], peri[2] ) );
  Environment::set_topology(mesh);

  TestHelper status(true);

  try{
    status( test_coarse_field_exchange<LBMS::XVolField>(), " x-coarse exchange" );
    status( test_coarse_field_exchange<LBMS::YVolField>(), " y-coarse exchange" );
    status( test_coarse_field_exchange<LBMS::ZVolField>(), " z-coarse exchange" );
    return status.ok();
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
    return false;
  }
}

//===================================================================

int main( int iarg, char* carg[] )
{
  Coordinate length;
  bool periodic[3] = { false, false, false };

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "perix", "periodic in x" )
      ( "periy", "periodic in y" )
      ( "periz", "periodic in z" );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    periodic[0] = args.count("perix");
    periodic[1] = args.count("periy");
    periodic[2] = args.count("periz");

    if( args.count("help") ){
      cout << desc << "\n";
      return 1;
    }
  }// end parsing scope

  try{
    Environment::setup(iarg,carg);
    TestHelper status(true);

#   ifdef HAVE_MPI
    if( Environment::world().size() < 4 ){
#   endif

      // Vary coarse with a 3D fine:
      {
        const IntVec nFine( 24,36,12 );
        status( test_driver( IntVec(2,2,2), nFine, length, periodic ), "Coarse 2x2x2" );

        status( test_driver( nFine        , nFine,         length, periodic ), "Coarse == Fine" );
        status( test_driver( IntVec(2,2,2), IntVec(2,2,2), length, periodic ), "Coarse2 == Fine2" );

        status( test_driver( IntVec(4,2,2), nFine, length, periodic ), "Coarse 4x2x2" );
        status( test_driver( IntVec(2,4,2), nFine, length, periodic ), "Coarse 2x4x2" );
        status( test_driver( IntVec(2,2,4), nFine, length, periodic ), "Coarse 2x2x4" );

        status( test_driver( IntVec(4,3,2), nFine, length, periodic ), "Coarse 4x3x2" );
        status( test_driver( IntVec(2,3,4), nFine, length, periodic ), "Coarse 2x3x4" );
        status( test_driver( IntVec(3,4,2), nFine, length, periodic ), "Coarse 3x4x2" );

        // 2D
#       ifdef HAVE_MPI
        if( Environment::world().size() < 3 ){
#       endif
          status( test_driver( IntVec(2,2,1), nFine, length, periodic ), "Coarse 2x2x1 " );
          status( test_driver( IntVec(2,1,2), nFine, length, periodic ), "Coarse 2x1x2 " );
          status( test_driver( IntVec(1,2,2), nFine, length, periodic ), "Coarse 1x2x2 " );
#       ifdef HAVE_MPI
        }
#       endif

        //   1D
#       ifdef HAVE_MPI
        if( Environment::world().size() == 1 ){
#       endif
          status( test_driver( IntVec(2,1,1), nFine, length, periodic ), "Coarse 2x1x1 " );
          status( test_driver( IntVec(1,2,1), nFine, length, periodic ), "Coarse 1x2x1 " );
          status( test_driver( IntVec(1,1,2), nFine, length, periodic ), "Coarse 1x1x2 " );
#       ifdef HAVE_MPI
        }
#       endif
        }

        // vary fine with a 3D coarse:
        {
          const IntVec nCoarse( 2, 2, 2 );
          status( test_driver( nCoarse, IntVec(20,20,20), length, periodic ), "Fine 20x20x20" );
          status( test_driver( nCoarse, IntVec(10,20,20), length, periodic ), "Fine 10x20x20" );
          status( test_driver( nCoarse, IntVec(20,10,20), length, periodic ), "Fine 20x10x20" );
          status( test_driver( nCoarse, IntVec(20,20,10), length, periodic ), "Fine 20x20x10" );
        }

#     ifdef HAVE_MPI
      } //if( Environment::world().size() < 4 ){
#     endif
    {
      status( test_driver(IntVec(30,30,1), IntVec(30,30,1), length, periodic ), "XY 30x30x1");
      status( test_driver(IntVec(30,1,30), IntVec(30,1, 30), length, periodic ), "XZ 30x30x1");
      status( test_driver(IntVec(1,30,30), IntVec(1,30,30), length, periodic ), "YZ 30x30x1");
      status( test_driver(IntVec(12,12,12), IntVec(12,12,12), length, periodic ), "12x12x12");
      status( test_driver(IntVec(48,14,16), IntVec(48,14,16), length, periodic ), "12x12x12");
    }

    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;
}
