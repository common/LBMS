#include <string>
#include <limits>
#include <iostream>
#include <set>
#include <vector>
#include <cctype>
#include <algorithm>
#include <map>

#include <boost/lexical_cast.hpp>

#include <test/TestHelper.h>

#include <mpi/FieldExchangeInfo.h>
#include <mpi/Environment.h>

using namespace std;

int main( int iarg, char* carg[] )
{
  LBMS::Environment::setup(iarg,carg);

  const int nproc = 1000;

  //Tag + ID + X + if(fine)x,y,z | xminus,xplus, yminus,yplus,zminus,zplus + orginindex( so int n)
  string fa[] = { "XMINUS", "XPLUS", "YMINUS", "YPLUS", "ZMINUS", "ZPLUS" };
  string ca[] = { "Y", "Z" };

  //All the x tags for the TaylorGreen problem.
  //All the fine tags
  string tags[]={
      "density_Xbundle",
      "rhoE0_Xbundle",
      "x_momentum_Xbundle",
      "y_momentum_Xbundle",
      "z_momentum_Xbundle",
      "x_velocity_advect_Xbundle",
      "rhoE0_convectiveflux_xface_Xbundle",
      "density_convectiveflux_xface_Xbundle",
      "x_velocity_dx_Xbundle",
      "x_momentum_convectiveflux_xface_Xbundle",
      "y_velocity_dx_Xbundle",
      "y_momentum_convectiveflux_xface_Xbundle",
      "temperature_Xbundle",
      "pressure_Xbundle",
      "pressure_xface_Xbundle_Xbundle",
      "dpdx_Xbundle",
      "viscosity_Xbundle",
      "heatflux_xface_Xbundle",
      "z_velocity_dx_Xbundle",
      "z_momentum_convectiveflux_xface_Xbundle",
      "pressure_zface_Xbundle_Zbundle",
      "dpdz_Xbundle",
      "x_velocity_dz_Xbundle",
      "x_velocity_dz_vol_Xbundle",
      "x_velocity_dz_xface_Xbundle",
      "tauxz_Xbundle",
      "z_velocity_advect_Xbundle",
      "x_velocity_dy_Xbundle",
      "x_velocity_dy_vol_Xbundle",
      "x_velocity_dy_xface_Xbundle",
      "tauxy_Xbundle",
      "y_velocity_advect_Xbundle",
      "dilatation_Xbundle",
      "tauxx_Xbundle",
      "rhoE0_diffusiveflux_xface_Xbundle",
      "tauyz_Xbundle",
      "tauzy_Xbundle",
      "tauyx_Xbundle",
      "tauzx_Xbundle",
      "pressure_yface_Xbundle_Ybundle",
      "dpdy_Xbundle",
      "rhoE0_diffusiveflux_yface_Xbundle",
      "tauyy_Xbundle",
      "tauzz_Xbundle"
  };
  //All the coarse tags.
  string coarse_tags[]={
      "rhoE0_convectiveflux_xface_coarse_Xbundle",
      "x_velocity_advect_coarse_Xbundle",
      "density_convectiveflux_xface_coarse_Xbundle",
      "x_momentum_convectiveflux_xface_coarse_Xbundle",
      "y_velocity_dx_coarse_Xbundle",
      "y_momentum_convectiveflux_xface_coarse_Xbundle",
      "pressure_xface_coarse_Xbundle",
      "z_velocity_dx_coarse_Xbundle",
      "z_momentum_convectiveflux_xface_coarse_Xbundle",
      "tauxz_coarse_Xbundle",
      "tauxx_coarse_Xbundle",
      "rhoE0_diffusiveflux_xface_coarse_Xbundle",
      "tauxy_coarse_Xbundle"
  };

  vector<string> fine_t(tags, tags+44);
  vector<string> coarse_t(coarse_tags, coarse_tags+13);
  vector<string> fine_augment(fa, fa+6);
  vector<string> coarse_augment(ca, ca+2);

  map<pair<int, int>, string > hashes;
  TestHelper overall(true);
  for( int proc=0; proc<nproc; ++proc ){
    TestHelper status(false);
    vector<string>::iterator si, sj;
    for( si=fine_t.begin(); si!=fine_t.end(); ++si ){
      for( sj=fine_augment.begin(); sj!=fine_augment.end(); ++sj ){
        const string str = *si+"X"+*sj+"0";
        const unsigned long h = LBMS::string_hash(str);
        const unsigned long maxtag = std::numeric_limits<int>::max();
        status( h>0 && h<=maxtag, "Hash is in bounds" );
        if( h>maxtag ) std::cout << h << std::endl;
        pair<int, int> key(h, proc);
        status( hashes.find(key) == hashes.end(), " Hash clash " );
        hashes.insert( make_pair(key, str) );
      }
    }
    for( si=coarse_t.begin(); si!=coarse_t.end(); ++si ){
      for( sj=coarse_augment.begin(); sj!=coarse_augment.end(); ++sj ){
        const string str = *si+"X"+*sj+"0";
        const int h = LBMS::string_hash(str);
        pair<int, int> key(h, proc);
        status( hashes.find(key) == hashes.end(), " Hash clash ");
        hashes.insert( make_pair(key, str) );
      }
    }
    overall( status.ok(), boost::lexical_cast<string>(proc)+ " process" );
  }

  cout << "\n------------------------------------------\n\n";
  if( overall.ok() ){
    cout << "PASS\n";
    return 0;
  }
  cout << "FAIL\n";
  return -1;
}
