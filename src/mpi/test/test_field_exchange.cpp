/*
 *
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/** \file   test_field_exchange.cpp
 *  \date   Jul 15, 2013
 *  \author "James C. Sutherland"
 */

#include <spatialops/structured/IntVec.h>

#include <mpi/FieldBundleExchange.h>
#include <mpi/Environment.h>
#include <lbms/Bundle.h>
#include <lbms/Mesh.h>

#include <test/TestHelper.h>

#include <expression/ExprLib.h>
#include <spatialops/Nebo.h>

#include <boost/program_options.hpp>

#include <iostream>

//===================================================================

using namespace LBMS;
using namespace SpatialOps;
using namespace std;
namespace po = boost::program_options;

//===================================================================

const Expr::Tag idt("id",Expr::STATE_NONE);

//===================================================================

template<typename FieldT>
bool check_result( const FieldT& field,
                   const BundlePtr bundle,
                   const Faces face )
{
  const int nbrVal = Environment::neighbor_id(bundle,face);

  const MemoryWindow& mw = field.window_with_ghost();
  const IntVec& extent = mw.extent();

  int ix = extent[0]/2;
  int iy = extent[1]/2;
  int iz = extent[2]/2;

  switch(face){
    case XMINUS:  ix=0;           break;
    case YMINUS:  iy=0;           break;
    case ZMINUS:  iz=0;           break;
    case XPLUS :  ix=extent[0]-1; break;
    case YPLUS :  iy=extent[1]-1; break;
    case ZPLUS :  iz=extent[2]-1; break;
  }

  const double val = field( ix, iy, iz );
//  std::cout << "rank " << Environment::rank() << "\t" << IntVec(ix,iy,iz)
//            << " : " << val << " (" << nbrVal << ") -- diff="
//            << std::abs(val - double(nbrVal)) << std::endl;
  if( std::abs(val - double(nbrVal)) < 1e-14 ) return true;
  return false;
}

//===================================================================

template<typename FieldT>
bool test_field_exchange( const BundlePtr bundle )
{
  if( !bundle ) return true;

  typename Expr::FieldMgrSelector<FieldT>::type& fm = bundle->get_field_manager_list()->field_manager<FieldT>();
  const int nghost = 1;
  fm.register_field( idt, nghost );
  fm.allocate_fields( boost::cref( bundle->get_field_info() ) );
  FieldT& idField = fm.field_ref(idt);

  // Populate only the interior edge of the field with the processor id, having all else be -999.99
  idField <<= -999.99;
  typename FieldT::iterator iend = idField.interior_end();
  for( typename FieldT::iterator i= idField.interior_begin(); i != iend; ++i)
  {
      *i = Environment::rank();
  }
  const MemoryWindow fieldmw = idField.window_with_ghost();
  const MemoryWindow mw(fieldmw.glob_dim(),
                        IntVec(2,2,2),
                        fieldmw.glob_dim()-IntVec(4,4,4));
  FieldT f = FieldT(mw, idField);
  f <<= -999.99;

  const int xmID = Environment::neighbor_id( bundle, XMINUS );
  const int xpID = Environment::neighbor_id( bundle, XPLUS  );
  const int ymID = Environment::neighbor_id( bundle, YMINUS );
  const int ypID = Environment::neighbor_id( bundle, YPLUS  );
  const int zmID = Environment::neighbor_id( bundle, ZMINUS );
  const int zpID = Environment::neighbor_id( bundle, ZPLUS  );

  const int myID = Environment::rank();

  const bool doXMinus = (myID != xmID) || ( bundle->get_boundary_type(XMINUS) == PeriodicBoundary );
  const bool doXPlus  = (myID != xpID) || ( bundle->get_boundary_type(XPLUS ) == PeriodicBoundary );
  const bool doYMinus = (myID != ymID) || ( bundle->get_boundary_type(YMINUS) == PeriodicBoundary );
  const bool doYPlus  = (myID != ypID) || ( bundle->get_boundary_type(YPLUS ) == PeriodicBoundary );
  const bool doZMinus = (myID != zmID) || ( bundle->get_boundary_type(ZMINUS) == PeriodicBoundary );
  const bool doZPlus  = (myID != zpID) || ( bundle->get_boundary_type(ZPLUS ) == PeriodicBoundary );

  typedef GhostFieldSendHelper<FieldT> Sender;
  typedef GhostFieldRecvHelper<FieldT> Receiver;
  if( doXMinus ){
    Sender sender(bundle, idt, XMINUS, xmID);
    sender( idField );
  }
  if( doXPlus ){
    Sender sender(bundle, idt, XPLUS, xpID);
    sender( idField );
  }
  if( doYMinus ){
    Sender sender(bundle, idt, YMINUS, ymID);
    sender( idField );
  }
  if( doYPlus ){
    Sender sender(bundle, idt, YPLUS, ypID);
    sender( idField );
  }
  if( doZMinus ){
    Sender sender(bundle, idt, ZMINUS, zmID);
    sender( idField );
  }
  if( doZPlus ){
    Sender sender(bundle, idt, ZPLUS, zpID);
    sender( idField );
  }

  TestHelper status(true);

  if( doXMinus ){
    Receiver xmin(bundle, idt, XMINUS, xmID);
    while(!xmin.is_ready(idField)){}
    xmin( idField );
    status( check_result( idField, bundle, XMINUS ), "x- face" );
  }
  if( doXPlus ){
    Receiver recv(bundle, idt, XPLUS, xpID);
    while(!recv.is_ready(idField)){}
    recv( idField );
    status( check_result( idField, bundle, XPLUS  ), "x+ face" );
  }
  if( doYMinus ){
    Receiver recv(bundle, idt, YMINUS, ymID);
    while(!recv.is_ready(idField)){}
    recv( idField );
    status( check_result( idField, bundle, YMINUS ), "y- face" );
  }
  if( doYPlus ){
    Receiver recv(bundle, idt, YPLUS, ypID);
    while(!recv.is_ready(idField)){}
    recv( idField );
    status( check_result( idField, bundle, YPLUS  ), "y+ face" );
  }
  if( doZMinus ){
    Receiver recv(bundle, idt, ZMINUS, zmID);
    while(!recv.is_ready(idField)){}
    recv( idField );
    status( check_result( idField, bundle, ZMINUS ), "z- face" );
  }
  if( doZPlus ){
    Receiver recv(bundle, idt, ZPLUS, zpID);
    while(!recv.is_ready(idField)){}
    recv( idField );
    status( check_result( idField, bundle, ZPLUS  ), "z+ face" );
  }

  return status.ok();
}

//===================================================================

int main( int iarg, char* carg[] )
{
  IntVec nCoarse, nFine;
  Coordinate length;
  bool periodic[3];

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(20), "Fine grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(20), "Fine grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(20), "Fine grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(10),"Coarse grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(10),"Coarse grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(10),"Coarse grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "perix", "periodic in x" )
      ( "periy", "periodic in y" )
      ( "periz", "periodic in z" );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    periodic[0] = args.count("perix");
    periodic[1] = args.count("periy");
    periodic[2] = args.count("periz");

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }// end parsing scope

  try{

    const MeshPtr mesh( new Mesh( nCoarse, nFine, length, periodic[0], periodic[1], periodic[2] ) );

    Environment::setup(iarg,carg);
    Environment::set_topology(mesh);

    TestHelper status(true);

    status( test_field_exchange<LBMS::XVolField>( Environment::bundle( LBMS::XDIR ) ), "X-dir exchange" );
    status( test_field_exchange<LBMS::YVolField>( Environment::bundle( LBMS::YDIR ) ), "Y-dir exchange" );
    status( test_field_exchange<LBMS::ZVolField>( Environment::bundle( LBMS::ZDIR ) ), "Z-dir exchange" );

    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;

}
