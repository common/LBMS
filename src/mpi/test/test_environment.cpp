#include <mpi/Environment.h>

#include <test/TestHelper.h>
#include <lbms/Mesh.h>

#include <boost/program_options.hpp>
namespace po=boost::program_options;

using namespace std;
using SpatialOps::IntVec;

int main( int iarg, char* carg[] )
{
  LBMS::Environment::setup(iarg,carg);

  IntVec nCoarse, nFine;
  LBMS::Coordinate length;

  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(24), "Fine (ODT) grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(24), "Fine (ODT) grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(24), "Fine (ODT) grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(2), "Coarse (LES) grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(3), "Coarse (LES) grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(4), "Coarse (LES) grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1), "Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1), "Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1), "Domain length in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }

  try{
    const LBMS::MeshPtr globalMesh( new LBMS::Mesh( nCoarse, nFine, length ) );
    LBMS::Environment::set_topology( globalMesh );
  }
  catch( std::exception& err ){
    cout << endl << err.what() << endl;
    return -1;
  }
  return 0;
}
