#include <mpi/Partitioner.h>

#include <test/TestHelper.h>
#include <mpi/Environment.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>

using namespace std;
using SpatialOps::IntVec;
using namespace LBMS;

int main( int iarg, char* carg[] )
{
  Environment::setup(iarg,carg);
  TestHelper overall(true);
  Coordinate length(1,1,1);
  try{
    IntVec nCoarse(2,2,2), nFine(24,24,24);
    const LBMS::MeshPtr mesh1( new LBMS::Mesh( nCoarse, nFine, length ) );

    {
      TestHelper status(false);
      const Partitioner p1(mesh1,1);
      status( p1.get_my_bundles(0).size() == 3, "np1 #bundles" );

      // this is a really weak test of the neighbor calculation!
      const NeighborIDs nbrXbundle = p1.neighbors(mesh1->bundle(XDIR));
      status( nbrXbundle.xminus == 0 && nbrXbundle.xplus==0 &&
              nbrXbundle.yminus == 0 && nbrXbundle.yplus==0 &&
              nbrXbundle.zminus == 0 && nbrXbundle.zplus==0,
              "np1 neighbor IDs" );

      status( p1.origin(0,XDIR) == Coordinate(0,0,0), "p1 0 x origin" );
      status( p1.origin(0,YDIR) == Coordinate(0,0,0), "p1 0 y origin" );
      status( p1.origin(0,ZDIR) == Coordinate(0,0,0), "p1 0 z origin" );

      const BundlePtr xb = p1.get_my_bundle(0,XDIR);
      const BundlePtr yb = p1.get_my_bundle(0,YDIR);
      const BundlePtr zb = p1.get_my_bundle(0,ZDIR);

      status( xb->global_mesh_offset() == IntVec(0,0,0), "p1 xbundle mesh offset" );
      status( yb->global_mesh_offset() == IntVec(0,0,0), "p1 ybundle mesh offset" );
      status( zb->global_mesh_offset() == IntVec(0,0,0), "p1 zbundle mesh offset" );

      status( xb->bundle_offset() == IntVec(0,0,0), "p1 xbundle offset" );
      status( yb->bundle_offset() == IntVec(0,0,0), "p1 ybundle offset" );
      status( zb->bundle_offset() == IntVec(0,0,0), "p1 zbundle offset" );

      overall( status.ok(), "1 process" );
    }

    {
      TestHelper status(true);
      const Partitioner p2(mesh1,2);
      status( p2.bundle_partition_pattern(XDIR) == IntVec(1,1,1), "np2 xbundle partition" );
      status( p2.bundle_partition_pattern(YDIR) == IntVec(1,1,1), "np2 ybundle partition" );
      status( p2.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "np2 zbundle partition" );
      status( p2.get_my_bundles(0).size() == 2, "np2 p0 #bundles" );
      status( p2.get_my_bundles(1).size() == 1, "np2 p1 #bundles" );

      const NeighborIDs nbrXbundle = p2.neighbors(mesh1->bundle(XDIR));
      status( nbrXbundle.xminus == 0 && nbrXbundle.xplus==0 &&
              nbrXbundle.yminus == 0 && nbrXbundle.yplus==0 &&
              nbrXbundle.zminus == 0 && nbrXbundle.zplus==0,
              "np2 neighbor IDs" );

      status( p2.origin(0,XDIR) == Coordinate(0,0,0), "p2 0 x origin" );
      status( p2.origin(1,YDIR) == Coordinate(0,0,0), "p2 1 y origin" );
      status( p2.origin(0,ZDIR) == Coordinate(0,0,0), "p2 0 z origin" );

      overall( status.ok(), "2 processes" );
    }

    {
      TestHelper status(false);
      const Partitioner p3(mesh1,3);
      status( p3.bundle_partition_pattern(XDIR) == IntVec(1,1,1), "np3 xbundle partition" );
      status( p3.bundle_partition_pattern(YDIR) == IntVec(1,1,1), "np3 ybundle partition" );
      status( p3.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "np3 zbundle partition" );
      status( p3.get_my_bundles(0).size() == 1, "np3 p0 #bundles" );
      status( p3.get_my_bundles(1).size() == 1, "np3 p1 #bundles" );
      status( p3.get_my_bundles(2).size() == 1, "np3 p2 #bundles" );
      status( p3.global_id( p3.local_id(0,XDIR), XDIR ) == 0, "XBundle global->local id 0" );
      status( p3.global_id( p3.local_id(1,XDIR), XDIR ) == 1, "XBundle global->local id 1" );
      status( p3.global_id( p3.local_id(2,XDIR), XDIR ) == 2, "XBundle global->local id 2" );
      status( p3.global_id( p3.local_id(0,YDIR), YDIR ) == 0, "YBundle global->local id 0" );
      status( p3.global_id( p3.local_id(1,YDIR), YDIR ) == 1, "YBundle global->local id 1" );
      status( p3.global_id( p3.local_id(2,YDIR), YDIR ) == 2, "YBundle global->local id 2" );
      status( p3.global_id( p3.local_id(0,ZDIR), ZDIR ) == 0, "ZBundle global->local id 0" );
      status( p3.global_id( p3.local_id(1,ZDIR), ZDIR ) == 1, "ZBundle global->local id 1" );
      status( p3.global_id( p3.local_id(2,ZDIR), ZDIR ) == 2, "ZBundle global->local id 2" );

      status( p3.origin(0,XDIR) == Coordinate(0,0,0), "p3 0 x origin" );
      status( p3.origin(1,YDIR) == Coordinate(0,0,0), "p3 1 y origin" );
      status( p3.origin(2,ZDIR) == Coordinate(0,0,0), "p3 2 z origin" );

      overall( status.ok(), "3 processes" );
    }

    {
      TestHelper status(false);
      const LBMS::MeshPtr mesh( new LBMS::Mesh( IntVec(4,4,4), nFine, length ) );
      const Partitioner p4(mesh,4);
      status( p4.bundle_partition_pattern(XDIR) == IntVec(1,2,1), "np4 xbundle partition" );
      status( p4.bundle_partition_pattern(YDIR) == IntVec(1,1,1), "np4 ybundle partition" );
      status( p4.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "np4 zbundle partition" );
      status( p4.get_my_bundles(0).size() == 1, "np4 p0 #bundles" );
      status( p4.get_my_bundles(1).size() == 1, "np4 p1 #bundles" );
      status( p4.get_my_bundles(2).size() == 1, "np4 p2 #bundles" );
      status( p4.get_my_bundles(3).size() == 1, "np4 p3 #bundles" );
      status( p4.global_id( p4.local_id(0,XDIR), XDIR ) == 0, "XBundle global->local id 0" );
      status( p4.global_id( p4.local_id(1,XDIR), XDIR ) == 1, "XBundle global->local id 1" );
      status( p4.global_id( p4.local_id(2,XDIR), XDIR ) == 2, "XBundle global->local id 2" );
      status( p4.global_id( p4.local_id(3,XDIR), XDIR ) == 3, "XBundle global->local id 3" );
      status( p4.global_id( p4.local_id(0,YDIR), YDIR ) == 0, "YBundle global->local id 0" );
      status( p4.global_id( p4.local_id(1,YDIR), YDIR ) == 1, "YBundle global->local id 1" );
      status( p4.global_id( p4.local_id(2,YDIR), YDIR ) == 2, "YBundle global->local id 2" );
      status( p4.global_id( p4.local_id(3,YDIR), YDIR ) == 3, "YBundle global->local id 3" );
      status( p4.global_id( p4.local_id(0,ZDIR), ZDIR ) == 0, "ZBundle global->local id 0" );
      status( p4.global_id( p4.local_id(1,ZDIR), ZDIR ) == 1, "ZBundle global->local id 1" );
      status( p4.global_id( p4.local_id(2,ZDIR), ZDIR ) == 2, "ZBundle global->local id 2" );
      status( p4.global_id( p4.local_id(3,ZDIR), ZDIR ) == 3, "ZBundle global->local id 3" );

      status( p4.origin(0,XDIR) == Coordinate(0,0,  0), "p4 0 x origin" );
      status( p4.origin(1,XDIR) == Coordinate(0,0.5,0), "p4 1 x origin" );
      status( p4.origin(2,YDIR) == Coordinate(0,0,  0), "p4 2 y origin" );
      status( p4.origin(3,ZDIR) == Coordinate(0,0,  0), "p4 3 z origin" );

      overall( status.ok(), "4 processes" );
    }
    {
    nCoarse = IntVec(10,10,10);  nFine=IntVec(1000,100,100);
    const MeshPtr mesh2( new Mesh(nCoarse,nFine,length) );
      TestHelper status(false);
      const Partitioner p1(mesh2,1);
      status( p1.get_my_bundles(0).size() == 3, "np1 #bundles" );

      const Partitioner p2(mesh2,2);
      status( p2.bundle_partition_pattern(XDIR) == IntVec(1,2,1), "np2 xbundle partition" );
      status( p2.bundle_partition_pattern(YDIR) == IntVec(1,1,1), "np2 ybundle partition" );
      status( p2.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "np2 zbundle partition" );
      status( p2.get_my_bundles(0).size() == 3, "np2 p0 #bundles" );
      status( p2.get_my_bundles(1).size() == 1, "np2 p1 #bundles" );

      overall( status.ok(), "mesh 2 1 process" );
    }

    {
      TestHelper status(false);

      nCoarse = IntVec(10,10,10);  nFine=IntVec(1000,100,100);
      const MeshPtr mesh2( new Mesh(nCoarse,nFine,length) );
      const Partitioner p(mesh2,12);
      status( p.bundle_partition_pattern(XDIR) == IntVec(1,5,2), "np12 x-pattern" );
      status( p.bundle_partition_pattern(YDIR) == IntVec(1,1,1), "np12 y-pattern" );
      status( p.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "np12 z-pattern" );

      const BundlePtr b0x = p.get_my_bundle(0,XDIR);
      const NeighborIDs b0xnbr = p.neighbors( b0x );
      status( b0xnbr.xminus == 0, "b0x: x-" );  status( b0x->get_boundary_type(XMINUS) == DomainBoundary,    "0x-bc" );
      status( b0xnbr.xplus  == 0, "b0x: x+" );  status( b0x->get_boundary_type(XPLUS ) == DomainBoundary,    "0x+bc" );
      status( b0xnbr.yminus == 0, "b0x: y-" );  status( b0x->get_boundary_type(YMINUS) == DomainBoundary,    "0y-bc" );
      status( b0xnbr.yplus  == 1, "b0x: y+" );  status( b0x->get_boundary_type(YPLUS ) == ProcessorBoundary, "0y+bc" );
      status( b0xnbr.zminus == 0, "b0x: z-" );  status( b0x->get_boundary_type(ZMINUS) == DomainBoundary,    "0z-bc" );
      status( b0xnbr.zplus  == 5, "b0x: z+" );  status( b0x->get_boundary_type(ZPLUS ) == ProcessorBoundary, "0z+bc" );

      const BundlePtr b1x = p.get_my_bundle(1,XDIR);
      const NeighborIDs b1xnbr = p.neighbors( b1x );
      status( b1xnbr.xminus == 1, "b1x: x-" );   status( b1x->get_boundary_type(XMINUS) == DomainBoundary,    "1x-bc" );
      status( b1xnbr.xplus  == 1, "b1x: x+" );   status( b1x->get_boundary_type(XPLUS ) == DomainBoundary,    "1x+bc" );
      status( b1xnbr.yminus == 0, "b1x: y-" );   status( b1x->get_boundary_type(YMINUS) == ProcessorBoundary, "1y-bc" );
      status( b1xnbr.yplus  == 2, "b1x: y+" );   status( b1x->get_boundary_type(YPLUS ) == ProcessorBoundary, "1y+bc" );
      status( b1xnbr.zminus == 1, "b1x: z-" );   status( b1x->get_boundary_type(ZMINUS) == DomainBoundary,    "1z-bc" );
      status( b1xnbr.zplus  == 6, "b1x: z+" );   status( b1x->get_boundary_type(ZPLUS ) == ProcessorBoundary, "1z+bc" );


      const BundlePtr b2x = p.get_my_bundle(2,XDIR);
      const NeighborIDs b2xnbr = p.neighbors( b2x );
      status( b2xnbr.xminus == 2, "b2x: x-" );   status( b2x->get_boundary_type(XMINUS) == DomainBoundary,    "2y-bc" );
      status( b2xnbr.xplus  == 2, "b2x: x+" );   status( b2x->get_boundary_type(XPLUS ) == DomainBoundary,    "2y+bc" );
      status( b2xnbr.yminus == 1, "b2x: y-" );   status( b2x->get_boundary_type(YMINUS) == ProcessorBoundary, "2y-bc" );
      status( b2xnbr.yplus  == 3, "b2x: y+" );   status( b2x->get_boundary_type(YPLUS ) == ProcessorBoundary, "2y+bc" );
      status( b2xnbr.zminus == 2, "b2x: z-" );   status( b2x->get_boundary_type(ZMINUS) == DomainBoundary,    "2z-bc" );
      status( b2xnbr.zplus  == 7, "b2x: z+" );   status( b2x->get_boundary_type(ZPLUS ) == ProcessorBoundary, "2z+bc" );

      const BundlePtr b3x = p.get_my_bundle(3,XDIR);
      const NeighborIDs b3xnbr = p.neighbors( b3x );
      status( b3xnbr.xminus == 3, "b3x: x-" );   status( b3x->get_boundary_type(XMINUS) == DomainBoundary,    "3x-bc" );
      status( b3xnbr.xplus  == 3, "b3x: x+" );   status( b3x->get_boundary_type(XPLUS ) == DomainBoundary,    "3x+bc" );
      status( b3xnbr.yminus == 2, "b3x: y-" );   status( b3x->get_boundary_type(YMINUS) == ProcessorBoundary, "3y-bc" );
      status( b3xnbr.yplus  == 4, "b3x: y+" );   status( b3x->get_boundary_type(YPLUS ) == ProcessorBoundary, "3y+bc" );
      status( b3xnbr.zminus == 3, "b3x: z-" );   status( b3x->get_boundary_type(ZMINUS) == DomainBoundary,    "3z-bc" );
      status( b3xnbr.zplus  == 8, "b3x: z+" );   status( b3x->get_boundary_type(ZPLUS ) == ProcessorBoundary, "3z+bc" );

      const BundlePtr b4x = p.get_my_bundle(4,XDIR);
      const NeighborIDs b4xnbr = p.neighbors( b4x );
      status( b4xnbr.xminus == 4, "b4x: x-" );  status( b4x->get_boundary_type(XMINUS) == DomainBoundary,    "4x-bc" );
      status( b4xnbr.xplus  == 4, "b4x: x+" );  status( b4x->get_boundary_type(XPLUS ) == DomainBoundary,    "4x+bc" );
      status( b4xnbr.yminus == 3, "b4x: y-" );  status( b4x->get_boundary_type(YMINUS) == ProcessorBoundary, "4y-bc" );
      status( b4xnbr.yplus  == 4, "b4x: y+" );  status( b4x->get_boundary_type(YPLUS ) == DomainBoundary,    "4y+bc" );
      status( b4xnbr.zminus == 4, "b4x: z-" );  status( b4x->get_boundary_type(ZMINUS) == DomainBoundary,    "4z-bc" );
      status( b4xnbr.zplus  == 9, "b4x: z+" );  status( b4x->get_boundary_type(ZPLUS ) == ProcessorBoundary, "4z+bc" );

      const BundlePtr b5x = p.get_my_bundle(5,XDIR);
      const NeighborIDs b5xnbr = p.neighbors( b5x );
      status( b5xnbr.xminus == 5, "b5x: x-" );  status( b5x->get_boundary_type(XMINUS) == DomainBoundary,    "5x-bc" );
      status( b5xnbr.xplus  == 5, "b5x: x+" );  status( b5x->get_boundary_type(XPLUS ) == DomainBoundary,    "5x+bc" );
      status( b5xnbr.yminus == 5, "b5x: y-" );  status( b5x->get_boundary_type(YMINUS) == DomainBoundary,    "5y-bc" );
      status( b5xnbr.yplus  == 6, "b5x: y+" );  status( b5x->get_boundary_type(YPLUS ) == ProcessorBoundary, "5y+bc" );
      status( b5xnbr.zminus == 0, "b5x: z-" );  status( b5x->get_boundary_type(ZMINUS) == ProcessorBoundary, "5z-bc" );
      status( b5xnbr.zplus  == 5, "b5x: z+" );  status( b5x->get_boundary_type(ZPLUS ) == DomainBoundary,    "5z+bc" );

      const BundlePtr b9x = p.get_my_bundle(9,XDIR);
      const NeighborIDs b9xnbr = p.neighbors( b9x );
      status( b9xnbr.xminus == 9, "b9x: x-" );  status( b9x->get_boundary_type(XMINUS) == DomainBoundary,    "9x-bc" );
      status( b9xnbr.xplus  == 9, "b9x: x+" );  status( b9x->get_boundary_type(XPLUS ) == DomainBoundary,    "9x+bc" );
      status( b9xnbr.yminus == 8, "b9x: y-" );  status( b9x->get_boundary_type(YMINUS) == ProcessorBoundary, "9y-bc" );
      status( b9xnbr.yplus  == 9, "b9x: y+" );  status( b9x->get_boundary_type(YPLUS ) == DomainBoundary,    "9y+bc" );
      status( b9xnbr.zminus == 4, "b9x: z-" );  status( b9x->get_boundary_type(ZMINUS) == ProcessorBoundary, "9z-bc" );
      status( b9xnbr.zplus  == 9, "b9x: z+" );  status( b9x->get_boundary_type(ZPLUS ) == DomainBoundary,    "9z+bc" );

      status( p.origin( 0,XDIR) == Coordinate(0.0,0.0,0.0), " 0 x origin" );
      status( p.origin( 1,XDIR) == Coordinate(0.0,0.2,0.0), " 1 x origin" );
      status( p.origin( 2,XDIR) == Coordinate(0.0,0.4,0.0), " 2 x origin" );
      status( p.origin( 3,XDIR) == Coordinate(0.0,0.6,0.0), " 3 x origin" );
      status( p.origin( 4,XDIR) == Coordinate(0.0,0.8,0.0), " 4 x origin" );
      status( p.origin( 5,XDIR) == Coordinate(0.0,0.0,0.5), " 5 x origin" );
      status( p.origin( 6,XDIR) == Coordinate(0.0,0.2,0.5), " 6 x origin" );
      status( p.origin( 7,XDIR) == Coordinate(0.0,0.4,0.5), " 7 x origin" );
      status( p.origin( 8,XDIR) == Coordinate(0.0,0.6,0.5), " 8 x origin" );
      status( p.origin( 9,XDIR) == Coordinate(0.0,0.8,0.5), " 9 x origin" );
      status( p.origin(10,YDIR) == Coordinate(0.0,0.0,0.0), "10 y origin" );
      status( p.origin(11,ZDIR) == Coordinate(0.0,0.0,0.0), "11 z origin" );

      overall( status.ok(), "mesh2, 12 processes" );
    }

    {
      const LBMS::MeshPtr mesh3( new LBMS::Mesh( IntVec(6,3,3), IntVec(12,120,12),
                                                 length, true, true, true ) );
      const Partitioner p( mesh3, 4 );

      TestHelper status(false);

      status( p.bundle_partition_pattern(XDIR) == IntVec(1,1,1), "Xbundle partition" );
      status( p.bundle_partition_pattern(YDIR) == IntVec(3,1,1), "Ybundle partition" );
      status( p.bundle_partition_pattern(ZDIR) == IntVec(1,1,1), "Zbundle partition" );

      const NeighborIDs nbrs0x = p.neighbors( LBMS::XDIR, 0 );
      const NeighborIDs nbrs0z = p.neighbors( LBMS::ZDIR, 0 );
      const NeighborIDs nbrs1 = p.neighbors( LBMS::YDIR, 1 );
      const NeighborIDs nbrs3 = p.neighbors( LBMS::YDIR, 3 );

      status( nbrs3.xminus==2 && nbrs3.xplus==1 &&
              nbrs3.yminus==3 && nbrs3.yplus==3 &&
              nbrs3.zminus==3 && nbrs3.zplus==3, "rank 3 neighbors" );

      status( nbrs1.xminus==3 && nbrs1.xplus==2 &&
              nbrs1.yminus==1 && nbrs1.yplus==1 &&
              nbrs1.zminus==1 && nbrs1.zplus==1, "rank 1 z+ neighbor" );

      status( nbrs0z.xminus==0 && nbrs0z.xplus==0 &&
              nbrs0z.yminus==0 && nbrs0z.yplus==0 &&
              nbrs0z.zminus==0 && nbrs0z.zplus==0, "rank 0 x-bundle neighbors" );

      status( nbrs0x.xminus==0 && nbrs0x.xplus==0 &&
              nbrs0x.yminus==0 && nbrs0x.yplus==0 &&
              nbrs0x.zminus==0 && nbrs0x.zplus==0, "rank 0 z-bundle neighbors" );

      overall( status.ok(), "mesh3, 4 processors periodic" );
    }

    {
      //Was throwing an error for Derek.
      const LBMS::MeshPtr mesh3( new LBMS::Mesh( IntVec(67,67,67), IntVec(268,268,268),
              length, true, true, true ) );
      const Partitioner p( mesh3, 4 );
    }

    {
        //Loop through a set of point and processor counts looking for errors that aren't assinging too many processors to
        //the problem to more rigorously test the partitioner.
        TestHelper status(false);
        //ratio
        int r = 2;
        //x dir
        for(int i=21; i<48; i=i+4)
        {
            //y dir
            for(int j=21; j<48; j=j+4)
            {
                //z dir
                for(int k=21; k<48; k=k+4)
                {
                     //processor count
                     for(int p=3; p<48; p=p+4)
                     {
                          try{
                              LBMS::MeshPtr mesh_t(new LBMS::Mesh(IntVec(i,j,k),IntVec(i*r, j*r, k*r), length, true, true, true));
                              Partitioner par(mesh_t, p);
                          }
                          catch(std::exception& err)
                          {
                             const char * test = strstr(err.what(), "Too many processors");
                             if(test == NULL){
                                std::ostringstream msg;
                                msg<<std::endl<<std::endl<<"Problem not assigned: "<<std::endl<<err.what()<<std::endl
                                        <<" Coarse: ( "<<i<<", "<<j<<", "<<k<<" ) "<<"  Ratio: "<<r<<" Nprocs: "<<p<<std::endl<<std::endl;
                                status(false, msg.str());
                              }
                              p=100;
                          }
                     }
               }
           }
       }
       overall(status.ok(),"Loop through");
    }
  }
  catch( std::exception& err ){
    cout << endl << err.what() << endl;
    return -1;
  }
  if( overall.ok() ){
    proc0cout << std::endl << "------------------------------------" << std::endl << std::endl
              << "PASS" << endl;
    return 0;
  }
  cout << "FAIL" << endl;
  return -1;
}
