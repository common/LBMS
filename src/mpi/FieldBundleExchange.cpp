/**
 *  \file   FieldBundleExchange.cpp
 *
 *  \date   2012
 *  \author James C. Sutherland
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "FieldBundleExchange.h"
#include <mpi/FieldExchangeInfo.h>
#include <fields/Fields.h>
#include <mpi/Environment.h>
#include <mpi/CoarseFieldPollWorker.h>
#include <mpi/GhostFieldPollWorker.h>
#include <lbms/Bundle.h>
#include <lbms/bcs/PeriodicBC.h>

#include <spatialops/structured/SpatialField.h>

#include <expression/ExpressionFactory.h>
#include <expression/Expression.h>
#include <expression/Poller.h>

#include <algorithm>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/functional/hash.hpp>
#include <boost/static_assert.hpp>

using std::vector;

using SpatialOps::IntVec;
using SpatialOps::MemoryWindow;

namespace LBMS {


  template<typename FieldT>
  void
  setup_ghost_exchange( Expr::ExpressionFactory& factory,
                        const BundlePtr b,
                        const Expr::Tag& tag,
                        const LBMS::Faces f,
                        const int id )
  {
    // set up the send stuff:
    Expr::ExpressionBase& expr = factory.retrieve_expression( tag, false );
    GhostFieldSendHelper<FieldT>* sender = new GhostFieldSendHelper<FieldT>( b, tag, f, id );
    dynamic_cast<Expr::Expression<FieldT>&>(expr).process_after_evaluate( boost::ref(*sender), sender->is_gpu_runnable() );
    factory.get_nonblocking_poller(tag)->add_new( Expr::PollWorkerPtr( sender ) );

    // set up the receive stuff:
    factory.get_poller(tag)->add_new( Expr::PollWorkerPtr( new GhostFieldPollWorker<FieldT>(b, tag, f, id) ) );
  }

  //----------------------------------------------------------------------------

  template<typename FieldT>
  bool bundles_overlap( const Direction srcBundleDir,
                        const int srcID,
                        const Direction destBundleDir,
                        const int destID )
  {
    // Coarse Bundles always overlap except for when the third direction doesn't overlap.
    const Direction third = srcBundleDir==XDIR
                            ? ( destBundleDir==YDIR ? ZDIR :YDIR )
                            : ( srcBundleDir ==YDIR
                                ? ( destBundleDir==XDIR ? ZDIR : XDIR )
                                : ( destBundleDir==XDIR ? YDIR : XDIR )
                              );

    const IntVec  srcOrigin = Environment::partitioner().origin_index_coarse( srcID, srcBundleDir);
    const IntVec destOrigin = Environment::partitioner().origin_index_coarse(destID,destBundleDir);

    const Partitioner& partition = Environment::partitioner();
    const IntVec  srcRange = partition.owned_size_coarse( srcBundleDir, srcID);
    const IntVec destRange = partition.owned_size_coarse(destBundleDir,destID);

    // there is overlap if there is a nonzero intersection of the src & dest bundles
    const int loBound = std::max( srcOrigin[third], destOrigin[third] );
    const int hiBound = std::min( srcOrigin[third]+srcRange[third],  destOrigin[third]+destRange[third] );

    if( loBound >= hiBound ) return false;

    return true;
  }

  //----------------------------------------------------------------------------

  template<typename FieldT>
  void
  communicate_expression_result( const Expr::Tag& tag,
                                 const BundlePtr bundle,
                                 Expr::ExpressionFactory& factory )
  {
    FieldBundleExchange<FieldT> fbe( bundle, tag, factory );
  }

  //=================================================================

  template<typename FieldT>
  FieldBundleExchange<FieldT>::
  FieldBundleExchange( const BundlePtr bundle,
                       const Expr::Tag fieldTag,
                       Expr::ExpressionFactory& factory )
  : xmID_( Environment::neighbor_id(bundle,XMINUS) ),
    xpID_( Environment::neighbor_id(bundle,XPLUS ) ),
    ymID_( Environment::neighbor_id(bundle,YMINUS) ),
    ypID_( Environment::neighbor_id(bundle,YPLUS ) ),
    zmID_( Environment::neighbor_id(bundle,ZMINUS) ),
    zpID_( Environment::neighbor_id(bundle,ZPLUS ) ),
    bundle_(bundle),
    fieldTag_( fieldTag )
  {
    const int myID = Environment::rank();

    const bool doX( bundle->npts( LBMS::XDIR ) > 1 );
    const bool doY( bundle->npts( LBMS::YDIR ) > 1 );
    const bool doZ( bundle->npts( LBMS::ZDIR ) > 1 );

    const bool doXMinus = (myID != xmID_);   const bool doXPlus = (myID != xpID_);
    const bool doYMinus = (myID != ymID_);   const bool doYPlus = (myID != ypID_);
    const bool doZMinus = (myID != zmID_);   const bool doZPlus = (myID != zpID_);

    if( doXMinus ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, XMINUS, xmID_ );
    if( doXPlus  ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, XPLUS,  xpID_ );
    if( doYMinus ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, YMINUS, ymID_ );
    if( doYPlus  ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, YPLUS,  ypID_ );
    if( doZMinus ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, ZMINUS, zmID_ );
    if( doZPlus  ) setup_ghost_exchange<FieldT>( factory, bundle_, fieldTag_, ZPLUS,  zpID_ );

    /*
     * Periodic boundary conditions must be handled separately when there is
     * only one process in a given direction since no ghost exchange will occur.
     */
    if( doX ){
      if( !doXMinus && !doXPlus && (bundle_->get_boundary_type(XPLUS) == PeriodicBoundary || bundle_->get_boundary_type(XMINUS) == PeriodicBoundary )){
        PeriodicBC<FieldT> perix( XDIR, bundle_, fieldTag_, factory );
      }
    }
    if( doY ){
      if( !doYMinus && !doYPlus && (bundle_->get_boundary_type(YPLUS) == PeriodicBoundary || bundle_->get_boundary_type(YMINUS) == PeriodicBoundary )){
        PeriodicBC<FieldT> periy( YDIR, bundle_, fieldTag_, factory );
      }
    }
    if( doZ ){
      if( !doZMinus && !doZPlus && (bundle_->get_boundary_type(ZPLUS) == PeriodicBoundary || bundle_->get_boundary_type(ZMINUS) == PeriodicBoundary )){
        PeriodicBC<FieldT> periz( ZDIR, bundle_, fieldTag_, factory );
      }
    }
  }

  template<typename FieldT>
  FieldBundleExchange<FieldT>::~FieldBundleExchange()
  {}

  //===================================================================

  template<typename FieldT>
  CoarseFieldSendHelper<FieldT>::CoarseFieldSendHelper( const BundlePtr b,
                                                        const Expr::Tag& t,
                                                        const LBMS::Direction srcBundleDir,
                                                        const LBMS::Direction targetBundleDir )
    : bundle_(b),
      fieldTag_(t),
      dir_ ( bundle_->dir()  ),
      dir1_( get_perp1(dir_) ),
      dir2_( get_perp2(dir_) ),
      ids1_( Environment::partitioner().get_owner_ids(dir1_) ),
      ids2_( Environment::partitioner().get_owner_ids(dir2_) ),
      srcBundleDir_   ( srcBundleDir    ),
      targetBundleDir_( targetBundleDir )
    {
      // does not work for volume fields right now, since there isn't a unique transfer defined
      // dac We now send volume fields.  Should this check be reimplemented in another form
//      BOOST_STATIC_ASSERT( bool( !SpatialOps::IsSameType<typename FieldT::Location,SpatialOps::NODIR>::result ) );

      // we only allow sending face fields from the bundle that they natively reside on except for volume fields
      assert( dir_ == direction<typename FieldT::Location::FaceDir>() || direction<typename FieldT::Location::FaceDir>() == LBMS::NODIR );
    }


  template<typename FieldT>
  CoarseFieldSendHelper<FieldT>::~CoarseFieldSendHelper()
  {
    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo1_ ){
      delete fi;
    }
    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo2_ ){
      delete fi;
    }
  }

  template<typename FieldT>
  void
  CoarseFieldSendHelper<FieldT>::operator()( FieldT& field )
  {
    const int myID = Environment::rank();

    // jcs there should be a cleaner way of doing this.  Perhaps we should force specification of all target bundles rather than trying to infer special cases.
    if( bundle_->npts(dir1_) > 1 && ( dir1_ == targetBundleDir_ || srcBundleDir_ == targetBundleDir_ ) ){

      // jcs some of this logic could be moved to the constructor
      if( ids1_.size() != exchInfo1_.size() ){
        for(std::set<int>::iterator id = ids1_.begin(); id != ids1_.end(); ){
          if( LBMS::bundles_overlap<FieldT>( dir_, myID, dir1_, *id ) ){
            exchInfo1_.push_back( new CoarseExchangeFieldInfo<FieldT>( SEND, field, dir_, myID, dir1_, *id ));
            ++id;
          }
          else{
            ids1_.erase(id++);
          }
        }
      }
      BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo1_ ){
       fi->exchange(fieldTag_, field );
      }//End Boost
    }//End if

    // jcs there should be a cleaner way of doing this.  Perhaps we should force specification of all target bundles rather than trying to infer special cases.
    if( bundle_->npts(dir2_) > 1 && ( dir2_ == targetBundleDir_ || srcBundleDir_ == targetBundleDir_ ) ){
      if( ids2_.size() != exchInfo2_.size() ){
        for(std::set<int>::iterator id = ids2_.begin(); id != ids2_.end(); ){
          if( LBMS::bundles_overlap<FieldT>( dir_, myID, dir2_, *id ) ){
            exchInfo2_.push_back( new CoarseExchangeFieldInfo<FieldT>( SEND, field, dir_, myID, dir2_, *id ));
            ++id;
          }
          else{
            ids2_.erase(id++);
          }
        }
      }
      BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo2_ ){
        fi->exchange(fieldTag_, field );
      }//End Boost
    }//End if

  }

  template<typename FieldT>
  bool
  CoarseFieldSendHelper<FieldT>::operator()()
  {
    bool isDone = true;

    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo1_ ){
        if(fi->check_status()) fi->release_buffer();
        else                   isDone=false;
    }

    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, exchInfo2_ ){
        if(fi->check_status()) fi->release_buffer();
        else                   isDone=false;
    }

    return isDone;
  }

  //=================================================================

  template<typename FieldT>
  CoarseFieldRecvHelper<FieldT>::CoarseFieldRecvHelper( const BundlePtr b, const Expr::Tag& t, const Direction srcBundleDir )
  : fieldTag_(t),
    targetBundleDir_( b->dir() ),
    srcBundleDir_( srcBundleDir ),
    ids_( Environment::partitioner().get_owner_ids(srcBundleDir_) ),
    srcfieldTag_( fieldTag_.name().substr( 0, fieldTag_.name().size()-8 ) + get_bundle_name_suffix(srcBundleDir_), fieldTag_.context())
  {
    assert( targetBundleDir_ != srcBundleDir_ );
    if( b->npts(srcBundleDir_) <= 1 ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
           << "Attempting to recieve a field which doesn't exist, \nFieldDir: "
           <<srcBundleDir_<<",  Points: "<<b->npts() << ",  Tag: "  << fieldTag_ << std::endl;
        throw std::runtime_error( msg.str() );
    }
  }

  //=================================================================

  template<typename FieldT>
  CoarseFieldRecvHelper<FieldT>::~CoarseFieldRecvHelper()
  {
    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, info_ ){
      delete fi;
    }
  }

  template<typename FieldT>
  bool
  CoarseFieldRecvHelper<FieldT>::is_ready( FieldT& field )
  {
    if( ids_.size() != info_.size() ){
      const int myID = Environment::rank();
      for(std::set<int>::iterator it = ids_.begin(); it != ids_.end(); ){
        if( LBMS::bundles_overlap<FieldT>(srcBundleDir_, *it, targetBundleDir_, myID ) ){
          info_.push_back( new CoarseExchangeFieldInfo<FieldT>( RECEIVE, field, srcBundleDir_, *it, targetBundleDir_, myID ) );
          ++it;
        }
        else{
          ids_.erase(it++);
        }
      }
    }

    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, info_ ){
      if( !fi->is_recv_ready( srcfieldTag_, field ) ) return false;
    }
    return true;
  }

  template<typename FieldT>
  void
  CoarseFieldRecvHelper<FieldT>::operator()( FieldT& field )
  {
    if( ids_.size() != info_.size() ){
      const int myID = Environment::rank();
      for( std::set<int>::iterator it = ids_.begin(); it != ids_.end(); ){
        if( LBMS::bundles_overlap<FieldT>(srcBundleDir_, *it, targetBundleDir_, myID ) ){
          info_.push_back( new CoarseExchangeFieldInfo<FieldT>( RECEIVE, field, srcBundleDir_, *it, targetBundleDir_, myID ) );
          ++it;
        }
        else{
          ids_.erase(it++);
        }
      }
    }

    assert( ids_.size() == info_.size() );
    // if we get here, then the poller has already determined that the message is ready.
    // Therefore, we block (wait) to ensure that it is ready.
    BOOST_FOREACH( FieldExchangeInfo<FieldT>* fi, info_ ){
      fi->wait();
      fi->exchange( srcfieldTag_, field );
    }
  }

  //============================================================================

  template<typename FieldT>
  GhostFieldSendHelper<FieldT>::
  GhostFieldSendHelper( const BundlePtr b,   const Expr::Tag& t,
                        const LBMS::Faces f, const int id )
  : exchangeInfo_( NULL ),
    fieldTag_(t),
    dir_(b->dir()),
    face_(f),
    id_(id)
  {}

  template<typename FieldT>
  GhostFieldSendHelper<FieldT>::~GhostFieldSendHelper()
  {
    if( exchangeInfo_ ) delete exchangeInfo_;
  }

  template<typename FieldT>
  void
  GhostFieldSendHelper<FieldT>::operator()( FieldT& f )
  {
    if( !exchangeInfo_ )
      exchangeInfo_ = new GhostExchangeFieldInfo<FieldT>( SEND, f, dir_, Environment::rank(), id_, face_ );
    exchangeInfo_->exchange( fieldTag_, f );
  }

  template<typename FieldT>
  bool
  GhostFieldSendHelper<FieldT>::operator()()
  {
    if( !exchangeInfo_ )
      exchangeInfo_ = new GhostExchangeFieldInfo<FieldT>( SEND, dir_, Environment::rank(), id_, face_ );

    // This is called from the ExprLib scheduler as a non-blocking poller.
    // That means that it will be called periodically during the graph execution
    // until it returns "true" at which point it will be disabled until the next
    // time the node that it is associated with is evaluated.
    bool isDone = true;

    if( exchangeInfo_->check_status() ){
      exchangeInfo_->release_buffer();
    }
    else{
      isDone = false;
    }
    return isDone;
  }

  template<typename FieldT>
  GhostFieldRecvHelper<FieldT>::
  GhostFieldRecvHelper( const BundlePtr b, const Expr::Tag& t, const LBMS::Faces f, const int id )
  : fieldTag_(t),
    dir_(b->dir()),
    face_(f),
    id_(id),
    fInfo_(NULL)
  {}

  template<typename FieldT>
  GhostFieldRecvHelper<FieldT>::~GhostFieldRecvHelper()
  {
    if( fInfo_ ) delete fInfo_;
  }

  template<typename FieldT>
  void
  GhostFieldRecvHelper<FieldT>::set_fi( const FieldT& f )
  {
    assert( !fInfo_ );
    fInfo_ = new GhostExchangeFieldInfo<FieldT>( RECEIVE, f, dir_, Environment::rank(), id_, face_ );
  }

  template<typename FieldT>
  bool GhostFieldRecvHelper<FieldT>::is_ready( FieldT& f )
  {
    if( !fInfo_ ) this->set_fi( f );
    return fInfo_->is_recv_ready( fieldTag_, f );
  }

  template<typename FieldT>
  void
  GhostFieldRecvHelper<FieldT>::operator()( FieldT& f )
  {
    if( !fInfo_ ) this->set_fi(f);
    fInfo_->wait();
    fInfo_->exchange( fieldTag_, f );
  }

  //===================================================================

  template< typename FieldT >
  void
  communicate_coarse_expression_result( Expr::ExpressionFactory& factory,
                                        const BundlePtr bundle,
                                        const Expr::Tag& fieldTag,
                                        const LBMS::Direction srcBundleDir,
                                        const LBMS::Direction targetBundleDir )
  {
    const Direction fieldDir = direction<typename FieldT::Location::FaceDir>();
    if( fieldDir != LBMS::NODIR ){
      assert( srcBundleDir == fieldDir );
    }
    if( srcBundleDir == bundle->dir() ){
      // the field is aligned with the bundle direction, which means it is
      // native to the bundle and we will be sending it.
      CoarseFieldSendHelper<FieldT>* sender = new CoarseFieldSendHelper<FieldT>( bundle, fieldTag, srcBundleDir, targetBundleDir );
      Expr::ExpressionBase& expr = factory.retrieve_expression( fieldTag, false );
      dynamic_cast<Expr::Expression<FieldT>&>(expr).process_after_evaluate( boost::ref(*sender), sender->is_gpu_runnable() );
      factory.get_nonblocking_poller(fieldTag)->add_new( Expr::PollWorkerPtr( sender ) );
    }
    else{
      // Not aligned with the bundle, which means we will be receiving it.
      // Set up poll workers.
      typedef CoarseFieldPollWorker<FieldT> PollWorker;
      factory.get_poller(fieldTag)->add_new( Expr::PollWorkerPtr( new PollWorker( fieldTag, Environment::bundle(targetBundleDir), srcBundleDir ) ) );
    }
  }

  //===================================================================

  using SpatialOps::FaceTypes;

#define INSTANTIATE( T )                             \
    template void communicate_expression_result<T>(  \
        const Expr::Tag&,                            \
        const BundlePtr,                             \
        Expr::ExpressionFactory& );                  \
    template class FieldBundleExchange<T>;           \
    template class GhostFieldRecvHelper<T>;          \
    template class GhostFieldSendHelper<T>;

#define INSTANTIATE_VARIANTS( Vol )       \
    INSTANTIATE(           Vol         )  \
    INSTANTIATE( FaceTypes<Vol>::XFace )  \
    INSTANTIATE( FaceTypes<Vol>::YFace )  \
    INSTANTIATE( FaceTypes<Vol>::ZFace )

  INSTANTIATE_VARIANTS( XVolField )
  INSTANTIATE_VARIANTS( YVolField )
  INSTANTIATE_VARIANTS( ZVolField )

#define COARSE_SEND_RECV(T)                               \
  template void communicate_coarse_expression_result<T>(  \
      Expr::ExpressionFactory&,                           \
      const BundlePtr,                                    \
      const Expr::Tag&,                                   \
      const LBMS::Direction,                              \
      const LBMS::Direction);                             \
  template class CoarseFieldRecvHelper<T>;                \
  template class CoarseFieldSendHelper<T>;

  COARSE_SEND_RECV( SpatialOps::SSurfXField )
  COARSE_SEND_RECV( SpatialOps::SSurfYField )
  COARSE_SEND_RECV( SpatialOps::SSurfZField )
  COARSE_SEND_RECV( SpatialOps::SVolField   )

} /* namespace LBMS */
