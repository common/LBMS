set( src
 CoarseFieldPollWorker.cpp
 GhostFieldPollWorker.cpp
 Environment.cpp
 FieldBundleExchange.cpp
 FieldExchangeInfo.cpp
 Partitioner.cpp
 PartitionHelper.cpp
)

add_subdirectory( test )

# advertise this upward, appending the path information...
get_filename_component( dir ${CMAKE_CURRENT_LIST_FILE} DIRECTORY )
foreach( file ${src} )
  list( APPEND SRCS ${dir}/${file} )
endforeach()
set( LBMS_SRC ${LBMS_SRC} ${SRCS} PARENT_SCOPE )