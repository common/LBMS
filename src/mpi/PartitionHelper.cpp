/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   PartitionHelper.cpp
 */

#include "PartitionHelper.h"
#include "Environment.h"
#include <boost/foreach.hpp>

namespace LBMS{

  PartitionHelper::PartitionHelper( const SpatialOps::IntVec & nCoarse, const SpatialOps::IntVec & nFine )
  : nCoarse_( nCoarse ),
    nFine_  ( nFine   ),
    maxs_( nCoarse_[0]>1 ? std::floor(nCoarse_[0]/2.) : 1,
           nCoarse_[1]>1 ? std::floor(nCoarse_[1]/2.) : 1,
           nCoarse_[2]>1 ? std::floor(nCoarse_[2]/2.) : 1 ),
    maxx_( maxs_[0] ),
    maxy_( maxs_[1] ),
    maxz_( maxs_[2] ),
    maxDir_( (nCoarse_[0] > 1) ? maxs_[1]*maxs_[2] : 0,
             (nCoarse_[1] > 1) ? maxs_[0]*maxs_[2] : 0,
             (nCoarse_[2] > 1) ? maxs_[1]*maxs_[0] : 0 ),
    maxxproc_( maxDir_[0] ),
    maxyproc_( maxDir_[1] ),
    maxzproc_( maxDir_[2] ),
    maxTwoDir_( nCoarse_[0]>1 ? std::max(maxy_, maxz_):0,
                nCoarse_[1]>1 ? std::max(maxx_, maxz_):0,
                nCoarse_[2]>1 ? std::max(maxy_, maxx_):0 ),
    maxproc_( maxDir_.sum() )
    {
    // set bundle weights
    const WeightVec ws( nFine_[0]*nCoarse_[1]*nCoarse_[2],
                        nCoarse_[0]*nFine_[1]*nCoarse_[2],
                        nCoarse_[0]*nCoarse_[1]*nFine_[2] );

    //-- compute (normalized) loadings to determine appropriate processor distribution
    //-- be careful of the situation where we only have one line in a given dir.
    loading_ = WeightVec();
    loading_[0] = 1./(1.+ws[1]/ws[0]+ws[2]/ws[1]);
    loading_[1] = nCoarse_[1]>1 && nCoarse_[0]*nCoarse_[2] > 1 ? loading_[0]*ws[1]/ws[0] : 0;
    loading_[2] = nCoarse_[2]>1 && nCoarse_[0]*nCoarse_[1] > 1 ? loading_[0]*ws[2]/ws[0] : 0;
    if( nCoarse_[1]*nCoarse_[2] == 1 || !(nCoarse_[0]>1)) loading_[0]=0;
    const float sum_ = loading_.sum();
    if( sum_ != 1 ){
      loading_ = loading_/sum_;
    }
  }

  std::set<size_t>
  PartitionHelper::get_valid_nums( const size_t num1, const size_t num2 ) const
  {
    //loop through num1 and num2 getting all combinations of the them, which are all valid process counts in a given direction
    std::set<size_t> ret;
    for(size_t i = 0; i<=num1; ++i){
      for(size_t j = 0; j<=num2; ++j ){
        ret.insert(i*j);
      }
    }
    return ret;
  }

  /**
   * @brief gets the next valid number from the set, above the in value.
   */
  size_t
  PartitionHelper::nearest_valid( const size_t in, const std::set<size_t> & theSet ) const
  {
    BOOST_FOREACH( const size_t ii, theSet ){
      if( ii >= in ) return ii;
    }
    std::ostringstream msg;
    msg<<"The in value is greater than all numbers in the set. This should never happen "<<__FILE__<<" : "<<__LINE__<<std::endl
       <<"In value: "<<in<<std::endl;
    throw std::runtime_error(msg.str());
    return 0;
  }

  /**
   *  @brief returns a valid processor assignment, given the maxs and a trial number
   */
  ProcVec
  PartitionHelper::proc_assignment( ProcVec trial, const ProcVec& maxs, const size_t nproc_) const
  {
    const std::set<size_t> xvalidnumbers = get_valid_nums(maxs[1], maxs[2]); //get the valid numbers in the x direction given the y and z maxs
    const std::set<size_t> yvalidnumbers = get_valid_nums(maxs[0], maxs[2]);
    const std::set<size_t> zvalidnumbers = get_valid_nums(maxs[0], maxs[1]);

    //We always want to use all processors make sure that happens
    if(trial.sum()<nproc_)
    {
        size_t lowerthan = nproc_-trial.sum();
        if(trial[0]<maxxproc_){
            while(lowerthan != 0 && trial[0]+1<=maxxproc_)
            {
                trial[0] += 1;
                lowerthan -= 1;
            }
        }
        if(trial[1]<maxyproc_)
        {
            while(lowerthan != 0 && trial[1]+1<=maxyproc_)
            {
                trial[1] += 1;
                lowerthan -= 1;
            }
        }
        if(trial[2]<maxzproc_)
        {
            while(lowerthan != 0 && trial[2]+1<=maxzproc_)
            {
                trial[2] += 1;
                lowerthan -= 1;
            }
        }
    }
    //if the trial number is not one of the valid numbers then replace it with a valid number which is greater than trial.
    //This can and will place multiple bundles per process.
     if( !contains(xvalidnumbers, trial[0]) ) trial[0] = nearest_valid(trial[0], xvalidnumbers);
     if( !contains(yvalidnumbers, trial[1]) ) trial[1] = nearest_valid(trial[1], yvalidnumbers);
     if( !contains(zvalidnumbers, trial[2]) ) trial[2] = nearest_valid(trial[2], zvalidnumbers);
    return trial;
  }

  /**
   * @brief The helper function which takes in the number of processors, the number of coarse grid points, and the number of fine grid points
   * It turns these into a size_t 3 vec which is the processor counts for x,y,z.
   *
   */

  ProcVec
  PartitionHelper::check_proc_number( const size_t nproc_, const bool output ) const
  {
    Sieve<size_t> sieve;

    if( nproc_ > maxproc_ ){
      std::ostringstream msg;
      msg << "Too many processors (" << nproc_ << ") were provided for this problem size \n"
          << "Subject to other constraints the maximum number allowed is " << maxproc_
          << ".\nPlease repartition your problem.  Grid information follows:"
          << "\n\tnCoarse: " << nCoarse_
          << "\n\tnFine  : " << nFine_
          << "\nError thrown from\n\t" << __FILE__ << " : " << __LINE__ << std::endl;
      throw std::runtime_error(msg.str());
    }
    if(nproc_ == maxproc_)
    {
        return ProcVec(maxxproc_, maxyproc_, maxzproc_);
    }

    const ProcVec nprocloading = loading_*nproc_;

    //calculates the base number of processors in each direction, limiting based on the max processor count in each direction.
    const ProcVec baseprocs( std::min(nprocloading[0], maxxproc_),
                             std::min(nprocloading[1], maxyproc_),
                             std::min(nprocloading[2], maxzproc_) );

    //capture any leftover processors
    size_t nextra=0;
    if(baseprocs.sum()< maxproc_){
      nextra = nproc_ - baseprocs.sum();
    }
    const int num_dirs =  (nCoarse_[0]>1) + (nCoarse_[1]>1)+(nCoarse_[2]>1);
    
    const ProcVec nextradis = ProcVec(nCoarse_[0]>1?size_t(nextra/num_dirs)+ ( nextra%num_dirs > 0 ? 1 : 0 ):0, (nCoarse_[1]>1) ? size_t(nextra/num_dirs)+ ( nextra%num_dirs > 1 ? 1 : 0 ) : 0, (nCoarse_[2]>1) ? size_t(nextra/num_dirs)+ (size_t(nextra%num_dirs)>1? 1:0): 0);
    //use up the left over processors
    const ProcVec cbs = baseprocs + nextradis;
    const size_t& cxb = cbs[0];
    const size_t& cyb = cbs[1];
    const size_t& czb = cbs[2];
    //the return processor counts.
    ProcVec tnb;
    //Weighting done, now need to check if the processor counts are sane
    //If the process assignments are large then use large proc assignment, otherwise reshuffle around the primes.
    
    //Get the trial numbers
      size_t tnxb = cxb;
      size_t tnyb = cyb;
      size_t tnzb = czb;


      const size_t sum_tn = tnxb+tnyb+tnzb;
      const bool ngts = nproc_>=sum_tn;
      const size_t leftovers=ngts?nproc_-sum_tn:sum_tn-nproc_; //leftover processors

      if( leftovers != 0 ){
        if( output ){
          proc0cout << "Change in layout happening "<<std::endl
                    << "Current Layout: " << tnxb << " " << tnyb << " " << tnzb << "  Leftover: " << leftovers << std::endl
                    << " " << sieve.is_prime(tnxb+leftovers)
                    << " " << sieve.is_prime(tnyb+leftovers)
                    << " " << sieve.is_prime(tnzb+leftovers)
                    <<std::endl;
        }

       //Create the pair given the trial number and the direction number for sorting purposes
        const std::pair<size_t*, int> xpair(&tnxb,0);
        const std::pair<size_t*, int> ypair(&tnyb,1);
        const std::pair<size_t*, int> zpair(&tnzb,2);

        //Sort the trial numbers
        const std::pair<size_t*, int> & max =  (*xpair.first< ( (*ypair.first<*zpair.first) ? *zpair.first : *ypair.first) ) ? ( (*ypair.first<*zpair.first) ? zpair : ypair ) : xpair;
        const std::pair<size_t*, int> & min = !(*xpair.first< (!(*ypair.first<*zpair.first) ? *zpair.first : *ypair.first) ) ? (!(*ypair.first<*zpair.first) ? zpair : ypair ) : xpair;
        const std::pair<size_t*, int> & remain = xpair != max && xpair != min ? xpair : ypair != max && ypair != min ? ypair: zpair;

        const size_t minr = (ngts?*min.first +leftovers:leftovers<*min.first?*min.first-leftovers:maxDir_[min.second]+1);
        const size_t maxr = (ngts?*max.first +leftovers:leftovers<*max.first?*max.first-leftovers:maxDir_[max.second]+1);
        const size_t remainr = (ngts?*remain.first+leftovers:leftovers<*remain.first?*remain.first-leftovers:maxDir_[remain.second]+1);
        //Distribute the leftovers based on the sorting.
        if(  minr <=maxDir_[min.second] && !(sieve.is_prime(minr) ) ){
          *min.first = minr;
        }
        else if( remainr <=maxDir_[remain.second] && !(sieve.is_prime(remainr) ) ){
          *remain.first = remainr;
        }
        else if( maxr<=maxDir_[max.second] && !(sieve.is_prime(maxr) ) ){
          *max.first = maxr;
        }
      }
        ProcVec trial = ProcVec(tnxb, tnyb, tnzb);
        if(tnxb > maxxproc_){
            trial[0]=maxxproc_;
        }
        if(tnyb > maxyproc_)
        {
            trial[1]=maxyproc_;
        }
        if(tnzb > maxzproc_)
        {
            trial[2]=maxzproc_;
        }

       //Check sanity of result and fix if there are problems.
       tnb = proc_assignment(trial, maxs_, nproc_);
    if( output ){
      proc0cout << "Current Layout: " << tnb[0] << " " << tnb[1] << " " << tnb[2] << std::endl;
    }
    return tnb;
  }

}
