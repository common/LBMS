/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 *  \file   FieldExchangeInfo.cpp
 *  \date   Aug 1, 2013
 *  \author "James C. Sutherland"
 */

#include "FieldExchangeInfo.h"
#include <mpi/Environment.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>
#include <spatialops/structured/FieldHelper.h>

#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

using SpatialOps::IntVec;
using SpatialOps::MemoryWindow;

//#define DUMP_DIAGNOSTIC_OUTPUT

class BufferHelper
{
  class ExchangeWrapper{
    std::vector<double> vec_;
    bool isLocal_;
  public:
    ExchangeWrapper( const size_t n )
    : vec_( n ), isLocal_(false)
    {}
    inline void buffer_is_local( const bool b ){ isLocal_=b; }
    inline bool is_buffer_local() const{ return isLocal_; }
    inline std::vector<double>& vec(){ return vec_; }
  };

  typedef boost::shared_ptr<ExchangeWrapper> ExchangeWrapPtr;
  typedef std::map< std::pair<int,int>, ExchangeWrapPtr >  ExchangeMap;
  ExchangeMap exchangeBuffers_; // active buffers
  ExchangeMap holdingBuffers_;  // inactive (available) buffers
  static BufferHelper& self(){
    static BufferHelper bh;
    return bh;
  }
public:
  static void buffer_is_local_exchange( const std::pair<int,int> id )
  {
    BufferHelper& bh = self();
    ExchangeMap::iterator iexchBuf = bh.exchangeBuffers_.find( id );
    if( iexchBuf != bh.exchangeBuffers_.end() )
      iexchBuf->second->buffer_is_local(true);
  }

  static bool is_buffer_local_exchange( const std::pair<int,int> id )
  {
    BufferHelper& bh = self();
    ExchangeMap::iterator iexchBuf = bh.exchangeBuffers_.find( id );
    if( iexchBuf != bh.exchangeBuffers_.end() )
      return iexchBuf->second->is_buffer_local();
    return bh.holdingBuffers_.find( id )->second->is_buffer_local();
  }

  static std::vector<double>& get_buffer( const std::pair<int,int> id, const size_t npts )
  {
    //    std::cout << "retrieving buffer for " << id << std::endl;
    BufferHelper& bh = self();
    ExchangeMap::iterator iexchBuf = bh.exchangeBuffers_.find( id );

    if( iexchBuf == bh.exchangeBuffers_.end() ){
      ExchangeMap::iterator iholdBuf = bh.holdingBuffers_.find( id );
      if( iholdBuf == bh.holdingBuffers_.end() ){
        // no buffers available.  Build a new one.
        ExchangeWrapPtr ptr( new ExchangeWrapper( npts ) );
        iexchBuf = bh.exchangeBuffers_.insert( iexchBuf, std::make_pair(id,ptr) );
      }
      else{
        iexchBuf = bh.exchangeBuffers_.insert( iexchBuf, std::make_pair(id,iholdBuf->second) );
      }
    }
    else{
      std::ostringstream msg;
      msg << "Attempting to overwrite exchange buffers Already exists in map.\n"
          << "\tfound: " << id << " on send \n"
          << std::endl;
      throw std::runtime_error( msg.str() );
    }
    return iexchBuf->second->vec();
  }

  static bool has_active_buffer( const std::pair<int,int> id )
  {
    const BufferHelper& bh = self();
    return ( bh.exchangeBuffers_.find( id ) != bh.exchangeBuffers_.end() );
  }

  static std::vector<double>& get_active_buffer( const std::pair<int,int> id )
  {
    BufferHelper& bh = self();
    ExchangeMap::iterator ibuf = bh.exchangeBuffers_.find( id );
    if( ibuf == bh.exchangeBuffers_.end() ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Could not obtain an active buffer for id " << id
          << "\nAvailable tags follow: " << std::endl;
      for( ExchangeMap::const_iterator i=bh.exchangeBuffers_.begin(); i!=bh.exchangeBuffers_.end(); ++i ){
        msg << "\t" << i->first << "\t(" << i->second->vec().size() << " pts)" << std::endl;
      }
      throw std::runtime_error( msg.str() );
    }
    return ibuf->second->vec();
  }

  static void deactivate_buffer( const std::pair<int,int> id )
  {
    BufferHelper& bh = self();
    ExchangeMap::iterator ibuf = bh.exchangeBuffers_.find( id );
    if( ibuf == bh.exchangeBuffers_.end() ) return;
    bh.holdingBuffers_[ ibuf->first ] = ibuf->second;
//    std::cout << " deleting one buffer (" << bh.exchangeBuffers_.size() << " - ";
    bh.exchangeBuffers_.erase( ibuf );
//    std::cout << bh.exchangeBuffers_.size() << ")\n";
  }

  static void deactivate_all_buffers()
  {
    BufferHelper& bh = self();
//    if( bh.exchangeBuffers_.size() > 0 )
//      std::cout << "deactivate_all_buffers -- FOUND " << bh.exchangeBuffers_.size() << " EXISTING BUFFERS!!!\n";
    ExchangeMap::iterator ibuf = bh.exchangeBuffers_.begin();
    while( ibuf != bh.exchangeBuffers_.end() ){
      bh.holdingBuffers_[ ibuf->first ] = ibuf->second;
      bh.exchangeBuffers_.erase( ibuf++ );
    }
//    std::cout << " " << bh.exchangeBuffers_.size() << std::endl;
  }

  static void free_all_buffers()
  {
    BufferHelper& bh = self();
    ExchangeMap::iterator ixch = bh.exchangeBuffers_.begin();
    while(ixch != bh.exchangeBuffers_.end()){
      bh.exchangeBuffers_.erase(ixch++);
    }
    ExchangeMap::iterator ihch = bh.holdingBuffers_.begin();
    while(ihch != bh.holdingBuffers_.end()){
      bh.holdingBuffers_.erase(ihch++);
    }
  }

};


namespace LBMS{

  void clear_send_buffer()
  {
    BufferHelper::deactivate_all_buffers();
  }

  void clear_all_buffers()
  {
    BufferHelper::free_all_buffers();
  }

  unsigned long string_hash( const std::string& str )
  {
#   ifdef HAVE_MPI
    const unsigned long maxtag = Environment::boost_mpi_env().max_tag();
#   else
    // jcs most MPI installations don't have larger than int, and the boost interface takes int as well, so don't allow anything larger than that.
    const unsigned long maxtag = std::numeric_limits<int>::max();
#   endif

    // https://en.wikipedia.org/wiki/Universal_hashing#Hashing_strings
    const unsigned long a = 33;
    unsigned long h = 5381;
    for( size_t i = 0; i < str.size(); ++i ){
      h = ( h * a + str[i] ) % maxtag;
    }

    const int itmp = h;
    if( (unsigned long)( itmp ) != h ){
      std::cout << "\tError in string_hash() on rank " << Environment::rank()
      << " : long uint -> int conversion truncates information (" << h << " -> " << itmp << ")\n";
    }

    return h;
  }

  //=================================================================

  template< typename FieldT >
  FieldExchangeInfo<FieldT>::
  FieldExchangeInfo( const CommMode mode,
                     const Direction dir,
                     const int srcID,
                     const int destID,
                     const bool isGhostExchange )
  : mode_( mode ),
    srcID_( srcID ),
    destID_( destID ),
    srcBundleDir_( dir ),
#   ifdef HAVE_MPI
    resetRequest_( true ),
#   endif
    ghost_( isGhostExchange )
  {
    hasSetFieldID_ = false;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  FieldExchangeInfo<FieldT>::~FieldExchangeInfo()
  {}

  //-----------------------------------------------------------------

  template< typename FieldT >
  int
  FieldExchangeInfo<FieldT>::
  field_tag( const Expr::Tag fieldNameTag,
             const MemoryWindow& mw,
             const size_t ID ) const
  {
    const IntVec originIndex = origin_index( mode_, mw );
    const std::string augment = get_tag_augment( mode_ );
    const int fieldOriginFlatIndex = ijk_to_flat( this->glob_field_dim(), originIndex );

    const unsigned long fieldTag = string_hash( fieldNameTag.name()
                                                + get_dir_name(srcBundleDir_)
                                                + augment
                                                + boost::lexical_cast<std::string>(fieldOriginFlatIndex) );

    //std::cout << "\t" << Environment::rank() << " : '"
//       << fieldNameTag.name() << "' on bundle dir: '"
//        << get_dir_name(srcBundleDir_) << "' with origin index: "
//      << fieldOriginFlatIndex
//       << " and extra: '" << augment << "' (" << fieldTag << ")"<< std::endl;
#   ifdef HAVE_MPI
    if( fieldTag > Environment::boost_mpi_env().max_tag() || fieldTag<0){
      std::ostringstream msg;
      msg << "field tag for " << fieldNameTag << " exceeded valid range.\n"
          << "\tfound: " << fieldTag << "\n"
          << "\t  max: " << Environment::boost_mpi_env().max_tag() << std::endl;
      throw std::runtime_error( msg.str() );
    }
#   endif

    return fieldTag;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::
  pack_buffer( const FieldT& fieldSlice,
               std::vector<typename FieldT::value_type>& buffer ) const
  {
    typename std::vector<typename FieldT::value_type>::iterator ibuf = buffer.begin();
    const typename FieldT::const_iterator iend = fieldSlice.end();
    for( typename FieldT::const_iterator i=fieldSlice.begin(); i!=iend; ++i, ++ibuf ){
      *ibuf = *i;
    }
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::
  unpack_buffer( const std::vector<typename FieldT::value_type>& buf,
                 FieldT& fieldSlice ) const
  {
    typename std::vector<typename FieldT::value_type>::const_iterator ibuf = buf.begin();
    const typename FieldT::iterator iend = fieldSlice.end();
    for( typename FieldT::iterator i=fieldSlice.begin(); i!=iend; ++i, ++ibuf ){
      *i = *ibuf;
    }
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::
  send( const Expr::Tag& fieldNameTag,
        const FieldT& field )
  {
#   if HAVE_MPI
    const int dest = destID_;
#   else
    const int dest = 0;
#   endif

    const int fieldTag = field_tag( fieldNameTag,
                                    field.window_with_ghost(),
                                    dest );

    fieldID_ = std::pair<int,int>( fieldTag, dest );

    const FieldT fSlice( this->get_src_field( field ) );
    const size_t npts = fSlice.window_with_ghost().local_npts();
    std::vector<double>& buffer = BufferHelper::get_buffer( fieldID_, npts );
    pack_buffer( fSlice, buffer );

    if( destID_!=srcID_ ){
#     ifdef HAVE_MPI
#     ifdef DUMP_DIAGNOSTIC_OUTPUT
      std::cout << Environment::rank() << " sending to " << destID_ << std::endl;
#     endif
      sendRequest_ = Environment::world().isend( destID_, fieldID_.first, &buffer[0], npts );
#     endif
    }
    else{
      BufferHelper::buffer_is_local_exchange(fieldID_);
    }
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::
  exchange( const Expr::Tag& fieldNameTag,
            FieldT& field )
  {
    switch( mode_ ){
      case RECEIVE: this->receive( fieldNameTag, field ); break;
      case SEND   : this->   send( fieldNameTag, field ); break;
    }
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::release_buffer()
  {
//    std::cout << "release_buffer()  for " << fieldID_ << std::endl;
    switch( mode_ ){
      case SEND   :  if( BufferHelper::is_buffer_local_exchange(fieldID_) ) break;
      case RECEIVE:  BufferHelper::deactivate_buffer( fieldID_ );
    }
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  bool
  FieldExchangeInfo<FieldT>::check_status()
  {
#   ifdef HAVE_MPI
    if( srcID_ != destID_ ){
      switch( mode_ ){
        case SEND   : return static_cast<bool>( sendRequest_.test()    );
        case RECEIVE: return static_cast<bool>( receiveRequest_.test() );
      }
    }
#   endif
    return true;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::wait()
  {
#   ifdef HAVE_MPI
    if(srcID_ != destID_)
    {
    switch( mode_ ){
      case SEND   : sendRequest_.wait();    break;
      case RECEIVE: receiveRequest_.wait(); break;
    }}
#   endif
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  bool
  FieldExchangeInfo<FieldT>::
  is_recv_ready( const Expr::Tag fieldNameTag,
                 FieldT& field )
  {
    assert( mode_ == RECEIVE );

#   if HAVE_MPI
    const int myID = LBMS::Environment::rank();
    const int dest = destID_;
#   else
    const int myID = 0;
    const int dest = 0;
#   endif

    // avoid repeated calls to getting the field ID by caching this. The probe
    // call may be made quite frequently, so we want it to be inexpensive
    if( !hasSetFieldID_ ){
      fieldID_ = std::pair<int,int>(field_tag( fieldNameTag,
                                               field.window_with_ghost(),
                                               myID ),
                                    dest );
      hasSetFieldID_ = true;
    }
#   ifdef DUMP_DIAGNOSTIC_OUTPUT
    std::cout << Environment::rank() << " checking for field "<<fieldNameTag<<" from " << srcID_ << " and ID: " <<fieldID_.first<<std::endl;
#   endif

#   ifdef HAVE_MPI
    if( destID_ != srcID_ ){

      if( resetRequest_ ){

        const FieldT fSlice( get_dest_field( field ) );
        const int npts = fSlice.window_with_ghost().local_npts();
        std::vector<double>& buffer = BufferHelper::get_buffer( fieldID_, npts );

        // here we post a nonblocking receive because the "eager limit" in MPI
        // can result in large messages not being sent until a matching receive
        // has been posted.  For the coarse field exchange, this is frequently
        // the case.  The "receive" method will just assume that this has been
        // properly received - i.e. you should not call receive() until this
        // method has returned "true" indicating that the message has been received.
        receiveRequest_ = Environment::world().irecv( ghost_ ? destID_ : srcID_, fieldID_.first, &buffer[0], npts );

        resetRequest_ = false;
      }

      return static_cast<bool>(receiveRequest_.test());
    }
#   endif

    return BufferHelper::has_active_buffer( fieldID_ );
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  FieldExchangeInfo<FieldT>::
  receive( const Expr::Tag& fieldNameTag,
           FieldT& field )
  {
#   ifdef DUMP_DIAGNOSTIC_OUTPUT
    std::cout << Environment::rank() << " receiving " << fieldNameTag << " from " << srcID_ << std::endl;
#   endif

#   if HAVE_MPI
    const int myID = LBMS::Environment::rank();
    const int dest = destID_;
#   else
    const int myID = 0;
    const int dest = 0;
#   endif

    // avoid repeated calls to getting the field ID by caching this. The probe
    // call may be made quite frequently, so we want it to be inexpensive
    if( !hasSetFieldID_ ){
      fieldID_ = std::pair<int,int>(field_tag( fieldNameTag,
                                               field.window_with_ghost(),
                                               myID ),
                                    dest );
      hasSetFieldID_ = true;
    }

    FieldT fSlice( get_dest_field( field ) );

    // the MPI receive has already occurred via the nonblocking receive.
#   ifdef HAVE_MPI
    resetRequest_ = true;
    assert( receiveRequest_.test() );
#   endif

    unpack_buffer( BufferHelper::get_active_buffer( fieldID_ ), fSlice );

    if( destID_ == srcID_ ) BufferHelper::buffer_is_local_exchange( fieldID_ );

    BufferHelper::deactivate_buffer( fieldID_ );
  }

  //=================================================================

  template< typename FieldT >
  GhostExchangeFieldInfo<FieldT>::
  GhostExchangeFieldInfo( const CommMode mode,
                          const FieldT& f,
                          const Direction bundleDir,
                          const int srcID,
                          const int destID,
                          const Faces face )
  : FieldExchangeInfo<FieldT>( mode, bundleDir, srcID, destID, true ),
    hasFieldInConstructor_( true ),
    face_( face ),
    nGhostMinus_( f.get_ghost_data().get_minus() ),
    nGhostPlus_ ( f.get_ghost_data().get_plus()  )
  {
    switch( face_ ){
      case XMINUS:  nghost_=nGhostMinus_[0];  nShiftSrc_=  nghost_;  nShiftDest_=0;        break;
      case XPLUS :  nghost_=nGhostPlus_ [0];  nShiftSrc_=2*nghost_;  nShiftDest_=nghost_;  break;
      case YMINUS:  nghost_=nGhostMinus_[1];  nShiftSrc_=  nghost_;  nShiftDest_=0;        break;
      case YPLUS :  nghost_=nGhostPlus_ [1];  nShiftSrc_=2*nghost_;  nShiftDest_=nghost_;  break;
      case ZMINUS:  nghost_=nGhostMinus_[2];  nShiftSrc_=  nghost_;  nShiftDest_=0;        break;
      case ZPLUS :  nghost_=nGhostPlus_ [2];  nShiftSrc_=2*nghost_;  nShiftDest_=nghost_;  break;
    }
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  GhostExchangeFieldInfo<FieldT>::
  GhostExchangeFieldInfo( const CommMode mode,
                          const Direction bundleDir,
                          const int srcID,
                          const int destID,
                          const Faces face )
  : FieldExchangeInfo<FieldT>( mode, bundleDir, srcID, destID, true ),
    hasFieldInConstructor_( false ),
    face_( face )
  {}

  //-----------------------------------------------------------------

  template< typename FieldT >
  std::string GhostExchangeFieldInfo<FieldT>::
  get_tag_augment( const CommMode mode ) const
  {
    std::string tag;
    switch( face_ ){
      case XMINUS: if( mode==SEND ) tag="XMINUS"; else tag="XPLUS" ; break;
      case XPLUS : if( mode==SEND ) tag="XPLUS" ; else tag="XMINUS"; break;
      case YMINUS: if( mode==SEND ) tag="YMINUS"; else tag="YPLUS" ; break;
      case YPLUS : if( mode==SEND ) tag="YPLUS" ; else tag="YMINUS"; break;
      case ZMINUS: if( mode==SEND ) tag="ZMINUS"; else tag="ZPLUS" ; break;
      case ZPLUS : if( mode==SEND ) tag="ZPLUS" ; else tag="ZMINUS"; break;
    }
    return tag;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  const FieldT
  GhostExchangeFieldInfo<FieldT>::
  get_src_field( const FieldT& srcField ) const
  {
    assert( hasFieldInConstructor_ );
    return get_face_field( srcField, face_, nShiftSrc_, nghost_ );
  }
  
  //-----------------------------------------------------------------

  template<typename FieldT>
  FieldT
  GhostExchangeFieldInfo<FieldT>::
  get_dest_field( FieldT& destField ) const
  {
    assert( hasFieldInConstructor_ );
    return get_face_field( destField, face_, nShiftDest_, nghost_ );
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  IntVec
  GhostExchangeFieldInfo<FieldT>::
  origin_index( const CommMode mode, const MemoryWindow& fieldWindow ) const
  {
    assert( hasFieldInConstructor_ );
    const Partitioner& p = Environment::partitioner();
    IntVec origin;

    switch( mode ){
      case RECEIVE:{
        origin = p.origin_index( this->destID_, this->srcBundleDir_ );

        const IntVec bundleSize = p.owned_size( this->srcBundleDir_, this->destID_ );
       // const NeighborIDs destNbr = p.neighbors( this->srcBundleDir_, this->destID_ );
        int nShift=0;
        if     ( this->face_ == XMINUS  ) nShift = this->nExtra_[0] + bundleSize[0];
        else if( this->face_ == YMINUS  ) nShift = this->nExtra_[1] + bundleSize[1];
        else if( this->face_ == ZMINUS  ) nShift = this->nExtra_[2] + bundleSize[2];

        origin[((this->face_==XPLUS || this->face_==XMINUS)?XDIR:((this->face_==YPLUS || this->face_==YMINUS)?YDIR:ZDIR))] += nShift;
        break;
      }
      case SEND:{
        origin = p.origin_index( this->srcID_, this->srcBundleDir_ );
        const IntVec bundleSize = p.owned_size( this->srcBundleDir_, this->srcID_ );
        int nShift = 0;

        if     ( this->face_ == XPLUS ) nShift = this->nExtra_[0] + bundleSize[0];
        else if( this->face_ == YPLUS ) nShift = this->nExtra_[1] + bundleSize[1];
        else if( this->face_ == ZPLUS ) nShift = this->nExtra_[2] + bundleSize[2];

        origin[((this->face_==XPLUS || this->face_==XMINUS)?XDIR:((this->face_==YPLUS || this->face_==YMINUS)?YDIR:ZDIR))] += nShift;
        break;
      }
    }//end switch

    return origin;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  IntVec
  GhostExchangeFieldInfo<FieldT>::glob_field_dim() const
  {
    assert( hasFieldInConstructor_ );
    IntVec dim = Environment::glob_dim(this->srcBundleDir_) + this->nExtra_ + this->nGhostMinus_ + this->nGhostPlus_;
    return dim;
  }

  //=================================================================
  //=================================================================

  template< typename FieldT >
  CoarseExchangeFieldInfo<FieldT>::
  CoarseExchangeFieldInfo( const CommMode mode,
                           const FieldT& f,
                           const Direction srcBundleDir,
                           const int srcID,
                           const Direction destBundleDir,
                           const int destID )
  : FieldExchangeInfo<FieldT>( mode, srcBundleDir, srcID, destID, false ),
    destBundleDir_( destBundleDir ),
    nGhostMinus_( f.get_ghost_data().get_minus() ),
    nGhostPlus_ ( f.get_ghost_data().get_plus()  ),
    srcOrigin_ ( Environment::partitioner().origin_index_coarse( srcID, srcBundleDir) ),
    destOrigin_( Environment::partitioner().origin_index_coarse(destID,destBundleDir) )
  {
    const Partitioner& partition = Environment::partitioner();

    const IntVec  srcRange = partition.owned_size_coarse( srcBundleDir, srcID);
    const IntVec destRange = partition.owned_size_coarse(destBundleDir,destID);

    const IntVec nxyz = Environment::glob_dim_coarse();
    const IntVec active( nxyz[0]>1 ? 1 : 0,
        nxyz[1]>1 ? 1 : 0,
            nxyz[2]>1 ? 1 : 0 );

    // src start location is at the first intersection of the two parts of the mesh
    for( size_t i=0; i<3; ++i ){
      srcFieldStartIJK_  [i] = std::max( srcOrigin_[i], destOrigin_[i] );
      destFieldStartIJK_ [i] = std::max( srcOrigin_[i], destOrigin_[i] );

      srcFieldExtentIJK_ [i] = std::min( srcRange[i]-(destOrigin_[i]-srcOrigin_[i]), destRange[i]-(srcOrigin_[i]-destOrigin_[i]) );
      destFieldExtentIJK_[i] = std::min( srcRange[i]-(destOrigin_[i]-srcOrigin_[i]), destRange[i]-(srcOrigin_[i]-destOrigin_[i]) );
      //In cases where the origins are negative, this ensures that the appropriate range is used
      srcFieldExtentIJK_ [i] = std::min( std::min( srcRange[i], destRange[i] ), srcFieldExtentIJK_ [i] );
      destFieldExtentIJK_[i] = std::min( std::min( srcRange[i], destRange[i] ), destFieldExtentIJK_[i] );
    }
    srcFieldStartIJK_  -=  srcOrigin_;
    destFieldStartIJK_ -= destOrigin_;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  IntVec
  CoarseExchangeFieldInfo<FieldT>::
  origin_index( const CommMode mode, const MemoryWindow& fieldSliceWindow ) const
  {
    // jcs need to shift by ghost cells???
    return srcOrigin_;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  const FieldT
  CoarseExchangeFieldInfo<FieldT>::
  get_src_field( const FieldT& srcField ) const
  {
    const MemoryWindow& srcFieldWindow = srcField.window_with_ghost();
    const MemoryWindow mw( srcFieldWindow.glob_dim(),
                           srcFieldStartIJK_ + nGhostMinus_, /* offset */
                           srcFieldExtentIJK_ + nGhostPlus_ /* extent */ );

    return FieldT(mw,srcField);
  }

  template<typename FieldT>
  FieldT
  CoarseExchangeFieldInfo<FieldT>::
  get_dest_field( FieldT& destField ) const
  {
    const MemoryWindow& destFieldWindow = destField.window_with_ghost();
    const MemoryWindow mw( destFieldWindow.glob_dim(),
                           destFieldStartIJK_ + nGhostMinus_,
                           destFieldExtentIJK_ + nGhostPlus_);
    return FieldT(mw,destField);
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  IntVec
  CoarseExchangeFieldInfo<FieldT>::glob_field_dim() const
  {
    return Environment::glob_dim_coarse();
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  std::string
  CoarseExchangeFieldInfo<FieldT>::get_tag_augment( const CommMode mode ) const
  {
    return get_dir_name(destBundleDir_);
  }

  //=================================================================
  //=================================================================

} // namespace LBMS

// explicit template instantiation

#define INSTANTIATE_BASE( FieldT ) template class LBMS::FieldExchangeInfo<FieldT>;


#define INSTANTIATE_GHOST_EXCHANGE( T )                 \
  template class LBMS::GhostExchangeFieldInfo<T>;       \
  INSTANTIATE_BASE( T )

#define INSTANTIATE_GHOST_EXCHANGE_GROUP( VOLT )                    \
  INSTANTIATE_GHOST_EXCHANGE( VOLT )                                \
  INSTANTIATE_GHOST_EXCHANGE( SpatialOps::FaceTypes<VOLT>::XFace )  \
  INSTANTIATE_GHOST_EXCHANGE( SpatialOps::FaceTypes<VOLT>::YFace )  \
  INSTANTIATE_GHOST_EXCHANGE( SpatialOps::FaceTypes<VOLT>::ZFace )

INSTANTIATE_GHOST_EXCHANGE_GROUP( LBMS::XVolField )
INSTANTIATE_GHOST_EXCHANGE_GROUP( LBMS::YVolField )
INSTANTIATE_GHOST_EXCHANGE_GROUP( LBMS::ZVolField )


#define INSTANTIATE_COARSE_EXCHANGE( T )                \
  template class LBMS::CoarseExchangeFieldInfo<T>;      \
  INSTANTIATE_BASE( T )

INSTANTIATE_COARSE_EXCHANGE( SpatialOps::SVolField   )
INSTANTIATE_COARSE_EXCHANGE( SpatialOps::SSurfXField )
INSTANTIATE_COARSE_EXCHANGE( SpatialOps::SSurfYField )
INSTANTIATE_COARSE_EXCHANGE( SpatialOps::SSurfZField )
