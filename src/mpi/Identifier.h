/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/** \file Identifier.h */

#ifndef LBMS_Identifier_h
#define LBMS_Identifier_h

#include <iosfwd>

namespace LBMS{

  /**
   *  @class Identifier
   *  @brief Construction of a Identifier object results in a unique
   *         identifier (unique to a given process).
   *
   *  @ingroup Comm
   */
  class Identifier
  {
  public:
    // we might have a lot of these, so make it a long int
    typedef unsigned long int  ID;

    Identifier();
    Identifier( const Identifier& );
    Identifier& operator=(const Identifier&);

    inline bool operator==(const Identifier pid ) const { return id_==pid.id_; }
    inline bool operator< (const Identifier pid ) const { return id_< pid.id_; }
    inline bool operator> (const Identifier pid ) const { return id_> pid.id_; }
    inline bool operator!=(const Identifier pid ) const { return id_!=pid.id_; }

    inline ID value() const{ return id_; }

  private:
    static ID get_id();
    ID id_;
  };

  /** \fn  std::ostream& operator<<( std::ostream&, const Identifier& );
   *  \brief output an Identifier object
   */
  std::ostream& operator<<( std::ostream&, const Identifier& );

} // namespace LBMS

#endif // LBMS_Identifier_h
