/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   Partitioner.cpp
 *  \date   Jun 7, 2012
 *  \author James C. Sutherland
 */

#include <mpi/Partitioner.h>
#include <mpi/Environment.h> // for proc0cout
#include <mpi/PartitionHelper.h>
#include <lbms/Bundle.h>

#include <cmath>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <stdexcept>
#include <sstream>

#include <boost/math/common_factor_rt.hpp>
#include <boost/foreach.hpp>

using std::cout;
using std::endl;

using SpatialOps::IntVec;

namespace LBMS {

  typedef std::pair<unsigned,unsigned>    IntPair;
  typedef std::vector<IntPair>            IntPairVec;
  typedef SpatialOps::Numeric3Vec<size_t> ProcVec;

  //=================================================================


  //=================================================================

  // obtain candidate decompositions on 2D bundles.
  //  n1 - number of points in "1" dir
  //  n2 - number of points in "2" dir
  //  nproc - number of processors desired in the decomposition
  //
  // returns a vector of candidate [np1,np2] pairs that give
  // equally spaced decompositions.
  IntPairVec
  candidate_decompositions( const unsigned n1, const unsigned n2,
                            const unsigned nproc,
                            const bool dumpDiagnostics=false )
  {
    using boost::math::gcd;

    //contains equally spaced decompositions
    IntPairVec candidates;
    //contains any valid unequally spaced decompositions
    IntPairVec uneven;

    // brute force search on dim 1
    const unsigned max = nproc;
    for( unsigned np1=max; np1>0; np1-- ){
      if( n1%np1 == 0 && n1/np1 >1){ // np1 is a candidate - check np2
        const unsigned np2 = nproc/np1;
        if( dumpDiagnostics ){
          proc0cout << "examining (np1,np2) = (" << std::setw(3) << np1
              << "," << std::setw(3) << np2 << ") ... " << std::flush;
        }
        if( std::floor(n2/np2)>1 || n2 ==1 ){
          if( nproc%np1==0 && n2%np2 == 0  && np1*np2==nproc ){
            if( dumpDiagnostics ) proc0cout << "accepted" << endl;
            candidates.push_back( std::make_pair(np1,np2) );
          }
          else if(nproc%np1==0 && std::floor(n2/np2)>=2 && np1*np2==nproc)
          {
            if(dumpDiagnostics) proc0cout <<" in uneven" <<endl;
            uneven.push_back(std::make_pair(np1,np2));
          }
          else if( dumpDiagnostics ){
            proc0cout << "rejected" << endl;
          }
        }
      }
      else if( std::floor(n1/np1)>=2 && nproc%np1==0 ){ // an uneven canidate;
        const unsigned np2 =nproc/np1;
        if( std::floor(n2/np2)>=(n2>1?2:1) && np1*np2==nproc ){
          uneven.push_back(std::make_pair(np1,np2));
        }
      }
    }
    // brute force search on dim 2
    for( unsigned np2=max; np2>0; np2-- ){
      if( n2%np2 == 0 && n2/np2>1){
        const unsigned np1 = nproc/np2;
        if( dumpDiagnostics ){
          proc0cout << "examining (np1,  np2) = (" << std::setw(3) << np1
              << "," << std::setw(3) << np2 << ") ... " << std::flush;
        }
        if(std::floor(n1/np1)>1 || n1 ==1 ){
          if( nproc%np2==0 && n1%np1==0 && np1*np2==nproc ){
            if( dumpDiagnostics ) proc0cout << "accepted" << endl;
            candidates.push_back( std::make_pair(np1,np2) );
          }
          else if(nproc%np2==0 && std::floor(n1/np1)>=2 && np1*np2==nproc)
          {
            uneven.push_back(std::make_pair(np1,np2));
          }
          else if( dumpDiagnostics ){
            proc0cout << "rejected" << endl;
          }
        }
      }
      else if( std::floor(n2/np2)>=2 && nproc%np2==0 ){ // an uneven canidate;
        const unsigned np1 =nproc/np2;
        if( std::floor(n1/np1)>=(n1>1?2:1) && np1*np2==nproc ){
          uneven.push_back(std::make_pair(np1,np2));
        }
      }
    }
    // return even division of domain first.
    if (!candidates.empty() ){
      if(dumpDiagnostics) proc0cout <<"Return canidates"<<std::endl;
      return candidates;
    }
    //if there are no even divisions then return uneven.
    return uneven;
  }

  //=================================================================

  /*
   * \brief determine the processor decomposition within a bundle
   *
   * \param dir (input) the bundle direction
   * \param nproc (input) the number of processes
   *
   * \return the processor decomposition on the bundle. Note that
   *         this is multiplicative so that nproc = np1*np2*np3.
   */
  IntVec
  partition_within_bundle( const Mesh& mesh,
                           const Direction dir,
                           const unsigned nproc )
  {
    if( nproc <= 1 ) return IntVec(1,1,1);

    /** \todo expand this to handle situations where the problem does not partition equally */
    const BundlePtr bundle = mesh.bundle(dir);
    const unsigned n1 = bundle->npts( get_perp1(dir) );
    const unsigned n2 = bundle->npts( get_perp2(dir) );

    //-- obtain candidate decompositions based on a uniform (or non-uniform..) partitioning scheme
    const IntPairVec candidates = candidate_decompositions( n1, n2, nproc );

    if( candidates.empty() ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "ERROR: could not find an appropriate partitioning for the " << get_dir_name(dir) << " bundle." << endl
          << "  Bundle dimension: " << bundle->npts() << " cannot be split evenly onto " << endl
          << "  " << nproc << " processes along the [" << n1 << "," << n2 << "] dimension" << endl;
      throw std::invalid_argument( msg.str() );
    }

    int np1=nproc, np2=1;
    for( IntPairVec::const_iterator ii=candidates.begin(); ii!=candidates.end(); ++ii ){
      if( std::abs(np1-np2) >= std::abs( (int)ii->first - (int)ii->second ) ){
        np1 = ii->first;
        np2 = ii->second;
      }
    }

    if( nproc % (np1*np2) != 0 ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "ERROR: cannot partition " << get_dir_name(dir) << " bundle." << endl
          << "[" << n1 << "," << n2 << "] is not divisible into " << nproc <<" pieces"
          << endl;
      throw std::invalid_argument( msg.str() );
    }

    IntVec result;
    result[           dir  ] = 1;
    result[ get_perp1(dir) ] = np1;
    result[ get_perp2(dir) ] = np2;
    return result;
  }

  //=================================================================

  /**
   * \class BundlePartition
   * \author James C. Sutherland
   * \date July 2013
   * \brief Partition information for a given global bundle
   *
   */
  class BundlePartition
  {
    typedef std::map<int,BundlePtr> ProcBundleMap;

    const BundlePtr globBundle_;
    const IntVec partition_;
    const int procOffset_;

    IntVec nptsLo_, nptsHi_, breakPoint_;

    ProcBundleMap procBundleMap_;

    typedef boost::multi_array<NeighborIDs,3> IntArray;
    IntArray neighbors_;


  public:
    /**
     */
    BundlePartition( const BundlePtr b,
                     const IntVec& partition,
                     const int procOffset );

    ~BundlePartition();

    /**
     * \brief obtain the bundles owned by this process
     * @param procid the id for the process of interest
     * @return the bundles owned by the given process
     */
    BundlePtr owned_bundle( const int procid );

    NeighborIDs neighbors( const int procid ) const;

    /**
     * @param procid the process id
     * @return the lower bound for the global bundles that this processor owns
     */
    IntVec ownership_lower_bound( const int procid ) const;

    /**
     * @param procid the process id
     * @return the upper bound for the global bundles that this processor owns
     */
    IntVec ownership_upper_bound( const int procid ) const;

    /**
     * @param procid the process id
     * @return the global offset index for the origin of this bundle
     */
    IntVec origin_index( const int procid ) const;
    IntVec origin_index_coarse( const int procid, const MeshPtr mesh ) const;

    /**
     * @param procid the process id
     * @return the global offset coordinate for the origin of this bundle
     */
    Coordinate origin_coord( const int procid ) const;
    Coordinate origin_coord_coarse( const int procid, const MeshPtr mesh ) const;

    IntVec owned_size( const int procid ) const;
  };

  //=================================================================

  BundlePartition::BundlePartition( const BundlePtr b,
                                    const SpatialOps::IntVec& partition,
                                    const int procOffset )
  : globBundle_( b ),
    partition_( partition ),
    procOffset_( procOffset ),
    neighbors_( boost::extents[ partition[0] ][ partition[1] ][ partition[2] ] )
  {
    const IntVec unitX(1,0,0), unitY(0,1,0), unitZ(0,0,1);

    //--- set the neighbor index information
    typedef IntArray::index Index;
    for( Index k=0; k<partition[2]; ++k ){
      for( Index j=0; j<partition[1]; ++j ){
        for( Index i=0; i<partition[0]; ++i ){

          const IntVec ijk( i, j, k );
          IntVec ijkPlus  = ijk + IntVec(1,1,1);
          IntVec ijkMinus = ijk - IntVec(1,1,1);

          // modify x-face information
          if( i==0 ){
            switch( b->get_boundary_type(XMINUS) ){
              case PeriodicBoundary : ijkMinus[0] = partition[0]-1;  break;
              case ProcessorBoundary:                                break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkMinus[0] = i;               break;
            }
          }
          if( i==partition[0]-1 ){
            switch( b->get_boundary_type(XPLUS) ){
              case PeriodicBoundary : ijkPlus[0] = 0;  break;
              case ProcessorBoundary:                  break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkPlus[0] = i;  break;
            }
          }

          // modify y-face information
          if( j==0 ){
            switch( b->get_boundary_type(YMINUS) ){
              case PeriodicBoundary : ijkMinus[1] = partition[1]-1;  break;
              case ProcessorBoundary:                                break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkMinus[1] = j;               break;
            }
          }
          if( j==partition[1]-1 ){
            switch( b->get_boundary_type(YPLUS) ){
              case PeriodicBoundary : ijkPlus[1] = 0;   break;
              case ProcessorBoundary:                   break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkPlus[1] = j;   break;
            }
          }

          // modify z-face information
          if( k==0 ){
            switch( b->get_boundary_type(ZMINUS) ){
              case PeriodicBoundary : ijkMinus[2] = partition[2]-1;  break;
              case ProcessorBoundary:                                break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkMinus[2] = k;               break;
            }
          }
          if( k==partition[2]-1 ){
            switch( b->get_boundary_type(ZPLUS) ){
              case PeriodicBoundary : ijkPlus[2] = 0;  break;
              case ProcessorBoundary:                  break;
              case DomainBoundary   : // same as NONE
              case NONE             : ijkPlus[2] = k;  break;
            }
          }

          NeighborIDs& nbr = neighbors_[i][j][k];
          nbr.xminus = procOffset_ + ijk_to_flat( partition, IntVec(ijkMinus[0],     ijk[1],     ijk[2]) );
          nbr.xplus  = procOffset_ + ijk_to_flat( partition, IntVec( ijkPlus[0],     ijk[1],     ijk[2]) );
          nbr.yminus = procOffset_ + ijk_to_flat( partition, IntVec(     ijk[0],ijkMinus[1],     ijk[2]) );
          nbr.yplus  = procOffset_ + ijk_to_flat( partition, IntVec(     ijk[0], ijkPlus[1],     ijk[2]) );
          nbr.zminus = procOffset_ + ijk_to_flat( partition, IntVec(     ijk[0],     ijk[1],ijkMinus[2]) );
          nbr.zplus  = procOffset_ + ijk_to_flat( partition, IntVec(     ijk[0],     ijk[1], ijkPlus[2]) );

//          if( Environment::rank() == 0 ){
//            std::cout << "Neighbors for " << b->dir() << " bundle on process "
//                      << procOffset_ + i + j*partition[0] + k*partition[0]*partition[1]
//                      << ":\t" << nbr << std::endl;
//          }
        } // k-loop
      } // j-loop
    } // i-loop

    //--- set information to help determine bundle start/end locations
    const IntVec nptsGlob = b->npts();

    breakPoint_ = IntVec( nptsGlob[0] % partition_[0],
                          nptsGlob[1] % partition_[1],
                          nptsGlob[2] % partition_[2] );
    nptsLo_ = nptsGlob / partition_;
    nptsHi_ = nptsLo_ + IntVec(1,1,1);

    /*
     * If a bundle has one dimension with size "1" and the global size is not 1,
     * then the field allocators do not work properly since they will not produce
     * ghost cells in that case.
     */
    for( short i=0; i<3; ++i ){
      if( nptsGlob[i] > 1 && (nptsLo_[i] == 1 || nptsHi_[i] == 1) ){
        std::ostringstream msg;
        msg << "The requested decomposition yields at least one process with \n"
            << "a bundle of size '1' in direction " << direction(i)
            << ". This is not allowed.\n\t"
            << __FILE__ << " : " << __LINE__ << std::endl;
        throw std::runtime_error(msg.str());
      }
    }
  }

  //-----------------------------------------------------------------

  BundlePartition::~BundlePartition()
  {}

  //-----------------------------------------------------------------

  IntVec BundlePartition::ownership_lower_bound( const int procid ) const
  {
    // local processor ijk index
    // this assumes one process per child bundle.
    return flat_to_ijk( partition_, procid-procOffset_ );
  }

  //-----------------------------------------------------------------

  IntVec BundlePartition::ownership_upper_bound( const int procid ) const
  {
    // this assumes one process per child bundle.
    return ownership_lower_bound(procid) + IntVec(1,1,1);
  }

  //-----------------------------------------------------------------

  BundlePtr BundlePartition::owned_bundle( const int procid )
  {
    // don't rebuild bundles on subsequent calls since that will cause bad things to happen
    ProcBundleMap::iterator ipbm = procBundleMap_.find( procid );
    if( ipbm != procBundleMap_.end() ) return ipbm->second;

    // jcs need to ensure that this bundle is valid...
    const Direction dir = globBundle_->dir();
    const IntVec npts = owned_size(procid);
    const IntVec originIndex = origin_index(procid);
    const Coordinate length( globBundle_->spacing(XDIR) * npts[0],
                             globBundle_->spacing(YDIR) * npts[1],
                             globBundle_->spacing(ZDIR) * npts[2] );

    if( originIndex == IntVec(-1,-1,-1) || npts == IntVec(0,0,0) ){
      return BundlePtr();
    }

    assert( procid-procOffset_ < partition_[0]*partition_[1]*partition_[2] );
    assert( procid-procOffset_ >= 0 );

    BundlePtr bundle( new Bundle( dir,
                                  globBundle_->ncoarse(dir),
                                  npts,
                                  length,
                                  originIndex,
                                  ownership_lower_bound(procid) ) );
    // imprint boundary information on this bundle
    const IntVec ijkID = flat_to_ijk( partition_, procid-procOffset_ );
    const BoundaryType btype[6] = {
        ( ijkID[0] == 0               ) ? globBundle_->get_boundary_type(XMINUS) : ProcessorBoundary,
        ( ijkID[0] == partition_[0]-1 ) ? globBundle_->get_boundary_type(XPLUS ) : ProcessorBoundary,
        ( ijkID[1] == 0               ) ? globBundle_->get_boundary_type(YMINUS) : ProcessorBoundary,
        ( ijkID[1] == partition_[1]-1 ) ? globBundle_->get_boundary_type(YPLUS ) : ProcessorBoundary,
        ( ijkID[2] == 0               ) ? globBundle_->get_boundary_type(ZMINUS) : ProcessorBoundary,
        ( ijkID[2] == partition_[2]-1 ) ? globBundle_->get_boundary_type(ZPLUS ) : ProcessorBoundary,
    };

    bundle->set_boundary_types( btype );

    procBundleMap_[procid] = bundle;

#   ifdef HAVE_MPI
    //Set the global data variables for the bundles for the purpose of the xml output of checkpoints.
    bundle->set_glob_length      ( globBundle_->glob_length()            );
    bundle->set_glob_spacing     ( globBundle_->glob_spacing()           );
    bundle->set_glob_origin      ( globBundle_->glob_origin()            );
    bundle->set_glob_npts        ( globBundle_->glob_npts()              );
    bundle->set_glob_globOffset  ( globBundle_->glob_global_mesh_offset());
    bundle->set_glob_bundleOffset( globBundle_->glob_bundle_offset()     );
#   endif

    return bundle;
  }

  //-----------------------------------------------------------------

  IntVec BundlePartition::owned_size( const int procid ) const
  {
    if( procid-procOffset_ >= partition_[0]*partition_[1]*partition_[2] ||
        procid < procOffset_ )
      return IntVec(0,0,0);

    const IntVec ijkID = flat_to_ijk( partition_, procid - procOffset_ );

    IntVec npts;
    for( int i=0; i<3; ++i ){
      if( ijkID[i] >= breakPoint_[i] ) npts[i] = nptsLo_[i];
      else                             npts[i] = nptsHi_[i];
    }
    return npts;
  }

  //-----------------------------------------------------------------

  IntVec BundlePartition::origin_index( const int procid ) const
  {
    if( procid-procOffset_ >= partition_[0]*partition_[1]*partition_[2] ||
        procid < procOffset_ )
      return IntVec(-1,-1,-1);
    IntVec npts = IntVec();
    if(procid - procOffset_ == 0){return npts;}

    IntVec ijkID = flat_to_ijk(partition_, procid-procOffset_);
    for(int i=0; i<3; i++)
    {
      for(int j = 0; j<ijkID[i]; j++)
      {
        if(j>=breakPoint_[i])
        {
          npts[i]+=nptsLo_[i];
        }else{
          npts[i] += nptsHi_[i];
        }
      }
    }
    return npts;
  }

  IntVec BundlePartition::origin_index_coarse( const int procid, const MeshPtr mesh ) const
  {
    if( procid-procOffset_ >= partition_[0]*partition_[1]*partition_[2] ||
        procid < procOffset_ )
      return IntVec(-1,-1,-1);

    const IntVec ijkID = flat_to_ijk( partition_, procid - procOffset_ );
    const IntVec nCoarse = mesh->npts_coarse();
    const IntVec breakPointC_ = IntVec( nCoarse[0] % partition_[0],
                                      nCoarse[1] % partition_[1],
                                      nCoarse[2] % partition_[2] );
    const IntVec nLo = nCoarse / partition_;
    const IntVec nHi = nLo + IntVec(1,1,1);
    IntVec npts = IntVec();
    for( int i=0; i<3; ++i ){
      for(int j=0; j<ijkID[i]; j++)
      {
          if( j >= breakPointC_[i] ) npts[i] += nLo[i];
          else npts[i] += nHi[i];
      }
    }
    return npts;
  }

  //-----------------------------------------------------------------

  Coordinate BundlePartition::origin_coord( const int procid ) const
  {
    const IntVec ijk = origin_index(procid);
    const Coordinate spacing = globBundle_->spacing();
    return Coordinate( ijk[0]*spacing[0],
                       ijk[1]*spacing[1],
                       ijk[2]*spacing[2] );
  }

  Coordinate BundlePartition::origin_coord_coarse( const int procid, const MeshPtr mesh ) const
  {
    const IntVec ijk = origin_index_coarse( procid, mesh );
    const Coordinate spacing = mesh->spacing_coarse();
    return Coordinate( ijk[0]*spacing[0],
                       ijk[1]*spacing[1],
                       ijk[2]*spacing[2] );
  }

  //-----------------------------------------------------------------

  NeighborIDs
  BundlePartition::neighbors( const int procid ) const
  {
    // get the local ID:
    IntVec ijk = flat_to_ijk( partition_, procid-procOffset_ );
    return neighbors_[ ijk[0] ][ ijk[1] ][ ijk[2] ];
  }

  //=================================================================

  Partitioner::Partitioner( const MeshPtr mesh, const unsigned nproc )
  : mesh_ ( mesh ),
    nproc_( nproc ),
    helper_( mesh_->npts_coarse(), mesh_->npts_fine() )
  {
    // determine the process id range associated with each bundle
    partition_bundles();

    // determine the local processor decomposition within a bundle.  Note that
    // these are 2D decompositions.  Therefore, on a Y-bundle we will have a
    // decomposition of the form [npx,1,npz]
    nProcX_ = partition_within_bundle( *mesh_, XDIR, xrange_.second-xrange_.first );
    nProcY_ = partition_within_bundle( *mesh_, YDIR, yrange_.second-yrange_.first );
    nProcZ_ = partition_within_bundle( *mesh_, ZDIR, zrange_.second-zrange_.first );

    if( mesh_->npts_fine()[0] > 1 ){
      xBundlePartition_ = new BundlePartition( mesh->bundle(XDIR), nProcX_, bundleProcOffset_[0] );
    }
    if( mesh_->npts_fine()[1] > 1 ){
      yBundlePartition_ = new BundlePartition( mesh->bundle(YDIR), nProcY_, bundleProcOffset_[1] );
    }
    if( mesh_->npts_fine()[2] > 1 ){
      zBundlePartition_ = new BundlePartition( mesh->bundle(ZDIR), nProcZ_, bundleProcOffset_[2] );
    }
    xids_ = owner_ids( XDIR, nproc );
    yids_ = owner_ids( YDIR, nproc );
    zids_ = owner_ids( ZDIR, nproc );

//    std::cout << " --> X proc range: " << xrange_.first << " : " << xrange_.second << std::endl
//              << "   x bundle owners: ";
//    BOOST_FOREACH( const int id, xids_ ) std::cout << id << " ";
//    std::cout << std::endl << " --> Y proc range: " << yrange_.first << " : " << yrange_.second
//              << std::endl << "   y bundle owners: ";
//    BOOST_FOREACH( const int id, yids_ ) std::cout << id << " ";
//    std::cout << std::endl << " --> Z proc range: " << zrange_.first << " : " << zrange_.second
//              << std::endl << "   z bundle owners: ";
//    BOOST_FOREACH( const int id, zids_ ) std::cout << id << " ";
//    std::cout << std::endl
//              << "   np(X) : " << nProcX_ << std::endl
//              << "   np(Y) : " << nProcY_ << std::endl
//              << "   np(Z) : " << nProcZ_ << std::endl
//              << std::endl;
  }

  //-----------------------------------------------------------------

  Partitioner::~Partitioner()
  {
    if(mesh_->npts_fine()[0] > 1) delete xBundlePartition_;
    if(mesh_->npts_fine()[1] > 1) delete yBundlePartition_;
    if(mesh_->npts_fine()[2] > 1) delete zBundlePartition_;
  }

  //-----------------------------------------------------------------

  void
  Partitioner::partition_bundles()
  {
    if( nproc_ == 1 ){
      xrange_ = std::make_pair(0,1);
      yrange_ = std::make_pair(0,1);
      zrange_ = std::make_pair(0,1);
      return;
    }

    //with out spliting the bundles this is the approximate maximum number of processors that can be used.
    const ProcVec tnb = helper_.check_proc_number(nproc_);

    //-- determine processor layout - number of processes that should be allocated to each bundle direction
    const size_t nxb = tnb[0];
    const size_t nyb = tnb[1];
    const size_t nzb = tnb[2];

    xrange_.first  = 0;
    xrange_.second = nxb;
    bundleProcOffset_[0] = 0;
    size_t pos = nxb;
    //check for empty procs
    if( nxb + nyb + nzb < nproc_ ){
      std::ostringstream msg;
      msg << "There are empty processors \n"
          << "This should never happen " << nproc_
          << " It does due to a weighting issue.\n\t"
          << __FILE__ << " : " << __LINE__ << std::endl;
      throw std::runtime_error(msg.str());
    }
    //distribute processors across bundles as given by the computational
   //  loading and available resources determined above.
   if( nproc_ > nxb+(nzb>nyb?nzb:0)){ // still have more to distribute
      yrange_.first  = pos;
      yrange_.second = yrange_.first+nyb;
      pos += nyb;
      if( yrange_.second > nproc_ ){
        yrange_.first -= yrange_.second-nproc_;
        yrange_.second -= yrange_.second-nproc_;
      }
      bundleProcOffset_[1] = yrange_.first;
   }
    else{ // spread remaining across existing processors
      yrange_.first  = 0;
      yrange_.second = yrange_.first + std::max(size_t(1),nyb);
      bundleProcOffset_[1] = 0;
    }

    if( nproc_ > nxb+(nzb>nyb?0:nyb) ){  // still have more to distribute
      zrange_.first  = pos;
      zrange_.second = zrange_.first+nzb;
      if( zrange_.second > nproc_ ){
        zrange_.first -= zrange_.second-nproc_;
        zrange_.second -= zrange_.second - nproc_;
      }
      bundleProcOffset_[2] = zrange_.first;
    }
    else{ // spread remaining across existing processors
      zrange_.first  = 0;
      zrange_.second = zrange_.first + std::max(size_t(1),nzb);
      bundleProcOffset_[2] = 0;
    }
  }

  //-----------------------------------------------------------------

  IntVec
  Partitioner::bundle_partition_pattern( const Direction dir ) const
  {
    if( dir==NODIR ){
      std::ostringstream msg;
      msg << "Must specify a direction to use bundle_partition_pattern.  NODIR was given \n\t"
          << __FILE__ << " : " << __LINE__ << std::endl;
      throw std::invalid_argument( msg.str() );
      return IntVec(-1,-1,-1);
    }
    return select_from_dir(dir,nProcX_,nProcY_,nProcZ_);
  }

  //-----------------------------------------------------------------

  int
  Partitioner::bundle_owner( const BundlePtr b ) const
  {
    const IntVec offset = b->bundle_offset();
    const int localid = ijk_to_flat( bundle_partition_pattern(b->dir()), offset );
    return global_id( localid, b->dir() );
  }

  //-----------------------------------------------------------------

  IntVec
  Partitioner::origin_index( const int procid, const Direction dir ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->origin_index(procid);
  }

  IntVec Partitioner::origin_index_coarse( const int procid, const Direction dir ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->origin_index_coarse( procid, mesh_ );
  }

  //-----------------------------------------------------------------

  NeighborIDs
  Partitioner::neighbors( const BundlePtr bundle ) const
  {
    return neighbors( bundle->dir(), bundle_owner(bundle) );
  }

  //-----------------------------------------------------------------

  NeighborIDs
  Partitioner::neighbors( const Direction dir, const int procid ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->neighbors(procid);
  }

  //-----------------------------------------------------------------

  int
  Partitioner::local_id( const unsigned procid, const Direction dir ) const
  {
    return procid - select_from_dir( dir, bundleProcOffset_[0], bundleProcOffset_[1], bundleProcOffset_[2] );
  }

  //-----------------------------------------------------------------

  int
  Partitioner::global_id( const unsigned localid, const Direction dir ) const
  {
    return localid + select_from_dir( dir, bundleProcOffset_[0], bundleProcOffset_[1], bundleProcOffset_[2] );
  }

  //-----------------------------------------------------------------

  BundlePtr
  Partitioner::get_my_bundle( const int procid, const Direction dir ) const
  {
    BundlePtr b;
    switch(dir){
      case XDIR: if( mesh_->npts_fine()[0] > 1 ) b = xBundlePartition_->owned_bundle(procid);   break;
      case YDIR: if( mesh_->npts_fine()[1] > 1 ) b = yBundlePartition_->owned_bundle(procid);   break;
      case ZDIR: if( mesh_->npts_fine()[2] > 1 ) b = zBundlePartition_->owned_bundle(procid);   break;
      case NODIR:{
        throw std::invalid_argument("A valid direction must be specified to Partitioner::get_my_bundle()\n");
        break;
      }
      default: assert(false);
    }
    return b;
  }

  //-----------------------------------------------------------------

  BundlePtrVec
  Partitioner::get_my_bundles( const int procid ) const
  {
    BundlePtrVec bundles;
    const BundlePtr xb = get_my_bundle(procid,XDIR);  if( xb ) bundles.push_back( xb );
    const BundlePtr yb = get_my_bundle(procid,YDIR);  if( yb ) bundles.push_back( yb );
    const BundlePtr zb = get_my_bundle(procid,ZDIR);  if( zb ) bundles.push_back( zb );
    return bundles;
  }

  //-----------------------------------------------------------------

  std::set<int>
  Partitioner::owner_ids( const Direction dir, const int nproc ) const
  {
    std::set<int> ids;
    const ProcRange& range = select_from_dir( dir, xrange_, yrange_, zrange_ );
    assert( range.first >=0 );
    assert( range.second <= nproc );
    for( int i=range.first; i<range.second; ++i ){
      ids.insert(i);
    }

    return ids;
  }

  //-----------------------------------------------------------------

  const std::set<int>&
  Partitioner::get_owner_ids(const Direction dir) const
  {
    return select_from_dir( dir, xids_, yids_, zids_ );
  }

  //-----------------------------------------------------------------

  IntVec
  Partitioner::owned_size( const Direction dir, const int procid ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->owned_size(procid);
  }


  //-----------------------------------------------------------------

  IntVec Partitioner::owned_size_coarse( const Direction dir, const int procid ) const
  {
    IntVec npts = owned_size(dir,procid);
    npts[dir] /= (mesh_->npts_fine(dir) / mesh_->npts_coarse(dir));
    return npts;
  }

  //-----------------------------------------------------------------

  Coordinate
  Partitioner::origin( const int procid, const Direction dir ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->origin_coord(procid);
  }

  Coordinate Partitioner::origin_coarse( const Direction dir, const int procid ) const
  {
    return select_from_dir( dir, xBundlePartition_, yBundlePartition_, zBundlePartition_ )->origin_coord_coarse( procid, mesh_ );
  }

  //-----------------------------------------------------------------

  Coordinate Partitioner::terminal_coarse( const Direction dir, const int procid ) const
  {
    const IntVec ijkExtent = owned_size_coarse( dir, procid );
    Coordinate extent = mesh_->spacing_coarse();
    extent[0] *= ijkExtent[0];
    extent[1] *= ijkExtent[1];
    extent[2] *= ijkExtent[2];
    return extent;
  }

  //=================================================================

} /* namespace LBMS */
