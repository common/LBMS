/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file sieve.h
 * \date Sept. 17, 2013
 * \author John Hutchins
 */

#ifndef SIEVE_H_
#define SIEVE_H_

#include <algorithm>
#include <iostream>
#include <iterator>
#include <queue>
#include <utility>
#include <vector>
#include <cmath>

namespace std{
  /**
   * @brief Allows for cout<<pair based on the values.
   */
  template <class T1, class T2>
  std::ostream& operator<<(std::ostream& out, const std::pair<T1, T2>& value)
  {
    out << '(' << value.first << ',' << value.second << ')';
    return out;
  }
}

/**
 * @struct SortPairSecond
 * @brief This lets sorting to happen by the second point in a pair.
 */
template<class T1, class T2, class Pred = std::less<T1> >
struct SortPairSecond {
  bool operator()(const std::pair<T1,T2>&left, const std::pair<T1,T2>&right) {
    Pred p;
    return p(left.second, right.second);
  }
};

/**
 * @brief This gets the vector out of a priority queue.
 */
template<class T, class Container = std::vector<T>,
         class Compare = std::less<typename Container::value_type> >
class PQV : public std::priority_queue<T, Container, Compare>
{
public:
  typedef std::vector<T> TVec;
  TVec get_vector() const {
    TVec r( this->c.begin(), this->c.end() );
    // c is already a heap
    sort_heap(r.begin(), r.end(), this->comp);
    // Put it into priority-queue order:
    reverse(r.begin(), r.end());
    return r;
  }
};

/***
 * @brief This calculates primes via the sieve of Erostophanes starting at squares of primes, with a wheel to speed it up.
 */
template<typename T>
class Sieve
{
private:
  typedef std::pair<T, T> P;

  PQV< P, std::vector<P>, std::greater<P> > q_;
  std::vector<int> wheel2357_;
  int wheelpos_;
  int wheelend_;
  T pos_;

  /**
   * @brief a private method to update the priority queue after and insertion.
   */
  void update_q()
  {
    const size_t end = q_.size();
    for( size_t i=0; i<end; ++i ){
      if( q_.top().first<pos_ ){
         P p = q_.top();
         q_.pop();
         p.first += p.second;
         q_.push(p);
      }
      else{
        return;
      }
    }
  }
  /**
   * @brief moves to the next position on the wheel
   */
  void shift_wheel()
  {
    if( wheelpos_ == wheelend_ ) wheelpos_=0;
    if( pos_ != q_.top().first ){ // prime
      q_.push( P(pos_*pos_, pos_) );
    }
    pos_ += wheel2357_[wheelpos_];
    update_q();
    ++wheelpos_;
  }


public:
  /**
   * @brief the constructor for the sieve.
   */
  Sieve(){
    q_ = PQV<P, std::vector<P>, std::greater<P> >();
    q_.push(P(12,2));
    q_.push(P(12,3));
    q_.push(P(25,5));
    q_.push(P(49,7));
    int temp[] = {2,4,2,4,6,2,6,4,2,4,6,6,2,6,4,2,6,4,6,8,4,2,4,2,4,8,6,4,6,2,4,6,2,6,6,4,2,4,6,2,6,4,2,4,2,10,2,10};
    wheel2357_ = std::vector<int>(temp, temp + 48 );
    wheelpos_ = 0;
    wheelend_ = 48;
    pos_=11;
  }

  /**
   * @brief returns list of primes sorted in order.
   */
  std::vector<T> gets_primes() const
  {
    std::vector<P> pairs = q_.get_vector();
    std::vector<T> primes;
    std::sort( pairs.begin(), pairs.end(), SortPairSecond<T,T>() );
    const size_t end = pairs.size();
    for( size_t i = 0; i<end; ++i ){
      primes.push_back( pairs[i].second );
    }
    return primes;
  }

  /**
   * @brief Gets the primes, ensuring that there are at least num primes, there could be more.
   */
  std::vector<T> gets_primes( const T num )
  {
    while( q_.size()<num){
      shift_wheel();
    }
    return gets_primes();
  }


  /***
   * @brief This is part of the sieve to ensure that the primes are kept, it returns the prime factors.
   */
  std::vector<P> get_prime_factors( T num )
  {
    std::vector<P> primesp;

    //Get rid of evens.
    if(num%2 ==0){
      P p(0,2);
      while(num%2 == 0){
        p.first +=1;
        num /= 2;
      }
      primesp.push_back(p);
      if(num == 1){ return primesp; }
    }

    const T sqrtnum = std::sqrt(num);

    //gives certianty that all primes are in it
    while( sqrtnum >= q_.top().first ) shift_wheel();

    std::vector<T> primes = gets_primes();

    //so have the list of primes ordered least to greatest and the certainty that all prime factors are in list.
    const size_t end = primes.size();
    for(size_t i=0; i<end; ++i ){
      if( num%primes[i]==0 ){
        P p(0,primes[i]);
        while(num%primes[i]==0){
          ++p.first;
          num /= primes[i];
        }
        primesp.push_back(p);
        if( num == 1 ){ return primesp; } //It should always end up returning here unless it is prime.
      }
    }

    //so it is itself prime.
    P p(1, num);
    primesp.push_back(p);
    return primesp;
  }

  /**
   * @brief Test to see if n is prime based on sieve.
   */
  bool is_prime( const T n )
  {
    if( n<2 ){ return false; }
    const std::vector<P> factors = get_prime_factors(n);
    return factors.front().second == n;
  }

};

#endif /* SIEVE_H_ */
