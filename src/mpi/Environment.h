/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file Environment.h
 * \author James C. Sutherland
 */

#ifndef LBMS_Environment_h
#define LBMS_Environment_h

#include <spatialops/structured/IntVec.h>

#ifdef HAVE_MPI
#include <boost/mpi.hpp>
#endif

#include <vector>
#include <list>
#include <map>
#include <string>
#include <iostream>

#include <lbms/Mesh.h>
#include <mpi/NeighborIDs.h>
#include <mpi/Partitioner.h>

#if HAVE_MPI
#define proc0cout if( LBMS::Environment::self().rank() == 0 ) std::cout
#else
#define proc0cout std::cout
#endif // HAVE_MPI

namespace LBMS{

//=================================================================



  /**
   *  \class Environment
   *  \author James C. Sutherland
   *  \brief Sets up a parallel execution environment and manages creation of bundles, etc.
   *  \ingroup Comm
   *  \ingroup Tools
   *
   *  The Environment class creates the MPI instantiation and builds
   *  the world communicator.
   *
   *  \todo create an exception/error class that handles parallel
   *  exceptions.  This requires broadcasts - costly...
   */
  class Environment
  {
  public:

    static Environment& self();

    ~Environment();

    static void setup( int& argc, char**& argv );

    /**
     *  \brief Decompose the domain into the specified topology.  Note
     *         that this must be done only once.
     *
     *  \param globalMesh the global mesh information
     *
     *  \todo Implement local topology splitting?
     */
    static void set_topology( const MeshPtr globalMesh );

    /**
     * @param dir the direction of interest
     * @return true if the domain is periodic in the given direction
     */
    static bool is_periodic( const Direction dir );

    /**
     *  \brief Obtain the MPI rank for the neighbor in the requested bundle in
     *         the requested direction.
     *  \param bundle the bundle to obtain the neighbor for
     *  \param face the side to obtain the neighbor for
     */
    static int neighbor_id( const BundlePtr bundle,
                            const Faces face );

    static int neighbor_id( const Direction dir,
                            const int procid,
                            const Faces face );

    /**
     * \brief obtain the global mesh dimension for the bundle in the given direction
     * \param dir the bundle direction of interest
     */
    static SpatialOps::IntVec glob_dim( const Direction dir );

    /**
     * @brief obtain the number of coarse points in the domain in the (x,y,z) directions.
     * @return
     */
    static SpatialOps::IntVec glob_dim_coarse();

#   ifdef HAVE_MPI
    static boost::mpi::communicator& world();
    static boost::mpi::communicator& x_comm();
    static boost::mpi::communicator& y_comm();
    static boost::mpi::communicator& z_comm();
    static const boost::mpi::group& xbundle_group();
    static const boost::mpi::group& ybundle_group();
    static const boost::mpi::group& zbundle_group();
    static const boost::mpi::environment& boost_mpi_env();
#   endif

    /**
     * @return the rank of this process
     */
    static size_t rank();

    /**
     * @brief Obtain the bundles owned by this process
     * @return the vector of bundles owned by this process.
     */
    static const BundlePtrVec bundles();

    /**
     * @brief Obtain the bundle owned by this process in the specified direction
     * @param dir the direction of interest.
     * @return the bundle owned by this process in the specified direction
     */
    static const BundlePtr bundle( const Direction dir );

    /**
     * @return the partitioner
     */
    static const Partitioner& partitioner();

  private:

    MeshPtr mesh_;

#   ifdef HAVE_MPI
    boost::mpi::environment* mpiEnv_;
    boost::mpi::communicator world_;
    boost::mpi::group xBundleGroup_, yBundleGroup_, zBundleGroup_;
    boost::mpi::communicator xcomm_, ycomm_, zcomm_;
    bool setGroup_;
#   endif

    bool setup_;
    bool setTopology_;
    bool isPeriodic_[3];

    Partitioner* partition_;

    SpatialOps::IntVec globDimX_, globDimY_, globDimZ_;

    Environment();
    Environment( const Environment& );  ///< no copying
    Environment& operator=( const Environment& ); ///< no assignment
  };

# ifdef HAVE_MPI
  /**
   * @brief Serialize a portion of the code, requires end_gate to finish
   * @param comm The MPI communicator to use, defaults to World
   * @param myID The process rank to use for this process, defaults to the Envirnonment rank
   */
  void
  begin_gate( boost::mpi::communicator& comm=Environment::world(),
              const size_t myID=Environment::rank() );

  /**
   * @brief Ends the Serialization of the code from the begin_gate
   */
  void
  end_gate( boost::mpi::communicator& comm=Environment::world(),
            const size_t myID = Environment::rank() );
# endif

}// namespace lbms



#endif // LBMS_Environment_h
