/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Environment.h"

#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <mpi/Partitioner.h>

#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <cmath>

#include <boost/foreach.hpp>

using SpatialOps::IntVec;
using std::cout;
using std::endl;

#define DUMP_DIAGNOSTICS

//===================================================================

inline void
require( const bool condition,
         const std::string& msg )
{
  if( !condition ){
    throw std::runtime_error( msg );
  }
}


namespace LBMS{

# ifdef HAVE_MPI
  void
  begin_gate( boost::mpi::communicator& comm,
              const size_t myID )
  {
    if( myID != 0 ){
      bool myTurn = false;
      comm.recv( myID-1, myID-1, myTurn );
    }
  }

  void
  end_gate( boost::mpi::communicator& comm,
            const size_t myID )
  {
    if( myID < comm.size()-1 ){
      comm.send( myID+1, myID, true );
    }
    comm.barrier();
  }
# endif


  //==================================================================

  Environment::Environment()
  {
#   if HAVE_MPI
    mpiEnv_ = NULL;
#   endif
    setup_       = false;
    setTopology_ = false;
    partition_   = NULL;
  }

  //------------------------------------------------------------------

  Environment::~Environment()
  {
    Environment& env = self();
    if( env.partition_ != NULL ) delete env.partition_;
#   ifdef HAVE_MPI
    if( env.setup_ ) delete env.mpiEnv_;
#   endif
  }

  //------------------------------------------------------------------

  Environment&
  Environment::self()
  {
    static Environment env;
    return env;
  }

  void
  Environment::setup( int & argc, char **& argv )
  {
    Environment& env = self();
    require( env.setup_ == false, "Environment::setup() may only be called once!" );
    env.setup_ = true;

#   ifdef HAVE_MPI
    env.mpiEnv_ = new boost::mpi::environment(argc,argv);
    env.world_ = boost::mpi::communicator();
    env.setGroup_ = false;
#   ifdef DUMP_DIAGNOSTICS
    proc0cout << "Running with MPI and " << env.world_.size() << " processes." << std::endl
        << "  Ranks follow: " << std::flush;
    begin_gate();{
      std::cout << env.rank() << " " << std::flush;
    }end_gate();
    proc0cout << std::endl;
    env.world().barrier();
#   endif
#   endif
  }

  //------------------------------------------------------------------

# ifdef HAVE_MPI
  const boost::mpi::environment&
  Environment::boost_mpi_env()
  {
    Environment& env = self();
    require( env.setup_, "Environment::setup() must be called prior to Environment::boost_mpi_env()" );
    return *env.mpiEnv_;
  }
# endif

  //------------------------------------------------------------------

  typedef std::pair<size_t,size_t> Bounds;
  Bounds
  my_range( const size_t ntot,
            const size_t ndiv,
            const size_t myid )
  {
    const size_t ilo = ntot%ndiv;
    const size_t seg = ntot/ndiv;
    Bounds p;
    p.first  = myid*seg + ( (myid  <ilo) ? 1 : 0 );
    p.second = p.first + seg + ( (myid+1<ilo) ? 1 : 0 );
    return p;
  }

  void
  Environment::set_topology( const MeshPtr globalMesh )
  {
    Environment& env = self();
    require( env.setup_, "Environment::setup() must be called prior to Environment::set_topology()" );

    proc0cout << endl
        << "Mesh information: "                              << endl
        << " nCoarse  : " << globalMesh->npts_coarse()        << endl
        << " nFine    : " << globalMesh->npts_fine()          << endl;
    if( globalMesh->npts_fine()[0] > 1 ) {
      proc0cout << " X-Bundle : " << globalMesh->bundle(XDIR)->npts() << endl;
    }
    if( globalMesh->npts_fine()[1] > 1 ) {
      proc0cout << " Y-Bundle : " << globalMesh->bundle(YDIR)->npts() << endl;
    }
    if( globalMesh->npts_fine()[2] > 1 ) {
      proc0cout << " Z-Bundle : " << globalMesh->bundle(ZDIR)->npts() << endl;
    }
    proc0cout << endl;

    env.mesh_ = globalMesh;

    env.globDimX_ = globalMesh->npts_fine()[0] > 1 ? globalMesh->bundle(XDIR)->npts() : IntVec(1,1,1);
    env.globDimY_ = globalMesh->npts_fine()[1] > 1 ? globalMesh->bundle(YDIR)->npts() : IntVec(1,1,1);
    env.globDimZ_ = globalMesh->npts_fine()[2] > 1 ? globalMesh->bundle(ZDIR)->npts() : IntVec(1,1,1);

#   ifdef HAVE_MPI
    const size_t nproc = env.world().size();
#   else
    const size_t nproc = 1;
#   endif
    const size_t myID = env.rank();

#   if defined HAVE_MPI && defined DUMP_DIAGNOSTICS
    begin_gate();
#   endif

    if( env.partition_ != NULL ) delete env.partition_;

    //--- partition resources into approximately equal workloads by bundle
    env.partition_ = new Partitioner( globalMesh, nproc );

#   ifdef DUMP_DIAGNOSTICS
    proc0cout << endl
        << " X-Bundle proc layout: " << env.partition_->bundle_partition_pattern(XDIR) << endl
        << " Y-Bundle proc layout: " << env.partition_->bundle_partition_pattern(YDIR) << endl
        << " Z-Bundle proc layout: " << env.partition_->bundle_partition_pattern(ZDIR) << endl
        << endl;
    unsigned totPts = 0;
    const BundlePtrVec allBundles = env.partition_->get_my_bundles(myID);
    cout << myID << " owns " << allBundles.size() << " bundles" << endl;
    BOOST_FOREACH( BundlePtr bp, allBundles ){
      const unsigned int npts = bp->npts(XDIR)*bp->npts(YDIR)*bp->npts(ZDIR);
      totPts += npts;
      cout << " " << bp->dir() << " offset: " << bp->global_mesh_offset()
           << ", dim: " << bp->npts()
           << "  (" << npts << " pts)"
           << endl;
    }
    cout << " total points: " << totPts << endl << endl;
#   endif // DUMP_DIAGNOSTICS

#   if defined HAVE_MPI && defined DUMP_DIAGNOSTICS
    end_gate();
#   endif

    //--- set up groups for each bundle
    {
#     ifdef HAVE_MPI
      if(!env.setGroup_){
      const std::set<int>& xids = env.partition_->get_owner_ids(XDIR);
      const std::set<int>& yids = env.partition_->get_owner_ids(YDIR);
      const std::set<int>& zids = env.partition_->get_owner_ids(ZDIR);
      env.xBundleGroup_ = env.world().group().include( xids.begin(), xids.end() );
      env.yBundleGroup_ = env.world().group().include( yids.begin(), yids.end() );
      env.zBundleGroup_ = env.world().group().include( zids.begin(), zids.end() );
        env.xcomm_ = boost::mpi::communicator( env.world(), env.xBundleGroup_ );
        env.ycomm_ = boost::mpi::communicator( env.world(), env.yBundleGroup_ );
        env.zcomm_ = boost::mpi::communicator( env.world(), env.zBundleGroup_ );
        env.setGroup_=true;
      }
#     endif
    }

    env.setTopology_ = true;
  }

  //------------------------------------------------------------------
#   ifdef HAVE_MPI
  const boost::mpi::group&
  Environment::xbundle_group()
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::xbundle_group()" );
    return Environment::self().xBundleGroup_;
  }

  const boost::mpi::group&
  Environment::ybundle_group()
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::ybundle_group()" );
    return Environment::self().yBundleGroup_;
  }

  const boost::mpi::group&
  Environment::zbundle_group()
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::zbundle_group()" );
    return Environment::self().zBundleGroup_;
  }

  //------------------------------------------------------------------

  boost::mpi::communicator&
  Environment::x_comm()
  {
    Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::x_comm()" );
    if( env.xcomm_ == MPI_COMM_NULL ) cout << env.rank() << " has NULL X-comm\n"; else cout << env.rank() << " has X-comm with size " << env.xcomm_.size() << "\n";
    return env.xcomm_;
  }

  boost::mpi::communicator&
  Environment::y_comm(){
    Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::y_comm()" );
    if( env.ycomm_ == MPI_COMM_NULL ) cout << env.rank() << " has NULL Y-comm\n"; else cout << env.rank() << " has Y-comm with size " << env.ycomm_.size() << "\n";
    return env.ycomm_;
  }

  boost::mpi::communicator&
  Environment::z_comm(){
    Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::z_comm()" );
    if( env.zcomm_ == MPI_COMM_NULL ) cout << env.rank() << " has NULL Z-comm\n"; else cout << env.rank() << " has Z-comm with size " << env.zcomm_.size() << "\n";
    return env.zcomm_;
  }
#     endif
  //------------------------------------------------------------------

  IntVec
  Environment::glob_dim( const Direction dir )
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::glob_dim()" );
    return select_from_dir( dir, env.globDimX_, env.globDimY_, env.globDimZ_ );
  }

  //------------------------------------------------------------------

  IntVec Environment::glob_dim_coarse()
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology() must be called prior to Environment::glob_dim_coarse()" );
    return env.mesh_->npts_coarse();
  }

  //------------------------------------------------------------------

# ifdef HAVE_MPI
  boost::mpi::communicator&
  Environment::world()
  {
    Environment& env = self();
    require( env.setup_, "Environment::setup() must be called prior to Environment::world()" );
    return env.world_;
  }
# endif

  //------------------------------------------------------------------

  size_t Environment::rank()
  {
    Environment& env = self();
    require( env.setup_, "Environment::setup() must be called prior to Environment::rank()" );
#   ifdef HAVE_MPI
    return env.world_.rank();
#   else
    return 0;
#   endif
  }

  //------------------------------------------------------------------

  const BundlePtrVec
  Environment::bundles()
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology must be called prior to Environment::bundles()" );
    return env.partition_->get_my_bundles( env.rank() );
  }

  //------------------------------------------------------------------

  const BundlePtr
  Environment::bundle( const Direction dir )
  {
    const Environment& env = self();
    require( env.setTopology_, "Environment::set_topology must be called prior to Environment::bundles()" );
    return env.partition_->get_my_bundle( env.rank(), dir );
  }

  //------------------------------------------------------------------

  bool Environment::is_periodic( const Direction dir )
  {
    const Environment& env = Environment::self();
    require( env.setTopology_, "Environment::set_topology must be called prior to Environment::is_periodic()" );
    return env.mesh_->is_periodic(dir);
  }

  //------------------------------------------------------------------

  int
  Environment::neighbor_id( const BundlePtr bundle,
                            const Faces face )
  {
    Environment& env = self();
    require( env.setTopology_, "Environment::neighbor_id() - set_topology() must be called first" );
    return Environment::neighbor_id( bundle->dir(), env.partition_->bundle_owner(bundle), face );
  }

  //------------------------------------------------------------------

  int
  Environment::neighbor_id( const Direction dir,
                            const int procid,
                            const Faces face )
  {
    Environment& env = self();
    require( env.setTopology_, "Environment::neighbor_id() - set_topology() must be called first" );

    const NeighborIDs nbr = env.partition_->neighbors(dir,procid);

    int id;
    switch( face ){
    case XMINUS: id=nbr.xminus; break;
    case YMINUS: id=nbr.yminus; break;
    case ZMINUS: id=nbr.zminus; break;
    case XPLUS : id=nbr.xplus;  break;
    case YPLUS : id=nbr.yplus;  break;
    case ZPLUS : id=nbr.zplus;  break;
    }
    return id;
  }

  //------------------------------------------------------------------

  const Partitioner& Environment::partitioner()
  {
    Environment& env = self();
    require( env.setTopology_, "Environment::partitioner() - set_topology() must be called first" );
    return *env.partition_;
  }

  //------------------------------------------------------------------

} // namespace lbms
