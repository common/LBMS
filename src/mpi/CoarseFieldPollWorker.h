/* Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file CoarseFieldPollWorker.h
 * \date Dec 7, 2012
 * \author James C. Sutherland
 */

#ifndef COARSEFIELDPOLLWORKER_H_
#define COARSEFIELDPOLLWORKER_H_

#include <lbms/Bundle_fwd.h>
#include <mpi/FieldBundleExchange.h>

#include <expression/ExprFwd.h>
#include <expression/Poller.h>

namespace LBMS{

  template<typename FieldT> class CoarseFieldRecvHelper; // forward declaration

  /**
   * @class CoarseFieldPollWorker
   * @author James C. Sutherland
   * @brief Provides MPI receiving of messages without blocking other work
   * @ingroup Comm
   *
   * This should be used to receive coarse field fluxes.  It is associated with
   * a poller that is attached to the expression where the flux is computed.
   *
   * It allows for compute resources to be released back to the graph scheduler
   * while communication is underway.  Every time a node in the graph completes,
   * a callback is made here to see if the field has been received.  If it has,
   * then the node is marked complete.
   */
  template< typename FieldT >
  class CoarseFieldPollWorker : public Expr::PollWorker
  {
    const Expr::Tag fieldTag_;
    CoarseFieldRecvHelper<FieldT>* const recv_;
    Expr::FieldManagerList* fml_; 

  public:

    /**
     * @brief create a CoarseFieldRecvHelper
     * @param fieldTag the field that we will be populating
     * @param b the Bundle associated with the field we are populating
     * @param direction the Direction associated with the bundle that we are receiving from.
     */
    CoarseFieldPollWorker( const Expr::Tag& fieldTag,
                           const BundlePtr b,
                           const LBMS::Direction srcBundleDir );

    ~CoarseFieldPollWorker();

    /**
     * @brief checks to see if a message is ready.  If it is, it posts the
     *        receive and puts the message into the field specified in the
     *        constructor.
     * @param fml the FieldManagerList to extract the field from.
     * @return true if a message was received.  False otherwise.
     */
    bool operator()( Expr::FieldManagerList* fml );

  };

} // namespace LBMS

#endif /* COARSEFIELDPOLLWORKER_H_ */
