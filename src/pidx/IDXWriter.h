/*
 * IDXWriter.h
 *
 *  Created on: Jul 31, 2012
 *      Author: "James C. Sutherland"
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef IDXWRITER_H_
#define IDXWRITER_H_

#include <expression/FieldManagerList.h>
#include <spatialops/structured/MemoryWindow.h>

#include <string>

#include <lbms/Bundle_fwd.h>
#include <PIDX.h>

namespace LBMS{

/**
 * \class IDXWriter
 * \brief Provides support for writing visualization files in the IDX format.
 * \author James C. Sutherland
 * \date August 2012
 */
class IDXWriter
{
  const std::string fnameBase_;
  mutable int fileCounter_;
  const BundlePtr bundle_;
  const std::string jobName_;
public:

  /**
   * \brief Create an IDXWriter for the associated bundle.
   * \param bundle the Bundle associated with this IDXWriter.
   */
  IDXWriter( const BundlePtr bundle, const std::string& jobName );

  ~IDXWriter();

  /**
   * \brief Request output for a field to this IDXWriter
   * \param tag the Expr::Tag for the field to be written.
   * \param fmlID the FieldManagerList identifier for this field
   * \param outputName the name for this field in the output
   */
  template< typename FieldT >
  void add_output_field( const Expr::Tag& tag,
                         const int fmlID,
                         std::string outputName="" );

  /**
   * \brief write a file with the given time stamp.
   * @param time the simulation time
   * @param fmls all FieldManagerList objects
   */
  void write_entry( const double time,
                    const Expr::FMLMap& fmls ) const;

  /**
   * @return tags for fields output by this IDXWriter
   */
  Expr::TagList get_tags() const;

  class VariableExtractorBase
  {
  protected:
    mutable std::vector<double> buffer_;
    mutable std::vector<PIDX_variable> vars_;
    mutable std::vector<char*> varNames_;
  public:
    VariableExtractorBase();
    virtual ~VariableExtractorBase();
    virtual void extract_variable_to_file( PIDX_file file,
					   const int localBundleID,
                                           const SpatialOps::IntVec& globOffset,
                                           const std::string& outputName,
                                           const Expr::Tag&,
                                           const Expr::FieldManagerList& ) const = 0;
    void release_temporaries() const;
  };

private:

  struct VarInfo{
    int fmlID; const VariableExtractorBase* veb; std::string name;
    VarInfo(){}
    VarInfo( int _fmlid, const VariableExtractorBase* _veb, const std::string _name )
    : fmlID(_fmlid), veb(_veb), name(_name) {}
  };
  typedef std::map< Expr::Tag, VarInfo > VarFMMap;
  VarFMMap varFMMap_;

};


/**
 * \class IDXWriters
 * \author James C. Sutherland
 * \brief supports IDXWriter over a collection of Bundles.
 */
class IDXWriters
{
public:
  typedef std::map<int,IDXWriter*>  WriterMap;  ///< map between FieldManagerList id and IDXWriter*
  IDXWriters();
  ~IDXWriters();
  /**
   * \brief add a IDXWriter associated with a FieldManagerList of the given id
   */
  void add_writer( const int id, IDXWriter* const );
  /**
   * \brief write a file at the given time
   */
  void write( const double time, const Expr::FMLMap& fmls ) const;

  const WriterMap& get_writers() const{ return writers_; }

private:
  WriterMap writers_;
};


} // namespace LBMS

#endif /* IDXWRITER_H_ */

