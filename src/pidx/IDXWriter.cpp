/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/** IDXWriter.cpp
 *
 *  Created on: Jul 31, 2012
 *      Author: "James C. Sutherland"
 */

#include "IDXWriter.h"
#include <lbms/Bundle.h>
#include <mpi/Environment.h>

#include <expression/ExprLib.h>

#include <spatialops/structured/SpatialField.h>
using SpatialOps::IntVec;

namespace LBMS{

  /*
   * JCS QUESTIONS FOR IDX USAGE
   *
   *  - should we open a separate file for each timestep or combine them?
   * <ctc> one file for all timesteps
   *  - should we open a separate file for each variable or combine them?
   * <ctc> one file for all variables
   *
   *  - can't we tighten up the const correctness of the IDX API?
   *    There are several int* and char* that should be const.
   */

  IDXWriter::VariableExtractorBase::VariableExtractorBase()
  {}

  //-----------------------------------------------------------------

  IDXWriter::VariableExtractorBase::~VariableExtractorBase()
  {}

  //-----------------------------------------------------------------

  void IDXWriter::VariableExtractorBase::release_temporaries() const
  {
    buffer_.clear();
//    BOOST_FOREACH( PIDX_variable v, vars_     )  delete v;
//    BOOST_FOREACH( char* c,         varNames_ )  delete c;
    vars_.clear();
    varNames_.clear();
  }

  //=================================================================

  template< typename FieldT >
  class VariableExtractor : public IDXWriter::VariableExtractorBase
  {
  public:
    VariableExtractor() : IDXWriter::VariableExtractorBase()
    {}

    ~VariableExtractor(){}

    void extract_variable_to_file( PIDX_file file,
				   const int localBundleID,
                                   const IntVec& globOffset,
                                   const std::string& varname,
                                   const Expr::Tag& tag,
                                   const Expr::FieldManagerList& fml ) const
    {
      const FieldT& field = fml.field_ref<FieldT>(tag);

      const SpatialOps::MemoryWindow& window = field.window_without_ghost();

      assert( varname.size() < 200 );
      char variableName[200];
      sprintf( variableName, "%s", varname.c_str() );
      varNames_.push_back(variableName);

      // PIDX requires int* so we need to copy from the const IntVec into a int*
      // note that we must also save these pointers so they can be deleted later
      PIDX_point goffset, dimSize;
      for(unsigned i=0; i<3; ++i ){
        goffset[i] = globOffset[i];
        dimSize[i] = window.extent(i);
      }
      goffset[3]=0; goffset[4]=0;
      dimSize[3]=1; dimSize[4]=1;

      const int npts = window.local_npts();
      assert( npts == dimSize[0]*dimSize[1]*dimSize[2] );

      buffer_.resize(npts);
      size_t i=0;
      const typename FieldT::const_iterator ivale=field.interior_end();
      for( typename FieldT::const_iterator ival=field.interior_begin(); ival!=ivale; ++ival, ++i ){
        buffer_[i] = *ival;
	//printf("%f\t",*ival);
      }
      assert( npts == dimSize[0]*dimSize[1]*dimSize[2]*dimSize[3]*dimSize[4] );

      // prepare the IDX file for this field.
      const int nVars = 1;
      PIDX_variable var;
      PIDX_return_code ret = PIDX_variable_create( file, variableName, sizeof(double) * 8, PIDX_DOUBLE, &var );
      if (ret != PIDX_success)
      {
	printf("VariableExtractor::extract_variable_to_file: ERROR - CREATE VARIABLE FAILED\n");
	return;
      }

      printf("bundle is %d\n",localBundleID);
      ret = PIDX_append_and_write_variable( var, goffset, dimSize, &buffer_[0], PIDX_row_major );
      if (ret != PIDX_success)
      {
	printf("VariableExtractor::extract_variable_to_file: ERROR - WRITE FAILED, ret = %d\n",ret);
	return;
      }
      vars_.push_back( var );  // need to save this to delete it later.
      //PIDX_flush(file); // NOTE: faster without flush, more memory efficient with flush
      //endfor
    }
  }; // class VariableExtractor

  //=================================================================

  IDXWriter::IDXWriter( const BundlePtr bundle, const std::string& jobName )
  : fnameBase_( bundle->name() ),
    bundle_( bundle ),
    jobName_( jobName ),
    fileCounter_(0)
  {
  }

  //-----------------------------------------------------------------

  IDXWriter::~IDXWriter()
  {
    BOOST_FOREACH( VarFMMap::value_type vt, varFMMap_ ) delete vt.second.veb;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  void
  IDXWriter::add_output_field( const Expr::Tag& tag,
                               const int fmlID,
                               std::string outputName )
  {
    if( outputName=="" ) outputName = tag.name();
    VarInfo info( fmlID, new VariableExtractor<FieldT>(), outputName );
    proc0cout << "IDXWriter::add_output_field: " << tag << std::endl;
    varFMMap_[tag] = info;
  }

  //-----------------------------------------------------------------

  Expr::TagList
  IDXWriter::get_tags() const
  {
    Expr::TagList tags;
    BOOST_FOREACH( const VarFMMap::value_type& vt, varFMMap_ ){
      tags.push_back( vt.first );
    }
    return tags;
  }

  //-----------------------------------------------------------------

  void
  IDXWriter::write_entry( const double time, const Expr::FMLMap& fmls ) const
  {
    char fname[100];
    sprintf( fname, "idx_%s_%s.idx", bundle_->name().c_str(), jobName_.c_str() );

    const IntVec globDim = Environment::glob_dim( bundle_->dir() );
    PIDX_point dims = { globDim[0], globDim[1], globDim[2], 1, 1 };

    //--- Create the IDX file
    const int nDim = 3; // jcs could drop to 2 or 1 in case of reduced dimensionality...

    PIDX_access access;
    PIDX_return_code ret = PIDX_create_access( &access );
#   ifdef HAVE_MPI
    boost::mpi::communicator *communicator;
    switch( bundle_->dir() ){
      case XDIR: communicator = &Environment::x_comm(); break;
      case YDIR: communicator = &Environment::y_comm(); break;
      case ZDIR: communicator = &Environment::z_comm(); break;
      case NODIR: assert(false); break;
    }

    if( *communicator == MPI_COMM_NULL ){
      printf("IDXWriter::write_entry: ERROR - FAILED TO GET MPI COMMUNICATOR\n");
      return;
    }

    ret = PIDX_set_mpi_access( access, *communicator );
    if( ret != PIDX_success ){
      printf("IDXWriter::write_entry: ERROR CREATING PIDX ACCESS\n");
      return;
    }
#   endif

    PIDX_file file;
    ret = PIDX_file_create( fname, PIDX_file_trunc, access, &file );
    if( ret != PIDX_success ){
      printf("IDXWriter::write_entry: ERROR OPENING PIDX FILE %s, ret = %d\n",fname,ret);
      return;
    }

    // set dims
    PIDX_set_dims( file, dims );

    // set current timestep
    PIDX_set_current_time_step( file, fileCounter_++ );

    // set number of vars
    PIDX_set_variable_count( file, varFMMap_.size() );

    // set transformation to physical coordinates
    double transform[16];
    memset(transform,0,sizeof(double)*16);
    transform[0] = transform[5] = transform[10] = transform[15] = 1.0; 
    switch( bundle_->dir() ){
      case XDIR: transform[5] = transform[10] = (float)globDim[0]/(float)globDim[1]; break;
      case YDIR: transform[0] = transform[10] = (float)globDim[1]/(float)globDim[0]; break;
      case ZDIR: transform[0] = transform[5]  = (float)globDim[2]/(float)globDim[0]; break;
      case NODIR: assert(false); break;
    }
    PIDX_set_transform( file, transform );

    //--- setup each requested variable for writing to the file
    BOOST_FOREACH( const VarFMMap::value_type& vfmm, varFMMap_ ){
      const VarInfo& info = vfmm.second;
      const Expr::Tag& tag = vfmm.first;
      printf("writing variable %s...\n",info.name.c_str());
      const Expr::FieldManagerList* const fml = Expr::extract_field_manager_list( fmls, info.fmlID );

      // get local bundle/patch id
      const BundlePtrVec bundles = Environment::bundles();
      size_t localBundleID = 0;
      for( ; localBundleID<bundles.size(); ++localBundleID ){
      	if( bundles[localBundleID] == bundle_ ) break;
      }

      info.veb->extract_variable_to_file( file, localBundleID, bundle_->global_mesh_offset(), info.name, tag, *fml );
    }

    //--- write & close the IDX file
    printf("IDXWriter::write_entry: Writing IDX...\n");
    PIDX_close(file);
    PIDX_close_access(access);

    //--- release the buffered memory
    BOOST_FOREACH( VarFMMap::value_type vfmm, varFMMap_ ){
      vfmm.second.veb->release_temporaries();
    }
  }

  //=================================================================

  IDXWriters::IDXWriters(){}

  //-----------------------------------------------------------------

  IDXWriters::~IDXWriters()
  {
    BOOST_FOREACH( WriterMap::value_type& wpair, writers_ ){
      delete wpair.second;
    }
  }

  //-----------------------------------------------------------------

  void
  IDXWriters::add_writer( const int id, IDXWriter* const writer )
  {
    writers_[id] = writer;
  }

  //-----------------------------------------------------------------

  void
  IDXWriters::write( const double time, const Expr::FMLMap& fmls ) const
  {
    // jcs this implies that each field is on all FMLs.  This could be a problem
    // if we decide to output coarse fields.  In that case, we would need to
    // extract the appropriate FML here and then the IDXWriter::write_entry()
    // method would only take a FML rather than a FMLMap.
    BOOST_FOREACH( const WriterMap::value_type& wpair, writers_ ){
      wpair.second->write_entry( time, fmls );
    }
  }

  //-----------------------------------------------------------------

} // namespace LBMS

//===================================================================

#include <fields/Fields.h>

#define INSTANTIATE( T ) \
  template void LBMS::IDXWriter::add_output_field<T>( const Expr::Tag&, const int, const std::string );

using SpatialOps::FaceTypes;
#define INSTANTIATE_VARIANTS( VOL )     \
 INSTANTIATE( VOL                   )   \
 INSTANTIATE( FaceTypes<VOL>::XFace )   \
 INSTANTIATE( FaceTypes<VOL>::YFace )   \
 INSTANTIATE( FaceTypes<VOL>::ZFace )

INSTANTIATE_VARIANTS( LBMS::XVolField )
INSTANTIATE_VARIANTS( LBMS::YVolField )
INSTANTIATE_VARIANTS( LBMS::ZVolField )
