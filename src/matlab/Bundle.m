classdef Bundle
  % Bundle a datastructure to describe bundles
  %
  % Bundle properties:
  %   dir       - bundle direction
  %   path      - path to checkpoint files
  %   dim       - dimensions of this bundle
  %   fieldInfo - information about fields on this bundle
  %
  % Bundle methods:
  %   get_field_names - obtain the names of the fields on this bundle
  %   field_dim       - obtain the dimension of the given field
  %   line_dim        - obtain the dimension of a line
  %   load_var        - extract the requested variable from disk
  
  properties( SetAccess=private, GetAccess=public )
    dir;        % bundle direction
    dim;        % bundle dimensions
    fieldInfo;  % information on fields on this bundle
  end
  
  
  methods( Access=public )
    
    function obj = Bundle( varargin )
      % b = Bundle();
      % bx = Bundle('X',path);
      % by = Bundle('Y',path);
      % bz = Bundle('Z',path);
      switch nargin
        case 0
          [file,path] = uigetfile('*.xml','Select the Bundle XML file');
          fnam = strcat(path,'/',file);
        case 2
          dir = varargin{1};
          path = varargin{2};
          fnam = strcat(path,strcat('/',dir,'_FieldInfo.xml'));
        otherwise
          error('Invalid use of Bundle constructor');
      end
      xml           = xml2struct(fnam);
      obj.dir       = xml.info.bundle.dir.Text;
      obj.dim       = Bundle.parse_dim_info( xml.info.bundle );
      obj.fieldInfo = Bundle.parse_field_info( xml.info.field );
    end
    
    function names = get_field_names(obj)
      % names = bndl.get_field_names();
      for i=1:length(obj.fieldInfo)
        names{i} = obj.fieldInfo{i}.name;
      end
    end
    
    function [n1,n2] = line_dim(obj)
      % [n1,n2] = bndl.line_dim();
      switch obj.dir
        case 'X'
          n1 = obj.dim(2);  n2=obj.dim(3);
        case 'Y'
          n1 = obj.dim(1);  n2=obj.dim(3);
        case 'Z'
          n1 = obj.dim(1);  n2=obj.dim(2);
      end
    end
    
    function dim = field_dim( obj, varname )
      % dim = bndl.field_dim(varname);
      % Obtain the dimensionality of the requested field on this bundle
      i = obj.find_field_index(varname);
      dim = obj.fieldInfo{i}.dim;
    end
    
    function var = load_var( obj, varname, fnam )
      % name - name of the variable to load
      % fname - name of the file to load from
      irow = obj.find_field_index(varname);
%       fprintf('Loading file %s\n',fnam);
      fid = fopen(fnam,'r');
      for i=1:1:irow-1
        fgetl(fid);
      end
      n = obj.fieldInfo{irow}.dim( Bundle.dir_ix(obj.dir) );
      var = fscanf(fid,'%f',n);
      fclose(fid);
%       fprintf('read %i entries for variable %s\n',n,varname);
    end
        
  end
  
  
  methods( Static, Access=private )
    
    function ix = dir_ix(dirname)
      ix=-1;
      switch dirname
        case {'X' 'x'}
          ix=1;
        case {'Y' 'y'}
          ix=2;
        case {'Z' 'z'}
          ix=3;
      end
    end
    
    function info = parse_field_info( fields )
      % parse field information from the xml file
      if ~iscell(fields)
        info{1}.name = fields.name.Text;
        info{1}.dim  = [ ...
          str2num( fields.nx.Text ), ...
          str2num( fields.ny.Text ), ...
          str2num( fields.nz.Text ) ];
      else
        for( i=1:length(fields) )
          info{i}.name = fields{i}.name.Text;
          info{i}.dim  = [ ...
            str2num( fields{i}.nx.Text ), ...
            str2num( fields{i}.ny.Text ), ...
            str2num( fields{i}.nz.Text ) ];
        end
      end
    end
    
    function dim = parse_dim_info( bundleInfo )
      % parse information on bundle dimensions from the XML input
      dim = [ str2num(bundleInfo.nX.Text), ...
        str2num(bundleInfo.nY.Text), ...
        str2num(bundleInfo.nZ.Text) ];
    end

    function data = load_file( obj, varname, ix1, ix2 )
      % row in the file corresponds to the variable index
      irow = obj.find_field_index(varname);
      fnam = strcat(obj.path,'_',ix1,'_',ix2);
    end
    
  end
  
  methods( Access=private )
    function ix = find_field_index( obj, name )
      % given the struct of field entries (e.g. xEntries) and a name, this
      % returns the index for the field
      ix=-1;
      for i=1:length(obj.fieldInfo)
        if( strcmp( obj.fieldInfo{i}.name, name ) )
          ix = i;
          break;
        end
      end
      if ix==-1
        error(strcat('Variable: ',name,' not found!'));
      end
    end
    
  end
end