classdef LBMSCheckpoint
  % LBMSCheckpoint   A class to hold a LBMS checkpoint.
  %
  % Provides a way to load checkpoints into matlab and interrogate field
  % values.
  %
  %
  % LBMSCheckpoint methods:
  %
  %   extract_variable - extract a variable from the checkpoint file
  %
  %   bundle           - extract the requested bundle information from
  %                      the checkpoint
  %
  %   get_field_names  - extract the names of the fields contained in the
  %                      checkpoint file
  %
  %
  % LBMSCheckpoint properties:
  %   xBundle  - the x-bundle data structure
  %   yBundle  - the y-bundle data structure
  %   zBundle  - the z-bundle data structure
  %
  % Author: James C. Sutherland
  % Date: August, 2012
  
  properties( Access=private )
    fnam;  % name of the xml file
    path;  % path to checkpoint files
  end
  
  properties( SetAccess=private, GetAccess=public )
    xBundle;     % the x-bundle data structure
    yBundle;     % the y-bundle data structure
    zBundle;     % the z-bundle data structure
  end
  
  methods( Access=public )
    
    function obj = LBMSCheckpoint( varargin )
      %% Examples:
      % 
      %  ckpt = LBMSCheckpoint;        <-- interactively select the checkpoint path
      %  ckpt = LBMSCheckpoint(path);  <-- directly set the checkpoint path
      switch( nargin )
        case 0
          obj.path = uigetdir('','Select Checkpoint Directory');
        case 1
          obj.path = varargin{1};
        otherwise
          error('Invalid number of arguments to LBMSCheckpoint');
      end

      if( exist(strcat(obj.path,'/X_FieldInfo.xml'),'file') == 2 )
        obj.xBundle = Bundle( 'X', obj.path );
      end
      if( exist(strcat(obj.path,'/Y_FieldInfo.xml'),'file') == 2 )
        obj.yBundle = Bundle( 'Y', obj.path );
      end
      if( exist(strcat(obj.path,'/Z_FieldInfo.xml'),'file') == 2 )
        obj.zBundle = Bundle( 'Z', obj.path );
      end
    end
    
    function bndl = bundle(obj,dir)
      % bndl = obj.bundle(dir)
      % Obtain the bundle data structure in the requested direction
      switch upper(dir)
        case 'X'
          bndl = obj.xBundle;
        case 'Y'
          bndl = obj.yBundle;
        case 'Z'
          bndl = obj.zBundle;
        otherwise
          error('Invalid bundle direction selected');
      end
    end
    
    function vals = extract_variable(obj,varargin)
      % Examples:
      %  vals = chkpt.extract_variable();
      %  vals = chkpt.extract_variable(varName,bundleDir);
      %  vals = chkpt.extract_variable(varName,bundleDir,fileName);
      %  vals = chkpt.extract_variable(varName,bundleDir,ix1,ix2);
      if nargin==1
        dirs = {'X','Y','Z'};
        idir = listdlg('PromptString','Select bundle direction','ListString', dirs);
        dir = dirs{idir};
        fields = obj.get_field_names(dir);
        ivar = listdlg('PromptString','Select field to extract','ListString',fields);
        vals = obj.extract_variable( fields{ivar}, dir );
        return;
      elseif nargin >= 3
        varname = varargin{1};
        dir = upper(varargin{2});
      else
        error('Invalid usage of LBMSCheckpoint::extract_variable()');
      end
      
      switch dir
        case 'X'
          bundle = obj.xBundle;
        case 'Y'
          bundle = obj.yBundle;
        case 'Z'
          bundle = obj.zBundle;
        otherwise
          error('Invalid direction. Valid arguments are "X","Y" or "Z"');
      end
      
      switch nargin
        case 3
          fnam = uigetfile('*','Select file');
          vals = bundle.load_var(varname,fnam);
        case 4
          vals = bundle.load_var(varname,varargin{3});
        case 5
          vals = bundle.load_var( varname, ...
            strcat(obj.path,'/',dir,'_', ...
            num2str(varargin{3}),'_',num2str(varargin{4}) )...
            );
        otherwise
          error('improper usage of LBMSCheckpoint::extract_variable()');
      end
    end
    
    function names = get_field_names(obj,dir)
      % names = chkpt.get_field_names(dir)
      % get the field names associated with the bundle in the given direction
      switch upper(dir)
        case 'X'
          names = obj.xBundle.get_field_names();
        case 'Y'
          names = obj.yBundle.get_field_names();
        case 'Z'
          names = obj.zBundle.get_field_names();
      end
    end
    
  end
  
  methods( Static, Access=private )
    
    

    
    
  end
  
end