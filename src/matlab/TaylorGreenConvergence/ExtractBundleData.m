function[xvel,yvel]=ExtractBundleData(directory,Nx,Ny,dir)

%Function designed to extract the second, third and fourth term in a Taylor
%Green XY checkpoint file.  Four fields will exist in this file, the first
%of which will be energy

%The grid size is Nx,Ny and dir is the bundle that you wish to extract


xmom = zeros(Nx,Ny);
ymom = zeros(Nx,Ny);
dens = zeros(Nx,Ny);

for x = 0:1:Ny-1
    
    tempstr =num2str(x);
    file = strcat(directory,dir,tempstr,'_0');
    
    tempdata = importdata(file);

    xmom(x+1,:) = tempdata(2,:);
    ymom(x+1,:) = tempdata(3,:);
    dens(x+1,:) = tempdata(4,:);
    
end

xvel = xmom ./ dens;
yvel = ymom ./ dens;