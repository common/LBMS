%=========================================
%A file to test the convergence of a 2D 
%Taylor Green Vortex problem against
%the analytic solution
%Derek Cline
%April 1st, 2014

%To use this file, simply set the end time,
%the number of grid points, the domain length, 
%the bundle direction and the kinematic viscosity
%=========================================
%clc;
clear,close all;

%-----------------------------------------
%Variables required to be set
bundledir = 'X';   %Bundle direction (i.e. 'X' or 'Y')
N   = 20;          %convenient for uniform grids
Nx  = N;           %Spatial step in the X direction
Ny  = N;           %Spatial step in the Y direction
Lx  = 1;           %Spatial length in the X direction
Ly  = 1;           %Spatial length in the Y direction
t   = 1e-6;       %Time at which we want to compare
nu  = 1.609333748701848e-05; %300 C, 100 kPa air kinematic viscosity
A   = 1.0;         %Amplitude of initial velocity condition
%-----------------------------------------

file = uigetdir('./','Select directory containing output');
bundlename = strcat('/',bundledir,'_'); %Sets appropriate path

XVelocityAnalytic = zeros(Nx,Ny);
YVelocityAnalytic = zeros(Nx,Ny);
XVelocityLBMS     = zeros(Nx,Ny);
YVelocityLBMS     = zeros(Nx,Ny);
X                 = 0.5:1:Nx;
Y                 = 0.5:1:Ny;
XCoord            = Lx * X / Nx;
YCoord            = Ly * Y / Ny;

for nx = 1:1:Nx
    for ny = 1:1:Ny
        
        XVelocityAnalytic(nx,ny) = A*cos(2*pi*XCoord(nx)-t)*sin(2*pi*YCoord(ny)-t)*exp(-2*nu*t);
        YVelocityAnalytic(nx,ny) = -A*sin(2*pi*XCoord(nx)-t)*cos(2*pi*YCoord(ny)-t)*exp(-2*nu*t);

    end
end

[XVelocityLBMS,YVelocityLBMS] = ExtractBundleData(file,Nx,Ny,bundlename);

XError = XVelocityLBMS - XVelocityAnalytic;
YError = YVelocityLBMS - YVelocityAnalytic;

XErrorRelative = norm( XVelocityLBMS - XVelocityAnalytic ) / norm(XVelocityAnalytic)
YErrorRelative = norm( YVelocityLBMS - YVelocityAnalytic ) / norm(YVelocityAnalytic)

% figure
% quiver(XVelocityLBMS,YVelocityLBMS)
% figure
% quiver(XVelocityAnalytic,YVelocityAnalytic)