% Function: PES_3D.m
% Author:   Phillip Helms
% Date:     June 2014
%
% This code is adapted from plot_energy_spectrum_uda.m  and tke_spectrum.m
% created by Tony Saad, September 2012
%
% This function  uses data taken from checkpoint files created by the LBMS
% code.  The u, v, and w velocities are calculated and then used to
% determine the turbulent kinetic energy.
%
% Inputs:
%
%   CMesh:  The coarse mesh size where Mesh(1) = x direction, Mesh(2) = y direction,
%           and Mesh(3) = z direction.
%   FMesh:  The fine mesh size where Mesh(1) = x direction, Mesh(2) = y
%           direction, and mesh(3) = z direction
%   Path:   Specifies the path where the file to be analyzed is located.
%           The path should be a string ending in '/'.
%   L:      The domain length
%
% Outputs:
%   This function creates the energy cascade plot.
function PES_3D(CMesh, FMesh, Path, L)

    nxc = CMesh(1);
    nyc = CMesh(2);
    nzc = CMesh(3);
    nxf = FMesh(1);
    nyf = FMesh(2);
    nzf = FMesh(3);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    n_tot = nxf*nyc*nzc;
%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nxf, nyc, nzc);
    V = zeros(nxf, nyc, nzc);
    W = zeros(nxf, nyc, nzc);
    iter = 0;
    for j = 1:nyc
        for k = 1:nzc
            iter = iter+1;
            indi = num2str(j-1);
            indj = num2str(k-1);
            name = strcat(Path, 'X_', indi, '_', indj);
            data_tmp = importdata(name);
            U(:,j,k) = data_tmp(3,:)./data_tmp(1,:); % xvel = momentum/density
            V(:,j,k) = data_tmp(4,:)./data_tmp(1,:); % yvel
            W(:,j,k) = data_tmp(5,:)./data_tmp(1,:); % zvel
        end
    end  
      
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

  % perform multidimensional fourier transform
  u_hat = fftn(U)/n_tot;
  v_hat = fftn(V)/n_tot;
  w_hat = fftn(W)/n_tot;

  tke_hat=zeros(nxf,nyc,nzc); % allocate memory for spectral tke
  % calculate tubulent kinetic energy
  for k=1:nzc
    for j=1:nyc
      for i=1:nxf
        % NOTE: use conjugate in spectral space
        tke_hat(i,j,k) = 0.5*( u_hat(i,j,k)*conj(u_hat(i,j,k)) + ...
                         v_hat(i,j,k)*conj(v_hat(i,j,k)) + ...
                         w_hat(i,j,k)*conj(w_hat(i,j,k)) );
      end
    end
  end
  
%--------------------------------------------------------------------------
% Spectrum Calculation
%--------------------------------------------------------------------------

  % For a wave form cos(2*Pi*m*x/L) = cos(k_m x), k0 = 2*Pi/L is the
  % largest wave form (one period) that can be fit into a grid
  n_max = max([nxf, nyc, nzc]);
  n_min = min([nxf, nyc, nzc]);
  k0 = 2*pi/L;
  kmax = n_max/2; % This is the maximum number of "waves" or peaks that can fit on a grid with npoints
  wave_numbers = k0*[0:n_max]; % wavenumber array
  tke_spectrum = zeros(size(wave_numbers));

  for kx=1:nxf
    rkx = kx-1;
    
    if (kx>nxf/2+1); 
        rkx=rkx-nxf; 
    end % conjugate symmetry

    for ky=1:nyc
      rky = ky-1;
      
      if (ky>nyc/2+1); 
          rky=rky-nyc; 
      end % conjugate symmetry

      for kz=1:nzc
        rkz = kz-1;
        
        if (kz>nzc/2+1); 
            rkz=rkz-nzc; 
        end % conjugate symmetry
        
        rk = sqrt(rkx^2+rky^2+rkz^2);
        k = round(rk);

        tke_spectrum(k+1) = tke_spectrum(k+1) + tke_hat(kx,ky,kz)/k0;
        
      end
    end
  end
  
  loglog(wave_numbers(1:n_max),tke_spectrum(1:n_max), 'k')
  hold on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Y Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  n_tot = nxc*nyf*nzc;
%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nxc, nyf, nzc);
    V = zeros(nxc, nyf, nzc);
    W = zeros(nxc, nyf, nzc);
    iter = 0;
    for i = 1:nxc
        for k = 1:nzc
            iter = iter+1;
            indi = num2str(i-1);
            indj = num2str(k-1);
            name = strcat(Path, 'Y_', indi, '_', indj);
            data_tmp = importdata(name);
            U(i,:,k) = data_tmp(3,:)./data_tmp(1,:); % xvel = momentum/density
            V(i,:,k) = data_tmp(4,:)./data_tmp(1,:); % yvel
            W(i,:,k) = data_tmp(5,:)./data_tmp(1,:); % zvel
        end
    end  
      
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

  % perform multidimensional fourier transform
  u_hat = fftn(U)/n_tot;
  v_hat = fftn(V)/n_tot;
  w_hat = fftn(W)/n_tot;

  tke_hat=zeros(nxf,nyc,nzc); % allocate memory for spectral tke
  % calculate tubulent kinetic energy
  for k=1:nzc
    for j=1:nyf
      for i=1:nxc
        % NOTE: use conjugate in spectral space
        tke_hat(i,j,k) = 0.5*( u_hat(i,j,k)*conj(u_hat(i,j,k)) + ...
                         v_hat(i,j,k)*conj(v_hat(i,j,k)) + ...
                         w_hat(i,j,k)*conj(w_hat(i,j,k)) );
      end
    end
  end
  
%--------------------------------------------------------------------------
% Spectrum Calculation
%--------------------------------------------------------------------------

  % For a wave form cos(2*Pi*m*x/L) = cos(k_m x), k0 = 2*Pi/L is the
  % largest wave form (one period) that can be fit into a grid
  n_max = max([nxc, nyf, nzc]);
  n_min = min([nxc, nyf, nzc]);
  k0 = 2*pi/L;
  kmax = n_max/2; % This is the maximum number of "waves" or peaks that can fit on a grid with npoints
  wave_numbers = k0*[0:n_max]; % wavenumber array
  tke_spectrum = zeros(size(wave_numbers));

  for kx=1:nxc
    rkx = kx-1;
    
    if (kx>nxc/2+1); 
        rkx=rkx-nxc; 
    end % conjugate symmetry

    for ky=1:nyf
      rky = ky-1;
      
      if (ky>nyf/2+1); 
          rky=rky-nyf; 
      end % conjugate symmetry

      for kz=1:nzc
        rkz = kz-1;
        
        if (kz>nzc/2+1); 
            rkz=rkz-nzc; 
        end % conjugate symmetry
        
        rk = sqrt(rkx^2+rky^2+rkz^2);
        k = round(rk);

        tke_spectrum(k+1) = tke_spectrum(k+1) + tke_hat(kx,ky,kz)/k0;
        
      end
    end
  end
  
  loglog(wave_numbers(1:n_max),tke_spectrum(1:n_max), 'b')
  hold on
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    n_tot = nxc*nyc*nzf;

%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nxc, nyc, nzf);
    V = zeros(nxc, nyc, nzf);
    W = zeros(nxc, nyc, nzf);
    iter = 0;
    for i = 1:nxc
        for j = 1:nyc
            iter = iter+1;
            indi = num2str(i-1);
            indj = num2str(j-1);
            name = strcat(Path, 'Z_', indi, '_', indj);
            data_tmp = importdata(name);
            U(i,j,:) = data_tmp(3,:)./data_tmp(1,:); % xvel = momentum/density
            V(i,j,:) = data_tmp(4,:)./data_tmp(1,:); % yvel
            W(i,j,:) = data_tmp(5,:)./data_tmp(1,:); % zvel
        end
    end  
      
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

  % perform multidimensional fourier transform
  u_hat = fftn(U)/n_tot;
  v_hat = fftn(V)/n_tot;
  w_hat = fftn(W)/n_tot;

  tke_hat=zeros(nxf,nyc,nzc); % allocate memory for spectral tke
  % calculate tubulent kinetic energy
  for k=1:nzf
    for j=1:nyc
      for i=1:nxc
        % NOTE: use conjugate in spectral space
        tke_hat(i,j,k) = 0.5*( u_hat(i,j,k)*conj(u_hat(i,j,k)) + ...
                         v_hat(i,j,k)*conj(v_hat(i,j,k)) + ...
                         w_hat(i,j,k)*conj(w_hat(i,j,k)) );
      end
    end
  end
  
%--------------------------------------------------------------------------
% Spectrum Calculation
%--------------------------------------------------------------------------

  % For a wave form cos(2*Pi*m*x/L) = cos(k_m x), k0 = 2*Pi/L is the
  % largest wave form (one period) that can be fit into a grid
  n_max = max([nxc, nyc, nzf]);
  n_min = min([nxc, nyc, nzf]);
  k0 = 2*pi/L;
  kmax = n_max/2; % This is the maximum number of "waves" or peaks that can fit on a grid with npoints
  wave_numbers = k0*[0:n_max]; % wavenumber array
  tke_spectrum = zeros(size(wave_numbers));

  for kx=1:nxc
    rkx = kx-1;
    
    if (kx>nxc/2+1); 
        rkx=rkx-nxc; 
    end % conjugate symmetry

    for ky=1:nyc
      rky = ky-1;
      
      if (ky>nyc/2+1); 
          rky=rky-nyc; 
      end % conjugate symmetry

      for kz=1:nzf
        rkz = kz-1;
        
        if (kz>nzf/2+1); 
            rkz=rkz-nzf; 
        end % conjugate symmetry
        
        rk = sqrt(rkx^2+rky^2+rkz^2);
        k = round(rk);

        tke_spectrum(k+1) = tke_spectrum(k+1) + tke_hat(kx,ky,kz)/k0;
        
      end
    end
  end
  
  loglog(wave_numbers(1:n_max),tke_spectrum(1:n_max), 'r')
  hold on
  legend('X', 'Y', 'Z')
  title({'Energy Cascade for' Path '3D'})
  n_min = min([nxc, nyc, nzc]);
  k_nyquist = k0*n_min/2;
  loglog( [k_nyquist,k_nyquist],[1e-6,1e-2],'k--')
end 





















