% Function: PES_1D.m
% Author:   Phillip Helms
% Date:     June 2014
%
% This code is adapted from plot_energy_spectrum_uda.m  and tke_spectrum.m
% created by Tony Saad, September 2012
%
% This function  uses data taken from checkpoint files created by the LBMS
% code.  The u, v, and w velocities are calculated and then used to
% determine the turbulent kinetic energy.
%
% Inputs:
%
%   CMesh:  The coarse mesh size where Mesh(1) = x direction, Mesh(2) = y direction,
%           and Mesh(3) = z direction.
%   FMesh:  The fine mesh size where Mesh(1) = x direction, Mesh(2) = y
%           direction, and mesh(3) = z direction
%   Path:   Specifies the path where the file to be analyzed is located.
%           The path should be a string ending in '/'.
%   L:      The domain length
%   Dir:    Indicates direction of interest. Possible strings are: 'X', 
%           'Y', or 'Z' in capital letters
%
% Outputs:
%   This function creates a plot showing...
function PES_1D(CMesh, FMesh, Path, L)

    nxc = CMesh(1);
    nyc = CMesh(2);
    nzc = CMesh(3);
    nxf = FMesh(1);
    nyf = FMesh(2);
    nzf = FMesh(3);
    
    nfavg = 71824; % Number of lines used to reduce noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nfavg,nxf);
    V = zeros(nfavg,nyf);
    W = zeros(nfavg,nzf);
    iter = 0;
    for j = 1:ceil(nyc/sqrt(nfavg)):nyc
        for k = 1:ceil(nzc/sqrt(nfavg)):nzc
            iter = iter+1;
            indj = num2str(j-1);
            indk = num2str(k-1);
            name = strcat(Path, 'X_', indj, '_', indk);
            data_tmp = importdata(name);
            U(iter,:) = data_tmp(2,:)./data_tmp(5,:); % xvel = momentum/density
            V(iter,:) = data_tmp(3,:)./data_tmp(5,:); % yvel
            W(iter,:) = data_tmp(4,:)./data_tmp(5,:); % zvel
        end
    end
   
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

    % Fourier transform
    u_hat = fftn(U)/nfavg; % Originally fftn(U)/n_tot, do I need to change this?
    v_hat = fftn(V)/nfavg;
    w_hat = fftn(W)/nfavg;

    % Allocate memory for tke
    tke_hat = zeros(nfavg,nxf);

    % calculate tke
    for i = 1:nfavg
        for j = 1:nxf
                tke_hat(i,j) = 0.5*(u_hat(i,j)*conj(u_hat(i,j)) + ...
                    v_hat(i,j)*conj(v_hat(i,j)) + ...
                    w_hat(i,j)*conj(w_hat(i,j)));
        end
    end

%--------------------------------------------------------------------------
% Calculate the spectrum
%--------------------------------------------------------------------------
    k0 = 2*pi/L; % Largest waveform that can fit on grid (period)
    kmax = nxf;
    wavenums = k0*(0:nxf);
    tke_spectra = zeros(size(nfavg,wavenums));
    tke_avg_spectrum = zeros(size(wavenums));
    
    for l = 1:nfavg
        for kx = 1:nxf
            rkx = kx-1;

            if kx>kmax+1
                rkx = rkx-nxf;
            end % conjugate symmetry
            k = sqrt(rkx^2);
            tke_spectra(l,k+1) = tke_spectra(l,k+1) + tke_hat(l,k+1)/k0;
        end
    end
    
    %average
    tke_avg_spectrum = sum(tke_spectra)/nfavg;
    
%--------------------------------------------------------------------------
% Create energy cascade plot
%--------------------------------------------------------------------------
    loglog(wavenums, tke_spectra, 'k')
    hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Y Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nfavg,nyf);
    V = zeros(nfavg,nyf);
    W = zeros(nfavg,nyf);
    iter = 0;
    for i = 1:ceil(nxc/sqrt(nfavg)):nxc
        for j = 1:ceil(nzc/sqrt(nfavg)):nzc
            iter = iter+1;
            indi = num2str(i-1);
            indj = num2str(j-1);
            name = strcat(Path, 'Y_', indi, '_', indj);
            data_tmp = importdata(name);
            U(iter,:) = data_tmp(2,:)./data_tmp(5,:); % xvel = momentum/density
            V(iter,:) = data_tmp(3,:)./data_tmp(5,:); % yvel
            W(iter,:) = data_tmp(4,:)./data_tmp(5,:); % zvel
        end
    end
   
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

    % Fourier transform
    u_hat = fftn(U)/nfavg; % Originally fftn(X)/n_tot, do I need to change this?
    v_hat = fftn(V)/nfavg;
    w_hat = fftn(W)/nfavg;

    % Allocate memory for tke
    tke_hat = zeros(nfavg,nyf);

    % calculate tke
    for i = 1:nfavg
        for j = 1:nyf
                tke_hat(i,j) = 0.5*(u_hat(i,j)*conj(u_hat(i,j)) + ...
                    v_hat(i,j)*conj(v_hat(i,j)) + ...
                    w_hat(i,j)*conj(w_hat(i,j)));
        end
    end
    
%--------------------------------------------------------------------------
% Calculate the spectrum
%--------------------------------------------------------------------------
    k0 = 2*pi/L; % Largest waveform that can fit on grid (period)
    kmax = nyf;
    wavenums = k0*(0:nyf);
    tke_spectra = zeros(size(nfavg,wavenums));
    tke_avg_spectrum = zeros(size(wavenums));
    
    for l = 1:nfavg
        for ky = 1:nxf
            rky = ky-1;
        
            if ky>kmax+1
                rky = rky-nyf;
            end % conjugate symmetry
            k = sqrt(rky^2);
            tke_spectra(l,k+1) = tke_spectra(l,k+1) + tke_hat(l,k+1)/k0;
        end
    end
    
    %average
    tke_avg_spectrum = sum(tke_spectra)/nfavg;
 
%--------------------------------------------------------------------------
% Create energy cascade plot
%--------------------------------------------------------------------------
    loglog(wavenums, tke_avg_spectrum, 'b')
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z Direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Calculate Velocity in u, v, and w directions
%--------------------------------------------------------------------------
    U = zeros(nfavg,nzf);
    V = zeros(nfavg,nzf);
    W = zeros(nfavg,nzf);
    iter = 0;
    for i = 1:ceil(nxc/sqrt(nfavg)):nxc
        for j = 1:ceil(nyc/sqrt(nfavg)):nyc
            iter = iter+1;
            indi = num2str(i-1);
            indj = num2str(j-1);
            name = strcat(Path, 'Z_', indi, '_', indj);
            data_tmp = importdata(name);
            U(iter,:) = data_tmp(2,:)./data_tmp(5,:); % xvel = momentum/density
            V(iter,:) = data_tmp(3,:)./data_tmp(5,:); % yvel
            W(iter,:) = data_tmp(4,:)./data_tmp(5,:); % zvel
        end
    end
   
%--------------------------------------------------------------------------
% Calculate the Turbulent Kinetic Energy
%--------------------------------------------------------------------------

    % Fourier transform
    u_hat = fftn(U)/nfavg; % Originally fftn(X)/n_tot, do I need to change this?
    v_hat = fftn(V)/nfavg;
    w_hat = fftn(W)/nfavg;

    % Allocate memory for tke
    tke_hat = zeros(nfavg,nzf);

    % calculate tke
    for i = 1:nfavg
        for j = 1:nzf
                tke_hat(i,j) = 0.5*(u_hat(i,j)*conj(u_hat(i,j)) + ...
                    v_hat(i,j)*conj(v_hat(i,j)) + ...
                    w_hat(i,j)*conj(w_hat(i,j)));
        end
    end
    
    % Find average tke
    tke_hat = sum(tke_hat)/nfavg;

%--------------------------------------------------------------------------
% Calculate the spectrum
%--------------------------------------------------------------------------
    k0 = 2*pi/L; % Largest waveform that can fit on grid (period)
    kmax = nzf;
    wavenums = k0*(0:kmax);
    tke_spectra = zeros(size(nfavg,wavenums));
    tke_avg_spectrum = zeros(size(wavenums));
    
    for l = 1:nfavg
    
        for kz = 1:nzf
            rkz = kz-1;

            if kz>kmax+1
                rkz = rkz-nzf;
            end % conjugate symmetry
            k = sqrt(rkz^2);
        
            tke_spectra(l,k+1) = tke_spectra(l,k+1) + tke_hat(l,k+1)/k0;
        end
    end
    
    %average
    tke_avg_spectrum = sum(tke_spectra)/nfavg;
    
%--------------------------------------------------------------------------
% Create energy cascade plot
%--------------------------------------------------------------------------
    loglog(wavenums, tke_avg_spectrum, 'r')
    legend('X', 'Y', 'Z')
    title({'Energy Cascade' '1D'})
    hold off
end