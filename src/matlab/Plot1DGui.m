function varargout = Plot1DGui(varargin)
%PLOT1DGUI M-file for Plot1DGui.fig
%      PLOT1DGUI, by itself, creates a new PLOT1DGUI or raises the existing
%      singleton*.
%
%      H = PLOT1DGUI returns the handle to a new PLOT1DGUI or the handle to
%      the existing singleton*.
%
%      PLOT1DGUI('Property','Value',...) creates a new PLOT1DGUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Plot1DGui_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      PLOT1DGUI('CALLBACK') and PLOT1DGUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in PLOT1DGUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Plot1DGui

% Last Modified by GUIDE v2.5 01-Sep-2012 10:26:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Plot1DGui_OpeningFcn, ...
                   'gui_OutputFcn',  @Plot1DGui_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Plot1DGui is made visible.
function Plot1DGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for Plot1DGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

data = guidata(hObject);
data.bundle = 'X';       % the bundle
data.i1 = 1;             % first dimension for line
data.i2 = 1;             % second dimension for line
data.field = [];         % field values
data.fieldNames = {};    % list of available fields
data.activeField = '';   % current field
data.varSelPopup = [];   % gui element
data.ds = [];            % LBMSCheckpoint

% Initialize sliders
% set(data.slider1,'Min',1,'Max',1)
% set(data.slider1,'Value',1)
% set(data.slider1,'SliderStep',[1,1]);
% set(data.slider2,'Min',1,'Max',1)
% set(data.slider2,'Value',1)
% set(data.slider1,'SliderStep',[1,1]);

guidata(hObject,data);

% UIWAIT makes Plot1DGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Plot1DGui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in DataSetSelector.
function DataSetSelector_Callback(hObject, eventdata, handles)
% hObject    handle to DataSetSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = guidata(hObject);
data.ds = LBMSCheckpoint;

% set the bundles available in the drop-down.
bundleStr = {};
if( ~isempty(data.ds.xBundle) )
  bundleStr{length(bundleStr)+1} = 'X-Bundle';
end
if( ~isempty(data.ds.yBundle) )
  bundleStr{length(bundleStr)+1} = 'Y-Bundle';
end
if( ~isempty(data.ds.zBundle) )
  bundleStr{length(bundleStr)+1} = 'Z-Bundle';
end
set( data.BundleSelector, 'String', bundleStr );

data = setup_variables(data);
data = load_data_from_bundle( data );
render_plot(data)
guidata(hObject,data);

function data = setup_variables(data)
data.fieldNames = data.ds.get_field_names(data.bundle);
% set up variable selector popup
if isempty(data.varSelPopup)
  data.varSelPopup = uicontrol('Style','popup',...
    'String',data.fieldNames,...
    'Position',[140,470,100,20],...
    'Callback',@VarSelector_Callback ...
    );
  uicontrol('Style','text','String','Select variable:','Position',[40,470,100,20] );
else
  set( data.varSelPopup, 'String', data.fieldNames );
end
data.activeField = data.fieldNames{1};


% --- Executes on VarSelector usage
function VarSelector_Callback(hObject,eventdata)
data = guidata(hObject);
data.activeField = data.fieldNames{get(hObject,'Value')};
fprintf('Variable "%s" selected\n',data.activeField);
data = load_data_from_bundle( data );
render_plot(data);
guidata(hObject,data);


function info = load_data_from_bundle( info )
bundle = info.ds.bundle(info.bundle);
[lo1,lo2] = bundle.line_dim();
dim = bundle.field_dim(info.activeField);
switch info.bundle
  case 'Y'
    dim = [dim(2),dim(1),dim(3)];
  case 'Z'
    dim = [dim(3),dim(1),dim(2)];
end
info.field = zeros(dim);
for i1=1:lo1
  for i2=1:lo2
    info.field(:,i1,i2) = info.ds.extract_variable(info.activeField,info.bundle,i1-1,i2-1);
  end
end
fprintf('Loaded "%s" with dimensions [%i,%i,%i]\n',info.activeField,dim);

% set sliders
n1 = size(info.field,2);
n2 = size(info.field,3);
set(info.slider1,'Min',1,'Max',n1);
set(info.slider1,'Value',1);
set(info.slider1,'SliderStep',[1/n1,5/n1]);
set(info.slider2,'Min',1,'Max',n2);
set(info.slider2,'Value',1);
set(info.slider2,'SliderStep',[1/n2,5/n2]);


function render_plot( data )
fprintf('%i\t%i\n',data.i1,data.i2);
plot( data.axes1, data.field(:,data.i1,data.i2), 'b.-' );


% --- Executes on selection change in BundleSelector.
function BundleSelector_Callback(hObject, eventdata, handles)
% hObject    handle to BundleSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BundleSelector contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BundleSelector
data = guidata(hObject);
sel = get(hObject,'Value');
options = get(hObject,'String');
if( strcmp( options{sel}, 'X-Bundle' ) )
  data.bundle = 'X';
elseif( strcmp( options{sel}, 'Y-Bundle' ) )
  data.bundle = 'Y';
elseif( strcmp( options{sel}, 'Z-Bundle' ) )
  data.bundle = 'Z';
else
  error('something bad happened');
end
fprintf('New bundle selected: %c\n',data.bundle);
data = setup_variables(data);
data = load_data_from_bundle(data);
render_plot(data);
guidata(hObject,data);


% --- Executes during object creation, after setting all properties.
function DimSelector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DimSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
b1 = uicontrol('Style','Radio','String','1','pos',[10,2,50,20],'parent',hObject);
b2 = uicontrol('Style','Radio','String','2','pos',[50,2,50,20],'parent',hObject);
set(hObject,'SelectedObject',b1);
set(hObject,'Visible','on');
data = guidata(hObject);
data.dim = '1';
guidata(hObject,data);


% --- Executes during object creation, after setting all properties.
function BundleSelector_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BundleSelector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data = guidata(hObject);
if isempty(data.field) return; end
data.i1 = round(get(hObject,'Value'));
fprintf('[%i,%i]  %f\n',get(data.slider1,'Min'),get(data.slider1,'Max'),data.i1);
guidata(hObject,data);
render_plot( data );


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data = guidata(hObject);
if isempty(data.field) return; end
data.i2 = round(get(hObject,'Value'));
fprintf('[%i,%i]  %f\n',get(data.slider2,'Min'),get(data.slider2,'Max'),data.i2);
guidata(hObject,data);
render_plot( data );
