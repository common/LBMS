/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Options.h"
#include <pokitt/CanteraObjects.h>  // to obtain cantera objects
#include <fields/StringNames.h>

#include <sys/stat.h>

using std::string;

namespace LBMS{

//====================================================================

  IntegratorOptions::IntegratorOptions( const YAML::Node& pg )
  {
    const YAML::Node tspg = pg["TimeStepper"];
    timeintegrator = string_to_ts_method( tspg["TimeIntegrator"].as<string>("Forward Euler") );
    timestep    = tspg["Timestep"      ].as<double>( 1e-8         );
    monitorTime = tspg["MonitorTime"   ].as<double>( timestep*20  );
    fileTime    = tspg["CheckpointTime"].as<double>( timestep*1000);
    endTime     = tspg["EndTime"       ].as<double>( 1e-3         );
  }

//====================================================================

  PoKiTTOptions::PoKiTTOptions( const YAML::Node& pg )
    : pokittParser( pg )
  {
    if( !pg["CanteraInputFile"] ){
      std::ostringstream msg;
      msg << "\nYou must specify 'CanteraInputFile' within the PoKiTT block at main scope\n\n"
          << "PoKiTT:\n\tCanteraInputFile : [filename]\n\tCanteraInputGroup: [groupname]\n\n";
      throw std::invalid_argument( msg.str() );
    }
    canteraInputFileName = pg["CanteraInputFile" ].as<string>();
    canteraGroupName     = pg["CanteraInputGroup"].as<string>("");  // default is empty
    doSpeciesTransport   = pg["SpeciesTransport"];

    doReactions = false;
    if( doSpeciesTransport ) doReactions = pg["SpeciesTransport"]["Reactions"];

    setup_cantera();
    nspecies = CanteraObjects::number_species();

    const StringNames& sName = StringNames::self();

    viscosity   = pg["Viscosity"          ]["name"].as<string>(sName.viscosity           );
    thermCond   = pg["ThermalConductivity"]["name"].as<string>(sName.thermal_conductivity);
    temperature = pg["Temperature"        ]["name"].as<string>(sName.temperature         );
    pressure    = pg["Pressure"           ]["name"].as<string>(sName.pressure            );
  }

  void PoKiTTOptions::setup_cantera()
  {
    try{
      if( canteraInputFileName.substr(canteraInputFileName.find_last_of(".") + 1) == "cti" ){
        const string xmlv = canteraInputFileName.substr(0, canteraInputFileName.find_last_of(".")) + ".xml";
        struct stat buffer;
        if( stat( xmlv.c_str(), &buffer ) == 0 ){
          CanteraObjects::Setup canteraSetup( "Mix", xmlv, canteraGroupName );
          CanteraObjects::setup_cantera( canteraSetup );
          return;
        }
      }
      CanteraObjects::Setup canteraSetup( "Mix", canteraInputFileName, canteraGroupName );
      CanteraObjects::setup_cantera( canteraSetup );
    }
    catch( std::runtime_error& e ){
      std::ostringstream msg;
      msg << __FILE__ << ":" << __LINE__ << std::endl
          << "Error building Cantera object from input file: '"
          << canteraInputFileName << "'\n"
          << "   and group: '" << canteraGroupName << "'\n"
          << e.what() << std::endl;
      throw std::runtime_error( msg.str() );
    }
  }

  Expr::TagList
  PoKiTTOptions::species_tags( const LBMS::Direction dir,
                               const Expr::Context state,
                               const std::string name ) const
  {
    Expr::TagList tags;
    for( size_t i=0; i<CanteraObjects::number_species(); ++i ){
      tags.push_back( create_tag( CanteraObjects::species_name(i) + name, state, dir ) );
    }
    return tags;
  }

  Expr::TagList
  PoKiTTOptions::species_flux_tags( const LBMS::Direction fluxDir,
                                    const LBMS::Direction bundleDir,
                                    const Expr::Context state ) const
  {
    const StringNames& sName = StringNames::self();
    const std::string suffix =  "_" + sName.species_flux
                             + select_from_dir( fluxDir, sName.xface, sName.yface, sName.zface );
    return species_tags( bundleDir, state, suffix );
  }

  Expr::TagList
  PoKiTTOptions::species_flux_tags_reconstructed( const LBMS::Direction fluxDir,
                                                  const LBMS::Direction bundleDir,
                                                  const Expr::Context state,
                                                  const std::string appendage ) const
  {
    const StringNames& sName = StringNames::self();
    const std::string suffix =  "_" + sName.species_flux
                             + select_from_dir( fluxDir, sName.xface, sName.yface, sName.zface )
                             + appendage;
    return species_tags( bundleDir, state, suffix );
  }

//====================================================================

  Options::Options( const YAML::Node& pg )
  : integrator   ( pg ),
    pokittOptions( pg["PoKiTT"] ), // jcs this essentially REQUIRES pokitt for all calculations.  Do we really want/need that?
    filtering    ( false )
  {
    filtering = pg["Filtering"];
  }

//====================================================================

} // namespace LBMS
