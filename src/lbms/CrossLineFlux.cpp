/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file CrossLineFlux.cpp
 *  \date Jan 28, 2013
 *  \author Derek
 */

#include "CrossLineFlux.h"
#include <mpi/Environment.h>

namespace LBMS{

  //------------------------------Constructor----------------------------------------
  template< typename FluxT, typename CoarseFluxT >
  CrossLineFlux<FluxT, CoarseFluxT>::CrossLineFlux( const BundlePtr bundle )
  : dir_ ( bundle->dir()  ),
    npts_( bundle->npts() )
  {
    coarseVolExtent_ = npts_[dir_] / bundle->ncoarse(dir_);

    const Direction faceDir = direction<typename CoarseFluxT::Location::FaceDir>();

    //shift in the face direction when we reconstruct fluxes not volumes
    shift_ = SpatialOps::IntVec(0,0,0);
    switch( faceDir ){
      case LBMS::XDIR:
          shift_ = SpatialOps::IntVec(1,0,0);
        break;
      case LBMS::YDIR:
          shift_ = SpatialOps::IntVec(0,1,0);
        break;
      case LBMS::ZDIR:
          shift_ = SpatialOps::IntVec(0,0,1);
        break;
      case LBMS::NODIR:
        //This case implies a volume communication, thus no shift.
        assert( shift_ == SpatialOps::IntVec(0,0,0) );
        break;
    }//end switch
  }
  //---------------------------------------------------------------------------------

  template< typename FluxT, typename CoarseFluxT >
  void CrossLineFlux<FluxT, CoarseFluxT>::
  create_synthetic_flux( const FluxT&       highWaveNumberFlux,
                         const CoarseFluxT& fullyResolvedFlux,
                               FluxT&       syntheticFlux )
  {
    using namespace SpatialOps;
    double tempFlux;

    const bool isVol = int(FluxT::Location::FaceDir::value) == int(NODIR);

    const double div = isVol ? coarseVolExtent_ : 1.0;

    Direction perpDir1 = get_perp1(dir_);
    Direction perpDir2 = get_perp2(dir_);

    syntheticFlux <<= highWaveNumberFlux;

    for ( size_t idim1=0; idim1!=(npts_[perpDir1]+shift_[perpDir1]); idim1++ ){
      for ( size_t idim2=0; idim2!=(npts_[perpDir2]+shift_[perpDir2]); idim2++ ){

        //location for creation of line windows
        location_[perpDir1] = idim1;
        location_[perpDir2] = idim2;

        //Line window for fine (high wave number) field and constructed field
        const FluxT perpFineLine =  LBMS::get_line_field<FluxT>( highWaveNumberFlux, location_, dir_ );
              FluxT perpSynthLine( get_line_field( syntheticFlux, location_ ) );

        //Line window for coarse (fully resolved) field
        const CoarseFluxT perpCoarseLine( get_line_field( fullyResolvedFlux, location_, dir_ ) );

        ConstFineIterT dirSurfPi  = perpFineLine.begin();
        CoarseIterT    pSurfPi    = perpCoarseLine.begin();
        FineIterT      synthFluxi = perpSynthLine.begin();

        for( ; dirSurfPi < perpFineLine.end(); ++pSurfPi ){
          tempFlux = 0.0;
          for( int k=0 ; k != coarseVolExtent_; ++dirSurfPi, ++k ){
            tempFlux += (*dirSurfPi) / coarseVolExtent_;
          }//Loop over coarse volume to obtain divisor

          //resets dirSurfP1i
          dirSurfPi -= (coarseVolExtent_);

          for( int k=0 ; k != coarseVolExtent_; ++k, ++dirSurfPi, ++synthFluxi ){
            *synthFluxi = *dirSurfPi - tempFlux + ( *pSurfPi / coarseVolExtent_ );
          } //Loop over coarse volume to create a constructed flux
        } //Loop over line
      } //Loop over perp1
    }//Loop over perp2
  }
  //---------------------------------------------------------------------------------

  //=================================================
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::XVolField>::XFace, SpatialOps::SSurfXField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::YVolField>::XFace, SpatialOps::SSurfXField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::ZVolField>::XFace, SpatialOps::SSurfXField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::XVolField>::YFace, SpatialOps::SSurfYField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::YVolField>::YFace, SpatialOps::SSurfYField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::ZVolField>::YFace, SpatialOps::SSurfYField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::XVolField>::ZFace, SpatialOps::SSurfZField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::YVolField>::ZFace, SpatialOps::SSurfZField>;
  template class CrossLineFlux<SpatialOps::FaceTypes<LBMS::ZVolField>::ZFace, SpatialOps::SSurfZField>;
  template class CrossLineFlux<XVolField, SpatialOps::SVolField>;
  template class CrossLineFlux<YVolField, SpatialOps::SVolField>;
  template class CrossLineFlux<ZVolField, SpatialOps::SVolField>;
  //=================================================

} //namespace LBMS
