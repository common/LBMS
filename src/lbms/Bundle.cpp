/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file Bundle.cpp
 *  \date   May 24, 2012
 *  \author James C. Sutherland
 */
#include "Bundle.h"

#include <expression/ExprLib.h>

#include <parser/ParseGroup.h>
#include <mpi/Environment.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/OperatorDatabase.h>
#include <spatialops/Nebo.h>

#include <cmath>
#include <ostream>
#include <istream>

using SpatialOps::IntVec;
using namespace SpatialOps;

namespace LBMS{

  Bundle::Bundle( const Direction dir,
                  const int ncoarse,
                  const IntVec npts,
                  const Coordinate length,
                  const IntVec globMeshOffset,
                  const IntVec bundleOffset )
    : dir_( dir ),
      npts_( npts ),
      globOffset_( globMeshOffset ),
      bundleOffset_( bundleOffset ),
      ncoarse_( ncoarse ),
      length_(length),
      spacing_( length[0]/npts[0],
                length[1]/npts[1],
                length[2]/npts[2] ),
      origin_( globMeshOffset[0]*spacing_[0],
               globMeshOffset[1]*spacing_[1],
               globMeshOffset[2]*spacing_[2] ),
      fml_( new Expr::FieldManagerList() ),
      exprFactory_( new Expr::ExpressionFactory() ),
      opDB_( new SpatialOps::OperatorDatabase() )
  {
    assert( ncoarse>0 );
    assert( npts[0]>0 && npts[1]>0 && npts[2]>0 );
    assert( globMeshOffset[dir] == 0 );

    for( size_t i=0; i<6; ++i ){ btype_[i]=DomainBoundary; }

    {
      IntVec nCoarse(npts);
      nCoarse[dir] = ncoarse;
      fieldInfo_ = new Expr::LBMSFieldInfo( get_boundary_type(XPLUS)==DomainBoundary,
                                            get_boundary_type(YPLUS)==DomainBoundary,
                                            get_boundary_type(ZPLUS)==DomainBoundary,
                                            npts_,
                                            nCoarse );
    }

    globNpts_         = npts_;
    globGlobOffset_   = globOffset_;
    globBundleOffset_ = bundleOffset_;
    globLength_       = length_;
    globSpacing_      = spacing_;
    globOrigin_       = origin_;
  }

  //--------------------------------------------------------------------

  Bundle::~Bundle()
  {
    delete fml_;
    delete exprFactory_;
    delete fieldInfo_;
    delete opDB_;
  }

  //--------------------------------------------------------------------

  int Bundle::id() const
  {
    using SpatialOps::ijk_to_flat;
    int id = 100000 * dir() + ijk_to_flat( Environment::glob_dim(dir()), bundle_offset() );
    return id;
  }

  //--------------------------------------------------------------------

  std::string
  Bundle::name() const
  {
    return get_bundle_name_suffix(dir_);
  }

  //--------------------------------------------------------------------

  SpatialOps::IntVec
  Bundle::owning_cell( const Coordinate point ) const
  {
    const double tol = 10*std::numeric_limits<double>::epsilon();
    return IntVec( std::floor( (point[0]-origin_[0]+tol)/spacing_[0] ),
                   std::floor( (point[1]-origin_[1]+tol)/spacing_[1] ),
                   std::floor( (point[2]-origin_[2]+tol)/spacing_[2] ) );
  }

  //--------------------------------------------------------------------

  Coordinate
  Bundle::cell_coord( const IntVec& point ) const
  {
    Coordinate x;
    for( size_t i=0; i<3; ++i ){
      if(spacing_[i]>0)
          x[i] = ( point[i] +  origin_[i]/spacing_[i] + 0.5 )*spacing_[i];
      else
          x[i]=origin_[i];
      // #   ifndef NDEBUG
      //     // jcs do we really want this check here?
      //     if( point(i) >= npts_[i] || point(i)<0 ){
      //       std::ostringstream msg;
      //       msg << endl
      //           << __FILE__ << " : " << __LINE__  << " - Bundle::cell_coord()" << endl
      //           << "  invalid point requested from " << dir() << " bundle" << endl
      //           << "  " << Direction(i) << " range: (0," << npts_[i]-1 << "), but requested point: " << point(i) << endl;
      //       throw std::runtime_error( msg.str() );
      //     }
      // #   endif
    }
//    std::cout << x << globOffset_ << std::endl;
    return x;
  }

  //--------------------------------------------------------------------

  IntVec
  Bundle::flat2ijk( const size_t pt ) const{
    return SpatialOps::flat_to_ijk( npts_, pt );
  }
  size_t
  Bundle::ijk2flat( const IntVec& ijk ) const{
    return SpatialOps::ijk_to_flat( npts_, ijk );
  }

  //--------------------------------------------------------------------

  void
  Bundle::write_xml( std::ostream& os ) const
  {
    using std::endl;
    const Coordinate origin = cell_coord(IntVec(0,0,0));
    os << "<direction>" << dir() << "</direction>" << endl
       << "<ncoarse>" << ncoarse(dir_) << "</ncoarse>" << endl
       << "<length> " << length(XDIR) << ", " << length(YDIR) << "," << length(ZDIR) << "</length>" << endl
       << "<spacing> " << spacing(XDIR) << ", " << spacing(YDIR) << ", " << spacing(ZDIR) << "</spacing>" << endl
       << "<npts> " << npts(XDIR) << ", " << npts(YDIR) << "," << npts(ZDIR) << "</npts>" << endl
       << "<MeshOffset>" << globOffset_[0] << "," << globOffset_[1] << "," << globOffset_[2] << "</MeshOffset>" << endl
       << "<BundleOffset>" << bundleOffset_[0] << "," << bundleOffset_[1] << "," << bundleOffset_[2] << "</BundleOffset>" << endl
       << "<origin> " << origin[0] << ", " << origin[1] << ", " << origin[2] << "</origin>" << endl;
  }

  BundlePtr create_bundle_from_xml( std::istream& is )
  {
    using std::endl; using std::cout;
    const ParseGroup pg(is);
    return BundlePtr( new Bundle( direction(pg.get_value<std::string>("direction")),
                                  pg.get_value<int>("ncoarse"),
                                  pg.get_value<IntVec>("npts"),
                                  Coordinate( pg.get_value_vec<double>("length") ),
                                  pg.get_value<IntVec>("MeshOffset"),
                                  pg.get_value<IntVec>("BundleOffset") ) );
  }

  //--------------------------------------------------------------------

  std::ostream& operator<<( std::ostream& os, const Bundle& b )
  {
    std::string indent("  ");
    os << b.dir() << " Bundle:"
       << std::endl << indent << "(Lx,Ly,Lz) = (" << b.length(XDIR) << "," << b.length(YDIR) << "," << b.length(ZDIR) << ")"
       << std::endl << indent << "(nx,ny,nz) = (" << b.npts(XDIR) << "," << b.npts(YDIR) << "," << b.npts(ZDIR) << ")"
       << std::endl << indent << "(dx,dy,dz) = (" << b.spacing(XDIR) << "," << b.spacing(YDIR) << "," << b.spacing(ZDIR) << ")"
       << std::endl << indent << "origin     = " << b.cell_coord( IntVec(0,0,0) )
       << std::endl << indent << "offset     = " << b.global_mesh_offset() << " -- " << b.glob_global_mesh_offset()
       << std::endl << indent << "offset     = " << b.bundle_offset()
       << std::endl << indent
       << b.get_boundary_type(XMINUS) << " " << b.get_boundary_type(XPLUS) << " "
       << b.get_boundary_type(YMINUS) << " " << b.get_boundary_type(YPLUS) << " "
       << b.get_boundary_type(ZMINUS) << " " << b.get_boundary_type(ZPLUS) << " "
    << std::endl;
    return os;
  }

  //--------------------------------------------------------------------

  IntVec
  Bundle::fine_to_coarse( IntVec index ) const
  {
    const int rat = npts_[dir_] / ncoarse(dir_);
    index[dir_] /= rat;
    return index;
  }

  //--------------------------------------------------------------------

  size_t
  Bundle::ncoarse(const Direction dim) const{
    if( dim == dir_ ) return ncoarse_;
    return npts(dim);
  }

  SpatialOps::IntVec
  Bundle::ncoarse() const{
    return IntVec( ncoarse(XDIR), ncoarse(YDIR), ncoarse(ZDIR) );
  }


  size_t Bundle::glob_ncoarse(Direction dim) const{
    if( dim == dir_ ) return ncoarse_;
    return glob_npts(dim);
  }

  SpatialOps::IntVec Bundle::glob_ncoarse() const{
    return SpatialOps::IntVec( glob_ncoarse(XDIR), glob_ncoarse(YDIR), glob_ncoarse(ZDIR));
  }

  //--------------------------------------------------------------------

  void Bundle::set_boundary_type( const Faces loc, const BoundaryType bt )
  {
    if( loc>5 || loc<0 ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "LineGroupInfo::set_bc_status() - invalid bc flag"
          << std::endl;
      throw std::invalid_argument(msg.str());
    }
    //set the btype for this Face.
    btype_[loc] = bt;

    // update the LBMSFieldInfo object that is used for field allocation on this bundle
    //the bools are only for the plus side.
    switch( loc ){
    case XPLUS: fieldInfo_->xbcp = (bt==DomainBoundary);  break;
    case YPLUS: fieldInfo_->ybcp = (bt==DomainBoundary);  break;
    case ZPLUS: fieldInfo_->zbcp = (bt==DomainBoundary);  break;
    case XMINUS:  case YMINUS:  case ZMINUS: break;
    }
  }

  void
  Bundle::set_boundary_types( const BoundaryType bt[6] )
  {
    set_boundary_type( XMINUS, bt[0] );
    set_boundary_type( XPLUS , bt[1] );
    set_boundary_type( YMINUS, bt[2] );
    set_boundary_type( YPLUS , bt[3] );
    set_boundary_type( ZMINUS, bt[4] );
    set_boundary_type( ZPLUS , bt[5] );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  inline
  size_t npts( const Direction dim,
               const SpatialOps::GhostData& ghost,
               const BundlePtr& b )
  {
    size_t n = b->npts(dim);
    if( n>1 ){
      n += ghost.get_minus(dim) + ghost.get_plus(dim);
      Faces face;
      switch( dim ){
        case XDIR : face = XPLUS; break;
        case YDIR : face = YPLUS; break;
        case ZDIR : face = ZPLUS; break;
        case NODIR: face = XPLUS; assert(false); break;
      }
      if( dim == direction<typename FieldT::Location::FaceDir>() &&
          b->get_boundary_type(face) == DomainBoundary ){
        ++n;
      }
    }
    return n;
  }
  template< typename FieldT >
  size_t npts( const SpatialOps::GhostData& ghost,
               const BundlePtr& b )
  {
    return npts<FieldT>(XDIR,ghost,b)
        *  npts<FieldT>(YDIR,ghost,b)
        *  npts<FieldT>(ZDIR,ghost,b);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  Coordinate
  coord( SpatialOps::IntVec ijk,
         const SpatialOps::GhostData& ghost,
         const BundlePtr& b )
  {
    if( b->npts(XDIR)>1 ) ijk[0] -= ghost.get_minus(0);
    if( b->npts(YDIR)>1 ) ijk[1] -= ghost.get_minus(1);
    if( b->npts(ZDIR)>1 ) ijk[2] -= ghost.get_minus(2);
    Coordinate c = b->cell_coord(ijk);

    const LBMS::Direction dir = direction<typename FieldT::Location::FaceDir>();
    if( dir != NODIR )  c[dir] -= b->spacing(dir)*0.5;
    return c;
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  SpatialOps::IntVec
  index( const Coordinate& c,
         const SpatialOps::GhostData& ghost,
         const BundlePtr& b )
  {
    SpatialOps::IntVec ijk( b->owning_cell(c) );
    if( b->npts(XDIR)>1 ) ijk[0] += ghost.get_minus(0);
    if( b->npts(YDIR)>1 ) ijk[1] += ghost.get_minus(1);
    if( b->npts(ZDIR)>1 ) ijk[2] += ghost.get_minus(2);
    return ijk;
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  size_t flat( const SpatialOps::IntVec& ijk,
               const SpatialOps::GhostData& ghost,
               const BundlePtr& b )
  {
    const size_t nx = npts<FieldT>(XDIR,ghost,b);
    const size_t ny = npts<FieldT>(YDIR,ghost,b);
    return ijk[0] + ijk[1]*nx + ijk[2]*nx*ny;
  }

  template< typename FieldT >
  SpatialOps::IntVec
  flat2ijk( const unsigned ix,
            const SpatialOps::GhostData& ghost,
            const BundlePtr& b )
  {
    return SpatialOps::flat_to_ijk( IntVec( npts<FieldT>(XDIR,ghost,b), npts<FieldT>(YDIR,ghost,b), npts<FieldT>(ZDIR,ghost,b) ), ix );
  }

  template< typename FieldT >
  unsigned
  ijk2flat( const SpatialOps::IntVec& ijk,
            const SpatialOps::GhostData& ghost,
            const BundlePtr& b )
  {
    return SpatialOps::ijk_to_flat( IntVec( npts<FieldT>(XDIR,ghost,b), npts<FieldT>(YDIR,ghost,b), npts<FieldT>(ZDIR,ghost,b) ), ijk );
  }

  //--------------------------------------------------------------------

#define INSTANTIATE( T )                                                                                                                   \
  template size_t npts<T>(const Direction dim, const SpatialOps::GhostData& ghost, const BundlePtr&);                          \
  template size_t npts<T>(const SpatialOps::GhostData& ghost,const BundlePtr&);                                                \
  template Coordinate coord<T>( SpatialOps::IntVec, const SpatialOps::GhostData& ghost, const BundlePtr&);         \
  template SpatialOps::IntVec index<T>(const Coordinate&, const SpatialOps::GhostData& ghost, const BundlePtr&);   \
  template size_t flat<T>(const SpatialOps::IntVec&, const SpatialOps::GhostData& ghost, const BundlePtr&);        \
  template SpatialOps::IntVec flat2ijk<T>( const unsigned, const SpatialOps::GhostData& ghost, const BundlePtr&);  \
  template unsigned ijk2flat<T>(const SpatialOps::IntVec&, const SpatialOps::GhostData& ghost, const BundlePtr&);

#define INSTANTIATE_GROUP( VolT )                               \
  INSTANTIATE( VolT )                                           \
  INSTANTIATE( SpatialOps::FaceTypes<VolT>::XFace ) \
  INSTANTIATE( SpatialOps::FaceTypes<VolT>::YFace ) \
  INSTANTIATE( SpatialOps::FaceTypes<VolT>::ZFace )

  INSTANTIATE_GROUP( XVolField )
  INSTANTIATE_GROUP( YVolField )
  INSTANTIATE_GROUP( ZVolField )

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  set_coord_values( FieldT& x, FieldT& y, FieldT& z, const BundlePtr& b )
  {
    const SpatialOps::GhostData& ghost = x.get_ghost_data();

    const size_t nx = npts<FieldT>(XDIR,ghost,b);
    const size_t ny = npts<FieldT>(YDIR,ghost,b);
    const size_t nz = npts<FieldT>(ZDIR,ghost,b);
#   ifndef NDEBUG
    {
      const SpatialOps::MemoryWindow& xmw = x.window_with_ghost();
      const SpatialOps::MemoryWindow& ymw = y.window_with_ghost();
      const SpatialOps::MemoryWindow& zmw = z.window_with_ghost();
      assert(xmw.extent(0)==nx);  assert(ymw.extent(0)==nx);  assert(zmw.extent(0)==nx);
      assert(xmw.extent(1)==ny);  assert(ymw.extent(1)==ny);  assert(zmw.extent(1)==ny);
      assert(xmw.extent(2)==nz);  assert(ymw.extent(2)==nz);  assert(zmw.extent(2)==nz);
    }
#   endif
    typename FieldT::iterator ix=x.begin(), iy=y.begin(), iz=z.begin();
    for( size_t k=0; k<nz; ++k ){
      for( size_t j=0; j<ny; ++j ){
        for( size_t i=0; i<nx; ++i ){
          const Coordinate c = coord<FieldT>( IntVec(i,j,k), ghost, b );
          *ix = c[0];
          *iy = c[1];
          *iz = c[2];
          ++ix; ++iy; ++iz;
        }
      }
    }
  }

  template<typename FieldT>
  void
  set_coarse_coords( FieldT& x, FieldT& y, FieldT& z, const BundlePtr& b )
  {
    const Coordinate len( b->length(XDIR), b->length(YDIR), b->length(ZDIR) );
    const SpatialOps::Grid grid( b->ncoarse(), len );
    grid.set_coord<SpatialOps::XDIR>( x );
    grid.set_coord<SpatialOps::YDIR>( y );
    grid.set_coord<SpatialOps::ZDIR>( z );

    const Coordinate origin = b->origin();
    x <<= x + origin[0];
    y <<= y + origin[1];
    z <<= z + origin[2];
  }

#define INSTANTIATE_COARSE_COORDS( T )               \
  template<>                                         \
  void set_coord_values<T>( T& x, T& y, T& z,        \
    const BundlePtr& b )                             \
  {                                                  \
    set_coarse_coords(x,y,z,b);                      \
  }

  INSTANTIATE_COARSE_COORDS( SpatialOps::SVolField   );
  INSTANTIATE_COARSE_COORDS( SpatialOps::SSurfXField );
  INSTANTIATE_COARSE_COORDS( SpatialOps::SSurfYField );
  INSTANTIATE_COARSE_COORDS( SpatialOps::SSurfZField );

#define INSTANTIATE_COORD_VALS( T )  \
  template void set_coord_values<T>( T&, T&, T&, const BundlePtr& );

#define INSTANTIATE_VARIANTS( VolT )                                          \
    INSTANTIATE_COORD_VALS( VolT )                                            \
    INSTANTIATE_COORD_VALS( SpatialOps::FaceTypes<VolT>::XFace )  \
    INSTANTIATE_COORD_VALS( SpatialOps::FaceTypes<VolT>::YFace )  \
    INSTANTIATE_COORD_VALS( SpatialOps::FaceTypes<VolT>::ZFace )

  INSTANTIATE_VARIANTS( XVolField );
  INSTANTIATE_VARIANTS( YVolField );
  INSTANTIATE_VARIANTS( ZVolField );

} // namespace LBMS
