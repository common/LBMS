/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>

//--- LBMS Stuff ---//
#include <fields/Fields.h>
#include <lbms/ProblemSolver.h>
#include <lbms/Options.h>
#include <lbms/Bundle.h>
#include <lbms/BCOptions.h>
#include <lbms/ProblemSolver.h>
#include <mpi/Environment.h>
#include <mpi/FieldBundleExchange.h>
#include <operators/Operators.h>
#include <transport/ParseEquation.h>
#include <transport/DensityEquation.h>
#include <transport/SpeciesEquation.h>
#include <yaml-cpp/yaml.h>
#include <parser/ParseTools.h>
#include <parser/BasicExprBuilder.h>
#include <parser/PoKiTTExprBuilder.h>
#include <fields/StringNames.h>

//--- ExprLib Headers ---//
#include <expression/ExprLib.h>
#include <expression/TimeStepper.h>

using std::endl;
using std::vector;

namespace LBMS{

  void build_coord_exprs( const BundlePtr bundle,
                          Expr::ExpressionFactory* const factory,
                          GraphHelper* const graph )
  {
    switch( bundle->dir() ){
    case XDIR: LBMS::build_coord_expr<XVolField>( factory, bundle, graph ); break;
    case YDIR: LBMS::build_coord_expr<YVolField>( factory, bundle, graph ); break;
    case ZDIR: LBMS::build_coord_expr<ZVolField>( factory, bundle, graph ); break;
    case NODIR: assert(false);
    }
  }

  //------------------------------------------------------------------

  ProblemSolver::ProblemSolver( const YAML::Node& parser,
                                GraphCategories& gc,
                                const BundlePtr& bundle,
                                const Options& options,
                                Expr::TimeStepper& ts )
    : parser_ ( parser  ),
      bundle_ ( bundle  ),
      options_( options ),
      gc_     ( gc      )
  {
    try{
      build_operators( bundle_ );
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error building spatial operators.\n"
          << "\t" << __FILE__ << ":" << __LINE__ << endl
          << e.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    try{
      densityTag_ = parse_nametag( parser["Density"]["NameTag"], bundle_->dir() );
    }
    catch( std::exception& err ){
      std::ostringstream msg;
      msg << "Error obtaining density.\nPlease add a <Density> block to your input file in the <LBMS> scope\n"
          << "\t" << __FILE__ << " : " << __LINE__ << endl
          << err.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    //--- Build basic expressions from the input file
    LBMS::create_expressions_from_input( parser, bundle, gc );

    build_coord_exprs( bundle_, gc[ADVANCE_SOLUTION]->exprFactory, gc[ADVANCE_SOLUTION] );

    build_pokitt_expressions( options.pokittOptions, bundle, *gc[ADVANCE_SOLUTION]->exprFactory );

    NSCBCVecType<XVolField>::NSCBCVec *nscbcVecX = NULL;
    NSCBCVecType<YVolField>::NSCBCVec *nscbcVecY = NULL;
    NSCBCVecType<ZVolField>::NSCBCVec *nscbcVecZ = NULL;

    try{
      switch(bundle->dir()){
        case XDIR: nscbcVecX = new NSCBCVecType<XVolField>::NSCBCVec( build_boundary_conditions<XVolField>() ); break;
        case YDIR: nscbcVecY = new NSCBCVecType<YVolField>::NSCBCVec( build_boundary_conditions<YVolField>() ); break;
        case ZDIR: nscbcVecZ = new NSCBCVecType<ZVolField>::NSCBCVec( build_boundary_conditions<ZVolField>() ); break;
        case NODIR: assert(false);
      }
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error building boundary conditions \n\t"
          << __FILE__ << ":" << __LINE__ << endl
          << e.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    try{
      proc0cout << endl << "Building equations on " << bundle_->name() << endl;
      switch(bundle->dir()){
        case XDIR: build_equations<XVolField>( ts, *nscbcVecX ); break;
        case YDIR: build_equations<YVolField>( ts, *nscbcVecY ); break;
        case ZDIR: build_equations<ZVolField>( ts, *nscbcVecZ ); break;
        case NODIR: assert(false);
      }
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error building transport equations\n\t"
          << __FILE__ << ":" << __LINE__ << endl
          << e.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    try{
      if( parser_["MomentumEquations"] ){
        proc0cout << endl << "Building momentum equations on " << bundle_->name() << endl;
        
        switch(bundle_->dir()){
          case XDIR: build_momentum_equations<XVolField>( ts, *nscbcVecX ); break;
          case YDIR: build_momentum_equations<YVolField>( ts, *nscbcVecY ); break;
          case ZDIR: build_momentum_equations<ZVolField>( ts, *nscbcVecZ ); break;
          case NODIR: assert(false);
        }
      }
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error building momentum equations\n\t"
          << __FILE__ << ":" << __LINE__ << endl
          << e.what() << endl;
      throw std::runtime_error( msg.str() );
    }

    // Update ghost cells for solution variables for filtering
    // Necessary for non-periodic as well (to update processor boundary cells)
    if( options_.filtering ){
      Expr::ExpressionFactory& factory = *gc_[ADVANCE_SOLUTION]->exprFactory;
      BOOST_FOREACH( EquationBase* eqn, eqns_ ){
        switch(bundle_->dir()){
        case XDIR: communicate_expression_result<XVolField>( eqn->get_rhs_tag(), bundle_, factory ); break;
        case YDIR: communicate_expression_result<YVolField>( eqn->get_rhs_tag(), bundle_, factory ); break;
        case ZDIR: communicate_expression_result<ZVolField>( eqn->get_rhs_tag(), bundle_, factory ); break;
        case NODIR: assert(false); break;
        }
      }
    }



    initial_conditions();

    if( nscbcVecX != NULL) delete nscbcVecX;
    if( nscbcVecY != NULL) delete nscbcVecY;
    if( nscbcVecZ != NULL) delete nscbcVecZ;
  }

  //--------------------------------------------------------------------

  ProblemSolver::~ProblemSolver()
  {
    BOOST_FOREACH( LBMS::EquationBase* eqn, eqns_ ){
      delete eqn;
    }
  }

  //------------------------------------------------------------------
  template<typename FieldT>
  void
  ProblemSolver::
  build_equations( Expr::TimeStepper& ts, const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec nscbcVec )
  {
    if( parser_["ScalarTransportEquation"] ){
      const int nghost = 1;
      /*
       *  1. Parse each equation and add it to the timestepper.
       *
       *  2. Request communication for each solution variable.  Unfortunately,
       *    this will essentially synchronize the entire graph as we wait on
       *    communication before the bottom layer of nodes can proceed.
       */
      const YAML::Node transEqnVec( parser_["ScalarTransportEquation"] );
      for( YAML::Node::const_iterator ieq=transEqnVec.begin(); ieq!=transEqnVec.end(); ++ieq ){
        LBMS::EquationBase* transEqn = NULL;
        transEqn = parse_equation<FieldT>( bundle_, *ieq, options_, densityTag_, gc_, nscbcVec );
        ts.add_equation<FieldT>( transEqn->solution_variable_name(),
                                 transEqn->get_rhs_tag(),
                                 nghost,
                                 bundle_->id() );
        communicate_expression_result<FieldT>(
            Expr::Tag( transEqn->solution_variable_name(), Expr::STATE_N ),
            bundle_, *gc_[ADVANCE_SOLUTION]->exprFactory );

        eqns_.push_back( transEqn );

      } // loop over equations
    }

    // set up species transport equations
    if( options_.pokittOptions.doSpeciesTransport ){
      Expr::ExpressionFactory& factory = *gc_[ADVANCE_SOLUTION]->exprFactory;
//      for( size_t i=0; i<options_.pokittOptions.nspecies; ++i ){
      for( size_t i=0; i<options_.pokittOptions.nspecies-1; ++i ){
        typedef SpeciesEquation<FieldT> SpecEqn;
        TransportEquationBase<FieldT>* specEqn = new SpecEqn( factory, bundle_, i, options_,
                                                              parser_["MomentumEquations"], // determines if we do momentum.
                                                              gc_, nscbcVec );
        ts.add_equation<FieldT>( specEqn->solution_variable_name(), specEqn->get_rhs_tag(), 1, bundle_->id() );
        communicate_expression_result<FieldT>( Expr::Tag( specEqn->solution_variable_name(), Expr::STATE_N ), bundle_, factory );
        eqns_.push_back( specEqn );
      }
    }
  }

  //------------------------------------------------------------------

  template< typename FieldT >
  const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec
  ProblemSolver::build_boundary_conditions()
  {
    try{
      // Parse the Boundary Conditions and then attach them to the field expression.
      const typename NSCBCVecType<FieldT>::NSCBCVec nscbcVec = parse_boundary_conditions<FieldT>(parser_, bundle_, gc_, options_ );
      return nscbcVec;
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error parsing boundary conditions \n\t"
          << __FILE__<< " : " << __LINE__ << endl
          << e.what() << endl;
      throw std::runtime_error(msg.str() );
    }
  }

  //------------------------------------------------------------------
  template<typename FieldT>
  void ProblemSolver::build_momentum_equations( Expr::TimeStepper& ts, const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec nscbcVec )
  {
    const YAML::Node momentumPG = parser_["MomentumEquations"];

    Expr::ExpressionFactory& factory = *gc_[ADVANCE_SOLUTION]->exprFactory;

    const int nghost = 1;

    vector< TransportEquationBase<FieldT>* >
    transEqns = parse_momentum_equations<FieldT>( bundle_, momentumPG, options_, densityTag_, gc_, nscbcVec );
    for( typename vector<TransportEquationBase<FieldT>*>::iterator ieqn = transEqns.begin(); ieqn!=transEqns.end(); ++ieqn ){
      ts.add_equation<FieldT>( (*ieqn)->solution_variable_name(),
                               (*ieqn)->get_rhs_tag(),
                               nghost,
                               bundle_->id() );
      eqns_.push_back( *ieqn );
      //--- Request communication for each solution variable.  Unfortunately,
      //    this will essentially synchronize the entire graph as we wait on
      //    communication before the bottom layer of nodes can proceed.
      communicate_expression_result<FieldT>( Expr::Tag( (*ieqn)->solution_variable_name(), Expr::STATE_N ),
                                             bundle_, factory );
    }

    if( parser_["DensityEquation"] ){
      typedef DensityEquation<FieldT> DensityEqn;
      DensityEqn* densEqn = new DensityEqn( *gc_[ADVANCE_SOLUTION]->exprFactory, bundle_, options_, true, gc_, nscbcVec );
      ts.add_equation<FieldT>( densityTag_.name(), densEqn->get_rhs_tag(), nghost, bundle_->id() );
      eqns_.push_back( densEqn );
      communicate_expression_result<FieldT>( densityTag_, bundle_, factory );
    }
  }

  //------------------------------------------------------------------

  void ProblemSolver::initial_conditions()
  {
    GraphHelper* const icgh = gc_[INITIALIZATION];

    //--- build expressions to set coordinate values in case they are required
    build_coord_exprs( bundle_, icgh->exprFactory, icgh );

    //_____________________________________________________
    // set up initial conditions on this transport equation
    BOOST_FOREACH( LBMS::EquationBase* te, eqns_ ){
      try{
        icgh->rootIDs.insert( te->initial_condition( *icgh->exprFactory ) );
      }
      catch( std::exception& e ){
        std::ostringstream msg;
        msg << __FILE__ <<" : " << __LINE__ << endl
            << "ERORR while setting initial conditions on equation '" << te->solution_variable_name() << "'"
            << endl << e.what() << endl ;
        throw std::runtime_error( msg.str() );
      }
    }
  }

} // namespace LBMS


