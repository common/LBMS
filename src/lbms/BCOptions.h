/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMS_BCOptions_h
#define LBMS_BCOptions_h

#include <vector>

#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <lbms/GraphHelperTools.h>
#include <lbms/Options.h>
#include <lbms/bcs/PointCollection.h>

#include <fields/Fields.h>

#include <yaml-cpp/yaml.h>
#include <parser/ParseTools.h>

#include <expression/Tag.h>
#include <expression/Expression.h>

// NSCBC includes
#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>
#include <nscbc/TagManager.h>

namespace LBMS{

  //forward declarations
  struct Options;

  //Options for types of Boundary Conditions; still being implemented
  enum BdType{ WALL, OUTFLOW, INFLOW, INVALID };

  template<typename FieldT>
  struct NSCBCVecType{
      typedef std::vector< std::pair < boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, PointCollection2D > > NSCBCVec;
  };


  /**
   * \brief convert from the string to the BDType preset
   */
  BdType select_bd_type( const std::string& bcTypeStr );

  /**
   * \brief convert from the BdType to the string
   */
  const std::string bd_type_to_string( const BdType bdtype );

  /**
   * \brief convert from the string to the BCType
   */
  SpatialOps::BCType select_bctype( const std::string& bctypestr );

  /**
   * \brief convert from the BCType to the string.
   */
  const std::string bctype_to_string( const SpatialOps::BCType bct );

  //==================================================================

  //The options class for the Boundary conditions filled out by the parser.
  struct BCOptions
  {
    SpatialOps::BCType bctype_;           ///< NEUMANN, DIRICHLET
    const PointCollection2D points;       ///< The points that this BC is associated with
    double val;                           ///< Value if constant.
    bool isfunct;                         ///< is it a function or just a constant.
    LBMS::Faces face_;                    ///< Face that the boundary condition applies to.
    const Expr::Tag tag_;                 ///< Tag of applicable expression.
    const Direction dir_;                 ///< Direction that is applied
    const LBMS::Options& lbmsOptions_;

    BCOptions( const PointCollection2D& pc, const Expr::Tag& tag, const Direction dir, const LBMS::Options& options )
    : points( pc ),
      tag_( tag ),
      dir_( dir ),
      lbmsOptions_( options )
    {}
  private:
    BCOptions(); // no default construction!
  };

  /**
   * @brief Parse all BCs from the input file.
   * @param parser Should contain 'BoundaryCondition' and 'Geometries' blocks
   * @param bundle the bundle we are parsing on
   * @param gc graphcategories
   * @param options LBMS::Options
   * @return
   */
  template< typename FieldT >
  typename NSCBCVecType<FieldT>::NSCBCVec
  parse_boundary_conditions( const YAML::Node& parser,
                             const BundlePtr bundle,
                             GraphCategories& gc,
                             const LBMS::Options& options );


} // namespace LBMS

#endif
