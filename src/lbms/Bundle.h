/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   Bundle.h
 *
 *  \date   May 24, 2012
 *  \author James C. Sutherland
 */

#ifndef LBMS_BUNDLE_H_
#define LBMS_BUNDLE_H_

#include "Direction.h"
#include "Coordinate.h"
#include "Bundle_fwd.h"

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/GhostData.h>


///////////////////////
// jcs remove
#ifdef HAVE_MPI
#include <boost/mpi.hpp>
#endif
///////////////////////

#include <expression/ExprLib.h>

#include <list>
#include <iosfwd>

namespace SpatialOps{
  class OperatorDatabase;
}

namespace Expr{
  struct LBMSFieldInfo;
  class FieldManagerList;
  class ExpressionFactory;
}

namespace LBMS{

  /**
   *  \class Bundle
   *  \brief Describes the dimensions of an LBMS bundle - a collection
   *         of lines oriented in a given direction.
   */
  class Bundle
  {
  public:

    /**
     *  \brief Construct a Bundle
     *  \param dir     Orientation of this bundle.
     *  \param ncoarse Number of coarse points in the bundle direction
     *  \param npts    Number of points in the bundle
     *  \param length  length of the bundle in each direction
     *  \param globIndexOffset Offset of this bundle on the global mesh
     *  \param bundleOffset
     */
    Bundle( const Direction dir,
            const int ncoarse,
            const SpatialOps::IntVec npts,
            const Coordinate length,
            const SpatialOps::IntVec globIndexOffset,
            const SpatialOps::IntVec bundleOffset );

    ~Bundle();

    /**
     * \brief obtain a unique identifier for this bundle.
     */
    int id() const;

    /** \return the Expr::LBMSFieldInfo object required to allocate fields on a graph */
    const Expr::LBMSFieldInfo& get_field_info() const{ return *fieldInfo_; }

    /** \return the OperatorDatabase associated with this Bundle */
    SpatialOps::OperatorDatabase& operator_database(){ return *opDB_; }
    const SpatialOps::OperatorDatabase& operator_database() const{ return *opDB_; }

    /**
     *  \brief obtain the number of points on this bundle in the given direction.
     *  \param dim the Direction of interest
     *  \return the number of points in the requested direction
     */
    inline int npts( const Direction dim ) const{ return npts_[dim]; }

    /** \return obtain the global mesh (i,j,k) index for origin of this bundle */
    inline SpatialOps::IntVec global_mesh_offset() const{ return globOffset_; }

    /** \return obtain the offset for this bundle (i.e., how many bundles precede it in each direction locally) */
    inline SpatialOps::IntVec bundle_offset() const{ return bundleOffset_; }

    inline Coordinate origin() const{ return origin_; }

    /** \brief Obtain the length of the bundle in the requested direction */
    inline double length( const Direction dim ) const{ return length_[dim]; }
    inline Coordinate length() const{ return length_; }

    /** \brief obtain the mesh spacing in the requested dimension */
    inline double spacing( const Direction dim ) const{ return spacing_[dim]; }
    inline Coordinate spacing() const{ return spacing_; }

    /** \brief obtain the vector of number of points in each direction for this bundle. */
    inline const SpatialOps::IntVec& npts() const{return npts_;}

    /** \brief obtain the orientation for this bundle (the direction of high resolution) */
    inline Direction dir() const{ return dir_; }

    /** \brief obtain the name of this bundle. */
    std::string name() const;

    /** \brief obtain the index (0-based at interior, excludes ghost cells) for the cell containing the given Coordinate */
    SpatialOps::IntVec owning_cell( const Coordinate ) const;

    /** \brief given the flat index of a cell, return its ijk index. */
    SpatialOps::IntVec flat2ijk( const size_t ) const;

    /** \brief given the IntVec, return a flat index. */
    size_t ijk2flat( const SpatialOps::IntVec& ) const;

    /** \brief given the IntVec of a cell, return its centroid Coordinate. */
    Coordinate cell_coord( const SpatialOps::IntVec& ) const;

    /** \brief obtain the number of coarse points */
    size_t ncoarse(const Direction dim) const;
    SpatialOps::IntVec ncoarse() const;

    inline int glob_npts( const Direction dim ) const{ return globNpts_[dim]; }

    /** \return obtain the global mesh (i,j,k) index for origin of the parent bundle */
    inline SpatialOps::IntVec glob_global_mesh_offset() const{ return globGlobOffset_; }

    /** \return obtain the offset for the parent bundle (i.e., how many bundles precede it in each direction locally) */
    inline SpatialOps::IntVec glob_bundle_offset() const{ return globBundleOffset_; }

    inline Coordinate glob_origin() const{ return globOrigin_; }

    /** \brief Obtain the length of the parent bundle in the requested direction */
    inline double glob_length( const Direction dim ) const{ return globLength_[dim]; }
    inline Coordinate glob_length() const{ return globLength_; }

    /** \brief obtain the mesh spacing in the requested dimension for the parent */
    inline double glob_spacing( const Direction dim ) const{ return globSpacing_[dim]; }
    inline Coordinate glob_spacing() const{ return globSpacing_; }

    /** \brief obtain the vector of number of points in each direction for the parent bundle. */
    inline const SpatialOps::IntVec& glob_npts() const{return globNpts_;}

    size_t glob_ncoarse(Direction dim) const;
    SpatialOps::IntVec glob_ncoarse() const;

    /**
     * \brief Given an index on the fine mesh, obtain the corresponding index on the coarse mesh.
     * \param fine the index on the fine mesh
     * \return the index on the coarse mesh
     */
    SpatialOps::IntVec
    fine_to_coarse( SpatialOps::IntVec fine ) const;

    /**
     *  \brief set whether this bundle has a physical boundary on the given face
     *  \param loc The face of interest.  Faces are ordered (-x +x -y
     *         +y -z +z) so that (3) refers to the -y face
     *  \param bt specifies the type of boundary (see BoundaryType enum)
     */
    void set_boundary_type( const Faces loc, const BoundaryType bt );

    /**
     *  \brief set whether this bundle has a physical boundary on the given face
     *  \param bt specifies the type of boundary (see BoundaryType enum) on each of the six faces
     */
    void set_boundary_types( const BoundaryType bt[6] );

    /**
     *  \brief determine the boundary type for the given face of the bundle
     *  \param loc The face of interest.
     *  \return the BoundaryType
     */
    inline BoundaryType get_boundary_type( const Faces loc ) const{
      return btype_[loc];
    }

    /**
     * \brief obtain the FieldManagerList associated with this bundle.
     */
    Expr::FieldManagerList* get_field_manager_list(){ return fml_; }
    const Expr::FieldManagerList* get_field_manager_list() const{ return fml_; }

    void write_xml( std::ostream& ) const;

    void set_glob_length (const Coordinate  l){globLength_=l;}
    void set_glob_spacing(const Coordinate  l){globSpacing_=l;}
    void set_glob_origin (const Coordinate  l){globOrigin_=l;}
    void set_glob_npts        (const SpatialOps::IntVec vec){globNpts_=vec;}
    void set_glob_globOffset  (const SpatialOps::IntVec vec){globGlobOffset_= vec;}
    void set_glob_bundleOffset(const SpatialOps::IntVec vec){globBundleOffset_=vec;}

  private:

    const Direction dir_;
    const SpatialOps::IntVec npts_, globOffset_, bundleOffset_;

    /**
     * globGlobOffset_ retains the globOffset from the very first parent bundle, globBundleOffset retains bundleOffset_
     * from the first parent bundle. This is used for outputting the correct global information on the Checkpoints.
     * Used by Checkpoint.cpp and set in the Partitioner.
     */
    SpatialOps::IntVec globNpts_, globGlobOffset_, globBundleOffset_;
    Coordinate globLength_, globSpacing_, globOrigin_;
    const int ncoarse_;
    const Coordinate length_, spacing_, origin_;

    ///////////////////////////////////
    // jcs remove
#   ifdef HAVE_MPI
    /** \brief Array of mpisenders to free at the end of the time step */
    std::list<boost::mpi::request> mpiReqs_;
#   endif
    ///////////////////////////////////

    Bundle& operator=(const Bundle& other);  // no assignment
    Bundle( const Bundle& );  // no copying
    Bundle(); // no default constructor

    BoundaryType btype_[6]; /**< flags for physical (domain) boundaries on each
                                 bundle face (default true).  If this bundle
                                 borders a physical boundary, these will be true */

    Expr::FieldManagerList*  fml_;
    Expr::ExpressionFactory* exprFactory_;
    Expr::LBMSFieldInfo* fieldInfo_;
    SpatialOps::OperatorDatabase* const opDB_;
  };

  std::ostream& operator<<( std::ostream&, const Bundle& ); ///< output information on a Bundle object
  inline std::ostream& operator<<( std::ostream& os, const BundlePtr& p ){
    os << *p; return os;
  }


  /**
   * \fn BundlePtr create_bundle_from_xml( std::istream& )
   * \param is the xml stream (file or otherwise) to read from
   * \return a BundlePtr obtained from reading the given stream
   * \see{ Bundle::write_xml }
   */
  BundlePtr create_bundle_from_xml( std::istream& is );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > size_t npts( const Direction dim, const Bundle& b )
   *  \brief Obtain the number of points in the given direction for this field (including ghosts)
   *  \return the number of points in the field associated with the given bundle in the requested direction.
   */
  template< typename FieldT >
  size_t npts( const Direction dim,  ///< the Direction of interest
               const SpatialOps::GhostData& ghost,  ///< ghost information
               const BundlePtr& b    ///< the Bundle of interest
  );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > size_t npts( const Bundle& b )
   *  \brief determine the total number of points in this field (including ghosts)
   *  \param ghost Information about the number of ghost cells on the field
   *  \param b the Bundle of interest
   */
  template< typename FieldT >
  size_t npts( const SpatialOps::GhostData& ghost,
               const BundlePtr& b );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > SpatialOps::IntVec flat2ijk( const unsigned int ix, const BundlePtr& b )
   *  \brief Obtain the IndexTriplet corresponding to the given flat
   *         index on a field type associated with the given bundle.
   *  \return the IndexTriplet corresponding to the given flat index
   */
  template< typename FieldT >
  SpatialOps::IntVec
  flat2ijk( const unsigned int ix, ///<  the flat index of interest
            const SpatialOps::GhostData& ghost,  ///< ghost information
            const BundlePtr& b     ///< the Bundle that this field lives on
  );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > unsigned int ijk2flat( const IndexTriplet& ijk, const Bundle& b )
   *  \brief obtain a flat index given the IndexTriplet for this field type on this bundle.
   */
  template< typename FieldT >
  unsigned int
  ijk2flat( const SpatialOps::IntVec& ijk, ///< the IndexTriplet that we want to convert to a flat index.
            const SpatialOps::GhostData& ghost,  ///< ghost information
            const BundlePtr& b                         ///<  the Bundle that this field lives on.
  );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > Coordinate coord( IndexTriplet ijk, const Bundle& b )
   *  \brief Obtain the Coordinate for the given field type at the
   *         requested ijk IndexTriplet location on the requested bundle.
   *  \param The IndexTriplet of the cell (0-based with ghosts)
   *  \param ghost the Ghost information for this field type
   *  \param the Bundle that this field resides on
   *  \return The Coordinate of this cell's center.
   */
  template< typename FieldT >
  Coordinate
  coord( SpatialOps::IntVec ijk,
         const SpatialOps::GhostData& ghost,
         const BundlePtr& b );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > IndexTriplet index( const Coordinate& c, const Bundle& b )
   *  \brief Calculates the field index associated with the given coordinate
   */
  template< typename FieldT >
  SpatialOps::IntVec
  index( const Coordinate& c,  ///< The Coordinate of interest
         const SpatialOps::GhostData& ghost,  ///< ghost information
         const BundlePtr& b    ///< The Bundle that this field resides on.
  );

  /**
   *  \ingroup Tools
   *  \fn template< typename FieldT > size_t flat( const IndexTriplet ijk, const Bundle& b )
   *  \brief Calculate the flat index given the IndexTriplet and Bundle.
   */
  template< typename FieldT >
  size_t flat( const SpatialOps::IntVec& ijk, ///< The IndexTriplet that we want to convert to a flat index
               const SpatialOps::GhostData& ghost,  ///< ghost information
               const BundlePtr& b                         ///< the Bundle in consideration
  );

  //====================================================================

  /**
   *  \ingroup Tools
   *  \fn void set_coord_values( FieldT& x, FieldT& y, FieldT& z, const Mesh& mesh )
   *  \brief set the field coordinates (x,y,z)
   *
   *  Note that this uses fortran indexing (first dimension varies fastest)
   */
  template< typename FieldT >
  void
  set_coord_values( FieldT& x,          ///< x The x coordinate of the field
                    FieldT& y,          ///< y The y coordinate of the field
                    FieldT& z,          ///< z The z coordinate of the field
                    const BundlePtr& b  ///< b the bundle that this field is associated with.
                    );

  /** @} */

} // LBMS

#endif /* LBMS_BUNDLE_H_ */
