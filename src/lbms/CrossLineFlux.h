/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   CrossLineFlux.h
 *  \date   2013
 *  \author Derek A. Cline
 */

#include <lbms/Bundle.h>
#include <fields/Fields.h>

using SpatialOps::IntVec;
using SpatialOps::MemoryWindow;

#ifndef CrossLineFlux_H_
#define CrossLineFlux_H_

namespace LBMS{

  /**
   * @class CrossLineFlux
   * @brief Creates an error value constructed from summing high wave number fluxes
   *        and subtracting the fully resolved cross line flux
   */
  template< typename FluxT, typename CoarseFluxT >
  class CrossLineFlux
  {
  public:
    /**
     * @brief creates a CrossLineFlux object
     */
    CrossLineFlux( const BundlePtr );

    typedef typename FluxT::iterator             FineIterT;
    typedef typename FluxT::const_iterator       ConstFineIterT;
    typedef typename CoarseFluxT::const_iterator CoarseIterT;

    /**
     * @brief Constructs the error field
     *        Note: The first perpendicular direction is y for x-dir and x for y-dir and z-dir
     *              The second perpendicular direction is y for z-dir and z for x-dir and y-dir
     *
     * @param highWaveNumberFlux1 Flux that is first perpendicular to DirT on the DirT bundle
     * @param fullyResolvedFlux1 Flux that is first perpendicular to DirT on a Coarse Field
     * @param syntheticFlux1 Flux that is constructed, first perpendicular to DirT on a Coarse Field
     */

    void create_synthetic_flux( const FluxT&       highWaveNumberFlux,
                                const CoarseFluxT& fullyResolvedFlux,
                                      FluxT&       syntheticFlux );

  protected:

  private:
    IntVec location_;
    const IntVec npts_;
    IntVec shift_;           ///< When non-periodic, there will be an additional point in the flux face direction
    double coarseVolExtent_; ///< points in a coarse volume in DirT
    const Direction dir_;    ///< Direction constructed from DirT
  };
}//LBMS namespace

#endif /* CrossLineFlux_H_ */
