/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Direction.h"

#include <fields/StringNames.h>
#include <fields/Fields.h>

#include <string>
#include <ostream>

namespace LBMS{

  std::ostream& operator<<( std::ostream& os, const Direction d )
  {
    os << get_dir_name(d);
    return os;
  }

  Direction direction( const std::string& s )
  {
    if     ( s == get_dir_name(XDIR) ) return XDIR;
    else if( s == get_dir_name(YDIR) ) return YDIR;
    else if( s == get_dir_name(ZDIR) ) return ZDIR;
    return NODIR;
  }

  unsigned direction( const Direction d )
  {
    switch( d ){
    case XDIR: return 0;
    case YDIR: return 1;
    case ZDIR: return 2;
    default:
      assert(false);
    }
    return 999;
  }

  Direction direction( const size_t i )
  {
    Direction d;
    switch(i){
    case 0 : d=XDIR; break;
    case 1 : d=YDIR; break;
    case 2 : d=ZDIR; break;
    default: d=NODIR; break;
    }
    return d;
  }

  std::string get_dir_name( const Direction dir )
  {
    const StringNames& sName = StringNames::self();
    std::string name;
    switch(dir){
    case XDIR:  name=sName.x;   break;
    case YDIR:  name=sName.y;   break;
    case ZDIR:  name=sName.z;   break;
    case NODIR: name="INVALID"; break;
    }
    return name;
  }

  //====================================================================

  std::string get_bundle_name_suffix( const Direction dir ){
    return std::string( "_" + get_dir_name(dir) + "bundle" );
  }

  //====================================================================

  Direction get_perp1( const Direction dir ){ return dir == XDIR ? YDIR : XDIR; }
  Direction get_perp2( const Direction dir ){ return dir == ZDIR ? YDIR : ZDIR; }

  //====================================================================

  std::string
  get_tau_name( const Direction face,
                const Direction dir )
  {
    std::string tau;
    const StringNames& sName = StringNames::self();
    switch( face ){
    case XDIR: {
      switch( dir ){
      case XDIR: tau=sName.tauxx; break;
      case YDIR: tau=sName.tauxy; break;
      case ZDIR: tau=sName.tauxz; break;
      case NODIR: assert(false); break;
      }
      break;
    }
    case YDIR: {
      switch( dir ){
      case XDIR: tau=sName.tauyx; break;
      case YDIR: tau=sName.tauyy; break;
      case ZDIR: tau=sName.tauyz; break;
      case NODIR: assert(false); break;
      }
      break;
    }
    case ZDIR: {
      switch( dir ){
      case XDIR: tau=sName.tauzx; break;
      case YDIR: tau=sName.tauzy; break;
      case ZDIR: tau=sName.tauzz; break;
      case NODIR: assert(false); break;
      }
      break;
    }
    case NODIR: assert(false); break;
    }
    return tau;
  }

  //====================================================================

  template<> Direction direction<SpatialOps::XDIR >(){ return XDIR; }
  template<> Direction direction<SpatialOps::YDIR >(){ return YDIR; }
  template<> Direction direction<SpatialOps::ZDIR >(){ return ZDIR; }
  template<> Direction direction<SpatialOps::NODIR>(){ return NODIR; }

  template<> Direction direction<XDIR>(){ return XDIR; }
  template<> Direction direction<YDIR>(){ return YDIR; }
  template<> Direction direction<ZDIR>(){ return ZDIR; }

  template Direction direction<SpatialOps::XDIR>();
  template Direction direction<SpatialOps::YDIR>();
  template Direction direction<SpatialOps::ZDIR>();
  template Direction direction<SpatialOps::NODIR>();

  template Direction direction<XDIR>();
  template Direction direction<YDIR>();
  template Direction direction<ZDIR>();

} // namespace LBMS

