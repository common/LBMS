/*
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SHAPES_h
#define SHAPES_h

#include <lbms/Coordinate.h>
#include <lbms/Direction.h>
#include <lbms/Bundle_fwd.h>
#include <lbms/bcs/Geom_fwd.h>


namespace LBMS{

  /**
   * \brief Virtual base class for shapes
   */
  class Shape{
  protected:
    Shape(){}
  public:
    virtual ~Shape(){}

    /**
     * \brief determines if the coordinate is in the shape, returning true if it is, otherwise false.
     * \param c the Coordinate of interest
     */
    virtual bool is_in_shape( const Coordinate& c ) const = 0;

    /**
     * @return the bounding box that contains the shape.
     */
    virtual Box bounding_box() const = 0;

    /**
     * @param s the shape to compare to this one.
     * @return true if the two shapes are equal
     */
    virtual bool operator==(const Shape& s) const = 0;

    /**
     * @param s the shape to compare to this one.
     * @return true if the two shapes are not equal
     */
    bool operator !=(const Shape& s) const{ return !(*this==s); }

    /**
     * @param bundle
     * @return the set of indices associated with this Shape
     *
     * By convention, we assume that faces on the (-) side of a volume are
     * associated with the volume centroid. Therefore, a Shape that lives on a
     * (-) face is indexed by the centroid to its (+) side whereas a Shape that
     * lives on a (+) face is indexed by the centroid to its (+) side.
     * That means that if you have a shape on the (+) domain boundary, it will
     * have an index outside the domain.
     */
    IndexSetPtr points( const BundlePtr bundle ) const;

    virtual void print( std::ostream& os ) const = 0;
  };

  /**
   * \class Sphere
   * \brief Implements support for a Sphere
   */
  class Sphere: public Shape
  {
    const double radius_;
    const Coordinate center_;
  public:
    /**
     * @param radius the radius of the sphere
     * @param c the coordinate for the center of the sphere
     */
    Sphere( const double radius, const Coordinate c );

    bool is_in_shape( const Coordinate& c ) const;
    Box bounding_box() const;
    bool operator==(const Shape& s) const;
    void print( std::ostream& os ) const;
  };

  /**
   * \class Box
   * \brief Support for box objects
   */
  class Box: public Shape
  {
    const Coordinate lower_, upper_;
  public:
    /**
     * @param c1 Lower (minus side) coordinate for the bounding box
     * @param c2 Upper (plus side) coordinate for the bounding box
     */
    Box( const Coordinate c1,
         const Coordinate c2 );

    virtual ~Box(){}

    const Coordinate& low() const{ return lower_; }
    const Coordinate& high() const{ return upper_; }

    bool is_in_shape( const Coordinate& c ) const;
    Box bounding_box() const{ return Box(lower_,upper_); }
    bool operator==(const Shape& s) const;
    void print( std::ostream& os ) const;
  };


  /**
   * \brief Describes a 2D shape with a specific orientation
   *
   * Shape2D has an orientation defined by a unit normal.  That means that two
   * shapes that are otherwise identical are distinct if they are oriented in
   * opposite directions (e.g., x+ vs. x-).
   */
  class Shape2D : public Shape{
  protected:
    const unsigned short ix_;
    const Faces unitNormal_;  ///< can only be axis-aligned
    Shape2D( const Faces normal );
  public:
    virtual ~Shape2D(){}

    /**
     * Obtain the unit normal for this shape.
     * @return
     */
    Faces unit_normal() const{return unitNormal_;}

    /**
     * @param bundle
     * @return the set of indices associated with this Shape
     *
     * By convention, we assume that faces on the (-) side of a volume are
     * associated with the volume centroid. Therefore, a Shape that lives on a
     * (-) face is indexed by the centroid to its (+) side whereas a Shape that
     * lives on a (+) face is indexed by the centroid to its (+) side.
     * That means that if you have a shape on the (+) domain boundary, it will
     * have an index outside the domain.
     */
    IndexSetPtr points( const BundlePtr bundle ) const;
  };

  /**
   * \class Plane
   * \brief Support for describing an entire plane
   */
  class Plane : public Shape2D
  {
    const Coordinate loc_;
  public:
    Plane( const Faces normal,
           const Coordinate location );
    bool is_in_shape( const Coordinate& c ) const;
    Box bounding_box() const;
    bool operator==(const Shape& s) const;
    void print( std::ostream& os ) const;
  };

  /**
   * \class Circle
   * \brief describes a circle in an ordinal plane
   */
  class Circle : public Shape2D
  {
    const Coordinate center_; ///< center of the circle
    const double radius_;     ///< radius of the circle
  public:
    /**
     * @param normal the normal direction for this Circle
     * @param center the center of the circle
     * @param radius the radius of the circle
     */
    Circle( const Faces normal,
            const Coordinate center,
            const double radius );
    bool is_in_shape( const Coordinate& c ) const;
    Box bounding_box() const;
    bool operator==(const Shape& s) const;
    void print( std::ostream& os ) const;
  };

  /**
   * \class Rectangle
   * \brief describes a rectangle in an ordinal plane
   */
  class Rectangle : public Shape2D
  {
    const Coordinate lower_, upper_;
  public:
    /**
     * @param perpDir the plane that this Rectangle resides in
     * @param c1 the lower corner of the rectangle
     * @param c2 the upper corner of the rectangle
     * Note that c1 and c2 must both lie in the plane defined by perpDir.
     * An exception will be thrown if this is not the case.
     */
    Rectangle( const Faces normal,
               const Coordinate c1,
               const Coordinate c2 );
    bool is_in_shape( const Coordinate& c ) const;
    Box bounding_box() const;
    bool operator==(const Shape& s) const;
    void print( std::ostream& os ) const;
  };

  inline std::ostream& operator<<( std::ostream& os, const Shape& s ){ s.print(os); return os; }

}//namespace LBMS

#endif
