/*
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMS_BCGeometry_h
#define LBMS_BCGeometry_h

#include <lbms/Bundle.h>
#include <lbms/bcs/Geom_fwd.h>
#include <parser/ParseTools.h>

#include <map>

namespace LBMS{

  class PointCollection2D;

  typedef std::map<std::string, PointCollection2D> GeoMap;
  typedef boost::shared_ptr<GeoMap > GeoMapPtr;

  /**
   * @brief Parse the input file to create geometry objects.
   * @param geoParser the YAML node containing all of the geometry objects. This should be within a "Geometries" scope.
   * @param bundle the bundle
   * @return a map of descriptors to a collection of points describing the geometry.
   */
  GeoMapPtr parse_geometries( const YAML::Node& geoParser,
                              const BundlePtr bundle );

}


#endif //LBMS_BCGeometry_h
