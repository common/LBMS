/*
 * PeriodicBC.cpp
 *
 *  Created on: Jul 5, 2012
 *      Author: james
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "PeriodicBC.h"
#include <lbms/Bundle.h>
#include <fields/Fields.h>
#include <mpi/Environment.h>
#include <mpi/FieldBundleExchange.h>
#include <mpi/GhostFieldPollWorker.h>

#include <expression/ExpressionFactory.h>
#include <expression/Expression.h>

#include <spatialops/structured/FVStaggered.h>

#include <stdexcept>

using namespace SpatialOps;

namespace LBMS{

//------------------------------------------------------------------

template<typename FieldT>
PeriodicBC<FieldT>::
PeriodicBC( const Direction dir,
            const BundlePtr bundle, const Expr::Tag& fieldTag, Expr::ExpressionFactory& factory )
  : fieldtag_( fieldTag )
{
  BoundaryType bcMinus, bcPlus;

  switch( dir ){
  case XDIR:
    plus_  = XPLUS;
    minus_ = XMINUS;
    break;
  case YDIR:
    plus_  = YPLUS;
    minus_ = YMINUS;
    break;
  case ZDIR:
    plus_  = ZPLUS;
    minus_ = ZMINUS;
    break;
  case NODIR: break;
  }

  bcMinus=bundle->get_boundary_type(minus_);
  bcPlus =bundle->get_boundary_type(plus_ );

  if( (bcMinus == LBMS::DomainBoundary || bcPlus == LBMS::DomainBoundary)){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "ERROR!  Cannot apply a periodic BC to a bundle with a non-periodic boundary!" << std::endl
        << "        Problem identified when imposing a periodic BC in the " << dir << " direction" << std::endl
        << "       " << bundle->get_boundary_type(minus_) << "," << bundle->get_boundary_type(plus_) << std::endl
        << std::endl;
    throw std::invalid_argument( msg.str() );
  }
  assert( dir != NODIR );

  minus = bcMinus == LBMS::PeriodicBoundary ? LBMS::Environment::rank() : LBMS::Environment::neighbor_id(bundle, plus_);
  plus  =  bcPlus == LBMS::PeriodicBoundary ? LBMS::Environment::rank() : LBMS::Environment::neighbor_id(bundle, minus_);

  const LBMS::Direction faceDir = LBMS::direction<typename FieldT::Location::FaceDir>();
  if( faceDir != NODIR ){
    assert( bundle->npts(faceDir) > 1 );
  }
  using SpatialOps::IntVec;

  const int myID = Environment::rank();

  typedef GhostFieldSendHelper<FieldT> Sender;
  typedef GhostFieldPollWorker<FieldT> Receiver;
  if( myID == minus ){
    Sender* sender = new Sender(bundle, fieldtag_, minus_, plus);
    Expr::ExpressionBase& expr = factory.retrieve_expression( fieldTag, false );
    dynamic_cast<Expr::Expression<FieldT>&>(expr).process_after_evaluate( boost::ref(*sender), sender->is_gpu_runnable() );
    factory.get_nonblocking_poller(fieldTag)->add_new( Expr::PollWorkerPtr( sender ) );

    factory.get_poller(fieldtag_)->add_new(Expr::PollWorkerPtr(new Receiver(bundle, fieldtag_, minus_, plus)));
  }
  if( myID == plus ){
    Sender* sender = new Sender(bundle, fieldtag_, plus_, minus);
    Expr::ExpressionBase& expr = factory.retrieve_expression( fieldTag, false );
    dynamic_cast<Expr::Expression<FieldT>&>(expr).process_after_evaluate( boost::ref(*sender), sender->is_gpu_runnable() );
    factory.get_nonblocking_poller(fieldTag)->add_new( Expr::PollWorkerPtr( sender ) );

    factory.get_poller(fieldtag_)->add_new(Expr::PollWorkerPtr(new Receiver(bundle, fieldtag_, plus_, minus)));
  }
}

//--------------------------------------------------------------------

} // namespace LBMS

#include <fields/Fields.h>
#define INSTANTIATE( VolT )                                              \
  template class LBMS::PeriodicBC<VolT>;                                 \
  template class LBMS::PeriodicBC< SpatialOps::FaceTypes<VolT>::XFace>;  \
  template class LBMS::PeriodicBC< SpatialOps::FaceTypes<VolT>::YFace>;  \
  template class LBMS::PeriodicBC< SpatialOps::FaceTypes<VolT>::ZFace>;

INSTANTIATE( LBMS::XVolField )
INSTANTIATE( LBMS::YVolField )
INSTANTIATE( LBMS::ZVolField )
