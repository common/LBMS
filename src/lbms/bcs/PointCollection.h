/*
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef POINTCOLLECTION_H
#define POINTCOLLECTION_H

#include <lbms/Bundle_fwd.h>
#include <lbms/bcs/Geom_fwd.h>
#include <lbms/Coordinate.h>
#include <fields/Fields.h>

#include <spatialops/structured/IntVec.h>
#include <spatialops/structured/SpatialMask.h>



namespace LBMS{

/**
 * \class 2DPointCollection
 * \author James C. Sutherland
 * \date July 2015
 * \brief Supports operations on sets of points that represent a surface
 *
 * This is currently restricted to planar objects in a given coordinate-aligned direction.
 */
class PointCollection2D{
public:
  typedef SpatialOps::SpatialMask<LBMS::XVolField> XVolMask;       typedef boost::shared_ptr<XVolMask> XVolMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::YVolField> YVolMask;       typedef boost::shared_ptr<YVolMask> YVolMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::ZVolField> ZVolMask;       typedef boost::shared_ptr<ZVolMask> ZVolMaskPtr;

  typedef SpatialOps::SpatialMask<LBMS::XSurfXField> XSurfXMask;   typedef boost::shared_ptr<XSurfXMask> XSurfXMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::XSurfYField> XSurYXMask;   typedef boost::shared_ptr<XSurYXMask> XSurfYMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::XSurfZField> XSurZXMask;   typedef boost::shared_ptr<XSurZXMask> XSurfZMaskPtr;

  typedef SpatialOps::SpatialMask<LBMS::YSurfXField> YSurfXMask;   typedef boost::shared_ptr<YSurfXMask> YSurfXMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::YSurfYField> YSurYXMask;   typedef boost::shared_ptr<YSurYXMask> YSurfYMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::YSurfZField> YSurZXMask;   typedef boost::shared_ptr<YSurZXMask> YSurfZMaskPtr;

  typedef SpatialOps::SpatialMask<LBMS::ZSurfXField> ZSurfXMask;   typedef boost::shared_ptr<ZSurfXMask> ZSurfXMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::ZSurfYField> ZSurYXMask;   typedef boost::shared_ptr<ZSurYXMask> ZSurfYMaskPtr;
  typedef SpatialOps::SpatialMask<LBMS::ZSurfZField> ZSurZXMask;   typedef boost::shared_ptr<ZSurZXMask> ZSurfZMaskPtr;


  PointCollection2D( const Shape2D& shape, const BundlePtr bundle );

  PointCollection2D( const PointCollection2D& other );

  PointCollection2D( const Faces unitNormal );

  PointCollection2D();

  PointCollection2D& operator=( const PointCollection2D& other );

  ~PointCollection2D();

  Faces unit_normal() const{ return unitNormal_; }

  SpatialOps::BCSide side() const;

  /**
   * @param pc the PointCollection2D to merge with this one
   * @return A new PointCollection2D containing the union of this and the supplied PointCollection2D objects.
   */
  PointCollection2D create_union( const PointCollection2D& pc,
                                  const PointCollection2D* const pc2=NULL,
                                  const PointCollection2D* const pc3=NULL,
                                  const PointCollection2D* const pc4=NULL ) const;


  /**
   * \param pc the PointCollection to intersect with this one.
   * \return all elements that are in both this and in pc (AND operation)
   */
  PointCollection2D create_intersection( const PointCollection2D& pc,
                                         const PointCollection2D* const pc2=NULL,
                                         const PointCollection2D* const pc3=NULL,
                                         const PointCollection2D* const pc4=NULL ) const;

  /**
   * \return all elements in this but not in the passed in pc
   */
  PointCollection2D create_complement( const PointCollection2D& pc,
                                       const PointCollection2D* const pc2=NULL,
                                       const PointCollection2D* const pc3=NULL,
                                       const PointCollection2D* const pc4=NULL ) const;

  /**
   * @brief build the mask associated with the given bundle
   * @param bundle
   */
  template<typename FieldT>
  void create_masks( const BundlePtr bundle );

  /**
   * @return the desired mask associated with this PointCollection2D
   */
  template<typename FieldT>
  boost::shared_ptr< SpatialOps::SpatialMask<FieldT> >
  get_mask() const;

  void print( std::ostream& os ) const;
  void print_coordinates( std::ostream& os, const BundlePtr bundle ) const;

  size_t size() const{ return pts_->size(); }

private:

  template<typename FieldT>
  boost::shared_ptr<SpatialOps::SpatialMask<FieldT> >&
  select_mask();

  template<typename FieldT>
  const boost::shared_ptr<SpatialOps::SpatialMask<FieldT> >&
  select_mask() const;

  Faces unitNormal_; ///< only axis-aligned is allowed
  IndexSetPtr pts_;  ///< the collection of points

  // jcs note that we should really be only creating the volume masks and shifting
  // them when they are used, but we will do it the ugly way for now.
  XVolMaskPtr xVolMask_;
  YVolMaskPtr yVolMask_;
  ZVolMaskPtr zVolMask_;

  XSurfXMaskPtr xSXMask_;
  XSurfYMaskPtr xSYMask_;
  XSurfZMaskPtr xSZMask_;

  YSurfXMaskPtr ySXMask_;
  YSurfYMaskPtr ySYMask_;
  YSurfZMaskPtr ySZMask_;

  ZSurfXMaskPtr zSXMask_;
  ZSurfYMaskPtr zSYMask_;
  ZSurfZMaskPtr zSZMask_;
};

inline std::ostream& operator<<( std::ostream& os, const PointCollection2D& pc ){ pc.print(os); return os; }

} // namespace LBMS

#endif
