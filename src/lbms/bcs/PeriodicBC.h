#ifndef PeriodicBC_h
#define PeriodicBC_h

#include <vector>


//--- LBMS Includes ---//
#include <lbms/Bundle_fwd.h>
#include <lbms/Direction.h>

#include <expression/Tag.h>
#include <spatialops/structured/IntVec.h>

namespace LBMS{

  /**
   *  \class PeriodicBC
   *  \author James C. Sutherland
   *
   *  \brief Applies a periodic BC on an entire face of the bundle.
   *
   *  This is intended to work well with the ExprLib process_after_evaluate()
   *  mechanism by exposing an operator that accepts the field as an argument.
   *
   *  This implementation assumes that the entire domain (in the specified
   *  direction) is in the given bundle.  Notably, this will NOT work in
   *  domain-decomposed situations!
   */
  template< typename FieldT >
  class PeriodicBC
  {
    LBMS::Faces plus_, minus_;
    int plus, minus;
    const Expr::Tag fieldtag_;
    void set(FieldT& f) const;
  public:
    PeriodicBC( const LBMS::Direction,
                const LBMS::BundlePtr, const Expr::Tag&, Expr::ExpressionFactory& factory);
  private:
    PeriodicBC();
    PeriodicBC& operator=( const PeriodicBC& );
  };

} // namespace LBMS

#endif // PeriodicBC_h
