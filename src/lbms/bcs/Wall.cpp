/*
 * The MIT License
 *
 * Copyright (c) 2016 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   Wall.cpp
 *  \date   Aug 3, 2016
 *  \author derek
 */

#include "Wall.h"

#include <fields/StringNames.h>
#include <parser/ParseTools.h>
#include <operators/Operators.h>

#include <operators/OneSidedPressureDiv.h>
#include <expression/BoundaryConditionExpression.h>
#include <exprs/MaskedAssign.h>
#include <exprs/HeatFlux.h>

#include <transport/TransportEquationBase.h>

#include <pokitt/CanteraObjects.h>

namespace LBMS{

  template< typename FieldT >
  Wall<FieldT>::Wall( const YAML::Node& bcParser,
                                  const BundlePtr bundle,
                                  GraphCategories& gc,
                                  const PointCollection2D& pts,
                                  const LBMS::Options& lbmsOptions )
   : bcParser_   ( bcParser ),
     bundle_     ( bundle   ),
     pts_        ( pts      ),
     doX_        ( bundle_->npts(XDIR) > 1 ),
     doY_        ( bundle_->npts(YDIR) > 1 ),
     doZ_        ( bundle_->npts(ZDIR) > 1 ),
     sName_      ( StringNames::self()     ),
     bcType_     ( bcParser_["BCType"].template as<std::string>() ),
     bcLabel_    ( bcParser_["label"] .template as<std::string>() ),
     lbmsOptions_( lbmsOptions )
  {
    using Expr::STATE_NONE;
    using Expr::Tag; using Expr::TagList;

    Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;

    const std::string bcFace = face_name(pts_.unit_normal());

    const Tag xVelTag = create_tag( sName_.xvel       , STATE_NONE, bundle_->dir() );
    const Tag yVelTag = create_tag( sName_.yvel       , STATE_NONE, bundle_->dir() );
    const Tag zVelTag = create_tag( sName_.zvel       , STATE_NONE, bundle_->dir() );
    const Tag tempTag = create_tag( sName_.temperature, STATE_NONE, bundle_->dir() );

    const Tag xVelModTag = create_tag( sName_.xvel        + "_" + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );
    const Tag yVelModTag = create_tag( sName_.yvel        + "_" + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );
    const Tag zVelModTag = create_tag( sName_.zvel        + "_" + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );
    const Tag tempModTag = create_tag( sName_.temperature + "_" + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

    const Tag thermCondTag    = create_tag( lbmsOptions_.pokittOptions.thermCond,                          STATE_NONE, bundle_->dir() );
    const Tag thermCondModTag = create_tag( lbmsOptions_.pokittOptions.thermCond + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

    TagList diffModTags, convModTags;


    const double xVelValue = bcParser_["x_velocity" ].template as<double>( 0.0   );
    const double yVelValue = bcParser_["y_velocity" ].template as<double>( 0.0   );
    const double zVelValue = bcParser_["z_velocity" ].template as<double>( 0.0   );
    const double tempValue = bcParser_["temperature"].template as<double>( 300.0 );

    SpatialOps::BCSide side;

    switch( pts_.unit_normal() ){
      case XMINUS:
        side = SpatialOps::MINUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::XDIR>( xVelTag, xVelModTag,  xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::XDIR>( yVelTag, yVelModTag,  yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::XDIR>( zVelTag, zVelModTag,  zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::XDIR>( tempTag, tempModTag,  tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface = select_from_dir( direction<LBMS::XDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::XDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::XDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::XDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::XDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::XDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::XDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::XDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::XDIR>( diffFluxTag, diffModTags[i], 0.0, side, factory );
              set_neumann_condition<SpatialOps::XDIR>( convFluxTag, convModTags[i], 0.0, side, factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
      case YMINUS:
        side = SpatialOps::MINUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::YDIR>( xVelTag, xVelModTag,  xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::YDIR>( yVelTag, yVelModTag,  yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::YDIR>( zVelTag, zVelModTag,  zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::YDIR>( tempTag, tempModTag,  tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface = select_from_dir( direction<LBMS::YDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::YDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::YDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::YDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::YDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::YDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::ZDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::ZDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::YDIR>( diffFluxTag, diffModTags[i], 0.0, side, factory );
              set_neumann_condition<SpatialOps::YDIR>( convFluxTag, convModTags[i], 0.0, side, factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
      case ZMINUS:
        side = SpatialOps::MINUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::ZDIR>( xVelTag, xVelModTag,  xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::ZDIR>( yVelTag, yVelModTag,  yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::ZDIR>( zVelTag, zVelModTag,  zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::ZDIR>( tempTag, tempModTag,  tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface = select_from_dir( direction<LBMS::ZDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::ZDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::ZDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::ZDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::ZDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::ZDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::ZDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::ZDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::ZDIR>( diffFluxTag, diffModTags[i], 0.0, side, factory );
              set_neumann_condition<SpatialOps::ZDIR>( convFluxTag, convModTags[i], 0.0, side, factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
      case XPLUS:
        side = SpatialOps::PLUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::XDIR>( xVelTag,  xVelModTag, xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::XDIR>( yVelTag,  yVelModTag, yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::XDIR>( zVelTag,  zVelModTag, zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::XDIR>( tempTag,  tempModTag, tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface   = select_from_dir( direction<LBMS::XDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::XDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::XDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::XDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::XDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::XDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::XDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::XDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::XDIR>( diffFluxTag, diffModTags[i], 0.0, side, factory );
              set_neumann_condition<SpatialOps::XDIR>( convFluxTag, convModTags[i], 0.0, side, factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
      case YPLUS:
        side = SpatialOps::PLUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::YDIR>( xVelTag,  xVelModTag, xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::YDIR>( yVelTag,  yVelModTag, yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::YDIR>( zVelTag,  zVelModTag, zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::YDIR>( tempTag,  tempModTag, tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface = select_from_dir( direction<LBMS::YDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::YDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::YDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::YDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::YDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::YDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::YDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::YDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::YDIR>( diffFluxTag, diffModTags[i], 0.0, side, factory );
              set_neumann_condition<SpatialOps::YDIR>( convFluxTag, convModTags[i], 0.0, side, factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
      case ZPLUS:
        side = SpatialOps::PLUS_SIDE;
        if (doX_) set_ghost_cell<SpatialOps::ZDIR>( xVelTag,  xVelModTag, xVelValue, side, "DIRICHLET", factory );
        if (doY_) set_ghost_cell<SpatialOps::ZDIR>( yVelTag,  yVelModTag, yVelValue, side, "DIRICHLET", factory );
        if (doZ_) set_ghost_cell<SpatialOps::ZDIR>( zVelTag,  zVelModTag, zVelValue, side, "DIRICHLET", factory );
                  set_ghost_cell<SpatialOps::ZDIR>( tempTag,  tempModTag, tempValue, side, "DIRICHLET", factory );

        {
          const std::string fluxface = select_from_dir( direction<LBMS::ZDIR>(), sName_.xface, sName_.yface, sName_.zface );
          const std::string eDiFlux = LBMS::ZDIR == this->bundle_->dir() ?
                                      sName_.rhoE0+"_diffusiveflux"+fluxface :
                                      sName_.rhoE0+"_diffusiveflux"+fluxface+sName_.fine;

          const Tag heatFluxTag = create_tag( sName_.heat_flux + fluxface,      STATE_NONE, bundle_->dir() );
          const Tag dFluxTag    = create_tag( eDiFlux,                          STATE_NONE, bundle_->dir() );
          const Tag dFluxModTag = create_tag( eDiFlux + bcType_ + "_" + bcFace, STATE_NONE, bundle_->dir() );

          //Make sure we have valid thermal conductivity tags in the ghost cells for calculating heat flux across a wall
          set_ghost_cell <SpatialOps::ZDIR>( thermCondTag, thermCondModTag, 0.0, side, "NEUMANN", factory );
          reset_heat_flux<SpatialOps::ZDIR>( dFluxTag, dFluxModTag, heatFluxTag, fluxface, factory );

          const TagList diffFluxTags = lbmsOptions_.pokittOptions.species_flux_tags              ( direction<LBMS::ZDIR>(), bundle_->dir(), STATE_NONE );
          const TagList fineTags     = lbmsOptions_.pokittOptions.species_flux_tags_reconstructed( direction<LBMS::ZDIR>(), bundle_->dir(), STATE_NONE, sName_.fine );

          if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
            for( size_t i=0; i<lbmsOptions_.pokittOptions.nspecies-1; ++i ){
              diffModTags.push_back( create_tag( CanteraObjects::species_name(i) + fluxface + "_diffFlux_"
                                                    + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );
              convModTags.push_back( create_tag( sName_.density + CanteraObjects::species_name(i) + fluxface + "_convFlux_"
                                                   + bcType_ + bcFace,
                                                 STATE_NONE,
                                                 bundle_->dir() ) );

              const Tag diffFluxTag = LBMS::ZDIR == bundle_->dir() ? diffFluxTags[i] : fineTags[i];
              const Tag convFluxTag = ( LBMS::ZDIR == this->bundle_->dir() )
                                        ?
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() )
                                        :
                                        create_tag( sName_.density + "_" + CanteraObjects::species_name(i) + "_convectiveflux" + sName_.fine + fluxface,
                                                    STATE_NONE,
                                                    bundle_->dir() );
              set_neumann_condition<SpatialOps::ZDIR>( diffFluxTag, diffModTags[i], 0.0, side,  factory );
              set_neumann_condition<SpatialOps::ZDIR>( convFluxTag, convModTags[i], 0.0, side,  factory );
            }//end for species i
          }//end if doSpecies
        }
        break;
    }//end Switch
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  template< typename DirT   >
  void
  Wall<FieldT>::set_ghost_cell( const Expr::Tag tag,
                                const Expr::Tag modTag,
                                const double value,
                                const SpatialOps::BCSide side,
                                const std::string bcTypeString,
                                Expr::ExpressionFactory& factory )
  {
    Expr::ExpressionBuilder* builder = NULL;
    typedef typename Expr::ConstantBCOpExpression< FieldT, DirT >::Builder BCExpr;

    typedef typename BCExpr::MaskType  Mask;
    typedef typename BCExpr::MaskPtr   MaskPtr;
    builder = new BCExpr( modTag, (pts_.get_mask< typename Mask::field_type >()), LBMS::select_bctype(bcTypeString), side, value );

    factory.register_expression(builder, false, bundle_->id());
    factory.attach_modifier_expression(modTag, tag, ALL_PATCHES, false);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  template< typename DirT   >
  void
  Wall<FieldT>::set_neumann_condition( const Expr::Tag tag,
                                       const Expr::Tag modTag,
                                       const double value,
                                       const SpatialOps::BCSide side,
                                       Expr::ExpressionFactory& factory )
  {
    Expr::ExpressionBuilder* builder = NULL;

    typedef typename FieldT::Location::Bundle BundleDirT;
    typedef typename LBMS::TransportEquationBase<FieldT>::template FaceSelector<BundleDirT,DirT>::type FaceFieldT;

    typedef typename Expr::ConstantBCOpExpression< FaceFieldT, DirT >::Builder BCExpr;

    typedef typename BCExpr::MaskType  Mask;
    typedef typename BCExpr::MaskPtr   MaskPtr;
    builder = new BCExpr( modTag, (pts_.get_mask< typename Mask::field_type >()), LBMS::select_bctype("NEUMANN"), side, value );

    factory.register_expression(builder, false, bundle_->id());
    factory.attach_modifier_expression(modTag, tag, ALL_PATCHES, false);
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  template< typename DirT   >
  void
  Wall<FieldT>::reset_heat_flux( const Expr::Tag tag,
                                 const Expr::Tag modTag,
                                 const Expr::Tag heatFluxTag,
                                 const std::string fluxFace,
                                 Expr::ExpressionFactory& factory )
  {
    //We overwrite the total diffusive flux here with the heat flux
    //We have a valid temperature in the ghost cell but the pressure and stress terms
    //are invalid and NaN * 0 = garbage.
    Expr::Tag heatFluxNoSpecFluxTag;
    if( lbmsOptions_.pokittOptions.doSpeciesTransport ){
      heatFluxNoSpecFluxTag = create_tag( StringNames::self().heat_flux + fluxFace + "_noSpeciesFlux", Expr::STATE_NONE, bundle_->dir() );
      if( !factory.have_entry(heatFluxNoSpecFluxTag) ){
        const Expr::Tag tempTag      = create_tag( lbmsOptions_.pokittOptions.temperature, Expr::STATE_NONE, bundle_->dir() );
        const Expr::Tag thermCondTag = create_tag( lbmsOptions_.pokittOptions.thermCond,   Expr::STATE_NONE, bundle_->dir() );

        typedef typename FieldT::Location::Bundle BundleDirT;
        typedef typename LBMS::TransportEquationBase<FieldT>::template FaceSelector<BundleDirT,DirT>::type FaceT;
        typedef typename HeatFlux< FieldT, FaceT >::Builder HeatFlux;
        factory.register_expression( new HeatFlux( heatFluxNoSpecFluxTag, thermCondTag, tempTag ), false, bundle_->id() );
      }
    }
    else
      heatFluxNoSpecFluxTag = heatFluxTag;

    Expr::ExpressionBuilder* builder = NULL;

    typedef typename Expr::ConstantBCOpExpression< FieldT, DirT >::Builder BCExpr;
    typedef typename BCExpr::MaskType  Mask;

    typedef typename MaskedAssign< typename Mask::field_type >::Builder TotalIntEnergyDiffFlux;
    builder = new TotalIntEnergyDiffFlux( modTag, *(pts_.get_mask< typename Mask::field_type >()), heatFluxNoSpecFluxTag );

    factory.register_expression(builder, false, bundle_->id());
    factory.attach_modifier_expression(modTag, tag, ALL_PATCHES, false);
  }

}//namespace LBMS

// explicit template instantiation
#define INSTANTIATEWALL( VolT ) \
template class LBMS::Wall<VolT>;    \
INSTANTIATEWALLMETHOD( SpatialOps::XDIR, VolT )    \
INSTANTIATEWALLMETHOD( SpatialOps::YDIR, VolT )    \
INSTANTIATEWALLMETHOD( SpatialOps::ZDIR, VolT )

#define INSTANTIATEWALLMETHOD( DirT, VolT ) \
template void LBMS::Wall<VolT>::set_ghost_cell <DirT>( const Expr::Tag, const Expr::Tag, const double, const SpatialOps::BCSide, const std::string bcTypeString, Expr::ExpressionFactory& );  \
template void LBMS::Wall<VolT>::reset_heat_flux<DirT>( const Expr::Tag, const Expr::Tag, const Expr::Tag, const std::string, Expr::ExpressionFactory& ); \

INSTANTIATEWALL( LBMS::XVolField )
INSTANTIATEWALL( LBMS::YVolField )
INSTANTIATEWALL( LBMS::ZVolField )
