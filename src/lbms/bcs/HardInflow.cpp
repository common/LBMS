/*
 * The MIT License
 *
 * Copyright (c) 2016 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   HardInflow.cpp
 *  \date   Aug 3, 2016
 *  \author derek
 */

#include "HardInflow.h"

#include <exprs/GhostRhoPhi.h>
#include <exprs/GhostAssign.h>

#include <fields/StringNames.h>
#include <parser/ParseTools.h>
#include <mpi/Environment.h>

#include <pokitt/thermo/InternalEnergy.h>
#include <pokitt/MixtureMolWeight.h>
#include <pokitt/thermo/Density.h>
#include <pokitt/SpeciesN.h>

namespace LBMS{

  template< typename FieldT >
  HardInflow<FieldT>::HardInflow( const YAML::Node& bcParser,
                                  const BundlePtr bundle,
                                  GraphCategories& gc,
                                  const PointCollection2D& pts,
                                  const LBMS::Options& lbmsOptions )
   : bcParser_( bcParser ),
     bundle_  ( bundle   ),
     pts_     ( pts      ),
     doX_     ( bundle_->npts(XDIR) > 1 ),
     doY_     ( bundle_->npts(YDIR) > 1 ),
     doZ_     ( bundle_->npts(ZDIR) > 1 ),
     sName_   ( StringNames::self()     ),
     bcType_  ( bcParser_["BCType"].template as<std::string>() ),
     bcLabel_ ( bcParser_["label"] .template as<std::string>() )
  {
    using Expr::STATE_NONE;
    using Expr::Tag;

    SpatialOps::BCSide side;
    LBMS::Direction dir;
    switch( pts_.unit_normal() ){
      case XMINUS:
        dir = LBMS::XDIR;
        side = SpatialOps::MINUS_SIDE;
        break;
      case YMINUS:
        dir = LBMS::YDIR;
        side = SpatialOps::MINUS_SIDE;
        break;
      case ZMINUS:
        dir = LBMS::ZDIR;
        side = SpatialOps::MINUS_SIDE;
        break;
      case XPLUS:
        dir = LBMS::XDIR;
        side = SpatialOps::PLUS_SIDE;
        break;
      case YPLUS:
        dir = LBMS::YDIR;
        side = SpatialOps::PLUS_SIDE;
        break;
      case ZPLUS:
        dir = LBMS::ZDIR;
        side = SpatialOps::PLUS_SIDE;
        break;
    }

    Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;
    Expr::ExpressionBuilder* builder = NULL;

    const std::string modifierString =   "_"      + bcLabel_
                                       + "_bc_"   + bcType_
                                       + "_Face_" + face_name(pts_.unit_normal());

    typedef typename pokitt::Density            <FieldT>::Builder DensityBuilder;
    typedef typename pokitt::TotalInternalEnergy<FieldT>::Builder EnergyBuilder;
    typedef typename pokitt::MixtureMolWeight   <FieldT>::Builder MMWBuilder;

    const Tag rhoBCTag = create_tag( sName_.density + modifierString, STATE_NONE, bundle_->dir() );
    const Tag e0Tag    = create_tag( sName_.rhoE0   + modifierString, STATE_NONE, bundle_->dir() );

    //TODO Add in some side type face strings
    Tag xVelTag = Tag();
    Tag yVelTag = Tag();
    Tag zVelTag = Tag();

    //TODO jet should be replaced with something else but the face,
    //     type, side, label is ugly to have in an input file
    if( doX_ ) xVelTag    = create_tag( sName_.xvel        + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
    if( doY_ ) yVelTag    = create_tag( sName_.yvel        + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
    if( doZ_ ) zVelTag    = create_tag( sName_.zvel        + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
    const Tag pressure    = create_tag( sName_.pressure    + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
    const Tag temperature = create_tag( sName_.temperature + "_" + bcLabel_, STATE_NONE, bundle_->dir() );

    // set up species transport equations
    Expr::TagList specTags;
    for( size_t i=0; i<lbmsOptions.pokittOptions.nspecies; ++i ){
      Tag specGhostTag  = create_tag( CanteraObjects::species_name(i) + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
      specTags.push_back( specGhostTag );
    }

    Expr::TagList velTags;
    velTags.push_back( xVelTag ); velTags.push_back( yVelTag ); velTags.push_back( zVelTag );

    const Tag mmwTag = create_tag( sName_.mixmw + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
    builder = new MMWBuilder( mmwTag, specTags, pokitt::MASS );
    factory.register_expression( builder, false, bundle_->id() );

    builder = new DensityBuilder( rhoBCTag, temperature, pressure, mmwTag );
    factory.register_expression( builder, false, bundle_->id() );

    builder = new EnergyBuilder( e0Tag, temperature, specTags, velTags );
    factory.register_expression( builder, false, bundle_->id() );

    //TODO dac We should pull these from a tagset of the transported variables
    //         rather than hard code
    const Tag densTag  = create_tag( sName_.density, Expr::STATE_N, bundle_->dir() );
    const Tag xMomTag  = create_tag( sName_.xmom,    Expr::STATE_N, bundle_->dir() );
    const Tag yMomTag  = create_tag( sName_.ymom,    Expr::STATE_N, bundle_->dir() );
    const Tag zMomTag  = create_tag( sName_.zmom,    Expr::STATE_N, bundle_->dir() );
    const Tag rhoE0Tag = create_tag( sName_.rhoE0,   Expr::STATE_N, bundle_->dir() );

    const Expr::Tag densModTag( densTag.name() + modifierString, Expr::STATE_NONE );

    typedef typename LBMS::GhostAssign<FieldT>::Builder GhostBuilder;
    builder = new GhostBuilder( densModTag, *(pts_.get_mask< FieldT >()), dir, side, rhoBCTag );

    factory.register_expression( builder, false, bundle_->id() );
    factory.attach_modifier_expression( densModTag, densTag, ALL_PATCHES, false );

    if( doX_ ) set_ghost_cells( rhoBCTag, xVelTag, xMomTag,  dir, side, factory );
    if( doY_ ) set_ghost_cells( rhoBCTag, yVelTag, yMomTag,  dir, side, factory );
    if( doZ_ ) set_ghost_cells( rhoBCTag, zVelTag, zMomTag,  dir, side, factory );
               set_ghost_cells( rhoBCTag, e0Tag,   rhoE0Tag, dir, side, factory );

    if( lbmsOptions.pokittOptions.doSpeciesTransport ){
      for( size_t i=0; i<lbmsOptions.pokittOptions.nspecies-1; ++i ){
        std::string rhoYi = sName_.density + "_" + CanteraObjects::species_name(i);
        const Tag specBCTag  = create_tag( CanteraObjects::species_name(i) + "_" + bcLabel_, STATE_NONE, bundle_->dir() );

        //Allow zero initialization for fields that are not specified
        if( !factory.have_entry(specBCTag) ){
          proc0cout << "initialization entry does not exist for " << specBCTag << " adding a zero constant value expression.\n";
          factory.register_expression( new typename Expr::ConstantExpr<FieldT>::Builder( specBCTag, 0.0 ), false, bundle->id() );
        }

        const Tag speciesTag = create_tag( rhoYi, Expr::STATE_N, bundle_->dir() );
        set_ghost_cells( rhoBCTag, specBCTag, speciesTag, dir, side, factory );
      }//end for species i

      //Set up nth species for hard inflow cases
      const int nghost = 1;
      typedef typename pokitt::SpeciesN<FieldT>::Builder SpecN;
      const Tag specNBCTag = create_tag( CanteraObjects::species_name(lbmsOptions.pokittOptions.nspecies-1) + "_" + bcLabel_, STATE_NONE, bundle_->dir() );
      factory.register_expression( new SpecN( specNBCTag, specTags, pokitt::ERRORSPECN, nghost ), false, bundle->id() );

      const Tag specNTag = create_tag( sName_.density + "_" + CanteraObjects::species_name(lbmsOptions.pokittOptions.nspecies-1), STATE_NONE, bundle_->dir() );
      set_ghost_cells( rhoBCTag, specNBCTag, specNTag, dir, side, factory );
    }//end if doSpecies
  }

  //--------------------------------------------------------------------

  template<typename FieldT>
  void
  HardInflow<FieldT>::set_ghost_cells( const Expr::Tag rhoBCTag,
                                       const Expr::Tag phiBCTag,
                                       const Expr::Tag rhoPhiTag,
                                       const LBMS::Direction dir,
                                       const SpatialOps::BCSide side,
                                       Expr::ExpressionFactory& factory )
  {
    Expr::ExpressionBuilder* builder = NULL;

    const Expr::Tag rhoPhiModTag( bcLabel_
                                  + "_" + rhoPhiTag.name()
                                  + "_bc_"+ bcType_
                                  + "_Face_"+face_name(pts_.unit_normal()),
                                  Expr::STATE_NONE );

    typedef typename LBMS::GhostRhoPhi<FieldT>::Builder BCSetter;
    builder = new BCSetter( rhoPhiModTag,
                            *(pts_.get_mask< FieldT >()),
                            dir,
                            side,
                            rhoBCTag,
                            phiBCTag );

    factory.register_expression( builder, false, bundle_->id() );
    factory.attach_modifier_expression( rhoPhiModTag, rhoPhiTag, ALL_PATCHES, false );
  }

}//namespace LBMS

#include <fields/Fields.h>
template class LBMS::HardInflow<LBMS::XVolField>;
template class LBMS::HardInflow<LBMS::YVolField>;
template class LBMS::HardInflow<LBMS::ZVolField>;
