#include <lbms/bcs/BCGeometry.h>
#include <lbms/bcs/PointCollection.h>
#include <lbms/bcs/Shapes.h>

#include <yaml-cpp/yaml.h>

#include <boost/foreach.hpp>


namespace LBMS{

//-------------------------------------------------------------------

void check_geom_exists( const std::string& entry, const GeoMap& geoMap, const int line )
{
  if( geoMap.find(entry) == geoMap.end() ){
    std::ostringstream msg;
    msg << "\n\n" << __FILE__ << " : " << line << "\n"
        << "Requested geometric entity '" << entry << "' was not found.  Perhaps it was not created yet?\n\n";
    throw std::invalid_argument( msg.str() );
  }
}

//-------------------------------------------------------------------

void gather_point_collections( const YAML::Node& geomSpec,
                               const GeoMap& geoMap,
                               PointCollection2D& pc1,
                               PointCollection2D& pc2,
                               const PointCollection2D*& pc3,
                               const PointCollection2D*& pc4,
                               const PointCollection2D*& pc5,
                               std::string& label )
{
  const std::string l1 = geomSpec["shape1"].as<std::string>();
  const std::string l2 = geomSpec["shape2"].as<std::string>();
  check_geom_exists( l1, geoMap, __LINE__ );
  check_geom_exists( l2, geoMap, __LINE__ );
  pc1 = geoMap.find(l1)->second;
  pc2 = geoMap.find(l2)->second;
  // optional entries
  pc3=NULL; pc4=NULL; pc5=NULL;
  if( geomSpec["shape3"] ){
    const std::string l3 = geomSpec["shape3"].as<std::string>();
    check_geom_exists( l3, geoMap, __LINE__ );
    pc3 = &geoMap.find(l3)->second;
  }
  if( geomSpec["shape4"] ){
    const std::string l4 = geomSpec["shape4"].as<std::string>();
    check_geom_exists( l4, geoMap, __LINE__ );
    pc4 = &geoMap.find(l4)->second;
  }
  if( geomSpec["shape5"] ){
    const std::string l5 = geomSpec["shape5"].as<std::string>();
    check_geom_exists( l5, geoMap, __LINE__ );
    pc5 = &geoMap.find(l5)->second;
  }
  label = geomSpec["label"].as<std::string>();
}

//-------------------------------------------------------------------

GeoMapPtr
parse_geometries( const YAML::Node& geoParser,
                  const BundlePtr bundle )
{
  GeoMapPtr result( new GeoMap() );
  GeoMap& geoMap = *result;

  for( YAML::Node::const_iterator igeom=geoParser.begin(); igeom!=geoParser.end(); ++igeom ){
    const YAML::Node& geomSpec = *igeom;
    const std::string geometry = geomSpec["Shape"].as<std::string>();

    if( geometry == "Circle"){
      const Circle circle( geomSpec["Face"  ].as<LBMS::Faces>(),
                           geomSpec["Center"].as<LBMS::Coordinate>(),
                           geomSpec["Radius"].as<double>() );

      const PointCollection2D pts( circle, bundle );
      const std::string label = geomSpec["label"].as<std::string>();
      geoMap[label] = pts;
    }
    else if( geometry == "Rectangle" ){
      const Rectangle rect( geomSpec["Face"   ].as<LBMS::Faces>(),
                            geomSpec["Corner1"].as<Coordinate>(),
                            geomSpec["Corner2"].as<Coordinate>() );

      const PointCollection2D pts( rect, bundle );
      const std::string label = geomSpec["label"].as<std::string>();
      geoMap[label ] = pts;
    }
    else if( geometry == "Plane" ){
      const Plane plane( geomSpec["Face"  ].as<LBMS::Faces>(),
                         geomSpec["Center"].as<LBMS::Coordinate>() );
      const PointCollection2D pts( plane, bundle );
      const std::string label = geomSpec["label"].as<std::string>();
      geoMap[label] = pts;
    }
    else if(geometry == "Union"){
      PointCollection2D s1, s2;
      const PointCollection2D *s3, *s4, *s5;
      std::string label;
      gather_point_collections( geomSpec, geoMap, s1, s2, s3, s4, s5, label );
      try{
        const PointCollection2D snew = s1.create_union(s2,s3,s4,s5);
        const std::string label = geomSpec["label"].as<std::string>();
        geoMap[label] = snew;
      }
      catch( std::exception& e ){
        std::ostringstream msg;
        msg << "ERROR creating union labeled '" << label << "'\n"
            << e.what() << std::endl;
        throw std::runtime_error( msg.str() );
      }
    }
    else if( geometry == "Complement" ){
      PointCollection2D s1, s2;
      const PointCollection2D *s3, *s4, *s5;
      std::string label;
      gather_point_collections( geomSpec, geoMap, s1, s2, s3, s4, s5, label );
      try{
        const PointCollection2D snew = s1.create_complement(s2,s3,s4,s5);
        geoMap[label] = snew;
      }
      catch( std::exception& e ){
        std::ostringstream msg;
        msg << "ERROR creating complement labeled '" << label << "'\n"
            << e.what() << std::endl;
        throw std::runtime_error( msg.str() );
      }
    }
    else if( geometry=="Intersection" ){
      PointCollection2D s1, s2;
      const PointCollection2D *s3, *s4, *s5;
      std::string label;
      gather_point_collections( geomSpec, geoMap, s1, s2, s3, s4, s5, label );
      try{
        const PointCollection2D snew = s1.create_intersection(s2,s3,s4,s5);
        const std::string label = geomSpec["label"].as<std::string>();
        geoMap[label] = snew;
      }
      catch( std::exception& e ){
        std::ostringstream msg;
        msg << "ERROR creating intersection labeled '" << label << "'\n"
            << e.what() << std::endl;
        throw std::runtime_error( msg.str() );
      }
    }
    else if( geometry=="XUnion" ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << "\nXUnion not yet supported\n";
      throw std::runtime_error( msg.str() );
    }
    else{
      std::ostringstream msg;
      msg << "\n" << __FILE__<< ":"<<__LINE__
          << "\nError Unsupported Geometry '" << geometry << "'\n\n";
      throw std::runtime_error(msg.str() );

    }
  } // loop
  return result;
}

//-------------------------------------------------------------------

} // namespace LBMS
