/*
 * The MIT License
 *
 * Copyright (c) 2016 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   HardInflow.h
 *  \date   Aug 3, 2016
 *  \author derek
 */

#ifndef HARDINFLOW_H_
#define HARDINFLOW_H_

#include <nscbc/NSCBCToolsAndDefs.h>

#include <lbms/GraphHelperTools.h>
#include <lbms/bcs/PointCollection.h>
#include <lbms/Bundle.h>
#include <lbms/Options.h>
#include <fields/StringNames.h>

#include <expression/Tag.h>
#include <yaml-cpp/yaml.h>

namespace LBMS{

  /**
   *  \class HardInflow
   *  \brief Sets up conserved quantities in the ghost cells of a hard inflow
   */
  template< typename FieldT >
  class HardInflow
  {
  public:

    /**
     *  \brief Set up hard inflow conserved quantities
     *  \param bcParser The YAML node containing information on the hard inflow boundary
     *  \param bundle The current bundle that the hard inflow is being set up on
     *  \param gc GraphGategories for setting up expressions with ExprLib
     *  \param pts PointCollection2D set of points that will be used to create masks
     *             for setting ghost cells.  Note that this will actually contain interior
     *             cells.  GhostAssign Expressions will shift the mask into the necessary
     *             ghost cells.
     */
    HardInflow( const YAML::Node& bcParser,
                const BundlePtr bundle,
                GraphCategories& gc,
                const PointCollection2D& pts,
                const LBMS::Options& lbmsOptions );

    private:

    const YAML::Node& bcParser_;
    const PointCollection2D& pts_;
    const BundlePtr bundle_;
    const std::string bcType_;
    const std::string bcLabel_;
    const bool doX_, doY_, doZ_;
    const StringNames& sName_;

    void set_ghost_cells( const Expr::Tag rhoBCTag,
                          const Expr::Tag phiBCTag,
                          const Expr::Tag rhoPhiTag,
                          const LBMS::Direction dir,
                          const SpatialOps::BCSide side,
                          Expr::ExpressionFactory& factory );

  };
}//lbms namespace





#endif /* HARDINFLOW_H_ */
