#include "PointCollection.h"
#include <lbms/bcs/Shapes.h>
#include <lbms/Bundle.h>

#include <boost/foreach.hpp>

#include <algorithm>

using SpatialOps::IntVec;

namespace LBMS{

  void check_compatibility( const PointCollection2D& pc1,
                            const PointCollection2D& pc2,
                            const int line )
  {
    if( pc1.unit_normal() != pc2.unit_normal() ){
      std::ostringstream msg;
      msg << "\n" << __FILE__ << " : " << line << "\n"
          << "PointCollection2D arguments are incompatible.  They must have the same unit normal\n"
          << face_name(pc1.unit_normal()) << " <=> "
          << face_name(pc2.unit_normal()) << "\n\n";
      throw std::invalid_argument( msg.str() );
    }
  }

  //-----------------------------------------------------------------

  PointCollection2D::PointCollection2D( const Shape2D& shape, const BundlePtr bundle )
  : unitNormal_( shape.unit_normal() ),
    pts_( shape.points(bundle) )
  {}

  //-----------------------------------------------------------------

  PointCollection2D::PointCollection2D( const Faces unitNormal )
  : unitNormal_( unitNormal ),
    pts_( new IndexSet )
  {}

  //-----------------------------------------------------------------

  PointCollection2D::PointCollection2D( const PointCollection2D& other )
  : unitNormal_( other.unitNormal_ ),
    pts_       ( other.pts_      ),
    xVolMask_  ( other.xVolMask_ ),
    yVolMask_  ( other.yVolMask_ ),
    zVolMask_  ( other.zVolMask_ ),
    xSXMask_   ( other.xSXMask_  ),
    xSYMask_   ( other.xSYMask_  ),
    xSZMask_   ( other.xSZMask_  ),
    ySXMask_   ( other.ySXMask_  ),
    ySYMask_   ( other.ySYMask_  ),
    ySZMask_   ( other.ySZMask_  ),
    zSXMask_   ( other.zSXMask_  ),
    zSYMask_   ( other.zSYMask_  ),
    zSZMask_   ( other.zSZMask_  )
  {}

  //-----------------------------------------------------------------

  PointCollection2D::PointCollection2D()
  : unitNormal_( XMINUS ),
    pts_( new IndexSet )
  {}

  //-----------------------------------------------------------------

  PointCollection2D& PointCollection2D::operator=( const PointCollection2D& other )
  {
    unitNormal_ = other.unitNormal_;
    pts_        = other.pts_;
    xVolMask_   = other.xVolMask_;
    yVolMask_   = other.yVolMask_;
    zVolMask_   = other.zVolMask_;
    xSXMask_    = other.xSXMask_ ;
    xSYMask_    = other.xSYMask_ ;
    xSZMask_    = other.xSZMask_ ;
    ySXMask_    = other.ySXMask_ ;
    ySYMask_    = other.ySYMask_ ;
    ySZMask_    = other.ySZMask_ ;
    zSXMask_    = other.zSXMask_ ;
    zSYMask_    = other.zSYMask_ ;
    zSZMask_    = other.zSZMask_ ;
    return *this;
  }

  //-----------------------------------------------------------------

  PointCollection2D::~PointCollection2D()
  {}

  //-----------------------------------------------------------------

  PointCollection2D
  PointCollection2D::create_union( const PointCollection2D& pc,
                                   const PointCollection2D* const pc2,
                                   const PointCollection2D* const pc3,
                                   const PointCollection2D* const pc4 ) const
  {
    check_compatibility( *this, pc, __LINE__ );
    PointCollection2D pcNew( *this );
    pcNew.pts_->insert( pc.pts_->begin(), pc.pts_->end() );
    if( pc2 ) pcNew.pts_->insert( pc2->pts_->begin(), pc2->pts_->end() );
    if( pc3 ) pcNew.pts_->insert( pc3->pts_->begin(), pc3->pts_->end() );
    if( pc4 ) pcNew.pts_->insert( pc4->pts_->begin(), pc4->pts_->end() );
    return pcNew;
  }

  //-----------------------------------------------------------------

  PointCollection2D
  PointCollection2D::create_intersection( const PointCollection2D& pc,
                                          const PointCollection2D* const pc2,
                                          const PointCollection2D* const pc3,
                                          const PointCollection2D* const pc4 ) const
  {
    check_compatibility( *this, pc, __LINE__ );
    const IndexSet& vec1 = *(this->pts_);
    const IndexSet& vec2 = *(pc.pts_);

    PointCollection2D pcNew( unitNormal_ );
    IndexSet& vec = *(pcNew.pts_);

    std::set_intersection( vec1.begin(), vec1.end(),
                           vec2.begin(), vec2.end(),
                           std::inserter(vec, vec.begin()), std::less<IntVec>() );

    if( pc2 ){
      check_compatibility( *this, *pc2, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec2 = *(pc2->pts_);
      std::set_intersection( vecOld.begin(), vecOld.end(),
                             vec2.begin(), vec2.end(),
                             std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }
    if( pc3 ){
      check_compatibility( *this, *pc3, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec3 = *(pc3->pts_);
      std::set_intersection( vecOld.begin(), vecOld.end(),
                             vec3.begin(), vec3.end(),
                             std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }
    if( pc4 ){
      check_compatibility( *this, *pc4, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec4 = *(pc4->pts_);
      std::set_intersection( vecOld.begin(), vecOld.end(),
                             vec4.begin(), vec4.end(),
                             std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }

    return pcNew;
  }

  //-----------------------------------------------------------------

  PointCollection2D
  PointCollection2D::create_complement( const PointCollection2D& pc,
                                        const PointCollection2D* const pc2,
                                        const PointCollection2D* const pc3,
                                        const PointCollection2D* const pc4 ) const
  {
    PointCollection2D pcNew( unitNormal_ );
    IndexSet& vec = *pcNew.pts_;
    const IndexSet& vec1 = *(this->pts_);

    {
      check_compatibility( *this, pc, __LINE__ );
      const IndexSet& vec2 = *(pc.pts_);
      std::set_difference( vec1.begin(), vec1.end(),
                           vec2.begin(), vec2.end(),
                           std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }

    if( pc2 ){
      check_compatibility( *this, *pc2, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec2 = *(pc2->pts_);
      std::set_difference( vecOld.begin(), vecOld.end(),
                           vec2.begin(), vec2.end(),
                           std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }
    if( pc3 ){
      check_compatibility( *this, *pc3, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec2 = *(pc3->pts_);
      std::set_difference( vecOld.begin(), vecOld.end(),
                           vec2.begin(), vec2.end(),
                           std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }
    if( pc4 ){
      check_compatibility( *this, *pc4, __LINE__ );
      IndexSet vecOld( vec );
      vec.clear();
      const IndexSet& vec2 = *(pc4->pts_);
      std::set_difference( vecOld.begin(), vecOld.end(),
                           vec2.begin(), vec2.end(),
                           std::inserter(vec, vec.begin()), std::less<IntVec>() );
    }

    return pcNew;
  }

  //-----------------------------------------------------------------

  SpatialOps::BCSide PointCollection2D::side() const{
    return ( unitNormal_ == XMINUS || unitNormal_ == YMINUS || unitNormal_ == ZMINUS )
        ? SpatialOps::MINUS_SIDE
        : SpatialOps::PLUS_SIDE;
  }

  //-----------------------------------------------------------------

  template<> PointCollection2D::XVolMaskPtr&   PointCollection2D::select_mask<XVolField  >(){ return xVolMask_; }
  template<> PointCollection2D::YVolMaskPtr&   PointCollection2D::select_mask<YVolField  >(){ return yVolMask_; }
  template<> PointCollection2D::ZVolMaskPtr&   PointCollection2D::select_mask<ZVolField  >(){ return zVolMask_; }

  template<> PointCollection2D::XSurfXMaskPtr& PointCollection2D::select_mask<XSurfXField>(){ return xSXMask_;  }
  template<> PointCollection2D::XSurfYMaskPtr& PointCollection2D::select_mask<XSurfYField>(){ return xSYMask_;  }
  template<> PointCollection2D::XSurfZMaskPtr& PointCollection2D::select_mask<XSurfZField>(){ return xSZMask_;  }

  template<> PointCollection2D::YSurfXMaskPtr& PointCollection2D::select_mask<YSurfXField>(){ return ySXMask_;  }
  template<> PointCollection2D::YSurfYMaskPtr& PointCollection2D::select_mask<YSurfYField>(){ return ySYMask_;  }
  template<> PointCollection2D::YSurfZMaskPtr& PointCollection2D::select_mask<YSurfZField>(){ return ySZMask_;  }

  template<> PointCollection2D::ZSurfXMaskPtr& PointCollection2D::select_mask<ZSurfXField>(){ return zSXMask_;  }
  template<> PointCollection2D::ZSurfYMaskPtr& PointCollection2D::select_mask<ZSurfYField>(){ return zSYMask_;  }
  template<> PointCollection2D::ZSurfZMaskPtr& PointCollection2D::select_mask<ZSurfZField>(){ return zSZMask_;  }

  template<> const PointCollection2D::XVolMaskPtr&   PointCollection2D::select_mask<XVolField  >() const{ return xVolMask_; }
  template<> const PointCollection2D::YVolMaskPtr&   PointCollection2D::select_mask<YVolField  >() const{ return yVolMask_; }
  template<> const PointCollection2D::ZVolMaskPtr&   PointCollection2D::select_mask<ZVolField  >() const{ return zVolMask_; }

  template<> const PointCollection2D::XSurfXMaskPtr& PointCollection2D::select_mask<XSurfXField>() const{ return xSXMask_;  }
  template<> const PointCollection2D::XSurfYMaskPtr& PointCollection2D::select_mask<XSurfYField>() const{ return xSYMask_;  }
  template<> const PointCollection2D::XSurfZMaskPtr& PointCollection2D::select_mask<XSurfZField>() const{ return xSZMask_;  }

  template<> const PointCollection2D::YSurfXMaskPtr& PointCollection2D::select_mask<YSurfXField>() const{ return ySXMask_;  }
  template<> const PointCollection2D::YSurfYMaskPtr& PointCollection2D::select_mask<YSurfYField>() const{ return ySYMask_;  }
  template<> const PointCollection2D::YSurfZMaskPtr& PointCollection2D::select_mask<YSurfZField>() const{ return ySZMask_;  }

  template<> const PointCollection2D::ZSurfXMaskPtr& PointCollection2D::select_mask<ZSurfXField>() const{ return zSXMask_;  }
  template<> const PointCollection2D::ZSurfYMaskPtr& PointCollection2D::select_mask<ZSurfYField>() const{ return zSYMask_;  }
  template<> const PointCollection2D::ZSurfZMaskPtr& PointCollection2D::select_mask<ZSurfZField>() const{ return zSZMask_;  }

  template< typename FieldT >
  void create_mask( const BundlePtr bundle,
                    const Faces face,
                    const IndexSet& pts,
                    boost::shared_ptr< SpatialOps::SpatialMask<FieldT> >& maskPtr )
  {
    // GhostData is currently hard set at 1 in ExprLib (dac: but only for directions that exist).
    const int ngx = bundle->glob_npts(LBMS::XDIR) > 1 ? 1 : 0;
    const int ngy = bundle->glob_npts(LBMS::YDIR) > 1 ? 1 : 0;
    const int ngz = bundle->glob_npts(LBMS::ZDIR) > 1 ? 1 : 0;

    IntVec shift(0,0,0);
    switch( face ){
      case XMINUS: case YMINUS: case ZMINUS: break;
      case XPLUS: if( FieldT::Location::FaceDir::value == SpatialOps::XDIR::value ) shift[0]++; break;
      case YPLUS: if( FieldT::Location::FaceDir::value == SpatialOps::YDIR::value ) shift[1]++; break;
      case ZPLUS: if( FieldT::Location::FaceDir::value == SpatialOps::ZDIR::value ) shift[2]++; break;
    }
    const SpatialOps::GhostData ghost(ngx,ngx,ngy,ngy,ngz,ngz);

    const SpatialOps::BoundaryCellInfo bcInfo = SpatialOps::BoundaryCellInfo::build<FieldT>( bundle->get_boundary_type(XMINUS) == DomainBoundary,
                                                                                             bundle->get_boundary_type(XPLUS ) == DomainBoundary,
                                                                                             bundle->get_boundary_type(YMINUS) == DomainBoundary,
                                                                                             bundle->get_boundary_type(YPLUS ) == DomainBoundary,
                                                                                             bundle->get_boundary_type(ZMINUS) == DomainBoundary,
                                                                                             bundle->get_boundary_type(ZPLUS ) == DomainBoundary);

    const SpatialOps::MemoryWindow mw = get_window_with_ghost( bundle->npts(), ghost, bcInfo );

    // the direction of the bundle orientation. This is the direction we will be applying BCs.
    const Direction dir = bundle->dir();

    // pack a vector to create the mask from
    std::vector<IntVec> vec( pts.size() );
    size_t i=0;
    BOOST_FOREACH( const IntVec& v, pts ){
      vec[i++] = IntVec( (bundle->glob_npts(LBMS::XDIR) > 1 ? v[0] : 0),
                         (bundle->glob_npts(LBMS::YDIR) > 1 ? v[1] : 0),
                         (bundle->glob_npts(LBMS::ZDIR) > 1 ? v[2] : 0) ) + shift;
    }
    maskPtr.reset( new SpatialOps::SpatialMask<FieldT>( mw, bcInfo, ghost, vec ) );
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void PointCollection2D::create_masks( const BundlePtr bundle )
  {
    typedef typename SpatialOps::FaceTypes<FieldT>::XFace  XFace;
    typedef typename SpatialOps::FaceTypes<FieldT>::YFace  YFace;
    typedef typename SpatialOps::FaceTypes<FieldT>::ZFace  ZFace;

    create_mask<FieldT>( bundle, unitNormal_, *pts_, select_mask<FieldT>() );
    create_mask<XFace >( bundle, unitNormal_, *pts_, select_mask<XFace >() );
    create_mask<YFace >( bundle, unitNormal_, *pts_, select_mask<YFace >() );
    create_mask<ZFace >( bundle, unitNormal_, *pts_, select_mask<ZFace >() );
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  boost::shared_ptr< SpatialOps::SpatialMask<FieldT> >
  PointCollection2D::get_mask() const
  {
    typedef SpatialOps::SpatialMask<FieldT> Mask;
    typedef boost::shared_ptr<Mask> MaskPtr;
    const MaskPtr& maskPtr = select_mask<FieldT>();

    // ensure that this mask has been set previously
    if( !maskPtr.get() ){
      std::ostringstream msg;
      msg << "\n" << __FILE__ << " : " << __LINE__
          << "\nYou must call create_masks() prior to calling get_mask()\n\n";
      throw std::runtime_error( msg.str() );
    }

    return maskPtr;
  }

  //-----------------------------------------------------------------

  void PointCollection2D::print( std::ostream& os ) const
  {
    BOOST_FOREACH( const IntVec& v, *pts_ ){
      os << v << std::endl;
    }
  }

  //-----------------------------------------------------------------

  void PointCollection2D::print_coordinates( std::ostream& os, const BundlePtr bundle ) const
  {
    BOOST_FOREACH( const IntVec& v, *pts_ ){
      os << bundle->cell_coord(v) << std::endl;
    }
  }

  //-----------------------------------------------------------------


  //=================================================================

  void get_points_on_plane( IntVec extent,
                            IntVec offset,
                            const Faces faceDir,
                            const int loc,
                            const int numPlanes,
                            IndexSet& points )
  {
    switch( faceDir ){
      case XMINUS:
        offset[0] += loc;
        extent[0] = numPlanes;
        break;
      case XPLUS:
        offset[0] += std::max(0,extent[0]-loc);
        extent[0] = numPlanes;
        break;
      case YMINUS:
        offset[1] += loc;
        extent[1] = numPlanes;
        break;
      case YPLUS:
        offset[1] += std::max(0,extent[1]-loc);
        extent[1] = numPlanes;
        break;
      case ZMINUS:
        offset[2] += loc;
        extent[2] = numPlanes;
        break;
      case ZPLUS:
        offset[2] += std::max(0,extent[2]-loc);
        extent[2] = numPlanes;
        break;
    }

    for( int i=offset[0]; i<offset[0]+extent[0]; i++ ){
      for( int j=offset[1]; j<offset[1]+extent[1]; j++ ){
        for( int k=offset[2]; k<offset[2]+extent[2]; k++ ){
          points.insert( IntVec(i,j, k) );
        }
      }
    }
  }

  //-----------------------------------------------------------------

//===================================================================
// Explicit template instantiation
#include <fields/Fields.h>
using SpatialOps::FaceTypes;
using SpatialOps::SpatialMask;
using boost::shared_ptr;

#define INSTANTIATE( VolT ) \
    template void PointCollection2D::create_masks<VolT>( const BundlePtr ); \
    template shared_ptr< SpatialMask<          VolT        > > PointCollection2D::get_mask<          VolT        >() const;  \
    template shared_ptr< SpatialMask<FaceTypes<VolT>::XFace> > PointCollection2D::get_mask<FaceTypes<VolT>::XFace>() const;  \
    template shared_ptr< SpatialMask<FaceTypes<VolT>::YFace> > PointCollection2D::get_mask<FaceTypes<VolT>::YFace>() const;  \
    template shared_ptr< SpatialMask<FaceTypes<VolT>::ZFace> > PointCollection2D::get_mask<FaceTypes<VolT>::ZFace>() const;

  INSTANTIATE( LBMS::XVolField )
  INSTANTIATE( LBMS::YVolField )
  INSTANTIATE( LBMS::ZVolField )

//===================================================================

} // namespace LBMS
