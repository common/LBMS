#include "Shapes.h"
#include <lbms/Bundle.h>

#include <limits>

namespace LBMS{

  //=================================================================

  IndexSetPtr
  Shape::points( const BundlePtr bundle ) const
  {
    IndexSetPtr pts( new IndexSet );
    // loop over each point in the bounding box to determine which of those are within the shape.
    const Box box( bounding_box() );
    const Coordinate& bundleLo = bundle->origin();
    const Coordinate& bundleHi = bundleLo + bundle->length();
    const Coordinate& boxLo = box.low();
    const Coordinate& boxHi = box.high();
    Coordinate loCoord, hiCoord;

    bool is2d = false;
    unsigned short ix2d=3;
    for( unsigned short i=0; i<3; ++i ){
      loCoord[i] = std::max( bundleLo[i], boxLo[i] );
      hiCoord[i] = std::min( bundleHi[i], boxHi[i] );

      // try to detect 2D shapes.
      if( boxLo[i] == boxHi[i] ){
        is2d = true;
        ix2d = i;
      }
    }

    const SpatialOps::IntVec lo( bundle->owning_cell(loCoord) );
    SpatialOps::IntVec hi( bundle->owning_cell(hiCoord) );
    for( unsigned short i=0; i<3; ++i ){
      hi[i] = std::max( lo[i]+1, hi[i] );
    }
    for( size_t k=lo[2]; k<hi[2]; ++k ){
      for( size_t j=lo[1]; j<hi[1]; ++j ){
        for( size_t i=lo[0]; i<hi[0]; ++i ){
          // This identifies the cell volume centroids that fall within the shape
          const SpatialOps::IntVec ipt(i,j,k);
          Coordinate pt( bundle->cell_coord(ipt) );
          if( is2d ) pt[ix2d] = boxLo[ix2d];
          if( is_in_shape(pt) ) pts->insert( ipt );
        }
      }
    }
    return pts;
  }

  //=================================================================

  Sphere::Sphere( const double radius, const Coordinate c )
  : radius_(radius),
    center_(c)
  {}

  //-----------------------------------------------------------------

  bool Sphere::is_in_shape( const Coordinate& c ) const{
    return radius_ >= distance(center_, c);
  }

  //-----------------------------------------------------------------

  Box Sphere::bounding_box() const
  {
    const Coordinate rad( radius_, radius_, radius_ );
    return Box( center_ - rad,
                center_ + rad );
  }

  //-----------------------------------------------------------------

  bool Sphere::operator==(const Shape& s) const{
    const Sphere& o = dynamic_cast<const Sphere&>(s);
    return center_ == o.center_ && radius_ == o.radius_;
  }

  //-----------------------------------------------------------------

  void Sphere::print( std::ostream& os ) const{
    os << "Sphere: centroid=" << center_ << ", radius=" << radius_;
  }

  //=================================================================

  Box::Box( const Coordinate c1, const Coordinate c2 )
  : lower_( c1 ),
    upper_( c2 )
  {
    assert( lower_[0] <= upper_[0] );
    assert( lower_[1] <= upper_[1] );
    assert( lower_[2] <= upper_[2] );
  }

  //-----------------------------------------------------------------

  bool Box::is_in_shape( const Coordinate& c ) const
  {
    return (
        lower_[0] <= c[0] && c[0] <= upper_[0] &&
        lower_[1] <= c[1] && c[1] <= upper_[1] &&
        lower_[2] <= c[2] && c[2] <= upper_[2]
    );
  }

  //-----------------------------------------------------------------

  bool Box::operator==(const Shape& s) const{
    const Box& o = dynamic_cast<const Box&>(s);
    return lower_ == o.lower_ && upper_ == o.upper_;
  }

  //-----------------------------------------------------------------

  void Box::print( std::ostream& os ) const{
    os << "Box: lower=" << lower_ << ", upper=" << upper_;
  }

  //=================================================================

  Shape2D::Shape2D( const Faces unitNormal )
  : unitNormal_( unitNormal ),
    ix_( (unitNormal==XMINUS || unitNormal==XPLUS) ? 0 : (unitNormal==YMINUS || unitNormal==YPLUS) ? 1 : 2 )
  {}

  //-----------------------------------------------------------------

  IndexSetPtr
  Shape2D::points( const BundlePtr bundle ) const
  {
    IndexSetPtr pts( new IndexSet );

    // Determine the coordinate bounds to search for inclusion in the Shape.
    const Box box( bounding_box() );
    const Coordinate& bundleLo = bundle->origin();
    const Coordinate& bundleHi = bundleLo + bundle->length();

    const Coordinate& boxLo = box.low();
    const Coordinate& boxHi = box.high();

    const double tol = 10*std::numeric_limits<double>::epsilon();
    // in parallel-decomposed bundles, address the situation where the Shape isn't in the bundle
    // dac:  This doesn't protect against partial planes
    if( !( std::abs(bundleLo[ix_] - boxLo[ix_]) < tol ) && !( std::abs(bundleHi[ix_] - boxHi[ix_]) < tol ) ){
      return pts;
    }

    Coordinate loCoord, hiCoord;
    for( unsigned short i=0; i<3; ++i ){
      loCoord[i] = std::max( bundleLo[i], boxLo[i] );
      hiCoord[i] = std::min( bundleHi[i], boxHi[i] );
    }

    // Determine the index bounds to search for inclusion in the shape
    SpatialOps::IntVec lo( bundle->owning_cell(loCoord) );
    SpatialOps::IntVec hi( bundle->owning_cell(hiCoord) );

    for( unsigned short i=0; i<3; ++i ){
      // ensure that we have a non-null index set when a bundle edge aligns with shape
      if( std::abs(hi[i] - lo[i]) < tol ) hi[i] = lo[i]+1;
      // If bundle is outside of the shape on a plane normal to the bundle (e.g. multiple planes in a parallel-decomp)
      if( hi[i] < (lo[i] - tol) && i != ix_ ) return pts;
    }

    /*
     * "minus" faces indicate that the volume coordinate location should be on the (+) side
     *  while "plus" faces indicate that the volume coordinate should be on the (-) side.
     *  So we shift "plus" coordinates to the (-) direction as appropriate.
     */
    switch( unit_normal() ){
      case XMINUS: case YMINUS: case ZMINUS: break;
      case XPLUS: lo[0]--; hi[0]--; break;
      case YPLUS: lo[1]--; hi[1]--; break;
      case ZPLUS: lo[2]--; hi[2]--; break;
    }

    for( size_t k=lo[2]; k<hi[2]; ++k ){
      for( size_t j=lo[1]; j<hi[1]; ++j ){
        for( size_t i=lo[0]; i<hi[0]; ++i ){
          // This identifies the cell volume centroids that fall within the shape
          const SpatialOps::IntVec ipt(i,j,k);
          Coordinate pt( bundle->cell_coord(ipt) );
          // correct the coordinate for the plane location.
          // Otherwise, we never fall on the surface.
          pt[ix_] = boxLo[ix_];
          if( is_in_shape(pt) ) pts->insert( ipt );
        }
      }
    }
    return pts;
  }

  //=================================================================

  Plane::Plane( const Faces normal,
                const Coordinate location )
  : Shape2D( normal ),
    loc_( location )
  {}

  //-----------------------------------------------------------------

  bool
  Plane::is_in_shape( const Coordinate& c ) const
  {
    // here we allow slight differences from precisely in plane to handle potential roundoff issues.
    const double tol = 10*std::numeric_limits<double>::epsilon();
    return std::abs(c[ix_] - loc_[ix_]) <= tol;
  }

  //-----------------------------------------------------------------

  Box
  Plane::bounding_box() const
  {
    const double tol = 10*std::numeric_limits<double>::epsilon();
    Coordinate low  = loc_;
    Coordinate high = loc_;
    for( unsigned short i=0; i<3; ++i ){
      if( !( std::abs(i - ix_) < tol )  ){
        low [i] = -std::numeric_limits<double>::max();
        high[i] =  std::numeric_limits<double>::max();
      }
    }
    return Box( low, high );
  }

  //-----------------------------------------------------------------

  bool
  Plane::operator==(const Shape& s) const{
    const Plane& o = dynamic_cast<const Plane&>(s);
    return o.unitNormal_ == unitNormal_ && loc_[ix_] == o.loc_[ix_];
  }

  //-----------------------------------------------------------------

  void Plane::print( std::ostream& os ) const{
    os << "Plane in " << face_name(unitNormal_) << ": centroid=" << loc_;
  }

  //=================================================================

  Circle::Circle( const Faces normal,
                  const Coordinate center,
                  const double radius )
  : Shape2D( normal ),
    center_( center ),
    radius_( radius )
  {}

  //-----------------------------------------------------------------

  bool
  Circle::is_in_shape( const Coordinate& c ) const
  {
    // here we allow slight differences from precisely in plane to handle potential roundoff issues.
    const double tol = 10*std::numeric_limits<double>::epsilon();
    return ( std::abs(c[ix_] - center_[ix_]) <= tol ) && ( radius_ >= distance(center_,c) );
  }

  //-----------------------------------------------------------------

  Box
  Circle::bounding_box() const
  {
    const Coordinate rad( radius_, radius_, radius_ );
    Coordinate low  = center_ - rad;
    Coordinate high = center_ + rad;
    low [ix_] = center_[ix_];
    high[ix_] = center_[ix_];
    return Box( low, high );
  }

  //-----------------------------------------------------------------

  bool
  Circle::operator==(const Shape& s) const{
    const Circle& c = dynamic_cast<const Circle&>(s);
    return c.unitNormal_ == unitNormal_ && c.center_ == center_ && c.radius_ == radius_;
  }

  //-----------------------------------------------------------------

  void Circle::print( std::ostream& os ) const{
    os << "Circle in " << face_name(unitNormal_) << " : centroid=" << center_ << ", radius=" << radius_;
  }

  //=================================================================

  Rectangle::Rectangle( const Faces normal,
                        const Coordinate c1,
                        const Coordinate c2 )
  : Shape2D( normal ),
    lower_( c1 ),
    upper_( c2 )
  {
    assert( lower_[0] <= upper_[0] );
    assert( lower_[1] <= upper_[1] );
    assert( lower_[2] <= upper_[2] );

    if( c1[ix_] != c2[ix_] ){
      std::ostringstream msg;
      msg << "\n" << __FILE__ " : " << __LINE__
          << "\nError creating a Rectangle.  The two coordinates must lie in an ordinal plane (x,y,z).\n"
          << "Found coordinates:\n\t" << c1 << "\n\t" << c2 << "\n\n";
      throw std::invalid_argument( msg.str() );
    }
  }

  //-----------------------------------------------------------------

  bool Rectangle::is_in_shape( const Coordinate& c ) const
  {
    const Direction dir0 = direction( ix_ );
    const Direction dir1( get_perp1(dir0) );
    const Direction dir2( get_perp2(dir0) );
    const unsigned short i0 = ix_;
    const unsigned short i1 = select_from_dir(dir1,0,1,2);
    const unsigned short i2 = select_from_dir(dir2,0,1,2);
    const double tol = 10*std::numeric_limits<double>::epsilon();

    return (
        std::abs( c[ix_] - lower_[ix_] ) < tol &&
        std::abs( c[ix_] - upper_[ix_] ) < tol &&
        lower_[i1] <= c[i1] && c[i1] <= upper_[i1] &&
        lower_[i2] <= c[i2] && c[i2] <= upper_[i2]
    );
  }

  //-----------------------------------------------------------------

  Box Rectangle::bounding_box() const{
    return Box(lower_,upper_);
  }

  //-----------------------------------------------------------------

  bool Rectangle::operator==(const Shape& s) const{
    const Rectangle& o = dynamic_cast<const Rectangle&>(s);
    return lower_ == o.lower_ && upper_ == o.upper_;
  }

  //-----------------------------------------------------------------

  void Rectangle::print( std::ostream& os ) const{
    os << "Rectangle in plane " << face_name(unitNormal_) << " : low=" << lower_ << ", high=" << upper_;
  }

  //=================================================================

} // namespace LBMS