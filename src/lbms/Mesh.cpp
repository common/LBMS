/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <sstream>
#include <stdexcept>
#include <iomanip>
#include <iostream>

#include "Mesh.h"
#include "Bundle.h"

#include <lbms/Direction.h>
#include <fields/StringNames.h>
#include <fields/Fields.h>

using namespace std;
using SpatialOps::IntVec;

namespace LBMS{

  //====================================================================

  void
  check_mesh_consistency( const Direction dir,
                          const int nfine,
                          const int ncoarse,
                          const double L )
  {
    const double dx = L / nfine;
    const double Dx = L / ncoarse;

    if( nfine<1 || ncoarse<1 ){
      ostringstream msg;
      msg << __FILE__ " : " << __LINE__ << endl
          << "ERROR: invalid mesh in the " << get_dir_name(dir) << " direction" << endl
          << "  Number of points must be >= 1." << endl;
      throw runtime_error( msg.str() );
    }
    if( nfine % ncoarse != 0 || nfine<ncoarse ){
      ostringstream msg;
      msg.setf(ios_base::left);
      msg << __FILE__ " : " << __LINE__ << endl
          << "ERROR: invalid mesh in the "
          << get_dir_name( dir ) << " direction" << endl
          << "  Coarse mesh:  Dx=" << setw(5) << Dx << "  N=" << ncoarse << endl
          << "  Fine   mesh:  dx=" << setw(5) << dx << "  n=" << nfine << endl
          << "Coarse mesh spacing must be an integer multiple of the fine mesh spacing." << endl
          << "  Dx/dx = " << Dx/dx << ",  mod(n,N) = " << nfine%ncoarse << endl;
      throw runtime_error( msg.str() );
    }
  }

  //====================================================================

  IntVec set_bundle_dim( const IntVec ncoarse, const Direction dir, IntVec nfine ){
    const size_t tmp=nfine[dir];
    nfine = ncoarse;
    nfine[dir] = tmp;
    return nfine;
  }

  Mesh::Mesh( const IntVec nCoarse,
              const IntVec nFine,
              const Coordinate& length,
              const bool periX,
              const bool periY,
              const bool periZ )
    : xBundle_( nFine[0] > 1 ? new Bundle( XDIR, nCoarse[0], set_bundle_dim( nCoarse, XDIR, nFine ), length, IntVec(0,0,0), IntVec(0,0,0) ) : NULL ),
      yBundle_( nFine[1] > 1 ? new Bundle( YDIR, nCoarse[1], set_bundle_dim( nCoarse, YDIR, nFine ), length, IntVec(0,0,0), IntVec(0,0,0) ) : NULL ),
      zBundle_( nFine[2] > 1 ? new Bundle( ZDIR, nCoarse[2], set_bundle_dim( nCoarse, ZDIR, nFine ), length, IntVec(0,0,0), IntVec(0,0,0) ) : NULL ),
      nCoarse_( nCoarse ),
      nFine_  ( nFine   ),
      length_ ( length  ),
      periX_( periX ? PeriodicBoundary : (nFine[0]>1 ? DomainBoundary : NONE) ),
      periY_( periY ? PeriodicBoundary : (nFine[1]>1 ? DomainBoundary : NONE) ),
      periZ_( periZ ? PeriodicBoundary : (nFine[2]>1 ? DomainBoundary : NONE) )
  {
    check_mesh_consistency( XDIR, nFine[0], nCoarse[0], length[0] );
    check_mesh_consistency( YDIR, nFine[1], nCoarse[1], length[1] );
    check_mesh_consistency( ZDIR, nFine[2], nCoarse[2], length[2] );

    // set the type of boundary present.  NONE if a direction is inactive.
    BoundaryType btype[6];  for( short i=0; i<6; ++i ){ btype[i]=NONE; }
    if( nFine[0]>1 ){ btype[0] = periX_; btype[1] = periX_; }  // x-direction is active
    if( nFine[1]>1 ){ btype[2] = periY_; btype[3] = periY_; }  // y-direction is active
    if( nFine[2]>1 ){ btype[4] = periZ_; btype[5] = periZ_; }  // z-direction is active

    // imprint boundary type on bundles
    if( nFine[0] > 1 ) xBundle_->set_boundary_types( btype );
    if( nFine[1] > 1 ) yBundle_->set_boundary_types( btype );
    if( nFine[2] > 1 ) zBundle_->set_boundary_types( btype );
  }

  //--------------------------------------------------------------------

  Mesh::~Mesh()
  {}

  //--------------------------------------------------------------------

  bool Mesh::is_periodic( const Direction dir ) const
  {
    return select_from_dir( dir, periX_==PeriodicBoundary, periY_==PeriodicBoundary, periZ_==PeriodicBoundary );
  }

  //--------------------------------------------------------------------

  Coordinate Mesh::spacing() const{
    return Coordinate( length_[0]/nFine_[0], length_[1]/nFine_[1], length_[2]/nFine_[2] );
  }

  Coordinate Mesh::spacing_coarse() const{
    return Coordinate( length_[0]/nCoarse_[0], length_[1]/nCoarse_[1], length_[2]/nCoarse_[2] );
  }

  //--------------------------------------------------------------------

  BundlePtr
  Mesh::bundle( const Direction dir ) const
  {
    switch (dir){
    case XDIR: return xBundle_;
    case YDIR: return yBundle_;
    case ZDIR: return zBundle_;
    case NODIR: assert( false );
    }
    assert(false);
    return xBundle_;  // should never get here.
  }

  //====================================================================

} // namespace LBMS
