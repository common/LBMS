#include "BCOptions.h"

#include <lbms/Bundle.h>
#include <lbms/bcs/BCGeometry.h>
#include <lbms/bcs/HardInflow.h>
#include <lbms/bcs/Wall.h>
#include <lbms/Coordinate.h>
#include <lbms/Direction.h>
#include <lbms/bcs/PointCollection.h>
#include <lbms/bcs/Shapes.h>
#include <fields/Fields.h>
#include <fields/StringNames.h>
#include <operators/Operators.h>
#include <exprs/BoundaryConditions.h>

#include <boost/algorithm/string.hpp>

#include <expression/ExprLib.h>
#include <expression/MaskedExpression.h>

#include <pokitt/CanteraObjects.h>

//--- NSCBC include for C --//
#include <nscbc/SpeedOfSound.h>

namespace LBMS{

  //-----------------------------------------------------------------

  /**
   * \brief create the function boundary condition expression
   * \param opts the BCOptions struct used for holding the options from the parser
   * \param mask the mask this boundary condition applies to
   * \param params the parser node for getting information not held in the Options struct
   * \param bundle the bundle this boundary condition applies to.
   * \prapm tag the tag for this boundary condition.
   */
  template<typename FieldT, typename DirT>
  Expr::ExpressionBuilder*
  build_boundary_expr( const BCOptions& opts,
                       const YAML::Node& params,
                       const BundlePtr bundle,
                       const Expr::Tag& tag )
  {
    LBMS::Direction dir = bundle->dir();
    Expr::ExpressionBuilder* builder = NULL;

    typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskType Mask;
    typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr MaskPtr;

    const MaskPtr mask = opts.points.get_mask<typename Mask::field_type>();

    if( params["Constant"] ){
      typedef typename Expr::ConstantBCOpExpression<FieldT, DirT>::Builder Builder;
      builder = new Builder( tag, mask, opts.bctype_, opts.points.side(), params["Constant"].as<double>() );
    }

    else if( params["LinearFunction"] ){
      const YAML::Node valParams = params["LinearFunction"];
      typedef typename LinearFunctionBC<FieldT, DirT>::Builder Builder;
      builder = new Builder( mask, opts, tag,
                             parse_nametag( valParams["NameTag"], dir ),
                             valParams["slope"    ].as<double>(),
                             valParams["intercept"].as<double>() );
    }

    else if( params["GaussianFunction"] ) {
      const YAML::Node valParams = params["GaussianFunction"];
      typedef typename GaussianFunctionBC<FieldT, DirT>::Builder Builder;
      builder = new Builder( mask, opts, tag,
                             parse_nametag( valParams["IndependentVariable"], dir ),
                             valParams["amplitude"].as<double>(),
                             valParams["deviation"].as<double>(),
                             valParams["mean"     ].as<double>(),
                             valParams["baseline" ].as<double>() );
    }

    if( builder == NULL ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "ERROR: did not resolve a builder for " << params.begin()->first.as<std::string>() << std::endl;
      throw std::invalid_argument( msg.str() );
    }

    return builder;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  boost::shared_ptr< NSCBC::BCBuilder<FieldT> >
  setup_NSCBC_boundary_conditions( const LBMS::Options& lbmsOptions,
                                   const YAML::Node& nscbcParser,
                                   const BundlePtr bundle,
                                   GraphCategories& gc,
                                   const PointCollection2D& points )
  {
    // Get the Type whether reflecting or hard flow
    const NSCBC::BCType type = NSCBC::type_def( nscbcParser["BCType"].as<std::string>());
    const std::string label = nscbcParser["label"].as<std::string>();
    const std::string jobName = label+bundle->name();
    std::map< NSCBC::TagName,     Expr::Tag     > tags;
    std::map< NSCBC::TagListName, Expr::TagList > tagLists;

    const double farfieldpressure = nscbcParser["FarFieldPressure"].as<double>( 101325.0 );

    SpatialOps::BCSide side;
    NSCBC::Direction dir;

    Direction lbmsDir = LBMS::NODIR;

    switch( points.unit_normal() ){
      case XMINUS:
        lbmsDir = LBMS::XDIR;
        dir     = NSCBC::XDIR;
        side    = SpatialOps::MINUS_SIDE;
        break;
      case XPLUS:
        lbmsDir = LBMS::XDIR;
        dir     = NSCBC::XDIR;
        side    = SpatialOps::PLUS_SIDE;
        break;
      case YMINUS:
        lbmsDir = LBMS::YDIR;
        dir     = NSCBC::YDIR;
        side    = SpatialOps::MINUS_SIDE;
        break;
      case YPLUS:
        lbmsDir = LBMS::YDIR;
        dir     = NSCBC::YDIR;
        side    = SpatialOps::PLUS_SIDE;
        break;
      case ZMINUS:
        lbmsDir = LBMS::ZDIR;
        dir     = NSCBC::ZDIR;
        side    = SpatialOps::MINUS_SIDE;
        break;
      case ZPLUS:
        lbmsDir = LBMS::ZDIR;
        dir     = NSCBC::ZDIR;
        side    = SpatialOps::PLUS_SIDE;
        break;
    }

    {
      using Expr::Tag; using Expr::STATE_NONE; using Expr::STATE_N;
      const std::string parVel = select_from_dir( lbmsDir, StringNames::self().xvel, StringNames::self().yvel, StringNames::self().zvel );
      const std::string dp     = select_from_dir( lbmsDir, StringNames::self().dpdx, StringNames::self().dpdy, StringNames::self().dpdz );
      const std::string dvel   = select_from_dir( lbmsDir, StringNames::self().dx,   StringNames::self().dy,   StringNames::self().dz   );
      tags[NSCBC::U]           = create_tag( StringNames::self().xvel, STATE_NONE, bundle->dir() );
      tags[NSCBC::V]           = create_tag( StringNames::self().yvel, STATE_NONE, bundle->dir() );
      tags[NSCBC::W]           = create_tag( StringNames::self().zvel, STATE_NONE, bundle->dir() );
      tags[NSCBC::FACEVEL]     = create_tag( parVel, STATE_NONE, bundle->dir() );
      tags[NSCBC::T]           = create_tag( lbmsOptions.pokittOptions.temperature, STATE_NONE, bundle->dir() );
      tags[NSCBC::P]           = create_tag( lbmsOptions.pokittOptions.pressure, STATE_NONE, bundle->dir() );

      tags[NSCBC::RHO]         = create_tag( StringNames::self().density, STATE_N   , bundle->dir());
      tags[NSCBC::MMW]         = create_tag( StringNames::self().mixmw,   STATE_NONE, bundle->dir() );
      tags[NSCBC::CP]          = create_tag( StringNames::self().cp,      STATE_NONE, bundle->dir() );
      tags[NSCBC::CV]          = create_tag( StringNames::self().cv,      STATE_NONE, bundle->dir() );
      tags[NSCBC::C]           = create_tag( StringNames::self().c,       STATE_NONE, bundle->dir() );
      tags[NSCBC::E0]          = create_tag( StringNames::self().e0,      STATE_NONE, bundle->dir() );

      tagLists[NSCBC::H_N]     = lbmsOptions.pokittOptions.species_tags( bundle->dir(), STATE_NONE, StringNames::self().enthalpy );
      tagLists[NSCBC::Y_N]     = lbmsOptions.pokittOptions.species_tags( bundle->dir(), STATE_NONE );
      tagLists[NSCBC::R_N]     = lbmsOptions.pokittOptions.species_tags( bundle->dir(), STATE_NONE, "_rr" );

      tags[NSCBC::DVELHARDINFLOW] = create_tag( parVel + dvel + "_vol", STATE_NONE, bundle->dir() );
      tags[NSCBC::DPHARDINFLOW]   = create_tag( dp, STATE_NONE, bundle->dir() );
    }

    NSCBC::TagManager tMgr( tags, tagLists, lbmsOptions.pokittOptions.doSpeciesTransport );

    // Register the Speed of Sound
    Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;
    if( !( factory.have_entry(tMgr[NSCBC::C]) ) ){
      typedef typename NSCBC::SpeedOfSound<FieldT>::Builder C;
      factory.register_expression( new C( tMgr[NSCBC::C], tMgr[NSCBC::P], tMgr[NSCBC::RHO], tMgr[NSCBC::CP], tMgr[NSCBC::CV] ),
                                           false, bundle->id() );
    }

    const double length = bundle->glob_length( lbmsDir );

    boost::shared_ptr<SpatialOps::SpatialMask<FieldT> > mask = points.get_mask<FieldT>();

    assert(         bundle->npts          (lbmsDir)  > 1);
    const bool do2( bundle->npts(get_perp1(lbmsDir)) > 1);
    const bool do3( bundle->npts(get_perp2(lbmsDir)) > 1);

    NSCBC::NSCBCInfo<FieldT> info( *mask, side, dir, type, jobName, farfieldpressure, length );

    typedef typename boost::shared_ptr< NSCBC::BCBuilder<FieldT> > bcBuilderT;
    return bcBuilderT( new NSCBC::BCBuilder<FieldT>( info,
                                                     CanteraObjects::molecular_weights(),
                                                     CanteraObjects::gas_constant(),
                                                     tMgr,
                                                     do2,
                                                     do3,
                                                     NSCBC::NonreflectingSubSwitch::SUBTRACTION_OFF,
                                                     bundle->id() ) );
  }

  //------------------------------------------------------------------

  template<typename FieldT, typename DirT>
  void
  setup_boundary_conditions( LBMS::BCOptions& options,
                             const YAML::Node& parser,
                             const BundlePtr bundle,
                             GraphCategories& gc )
  {
    // Get the Type whether Dirichlet or Neumann
    options.bctype_ = select_bctype( parser["BCType"].as<std::string>() );
    const PointCollection2D& points = options.points;

    Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;
    const Expr::Tag modTag( parser["label"].as<std::string>()
                            + "_" + options.tag_.name()
                            +"_bc_"+ bctype_to_string(options.bctype_)
                            + "_Face_"+face_name(points.unit_normal()),
                            Expr::STATE_NONE );
    Expr::ExpressionBuilder* builder = NULL;

    typedef SpatialOps::BCOpTypesFromDirection<FieldT,DirT> BCOpT;
    typedef typename BCOpT::Dirichlet  DirichletOpT;
    typedef typename DirichletOpT::OperatorType::DestFieldType BCValFieldT;

    // jcs can we create a ConvertedMask here and pass that into the expressions?
    // we want to be able to create masks only on the volume fields and then convert them as needed.
    // that may be possible if we pass ConvertedMask rather than SpatialMask to the expressions.


    // jcs note that build_boundary_expr also handles Constant, so perhaps we should drop this "Function" business altogether?
    if( options.isfunct ){
      const YAML::Node func( parser["Function"] );
      builder = build_boundary_expr<FieldT, DirT>( options, func, bundle, modTag );
    }
    else{
      typedef typename Expr::ConstantBCOpExpression<FieldT, DirT>::Builder BCExpr;
      typedef typename BCExpr::MaskType  Mask;
      typedef typename BCExpr::MaskPtr   MaskPtr;
      builder = new BCExpr( modTag,
                            points.get_mask< typename Mask::field_type >(),
                            options.bctype_,
                            points.side(),
                            options.val );
    }
    factory.register_expression( builder, false, bundle->id() );
    factory.attach_modifier_expression( modTag, options.tag_, ALL_PATCHES, false );
    return;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  void
  parse_field_conditions( const YAML::Node& bcParser,
                          const BundlePtr bundle,
                          GraphCategories& gc,
                          const PointCollection2D& pts,
                          const LBMS::Options& lbmsOptions,
                          typename LBMS::NSCBCVecType<FieldT>::NSCBCVec& nscbcVec )
  {
    if( bundle->get_boundary_type( pts.unit_normal() ) == DomainBoundary ){
      const std::string bcType = bcParser["BCType"].as<std::string>();
      // jcs consider reworking this a bit.  NSCBC requires different information so we may want to have separate parsing for it.
      if( bcType == "NonreflectingFlow" ){
        boost::shared_ptr< NSCBC::BCBuilder<FieldT> >
        nscbcBuilder = setup_NSCBC_boundary_conditions<FieldT>( lbmsOptions, bcParser, bundle, gc, pts );
        nscbcVec.push_back( std::make_pair( nscbcBuilder, pts ) );
      }
      else if( bcType == "HardInflow" ){
        //Sets up the incoming wave amplitude (NSCBC) for density.  Only needs to be done once
        boost::shared_ptr< NSCBC::BCBuilder<FieldT> >
        nscbcBuilder = setup_NSCBC_boundary_conditions<FieldT>( lbmsOptions, bcParser, bundle, gc, pts );
        nscbcVec.push_back( std::make_pair( nscbcBuilder, pts ) );

        HardInflow<FieldT> hardInflow = HardInflow<FieldT>( bcParser, bundle, gc, pts, lbmsOptions );
      }
      else if ( bcType == "Wall" ){
        boost::shared_ptr< NSCBC::BCBuilder<FieldT> >
        nscbcBuilder = setup_NSCBC_boundary_conditions<FieldT>( lbmsOptions, bcParser, bundle, gc, pts );
        nscbcVec.push_back( std::make_pair( nscbcBuilder, pts ) );

        Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;

        Wall<FieldT> wall = Wall<FieldT>( bcParser, bundle, gc, pts, lbmsOptions );

      }//end if wall
      else{
        // set up BCs associated with the specified Geometry (which is embodied in the PointCollection2D).
        LBMS::BCOptions options( pts,
                                 parse_nametag( bcParser["Target"], bundle->dir() ),
                                 bundle->dir(),
                                 lbmsOptions );

        if( bcParser["Value"] ){
          options.val = bcParser["Value"].as<double>();
          options.isfunct = false;
        }
        else{
          options.isfunct = true;
        }

        switch( pts.unit_normal() ){
          case XMINUS:
          case XPLUS :
            setup_boundary_conditions< FieldT, SpatialOps::XDIR >( options, bcParser, bundle, gc );
            break;
          case YMINUS:
          case YPLUS :
            setup_boundary_conditions< FieldT, SpatialOps::YDIR >( options, bcParser, bundle, gc );
            break;
          case ZMINUS:
          case ZPLUS :
            setup_boundary_conditions< FieldT, SpatialOps::ZDIR >( options, bcParser, bundle, gc );
            break;
        }//End Switch
      }//End else
    }//End Boundary Check
    return;
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  typename LBMS::NSCBCVecType<FieldT>::NSCBCVec
  parse_boundary_conditions( const YAML::Node& parser,
                             const BundlePtr bundle,
                             GraphCategories& gc,
                             const LBMS::Options& options )
  {
    const YAML::Node geoParser = parser["Geometries"];
    const YAML::Node bcParser  = parser["BoundaryCondition"];
    typename NSCBCVecType<FieldT>::NSCBCVec nscbcVec;

    // first, parse the geometry objects that will be used for the BCs
    GeoMapPtr geomMap;
    try{
      geomMap = parse_geometries(geoParser, bundle);
    }
    catch( std::exception& e ){
      std::ostringstream msg;
      msg << "Error parsing Geometry \n\t"
          << __FILE__ << " : " << __LINE__ << std::endl
          << e.what() << std::endl;
      throw std::runtime_error(msg.str());
    }

    for( YAML::Node::const_iterator iex=bcParser.begin(); iex!=bcParser.end(); ++iex ){
      const YAML::Node& bc = *iex;
      const std::string label = bc["label"].as<std::string>();
      const std::string geom  = bc["Geometry"].as<std::string>();
      GeoMap::iterator igeom = geomMap->find(geom);
      if( igeom != geomMap->end() ){
        PointCollection2D& pts = igeom->second;

        // create masks for this geometry since it will actually be used
        pts.create_masks<FieldT>( bundle );

        //Check to see if this processor has mask points for this field type
        {
          //We only have to check one field as volume and every face field
          //will have the same points prior to mask creation
          typedef typename FieldT::Location::Bundle DirT;
          typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskType Mask;
          typedef typename Expr::BoundaryConditionExpression<FieldT,DirT>::MaskPtr MaskPtr;
          if( pts.get_mask<typename Mask::field_type>()->points().size() > 0 ){
            parse_field_conditions<FieldT>( bc, bundle, gc, pts, options, nscbcVec );
          }
        }
      }
      else{
        std::ostringstream msg;
        msg << "\n" << __FILE__<< " : " << __LINE__
            << "\n\tERROR: While examining BC labeled '" << label
            << "'\n\tThe requested geometry '" << geom << "' was not specified in the 'Geometries' block"
            << "\n\tDouble-check your input file for consistency\n";
        throw std::runtime_error( msg.str() );
      }
    }
    return nscbcVec;
  }

  //-----------------------------------------------------------------

  BdType select_bd_type( const std::string& bcTypeStr )
  {
    if     ( boost::iequals(bcTypeStr,"wall"   )) return WALL;
    else if( boost::iequals(bcTypeStr,"outflow")) return OUTFLOW;
    else if( boost::iequals(bcTypeStr,"inflow" )) return INFLOW;
    return INVALID;
  }

  const std::string bd_type_to_string( const BdType bdtype )
  {
    switch(bdtype){
      case WALL   : return "Wall";    break;
      case OUTFLOW: return "Outflow"; break;
      case INFLOW : return "Inflow";  break;
      case INVALID:
      default:
        break;
    }
    return "Invalid";
  }

  const std::string bctype_to_string( const SpatialOps::BCType bct )
  {
    switch(bct){
      case SpatialOps::DIRICHLET: return "Dirichlet"; break;
      case SpatialOps::NEUMANN  : return "Neumann";   break;
      case SpatialOps::UNSUPPORTED_BCTYPE:
      default:
        break;
    }
    return "Unsupported";
  }

  SpatialOps::BCType select_bctype( const std::string& bctypestr )
  {
    if( boost::iequals(bctypestr, "dirichlet") ){
      return SpatialOps::DIRICHLET;
    }
    else if(boost::iequals(bctypestr, "neumann") || boost::iequals(bctypestr, "neuman") ){
      return SpatialOps::NEUMANN;
    }
    return SpatialOps::UNSUPPORTED_BCTYPE;
  }

} // namespace LBMS


#include <fields/Fields.h>
using SpatialOps::FaceTypes;
#define INSTANTIATEOPS( VolT )                         \
    template LBMS::NSCBCVecType<VolT>::NSCBCVec LBMS::parse_boundary_conditions<VolT>(const YAML::Node&, const BundlePtr, GraphCategories&, const LBMS::Options& );

INSTANTIATEOPS( LBMS::XVolField )
INSTANTIATEOPS( LBMS::YVolField )
INSTANTIATEOPS( LBMS::ZVolField )


