/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef LBMS_Options_h
#define LBMS_Options_h

#include <string>
#include <map>
#include <expression/ExprLib.h>

#include <lbms/BCOptions.h>
#include <lbms/Direction.h>

#include <yaml-cpp/yaml.h>

namespace LBMS{

  //forward declare
  class BCOptions;


  /**
   *  @struct IntegratorOptions
   *  @brief Describes options related to time integration
   */
  struct IntegratorOptions
  {
    Expr::TSMethod timeintegrator; ///< The time integrator to use, default Forward Euler
    double timestep;     ///< The timestep
    double monitorTime;  ///< The time interval for solution monitoring
    double fileTime;     ///< The time interval for file output
    double endTime;      ///< The end time for the simulation

    IntegratorOptions( const YAML::Node& );
  };



  /**
   *  @struct PoKiTTOptions
   *  @brief Describes options related to PoKiTT
   */
  struct PoKiTTOptions
  {
    bool doSpeciesTransport;  ///< Switch for species equations (default false)
    bool doReactions;         ///< activate chemical reactions in gas phase

    int nspecies;

    std::string canteraInputFileName;  ///< name of the cantera input file (*.cti)
    std::string canteraGroupName;      ///< name of the gas group in the canter input file.

    std::string viscosity, thermCond, temperature, pressure;

    const YAML::Node pokittParser;

    PoKiTTOptions( const YAML::Node& );

    Expr::TagList species_tags( const LBMS::Direction dir,
                                const Expr::Context state,
                                const std::string name="" ) const;

    /**
     * @brief Obtain the species flux tags
     * @param bundleDir the bundle direction of interest
     * @param fluxDir the flux direction
     * @param state the Expr::Context
     * @return
     */
    Expr::TagList species_flux_tags( const LBMS::Direction fluxDir,
                                     const LBMS::Direction bundleDir,
                                     const Expr::Context state ) const;

    /**
     * @brief Obtain the species flux tags for reconstruction
     * @param bundleDir the bundle direction of interest
     * @param fluxDir the flux direction
     * @param state the Expr::Context
     * @return
     */
    Expr::TagList species_flux_tags_reconstructed( const LBMS::Direction fluxDir,
                                                   const LBMS::Direction bundleDir,
                                                   const Expr::Context state,
                                                   const std::string appendage ) const;

  private:
    void setup_cantera();
  };

  /**
   *  @struct Options
   *  @brief  Master set of options
   */
  struct Options
  {
    IntegratorOptions integrator; ///< Options related to time integration

    PoKiTTOptions   pokittOptions;  ///< Options related to thermochemistry

    std::map< std::string, BCOptions >  bcoptions;

    bool filtering;  ///< Option related to filtering high frequency noise

    Options( const YAML::Node& );
  };


}  // namespace LBMS

/**
 * \brief changes from string to the time integrator enum, defaults to Forward Euler
 *
 * @param[in] s string containing the name of the method
 *
 * @return the enum of which method to use.
 */
inline Expr::TSMethod
string_to_ts_method( const std::string s )
{
  if( s.compare( "Forward Euler" ) == 0 )
    return Expr::FORWARD_EULER;

  if( s.compare( "SSPRK3" ) == 0 || s.compare( "RK3" ) == 0 )
    return Expr::SSPRK3;

  return Expr::FORWARD_EULER;
}

#endif
