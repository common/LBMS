/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef LBMSProblemSolver_h
#define LBMSProblemSolver_h

#include <list>

#include <fields/Fields.h>
#include <lbms/GraphHelperTools.h>
#include <lbms/Bundle_fwd.h>
#include <lbms/BCOptions.h>

#include <expression/ExprFwd.h>
#include <expression/Tag.h>

#include <nscbc/CharacteristicBCBuilder.h>

#include <transport/EquationBase.h>
#include <transport/TransportEquationBase.h>

// forward declarations...
namespace Expr{ class TimeStepper; }
namespace YAML{ class Node; }

class ParseGroup;

namespace LBMS{

  // forward declarations
  template<typename T> class TransportEquationBase;
  struct Options;

  template<typename FieldT> class TransportEquationBase;

//  templateclass EquationBase<FieldT>;

  /**
   *  @class ProblemSolver
   *  @author James C. Sutherland
   *  @date April, 2009
   *  @brief Sets up the problem (builds operators, transport
   *         equations, etc) and manages the solution
   */
  class ProblemSolver
  {
  public:

    ProblemSolver( const YAML::Node& parser,
                   GraphCategories& gc,
                   const BundlePtr&,
                   const Options&,
                   Expr::TimeStepper& ts );

    ~ProblemSolver();

    template< typename FieldT >
    const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec build_boundary_conditions();

  private:

    Expr::Tag densityTag_;

    typedef std::list<LBMS::EquationBase*> EqnList;

    /** @brief build the equations to be solved and the associated expressions */
    template<typename FieldT>
    void build_equations( Expr::TimeStepper& ts, const typename NSCBCVecType<FieldT>::NSCBCVec nscbcVec );

    /** @brief build the momentum equations & associated expressions */
    template<typename FieldT>
    void build_momentum_equations( Expr::TimeStepper& ts, const typename NSCBCVecType<FieldT>::NSCBCVec nscbcVec );

    void initial_conditions();

    const YAML::Node &parser_;
    const BundlePtr  bundle_;
    const Options    &options_;
    GraphCategories& gc_;

    EqnList eqns_;    ///< complete list of equations we are solving.

    ProblemSolver( const ProblemSolver& );          // no copying
    ProblemSolver operator=(const ProblemSolver& ); // no assignment

  };

} // namespace LBMS

#endif // LBMSProblemSolver_h
