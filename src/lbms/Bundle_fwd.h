/* Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   Bundle_fwd.h
 */

#ifndef BUNDLE_FWD_H_
#define BUNDLE_FWD_H_

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>

namespace LBMS{

  // foward declarations for bundles
  class Bundle;
  typedef boost::shared_ptr<Bundle> BundlePtr;
  typedef std::vector<BundlePtr>    BundlePtrVec;

  /** \enum BoundaryType */
  enum BoundaryType{
    ProcessorBoundary, ///< An MPI processor boundary - exchange ghost info
    DomainBoundary,    ///< A domain boundary - user must set BCs
    PeriodicBoundary,  ///< A periodic boundary - exchange ghost info
    NONE               ///< Only used to identify cases where a dimension doesn't exist (e.g., 1D 2D cases)
  };

  enum Faces{
    XMINUS=0,
    XPLUS =1,
    YMINUS=2,
    YPLUS =3,
    ZMINUS=4,
    ZPLUS =5
  };

  /**
   * @brief obtain the name of the given face
   */
  inline std::string face_name( const Faces face ){
    std::string s;
    switch( face ){
    case XMINUS: s="XMINUS"; break;
    case XPLUS : s="XPLUS";  break;
    case YMINUS: s="YMINUS"; break;
    case YPLUS : s="YPLUS";  break;
    case ZMINUS: s="ZMINUS"; break;
    case ZPLUS : s="ZPLUS";  break;
    }
    return s;
  }

  inline Faces get_face( const std::string& face ){
    if     ( face == face_name(XMINUS) ) return XMINUS;
    else if( face == face_name(XPLUS ) ) return XPLUS;
    else if( face == face_name(YMINUS) ) return YMINUS;
    else if( face == face_name(YPLUS ) ) return YPLUS;
    else if( face == face_name(ZMINUS) ) return ZMINUS;
    else if( face == face_name(ZPLUS ) ) return ZPLUS;
    assert( false );
    return XMINUS;  // should never get here.
  }

  inline std::string boundary_type_name( const BoundaryType bt ){
    std::string s;
    switch( bt ){
    case ProcessorBoundary: s="ProcessorBoundary";  break;
    case DomainBoundary   : s="DomainBoundary"   ;  break;
    case PeriodicBoundary : s="PeriodicBoundary" ;  break;
    case NONE             : s="NONE"             ;  break;
    }
    return s;
  }


  template<typename T>
  T& operator<<( T& t, const Faces f ){ t << face_name(f); return t; }

  template<typename T>
  T& operator<<( T& t, const BoundaryType f ){ t << boundary_type_name(f); return t; }

}

#endif /* BUNDLE_FWD_H_ */
