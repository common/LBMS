/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMSMesh_h
#define LBMSMesh_h

#include <vector>
#include <ostream>

#include <lbms/Direction.h>
#include <lbms/Bundle_fwd.h>
#include <lbms/Coordinate.h>

#include <spatialops/structured/IntVec.h>

#include <boost/shared_ptr.hpp>

/**
 *  \file Mesh.h
 *  \author James C. Sutherland
 *  \brief Various tools associated with the mesh for LBMS
 */

namespace LBMS{

  /**
   *  \class Mesh
   *  \brief Manages the mesh for LBMS.
   */
  class Mesh{

  public:

    /**
     *  \brief Construct a Mesh object. This builds Bundle descriptors.
     */
    Mesh( const SpatialOps::IntVec nptsCoarse, ///< Coarse mesh number of points
          const SpatialOps::IntVec nptsFine,   ///< Fine mesh number of points
          const Coordinate& length,            ///< Domain length in each direction
          const bool periX=false,              ///< true if periodic in X
          const bool periY=false,              ///< true if periodic in Y
          const bool periZ=false               ///< true if periodic in Z
    );

    ~Mesh();

    /**
     *  \brief Access the Bundle for the requested Direction.
     */
    BundlePtr bundle( const Direction dir ) const;

    const SpatialOps::IntVec& npts_coarse() const{ return nCoarse_; }
    const SpatialOps::IntVec& npts_fine  () const{ return nFine_; }

    int npts_coarse( const Direction dir ) const{ return nCoarse_[dir]; }
    int npts_fine  ( const Direction dir ) const{ return nFine_[dir]; }

    Coordinate spacing() const;
    Coordinate spacing_coarse() const;

    const Coordinate& length() const{ return length_; }
    double length( const Direction dir ) const{ return length_[dir]; }

    bool is_periodic( const Direction dir ) const;

  private:

    BundlePtr xBundle_, yBundle_, zBundle_;
    const SpatialOps::IntVec nCoarse_, nFine_;
    const Coordinate length_;
    const BoundaryType periX_, periY_, periZ_;
  };

  typedef boost::shared_ptr<Mesh> MeshPtr;

} // namespace LBMS

#endif  // LBMSMesh_h
