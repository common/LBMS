/*
 * test_flux.cpp
 *
 *  Created on: Feb, something, I forget
 *      Author: Derek Cline
 */

#include <fields/Checkpoint.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <mpi/Environment.h>
#include <fields/Fields.h>
#include <lbms/CrossLineFlux.h>
#include <operators/InterpCoarseToFine.h>
#include <test/TestHelper.h>
#include <boost/program_options.hpp>
#include <expression/ExprLib.h>
#include <spatialops/structured/Grid.h>

namespace po=boost::program_options;

using namespace Expr;
using namespace LBMS;
using namespace std;
using SpatialOps::IntVec;

template< typename FieldT>
void fill_flux_field_rand( FieldT& flux ){
  typename FieldT::iterator i = flux.begin();
    for(;i!=flux.end();i++){
      //Sets random number between -0.2 and 0.2 for flux test case
      *i = ( -0.2 ) + (double)rand() / ( (double)RAND_MAX / ( 0.2 - ( -0.2 ) ) );
    }
}

//used for comparing integral values before and after triplet mapping
bool compare_field_integrals( const std::vector<double>& before,
                              const std::vector<double>& after )
{
  for(int i=0; i!=before.size(); i++){
    if( abs(before[i] - after[i]) > 0.05 /*std::numeric_limits<double>::epsilon()*30 */) return 0;
  }
  return 1;
}

template< typename FieldT, typename CoordFieldT >
std::vector<double>
calculate_integral( const FieldT& field,
                    const CoordFieldT& coord,
                    const IntVec extent,
                    const int direc,
                    const double coarseVolExtent )
{
  std::vector<double> integral;
  IntVec location(0,0,0);

  Direction dir0 = Direction(direc);
  Direction dir1 = get_perp1(dir0);
  Direction dir2 = get_perp2(dir0);

    for ( size_t idim1=0; idim1!=(extent[dir1]); ++idim1 ){
      for ( size_t idim2=0; idim2!=(extent[dir2]); ++idim2 ){
        location[dir1] = idim1;
        location[dir2] = idim2;

        const FieldT fieldWindow( get_line_field( field, location ) );
        const CoordFieldT coordWindow ( get_line_field( coord, location ) );

        typename CoordFieldT::const_iterator coordi = coordWindow.begin();
        typename FieldT::const_iterator fieldi = fieldWindow.begin();

        double tempinteg = 0.0;

        for(int k=1; k!=extent[dir0]; ++coordi,++fieldi, ++k){
          tempinteg += ( ( *fieldi + *(fieldi+1) ) * ( *(coordi+1) - *coordi))/2;
        } //Loop across memory window
        integral.push_back(tempinteg/coarseVolExtent);
      } // Perpendicular Lattice Loop 1
    } // Perpendicular Lattice Loop 2
    return integral;
}

int main( int iarg, char* carg[] )
{
  const int nghost = 1;
  IntVec nCoarse, nFine;
  LBMS::Coordinate length;
  std::string name;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(20), "Fine grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(20), "Fine grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(20), "Fine grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(10),"Coarse grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(10),"Coarse grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(10),"Coarse grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "name", po::value<std::string>(&name)->default_value(""),"checkpoint dir tag");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }// end scope

  try{

    Environment::setup(iarg,carg);

    const MeshPtr mesh( new Mesh(nCoarse,nFine,length) ); // assumes not periodic
    Environment::set_topology( mesh );

    const BundlePtr xbundle = Environment::bundle(LBMS::XDIR);
    const BundlePtr ybundle = Environment::bundle(LBMS::YDIR);
    const BundlePtr zbundle = Environment::bundle(LBMS::ZDIR);

    BundlePtrVec bundles;
    bundles.push_back( xbundle );
    bundles.push_back( ybundle );
    bundles.push_back( zbundle );

    Expr::FMLMap fmls;

    BOOST_FOREACH( BundlePtr b, bundles ){ fmls[b->id()] = b->get_field_manager_list(); }

    FieldManagerList& fmlx = *fmls[xbundle->id()];
    FieldManagerList& fmly = *fmls[ybundle->id()];
    FieldManagerList& fmlz = *fmls[zbundle->id()];

    //I don't like this
    FieldManagerList& Sfml = *fmls[xbundle->id()];

    FieldMgrSelector<XVolField>::type& xvolfm = fmlx.field_manager<XVolField>();
    FieldMgrSelector<YVolField>::type& yvolfm = fmly.field_manager<YVolField>();
    FieldMgrSelector<ZVolField>::type& zvolfm = fmlz.field_manager<ZVolField>();

    FieldMgrSelector<XSurfXField>::type& xsurfxfm = fmlx.field_manager<XSurfXField>();
    FieldMgrSelector<XSurfYField>::type& xsurfyfm = fmlx.field_manager<XSurfYField>();
    FieldMgrSelector<XSurfZField>::type& xsurfzfm = fmlx.field_manager<XSurfZField>();

    FieldMgrSelector<YSurfXField>::type& ysurfxfm = fmly.field_manager<YSurfXField>();
    FieldMgrSelector<YSurfYField>::type& ysurfyfm = fmly.field_manager<YSurfYField>();
    FieldMgrSelector<YSurfZField>::type& ysurfzfm = fmly.field_manager<YSurfZField>();

    FieldMgrSelector<ZSurfXField>::type& zsurfxfm = fmlz.field_manager<ZSurfXField>();
    FieldMgrSelector<ZSurfYField>::type& zsurfyfm = fmlz.field_manager<ZSurfYField>();
    FieldMgrSelector<ZSurfZField>::type& zsurfzfm = fmlz.field_manager<ZSurfZField>();

    FieldMgrSelector<SpatialOps::SVolField>::type&     svolfm = Sfml.field_manager<SpatialOps::SVolField  >();
    FieldMgrSelector<SpatialOps::SSurfXField>::type& ssurfxfm = Sfml.field_manager<SpatialOps::SSurfXField>();
    FieldMgrSelector<SpatialOps::SSurfYField>::type& ssurfyfm = Sfml.field_manager<SpatialOps::SSurfYField>();
    FieldMgrSelector<SpatialOps::SSurfZField>::type& ssurfzfm = Sfml.field_manager<SpatialOps::SSurfZField>();

    TagList xtags, ytags, ztags, vtags, stags;
    TagSet tags;
    xtags.clear(), ytags.clear(), ztags.clear(), vtags.clear(), stags.clear(), tags.clear();

    const Tag xfluxt   ("xflux",   STATE_NONE), yfluxt   ("yflux",   STATE_NONE), zfluxt   ("zflux",   STATE_NONE), volt   ("vol",    STATE_NONE);
    const Tag xsynfluxt("xsynflux",STATE_NONE), ysynfluxt("ysynflux",STATE_NONE), zsynfluxt("zsynflux",STATE_NONE), synvolt("synvol", STATE_NONE);
    const Tag xcoordt  ("coordx",  STATE_NONE), ycoordt  ("coordy",  STATE_NONE), zcoordt  ("coordz",  STATE_NONE);
    const Tag xsft     ("xsf",     STATE_NONE), ysft     ("ysf",     STATE_NONE), zsft     ("zsf",     STATE_NONE), volsft ("volsf",  STATE_NONE);
    const Tag xcoarset ("xcoarse", STATE_NONE), ycoarset ("ycoarse", STATE_NONE), zcoarset ("zcoarse", STATE_NONE), coarset("coarse", STATE_NONE);

    xtags.push_back(xfluxt);    ytags.push_back(yfluxt);    ztags.push_back(zfluxt);    vtags.push_back(volt);
    xtags.push_back(xsynfluxt); ytags.push_back(ysynfluxt); ztags.push_back(zsynfluxt); vtags.push_back(synvolt);
    xtags.push_back(xcoordt);   ytags.push_back(ycoordt);   ztags.push_back(zcoordt);
    xtags.push_back(xsft);      ytags.push_back(ysft);      ztags.push_back(zsft);      vtags.push_back(volsft);
    xtags.push_back(xcoarset);  ytags.push_back(ycoarset);  ztags.push_back(zcoarset);  vtags.push_back(coarset);

    //tags for checkpoint writing
    tags.insert( xsynfluxt ); tags.insert( ysynfluxt ); tags.insert( zsynfluxt ), tags.insert( synvolt );

    xvolfm.register_field( xcoordt, nghost );
    xvolfm.register_field( ycoordt, nghost );
    xvolfm.register_field( zcoordt, nghost );

    yvolfm.register_field( xcoordt, nghost );
    yvolfm.register_field( ycoordt, nghost );
    yvolfm.register_field( zcoordt, nghost );

    zvolfm.register_field( xcoordt, nghost );
    zvolfm.register_field( ycoordt, nghost );
    zvolfm.register_field( zcoordt, nghost );

    svolfm.register_field( xcoordt, nghost );
    svolfm.register_field( ycoordt, nghost );
    svolfm.register_field( zcoordt, nghost );

    BOOST_FOREACH( const Expr::Tag& tag, xtags ){
      xsurfxfm.register_field( tag, nghost );
      ysurfxfm.register_field( tag, nghost );
      zsurfxfm.register_field( tag, nghost );
      ssurfxfm.register_field( tag, nghost );
      ssurfyfm.register_field( tag, nghost );
      ssurfzfm.register_field( tag, nghost );
    }
    BOOST_FOREACH( const Expr::Tag& tag, ytags ){
      xsurfyfm.register_field( tag, nghost );
      zsurfyfm.register_field( tag, nghost );
      ysurfyfm.register_field( tag, nghost );
      ssurfxfm.register_field( tag, nghost );
      ssurfyfm.register_field( tag, nghost );
      ssurfzfm.register_field( tag, nghost );
    }
    BOOST_FOREACH( const Expr::Tag& tag, ztags ){
      xsurfzfm.register_field( tag, nghost );
      ysurfzfm.register_field( tag, nghost );
      zsurfzfm.register_field( tag, nghost );
      ssurfxfm.register_field( tag, nghost );
      ssurfyfm.register_field( tag, nghost );
      ssurfzfm.register_field( tag, nghost );
    }
    BOOST_FOREACH( const Expr::Tag& tag, vtags ){
      xvolfm.register_field( tag, nghost );
      yvolfm.register_field( tag, nghost );
      zvolfm.register_field( tag, nghost );
      svolfm.register_field( tag, nghost );
    }

    xvolfm.allocate_fields( boost::cref(xbundle->get_field_info() ) );
    yvolfm.allocate_fields( boost::cref(ybundle->get_field_info() ) );
    zvolfm.allocate_fields( boost::cref(zbundle->get_field_info() ) );

    svolfm.allocate_fields( boost::cref(xbundle->get_field_info() ) );
    svolfm.allocate_fields( boost::cref(ybundle->get_field_info() ) );
    svolfm.allocate_fields( boost::cref(zbundle->get_field_info() ) );

    xsurfxfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    xsurfyfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    xsurfzfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );

    ysurfxfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    ysurfyfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    ysurfzfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );

    zsurfxfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );
    zsurfyfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );
    zsurfzfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );

    ssurfxfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    ssurfyfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    ssurfzfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );

    //xfluxy is a flux across the y surface that lives on the x-bundle
    XSurfYField& xfluxy = xsurfyfm.field_ref(yfluxt);
    XSurfZField& xfluxz = xsurfzfm.field_ref(zfluxt);
    YSurfXField& yfluxx = ysurfxfm.field_ref(xfluxt);
    YSurfZField& yfluxz = ysurfzfm.field_ref(zfluxt);
    ZSurfXField& zfluxx = zsurfxfm.field_ref(xfluxt);
    ZSurfYField& zfluxy = zsurfyfm.field_ref(yfluxt);

    XVolField& xvol = xvolfm.field_ref(volt);
    YVolField& yvol = yvolfm.field_ref(volt);
    ZVolField& zvol = zvolfm.field_ref(volt);

    CrossLineFlux<LBMS::XVolField, SpatialOps::SVolField> xvolcrosslinefluxobject(xbundle);
    CrossLineFlux<LBMS::YVolField, SpatialOps::SVolField> yvolcrosslinefluxobject(ybundle);
    CrossLineFlux<LBMS::ZVolField, SpatialOps::SVolField> zvolcrosslinefluxobject(zbundle);

    CrossLineFlux<SpatialOps::FaceTypes<LBMS::YVolField>::XFace, SpatialOps::SSurfXField> yxcrosslinefluxobject(ybundle);
    CrossLineFlux<SpatialOps::FaceTypes<LBMS::ZVolField>::XFace, SpatialOps::SSurfXField> zxcrosslinefluxobject(zbundle);
    CrossLineFlux<SpatialOps::FaceTypes<LBMS::XVolField>::YFace, SpatialOps::SSurfYField> xycrosslinefluxobject(xbundle);
    CrossLineFlux<SpatialOps::FaceTypes<LBMS::ZVolField>::YFace, SpatialOps::SSurfYField> zycrosslinefluxobject(zbundle);
    CrossLineFlux<SpatialOps::FaceTypes<LBMS::XVolField>::ZFace, SpatialOps::SSurfZField> xzcrosslinefluxobject(xbundle);
    CrossLineFlux<SpatialOps::FaceTypes<LBMS::YVolField>::ZFace, SpatialOps::SSurfZField> yzcrosslinefluxobject(ybundle);


    SpatialOps::SVolField&  volsin = svolfm.field_ref(coarset);
    SpatialOps::SSurfXField& xsin  = ssurfxfm.field_ref(xcoarset);
    SpatialOps::SSurfYField& ysin  = ssurfyfm.field_ref(ycoarset);
    SpatialOps::SSurfZField& zsin  = ssurfzfm.field_ref(zcoarset);

    SpatialOps::SVolField& xcoordvol = svolfm.field_ref(xcoordt);
    SpatialOps::SVolField& ycoordvol = svolfm.field_ref(ycoordt);
    SpatialOps::SVolField& zcoordvol = svolfm.field_ref(zcoordt);

    {

      const Coordinate len( xbundle->length(XDIR), xbundle->length(YDIR), xbundle->length(ZDIR) );
      const SpatialOps::Grid grid( xbundle->ncoarse(), len );
      grid.set_coord<SpatialOps::XDIR>( xcoordvol );
      grid.set_coord<SpatialOps::YDIR>( ycoordvol );
      grid.set_coord<SpatialOps::ZDIR>( zcoordvol );

  //    const Coordinate origin = xbundle->origin();
      xcoordvol <<= xcoordvol;
      ycoordvol <<= ycoordvol;
      zcoordvol <<= zcoordvol;
    }
//    LBMS::set_coarse_coords( xcoordvol, ycoordvol, zcoordvol, xbundle );
    volsin <<= sin( 90 + xcoordvol + ycoordvol + zcoordvol ) * 5;

    SpatialOps::SSurfXField& xcoordsurfx = ssurfxfm.field_ref(xcoordt);
    SpatialOps::SSurfXField& ycoordsurfx = ssurfxfm.field_ref(ycoordt);
    SpatialOps::SSurfXField& zcoordsurfx = ssurfxfm.field_ref(zcoordt);
    LBMS::set_coord_values( xcoordsurfx, ycoordsurfx, zcoordsurfx, xbundle );
    xsin <<= sin( 90 + xcoordsurfx ) * 5;

    SpatialOps::SSurfYField& xcoordsurfy = ssurfyfm.field_ref(xcoordt);
    SpatialOps::SSurfYField& ycoordsurfy = ssurfyfm.field_ref(ycoordt);
    SpatialOps::SSurfYField& zcoordsurfy = ssurfyfm.field_ref(zcoordt);
    LBMS::set_coord_values( xcoordsurfy, ycoordsurfy, zcoordsurfy, ybundle );
    ysin <<= sin( 90 + 2 * ycoordsurfy ) * 4;

    SpatialOps::SSurfZField& xcoordsurfz = ssurfzfm.field_ref(xcoordt);
    SpatialOps::SSurfZField& ycoordsurfz = ssurfzfm.field_ref(ycoordt);
    SpatialOps::SSurfZField& zcoordsurfz = ssurfzfm.field_ref(zcoordt);
    LBMS::set_coord_values( xcoordsurfz, ycoordsurfz, zcoordsurfz, zbundle );
    zsin <<= sin( 90 + 3 * zcoordsurfz ) * 3;

    fill_flux_field_rand<XVolField>(xvol);
    fill_flux_field_rand<YVolField>(yvol);
    fill_flux_field_rand<ZVolField>(zvol);

    fill_flux_field_rand<XSurfYField>(xfluxy);
    fill_flux_field_rand<XSurfZField>(xfluxz);
    fill_flux_field_rand<YSurfXField>(yfluxx);
    fill_flux_field_rand<YSurfZField>(yfluxz);
    fill_flux_field_rand<ZSurfXField>(zfluxx);
    fill_flux_field_rand<ZSurfYField>(zfluxy);

    XSurfXField& xsynfluxx = xsurfxfm.field_ref(xsynfluxt);
    XSurfYField& xsynfluxy = xsurfyfm.field_ref(ysynfluxt);
    XSurfZField& xsynfluxz = xsurfzfm.field_ref(zsynfluxt);
    YSurfXField& ysynfluxx = ysurfxfm.field_ref(xsynfluxt);
    YSurfYField& ysynfluxy = ysurfyfm.field_ref(ysynfluxt);
    YSurfZField& ysynfluxz = ysurfzfm.field_ref(zsynfluxt);
    ZSurfXField& zsynfluxx = zsurfxfm.field_ref(xsynfluxt);
    ZSurfYField& zsynfluxy = zsurfyfm.field_ref(ysynfluxt);
    ZSurfZField& zsynfluxz = zsurfzfm.field_ref(zsynfluxt);
    SpatialOps::SSurfXField& ssynfluxx = ssurfxfm.field_ref(xsynfluxt);
    SpatialOps::SSurfYField& ssynfluxy = ssurfyfm.field_ref(ysynfluxt);
    SpatialOps::SSurfZField& ssynfluxz = ssurfzfm.field_ref(zsynfluxt);

    XVolField& xsynvol = xvolfm.field_ref(synvolt);
    YVolField& ysynvol = yvolfm.field_ref(synvolt);
    ZVolField& zsynvol = zvolfm.field_ref(synvolt);

    xvolcrosslinefluxobject.create_synthetic_flux( xvol, volsin, xsynvol );
    yvolcrosslinefluxobject.create_synthetic_flux( yvol, volsin, ysynvol );
    zvolcrosslinefluxobject.create_synthetic_flux( zvol, volsin, zsynvol );

    xycrosslinefluxobject.create_synthetic_flux( xfluxy, ysin, xsynfluxy );
    xzcrosslinefluxobject.create_synthetic_flux( xfluxz, zsin, xsynfluxz );
    yxcrosslinefluxobject.create_synthetic_flux( yfluxx, xsin, ysynfluxx );
    yzcrosslinefluxobject.create_synthetic_flux( yfluxz, zsin, ysynfluxz );
    zxcrosslinefluxobject.create_synthetic_flux( zfluxx, xsin, zsynfluxx );
    zycrosslinefluxobject.create_synthetic_flux( zfluxy, ysin, zsynfluxy );

    XSurfYField& xsinfiney = xsurfyfm.field_ref(ysft);
    XSurfZField& xsinfinez = xsurfzfm.field_ref(zsft);
    YSurfXField& ysinfinex = ysurfxfm.field_ref(xsft);
    YSurfZField& ysinfinez = ysurfzfm.field_ref(zsft);
    ZSurfXField& zsinfinex = zsurfxfm.field_ref(xsft);
    ZSurfYField& zsinfiney = zsurfyfm.field_ref(ysft);

    XVolField& xvolfine = xvolfm.field_ref(volsft);
    YVolField& yvolfine = yvolfm.field_ref(volsft);
    ZVolField& zvolfine = zvolfm.field_ref(volsft);

    LBMS::InterpCoarseToFine<XVolField> xvolinterp_( *xbundle );
    xvolinterp_.apply_to_field( volsin, xvolfine );
    LBMS::InterpCoarseToFine<YVolField> yvolinterp_( *ybundle );
    yvolinterp_.apply_to_field( volsin, yvolfine );
    LBMS::InterpCoarseToFine<ZVolField> zvolinterp_( *zbundle );
    zvolinterp_.apply_to_field( volsin, zvolfine );

    LBMS::InterpCoarseToFine<YSurfXField> yxinterp_( *ybundle );
    yxinterp_.apply_to_field( xsin, ysinfinex );
    LBMS::InterpCoarseToFine<YSurfZField> yzinterp_( *ybundle );
    yzinterp_.apply_to_field( zsin, ysinfinez );

    LBMS::InterpCoarseToFine<ZSurfXField> zxinterp_( *zbundle );
    zxinterp_.apply_to_field( xsin, zsinfinex );
    LBMS::InterpCoarseToFine<ZSurfYField> zyinterp_( *zbundle );
    zyinterp_.apply_to_field( ysin, zsinfiney );

    LBMS::InterpCoarseToFine<XSurfYField> xyinterp_( *xbundle );
    xyinterp_.apply_to_field( ysin, xsinfiney );
    LBMS::InterpCoarseToFine<XSurfZField> xzinterp_( *xbundle );
    xzinterp_.apply_to_field( zsin, xsinfinez );

    std::vector<double> vector1,  vector2,  vector3,  vector4,  vector5,  vector6,  vector7,  vector8,  vector9;
    std::vector<double> svector1, svector2, svector3, svector4, svector5, svector6, svector7, svector8, svector9;

    IntVec xextent ( nFine[0],   nCoarse[1], nCoarse[2] );
    IntVec yextent ( nCoarse[0], nFine[1],   nCoarse[2] );
    IntVec zextent ( nCoarse[0], nCoarse[1], nFine[2]   );

    XVolField& xcoordx = xvolfm.field_ref(xcoordt);
    XVolField& xcoordy = xvolfm.field_ref(ycoordt);
    XVolField& xcoordz = xvolfm.field_ref(zcoordt);

    YVolField& ycoordx = yvolfm.field_ref(xcoordt);
    YVolField& ycoordy = yvolfm.field_ref(ycoordt);
    YVolField& ycoordz = yvolfm.field_ref(zcoordt);

    ZVolField& zcoordx = zvolfm.field_ref(xcoordt);
    ZVolField& zcoordy = zvolfm.field_ref(ycoordt);
    ZVolField& zcoordz = zvolfm.field_ref(zcoordt);

    LBMS::set_coord_values( xcoordx, xcoordy, xcoordz, xbundle );
    LBMS::set_coord_values( ycoordx, ycoordy, ycoordz, ybundle );
    LBMS::set_coord_values( zcoordx, zcoordy, zcoordz, zbundle );

    IntVec CoarseVolExtent = nFine / nCoarse;

    vector1  = calculate_integral<XSurfYField,XVolField>(xsynfluxy, xcoordx, xextent, LBMS::XDIR, 1.0 );
    svector1 = calculate_integral<XSurfYField,XVolField>(xsinfiney, xcoordx, xextent, LBMS::XDIR, CoarseVolExtent[0] );
    vector3  = calculate_integral<YSurfXField,YVolField>(ysynfluxx, ycoordy, yextent, LBMS::YDIR, 1.0 );
    svector3 = calculate_integral<YSurfXField,YVolField>(ysinfinex, ycoordy, yextent, LBMS::YDIR, CoarseVolExtent[1] );
    vector5  = calculate_integral<ZSurfXField,ZVolField>(zsynfluxx, zcoordz, zextent, LBMS::ZDIR, 1.0 );
    svector5 = calculate_integral<ZSurfXField,ZVolField>(zsinfinex, zcoordz, zextent, LBMS::ZDIR, CoarseVolExtent[2] );

    vector2  = calculate_integral<XSurfZField,XVolField>(xsynfluxz, xcoordx, xextent, LBMS::XDIR, 1.0 );
    svector2 = calculate_integral<XSurfZField,XVolField>(xsinfinez, xcoordx, xextent, LBMS::XDIR, CoarseVolExtent[0] );
    vector4  = calculate_integral<YSurfZField,YVolField>(ysynfluxz, ycoordy, yextent, LBMS::YDIR, 1.0 );
    svector4 = calculate_integral<YSurfZField,YVolField>(ysinfinez, ycoordy, yextent, LBMS::YDIR, CoarseVolExtent[1] );
    vector6  = calculate_integral<ZSurfYField,ZVolField>(zsynfluxy, zcoordz, zextent, LBMS::ZDIR, 1.0 );
    svector6 = calculate_integral<ZSurfYField,ZVolField>(zsinfiney, zcoordz, zextent, LBMS::ZDIR, CoarseVolExtent[2] );

    //Because we are not coarsening but directly creating a coarsened field, the volume integrals
    //of the coarse fields must be appropriately scaled to a coarse volume.
    vector7  = calculate_integral<XVolField,XVolField>(xsynvol,  xcoordx, xextent, LBMS::XDIR, 1.0 );
    svector7 = calculate_integral<XVolField,XVolField>(xvolfine, xcoordx, xextent, LBMS::XDIR, CoarseVolExtent[0] );
    vector8  = calculate_integral<YVolField,YVolField>(ysynvol,  ycoordy, yextent, LBMS::YDIR, 1.0 );
    svector8 = calculate_integral<YVolField,YVolField>(yvolfine, ycoordy, yextent, LBMS::YDIR, CoarseVolExtent[1] );
    vector9  = calculate_integral<ZVolField,ZVolField>(zsynvol,  zcoordz, zextent, LBMS::ZDIR, 1.0 );
    svector9 = calculate_integral<ZVolField,ZVolField>(zvolfine, zcoordz, zextent, LBMS::ZDIR, CoarseVolExtent[2] );

    LBMS::Checkpoint visualization( bundles, tags, fmls );
    visualization.write_checkpoint( "crosslineflux" );

    TestHelper status;
    status( compare_field_integrals( vector1 , svector1 ), "vector 1" );
    status( compare_field_integrals( vector2 , svector2 ), "vector 2" );
    status( compare_field_integrals( vector3 , svector3 ), "vector 3" );
    status( compare_field_integrals( vector4 , svector4 ), "vector 4" );
    status( compare_field_integrals( vector5 , svector5 ), "vector 5" );
    status( compare_field_integrals( vector6 , svector6 ), "vector 6" );
    status( compare_field_integrals( vector7 , svector7 ), "vector 7" );
    status( compare_field_integrals( vector8 , svector8 ), "vector 8" );
    status( compare_field_integrals( vector9 , svector9 ), "vector 9" );

    cout << endl;

    if( status.ok() ){
      return 0; // for successful exit.
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << endl;
  }
  return -1;
}
