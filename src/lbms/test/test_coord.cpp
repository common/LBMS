#include <lbms/Coordinate.h>
#include <test/TestHelper.h>

#include <stdexcept>
#include <iostream>
using std::cout;
using std::endl;

using namespace LBMS;

int main()
{
  Coordinate x1( 0, 0, 0 );
  Coordinate x2( 1, 2, 3 );
  Coordinate x3( 4, 6, 8 );

  TestHelper status(true);

  status( x3-x2 == Coordinate(3,4,5), "-" );
  status( x1-x2 == Coordinate(-1,-2,-3), "-" );

  status( x3+x2 == Coordinate(5,8,11), "+" );

  status( x1*4 == x1, "*");
  status( x3*2 == Coordinate(8,12,16), "*" );

  status( x1/4 == x1, "/");
  status( x3/2 == Coordinate(2,3,4), "*,/" );

  if( status.ok() ){
    cout << "PASS" << endl;
    return 0;
  }
  cout << "FAIL" << endl;
  return -1;
}
