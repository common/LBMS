#include <test/TestHelper.h>

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <sstream>
using std::cout;
using std::endl;

//--- LBMS Includes ---//
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>

using namespace LBMS;
using SpatialOps::IntVec;

bool test_coord_x()
{
  const IntVec npts(10,10,10);
  const IntVec zero(0,0,0);
  const Coordinate length( 1,2,3 );
  const BundlePtr bx( new Bundle(XDIR,1,npts,length,zero,zero) );
  const SpatialOps::GhostData ghost(1);
  const Coordinate spc = bx->spacing();

  TestHelper status(false);

  status( bx->cell_coord(IntVec(0,0,0)) == spc*0.5, "cell_coord (0,0,0)" );

  status( Coordinate(0,0,0)-spc*0.5 == coord<XVolField>( IntVec(0,0,0), ghost, bx ), "coord<XVol> (0,0,0)" );
  status( Coordinate(-spc[0],-0.5*spc[1],-0.5*spc[2]) == coord<XSurfXField>( IntVec(0,0,0), ghost, bx ), "coord<XSX> (0,0,0)" );
  status( Coordinate(-0.5*spc[0],-spc[1],-0.5*spc[2]) == coord<XSurfYField>( IntVec(0,0,0), ghost, bx ), "coord<XSY> (0,0,0)" );
  status( Coordinate(-0.5*spc[0],-0.5*spc[1],-spc[2]) == coord<XSurfZField>( IntVec(0,0,0), ghost, bx ), "coord<XSZ> (0,0,0)" );

  status( Coordinate(0,0,0)+spc*0.5 == coord<XVolField>( IntVec(1,1,1), ghost, bx ), "coord<XVol> (1,1,1)" );
  status( Coordinate(0,0.5*spc[1],0.5*spc[2]) == coord<XSurfXField>( IntVec(1,1,1), ghost, bx ), "coord<XSX> (1,1,1)" );
  status( Coordinate(0.5*spc[0],0,0.5*spc[2]) == coord<XSurfYField>( IntVec(1,1,1), ghost, bx ), "coord<XSY> (1,1,1)" );
  status( Coordinate(0.5*spc[0],0.5*spc[1],0) == coord<XSurfZField>( IntVec(1,1,1), ghost, bx ), "coord<XSZ> (1,1,1)" );

  status( index<  XVolField>( coord<  XVolField>(IntVec(1,1,1),ghost,bx), ghost, bx ) == IntVec(1,1,1), "  XVolField round trip ix->coord->ix (1,1,1)" );
  status( index<XSurfXField>( coord<XSurfXField>(IntVec(1,1,1),ghost,bx), ghost, bx ) == IntVec(1,1,1), "XSurfXField round trip ix->coord->ix (1,1,1)" );
  status( index<XSurfYField>( coord<XSurfYField>(IntVec(1,1,1),ghost,bx), ghost, bx ) == IntVec(1,1,1), "XSurfYField round trip ix->coord->ix (1,1,1)" );
  status( index<XSurfZField>( coord<XSurfZField>(IntVec(1,1,1),ghost,bx), ghost, bx ) == IntVec(1,1,1), "XSurfZField round trip ix->coord->ix (1,1,1)" );

  return status.ok();
}

bool test_coord_y()
{
  const IntVec npts(10,10,10);
  const IntVec zero(0,0,0);
  const Coordinate length( 1,2,3 );
  const BundlePtr by( new Bundle(YDIR,1,npts,length,zero,zero) );
  const SpatialOps::GhostData ghost(1);
  const Coordinate spc = by->spacing();

  TestHelper status(false);

  status( by->cell_coord(IntVec(0,0,0)) == spc*0.5, "cell_coord (0,0,0)" );

  status( Coordinate(0,0,0)-spc*0.5 == coord<YVolField>( IntVec(0,0,0), ghost, by ), "coord<YVol> (0,0,0)" );
  status( Coordinate(-spc[0],-0.5*spc[1],-0.5*spc[2]) == coord<YSurfXField>( IntVec(0,0,0), ghost, by ), "coord<YSX> (0,0,0)" );
  status( Coordinate(-0.5*spc[0],-spc[1],-0.5*spc[2]) == coord<YSurfYField>( IntVec(0,0,0), ghost, by ), "coord<YSY> (0,0,0)" );
  status( Coordinate(-0.5*spc[0],-0.5*spc[1],-spc[2]) == coord<YSurfZField>( IntVec(0,0,0), ghost, by ), "coord<YSZ> (0,0,0)" );

  status( Coordinate(0,0,0)+spc*0.5 == coord<YVolField>( IntVec(1,1,1), ghost, by ), "coord<YVol> (1,1,1)" );
  status( Coordinate(0,0.5*spc[1],0.5*spc[2]) == coord<YSurfXField>( IntVec(1,1,1), ghost, by ), "coord<YSX> (1,1,1)" );
  status( Coordinate(0.5*spc[0],0,0.5*spc[2]) == coord<YSurfYField>( IntVec(1,1,1), ghost, by ), "coord<YSY> (1,1,1)" );
  status( Coordinate(0.5*spc[0],0.5*spc[1],0) == coord<YSurfZField>( IntVec(1,1,1), ghost, by ), "coord<YSZ> (1,1,1)" );

  status( index<  YVolField>( coord<  YVolField>(IntVec(1,1,1),ghost,by), ghost, by ) == IntVec(1,1,1), "  YVolField round trip ix->coord->ix (1,1,1)" );
  status( index<YSurfXField>( coord<YSurfXField>(IntVec(1,1,1),ghost,by), ghost, by ) == IntVec(1,1,1), "YSurfXField round trip ix->coord->ix (1,1,1)" );
  status( index<YSurfYField>( coord<YSurfYField>(IntVec(1,1,1),ghost,by), ghost, by ) == IntVec(1,1,1), "YSurfYField round trip ix->coord->ix (1,1,1)" );
  status( index<YSurfZField>( coord<YSurfZField>(IntVec(1,1,1),ghost,by), ghost, by ) == IntVec(1,1,1), "YSurfZField round trip ix->coord->ix (1,1,1)" );

  return status.ok();
}

int main( int iarg, char* carg[] )
{
  const IntVec nCoarse( 3, 5, 4 );
  const IntVec nFine( 12, 20, 24 );

  try{

    const LBMS::Mesh mesh( nCoarse, nFine, Coordinate(1,1,1) );

    const double dxf=1.0/nFine[0], dyf=1.0/nFine[1], dzf=1.0/nFine[2];
    const double dxc=1.0/nCoarse[0], dyc=1.0/nCoarse[1], dzc=1.0/nCoarse[2];

    TestHelper status(true);

    status( test_coord_x(), "x-bundle coordinates" );
    status( test_coord_y(), "y-bundle coordinates" );

//    status( test_splitting( mesh, dxf, dyf, dzf, dxc, dyc, dzc ), "splitting" );

    const BundlePtr& xbundle = mesh.bundle(LBMS::XDIR);
    const BundlePtr& ybundle = mesh.bundle(LBMS::YDIR);
    const BundlePtr& zbundle = mesh.bundle(LBMS::ZDIR);

    {
      std::ostringstream ostr;
      xbundle->write_xml( ostr );
      std::istringstream istr(ostr.str());
      BundlePtr b = create_bundle_from_xml( istr );
      status( b->npts() == xbundle->npts() &&
              b->length() == xbundle->length() &&
              b->spacing() == xbundle->spacing(),
              "bundle XML IO" );
    }

    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;
}
