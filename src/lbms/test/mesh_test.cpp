#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>

#include <test/TestHelper.h>

#include <iostream>
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po=boost::program_options;

using SpatialOps::IntVec;

bool test_2d()
{
  using namespace LBMS;
  TestHelper status(false);
  const Coordinate length(1,1,1);
  {
    const Mesh mesh( IntVec(4,4,1), IntVec(100,100,1), length );
    status( mesh.bundle(XDIR)->npts() == IntVec(100,4,1) );
    status( mesh.bundle(YDIR)->npts() == IntVec(4,100,1) );
  }
  {
    const Mesh mesh( IntVec(4,1,4), IntVec(100,1,100), length );
    status( mesh.bundle(XDIR)->npts() == IntVec(100,1,4) );
    status( mesh.bundle(ZDIR)->npts() == IntVec(4,1,100) );
  }
  {
    const Mesh mesh( IntVec(1,4,4), IntVec(1,100,100), length );
    status( mesh.bundle(YDIR)->npts() == IntVec(1,100,4) );
    status( mesh.bundle(ZDIR)->npts() == IntVec(1,4,100) );
  }
  return status.ok();
}

bool test_1d()
{
  using namespace LBMS;
  const Coordinate length(1,1,1);
  TestHelper status(false);
  {
    const Mesh mesh( IntVec(4,1,1), IntVec(100,1,1), length );
    status( mesh.bundle(XDIR)->npts() == IntVec(100,1,1) );
  }
  {
    const Mesh mesh( IntVec(1,4,1), IntVec(1,100,1), length );
    status( mesh.bundle(YDIR)->npts() == IntVec(1,100,1) );
  }
  {
    const Mesh mesh( IntVec(1,1,4), IntVec(1,1,100), length );
    status( mesh.bundle(ZDIR)->npts() == IntVec(1,1,100) );
  }
  return status.ok();
}

bool flat_ix_test( const LBMS::BundlePtr b )
{
  TestHelper status(false);

  const int nx = b->npts(LBMS::XDIR);
  const int ny = b->npts(LBMS::YDIR);
  const int nz = b->npts(LBMS::ZDIR);

  const IntVec ijk1( 0,    0,    0    );
  const IntVec ijk2( nx/2, 0,    0    );
  const IntVec ijk3( 0,    ny/2, 0    );
  const IntVec ijk4( nx/2, ny/2, 0    );
  const IntVec ijk5( 0,    0,    nz/2 );
  const IntVec ijk6( nx/2, 0,    nz/2 );
  const IntVec ijk7( 0,    ny/2, nz/2 );
  const IntVec ijk8( nx/2, ny/2, nz/2 );

  const std::string bname = LBMS::get_dir_name( b->dir() );

  if( b->npts(LBMS::XDIR)>1 ){
    status( b->flat2ijk( b->ijk2flat( ijk1 ) ) == ijk1, bname+" ijk/flat transformations (1)" );
    status( b->flat2ijk( b->ijk2flat( ijk2 ) ) == ijk2, bname+" ijk/flat transformations (2)" );
    if( b->npts(LBMS::YDIR)>1 ){
      status( b->flat2ijk( b->ijk2flat( ijk3 ) ) == ijk3, bname+" ijk/flat transformations (3)" );
      status( b->flat2ijk( b->ijk2flat( ijk4 ) ) == ijk4, bname+" ijk/flat transformations (4)" );
      if( b->npts(LBMS::ZDIR)>1 ){
        status( b->flat2ijk( b->ijk2flat( ijk5 ) ) == ijk5, bname+" ijk/flat transformations (5)" );
        status( b->flat2ijk( b->ijk2flat( ijk6 ) ) == ijk6, bname+" ijk/flat transformations (6)" );
        status( b->flat2ijk( b->ijk2flat( ijk7 ) ) == ijk7, bname+" ijk/flat transformations (7)" );
        status( b->flat2ijk( b->ijk2flat( ijk8 ) ) == ijk8, bname+" ijk/flat transformations (8)" );
      }
    }
  }
  else if( b->npts(LBMS::YDIR)>1 ){
    status( b->flat2ijk( b->ijk2flat( ijk1 ) ) == ijk1, bname+" ijk/flat transformations (1)" );
    status( b->flat2ijk( b->ijk2flat( ijk3 ) ) == ijk3, bname+" ijk/flat transformations (3)" );
    if( b->npts(LBMS::ZDIR)>1 ){
      status( b->flat2ijk( b->ijk2flat( ijk7 ) ) == ijk7, bname+" ijk/flat transformations (7)" );
    }
  }
  else if( b->npts(LBMS::ZDIR) > 1 ){
    status( b->flat2ijk( b->ijk2flat( ijk1 ) ) == ijk1, bname+" ijk/flat transformations (1)" );
    status( b->flat2ijk( b->ijk2flat( ijk5 ) ) == ijk5, bname+" ijk/flat transformations (5)" );
  }
  return status.ok();
}

//====================================================================

bool test_coarse_fine()
{
  TestHelper status(false);
  const IntVec zero(0,0,0);
  {
    LBMS::Bundle b( LBMS::XDIR, 2, IntVec(10, 12, 16), LBMS::Coordinate(1,1,1), zero, zero );
    status( b.fine_to_coarse(IntVec(0, 6, 7)) == IntVec(0, 6, 7) );
    status( b.fine_to_coarse(IntVec(4, 1, 7)) == IntVec(0, 1, 7) );
    status( b.fine_to_coarse(IntVec(5,11,15)) == IntVec(1,11,15) );
    status( b.fine_to_coarse(IntVec(9, 0, 0)) == IntVec(1, 0, 0) );
  }
  {
    LBMS::Bundle b( LBMS::YDIR, 2, IntVec(10, 12, 16), LBMS::Coordinate(1,1,1), zero, zero );
    status( b.fine_to_coarse(IntVec(0,0,0)) == IntVec(0,0,0) );
    status( b.fine_to_coarse(IntVec(0,5,7)) == IntVec(0,0,7) );
    status( b.fine_to_coarse(IntVec(4,6,7)) == IntVec(4,1,7) );
    status( b.fine_to_coarse(IntVec(5,11,7)) == IntVec(5,1,7) );
    status( b.fine_to_coarse(IntVec(9,11,15)) == IntVec(9,1,15) );
  }
  {
    LBMS::Bundle b( LBMS::ZDIR, 2, IntVec(10, 12, 16), LBMS::Coordinate(1,1,1), zero, zero );
    status( b.fine_to_coarse(IntVec(0, 5, 7)) == IntVec(0, 5,0) );
    status( b.fine_to_coarse(IntVec(4, 6, 8)) == IntVec(4, 6,1) );
    status( b.fine_to_coarse(IntVec(5,11,15)) == IntVec(5,11,1) );
  }
  return status.ok();
}

//====================================================================

int main( int iarg, char* carg[] )
{
  IntVec nCoarse, nFine;
  LBMS::Coordinate length;

  SpatialOps::GhostData ghost(1);

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(50), "Fine (ODT) grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(50), "Fine (ODT) grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(50), "Fine (ODT) grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(5),"Coarse (LES) grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(5),"Coarse (LES) grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(5),"Coarse (LES) grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }

  using SpatialOps::IntVec;
  const LBMS::Mesh mesh( nCoarse, nFine, length );

  const int    nx=  nFine[0], ny=  nFine[1], nz=  nFine[2];
  const int    Nx=nCoarse[0], Ny=nCoarse[1], Nz=nCoarse[2];
  const double Lx= length[0], Ly= length[1], Lz= length[2];

  const LBMS::BundlePtr& xbundle = mesh.bundle( LBMS::XDIR );
  const LBMS::BundlePtr& ybundle = mesh.bundle( LBMS::YDIR );
  const LBMS::BundlePtr& zbundle = mesh.bundle( LBMS::ZDIR );

  if( nFine[0] > 1 ){ cout << *xbundle << endl; }
  if( nFine[1] > 1 ){ cout << *ybundle << endl; }
  if( nFine[2] > 1 ){ cout << *zbundle << endl; }

  TestHelper status;

  status( test_1d(), "1D" );
  status( test_2d(), "2D" );

  status( test_coarse_fine(), "fine -> coarse indexing" );

  LBMS::Coordinate coord(0,0,0), coord2;

  if( nFine[0] > 1 ){
    status( xbundle->spacing(LBMS::XDIR) == Lx/nx, " (X) x-spacing" );
    status( xbundle->spacing(LBMS::YDIR) == Ly/Ny, " (X) y-spacing" );
    status( xbundle->spacing(LBMS::ZDIR) == Lz/Nz, " (X) z-spacing" );
  }
  if( nFine[1] > 1 ){
    status( ybundle->spacing(LBMS::XDIR) == Lx/Nx, " (Y) x-spacing" );
    status( ybundle->spacing(LBMS::YDIR) == Ly/ny, " (Y) y-spacing" );
    status( ybundle->spacing(LBMS::ZDIR) == Lz/Nz, " (Y) z-spacing" );
  }
  if( nFine[2] > 1){
    status( zbundle->spacing(LBMS::XDIR) == Lx/Nx, " (Z) x-spacing" );
    status( zbundle->spacing(LBMS::YDIR) == Ly/Ny, " (Z) y-spacing" );
    status( zbundle->spacing(LBMS::ZDIR) == Lz/nz, " (Z) z-spacing" );
  }
  // verify mesh integrity
  if( nFine[0] > 1 ){
    const double dx = xbundle->spacing(LBMS::XDIR);
    const double dy = xbundle->spacing(LBMS::YDIR);
    const double dz = xbundle->spacing(LBMS::ZDIR);

    coord[LBMS::XDIR] = dx*0.5;
    coord[LBMS::YDIR] = dy*0.5;
    coord[LBMS::ZDIR] = dz*0.5;
    coord2 = xbundle->cell_coord( IntVec(0,0,0) );
    status( coord2 == coord, "x mesh (0,0,0) coordinate" );

    coord[LBMS::XDIR] = Lx-0.5*dx;
    coord2 = xbundle->cell_coord( IntVec(nx-1,0,0) );
    status( coord2 == coord, "x mesh (nx-1,0,0) coordinate" );

    coord[LBMS::YDIR] = Ly-0.5*dy;
    coord2 = xbundle->cell_coord( IntVec(nx-1,Ny-1,0) );
    status( coord2 == coord, "x mesh (nx-1,Ny-1,0) coordinate" );

    coord[LBMS::ZDIR] = Lz-0.5*dz;
    coord2 = xbundle->cell_coord( IntVec(nx-1,Ny-1,Nz-1) );
    status( coord2==coord, "x mesh (nx-1,Ny-1,Nz-1) coordinate" );

    coord = LBMS::Coordinate( dx*0.8, dy*0.01, dz*0.99 );
    status( xbundle->owning_cell( coord ) == IntVec(0,0,0), "x bundle owning_cell()" );

    coord = LBMS::Coordinate( Lx-dx*0.1, Ly-dy*0.2, Lz-dz*0.9 );
    status( xbundle->owning_cell(coord) == IntVec(nx-1,Ny-1,Nz-1), "x bundle owning_cell()" );
  }

  if( nFine[1] > 1 ){
    const double dx = ybundle->spacing(LBMS::XDIR);
    const double dy = ybundle->spacing(LBMS::YDIR);
    const double dz = ybundle->spacing(LBMS::ZDIR);

    coord[LBMS::XDIR] = dx*0.5;
    coord[LBMS::YDIR] = dy*0.5;
    coord[LBMS::ZDIR] = dz*0.5;
    coord2 = ybundle->cell_coord( IntVec(0,0,0) );
    status( coord2 == coord, "y mesh (0,0,0) coordinate" );

    coord[LBMS::XDIR] = Lx-0.5*dx;
    coord2 = ybundle->cell_coord( IntVec(Nx-1,0,0) );
    status( coord2 == coord, "y mesh (nx-1,0,0) coordinate" );

    coord[LBMS::YDIR] = Ly-0.5*dy;
    coord2 = ybundle->cell_coord( IntVec(Nx-1,ny-1,0) );
    status( coord2 == coord, "y mesh (nx-1,ny-1,0) coordinate" );

    coord[LBMS::ZDIR] = Lz-0.5*dz;
    coord2 = ybundle->cell_coord( IntVec(Nx-1,ny-1,Nz-1) );
    status( coord2==coord, "y mesh (nx-1,ny-1,nz-1) coordinate" );

    coord = LBMS::Coordinate( dx*0.8, dy*0.01, dz*0.99 );
    status( ybundle->owning_cell( coord ) == IntVec(0,0,0), "y bundle owning_cell()" );

    coord = LBMS::Coordinate( Lx-dx*0.1, Ly-dy*0.2, Lz-dz*0.9 );
    status( ybundle->owning_cell(coord) == IntVec(Nx-1,ny-1,Nz-1), "y bundle owning_cell()" );
  }

  if( nFine[2] > 1 ){
    const double dx = zbundle->spacing(LBMS::XDIR);
    const double dy = zbundle->spacing(LBMS::YDIR);
    const double dz = zbundle->spacing(LBMS::ZDIR);

    coord[LBMS::XDIR] = dx*0.5;
    coord[LBMS::YDIR] = dy*0.5;
    coord[LBMS::ZDIR] = dz*0.5;
    coord2 = zbundle->cell_coord( IntVec(0,0,0) );
    status( coord2 == coord, "z mesh (0,0,0) coordinate" );

    coord[LBMS::XDIR] = Lx-0.5*dx;
    coord2 = zbundle->cell_coord( IntVec(Nx-1,0,0) );
    status( coord2 == coord, "z mesh (nx-1,0,0) coordinate" );

    coord[LBMS::YDIR] = Ly-0.5*dy;
    coord2 = zbundle->cell_coord( IntVec(Nx-1,Ny-1,0) );
    status( coord2 == coord, "z mesh (nx-1,ny-1,0) coordinate" );

    coord[LBMS::ZDIR] = Lz-0.5*dz;
    coord2 = zbundle->cell_coord( IntVec(Nx-1,Ny-1,nz-1) );
    status( coord2==coord, "z mesh (nx-1,ny-1,nz-1) coordinate" );

    coord = LBMS::Coordinate( dx*0.8, dy*0.01, dz*0.99 );
    status( zbundle->owning_cell( coord ) == IntVec(0,0,0), "z bundle owning_cell()" );

    coord = LBMS::Coordinate( Lx-dx*0.1, Ly-dy*0.2, Lz-dz*0.9 );
    status( zbundle->owning_cell(coord) == IntVec(Nx-1,Ny-1,nz-1), "z bundle owning_cell()" );
  }

  if( nFine[0] > 1 ) status( flat_ix_test(xbundle), "x-bundle ijk/flat test" );
  if( nFine[1] > 1 ) status( flat_ix_test(ybundle), "y-bundle ijk/flat test" );
  if( nFine[2] > 1 ) status( flat_ix_test(zbundle), "z-bundle ijk/flat test" );

  if( nFine[0] > 1 ){
    status( LBMS::npts<LBMS::XVolField>(LBMS::XDIR,ghost,xbundle) == nx>1 ? nx+2 : 1, "XBundle Vol nx" );
    status( LBMS::npts<LBMS::XVolField>(LBMS::YDIR,ghost,xbundle) == Ny>1 ? Ny+2 : 1, "XBundle Vol ny" );
    status( LBMS::npts<LBMS::XVolField>(LBMS::ZDIR,ghost,xbundle) == Nz>1 ? Nz+2 : 1, "XBundle Vol nz" );

    status( LBMS::npts<LBMS::XSurfXField>(LBMS::XDIR,ghost,xbundle) == nx>1 ? nx+3 : 1, "XBundle XSurf nx" );
    status( LBMS::npts<LBMS::XSurfXField>(LBMS::YDIR,ghost,xbundle) == Ny>1 ? Ny+2 : 1, "XBundle XSurf ny" );
    status( LBMS::npts<LBMS::XSurfXField>(LBMS::ZDIR,ghost,xbundle) == Nz>1 ? Nz+2 : 1, "XBundle XSurf nz" );

    status( LBMS::npts<LBMS::YSurfXField>(LBMS::XDIR,ghost,xbundle) == nx>1 ? nx+2 : 1, "XBundle YSurf nx" );
    status( LBMS::npts<LBMS::YSurfXField>(LBMS::YDIR,ghost,xbundle) == Ny>1 ? Ny+3 : 1, "XBundle YSurf ny" );
    status( LBMS::npts<LBMS::YSurfXField>(LBMS::ZDIR,ghost,xbundle) == Nz>1 ? Nz+2 : 1, "XBundle YSurf nz" );

    status( LBMS::npts<LBMS::ZSurfXField>(LBMS::XDIR,ghost,xbundle) == nx>1 ? nx+2 : 1, "XBundle ZSurf nx" );
    status( LBMS::npts<LBMS::ZSurfXField>(LBMS::YDIR,ghost,xbundle) == Ny>1 ? Ny+2 : 1, "XBundle ZSurf ny" );
    status( LBMS::npts<LBMS::ZSurfXField>(LBMS::ZDIR,ghost,xbundle) == Nz>1 ? Nz+3 : 1, "XBundle ZSurf nz" );
  }
  if( nFine[1] > 1 ){
    status( LBMS::npts<LBMS::YVolField>(LBMS::XDIR,ghost,ybundle) == Nx>1 ? Nx+2 : 1, "YBundle Vol nx" );
    status( LBMS::npts<LBMS::YVolField>(LBMS::YDIR,ghost,ybundle) == ny>1 ? ny+2 : 1, "YBundle Vol ny" );
    status( LBMS::npts<LBMS::YVolField>(LBMS::ZDIR,ghost,ybundle) == Nz>1 ? Nz+2 : 1, "YBundle Vol nz" );

    status( LBMS::npts<LBMS::XSurfYField>(LBMS::XDIR,ghost,ybundle) == Nx>1 ? Nx+3 : 1, "YBundle XSurf nx" );
    status( LBMS::npts<LBMS::XSurfYField>(LBMS::YDIR,ghost,ybundle) == ny>1 ? ny+2 : 1, "YBundle XSurf ny" );
    status( LBMS::npts<LBMS::XSurfYField>(LBMS::ZDIR,ghost,ybundle) == Nz>1 ? Nz+2 : 1, "YBundle XSurf nz" );

    status( LBMS::npts<LBMS::YSurfYField>(LBMS::XDIR,ghost,ybundle) == Nx>1 ? Nx+2 : 1, "YBundle YSurf nx" );
    status( LBMS::npts<LBMS::YSurfYField>(LBMS::YDIR,ghost,ybundle) == ny>1 ? ny+3 : 1, "YBundle YSurf ny" );
    status( LBMS::npts<LBMS::YSurfYField>(LBMS::ZDIR,ghost,ybundle) == Nz>1 ? Nz+2 : 1, "YBundle YSurf nz" );

    status( LBMS::npts<LBMS::ZSurfYField>(LBMS::XDIR,ghost,ybundle) == Nx>1 ? Nx+2 : 1, "YBundle ZSurf nx" );
    status( LBMS::npts<LBMS::ZSurfYField>(LBMS::YDIR,ghost,ybundle) == ny>1 ? ny+2 : 1, "YBundle ZSurf ny" );
    status( LBMS::npts<LBMS::ZSurfYField>(LBMS::ZDIR,ghost,ybundle) == Nz>1 ? Nz+3 : 1, "YBundle ZSurf nz" );
  }
  if( nFine[2] > 1 ){
    status( LBMS::npts<LBMS::ZVolField>(LBMS::XDIR,ghost,zbundle) == Nx>1 ? Nx+2 : 1, "ZBundle Vol nx" );
    status( LBMS::npts<LBMS::ZVolField>(LBMS::YDIR,ghost,zbundle) == Ny>1 ? Ny+2 : 1, "ZBundle Vol ny" );
    status( LBMS::npts<LBMS::ZVolField>(LBMS::ZDIR,ghost,zbundle) == nz>1 ? nz+2 : 1, "ZBundle Vol nz" );

    status( LBMS::npts<LBMS::XSurfZField>(LBMS::XDIR,ghost,zbundle) == Nx>1 ? Nx+3 : 1, "ZBundle XSurf nx" );
    status( LBMS::npts<LBMS::XSurfZField>(LBMS::YDIR,ghost,zbundle) == Ny>1 ? Ny+2 : 1, "ZBundle XSurf ny" );
    status( LBMS::npts<LBMS::XSurfZField>(LBMS::ZDIR,ghost,zbundle) == nz>1 ? nz+2 : 1, "ZBundle XSurf nz" );

    status( LBMS::npts<LBMS::YSurfZField>(LBMS::XDIR,ghost,zbundle) == Nx>1 ? Nx+2 : 1, "ZBundle YSurf nx" );
    status( LBMS::npts<LBMS::YSurfZField>(LBMS::YDIR,ghost,zbundle) == Ny>1 ? Ny+3 : 1, "ZBundle YSurf ny" );
    status( LBMS::npts<LBMS::YSurfZField>(LBMS::ZDIR,ghost,zbundle) == nz>1 ? nz+2 : 1, "ZBundle YSurf nz" );

    status( LBMS::npts<LBMS::ZSurfZField>(LBMS::XDIR,ghost,zbundle) == Nx>1 ? Nx+2 : 1, "ZBundle ZSurf nx" );
    status( LBMS::npts<LBMS::ZSurfZField>(LBMS::YDIR,ghost,zbundle) == Ny>1 ? Ny+2 : 1, "ZBundle ZSurf ny" );
    status( LBMS::npts<LBMS::ZSurfZField>(LBMS::ZDIR,ghost,zbundle) == nz>1 ? nz+3 : 1, "ZBundle ZSurf nz" );
  }

  {
    const IntVec ijk( nx/2, ny/2, nz/2 );
    if( nFine[0] > 1 ){
      status( LBMS::index<LBMS::XVolField  >( LBMS::coord<LBMS::XVolField  >(ijk,ghost,xbundle), ghost,xbundle ) == ijk, "XVolField   coordinate/index ops" );
      status( LBMS::index<LBMS::XSurfXField>( LBMS::coord<LBMS::XSurfXField>(ijk,ghost,xbundle), ghost,xbundle ) == ijk, "XSurfXField coordinate/index ops" );
      status( LBMS::index<LBMS::XSurfYField>( LBMS::coord<LBMS::XSurfYField>(ijk,ghost,xbundle), ghost,xbundle ) == ijk, "XSurfYField coordinate/index ops" );
      status( LBMS::index<LBMS::XSurfZField>( LBMS::coord<LBMS::XSurfZField>(ijk,ghost,xbundle), ghost,xbundle ) == ijk, "XSurfZField coordinate/index ops" );
    }
    if( nFine[1] > 1 ){
      status( LBMS::index<LBMS::YVolField  >( LBMS::coord<LBMS::YVolField  >(ijk,ghost,ybundle), ghost,ybundle ) == ijk, "YVolField   coordinate/index ops" );
      status( LBMS::index<LBMS::YSurfXField>( LBMS::coord<LBMS::YSurfXField>(ijk,ghost,ybundle), ghost,ybundle ) == ijk, "YSurfXField coordinate/index ops" );
      status( LBMS::index<LBMS::YSurfYField>( LBMS::coord<LBMS::YSurfYField>(ijk,ghost,ybundle), ghost,ybundle ) == ijk, "YSurfYField coordinate/index ops" );
      status( LBMS::index<LBMS::YSurfZField>( LBMS::coord<LBMS::YSurfZField>(ijk,ghost,ybundle), ghost,ybundle ) == ijk, "YSurfZField coordinate/index ops" );
    }
    if( nFine[2] > 1 ){
      status( LBMS::index<LBMS::ZVolField  >( LBMS::coord<LBMS::ZVolField  >(ijk,ghost,zbundle), ghost,zbundle ) == ijk, "ZVolField   coordinate/index ops" );
      status( LBMS::index<LBMS::ZSurfXField>( LBMS::coord<LBMS::ZSurfXField>(ijk,ghost,zbundle), ghost,zbundle ) == ijk, "ZSurfXField coordinate/index ops" );
      status( LBMS::index<LBMS::ZSurfYField>( LBMS::coord<LBMS::ZSurfYField>(ijk,ghost,zbundle), ghost,zbundle ) == ijk, "ZSurfYField coordinate/index ops" );
      status( LBMS::index<LBMS::ZSurfZField>( LBMS::coord<LBMS::ZSurfZField>(ijk,ghost,zbundle), ghost,zbundle ) == ijk, "ZSurfZField coordinate/index ops" );
    }
  }

  if( status.ok() ){
    cout << endl << "PASS" << endl;
    return 0;
  }
  cout << endl << "FAIL" << endl;
  return -1;
}
