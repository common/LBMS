#include <lbms/bcs/Shapes.h>
#include <lbms/bcs/BCGeometry.h>
#include <lbms/bcs/PointCollection.h>
#include <test/TestHelper.h>
#include <lbms/Coordinate.h>
#include <lbms/Bundle.h>

#include <yaml-cpp/yaml.h>

#include <boost/foreach.hpp>
#include <fstream>

using namespace LBMS;
using SpatialOps::IntVec;
using boost::shared_ptr;
using SpatialOps::SpatialMask;

//-------------------------------------------------------------------

bool test_mask()
{
  const IntVec npts(3,3,3);
  const IntVec zero(0,0,0);
  const Coordinate length(3,3,3);
  const BundlePtr bundle( new Bundle(XDIR,3,npts,length,zero,zero) );

  // Note that these planes are on the domain boundaries. Therefore, on the (+)
  // domain boundary, the corresponding volume indices will be "outside" the domain.
  const Plane xminus( XMINUS, Coordinate( 0.0, 0.0, 0.0 ) );
  const Plane xplus ( XPLUS,  Coordinate( 3.0, 0.0, 0.0 ) );
  const Plane yminus( YMINUS, Coordinate( 0.0, 0.0, 0.0 ) );
  const Plane yplus ( YPLUS,  Coordinate( 0.0, 3.0, 0.0 ) );
  const Plane zminus( ZMINUS, Coordinate( 0.0, 0.0, 0.0 ) );
  const Plane zplus ( ZPLUS,  Coordinate( 0.0, 0.0, 3.0 ) );

  PointCollection2D xmPoints( xminus, bundle );   xmPoints.create_masks<XVolField>(bundle);
  PointCollection2D xpPoints( xplus,  bundle );   xpPoints.create_masks<XVolField>(bundle);
  PointCollection2D ymPoints( yminus, bundle );   ymPoints.create_masks<XVolField>(bundle);
  PointCollection2D ypPoints( yplus,  bundle );   ypPoints.create_masks<XVolField>(bundle);
  PointCollection2D zmPoints( zminus, bundle );   zmPoints.create_masks<XVolField>(bundle);
  PointCollection2D zpPoints( zplus,  bundle );   zpPoints.create_masks<XVolField>(bundle);

  TestHelper overall(true);

  // recall that mask points are 0-based on the interior
  {
    TestHelper status(false);
    const shared_ptr< SpatialMask<XVolField> > xvolMaskMinus = xmPoints.get_mask<XVolField>();
    const std::vector<IntVec>& pts = xvolMaskMinus->points();
//    BOOST_FOREACH( const IntVec& v, pts ) std::cout << v << std::endl;
    status( pts.size() == 9, "#pts" );
    status( pts[0] == IntVec(0,0,0), "xm0");
    status( pts[1] == IntVec(0,0,1), "xm1");
    status( pts[2] == IntVec(0,0,2), "xm2");
    status( pts[3] == IntVec(0,1,0), "xm3");
    status( pts[4] == IntVec(0,1,1), "xm4");
    status( pts[5] == IntVec(0,1,2), "xm5");
    status( pts[6] == IntVec(0,2,0), "xm6");
    status( pts[7] == IntVec(0,2,1), "xm7");
    status( pts[8] == IntVec(0,2,2), "xm8");

    overall( status.ok(), " x- Mask on XVol" );

    const shared_ptr< SpatialMask<XSurfXField> > xsxMaskMinus = xmPoints.get_mask<XSurfXField>();
    const std::vector<IntVec>& xsxpts = xsxMaskMinus->points();
    overall( xsxpts == pts, " x- Mask on XSX" );

    const shared_ptr< SpatialMask<XSurfYField> > xsyMaskMinus = xmPoints.get_mask<XSurfYField>();
    const std::vector<IntVec>& xsypts = xsyMaskMinus->points();
    overall( xsypts == pts, " x- Mask on XSY" );

    const shared_ptr< SpatialMask<XSurfZField> > xszMaskMinus = xmPoints.get_mask<XSurfZField>();
    const std::vector<IntVec>& xszpts = xszMaskMinus->points();
    overall( xszpts == pts, " x- Mask on XSZ" );
  }

  {
    TestHelper status(false);
    const shared_ptr< SpatialMask<XVolField> > xvolMaskPlus = xpPoints.get_mask<XVolField>();
    const std::vector<IntVec>& pts = xvolMaskPlus->points();

    status( pts.size() == 9, "#pts" );
    status( pts[0] == IntVec(2,0,0), "xvp0");
    status( pts[1] == IntVec(2,0,1), "xvp1");
    status( pts[2] == IntVec(2,0,2), "xvp2");
    status( pts[3] == IntVec(2,1,0), "xvp3");
    status( pts[4] == IntVec(2,1,1), "xvp4");
    status( pts[5] == IntVec(2,1,2), "xvp5");
    status( pts[6] == IntVec(2,2,0), "xvp6");
    status( pts[7] == IntVec(2,2,1), "xvp7");
    status( pts[8] == IntVec(2,2,2), "xvp8");

    overall( status.ok(), " x+ Mask on XVol" );
  }
  {
    TestHelper status(false);
    const shared_ptr< SpatialMask<XSurfXField> > xsxm = xpPoints.get_mask<XSurfXField>();
    const std::vector<IntVec>& xsxpts = xsxm->points();

    status( xsxpts.size() == 9, "#pts" );
    status( xsxpts[0] == IntVec(3,0,0), "xsxp0" );
    status( xsxpts[1] == IntVec(3,0,1), "xsxp1" );
    status( xsxpts[2] == IntVec(3,0,2), "xsxp2" );
    status( xsxpts[3] == IntVec(3,1,0), "xsxp3" );
    status( xsxpts[4] == IntVec(3,1,1), "xsxp4" );
    status( xsxpts[5] == IntVec(3,1,2), "xsxp5" );
    status( xsxpts[6] == IntVec(3,2,0), "xsxp6" );
    status( xsxpts[7] == IntVec(3,2,1), "xsxp7" );
    status( xsxpts[8] == IntVec(3,2,2), "xsxp8" );

    overall( status.ok(), "x+ masks on XSurfX" );
  }
  {
    TestHelper status(false);
    const shared_ptr< SpatialMask<XSurfYField> > xsyp = xpPoints.get_mask<XSurfYField>();
    const std::vector<IntVec>& xsypts = xsyp->points();

    status( xsypts.size() == 9, "#pts" );
    status( xsypts[0] == IntVec(2,0,0), "xsyp0" );
    status( xsypts[1] == IntVec(2,0,1), "xsyp1" );
    status( xsypts[2] == IntVec(2,0,2), "xsyp2" );
    status( xsypts[3] == IntVec(2,1,0), "xsyp3" );
    status( xsypts[4] == IntVec(2,1,1), "xsyp4" );
    status( xsypts[5] == IntVec(2,1,2), "xsyp5" );
    status( xsypts[6] == IntVec(2,2,0), "xsyp6" );
    status( xsypts[7] == IntVec(2,2,1), "xsyp7" );
    status( xsypts[8] == IntVec(2,2,2), "xsyp8" );

    overall( status.ok(), "x+ masks on XSurfY" );
  }
  {
    TestHelper status(false);
    const shared_ptr< SpatialMask<XSurfZField> > xszp = xpPoints.get_mask<XSurfZField>();
    const std::vector<IntVec>& xszpts = xszp->points();

    status( xszpts.size() == 9, "#pts" );
    status( xszpts[0] == IntVec(2,0,0), "xszp0" );
    status( xszpts[1] == IntVec(2,0,1), "xszp1" );
    status( xszpts[2] == IntVec(2,0,2), "xszp2" );
    status( xszpts[3] == IntVec(2,1,0), "xszp3" );
    status( xszpts[4] == IntVec(2,1,1), "xszp4" );
    status( xszpts[5] == IntVec(2,1,2), "xszp5" );
    status( xszpts[6] == IntVec(2,2,0), "xszp6" );
    status( xszpts[7] == IntVec(2,2,1), "xsyp7" );
    status( xszpts[8] == IntVec(2,2,2), "xsyp8" );

    overall( status.ok(), "x+ masks on XSurfZ" );
  }

  return overall.ok();
}

//-------------------------------------------------------------------

bool test_parser()
{
  try{
    const YAML::Node parser = YAML::LoadFile( "shape.yaml" );

    const IntVec npts = parser["npts"].as<IntVec>( IntVec(100,100,100) );
    const IntVec zero(0,0,0);
    const Coordinate length = parser["length"].as<Coordinate>( Coordinate(3,3,3) );
    const BundlePtr bundle( new Bundle(XDIR,100,npts,length,zero,zero) );


    const YAML::Node geoParser = parser["Geometries"];
    GeoMapPtr geoMapPtr = parse_geometries( geoParser, bundle );
    GeoMap& geoMap = *geoMapPtr;

    {
      std::ofstream fout("bear.txt");
      geoMap["bear"].print_coordinates( fout, bundle );
    }
    {
      std::ofstream fout("cutout.txt");
      geoMap["cutout"].print_coordinates( fout, bundle );
    }
  }
  catch(std::exception& e){
    std::cout << e.what() << std::endl;
    return false;
  }
  return true;
}

//-------------------------------------------------------------------

bool test_sphere()
{
  TestHelper status(false);

  const double r = 1;
  const Coordinate center(0,0,0);
  const Coordinate edge1(1,0,0);
  const Coordinate edge2(0,1,0);
  const Coordinate edge3(0,0,1);
  const Coordinate interior(.3, .3, .3);
  const Coordinate int2(.5,.5, 0);
  const Coordinate far(1, 1, 1);
  const Coordinate neg(-.3,-.3,-.3);

  const Sphere sph( r, center );

  status( sph.is_in_shape(center),   "center"     );
  status( sph.is_in_shape(edge1),    "edge1"      );
  status( sph.is_in_shape(edge2),    "edge2"      );
  status( sph.is_in_shape(edge3),    "edge3"      );
  status( sph.is_in_shape(interior), "interior"   );
  status( sph.is_in_shape(int2),     "interior2"  );
  status( sph.is_in_shape(neg),      "interior3"  );
  status(!sph.is_in_shape(far),      "outside"    );
  status( sph.bounding_box() == Box( Coordinate(-1,-1,-1),
                                     Coordinate( 1, 1, 1) ),
                                     "bounding box" );
  status( sph != Sphere(2*r,center), "!=" );
  status( sph == Sphere(r,center),   "==" );
  {
    const SpatialOps::IntVec npts(10,10,10);
    const SpatialOps::IntVec zero(0,0,0);
    const Coordinate length( 1,2,3 );
    const BundlePtr bundle( new Bundle(XDIR,1,npts,length,zero,zero) );
    IndexSetPtr pts = sph.points(bundle);
  }

  return status.ok();
}

//-------------------------------------------------------------------

bool test_box()
{
  TestHelper status(false);

  const Coordinate center(0,0,0);
  const Coordinate edge1(1,0,0);
  const Coordinate edge2(0,1,0);
  const Coordinate edge3(0,0,1);
  const Coordinate interior(.3, .3, .3);
  const Coordinate int2(.5,.5, 0);
  const Coordinate far(1, 1, 1);
  const Coordinate neg(-.3,-.3,-.3);

  const Box box( center, far );

  status( box.is_in_shape(center),   "center"          );
  status( box.is_in_shape(edge1),    "edge1"           );
  status( box.is_in_shape(edge2),    "edge2"           );
  status( box.is_in_shape(edge3),    "edge3"           );
  status( box.is_in_shape(interior), "interior"        );
  status( box.is_in_shape(int2),     "interior2"       );
  status( box.is_in_shape(far),      "edge4 of square" );
  status(!box.is_in_shape(neg),      "outside"         );

  status( box.bounding_box() == Box(center,far), "bounding box" );
  status( box == Box(center,far),  "==" );
  status( box != Box(center,int2), "!=" );

  return status.ok();
}

//-------------------------------------------------------------------

bool test_plane()
{
  TestHelper status(false);

  const Plane xplane( XMINUS, Coordinate(0,0,0) );  // x-plane through origin
  const Plane yplane( YMINUS, Coordinate(0,0,0) );  // y-plane through origin
  status( xplane.is_in_shape(Coordinate(0,10,10)), "xplane 1" );
  status( xplane.is_in_shape(Coordinate(0,-1,-1)), "xplane 2" );
  status( !xplane.is_in_shape(Coordinate(0.1,0,0)), "xplane 3" );
  status( yplane.is_in_shape(Coordinate(10,0,10)), "yplane 1" );
  status( yplane.is_in_shape(Coordinate(-1,0,-1)), "yplane 2" );
  status( !yplane.is_in_shape(Coordinate(0,0.1,0)), "yplane 3" );
  status( xplane != yplane, "!=" );
  status( xplane == xplane, "==" );
  status( xplane == Plane(XMINUS,Coordinate(0,1,1)), "== shifted plane" );

  return status.ok();
}

//-------------------------------------------------------------------

bool test_circle()
{
  TestHelper status(false);

  const Circle cx( XMINUS, Coordinate(1,1,1), 2.0 );  // circle in x-plane centered at (1,1,1) with radius 2
  status( !cx.is_in_shape( Coordinate(2,1,1)   ), "x outside 1" );
  status( !cx.is_in_shape( Coordinate(1,-1,-1) ), "x outside 2" );
  status( !cx.is_in_shape( Coordinate(1,1,-1.1)), "x outside 3" );
  status( cx.is_in_shape( Coordinate(1,0,0)   ),  "x inside 1" );
  status( cx.is_in_shape( Coordinate(1,1,-1)  ),  "x inside 2" );
  status( cx.is_in_shape( Coordinate(1,1,2)   ),  "x inside 3" );
  status( cx.is_in_shape( Coordinate(1,3,1)   ),  "x inside 4" );
  const Box cxbb = cx.bounding_box();
  status( cxbb.is_in_shape( Coordinate(1,-1,-1) ), "xbb inside" );

  const Circle cy( YMINUS, Coordinate(1,1,1), 2.0 );  // circle in x-plane centered at (1,1,1) with radius 2
  status( !cy.is_in_shape( Coordinate(1,2,1)   ), "y outside 1" );
  status( !cy.is_in_shape( Coordinate(-1,1,-1) ), "y outside 2" );
  status( !cy.is_in_shape( Coordinate(1,1,-1.1)), "y outside 3" );
  status(  cy.is_in_shape( Coordinate(0,1,0)   ), "y inside 1" );
  status(  cy.is_in_shape( Coordinate(1,1,-1)  ), "y inside 2" );
  status(  cy.is_in_shape( Coordinate(2,1,1)   ), "y inside 3" );

  const Circle cz( ZMINUS, Coordinate(1,1,1), 2.0 );  // circle in x-plane centered at (1,1,1) with radius 2
  status( !cz.is_in_shape( Coordinate(1,1,2)   ), "z outside 1" );
  status( !cz.is_in_shape( Coordinate(-1,-1,1) ), "z outside 2" );
  status( !cz.is_in_shape( Coordinate(-1.1,1,1)), "z outside 3" );
  status(  cz.is_in_shape( Coordinate(0,0,1)   ), "z inside 1" );
  status(  cz.is_in_shape( Coordinate(-1,1,1)  ), "z inside 2" );
  status(  cz.is_in_shape( Coordinate(2,1,1)   ), "z inside 3" );

  return status.ok();
}

//-------------------------------------------------------------------

bool test_rectangle()
{
  TestHelper status(false);

  const Rectangle rx( XMINUS, Coordinate(0,0,0), Coordinate(0,1,1) );
  status(  rx.is_in_shape( Coordinate(0,0.5,0.9)   ), "x inside 1"  );
  status( !rx.is_in_shape( Coordinate(0.1,0.5,0.9) ), "x outside 1" );
  status( !rx.is_in_shape( Coordinate(0,1.5,0.9)   ), "x outside 2" );

  const Rectangle ry( YMINUS, Coordinate(0,0,0), Coordinate(1,0,1) );
  status(  ry.is_in_shape( Coordinate(0.5,0,0.9)   ), "y inside 1" );
  status( !ry.is_in_shape( Coordinate(0.5,0.1,0.9) ), "y outside 1" );
  status( !ry.is_in_shape( Coordinate(1.5,0,0.9)   ), "y outside 2" );

  const Rectangle rz( ZMINUS, Coordinate(0,0,0), Coordinate(1,1,0) );
  status(  rz.is_in_shape( Coordinate(0.9,0.5,0)   ), "z inside 1"  );
  status( !rz.is_in_shape( Coordinate(0.1,0.5,0.9) ), "z outside 1" );
  status( !rz.is_in_shape( Coordinate(0.5,1.5,0)   ), "z outside 2" );

  try{
    const Rectangle invalid( XMINUS, Coordinate(0,0,0), Coordinate(1,1,1) );
    status( false, "cannot create a rectangle out of plane" );
  }
  catch( std::exception& e ){
    status( true, "cannot create a rectangle out of plane" );
  }

  const Box rxbb = rx.bounding_box();
  status( rxbb.is_in_shape(Coordinate(0,0,0)) );
  status( rxbb.is_in_shape(Coordinate(0,1,1)) );

  return status.ok();
}

//-------------------------------------------------------------------

int main()
{
  TestHelper status(true);

  status( test_sphere(),    "Sphere"    );
  status( test_box(),       "Box"       );
  status( test_plane(),     "Plane"     );
  status( test_circle(),    "Circle"    );
  status( test_rectangle(), "Rectangle" );

  status( test_parser(), "Parser" );

  status( test_mask(), "Masks" );

  if( status.ok() ){
    std::cout << "PASS" << std::endl;
    return 0;
  }

  std::cout << "FAIL" << std::endl;
  return -1;
}

//-------------------------------------------------------------------
