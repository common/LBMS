#include <lbms/Direction.h>
#include <fields/Fields.h>

#include <test/TestHelper.h>

using namespace LBMS;

bool test_dir()
{
  TestHelper status(false);

  status( XDIR == 0 );
  status( YDIR == 1 );
  status( ZDIR == 2 );

  status( direction(0) == XDIR );
  status( direction(1) == YDIR );
  status( direction(2) == ZDIR );
  status( direction(3) == NODIR);

  status( direction<0>() == XDIR );
  status( direction<1>() == YDIR );
  status( direction<2>() == ZDIR );

  status( direction<SpatialOps::XDIR>() == XDIR );
  status( direction<SpatialOps::YDIR>() == YDIR );
  status( direction<SpatialOps::ZDIR>() == ZDIR );

  return status.ok();
}


bool test_perp_dir_type(){

  TestHelper status(false);

  typedef LBMS::GetPerpDirType<SpatialOps::XDIR>::Perp1DirT XPerp1Dir;
  typedef LBMS::GetPerpDirType<SpatialOps::XDIR>::Perp2DirT XPerp2Dir;
  status( direction<XPerp1Dir>() == YDIR );
  status( direction<XPerp2Dir>() == ZDIR );

  typedef LBMS::GetPerpDirType<SpatialOps::YDIR>::Perp1DirT YPerp1Dir;
  typedef LBMS::GetPerpDirType<SpatialOps::YDIR>::Perp2DirT YPerp2Dir;
  status( direction<YPerp1Dir>() == XDIR );
  status( direction<YPerp2Dir>() == ZDIR );

  typedef LBMS::GetPerpDirType<SpatialOps::ZDIR>::Perp1DirT ZPerp1Dir;
  typedef LBMS::GetPerpDirType<SpatialOps::ZDIR>::Perp2DirT ZPerp2Dir;
  status( direction<ZPerp1Dir>() == XDIR );
  status( direction<ZPerp2Dir>() == YDIR );

  return status.ok();
}

int main()
{
  TestHelper status(true);
  const std::string s1="1", s2="2", s3="3";

  status( test_dir(), "Basic dir operations" );
  status( test_perp_dir_type(), "GetPerpDirType<> test" );

  if( status.ok() ){
    std::cout << "PASS" << std::endl;
    return 0;
  }
  std::cout << "FAIL" << std::endl;
  return -1;
}
