/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file Direction.h
 * \author James C. Sutherland
 */

#ifndef Direction_h
#define Direction_h

#include <iosfwd>
#include <string>
#include <cassert>

#include <spatialops/SpatialOpsDefs.h>

namespace LBMS{

  /**
   *  \enum Direction
   *  \brief Provides a description of each ordinal direction.
   */
  enum Direction
  {
    XDIR  = SpatialOps::XDIR ::value,   ///< The x-direction
    YDIR  = SpatialOps::YDIR ::value,   ///< The y-direction
    ZDIR  = SpatialOps::ZDIR ::value,   ///< The z-direction
    NODIR = SpatialOps::NODIR::value    ///< an invalid direction
  };

  /**
   *  \ingroup Tools
   *  \fn Direction direction( const size_t i )
   *  \brief convert the direction index to a Direction enum.  0->XDIR, 1->YDIR, 2->ZDIR
   */
  Direction direction( const size_t i );

  Direction direction( const std::string& );

  unsigned direction( const Direction );

  /**
   *  \ingroup Tools
   *  \fn LBMS::Direction direction()
   *  \brief obtain the direction enumeration given the direction type.
   */
  template< typename DirT > Direction direction();

  template< int Dir > Direction direction();

  /**
   *  \brief operator to support printing the Direction
   */
  std::ostream& operator<<( std::ostream& os, const Direction d );


  Direction get_perp1( const Direction dir );
  Direction get_perp2( const Direction dir );

  /**
   *  \ingroup Tools
   *  \fn std::string get_dir_name( const Direction )
   *  \brief get the string name describing the given Direction
   */
  std::string get_dir_name( const Direction );

  /**
   *  \ingroup Tools
   *  \fn std::string get_bundle_name_suffix( const Direction );
   *  \brief obtain the name of the bundle for the given Direction.
   */
  std::string get_bundle_name_suffix( const Direction );


  template<typename T>
  inline T& select_from_dir( const Direction dir, T& t1, T& t2, T& t3 )
  {
    switch( dir ){
    case XDIR: return t1; break;
    case YDIR: return t2; break;
    case ZDIR: return t3; break;
    case NODIR: break;
    }
    assert(false); // should never get here.
    return t1;
  }
  template<typename T>
  inline const T& select_from_dir( const Direction dir, const T& t1, const T& t2, const T& t3 )
  {
    switch( dir ){
    case XDIR: return t1; break;
    case YDIR: return t2; break;
    case ZDIR: return t3; break;
    case NODIR: break;
    }
    assert(false); // should never get here.
    return t1;
  }

  std::string
  get_tau_name( const Direction face,
                const Direction dir );

  /**
   *  \ingroup Tools
   *  \struct GetPerpDirType
   *  \brief Given a direction type, will return a perpendicular direction type
   *
   *  The following typedefs are supplied by a specialization of this struct:
   *  - \c GetPerpDirType<DirT> == XDIR ? YDIR : XDIR
   *  - \c GetPerpDirType<DirT> == ZDIR ? YDIR : ZDIR
   *
   *  Example:
   *  \code{.cpp}
   *  typedef LBMS::GetPerpDirType<SpatialOps::XDIR>::Perp1DirT XPerp1Dir;
   *  typedef LBMS::GetPerpDirType<SpatialOps::XDIR>::Perp2DirT XPerp2Dir;
   *  typedef LBMS::GetPerpDirType<SpatialOps::YDIR>::Perp1DirT YPerp1Dir;
   *  typedef LBMS::GetPerpDirType<SpatialOps::YDIR>::Perp2DirT YPerp2Dir;
   *  typedef LBMS::GetPerpDirType<SpatialOps::ZDIR>::Perp1DirT ZPerp1Dir;
   *  typedef LBMS::GetPerpDirType<SpatialOps::ZDIR>::Perp2DirT ZPerp2Dir;
   *  \endcode
   *
   *  \todo we could add functionality to access perpendicular face fields here?
   */
  template< typename DirT > struct GetPerpDirType;

  template<> struct GetPerpDirType<SpatialOps::XDIR>
  {
    typedef SpatialOps::YDIR Perp1DirT;
    typedef SpatialOps::ZDIR Perp2DirT;
  };
  template<> struct GetPerpDirType<SpatialOps::YDIR>
  {
    typedef SpatialOps::XDIR Perp1DirT;
    typedef SpatialOps::ZDIR Perp2DirT;
  };
  template<> struct GetPerpDirType<SpatialOps::ZDIR>
  {
    typedef SpatialOps::XDIR Perp1DirT;
    typedef SpatialOps::YDIR Perp2DirT;
  };
  //====================================================================

} // namespace LBMS

#endif
