/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   lbms/Coordinate.h
 *  \date   May 24, 2012
 *  \author James C. Sutherland
 */
#ifndef LBMS_COORDINATE_H_
#define LBMS_COORDINATE_H_

#include <spatialops/structured/DoubleVec.h>

#include <cmath>
#include <limits>

namespace LBMS{

  /**
   * \typedef Coordinate
   * \brief Defines a Coordinate type, which is essentially a vector with three elements.
   */
  typedef SpatialOps::DoubleVec Coordinate;

  /**
   * @param c1 the coordinate of the first point
   * @param c2 the coordinate of the second point
   * @return the Euclidian distance between the two points.
   */
  inline double distance( const Coordinate c1, const Coordinate c2 )
  {
    return std::sqrt( (c1[0]-c2[0])*(c1[0]-c2[0]) +
                      (c1[1]-c2[1])*(c1[1]-c2[1]) +
                      (c1[2]-c2[2])*(c1[2]-c2[2]) );
  }

} // namespace LBMS


#endif /* LBMS_COORDINATE_H_ */
