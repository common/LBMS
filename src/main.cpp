#include <string>
#include <iostream>
#include <iomanip>

#include <expression/ExprLib.h>

#include <LBMSVersion.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <lbms/Direction.h>
#include <lbms/Options.h>
#include <lbms/ProblemSolver.h>
#include <operators/Filter.h>
#include <odt/ODTWindow.h>
#include <parser/ParseTools.h>
#include <mpi/Environment.h>
#include <mpi/FieldExchangeInfo.h>
#include <fields/Checkpoint.h>
#include <pokitt/PoKiTTVersion.h>
#include <spatialops/util/MemoryUsage.h>
#ifdef HAVE_MPI
#include <pidx/IDXWriter.h>
#endif

#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
namespace po = boost::program_options;

using namespace std;
using SpatialOps::IntVec;


/**
 * A child of TimeStpper that adds in a mpi barrier for LBMS post stage/step.
 */
class TimeStepper: public Expr::TimeStepper
{
public:
  TimeStepper( Expr::ExpressionFactory& factory,
               const Expr::TSMethod method,
               const int patchID=0,
               const Expr::Tag timeTag = Expr::Tag( "time",     Expr::STATE_NONE ),
               const Expr::Tag tsTag = Expr::Tag( "timestep", Expr::STATE_NONE ) )
    : Expr::TimeStepper(factory,
                        method,
                        "timestepper",
                        patchID,
                        timeTag,
                        tsTag )
  {}

  void post_step()
  {
#   ifdef HAVE_MPI
    LBMS::Environment::world().barrier();
#   endif
  }
};

//====================================================================

void ensure_dir_is_not_duplicate( bool& hasBeenFound,
                                  const LBMS::Direction dir,
                                  const LBMS::BundlePtr b )
{
  if( b->dir() == dir ){
    if( hasBeenFound ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "multiple " << dir << " bundles found on process " << LBMS::Environment::rank() << std::endl
          << "this is not currently supported." << std::endl;
      throw std::runtime_error( msg.str() );
    }
    hasBeenFound = true;
  }
}

//====================================================================

template< typename FieldT >
void set_temperature_for_restarting()
{
  typedef typename FieldT::Location::Bundle BundleDirT;
  const LBMS::Direction bundleDir( LBMS::direction<BundleDirT>() );

  const StringNames& sName = StringNames::self();
  const Expr::Tag temperatureTag = create_tag( sName.temperature, Expr::STATE_NONE, bundleDir );

  Expr::FieldManagerList& fml = *(LBMS::Environment::bundle(bundleDir))->get_field_manager_list();
  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  FieldT& temperature = fm.field_ref(temperatureTag);

  temperature <<= 300.0;
}

//====================================================================

int main( int iarg, char* carg[] )
{
  using SpatialOps::IntVec;

  LBMS::Environment::setup( iarg, carg );

  using std::cout; using std::endl;
  proc0cout << "-------------------------------------------------------" << endl
      << " Lattice-Based Multiscale Simulation (LBMS) " << endl
      << endl
      << " For more information, contact James.Sutherland@utah.edu" << endl
      << endl
      << " This executable was built from source code version:" << endl
      << "    Git Hash: " << LBMSVersionHash << endl
      << "        Date: " << LBMSVersionDate << endl
      << endl
      << " Built against:" << endl
      << "   SpatialOps Hash: " << SOPS_REPO_HASH << endl
      << "              Date: " << SOPS_REPO_DATE << endl
      << "      ExprLib Hash: " << EXPR_REPO_HASH << endl
      << "              Date: " << EXPR_REPO_DATE << endl
      << "       PoKiTT Hash: " << PoKiTTVersionHash << endl
      << "              Date: " << PoKiTTVersionDate << endl
      << "-------------------------------------------------------" << endl
      << endl;

  string inputFileName, restartDBName, jobName;
  bool restarting = false;
  jobName ="";
  //--------------------------------------------------------
  // parse the command line options
  try{
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "restart-from", po::value<string>(&restartDBName), "restart from the specified database")
      ( "job-name", po::value<string>(&jobName));

    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("input-file", po::value<string>(&inputFileName), "input file");

    po::positional_options_description p;
    p.add("input-file", -1);

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    po::variables_map args;
    po::store( po::command_line_parser(iarg,carg).
               options(cmdline_options).positional(p).run(), args );
    po::notify(args);

    if( inputFileName.empty() ){
      proc0cout << "Must provide an input file!" << endl << desc << endl;
      return 1;
    }
    if( args.count("restart-from") ){
      proc0cout << "Restarting from " << restartDBName << endl;
      restarting = true;
    }
    if (args.count("help")) {
      proc0cout << "USAGE:  lbms [optional args] [input file]"
           << desc << "\n";
      return 1;
    }
  }
  catch( std::exception& err ){
    proc0cout << err.what() << std::endl;
    return -1;
  }

  std::list<LBMS::ProblemSolver*> problemSolvers;
  bool tripletMapping = false, filtering = false;
  LBMS::ODTWindow<SpatialOps::XDIR> *xODTWindow = NULL;
  LBMS::ODTWindow<SpatialOps::YDIR> *yODTWindow = NULL;
  LBMS::ODTWindow<SpatialOps::ZDIR> *zODTWindow = NULL;
  LBMS::Conservation<SpatialOps::XDIR> *xconservation = NULL;
  LBMS::Conservation<SpatialOps::YDIR> *yconservation = NULL;
  LBMS::Conservation<SpatialOps::ZDIR> *zconservation = NULL;
  LBMS::Filter<SpatialOps::XDIR> *xfilter = NULL;
  LBMS::Filter<SpatialOps::YDIR> *yfilter = NULL;
  LBMS::Filter<SpatialOps::ZDIR> *zfilter = NULL;
  Expr::FMLMap fmls;
  GraphCategories gc;
  TimeStepper* timeStepper;
  LBMS::Checkpoint *checkpoint=NULL, *visualization=NULL, *initialConditionVisualization=NULL;
# ifdef HAVE_MPI
  LBMS::IDXWriters* idxVis = NULL;
# endif

  //_______________________________________________________
  // build the problem and run it
  try{

    //_______________________________________________________
    // load the input file
    proc0cout << "Loading input file '" << inputFileName << "'" << std::endl;
    const YAML::Node parser = YAML::LoadFile( inputFileName );

    const IntVec nCoarse = parser["Mesh"]["Coarse"].as<IntVec>();
    const IntVec nFine   = parser["Mesh"]["Fine"  ].as<IntVec>();
    {
      const YAML::Node meshPG( parser["Mesh"] );
      std::vector<bool> periodic (3,false);
      const YAML::Node periPG( meshPG["Periodic"] );
      for( YAML::Node::const_iterator i=periPG.begin(); i!=periPG.end(); ++i ){
        std::string dir = i->as<std::string>();
        dir[0] = toupper(dir[0]);

        if( dir == "X" && nFine[0] > 1 ) periodic[0] = true;
        if( dir == "Y" && nFine[1] > 1 ) periodic[1] = true;
        if( dir == "Z" && nFine[2] > 1 ) periodic[2] = true;
      }

      LBMS::MeshPtr mesh( new LBMS::Mesh( nCoarse,
                                          nFine,
                                          LBMS::Coordinate( meshPG["Length"].as<std::vector<double> >() ),
                                          periodic[0],
                                          periodic[1],
                                          periodic[2] ) );

      LBMS::Environment::set_topology( mesh );
    }

    if(jobName.compare("") == 0){
     jobName = parser["JobName"].as<string>();
    }

    //_______________________________________________________
    // set up the solution options
    LBMS::Options options( parser );

    const LBMS::BundlePtrVec bundles = LBMS::Environment::bundles();

    {
      /* Build the field managers. Note that we build the coarse field managers
       * separately and load them on so that all FieldManagerLists share the
       * same coarse field managers. Other ones are built automatically in the
       * FieldManagerList as needed.
       */
      using namespace SpatialOps;
      typedef Expr::FieldManagerList::FMPtr FMPtr;

      BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
        FMPtr coarseVFM = FMPtr( new Expr::LBMSFieldManager<SVolField  >() );
        FMPtr coarseXFM = FMPtr( new Expr::LBMSFieldManager<SSurfXField>() );
        FMPtr coarseYFM = FMPtr( new Expr::LBMSFieldManager<SSurfYField>() );
        FMPtr coarseZFM = FMPtr( new Expr::LBMSFieldManager<SSurfZField>() );
        Expr::FieldManagerList* fml = b->get_field_manager_list();
        fml->set_field_manager<SVolField  >(coarseVFM);
        fml->set_field_manager<SSurfXField>(coarseXFM);
        fml->set_field_manager<SSurfYField>(coarseYFM);
        fml->set_field_manager<SSurfZField>(coarseZFM);
        fmls[b->id()] = fml;
      }
    }

    gc[INITIALIZATION  ] = new GraphHelper( new Expr::ExpressionFactory(false), fmls );
    gc[ADVANCE_SOLUTION] = new GraphHelper( new Expr::ExpressionFactory(false), fmls );

    // require all expressions to provide a patch ID when registered.
    gc[INITIALIZATION  ]->exprFactory->require_patch_id_specification();
    gc[ADVANCE_SOLUTION]->exprFactory->require_patch_id_specification();

    //_______________________________________________________________
    // Build the time stepper and then build ProblemSolver for each
    // bundle - this plugs the appropriate transport equations into
    // the time stepper.
    timeStepper = new TimeStepper( *gc[ADVANCE_SOLUTION]->exprFactory, options.integrator.timeintegrator );

    std::map<int,const Expr::LBMSFieldInfo*> idFieldInfoMap;
    Expr::OpDBMap opDBs;
    bool haveX=false, haveY=false, haveZ=false;
    BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
      // currently we do not support multiple bundles in the same direction
      // (e.g. "sub-bundles") per process.  This is due to the fact that we
      // don't have multiple "patch ids" in the time stepper and also because
      // some of the communication triggers (see ParseEquation.cpp) do not have
      // patch information to set up properly.
      ensure_dir_is_not_duplicate( haveX, LBMS::XDIR, b );
      ensure_dir_is_not_duplicate( haveY, LBMS::YDIR, b );
      ensure_dir_is_not_duplicate( haveZ, LBMS::ZDIR, b );

      problemSolvers.push_back( new LBMS::ProblemSolver( parser, gc, b, options, *timeStepper ) );
      idFieldInfoMap[ b->id() ] = &b->get_field_info();
      opDBs[ b->id() ] = &b->operator_database();
    }

    timeStepper->add_ids( gc[ADVANCE_SOLUTION]->rootIDs );
    timeStepper->finalize( fmls, opDBs, idFieldInfoMap );
    timeStepper->get_tree()->lock_fields( fmls );

    if( LBMS::Environment::rank() == 0 ){
      char rhsDotName[100];
      strcpy(rhsDotName,"RHS_"); strcat(rhsDotName,jobName.c_str()); strcat(rhsDotName,".dot");
      std::ofstream fout(rhsDotName);
#     ifdef NDEBUG
      const bool writeDetails = false;
#     else
      const bool writeDetails = true;
#     endif
      timeStepper->get_tree()->write_tree( fout, false, writeDetails );
    }

    //_______________________________________________________________
    // set up checkpoints and initial condition write
    Expr::TagSet checkpointTags;
    {
      std::vector<std::string> varNames;
      timeStepper->get_variables( varNames );
      BOOST_FOREACH( const std::string& name, varNames ){
        checkpointTags.insert( Expr::Tag( name, Expr::STATE_N ) );
      }
      checkpoint = new LBMS::Checkpoint ( bundles, checkpointTags, fmls );
      initialConditionVisualization = new LBMS::Checkpoint( bundles, checkpointTags, fmls );
    }

    //_______________________________________________________________
    // setting fields to persistent for Matlab visualization, IDX visualization and ODT triplet mapping
    {
      Expr::TagSet tags;
      YAML::Node mtlb( parser["Matlab"] );

      if( parser["Matlab"] ){
        Expr::TagList tmpTags;
        tmpTags = parse_taglist( parser["Matlab"], LBMS::XDIR );
        tags.insert( tmpTags.begin(), tmpTags.end() );
        tmpTags = parse_taglist( parser["Matlab"], LBMS::YDIR );
        tags.insert( tmpTags.begin(), tmpTags.end() );
        tmpTags = parse_taglist( parser["Matlab"], LBMS::ZDIR );
        tags.insert( tmpTags.begin(), tmpTags.end() );
        visualization = new LBMS::Checkpoint( bundles, tags, fmls );
      }

#     ifdef HAVE_MPI
      if( parser["IDX-Visualization"] ){
        idxVis = LBMS::parse_idx_vis_fields( parser, bundles );
        BOOST_FOREACH( const LBMS::IDXWriters::WriterMap::value_type& vt, idxVis->get_writers() ){
          BOOST_FOREACH( const Expr::Tag& tag, vt.second->get_tags() ){
            tags.insert(tag);
          }
        }
      }
#     endif

      if( parser["TripletMapping"] ){
        Expr::TagList tmpTags;
        tmpTags = parse_taglist( parser["TripletMapping"]["Fields"], LBMS::XDIR );
        tags.insert(  tmpTags.begin(), tmpTags.end() );
        tmpTags = parse_taglist( parser["TripletMapping"]["Fields"], LBMS::YDIR );
        tags.insert(  tmpTags.begin(), tmpTags.end() );
        tmpTags = parse_taglist( parser["TripletMapping"]["Fields"], LBMS::ZDIR );
        tags.insert( tmpTags.begin(), tmpTags.end() );
      }

      // lock fields on the graph so that their memory is not released and they are available for visualization.
      BOOST_FOREACH( const Expr::Tag& t, tags ){
        BOOST_FOREACH( Expr::FMLMap::value_type& vt, fmls ){
          /*
           * NOTE that this will throw exceptions because we will often get the
           * FML wrong and the requested field won't exist.  However, while this
           * is not pretty, it will not cause problems that I know of, so just
           * try/catch and keep going.
           */
          try{
            timeStepper->get_tree()->set_expr_is_persistent(t,*vt.second);
          }
          catch( std::exception& ){}
        }
      }
    }
    const std::string vis  = "vis_";

    //_____________________________________________________
    // Triplet mapping setup
    if( parser["TripletMapping"] ){
      tripletMapping = true;
      const YAML::Node odtParser( parser );

      LBMS::ODTParameters parameters = LBMS::fill_ODT_params( odtParser );
      parameters.timestep = parser["TimeStepper"]["Timestep"].as<double>();

      std::vector<std::string> varNames;
      timeStepper->get_variables( varNames );

      BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
        switch( b->dir() ){
          case LBMS::XDIR:
            xconservation = new LBMS::Conservation<SpatialOps::XDIR>( varNames );
            xODTWindow = new LBMS::ODTWindow<SpatialOps::XDIR>( b, parameters, varNames );
            break;
          case LBMS::YDIR:
            yconservation = new LBMS::Conservation<SpatialOps::YDIR>( varNames );
            yODTWindow = new LBMS::ODTWindow<SpatialOps::YDIR>( b, parameters, varNames );
            break;
          case LBMS::ZDIR:
            zconservation = new LBMS::Conservation<SpatialOps::ZDIR>( varNames );
            zODTWindow = new LBMS::ODTWindow<SpatialOps::ZDIR>( b, parameters, varNames );
            break;
          case LBMS::NODIR:
            throw std::runtime_error( "Invalid direction in bundle selection in main" );
        }
      }
    }

    if( !restarting ){
      //_______________________________________________________________
      // set initial conditions
      proc0cout << "Setting initial conditions ... " << std::flush;
      try{
        GraphHelper* const icgh = gc[INITIALIZATION];
        Expr::ExpressionTree icTree( icgh->rootIDs, *icgh->exprFactory, 0, "initial conditions" );

        icTree.register_fields( fmls );
        icTree.lock_fields(fmls);

        if( LBMS::Environment::rank() == 0 ){
          char initialDotName[100]; strcpy(initialDotName,"initial_conditions_");
          strcat(initialDotName,jobName.c_str()); strcat(initialDotName,".dot");
          std::ofstream fout(initialDotName);
          icTree.write_tree(fout);
        }
        icTree.bind_fields( fmls );
        icTree.execute_tree();
      }
      catch( std::exception& err ){
        std::ostringstream msg; msg << "\nError setting initial conditions!\n\n" << err.what();
        throw( std::runtime_error( msg.str() ) );
      }
      timeStepper->set_time( 0.0 );
      proc0cout << "Done." << std::endl;

      // drop initial conditions files
      if( visualization != NULL ){
        proc0cout << "Writing initial condition visualization files ... " << std::flush;
        std::ostringstream nam;
        nam << jobName << "_" << vis << "initial_condition";
        initialConditionVisualization->write_checkpoint( nam.str(), 0.0 );
        proc0cout << "Done" << std::endl;
      }
    }

    if( restarting ){
      // RESTART
      proc0cout << "\nRestarting!\n\n";
      LBMS::Checkpoint restart( bundles, checkpointTags, fmls );
      const double initialTime = restart.read_checkpoint( restartDBName );
      timeStepper->set_time(initialTime);

      BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
        //set_conservation_fields( b );
        switch( b->dir() ){
          //A reasonable temperature must be set for the initial guess in the temperature solve for restarting.
          case LBMS::XDIR: set_temperature_for_restarting<LBMS::XVolField>(); break;
          case LBMS::YDIR: set_temperature_for_restarting<LBMS::YVolField>(); break;
          case LBMS::ZDIR: set_temperature_for_restarting<LBMS::ZVolField>(); break;
          case LBMS::NODIR: assert(false); break;
        }
      }
    }

    //_______________________________________________________________
    // set up filtering (must occur after initial conditions/restart)
    if( parser["Filtering"] ){
      filtering = true;
      Expr::TagSet filteringTags;
      {
        std::vector<std::string> varNames;
        timeStepper->get_variables( varNames );
        BOOST_FOREACH( const std::string& name, varNames ){
          filteringTags.insert( Expr::Tag( name, Expr::STATE_N ) );
        }
        BOOST_FOREACH( LBMS::BundlePtr b, bundles ){
          //set_conservation_fields( b );
          switch( b->dir() ){
            case LBMS::XDIR: xfilter = new LBMS::Filter<SpatialOps::XDIR>( b, filteringTags ); break;
            case LBMS::YDIR: yfilter = new LBMS::Filter<SpatialOps::YDIR>( b, filteringTags ); break;
            case LBMS::ZDIR: zfilter = new LBMS::Filter<SpatialOps::ZDIR>( b, filteringTags ); break;
            case LBMS::NODIR: assert(false); break;
          }
        }
      }
    }

    //_____________________________________________________
    // march the solution forward.

    proc0cout << "\nMemory usage (MB): " << SpatialOps::get_memory_usage() / 1000000 << std::endl;

    const boost::posix_time::ptime start = boost::posix_time::microsec_clock::universal_time();
    proc0cout << "Starting time step loop" << std::endl;
    while( timeStepper->get_time() < options.integrator.endTime ){
      const boost::posix_time::ptime tsStart = boost::posix_time::microsec_clock::universal_time();

      timeStepper->step( options.integrator.timestep );
        
      const double simTime = timeStepper->get_time();
      // apply triplet mapping where appropriate
      if( tripletMapping ){
        if( xODTWindow != NULL) xODTWindow->loop_through( simTime, *xconservation );
        if( yODTWindow != NULL) yODTWindow->loop_through( simTime, *yconservation );
        if( zODTWindow != NULL) zODTWindow->loop_through( simTime, *zconservation );

        //--------------------------------------
        //Filtering solution variables
        if( filtering ){
          if( xfilter != NULL ) xfilter->apply_filter();
          if( yfilter != NULL ) yfilter->apply_filter();
          if( zfilter != NULL ) zfilter->apply_filter();
        }

        if( xODTWindow != NULL){
          Expr::FieldManagerList& parfml = *(LBMS::Environment::bundle(LBMS::XDIR))->get_field_manager_list();
          Expr::FieldMgrSelector<LBMS::XVolField>::type& fm = parfml.field_manager<LBMS::XVolField>();

          std::vector<std::string> varNames;
          timeStepper->get_variables( varNames );
          const std::string bundleName = get_bundle_name_suffix( LBMS::direction<LBMS::XDIR>() );
          const StringNames& sName = StringNames::self();

          BOOST_FOREACH( const std::string& name, varNames ){
            const Expr::Tag fieldTag( name, Expr::STATE_N  );
            if( fm.has_field(fieldTag) ){
              const Expr::Tag perp1CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.ycons + bundleName, Expr::STATE_NONE );
              const Expr::Tag perp2CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.zcons + bundleName, Expr::STATE_NONE );
              xconservation->apply_conservation( perp1CoarseTag, perp2CoarseTag, fieldTag );
            }//End if( has_field )
          }//End BOOST_FOREACH
        }//End if( this dir )

        if( yODTWindow != NULL){
          Expr::FieldManagerList& parfml = *(LBMS::Environment::bundle(LBMS::YDIR))->get_field_manager_list();
          Expr::FieldMgrSelector<LBMS::YVolField>::type& fm = parfml.field_manager<LBMS::YVolField>();

          std::vector<std::string> varNames;
          timeStepper->get_variables( varNames );
          const std::string bundleName = get_bundle_name_suffix( LBMS::direction<LBMS::YDIR>() );
          const StringNames& sName = StringNames::self();

          BOOST_FOREACH( const std::string& name, varNames ){
            const Expr::Tag fieldTag( name, Expr::STATE_N  );
            if( fm.has_field(fieldTag) ){
              const Expr::Tag perp1CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.xcons + bundleName, Expr::STATE_NONE );
              const Expr::Tag perp2CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.zcons + bundleName, Expr::STATE_NONE );
              yconservation->apply_conservation( perp1CoarseTag, perp2CoarseTag, fieldTag );
            }//End if( has_field )
          }//End BOOST_FOREACH
        }//End if( this dir )

        if( zODTWindow != NULL){
          Expr::FieldManagerList& parfml = *(LBMS::Environment::bundle(LBMS::ZDIR))->get_field_manager_list();
          Expr::FieldMgrSelector<LBMS::ZVolField>::type& fm = parfml.field_manager<LBMS::ZVolField>();

          std::vector<std::string> varNames;
          timeStepper->get_variables( varNames );
          const std::string bundleName = get_bundle_name_suffix( LBMS::direction<LBMS::ZDIR>() );
          const StringNames& sName = StringNames::self();

          BOOST_FOREACH( const std::string& name, varNames ){
            const Expr::Tag fieldTag( name, Expr::STATE_N  );
            if( fm.has_field(fieldTag) ){
              const Expr::Tag perp1CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.xcons + bundleName, Expr::STATE_NONE );
              const Expr::Tag perp2CoarseTag = Expr::Tag( name.substr( 0, name.size()-8 ) + sName.ycons + bundleName, Expr::STATE_NONE );
              zconservation->apply_conservation( perp1CoarseTag, perp2CoarseTag, fieldTag );
            }//End if( has_field )
          }//End BOOST_FOREACH
        }//End if( this dir )
      }//End if( tripletMapping )
      else{
        //--------------------------------------
        //Filtering solution variables
        if( filtering ){
          if( xfilter != NULL ) xfilter->apply_filter();
          if( yfilter != NULL ) yfilter->apply_filter();
          if( zfilter != NULL ) zfilter->apply_filter();
        }
      }

      // drop restart files if necessary
      if( std::fmod(simTime,options.integrator.fileTime) <= options.integrator.timestep ){
        std::ostringstream nam;
        nam << jobName << "_Checkpoint_" << simTime;
        proc0cout<< "Writing checkpoint file: " << nam.str() << std::endl;
        checkpoint->write_checkpoint( nam.str(), simTime );
      }

      // drop visualization files if necessary
      if( std::fmod(simTime,options.integrator.monitorTime) < options.integrator.timestep ){
        proc0cout << "Writing visualization files at t=" << simTime << std::endl;
        if( visualization != NULL ){
          std::ostringstream nam;
          nam << jobName << "_" << vis << simTime;
          visualization->write_checkpoint(  nam.str(), simTime );
        }
#       ifdef HAVE_MPI
        if( idxVis != NULL ){
          idxVis->write( simTime, fmls );
        }
#       endif
      }

      const boost::posix_time::time_duration elapsed = boost::posix_time::microsec_clock::universal_time() - tsStart;
      proc0cout << "Sim. time: " << timeStepper->get_time()
                << ",  timestep time (s) = "
                << elapsed.total_microseconds()*1e-6
                << ",  Memory usage (MB): " << SpatialOps::get_memory_usage() / 1000000 << endl
                << endl << endl;

      //Should live at the end of the loop,
      //so that as much work can be done before waiting on the receive
      if( xODTWindow != NULL) xODTWindow->clear_conservation_send_buffers();
      if( yODTWindow != NULL) yODTWindow->clear_conservation_send_buffers();
      if( zODTWindow != NULL) zODTWindow->clear_conservation_send_buffers();
    }

    const boost::posix_time::time_duration elapsed = boost::posix_time::microsec_clock::universal_time() - start;
    proc0cout << "\nTotal time (s) = "
        << elapsed.total_microseconds()*1e-6
        << endl;

  }
  catch( std::exception& err ){
    std::cerr << endl << endl
        << "********************************************************************" << endl
        << "*** A fatal error has occurred.  Information follows:" << endl << endl
        << err.what() << endl
        << "********************************************************************" << endl
        <<  "On Process: "<<LBMS::Environment::rank() << endl;
#   ifdef HAVE_MPI
    LBMS::Environment::boost_mpi_env().abort(-1);
#   endif
    return -1;
  }
  catch(...){
    std::cout << endl << endl
        << "An unknown error occurred.  Terminating now."
        << endl << "On Process: " << LBMS::Environment::rank() << endl;
#   ifdef HAVE_MPI
    LBMS::Environment::boost_mpi_env().abort(-1);
#   endif
    return -1;
  }

  //______________________________________________________
  // Clean up
  BOOST_FOREACH( LBMS::ProblemSolver* ps, problemSolvers ) delete ps;
  BOOST_FOREACH( GraphCategories::value_type entry, gc ){
    GraphHelper* gh = entry.second;
    delete gh->exprFactory;
    delete gh;
  }
  delete timeStepper;
  delete checkpoint;
  if( tripletMapping ){
    delete xODTWindow;
    delete yODTWindow;
    delete zODTWindow;
    delete xconservation;
    delete yconservation;
    delete zconservation;
  }
  if( filtering ){
    delete xfilter;
    delete yfilter;
    delete zfilter;
  }
  if( visualization != NULL ){
    delete visualization;
    delete initialConditionVisualization;
  }
# ifdef HAVE_MPI
  if( idxVis != NULL ) delete idxVis;
# endif
  //______________________________________________________

  proc0cout << std::endl << "DONE - exiting." << std::endl;
# ifdef HAVE_MPI
  LBMS::Environment::world().barrier();
# endif

  return 0;
}

//====================================================================
