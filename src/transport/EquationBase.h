/*
 * The MIT License
 *
 * Copyright (c) 2012 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LBMS_EquationBase_h
#define LBMS_EquationBase_h

#include <string>

//-- ExprLib includes --//
#include <expression/Tag.h>
#include <expression/ExpressionFactory.h>
#include <expression/ExpressionID.h>

#include <nscbc/CharacteristicBCBuilder.h>

//-- LBMS includes --//
//#include <operators/Operators.h>
#include <lbms/Bundle_fwd.h>
//#include <lbms/GraphHelperTools.h>
//#include <fields/Fields.h>

namespace LBMS{

  class ExprDeps;  // forward declaration.
//  class GraphHelper;

  /**
   *  \class  EquationBase
   *  \author Derek Cline (Lifted somewhat from EquationBase.h in Wasatch)
   *  \date   April, 2015
   *  \brief  Base class for defining a transport equation.
   */
  class EquationBase{
  public:

    /**
     * @brief Construct an EquationBase
     * @param bundle pointer to the bundle associated with the equation
     * @param solnVarName the name of the solution variable for this equation
     * @param rhsTag the tag for the rhs expression
     */
    EquationBase( const BundlePtr bundle,
                  const std::string solnVarName,
                  const Expr::Tag rhsTag );
//    GraphCategories& gc,

    virtual ~EquationBase(){}

    /**
     *  \brief Obtain the solution variable name for this transport equation.
     */
    inline std::string solution_variable_name() const{ return solnVarName_; }


    /**
     *  \brief Obtain the rhs tag of the solution variable for this transport equation.
     */
    inline const Expr::Tag& get_rhs_tag() const { return rhsTag_; }

    /**
     *  \brief Obtain the rhs name of the solution variable for this transport equation.
     */
    inline std::string rhs_name() const{ return rhsTag_.name(); }

    /**
     * \brief Returns the ExpressionID for the RHS expression associated with this EquationBase
     */
    Expr::ExpressionID get_rhs_id() const;

    virtual Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

  protected:

    const BundlePtr bundle_;         ///< The bundle that this TransportEquation lives on.
    const std::string solnVarName_;  ///< without the bundle direction appended
    const Expr::Tag rhsTag_;         ///< Tag for the rhs
  };

} // namespace LBMS

#endif // LBMS_EquationBase_h
