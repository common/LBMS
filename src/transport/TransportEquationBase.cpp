/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   TransportEquationBase.cpp
 *  \author Derek Cline
 */

#include "TransportEquationBase.h"

#include <lbms/Bundle.h>
#include <transport/ParseEquation.h>
#include <fields/Fields.h>

#include <exprs/ConvectiveFlux.h>
#include <operators/OneSidedConvectiveDiv.h>
#include <operators/NSCBCDiffFluxTreatment.h>

#include <mpi/Environment.h>

// ExprLib includes for NSCBCs
#include <expression/BoundaryConditionExpression.h>

namespace LBMS{

  template<typename FieldT>
  TransportEquationBase<FieldT>::
  TransportEquationBase( const BundlePtr bundle,
                         const std::string varname,
                         const Expr::Tag rhsTag )
    : LBMS::EquationBase( bundle, varname+bundle->name(), rhsTag ),
      doX_( bundle->npts(XDIR) > 1 ),
      doY_( bundle->npts(YDIR) > 1 ),
      doZ_( bundle->npts(ZDIR) > 1 ),
      bundle_( bundle ),
      rawSolnVarName_( varname ),
      sName_( StringNames::self() )
  {
    proc0cout << "Building transport equation for variable: '"
              << varname << "' on " << bundle->dir()
              << " bundle" << std::endl;
  }

  //-----------------------------------------------------------------

  template<typename FieldT>
  TransportEquationBase<FieldT>::~TransportEquationBase()
  {}

  //-----------------------------------------------------------------

  template<typename FieldT>
  void TransportEquationBase<FieldT>::
  build_convective_fluxes( Expr::ExpressionFactory& factory,
                           GraphCategories& gc,
                           FieldTagInfo& rhsInfo )
  {
    const Direction dir = bundle_->dir();
    const Direction perp1Dir = get_perp1(dir);
    const Direction perp2Dir = get_perp2(dir);

    const bool do1 = bundle_->npts(dir     ) > 1;
    const bool do2 = bundle_->npts(perp1Dir) > 1;
    const bool do3 = bundle_->npts(perp2Dir) > 1;

    assert( do1 );

    { // bundle-direction flux
      const std::string vel = select_from_dir( dir, sName_.xvel, sName_.yvel, sName_.zvel );

      const Expr::Tag varTag   = create_tag( rawSolnVarName_, Expr::STATE_N,    dir );
      const Expr::Tag advelTag = create_tag( vel + sName_.advect, Expr::STATE_NONE, dir );

      const std::string parFace   = select_from_dir( dir, sName_.xface, sName_.yface, sName_.zface );
      const Expr::Tag convFluxTag = create_tag( rawSolnVarName_ + "_convectiveflux" + parFace, Expr::STATE_NONE, dir );

      typedef typename ConvectiveFlux<ParFluxT>::Builder ConvFlux;
      factory.register_expression( new ConvFlux( convFluxTag, advelTag, varTag ), false, bundle_->id() );

      LBMS::setup_field_exchange<ParFluxT>( bundle_, convFluxTag, factory, gc, dir, dir );
      rhsInfo[ select_from_dir( dir, CONVECTIVE_FLUX_X, CONVECTIVE_FLUX_Y, CONVECTIVE_FLUX_Z ) ] = convFluxTag;
    }

    if( do2 ){

      const std::string vel = select_from_dir( perp1Dir, sName_.xvel, sName_.yvel, sName_.zvel );
      const Expr::Tag varTag   = create_tag( rawSolnVarName_, Expr::STATE_N,    dir );
      const Expr::Tag advelTag = create_tag( vel + sName_.advect, Expr::STATE_NONE, dir );

      const std::string perp1Face = select_from_dir( perp1Dir, sName_.xface, sName_.yface, sName_.zface );
      const Expr::Tag convFluxTag = create_tag( rawSolnVarName_ + "_convectiveflux" + perp1Face, Expr::STATE_NONE, dir );

      const Expr::Tag convFineFluxTag = create_tag( rawSolnVarName_ + "_convectiveflux_fine" + perp1Face, Expr::STATE_NONE, dir );

      typedef typename ConvectiveFlux<Perp1FluxT>::Builder ConvFlux;
      factory.register_expression( new ConvFlux( convFineFluxTag, advelTag, varTag ), false, bundle_->id() );

      LBMS::setup_field_exchange<Perp1FluxT>( bundle_, convFluxTag, factory, gc, perp1Dir, dir );

      const Expr::Tag reconsTag = create_tag( rawSolnVarName_ + "_convectiveflux" + sName_.reconstructed + perp1Face, Expr::STATE_NONE, dir );

      typedef typename OffLineFlux< Perp1FluxT, Perp1CoarseT >::Builder OffLineFluxBuilder;
      factory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                            create_tag( rawSolnVarName_ + "_convectiveflux" + perp1Face + sName_.coarse, Expr::STATE_NONE, dir ),
                                                            convFineFluxTag,
                                                            bundle_ ),
                                   false, bundle_->id()  );

      rhsInfo[ select_from_dir( perp1Dir, CONVECTIVE_FLUX_X, CONVECTIVE_FLUX_Y, CONVECTIVE_FLUX_Z ) ] = reconsTag;
    }

    if( do3 ){

      const std::string vel = select_from_dir( perp2Dir, sName_.xvel, sName_.yvel, sName_.zvel );
      const Expr::Tag varTag   = create_tag( rawSolnVarName_, Expr::STATE_N,    dir );
      const Expr::Tag advelTag = create_tag( vel + sName_.advect, Expr::STATE_NONE, dir );

      const std::string perp2Face = select_from_dir( perp2Dir, sName_.xface, sName_.yface, sName_.zface );
      const Expr::Tag convFluxTag = create_tag( rawSolnVarName_ + "_convectiveflux" + perp2Face, Expr::STATE_NONE, dir );

      const Expr::Tag convFineFluxTag = create_tag( rawSolnVarName_ + "_convectiveflux_fine" + perp2Face, Expr::STATE_NONE, dir );

      typedef typename ConvectiveFlux<Perp2FluxT>::Builder ConvFlux;
      factory.register_expression( new ConvFlux( convFineFluxTag, advelTag, varTag ), false, bundle_->id() );

      LBMS::setup_field_exchange<Perp2FluxT>( bundle_, convFluxTag, factory, gc, perp2Dir, dir );

      const Expr::Tag reconsTag = create_tag( rawSolnVarName_ + "_convectiveflux" + sName_.reconstructed + perp2Face, Expr::STATE_NONE, dir );

      typedef typename OffLineFlux< Perp2FluxT, Perp2CoarseT >::Builder OffLineFluxBuilder;
      factory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                            create_tag( rawSolnVarName_ + "_convectiveflux" + perp2Face + sName_.coarse, Expr::STATE_NONE, dir ),
                                                            convFineFluxTag,
                                                            bundle_ ),
                                   false, bundle_->id()  );

      rhsInfo[ select_from_dir( perp2Dir, CONVECTIVE_FLUX_X, CONVECTIVE_FLUX_Y, CONVECTIVE_FLUX_Z ) ] = reconsTag;
    }

  }


  template<typename FieldT>
  void TransportEquationBase<FieldT>::
  build_nscbc_convflux( Expr::ExpressionFactory& factory, const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec& nscbcVec )
  {
    const LBMS::Direction bundleDir = bundle_->dir();
    Expr::ExpressionBuilder* builder = NULL;

    //Necessary for BOOST_FOREACH (its macro will parse the two template parameters in
    //std::pair as two arguments).
    typedef std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D > NSCBCPairT;

    BOOST_FOREACH( const NSCBCPairT& bc, nscbcVec ){

      const std::string faceString = select_from_dir( direction((bc.first)->get_boundary_direction()),
                                                      sName_.xface,
                                                      sName_.yface,
                                                      sName_.zface );

      const Expr::Tag modConvTag = create_tag( rawSolnVarName_ + "_convectiveflux" + faceString
                                               + bc.first->type()
                                               + bc.first->side(),
                                               Expr::STATE_NONE,
                                               bundleDir );

      const Expr::Tag convFluxTag = ( LBMS::direction((bc.first)->get_boundary_direction()) == this->bundle_->dir() )
                                      ?
                                    create_tag( rawSolnVarName_ + "_convectiveflux" + faceString,
                                                Expr::STATE_NONE,
                                                bundleDir )
                                      :
                                    create_tag( rawSolnVarName_ + "_convectiveflux" + sName_.fine + faceString,
                                                Expr::STATE_NONE,
                                                bundleDir );

      const NSCBC::BCType bT = bc.first->get_boundary_type();
      if( bT == NSCBC::NONREFLECTING ){
        const Expr::Tag modRHSTag = create_tag( rawSolnVarName_ + "_RHS" + faceString
                                                + bc.first->type()
                                                + bc.first->side(),
                                                Expr::STATE_NONE,
                                                bundleDir );

        //Applying a neumann zero condition will, essentially, zero out the contribution
        //of the convective fluxes to the RHS at the boundary.  Then, we recompute the contribution
        //using a one sided stencil on the volume fields and attach it as a modifier to the RHS.
        switch( bc.first->get_boundary_direction() ){
          case SpatialOps::XDIR::value:
//            build_one_sided_convection<SpatialOps::XDIR>( factory, bc, modRHSTag, this->get_rhs_tag() );
            build_nscbc_neumann       <SpatialOps::XDIR>( factory, bc, modConvTag, convFluxTag );
            break;
          case SpatialOps::YDIR::value:
//            build_one_sided_convection<SpatialOps::YDIR>( factory, bc, modRHSTag, this->get_rhs_tag() );
            build_nscbc_neumann       <SpatialOps::YDIR>( factory, bc, modConvTag, convFluxTag );
            break;
          case SpatialOps::ZDIR::value:
//            build_one_sided_convection<SpatialOps::ZDIR>( factory, bc, modRHSTag, this->get_rhs_tag() );
            build_nscbc_neumann       <SpatialOps::ZDIR>( factory, bc, modConvTag, convFluxTag );
            break;
          case SpatialOps::NODIR::value:
          {
            std::ostringstream msg;
            msg << __FILE__ << " : " << __LINE__ << std::endl
                << "No direction was given" << std::endl;
            throw std::runtime_error( msg.str() );
            break;
          }
        }//End Switch
      }//End BoundaryType check
      else if( bT == NSCBC::WALL ){
        switch( bc.first->get_boundary_direction() ){
          case SpatialOps::XDIR::value: zero_convective_flux<SpatialOps::XDIR>( factory, bc, modConvTag, convFluxTag ); break;
          case SpatialOps::YDIR::value: zero_convective_flux<SpatialOps::YDIR>( factory, bc, modConvTag, convFluxTag ); break;
          case SpatialOps::ZDIR::value: zero_convective_flux<SpatialOps::ZDIR>( factory, bc, modConvTag, convFluxTag ); break;
          case SpatialOps::NODIR::value:
          {
            std::ostringstream msg;
            msg << __FILE__ << " : " << __LINE__ << std::endl
                << "No direction was given" << std::endl;
            throw std::runtime_error( msg.str() );
            break;
          }
        }//End Switch
      }//End elseif
    }//End BOOST_FOREACH
  }

  //========================================================================

  template< typename FieldT >
  template< typename DirT   >
  void TransportEquationBase<FieldT>::
  build_one_sided_convection( Expr::ExpressionFactory& factory,
                              const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                              const Expr::Tag& modTag,
                              const Expr::Tag& rhsTag )
  {
    Expr::ExpressionBuilder* builder = NULL;

    typedef typename SpatialOps::UnitTriplet<DirT>::type UnitTripletT;

    const std::string vel = select_from_dir( direction<DirT>(), sName_.xvel, sName_.yvel, sName_.zvel );

    //We use volume field velocities and conserved quantities to calculate a volume
    //field gradient.  This is then subtracted from the RHS in a modifier.
    const Expr::Tag velTag = create_tag( vel,             Expr::STATE_NONE, bundle_->dir() );
    const Expr::Tag phiTag = create_tag( rawSolnVarName_, Expr::STATE_N,    bundle_->dir() );

    if( nscbcPair.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
      //Create the one sided stencil gradient
      typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<typename UnitTripletT::Negate>,FieldT>::type OpT;
      typedef typename LBMS::OneSidedConvection<FieldT,OpT>::Builder OneSidedConvectionBuilder;

      builder = new OneSidedConvectionBuilder( modTag, *( (nscbcPair.second.template get_mask<FieldT>()) ), phiTag, velTag );
    }
    else if( nscbcPair.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
      //Create the one sided stencil gradient
      typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,FieldT>::type OpT;
      typedef typename LBMS::OneSidedConvection<FieldT,OpT>::Builder OneSidedConvectionBuilder;

      builder = new OneSidedConvectionBuilder( modTag, *( (nscbcPair.second.template get_mask<FieldT>()) ), phiTag, velTag );
    }
    factory.register_expression(builder, false, bundle_->id() );
    factory.attach_modifier_expression(modTag, rhsTag, ALL_PATCHES, false);
  }

  //========================================================================

  template< typename FieldT >
  template< typename DirT   >
  void TransportEquationBase<FieldT>::
  build_nscbc_neumann( Expr::ExpressionFactory& factory,
                       const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                       const Expr::Tag& modTag,
                       const Expr::Tag& fieldTag )
  {
    Expr::ExpressionBuilder* builder = NULL;

    typedef typename LBMS::TransportEquationBase<FieldT>::template FaceSelector<BundleDirT,DirT>::type FaceFieldT;
    typedef typename Expr::ConstantBCOpExpression< FaceFieldT, DirT >::Builder BCExpr;

    typedef typename Expr::BoundaryConditionExpression<FaceFieldT,DirT>::MaskType MaskT;
    typedef typename Expr::BoundaryConditionExpression<FaceFieldT,DirT>::MaskPtr MaskPtrT;

    const MaskPtrT mask = (nscbcPair.second).template get_mask<typename MaskT::field_type>();

    builder = new BCExpr( modTag, mask, LBMS::select_bctype("NEUMANN"), nscbcPair.first->get_boundary_side(), 0.0 );

    (factory).register_expression( builder, false, bundle_->id() );
    (factory).attach_modifier_expression( modTag, fieldTag, ALL_PATCHES, false );
  }
  //=================================================================

  template< typename FieldT >
  template< typename DirT >
  void TransportEquationBase<FieldT>::
  set_diffusive_flux_boundary( Expr::ExpressionFactory& factory,
                               const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                               const Expr::Tag& modTag,
                               const Expr::Tag& fieldTag,
                               const int isNormalStress )
  {
    typedef typename SpatialOps::UnitTriplet<DirT>::type UnitTripletT;
    typedef typename LBMS::TransportEquationBase<FieldT>::template FaceSelector<BundleDirT,DirT>::type FaceFieldT;

    const std::string vel = select_from_dir( direction<DirT>(), sName_.xvel, sName_.yvel, sName_.zvel );
    const Expr::Tag advelTag = create_tag( vel + sName_.advect, Expr::STATE_NONE, bundle_->dir() );

    Expr::ExpressionBuilder* builder = NULL;

    if( nscbcPair.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){

      typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<typename UnitTripletT::Negate>,FaceFieldT>::type OpT;
      typedef typename LBMS::NSCBCDiffFluxTreatment<FaceFieldT,OpT>::Builder DiffFluxBuilder;

      builder = new DiffFluxBuilder( modTag,
                                     advelTag,
                                     *( (nscbcPair.second.template get_mask<FaceFieldT>()) ),
                                     nscbcPair.first->get_boundary_side(),
                                     isNormalStress );
    }
    else if( nscbcPair.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
      typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,FaceFieldT>::type OpT;
      typedef typename LBMS::NSCBCDiffFluxTreatment<FaceFieldT,OpT>::Builder DiffFluxBuilder;

      builder = new DiffFluxBuilder( modTag,
                                     advelTag,
                                     *( (nscbcPair.second.template get_mask<FaceFieldT>()) ),
                                     nscbcPair.first->get_boundary_side(), isNormalStress );
    }
    else{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side when setting diffusive NSCBC fluxes" << std::endl;
      throw std::runtime_error( msg.str() );
    }

    (factory).register_expression( builder, false, bundle_->id() );
    (factory).attach_modifier_expression( modTag, fieldTag, ALL_PATCHES, false );
  }


  //========================================================================

  template< typename FieldT >
  template< typename DirT   >
  void TransportEquationBase<FieldT>::
  zero_convective_flux( Expr::ExpressionFactory& factory,
                        const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                        const Expr::Tag& modTag,
                        const Expr::Tag& convTag )
  {
    Expr::ExpressionBuilder* builder = NULL;

    typedef typename LBMS::TransportEquationBase<FieldT>::template FaceSelector<BundleDirT,DirT>::type FaceFieldT;
    typedef typename Expr::ConstantMaskedExpression<FaceFieldT>::Builder BCExpr;

    builder = new BCExpr( modTag, (nscbcPair.second).template get_mask<FaceFieldT>(), 0.0 );

    factory.register_expression(builder, false, bundle_->id() );
    factory.attach_modifier_expression(modTag, convTag, ALL_PATCHES, false);
  }

  //========================================================================

  // explicit template instantiation
#define INSTANTIATETRANSPORTEQUATIONBASE( VolT ) \
  template class TransportEquationBase<VolT>;    \
  INSTANTIATETRANSPROTEQUATIONMETHODS( SpatialOps::XDIR, VolT )    \
  INSTANTIATETRANSPROTEQUATIONMETHODS( SpatialOps::YDIR, VolT )    \
  INSTANTIATETRANSPROTEQUATIONMETHODS( SpatialOps::ZDIR, VolT )

#define INSTANTIATETRANSPROTEQUATIONMETHODS( DirT, VolT ) \
  template void TransportEquationBase<VolT>::build_nscbc_neumann        <DirT>( Expr::ExpressionFactory&, const std::pair< boost::shared_ptr< NSCBC::BCBuilder<VolT> >, LBMS::PointCollection2D >&, const Expr::Tag&, const Expr::Tag& );  \
  template void TransportEquationBase<VolT>::build_one_sided_convection <DirT>( Expr::ExpressionFactory&, const std::pair< boost::shared_ptr< NSCBC::BCBuilder<VolT> >, LBMS::PointCollection2D >&, const Expr::Tag&, const Expr::Tag& );  \
  template void TransportEquationBase<VolT>::set_diffusive_flux_boundary<DirT>( Expr::ExpressionFactory&, const std::pair< boost::shared_ptr< NSCBC::BCBuilder<VolT> >, LBMS::PointCollection2D >&, const Expr::Tag&, const Expr::Tag&,  const int );  \
  template void TransportEquationBase<VolT>::zero_convective_flux       <DirT>( Expr::ExpressionFactory&, const std::pair< boost::shared_ptr< NSCBC::BCBuilder<VolT> >, LBMS::PointCollection2D >&, const Expr::Tag&, const Expr::Tag& );

  INSTANTIATETRANSPORTEQUATIONBASE( XVolField )
  INSTANTIATETRANSPORTEQUATIONBASE( YVolField )
  INSTANTIATETRANSPORTEQUATIONBASE( ZVolField )


} // namespace LBMS
