/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file ParseEquation.h
 * \author James C. Sutherland
 */

#ifndef ParseEquations_h
#define ParseEquations_h

#include <exprs/RHSTerms.h>
#include <lbms/GraphHelperTools.h>
#include <lbms/Bundle_fwd.h>
#include <lbms/Options.h>

namespace YAML{ class Node; }

namespace Expr{ class ExpressionFactory; }

/**
 *  \file   ParseEquation.h
 *  \brief  Parser tools for transport equations.
 *  \author James C. Sutherland
 */

namespace LBMS{

  class TimeStepper;
  template< typename FieldT >  class TransportEquationBase;

  /** \addtogroup Parser
   *  @{
   */


  /**
   *  \brief Build the transport equation specified by "params"
   *  \tparam T the type of field to build the transport equation for
   *  \param bundle the bundle that this equation is associated with
   *  \param params the tag from the input file specifying the
   *         transport equation.
   *  \param options
   *  \param densityTag a tag for the density to be passed to
   *         the scalar transport equations if it is needed.
   *         otherwise it will be an empty tag.
   *  \param gc the GraphCategories.
   *  \return an EqnTimestepAdaptorBase object that can be used to
   *          plug this transport equation into a TimeStepper.
   */
  template<typename T>
  TransportEquationBase<T>*
  parse_equation( const BundlePtr bundle,
                  const YAML::Node& params,
                  const Options& options,
                  const Expr::Tag& densityTag,
                  GraphCategories& gc,
                  const typename LBMS::NSCBCVecType<T>::NSCBCVec& nscbcInfo );

  /**
   *  \brief Build the momentum equations specified by "params"
   *  \param bundle the bundle that this equation is associated with
   *  \param params The XML block from the input file specifying the
   *         momentum equation. This will be \code{.xml} <MomentumEquations> \endcode
   *  \param options
   *  \param densityTag the density
   *  \param gc The GraphCategories.
   *  \return an EqnTimestepAdaptorBase object that can be used to
   *          plug this transport equation into a TimeStepper.
   */
  template<typename T>
  std::vector<TransportEquationBase<T>*>
  parse_momentum_equations( const BundlePtr bundle,
                            const YAML::Node& params,
                            const Options& options,
                            const Expr::Tag& densityTag,
                            GraphCategories& gc,
                            const typename LBMS::NSCBCVecType<T>::NSCBCVec& nscbcInfo );

  /**
   * \brief Register diffusive flux calculation,
   *        \f$J_\phi = -\rho \Gamma_\phi \nabla \phi\f$,
   *        for the scalar quantity \f$ \phi \f$.
   *
   * \param bundle the bundle that this equation is associated with
   * \param diffFluxParams Parser block "DiffusiveFluxExpression"
   * \param densityTag the density
   * \param primVarTag the primitive variable to be advected (\f$ \phi \f$).
   * \param factory the factory to register the resulting expression on
   * \param info the FieldTagInfo object that will be populated with the
   *        appropriate convective flux entry.
   * \param gc
   */
  template< typename FieldT>
  void setup_diffusive_flux_expression( const BundlePtr bundle,
                                        const YAML::Node& diffFluxParams,
                                        const Expr::Tag& densityTag,
                                        const Expr::Tag& primVarTag,
                                        Expr::ExpressionFactory& factory,
                                        FieldTagInfo& info,
                                        GraphCategories& gc);

  /**
   * \brief Register convective flux calculation for the given scalar quantity
   *
   * \param bundle the bundle that this equation is associated with
   * \param convFluxParams Parser block "ConvectiveFluxExpression"
   * \param solnVarTag the solution variable to be advected
   * \param factory the factory to register the resulting expression on
   * \param info the FieldTagInfo object that will be populated with the
   *        appropriate convective flux entry.
   * \param gc
   */
  template< typename FieldT >
  void setup_convective_flux_expression( const BundlePtr bundle,
                                         const YAML::Node& convFluxParams,
                                         const Expr::Tag& solnVarTag,
                                         Expr::ExpressionFactory& factory,
                                         FieldTagInfo& info,
                                         GraphCategories& gc);
  /**
  * \brief Setup communication for coarse and fine fields across processors
  *
  * This sets up ghost cell exchanges as well as coarse flux and volume exchanges.
  * It should be called by each bundle, since that will set up both the sends and corresponding receives.
  *
  * \param bundle the bundle that this equation is associated with
  * \param fieldTag the field to be coarsened or refined (will check for fluxfacedir == bundledir or LBMS::NODIR)
  * \param factory the factory to register the resulting expression on
  * \param gc
  * \param srcBundleDir the Direction of the source (originating) bundle
  * \param targetBundleDir the Direction of the destination (target) bundle
  */
  template< typename FieldT >
  void setup_field_exchange( const BundlePtr bundle,
                             const Expr::Tag fieldTag,
                             Expr::ExpressionFactory& factory,
                             GraphCategories& gc,
                             const LBMS::Direction srcBundleDir,
                             const LBMS::Direction targetBundleDir );

  /** @} */

}// namespace LBMS

#endif // ParseEquations_h
