/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef MomentumEquation_h
#define MomentumEquation_h

//--- LBMS Headers ---//
#include <transport/TransportEquationBase.h>
#include <lbms/Options.h>
#include <fields/StringNames.h>

/**
 *  @class  MomentumEquation
 *  @author James C. Sutherland
 *
 *  @brief  Manages creation of momentum transport equations
 *
 *  The mometnum equations are given as
 *  \f[
 *    \frac{\partial \rho \mathbf{u}}{\partial t} = -\nabla\cdot \rho\mathbf{u} - \nabla\cdot\tau - \nabla p +\mathbf{f}
 *  \f]
 *  or in Einstein (index) notation,
 *  \f[
 *    \frac{\partial \rho u_i}{\partial t} = -\frac{\partial \rho u_i u_j}{\partial x_j} - \frac{\partial \tau_{ji}}{\partial x_j} - \frac{\partial p}{\partial x_i} + f_i
 *  \f]
 *  with Newton's law of viscosity for the stress tensor,
 *  \f[
 *    \tau_{ij} = -\mu \left( \frac{\partial u_i}{\partial x_j} + \frac{\partial u_j}{\partial x_i} \right) + \delta_{ij} \frac{2}{3}(\mu-\kappa) \frac{\partial u_k}{\partial x_k}
 *  \f]
 *
 *  \par Template Parameters
 *  <ul>
 *  <li> \b ScalarT - the field type for the momentum component being
 *       solved here.
 *  <li> \b CompDirT - the direction for this momentum component
 *  </ul>
 */

template< typename ScalarT,
          typename CompDirT >
class MomentumEquation
  : public LBMS::TransportEquationBase< ScalarT >
{
  typedef typename LBMS::GetPerpDirType<CompDirT>::Perp1DirT      CompPerp1DirT;
  typedef typename LBMS::GetPerpDirType<CompDirT>::Perp2DirT      CompPerp2DirT;

  typedef LBMS::TransportEquationBase<ScalarT> BaseT;
  typedef typename MomentumEquation<ScalarT, CompDirT>::BaseT::template FaceSelector< typename BaseT::BundleDirT, CompDirT      >::type CompParFluxT;   ///< Par   Component Flux dir
  typedef typename MomentumEquation<ScalarT, CompDirT>::BaseT::template FaceSelector< typename BaseT::BundleDirT, CompPerp1DirT >::type CompPerp1FluxT; ///< Perp1 Component Flux dir
  typedef typename MomentumEquation<ScalarT, CompDirT>::BaseT::template FaceSelector< typename BaseT::BundleDirT, CompPerp2DirT >::type CompPerp2FluxT; ///< Perp2 Component Flux dir

  typedef typename LBMS::CoarseFieldSelector<CompParFluxT>::type   CompParCoarseT;
  typedef typename LBMS::CoarseFieldSelector<CompPerp1FluxT>::type CompPerp1CoarseT;   ///< Coarse Flux in direction 2
  typedef typename LBMS::CoarseFieldSelector<CompPerp2FluxT>::type CompPerp2CoarseT;   ///< Coarse Flux in direction 3

public:

  /**
   *  \brief Construct a MomentumEquation along with all expressions required to calculate its RHS.
   *
   *  \param exprFactory The factory where expression builders will be
   *         registered and expressions will be retrieved from.
   *
   *  \param bundle The Bundle associated with this equation.
   *
   *  \param options The Options object that controls various problem
   *         setup parameters.
   *
   *  \param doAdvection true to include advective terms
   *  \param gc
   */
  MomentumEquation( Expr::ExpressionFactory& exprFactory,
                    const LBMS::BundlePtr bundle,
                    const LBMS::Options& options,
                    const bool doAdvection, 
                    GraphCategories& gc,
                    const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

  ~MomentumEquation();


  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

  void setup_boundary_conditions( GraphCategories& gc,
                                  const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

private:

  const StringNames& sName_;
  const LBMS::Direction component_;

  const LBMS::Direction parDir_;  ///< The first direction
  const LBMS::Direction perp1Dir_;  ///< The second direction
  const LBMS::Direction perp2Dir_;  ///< The third direction

  const bool do1_, do2_, do3_;

  const int id_;

  const LBMS::Options& options_;

  const std::string parBundleName_, perpBundleName1_, perpBundleName2_;
  const std::string vel1_, vel2_, vel3_;
  const std::string velgrad11_, velgrad12_, velgrad13_;
  const std::string velgrad21_, velgrad22_, velgrad23_;
  const std::string velgrad31_, velgrad32_, velgrad33_;
  const std::string advel1_, advel2_, advel3_;
  const std::string pface_, gradPressure_;
  const std::string parFace_, perp1Face_, perp2Face_;

  static std::string get_var_name( const LBMS::Direction component );

  static Expr::Tag get_rhs_tag( const LBMS::Direction dir );

};


#endif // MomentumEquation_h
