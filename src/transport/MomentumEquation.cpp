/*
 * The MIT License
 *
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <transport/MomentumEquation.h>

#include <transport/ParseEquation.h>
#include <lbms/Bundle.h>
#include <mpi/FieldBundleExchange.h>
#include <operators/Operators.h>

//--- Expressions that we build here ---//
#include <exprs/DensityWeight.h>
#include <exprs/ScalarRHS.h>
#include <exprs/RHSTerms.h>
#include <exprs/Pressure.h>
#include <exprs/AdvectVelocity.h>
#include <exprs/Derivative.h>
#include <exprs/Stress.h>
#include <exprs/NormalStress_dil.h>
#include <exprs/Dilatation.h>
#include <exprs/PrimVar.h>
#include <exprs/FluxInterpolation.h>

//--- PoKiTT Headers ---//
#include <pokitt/thermo/Pressure.h>
#include <pokitt/transport/ViscosityMix.h>

//--- NSCBC Headers ---//
#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>
#include <operators/OneSidedPressureDiv.h>
#include <operators/BoxFilter.h>

template< typename ScalarT, typename CompDirT >
MomentumEquation<ScalarT,CompDirT>::
MomentumEquation( Expr::ExpressionFactory& exprFactory,
                  const LBMS::BundlePtr bundle,
                  const LBMS::Options& options,
                  const bool doAdvection,
                  GraphCategories& gc,
                  const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
  : LBMS::TransportEquationBase<ScalarT>( bundle,
                                          get_var_name(LBMS::direction<CompDirT>()),
                                          get_rhs_tag( bundle->dir() ) ),

    sName_( StringNames::self() ),

    component_( LBMS::direction<CompDirT>() ),

    parDir_  ( bundle->dir() ),
    perp1Dir_( get_perp1( bundle->dir() )  ),
    perp2Dir_( get_perp2( bundle->dir() )  ),

    do1_( bundle->npts(parDir_  ) > 1 ),
    do2_( bundle->npts(perp1Dir_) > 1 ),
    do3_( bundle->npts(perp2Dir_) > 1 ),

    id_( bundle->id() ),

    options_( options ),

    parBundleName_  ( bundle->name() ),
    perpBundleName1_( get_bundle_name_suffix( get_perp1( bundle->dir() ) ) ),
    perpBundleName2_( get_bundle_name_suffix( get_perp2( bundle->dir() ) ) ),

    vel1_( select_from_dir( parDir_,   sName_.xvel, sName_.yvel, sName_.zvel ) ),
    vel2_( select_from_dir( perp1Dir_, sName_.xvel, sName_.yvel, sName_.zvel ) ),
    vel3_( select_from_dir( perp2Dir_, sName_.xvel, sName_.yvel, sName_.zvel ) ),

    velgrad11_( vel1_ + select_from_dir( parDir_,   sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad12_( vel1_ + select_from_dir( perp1Dir_, sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad13_( vel1_ + select_from_dir( perp2Dir_, sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad21_( vel2_ + select_from_dir( parDir_,   sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad22_( vel2_ + select_from_dir( perp1Dir_, sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad23_( vel2_ + select_from_dir( perp2Dir_, sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad31_( vel3_ + select_from_dir( parDir_,   sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad32_( vel3_ + select_from_dir( perp1Dir_, sName_.dx, sName_.dy, sName_.dz ) ),
    velgrad33_( vel3_ + select_from_dir( perp2Dir_, sName_.dx, sName_.dy, sName_.dz ) ),

    advel1_( vel1_ + sName_.advect ),
    advel2_( vel2_ + sName_.advect ),
    advel3_( vel3_ + sName_.advect ),

    pface_( sName_.pressure + select_from_dir( component_, sName_.xface, sName_.yface, sName_.zface ) ),
    gradPressure_( select_from_dir( component_, sName_.dpdx, sName_.dpdy, sName_.dpdz) ),

    parFace_  ( select_from_dir( parDir_,   sName_.xface, sName_.yface, sName_.zface ) ),
    perp1Face_( select_from_dir( perp1Dir_, sName_.xface, sName_.yface, sName_.zface ) ),
    perp2Face_( select_from_dir( perp2Dir_, sName_.xface, sName_.yface, sName_.zface ) )
{
  using Expr::STATE_N;
  using Expr::STATE_NONE;
  using Expr::STATE_NP1;
  using Expr::Tag;

  using namespace LBMS;

  const StringNames& sName = StringNames::self();

  const Tag advectVelTag1 =              create_tag( advel1_, STATE_NONE, parDir_ );
  const Tag advectVelTag2 = this->do2_ ? create_tag( advel2_, STATE_NONE, parDir_ ) : Tag();
  const Tag advectVelTag3 = this->do3_ ? create_tag( advel3_, STATE_NONE, parDir_ ) : Tag();

  const Expr::TagList speciesTags = options_.pokittOptions.species_tags( parDir_, STATE_NONE );

  const Tag gradPTag       = create_tag( gradPressure_,                      STATE_NONE, parDir_ );
  const Tag pressure       = create_tag( options_.pokittOptions.pressure,    STATE_NONE, parDir_ );
  const Tag temperatureTag = create_tag( options_.pokittOptions.temperature, STATE_NONE, parDir_ );
  const Tag viscosityTag   = create_tag( options_.pokittOptions.viscosity,   STATE_NONE, parDir_ );
  const Tag densityTag     = create_tag( sName_.density,                     STATE_N,    parDir_ );
  const Tag mwTag          = create_tag( sName_.mixmw,                       STATE_NONE, parDir_ );

  const Tag momentumTag = create_tag( this->get_var_name(component_), STATE_N, parDir_ );

  //tags for modifier expressions
  const Tag modCFluxTag1( "modcflux1", STATE_NONE );
  const Tag modCFluxTag2( "modcflux2", STATE_NONE );
  const Tag modDFluxTag1( "moddflux1", STATE_NONE );
  const Tag modDFluxTag2( "moddflux2", STATE_NONE );

  //
  // build expressions that are required only once per bundle (not unique per equation)
  //
  if( component_ == parDir_ ){
    //-----------------------------------------------------------
    //Dilatation
    const Tag dilTag = create_tag( sName.dilatation, STATE_NONE, parDir_ );
    typedef typename Dilatation<typename BaseT::ParDivT, typename BaseT::Perp1DivT, typename BaseT::Perp2DivT>::Builder DilT;
    exprFactory.register_expression( new DilT( dilTag, advectVelTag1, advectVelTag2, advectVelTag3 ), false, id_ );
    LBMS::communicate_expression_result<ScalarT>( dilTag, bundle, exprFactory );

    //-----------------------------------------------------------
    // Velocity Gradient
    // creates velocity derivatives for use in the stress expressions

    typedef typename Derivative<typename BaseT::ParFluxT,ScalarT>::Builder DerTBuilder;
    exprFactory.register_expression( new DerTBuilder( create_tag( velgrad11_, STATE_NONE, parDir_ ),
                                                      create_tag( vel1_,      STATE_NONE, parDir_ ) ),
                                     false, id_ );

    //Necessary for flux reconstruction
    if( do2_ ){
      typedef typename Derivative<typename BaseT::Perp1FluxT,ScalarT>::Builder DerTBuilder;
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad22_, STATE_NONE, parDir_ ),
                                                        create_tag( vel2_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );
      typedef typename Derivative<typename BaseT::Perp1FluxT,ScalarT>::Builder DerTBuilder;
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad12_, STATE_NONE, parDir_ ),
                                                        create_tag( vel1_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );
    }

    //Necessary for flux reconstruction
    if( do3_ ){
      typedef typename Derivative<typename BaseT::Perp2FluxT,ScalarT>::Builder DerTBuilder;
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad33_, STATE_NONE, parDir_ ),
                                                        create_tag( vel3_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );
      typedef typename Derivative<typename BaseT::Perp2FluxT,ScalarT>::Builder DerTBuilder;
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad13_, STATE_NONE, parDir_ ),
                                                        create_tag( vel1_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );
    }

    //Necessary for flux reconstruction
    if( do2_ && do3_ ){
      typedef typename Derivative<typename BaseT::Perp1FluxT,ScalarT>::Builder Der32TBuilder;
      exprFactory.register_expression( new Der32TBuilder( create_tag( velgrad32_, STATE_NONE, parDir_ ),
                                                          create_tag( vel3_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );

      typedef typename Derivative<typename BaseT::Perp2FluxT,ScalarT>::Builder Der23TBuilder;
      exprFactory.register_expression( new Der23TBuilder( create_tag( velgrad23_, STATE_NONE, parDir_ ),
                                                          create_tag( vel2_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );
    }


    if( do2_ ){
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad21_, STATE_NONE, parDir_ ),
                                                        create_tag( vel2_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Necessary for flux reconstruction
      {
        typedef typename FluxInterpolation<ScalarT,typename BaseT::ParFluxT>::Builder Perp1VelToVolInterp;
        exprFactory.register_expression( new Perp1VelToVolInterp( create_tag( velgrad21_+"_vol", STATE_NONE, parDir_ ),
                                                                  create_tag( velgrad21_,        STATE_NONE, parDir_ ) ),
                                         false, id_ );

        //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
        LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad21_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

        typedef typename FluxInterpolation<typename BaseT::Perp1FluxT,ScalarT>::Builder VolToPerp1FluxInterp;
        exprFactory.register_expression( new VolToPerp1FluxInterp( create_tag( velgrad21_+perp1Face_, STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad21_+"_vol",     STATE_NONE, parDir_ ) ),
                                         false, id_ );
      }
    }

    if( do3_ ){
      exprFactory.register_expression( new DerTBuilder( create_tag( velgrad31_, STATE_NONE, parDir_ ),
                                                        create_tag( vel3_,      STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Necessary for flux reconstruction
      {
        typedef typename FluxInterpolation<ScalarT,typename BaseT::ParFluxT>::Builder Perp2VelToVolInterp;
        exprFactory.register_expression( new Perp2VelToVolInterp( create_tag( velgrad31_+"_vol", STATE_NONE, parDir_ ),
                                                                  create_tag( velgrad31_,        STATE_NONE, parDir_ ) ),
                                         false, id_ );

        //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
        LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad31_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

        typedef typename FluxInterpolation<typename BaseT::Perp2FluxT,ScalarT>::Builder VolToPerp2FluxInterp;
        exprFactory.register_expression( new VolToPerp2FluxInterp( create_tag( velgrad31_+perp2Face_, STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad31_+"_vol",     STATE_NONE, parDir_ ) ),
                                         false, id_ );
      }
    }

    //stencil4 interpolation expression (e.g. xvel.dy, a YSurfY field becomes xvel.dy.xface, a YSurfX field)
    //necessary for a parallel bundle, perpendicular face stress
    //(e.g. xvel.dy.xface.ybundle, a YSurfX field, is used for TauXY on a Y bundle)

    if( do2_ ){
      typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp1FluxT>::Builder Perp1FluxToVolInterp;
      exprFactory.register_expression( new Perp1FluxToVolInterp( create_tag( velgrad12_+"_vol", STATE_NONE, parDir_ ),
                                                                 create_tag( velgrad12_,        STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
      LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad12_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

      typedef typename FluxInterpolation<typename BaseT::ParFluxT,ScalarT>::Builder VolToParFluxInterp;
      exprFactory.register_expression( new VolToParFluxInterp( create_tag( velgrad12_+parFace_, STATE_NONE, parDir_ ),
                                                               create_tag( velgrad12_+"_vol",   STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Necessary for flux reconstruction
      if( do3_ ){
        typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp1FluxT>::Builder Perp1FluxToVolInterp;
        exprFactory.register_expression( new Perp1FluxToVolInterp( create_tag( velgrad32_+"_vol", STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad32_,        STATE_NONE, parDir_ ) ),
                                         false, id_ );

        //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
        LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad32_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

        typedef typename FluxInterpolation<typename BaseT::Perp2FluxT,ScalarT>::Builder VolToPerp2FluxInterp;
        exprFactory.register_expression( new VolToPerp2FluxInterp( create_tag( velgrad32_+perp2Face_, STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad32_+"_vol",     STATE_NONE, parDir_ ) ),
                                         false, id_ );
      }
    }//End do2_

    if( do3_ ){
      typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp2FluxT>::Builder Perp2FluxToVolInterp;
      exprFactory.register_expression( new Perp2FluxToVolInterp( create_tag( velgrad13_+"_vol", STATE_NONE, parDir_ ),
                                                                 create_tag( velgrad13_,        STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
      LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad13_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

      typedef typename FluxInterpolation<typename BaseT::ParFluxT,ScalarT>::Builder VolToParFluxInterp;
      exprFactory.register_expression( new VolToParFluxInterp( create_tag( velgrad13_+parFace_, STATE_NONE, parDir_ ),
                                                               create_tag( velgrad13_+"_vol",   STATE_NONE, parDir_ ) ),
                                       false, id_ );

      //Necessary for flux reconstruction
      if( do2_ ){
        typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp2FluxT>::Builder Perp2FluxToVolInterp;
        exprFactory.register_expression( new Perp2FluxToVolInterp( create_tag( velgrad23_+"_vol", STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad23_,        STATE_NONE, parDir_ ) ),
                                         false, id_ );

        //Still necessary for refreshing ghost cells prior to VolToParFluxInterp
        LBMS::communicate_expression_result<ScalarT>( create_tag( velgrad23_+"_vol", STATE_NONE, parDir_ ), bundle, exprFactory );

        typedef typename FluxInterpolation<typename BaseT::Perp1FluxT,ScalarT>::Builder VolToPerp1FluxInterp;
        exprFactory.register_expression( new VolToPerp1FluxInterp( create_tag( velgrad23_+perp1Face_, STATE_NONE, parDir_ ),
                                                                   create_tag( velgrad23_+"_vol",     STATE_NONE, parDir_ ) ),
                                         false, id_ );
      }
    }//End do3_


    //-----------------------------------------------------------
    // cell pressure
    typedef typename pokitt::Pressure<ScalarT>::Builder CellPressure;
    exprFactory.register_expression( new CellPressure( pressure, temperatureTag, densityTag, mwTag ), false, id_ );

    //-----------------------------------------------------------
    // viscosity
    if( !exprFactory.have_entry(viscosityTag) ){
      typedef typename pokitt::Viscosity<ScalarT>::Builder Viscosity;
      exprFactory.register_expression( new Viscosity( viscosityTag, temperatureTag, speciesTags ), false, id_ );
    }

    //-----------------------------------------------------------
    // advecting velocities
    typedef typename AdvectVelocity<typename BaseT::ParInterpT>::Builder AdvVel1;
    exprFactory.register_expression( new AdvVel1( advectVelTag1, create_tag(vel1_,STATE_NONE,parDir_) ), false, id_ );

    if( do2_ ){
      typedef typename AdvectVelocity<typename BaseT::Perp1InterpT>::Builder AdvVel2;
      exprFactory.register_expression( new AdvVel2( advectVelTag2, create_tag(vel2_,STATE_NONE,parDir_) ), false, id_ );
    }

    if( do3_ ){
      typedef typename AdvectVelocity<typename BaseT::Perp2InterpT>::Builder AdvVel3;
      exprFactory.register_expression( new AdvVel3( advectVelTag3, create_tag(vel3_,STATE_NONE,parDir_) ), false, id_ );
    }

  }// expressions built once per bundle
  //-------------------------------------------------------------

  //-------------------------------------------------------------
  // extract velocity from momentum
  {
    std::string velName;
    switch( component_ ){
      case LBMS::XDIR:  velName=sName.xvel; break;
      case LBMS::YDIR:  velName=sName.yvel; break;
      case LBMS::ZDIR:  velName=sName.zvel; break;
      case LBMS::NODIR: assert(false); // shouldn't get here.
    }
    typedef typename PrimVar<ScalarT>::Builder PrimVar;
    const Tag velTag = create_tag( velName, STATE_NONE, parDir_ );
    exprFactory.register_expression( new PrimVar( velTag, momentumTag, densityTag ), false, id_);
  }

  //-------------------------------------------------------------
  // diffusive fluxes
  {
    const Tag tau1Tag = create_tag( get_tau_name( component_,            component_ ), STATE_NONE, parDir_ );
    const Tag tau2Tag = create_tag( get_tau_name( get_perp1(component_), component_ ), STATE_NONE, parDir_ );
    const Tag tau3Tag = create_tag( get_tau_name( get_perp2(component_), component_ ), STATE_NONE, parDir_ );

    std::string tau1Vel;
    std::string tau2Vel1;
    std::string tau2Vel2;
    std::string tau3Vel1;
    std::string tau3Vel2;

    if( parDir_ == component_ ){
      tau1Vel  = velgrad11_;
      tau2Vel1 = velgrad12_;
      tau2Vel2 = velgrad21_+perp1Face_;
      tau3Vel1 = velgrad13_;
      tau3Vel2 = velgrad31_+perp2Face_;
    }
    else if( perp1Dir_ == component_ ){
      tau1Vel = velgrad22_;
      if( parDir_ == LBMS::XDIR || parDir_ == LBMS::YDIR ){
        tau2Vel1 = velgrad21_;
        tau2Vel2 = velgrad12_+parFace_;
        tau3Vel1 = velgrad23_;
        tau3Vel2 = velgrad32_+perp2Face_;
      }
      else if( parDir_ == LBMS::ZDIR ){
        tau3Vel1 = velgrad21_;
        tau3Vel2 = velgrad12_+parFace_;
        tau2Vel1 = velgrad23_;
        tau2Vel2 = velgrad32_+perp2Face_;
      }
    }
    else if ( perp2Dir_ == component_ ){
      tau1Vel = velgrad33_;
      if( parDir_ == LBMS::XDIR ){
        tau2Vel1 = velgrad31_;
        tau2Vel2 = velgrad13_+parFace_;
        tau3Vel1 = velgrad32_;
        tau3Vel2 = velgrad23_+perp1Face_;
      }
      else if( parDir_ == LBMS::YDIR || parDir_ == LBMS::ZDIR ){
        tau3Vel1 = velgrad31_;
        tau3Vel2 = velgrad13_+parFace_;
        tau2Vel1 = velgrad32_;
        tau2Vel2 = velgrad23_+perp1Face_;
      }
    }

    const Tag tau1VelTag  = create_tag(  tau1Vel, STATE_NONE, parDir_ );
    const Tag tau2Vel1Tag = create_tag( tau2Vel1, STATE_NONE, parDir_ );
    const Tag tau2Vel2Tag = create_tag( tau2Vel2, STATE_NONE, parDir_ );
    const Tag tau3Vel1Tag = create_tag( tau3Vel1, STATE_NONE, parDir_ );
    const Tag tau3Vel2Tag = create_tag( tau3Vel2, STATE_NONE, parDir_ );

    if( parDir_ == component_ ){
      typedef typename NormalStress<CompParFluxT>::Builder Tau1;
      exprFactory.register_expression( new Tau1( tau1Tag,
                                                 tau1VelTag,
                                                 create_tag( sName.dilatation, STATE_NONE, parDir_ ),
                                                 viscosityTag ),
                                       false, id_ );
      LBMS::setup_field_exchange< typename BaseT::ParFluxT>( bundle, tau1Tag, exprFactory, gc, parDir_, parDir_ );

      if( do2_ ){
        typedef typename Stress<CompPerp1FluxT>::Builder Tau2;
        const Tag tau2FineTag = create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.fine, STATE_NONE, parDir_ );

        exprFactory.register_expression( new Tau2( tau2FineTag, tau2Vel1Tag, tau2Vel2Tag, viscosityTag ), false, id_ );

        LBMS::setup_field_exchange<CompPerp1FluxT>( bundle, tau2Tag, exprFactory, gc, perp1Dir_, parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompPerp1FluxT, CompPerp1CoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau2FineTag,
                                                                  bundle ),
                                     false, id_  );
      }
      if( do3_ ){
        typedef typename Stress<CompPerp2FluxT>::Builder Tau3;
        const Tag tau3FineTag = create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.fine, STATE_NONE, parDir_ );

        exprFactory.register_expression( new Tau3( tau3FineTag, tau3Vel1Tag, tau3Vel2Tag, viscosityTag ), false, id_ );

        LBMS::setup_field_exchange<CompPerp2FluxT>( bundle, tau3Tag, exprFactory, gc, perp2Dir_, parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompPerp2FluxT, CompPerp2CoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau3FineTag,
                                                                  bundle ),
                                     false, id_  );
      }
    }

    else if( parDir_ == get_perp1(component_) ){
      //Normal Flux reconstruction
      {
        const Tag tau1FineTag = create_tag( get_tau_name( component_, component_ ) + sName_.fine, STATE_NONE, parDir_ );

        typedef typename NormalStress<CompParFluxT>::Builder Tau1;
        exprFactory.register_expression( new Tau1( tau1FineTag,
                                                   tau1VelTag,
                                                   create_tag( sName.dilatation, STATE_NONE, parDir_ ),
                                                   viscosityTag ),
                                         false, id_ );

        LBMS::setup_field_exchange<CompParFluxT>( bundle, tau1Tag, exprFactory, gc, component_, parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( component_, component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompParFluxT, CompParCoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( component_, component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau1FineTag,
                                                                  bundle ),
                                     false, id_  );
      }

      //On Bundle Flux
      typedef typename Stress<CompPerp1FluxT>::Builder Tau2;
      exprFactory.register_expression( new Tau2( tau2Tag, tau2Vel1Tag, tau2Vel2Tag, viscosityTag ), false, id_ );
      LBMS::setup_field_exchange<CompPerp1FluxT>( bundle, tau2Tag, exprFactory, gc, parDir_, parDir_ );

      //Non-normal, flux reconstruction
      if( do2_ && do3_ ){
        typedef typename Stress<CompPerp2FluxT>::Builder Tau3;
        const Tag tau3FineTag = create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.fine, STATE_NONE, parDir_ );

        exprFactory.register_expression( new Tau3( tau3FineTag, tau3Vel1Tag, tau3Vel2Tag, viscosityTag ), false, id_ );

        LBMS::setup_field_exchange<CompPerp2FluxT>( bundle, tau3Tag, exprFactory, gc, get_perp2(component_), parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompPerp2FluxT, CompPerp2CoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( get_perp2(component_), component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau3FineTag,
                                                                  bundle ),
                                     false, id_  );
      }
    }

    else if( parDir_ == get_perp2(component_) ){
      //Normal Flux reconstruction
      {
        const Tag tau1FineTag = create_tag( get_tau_name( component_, component_ ) + sName_.fine, STATE_NONE, parDir_ );

        typedef typename NormalStress<CompParFluxT>::Builder Tau1;
        exprFactory.register_expression( new Tau1( tau1FineTag,
                                                   tau1VelTag,
                                                   create_tag( sName.dilatation, STATE_NONE, parDir_ ),
                                                   viscosityTag ),
                                         false, id_ );

        LBMS::setup_field_exchange<CompParFluxT>( bundle, tau1Tag, exprFactory, gc, component_, parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( component_, component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompParFluxT, CompParCoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( component_, component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau1FineTag,
                                                                  bundle ),
                                     false, id_  );
      }
      //Non-normal, flux reconstruction
      if( do2_ && do3_ ){
        typedef typename Stress<CompPerp1FluxT>::Builder Tau2;
        const Tag tau2FineTag = create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.fine, STATE_NONE, parDir_ );

        exprFactory.register_expression( new Tau2( tau2FineTag, tau2Vel1Tag, tau2Vel2Tag, viscosityTag ), false, id_ );

        LBMS::setup_field_exchange<CompPerp1FluxT>( bundle, tau2Tag, exprFactory, gc, get_perp1(component_), parDir_ );

        const Expr::Tag reconsTag = create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

        typedef typename OffLineFlux< CompPerp1FluxT, CompPerp1CoarseT >::Builder OffLineFluxBuilder;
        exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                                  create_tag( get_tau_name( get_perp1(component_), component_ ) + sName_.coarse, Expr::STATE_NONE, parDir_ ),
                                                                  tau2FineTag,
                                                                  bundle ),
                                     false, id_  );
      }

      //On Bundle Flux
      typedef typename Stress<CompPerp2FluxT>::Builder Tau3;
      exprFactory.register_expression( new Tau3( tau3Tag, tau3Vel1Tag, tau3Vel2Tag, viscosityTag ), false, id_ );

      LBMS::setup_field_exchange<CompPerp2FluxT>( bundle, tau3Tag, exprFactory, gc, parDir_, parDir_ );
    }
  }

  FieldTagInfo fieldTagInfo;

  //-------------------------------------------------------------
  // convective fluxes
  //
  if( doAdvection ) this->build_convective_fluxes( exprFactory, gc, fieldTagInfo );

  //-------------------------------------------------------------
  // pressure rhs term
  const Tag coarseGradPTag = create_tag( gradPressure_ + sName_.coarse, STATE_NONE, parDir_ );
  Tag gradPReconsTag;
  {
    const Tag pfaceTag = create_tag( pface_, STATE_NONE, parDir_ );

    if( component_ == parDir_ ){
      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence,  typename BaseT::ParFluxT, ScalarT >::type PressureDivT;
      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::ParFluxT >::type PressureInterpT;

      typedef typename PressureInterp<PressureInterpT>::Builder PFace;
      exprFactory.register_expression( new PFace( pfaceTag, pressure ), false, id_ );

      typedef typename PressureRHSTerm<PressureDivT>::Builder PRHS;
      exprFactory.register_expression( new PRHS( gradPTag, pfaceTag ), false, id_ );

      LBMS::setup_field_exchange<ScalarT>( bundle, gradPTag, exprFactory, gc, parDir_, parDir_ );
    }
    else if( component_ == perp1Dir_ ){
      LBMS::setup_field_exchange<ScalarT>( bundle, gradPTag, exprFactory, gc, perp1Dir_,  parDir_  );

      const std::string perp1Face = select_from_dir( perp1Dir_, sName_.xface, sName_.yface, sName_.zface );

      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence,  typename BaseT::Perp1FluxT, ScalarT >::type PressureDivT;
      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::Perp1FluxT >::type PressureInterpT;

      typedef typename PressureInterp<PressureInterpT>::Builder PFace;

      exprFactory.register_expression( new PFace( pfaceTag, pressure ), false, id_ );

      const Tag gradPFineTag = create_tag( gradPressure_ + sName_.fine, STATE_NONE, parDir_ );
      typedef typename PressureRHSTerm<PressureDivT>::Builder PRHS;
      exprFactory.register_expression( new PRHS( gradPFineTag, pfaceTag ), false, id_ );

      gradPReconsTag = create_tag( gradPressure_ + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

      typedef typename LBMS::CoarseFieldSelector<ScalarT>::type VolCoarseT;

      typedef typename OffLineFlux< ScalarT, VolCoarseT >::Builder OffLineFluxBuilder;
      exprFactory.register_expression( new OffLineFluxBuilder ( gradPReconsTag,
                                                                coarseGradPTag,
                                                                gradPFineTag,
                                                                bundle ),
                                       false,  id_ );
    }
    else if( component_ == perp2Dir_ ){
      LBMS::setup_field_exchange<ScalarT>( bundle, gradPTag, exprFactory, gc, perp2Dir_,  parDir_  );

      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence,  typename BaseT::Perp2FluxT, ScalarT >::type PressureDivT;
      typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::Perp2FluxT >::type PressureInterpT;

      typedef typename PressureInterp<PressureInterpT>::Builder PFace;

      exprFactory.register_expression( new PFace( pfaceTag, pressure ), false, id_ );

      typedef typename PressureRHSTerm<PressureDivT>::Builder PRHS;

      const Tag gradPFineTag = create_tag( gradPressure_ + sName_.fine, STATE_NONE, parDir_ );
      exprFactory.register_expression( new PRHS( gradPFineTag, pfaceTag ), false, id_ );

      gradPReconsTag = create_tag( gradPressure_ + sName_.reconstructed, Expr::STATE_NONE, parDir_ );

      typedef typename LBMS::CoarseFieldSelector<ScalarT>::type VolCoarseT;

      typedef typename OffLineFlux< ScalarT, VolCoarseT >::Builder OffLineFluxBuilder;
      exprFactory.register_expression( new OffLineFluxBuilder ( gradPReconsTag,
                                                                coarseGradPTag,
                                                                gradPFineTag,
                                                                bundle ),
                                       false,  id_ );
    }
    else{ assert(false); }
  }


  // RHS Expression
  {
    const std::string xFace = select_from_dir( LBMS::XDIR, sName.xface, sName.yface, sName.zface );
    const std::string yFace = select_from_dir( LBMS::YDIR, sName.xface, sName.yface, sName.zface );
    const std::string zFace = select_from_dir( LBMS::ZDIR, sName.xface, sName.yface, sName.zface );

    const LBMS::Direction component = LBMS::direction<CompDirT>();
    const LBMS::Direction dir = bundle->dir();

    const std::string convmom = get_var_name( component );
    const std::string thismom = get_var_name( component );

    const bool doX = bundle->npts( LBMS::XDIR ) > 1;
    const bool doY = bundle->npts( LBMS::YDIR ) > 1;
    const bool doZ = bundle->npts( LBMS::ZDIR ) > 1;

    const std::string xAppendage = ( dir == LBMS::XDIR ? "" : sName_.reconstructed );
    const std::string yAppendage = ( dir == LBMS::YDIR ? "" : sName_.reconstructed );
    const std::string zAppendage = ( dir == LBMS::ZDIR ? "" : sName_.reconstructed );

    if( doX ) fieldTagInfo[ DIFFUSIVE_FLUX_X ] = create_tag( get_tau_name( LBMS::XDIR, component ) + xAppendage, STATE_NONE, dir );;
    if( doY ) fieldTagInfo[ DIFFUSIVE_FLUX_Y ] = create_tag( get_tau_name( LBMS::YDIR, component ) + yAppendage, STATE_NONE, dir );;
    if( doZ ) fieldTagInfo[ DIFFUSIVE_FLUX_Z ] = create_tag( get_tau_name( LBMS::ZDIR, component ) + zAppendage, STATE_NONE, dir );;

    if( component != parDir_) fieldTagInfo[ SOURCE_TERM ] = gradPReconsTag;
    else                      fieldTagInfo[ SOURCE_TERM ] = gradPTag;

    typedef typename ScalarRHS<ScalarT>::Builder MomRHS;
    exprFactory.register_expression( new MomRHS( create_tag( thismom+"_RHS", STATE_NONE, dir ), fieldTagInfo ),
                                     false, bundle->id() );

  }
  this->setup_boundary_conditions( gc, nscbcVec );
}

//--------------------------------------------------------------------

template< typename ScalarT, typename CompDirT >
MomentumEquation<ScalarT,CompDirT>::
~MomentumEquation()
{}

//--------------------------------------------------------------------

template< typename ScalarT, typename CompDirT >
Expr::ExpressionID
MomentumEquation<ScalarT,CompDirT>::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  /// \todo need to rework this.
  using Expr::Tag;
  using Expr::STATE_N;
  using Expr::STATE_NONE;

  const StringNames& sName = StringNames::self();
  const std::string bundlename = this->bundle_->name();
  const std::string vel = select_from_dir( component_, sName.xvel, sName.yvel, sName.zvel );
  typedef typename DensityWeight<ScalarT>::Builder IC;
  return exprFactory.register_expression( new IC( Tag( this->solnVarName_, STATE_N ),
                                                  create_tag(vel,          STATE_NONE,parDir_),
                                                  create_tag(sName.density,STATE_N,   parDir_) ),
                                          false, id_ );
}


//-----------------------------------------------------------------

template<typename ScalarT, typename CompDirT >
void
MomentumEquation<ScalarT, CompDirT>::
setup_boundary_conditions( GraphCategories& gc, const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
{
  using Expr::STATE_NONE;

  Expr::ExpressionFactory& factory = *gc[ADVANCE_SOLUTION]->exprFactory;

  //NSCBC Momentum Direction
  const NSCBC::TransportVal nscbcType = select_from_dir( component_,
                                                         NSCBC::MOMENTUM_X,
                                                         NSCBC::MOMENTUM_Y,
                                                         NSCBC::MOMENTUM_Z );

  //Tags and builders for velocity and pressure extrapolation
  Expr::ExpressionBuilder* vBuilder = NULL;
  Expr::ExpressionBuilder* pBuilder = NULL;

  const std::string velString = select_from_dir( component_, sName_.xvel, sName_.yvel, sName_.zvel );
  const Expr::Tag velTag      = create_tag( velString,       STATE_NONE, parDir_ );
  const Expr::Tag pressureTag = create_tag( sName_.pressure, STATE_NONE, parDir_ );

  typedef const std::pair< boost::shared_ptr< NSCBC::BCBuilder<ScalarT> >, LBMS::PointCollection2D > NSCBCPairT;

  BOOST_FOREACH( const NSCBCPairT& bc, nscbcVec ){

    const Expr::Tag dpTag = ( LBMS::Direction((bc.first)->get_boundary_direction() ) == parDir_ ) ?
                            create_tag( select_from_dir( component_, sName_.dpdx, sName_.dpdy, sName_.dpdz), STATE_NONE, parDir_ ) :
                            create_tag( select_from_dir( component_, sName_.dpdx, sName_.dpdy, sName_.dpdz) + sName_.fine, STATE_NONE, parDir_ );

    (bc.first)->attach_rhs_modifier( factory, this->get_rhs_tag(parDir_), nscbcType, -1 );

    //Pulling Strings associated with the characteristic boundary condition
    const std::string sideString = bc.first->side();
    const std::string faceString = bc.first->face();
    const std::string typeString = bc.first->type();
    const std::string jobString  = bc.first->get_job_name();

    const Expr::Tag pModTag   = create_tag( sName_.pressure + typeString + faceString + sideString + jobString, STATE_NONE, parDir_ );

    const NSCBC::BCType bT =  bc.first->get_boundary_type();
    if( bT == NSCBC::NONREFLECTING || bT == NSCBC::HARD_INFLOW ){
      //Normal stress check
      int isNormTau;
      if( component_== LBMS::direction(bc.first->get_boundary_direction()) ) isNormTau =  1;
      else                                                 isNormTau = -1;

      //Stress Tags
      const std::string tauString = get_tau_name( LBMS::direction(bc.first->get_boundary_direction()), component_ );
      const Expr::Tag tauTag = ( LBMS::direction(bc.first->get_boundary_direction()) == parDir_ ) ?
                               create_tag( tauString, STATE_NONE, parDir_ ) :
                               create_tag( tauString + sName_.fine, STATE_NONE, parDir_ );
      const Expr::Tag tauModTag1 = create_tag( tauString + "_mod1" + typeString + faceString + sideString + jobString, STATE_NONE, parDir_ );
      const Expr::Tag tauModTag2 = create_tag( tauString + "_mod2" + typeString + faceString + sideString + jobString, STATE_NONE, parDir_ );

      //Modifiers for pressure and velocity
      const Expr::Tag velModTag = create_tag( velString + typeString + faceString + sideString + jobString,       STATE_NONE, parDir_ );

      //Applying conditions to velocity derivatives and stress terms.
      switch( bc.first->get_boundary_direction() ){
        case SpatialOps::XDIR::value:
          this->template build_nscbc_neumann<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag2, tauTag, isNormTau );
          if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::XDIR>::type::Negate UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag,
                                                 bc.first->get_mask(),
                                                 LBMS::XDIR,
                                                 bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag,
                                            bc.first->get_mask(),
                                            pressureTag );
          }
          else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::XDIR>::type UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag, bc.first->get_mask(), LBMS::XDIR, bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag, bc.first->get_mask(), pressureTag );
          }
          break;
        case SpatialOps::YDIR::value:
          this->template build_nscbc_neumann<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag2, tauTag, isNormTau );
          if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::YDIR>::type::Negate UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag, bc.first->get_mask(), LBMS::YDIR, bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag, bc.first->get_mask(), pressureTag );
          }
          else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::YDIR>::type UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag, bc.first->get_mask(), LBMS::YDIR, bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag, bc.first->get_mask(), pressureTag );
          }
          break;
        case SpatialOps::ZDIR::value:
          this->template build_nscbc_neumann<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag2, tauTag, isNormTau );
          if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type::Negate UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag, bc.first->get_mask(), LBMS::ZDIR, bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag, bc.first->get_mask(), pressureTag );
          }
          else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
            //Stencil direction
            typedef typename SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type UnitTripletT;

            //Op Type for velocity extrapolation
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
            typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
            vBuilder = new ExtrapolationBuilder( velModTag, bc.first->get_mask(), LBMS::ZDIR, bc.first->get_boundary_side() );

            //Op Type for one sided pressure gradient
            typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type GOpT;
            typedef typename LBMS::OneSidedPressure<ScalarT,GOpT>::Builder PressureBuilder;
            pBuilder = new PressureBuilder( pModTag, bc.first->get_mask(), pressureTag );
          }
          break;
        case SpatialOps::NODIR::value:
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << std::endl
              << "Invalid direction in MomentumEquation NSCBC" << std::endl;
          throw std::runtime_error( msg.str() );
          break;
        }//End Switch

      //Attaching modifier expressions on terms to be extrapolated
      if( bT == NSCBC::NONREFLECTING ){
        factory.register_expression(vBuilder, false, id_);
        factory.attach_modifier_expression(velModTag, velTag, ALL_PATCHES, false);
      }

      if( bT == NSCBC::NONREFLECTING ){
        typedef typename Expr::ConstantMaskedExpression<ScalarT>::Builder BCExpr;

        pBuilder = new BCExpr( pModTag, (bc.second).template get_mask<ScalarT>(), 0.0 );
        if( component_ == LBMS::direction(bc.first->get_boundary_direction()) ){
          factory.register_expression(pBuilder, false, id_ );
          factory.attach_modifier_expression(pModTag, dpTag, ALL_PATCHES, false);
        }
      }
      else{
        if( component_ == LBMS::direction(bc.first->get_boundary_direction()) ){
          factory.register_expression(pBuilder, false, id_);
          factory.attach_modifier_expression(pModTag, dpTag, ALL_PATCHES, false);
        }
      }//end component == boundary dir
    }//end if (nonreflecting check)

    if( bT == NSCBC::HARD_INFLOW || bT == NSCBC::WALL ){
      //Only need this once
      if( !( factory.have_entry( create_tag( velgrad11_+"_vol", STATE_NONE, parDir_ ) ) ) ){
        typedef typename FluxInterpolation<ScalarT,typename BaseT::ParFluxT>::Builder ParFluxToVolInterp;
        factory.register_expression( new ParFluxToVolInterp( create_tag( velgrad11_+"_vol", STATE_NONE, parDir_ ),
                                                             create_tag( velgrad11_,        STATE_NONE, parDir_ ) ),
                                     false, id_ );
        if( do2_ ){
          typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp1FluxT>::Builder Perp1FluxToVolInterp;
          factory.register_expression( new Perp1FluxToVolInterp( create_tag( velgrad22_+"_vol", STATE_NONE, parDir_ ),
                                                                 create_tag( velgrad22_,        STATE_NONE, parDir_ ) ),
                                           false, id_ );
        }
        if( do3_ ){
          typedef typename FluxInterpolation<ScalarT,typename BaseT::Perp2FluxT>::Builder Perp2FluxToVolInterp;
          factory.register_expression( new Perp2FluxToVolInterp( create_tag( velgrad33_+"_vol", STATE_NONE, parDir_ ),
                                                                 create_tag( velgrad33_,        STATE_NONE, parDir_ ) ),
                                           false, id_ );
        }
      }//End component_ == parDir_

      //Stress Tags
      const std::string tauString = get_tau_name( LBMS::direction(bc.first->get_boundary_direction()), component_ );
      const Expr::Tag tauTag = ( LBMS::direction(bc.first->get_boundary_direction()) == parDir_ ) ?
                               create_tag( tauString, STATE_NONE, parDir_ ) :
                               create_tag( tauString + sName_.fine, STATE_NONE, parDir_ );
      const Expr::Tag tauModTag1 = create_tag( tauString + "_mod1" + typeString + faceString + sideString + jobString, STATE_NONE, parDir_ );

      //Applying conditions to velocity derivatives and stress terms.
      if( bT == NSCBC::WALL ){
        switch( bc.first->get_boundary_direction() ){
          case SpatialOps::XDIR::value:
            this->template build_nscbc_neumann<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
            if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::XDIR>::type::Negate UnitTripletT;

              //Op Type for velocity extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::XDIR, bc.first->get_boundary_side() );
            }
            else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::XDIR>::type UnitTripletT;

              //Op Type for velocity extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::XDIR, bc.first->get_boundary_side() );
            }
            break;
          case SpatialOps::YDIR::value:
            this->template build_nscbc_neumann<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
            if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::YDIR>::type::Negate UnitTripletT;

              //Op Type for velocity extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::YDIR, bc.first->get_boundary_side() );
            }
            else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::YDIR>::type UnitTripletT;

              //Op Type for velocity extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::YDIR, bc.first->get_boundary_side() );
            }
            break;
          case SpatialOps::ZDIR::value:
            this->template build_nscbc_neumann<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, tauModTag1, tauTag );
            if( bc.first->get_boundary_side() == SpatialOps::PLUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type::Negate UnitTripletT;

              //Op Type for velocity extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::ZDIR, bc.first->get_boundary_side() );
            }
            else if( bc.first->get_boundary_side() == SpatialOps::MINUS_SIDE ){
              //Stencil direction
              typedef typename SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type UnitTripletT;

              //Op Type for pressure extrapolation
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Extrapolant,SpatialOps::OneSidedStencil3<UnitTripletT>,ScalarT>::type OpT;
              typedef typename LBMS::Extrapolation<ScalarT,OpT>::Builder ExtrapolationBuilder;
              pBuilder = new ExtrapolationBuilder( pModTag, bc.first->get_mask(), LBMS::ZDIR, bc.first->get_boundary_side() );
            }
            break;
          case SpatialOps::NODIR::value:
            std::ostringstream msg;
            msg << __FILE__ << " : " << __LINE__ << std::endl
                << "Invalid direction in MomentumEquation NSCBC" << std::endl;
            throw std::runtime_error( msg.str() );
            break;
        }//End Switch
        if( component_ == LBMS::direction(bc.first->get_boundary_direction()) ){
          factory.register_expression(pBuilder, false, id_);
          factory.attach_modifier_expression(pModTag, pressureTag, ALL_PATCHES, false);
        }//end component == boundary dir
      }//end wall check
    }//end else if hard inflow
  }//End BOOST_FOREACH

  this->build_nscbc_convflux( factory, nscbcVec );
}

//--------------------------------------------------------------------

template< typename ScalarT, typename CompDirT >
std::string
MomentumEquation<ScalarT,CompDirT>::
get_var_name( const LBMS::Direction dir )
{
  switch( dir ){
  case LBMS::XDIR: return StringNames::self().xmom;
  case LBMS::YDIR: return StringNames::self().ymom;
  case LBMS::ZDIR: return StringNames::self().zmom;
  case LBMS::NODIR: return "INVALID"; // to quiet compiler warnings.
  }
}

template< typename ScalarT, typename CompDirT >
Expr::Tag
MomentumEquation<ScalarT,CompDirT>::get_rhs_tag( const LBMS::Direction dir )
{
  const std::string name = get_var_name( LBMS::direction<CompDirT>() );
  return create_tag( name+"_RHS", Expr::STATE_NONE, dir );
}

//--------------------------------------------------------------------

// Explicit template instantiation
template class MomentumEquation<LBMS::XVolField,SpatialOps::XDIR>;
template class MomentumEquation<LBMS::XVolField,SpatialOps::YDIR>;
template class MomentumEquation<LBMS::XVolField,SpatialOps::ZDIR>;

template class MomentumEquation<LBMS::YVolField,SpatialOps::XDIR>;
template class MomentumEquation<LBMS::YVolField,SpatialOps::YDIR>;
template class MomentumEquation<LBMS::YVolField,SpatialOps::ZDIR>;

template class MomentumEquation<LBMS::ZVolField,SpatialOps::XDIR>;
template class MomentumEquation<LBMS::ZVolField,SpatialOps::YDIR>;
template class MomentumEquation<LBMS::ZVolField,SpatialOps::ZDIR>;
