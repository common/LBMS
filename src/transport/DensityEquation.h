/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef DensityEquation_h
#define DensityEquation_h

//--- LBMS Headers ---//
#include <transport/TransportEquationBase.h>
#include <transport/EquationBase.h>
#include <fields/StringNames.h>
#include <lbms/GraphHelperTools.h>
#include <lbms/Options.h>

//--- NSCBC Headers ---//
#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>

/**
 *  @class DensityEquation
 *
 *  @author James C. Sutherland
 *
 *  @date April, 2009
 *
 *  @brief The continuity transport equation, \f$\frac{\partial \rho}{\partial t}=-\nabla\cdot\mathbf{v}\f$.
 */

//template<typename ScalarT> class NSCBC::BCBuilder; // forward declaration

template< typename ScalarT >
class DensityEquation
  : public LBMS::TransportEquationBase< ScalarT >
{
  typedef LBMS::TransportEquationBase<ScalarT> BaseT;

public:

  DensityEquation( Expr::ExpressionFactory& exprFactory,
                   const LBMS::BundlePtr bundle,
                   const LBMS::Options& options,
                   const bool doAdvection,
                   GraphCategories& gc,
                   const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

  ~DensityEquation();

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

  void setup_boundary_conditions( GraphCategories& gc,
                                  const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

private:


  /**
   *  @class DensityICExpr
   *  @brief An expression to compute the initial condition for the
   *         density transport equation from the initial conditions on
   *         temperature, pressure, and composition.
   */
  class DensityICExpr : public Expr::Expression<ScalarT>
  {
  public:
    struct Builder : public Expr::ExpressionBuilder
    {
      Builder( const Expr::Tag resultTag,
               const Expr::Tag tempTag,
               const Expr::Tag pressTag,
               const Expr::TagList& yiTags )
        : ExpressionBuilder( resultTag ), tT_( tempTag ), pT_( pressTag ), yiT_(yiTags)
      {}

      Expr::ExpressionBase* build() const
      {
        return new DensityICExpr(tT_,pT_,yiT_);
      }
    private:
      const Expr::Tag tT_, pT_;
      const Expr::TagList yiT_;
    };

    void evaluate();

  private:
    DensityICExpr( const Expr::Tag tempTag,
                   const Expr::Tag pressTag,
                   const Expr::TagList& yiTags );

    DECLARE_FIELDS( ScalarT, temp_, press_ )
    DECLARE_VECTOR_OF_FIELDS( ScalarT, species_ )
  }; // class DensityICExpr

  const LBMS::Options& options_;
  const int id_;

};  // class DensityEquation

//====================================================================



#endif // DensityEquation_h
