/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TotalInternalEnergy_h
#define TotalInternalEnergy_h

//--- LBMS Headers ---//
#include "TransportEquationBase.h"
#include <transport/EquationBase.h>
#include <lbms/Options.h>
#include <lbms/GraphHelperTools.h>
#include <fields/StringNames.h>

//--- NSCBC Headers ---//
#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>

/**
 *  @class  TotalInternalEnergyEquation
 *  @author James C. Sutherland
 *
 *  @brief  Implements the total internal energy transport equation
 */
template< typename ScalarT >
class TotalInternalEnergyEquation
  : public LBMS::TransportEquationBase< ScalarT >
{
  typedef LBMS::TransportEquationBase<ScalarT> BaseT;
public:

  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::ParFluxT   >::type ParInterpT;
  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::Perp1FluxT >::type Perp1InterpT;
  typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, ScalarT, typename BaseT::Perp2FluxT >::type Perp2InterpT;

  TotalInternalEnergyEquation( Expr::ExpressionFactory& exprFactory,
                               const LBMS::BundlePtr bundle,
                               const LBMS::Options& options,
                               const bool doAdvection,
                               GraphCategories& gc,
                               const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

  ~TotalInternalEnergyEquation();

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

  void setup_boundary_conditions( GraphCategories& gc,
                                  const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

private:


  const LBMS::Options& options_;
  const StringNames& sName_;
  const int id_;
  const std::string parBundleName_, perpBundleName1_, perpBundleName2_, coarseName_;
  const LBMS::Direction parDir_, perp1Dir_, perp2Dir_;
  const bool do1_, do2_, do3_;
};

//====================================================================



#endif
