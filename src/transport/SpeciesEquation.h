#ifndef SpeciesEquation_h
#define SpeciesEquation_h

#include "TransportEquationBase.h"
#include <transport/EquationBase.h>
#include <lbms/Options.h>
#include <lbms/GraphHelperTools.h>

#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>
#include <vector>

/**
 *  @class SpeciesEquation
 *  @author James C. Sutherland
 *  @date May, 2009
 *
 *  @brief Defines a species transport equation, along with the
 *         expressions required by it.
 */
template< typename ScalarT >
class SpeciesEquation
  : public LBMS::TransportEquationBase< ScalarT >
{
  static std::string get_var_name( const int specNum );

  // disallow copy and assignment.
  SpeciesEquation( const SpeciesEquation& );
  SpeciesEquation operator=( const SpeciesEquation& );

  void register_one_time_expressions( Expr::ExpressionFactory& exprFactory,
                                      GraphCategories& gc,
                                      const Expr::TagList& diffCoefTags,
                                      const Expr::TagList& yiTags,
                                      const Expr::TagList& rrTags,
                                      const Expr::Tag& mixmwTag,
                                      const Expr::Tag& pressTag,
                                      const Expr::Tag& tempTag,
                                      const Expr::Tag& densTag,
                                      const int nghost );

  const int specNum_;
  const LBMS::PoKiTTOptions& pokittOpts_;
  const LBMS::Direction dir_, perp1Dir_, perp2Dir_;
  const bool do1_, do2_, do3_;

public:

  SpeciesEquation( Expr::ExpressionFactory& exprFactory,
                   const LBMS::BundlePtr bundle,
                   const int specNum,
                   const LBMS::Options& options,
                   const bool doAdvection,
                   GraphCategories& gc,
                   const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );

  Expr::ExpressionID initial_condition( Expr::ExpressionFactory& exprFactory );

  void setup_boundary_conditions( GraphCategories& gc,
                                  const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec );
};

template<typename ScalarT>
void
set_default_species_compositions( GraphCategories& gc,
                                  const LBMS::BundlePtr bundle,
                                  const LBMS::PoKiTTOptions& opts );

#endif // SpeciesEquation_h
