/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <transport/SpeciesEquation.h>
#include <transport/ParseEquation.h>

#include <mpi/Environment.h>
#include <lbms/Options.h>
#include <fields/StringNames.h>

// Expressions that we build here:
#include <exprs/ConvectiveFlux.h>
#include <exprs/DensityWeight.h>
#include <exprs/DiffusiveFlux.h>
#include <exprs/PrimVar.h>
#include <exprs/ScalarRHS.h>

// PoKiTT includes
#include <pokitt/transport/DiffusionCoeffMix.h>
#include <pokitt/kinetics/ReactionRates.h>
#include <pokitt/SpeciesN.h>

#include <boost/foreach.hpp>

using Expr::STATE_NONE;
using Expr::STATE_N;
using Expr::STATE_NP1;
using Expr::Tag;
using Expr::TagList;
using namespace LBMS;

//==============================================================================

template< typename ScalarT >
SpeciesEquation<ScalarT>::
SpeciesEquation( Expr::ExpressionFactory& exprFactory,
                 const LBMS::BundlePtr bundle,
                 const int specNum,
                 const LBMS::Options& options,
                 const bool doAdvection,
                 GraphCategories& gc,
                 const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
  : LBMS::TransportEquationBase<ScalarT>( bundle,
                                          get_var_name(specNum),
                                          create_tag( get_var_name(specNum)+"_RHS", STATE_NONE, bundle->dir() ) ),
    specNum_( specNum ),
    pokittOpts_( options.pokittOptions ),

    dir_     ( bundle->dir()     ),
    perp1Dir_( get_perp1( dir_ ) ),
    perp2Dir_( get_perp2( dir_ ) ),

    do1_( bundle->npts(dir_     ) > 1 ),
    do2_( bundle->npts(perp1Dir_) > 1 ),
    do3_( bundle->npts(perp2Dir_) > 1 )
{
  typedef TransportEquationBase<ScalarT>  BaseT;

  const int nghost = 1;

  const StringNames& sName = StringNames::self();

  const int id = bundle->id();

  const PoKiTTOptions& pokittOpts = options.pokittOptions;

  const TagList yiTags    = pokittOpts.species_tags( dir_, STATE_NONE               );
  const TagList rrTags    = pokittOpts.species_tags( dir_, STATE_NONE, "_rr"        );
  const TagList diffCoefs = pokittOpts.species_tags( dir_, STATE_NONE, "_diffCoeff" );

  const Tag rhoYiTag( this->solution_variable_name(), STATE_N );

  const Tag densTag  = create_tag( sName.density,                  STATE_N,    dir_ );
  const Tag mwTag    = create_tag( sName.mixmw,                    STATE_NONE, dir_ );
  const Tag tempTag  = create_tag( pokittOpts.temperature,         STATE_NONE, dir_ );
  const Tag pressTag = create_tag( pokittOpts.pressure,            STATE_NONE, dir_ );

  register_one_time_expressions( exprFactory, gc,
                                 diffCoefs, yiTags, rrTags, mwTag,
                                 pressTag, tempTag, densTag, nghost );

  const std::string parFace   = select_from_dir( dir_,      sName.xface, sName.yface, sName.zface );
  const std::string perp1Face = select_from_dir( perp1Dir_, sName.xface, sName.yface, sName.zface );
  const std::string perp2Face = select_from_dir( perp2Dir_, sName.xface, sName.yface, sName.zface );

  exprFactory.register_expression( new typename PrimVar<ScalarT>::Builder( yiTags[specNum], rhoYiTag, densTag ), false, id );

  FieldTagInfo rhsInfo;

  // Diffusive Flux
  {
    // bundle-direction flux:
    {
      typedef typename DiffusiveFlux2<typename BaseT::ParFluxT>::Builder DiffFlux;
      const TagList diffFluxTags = pokittOpts.species_flux_tags( dir_, dir_, STATE_NONE );
      exprFactory.register_expression( new DiffFlux( diffFluxTags[specNum], yiTags[specNum], diffCoefs[specNum], densTag ),
                                       false, id );
      LBMS::setup_field_exchange<typename BaseT::ParFluxT>( bundle, diffFluxTags[specNum], exprFactory, gc, dir_, dir_ );
      rhsInfo[ select_from_dir( dir_, DIFFUSIVE_FLUX_X, DIFFUSIVE_FLUX_Y, DIFFUSIVE_FLUX_Z ) ] = diffFluxTags[specNum];
    }
    if( do2_ ){
      typedef typename DiffusiveFlux2<typename BaseT::Perp1FluxT>::Builder DiffFlux;
      const TagList diffFineFluxTags = pokittOpts.species_flux_tags_reconstructed( perp1Dir_, dir_, STATE_NONE, sName.fine );
      exprFactory.register_expression( new DiffFlux( diffFineFluxTags[specNum], yiTags[specNum], diffCoefs[specNum], densTag ),
                                       false, id );

      const TagList diffFluxTags = pokittOpts.species_flux_tags( perp1Dir_, dir_, STATE_NONE );
      LBMS::setup_field_exchange<typename BaseT::Perp1FluxT>( bundle, diffFluxTags[specNum], exprFactory, gc, perp1Dir_, dir_ );

      const TagList reconsTags = pokittOpts.species_flux_tags_reconstructed( perp1Dir_, dir_, STATE_NONE, sName.reconstructed );
      const TagList coarseTags = pokittOpts.species_flux_tags_reconstructed( perp1Dir_, dir_, STATE_NONE, sName.coarse        );

      typedef typename OffLineFlux< typename BaseT::Perp1FluxT, typename BaseT::Perp1CoarseT >::Builder OffLineFluxBuilder;
      exprFactory.register_expression( new OffLineFluxBuilder ( reconsTags[specNum],
                                                                coarseTags[specNum],
                                                                diffFineFluxTags[specNum],
                                                                bundle ),
                                       false, id );

      rhsInfo[ select_from_dir( perp1Dir_, DIFFUSIVE_FLUX_X, DIFFUSIVE_FLUX_Y, DIFFUSIVE_FLUX_Z ) ] = reconsTags[specNum];
    }
    if( do3_ ){
      typedef typename DiffusiveFlux2<typename BaseT::Perp2FluxT>::Builder DiffFlux;
      const TagList diffFineFluxTags = pokittOpts.species_flux_tags_reconstructed( perp2Dir_, dir_, STATE_NONE, sName.fine );
      exprFactory.register_expression( new DiffFlux( diffFineFluxTags[specNum], yiTags[specNum], diffCoefs[specNum], densTag ),
                                       false, id );

      const TagList diffFluxTags = pokittOpts.species_flux_tags( perp2Dir_, dir_, STATE_NONE );
      LBMS::setup_field_exchange<typename BaseT::Perp2FluxT>( bundle, diffFluxTags[specNum], exprFactory, gc, perp2Dir_, dir_ );

      const TagList reconsTags = pokittOpts.species_flux_tags_reconstructed( perp2Dir_, dir_, STATE_NONE, sName.reconstructed );
      const TagList coarseTags = pokittOpts.species_flux_tags_reconstructed( perp2Dir_, dir_, STATE_NONE, sName.coarse        );

      typedef typename OffLineFlux< typename BaseT::Perp2FluxT, typename BaseT::Perp2CoarseT >::Builder OffLineFluxBuilder;
      exprFactory.register_expression( new OffLineFluxBuilder ( reconsTags[specNum],
                                                                coarseTags[specNum],
                                                                diffFineFluxTags[specNum],
                                                                bundle ),
                                       false, id );

      rhsInfo[ select_from_dir( perp2Dir_, DIFFUSIVE_FLUX_X, DIFFUSIVE_FLUX_Y, DIFFUSIVE_FLUX_Z ) ] = reconsTags[specNum];
    }
  }

  // Convective Flux
  if( doAdvection ) this->build_convective_fluxes( exprFactory, gc, rhsInfo );

  // RHS expression
  {
    typedef typename ScalarRHS<ScalarT>::Builder RHS;
    if( pokittOpts.doReactions ) rhsInfo[ SOURCE_TERM ] = rrTags[specNum];
    exprFactory.register_expression( new RHS( this->get_rhs_tag(), rhsInfo ), false, id );
  }

  //Boundary condition setup
  this->setup_boundary_conditions( gc, nscbcVec );
}

//--------------------------------------------------------------------

template< typename ScalarT >
std::string
SpeciesEquation<ScalarT>::
get_var_name( const int specnum )
{
  std::string varnam = StringNames::self().density + "_" + CanteraObjects::species_name(specnum);
  return varnam;
}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionID
SpeciesEquation<ScalarT>::initial_condition( Expr::ExpressionFactory& exprFactory )
{
  const TagList yiTags = pokittOpts_.species_tags( dir_, STATE_NONE );
  const Tag densTag  = create_tag( StringNames::self().density, STATE_N, dir_ );
  const Tag rhoYiTag = create_tag( get_var_name(specNum_),      STATE_N, dir_ );

  // if we don't have an expression set for the initial mass fraction, we should zero it out I suppose.
  typedef typename DensityWeight<ScalarT>::Builder IC;
  return exprFactory.register_expression( new IC( rhoYiTag, yiTags[specNum_], densTag, 1 ), false, this->bundle_->id() );
}

//-----------------------------------------------------------------

template<typename ScalarT>
void
set_default_species_compositions( GraphCategories& gc,
                                  const BundlePtr bundle,
                                  const PoKiTTOptions& opts )
{
  const Expr::TagList specTags = opts.species_tags( bundle->dir(), Expr::STATE_NONE );
  BOOST_FOREACH( const Expr::Tag& spTag, specTags ){
    Expr::ExpressionFactory& factory = *gc[INITIALIZATION]->exprFactory;
    if( !factory.have_entry(spTag) ){
      proc0cout << "initialization entry does not exist for " << spTag << " adding a zero constant value expression.\n";
      factory.register_expression( new typename Expr::ConstantExpr<ScalarT>::Builder( spTag, 0.0 ), false, bundle->id() );
    }
  }
}

//-----------------------------------------------------------------

template<typename ScalarT>
void
SpeciesEquation<ScalarT>::
register_one_time_expressions( Expr::ExpressionFactory& exprFactory,
                               GraphCategories& gc,
                               const TagList& diffCoefTags,
                               const TagList& yiTags,
                               const TagList& rrTags,
                               const Tag& mixmwTag,
                               const Tag& pressTag,
                               const Tag& tempTag,
                               const Tag& densTag,
                               const int nghost )
{
  static bool isInitializedX = false;
  static bool isInitializedY = false;
  static bool isInitializedZ = false;

  if( dir_ == XDIR ){
    if( isInitializedX ) return;
    isInitializedX = true;
  }
  if( dir_ == YDIR ){
    if( isInitializedY ) return;
    isInitializedY = true;
  }
  if( dir_ == ZDIR ){
    if( isInitializedZ ) return;
    isInitializedZ = true;
  }

  const int id = this->bundle_->id();

  // Reaction rates
  if( pokittOpts_.doReactions ){
    typedef typename pokitt::ReactionRates<ScalarT>::Builder RxnRates;
    exprFactory.register_expression( new RxnRates( rrTags, tempTag, densTag, yiTags, mixmwTag,
                                                   boost::shared_ptr<pokitt::ChemicalSourceJacobian>(),
                                                   nghost ), false, id );
  }

  // Diffusion coefficients
  typedef typename pokitt::DiffusionCoeff<ScalarT>::Builder DiffCoeff;
  exprFactory.register_expression( new DiffCoeff( diffCoefTags, tempTag, pressTag, yiTags, mixmwTag, nghost ), false, id );

  // species N mass fraction (we only solve n-1 species equations).
  typedef typename pokitt::SpeciesN<ScalarT>::Builder SpecN;
  const size_t specN = pokittOpts_.nspecies-1;
  const Tag yNTag = yiTags[specN];
  exprFactory.register_expression( new SpecN( yNTag, yiTags, pokitt::ERRORSPECN, nghost ), false, id );

  // ensure that the nth species is set during initialization.
  gc[INITIALIZATION]->exprFactory->register_expression( new SpecN( yNTag, yiTags, pokitt::ERRORSPECN, nghost ), false, id );

  // Bundle Direction Diffusive Flux for nth species
  typedef typename DiffusiveFlux2<typename TransportEquationBase<ScalarT>::ParFluxT>::Builder DiffFlux;
  const TagList diffFluxTags = pokittOpts_.species_flux_tags( dir_, dir_, STATE_NONE );
  exprFactory.register_expression( new DiffFlux( diffFluxTags[specN], yiTags[specN], diffCoefTags[specN], densTag ), false, id );
  std::cout << "Set up nth species flux on " << yiTags[specN] << std::endl;

  // dac Note that we now need the offline fluxes.  Heat flux requires the Nth species
  if( do2_ ){
    typedef typename DiffusiveFlux2<typename TransportEquationBase<ScalarT>::Perp1FluxT>::Builder DiffFlux;
    const TagList diffFluxTags = pokittOpts_.species_flux_tags( perp1Dir_, dir_, STATE_NONE );
    exprFactory.register_expression( new DiffFlux( diffFluxTags[specN], yiTags[specN], diffCoefTags[specN], densTag ), false, id );
  }
  if( do3_ ){
    typedef typename DiffusiveFlux2<typename TransportEquationBase<ScalarT>::Perp2FluxT>::Builder DiffFlux;
    const TagList diffFluxTags = pokittOpts_.species_flux_tags( perp2Dir_, dir_, STATE_NONE );
    exprFactory.register_expression( new DiffFlux( diffFluxTags[specN], yiTags[specN], diffCoefTags[specN], densTag ), false, id );
  }

  // set compositions to zero for species that have not been otherwise set from the input file.
  // Note that we parse basic expressions before transport equations.  This is important.
  set_default_species_compositions<ScalarT>( gc, this->bundle_, pokittOpts_ );
}

//-----------------------------------------------------------------


template<typename ScalarT>
void
SpeciesEquation<ScalarT>::
setup_boundary_conditions( GraphCategories& gc, const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
{
  //This avoids an issue with BOOST_FOREACH's macro, which will parse an std::pair template argument
  //as two arguments due to the comma in the template arguments.
  typedef const std::pair< boost::shared_ptr< NSCBC::BCBuilder<ScalarT> >, LBMS::PointCollection2D > NSCBCPairT;

  const StringNames& sName = StringNames::self();
  const std::string numString = boost::lexical_cast<std::string>(specNum_);

  BOOST_FOREACH( const NSCBCPairT& bc, nscbcVec ){
    (bc.first)->attach_rhs_modifier( *gc[ADVANCE_SOLUTION]->exprFactory, this->get_rhs_tag(), NSCBC::SPECIES, specNum_ );

    const NSCBC::BCType bT = bc.first->get_boundary_type();
    if( bT == NSCBC::NONREFLECTING || bT == NSCBC::WALL ){
      //Setting up diffusive boundary conditions
      //Note that 'direction((bc.first)->dir_)' converts from NSCBC::XDIR to LBMS::XDIR by
      //going through the integer values associated with the direction (i.e. SpatialOps::XDIR::value)
      //This should be safe as long as the NSCBC and LBMS direction types are both built upon SpatialOps
      const std::string faceString = select_from_dir( direction(bc.first->get_boundary_direction()), sName.xface, sName.yface, sName.zface );
      const Expr::Tag modTag1 = create_tag( "species_" + numString + "_diffusiveflux_mod1" + faceString
                                            + bc.first->type()
                                            + bc.first->side()
                                            + bc.first->get_job_name(),
                                            Expr::STATE_NONE, dir_ );
      const Expr::Tag modTag2 = create_tag( "species_" + numString + "_diffusiveflux_mod2" + faceString
                                            + bc.first->type()
                                            + bc.first->side()
                                            + bc.first->get_job_name(),
                                            Expr::STATE_NONE, dir_ );

      const TagList diffFluxTags = pokittOpts_.species_flux_tags              ( direction(bc.first->get_boundary_direction()), dir_, STATE_NONE                      );
      const TagList reconsTags   = pokittOpts_.species_flux_tags_reconstructed( direction(bc.first->get_boundary_direction()), dir_, STATE_NONE, sName.reconstructed );

      const Expr::Tag diffTag = LBMS::direction(bc.first->get_boundary_direction()) == dir_ ? diffFluxTags[specNum_] : reconsTags[specNum_];

      switch( bc.first->get_boundary_direction() ){
        case SpatialOps::XDIR::value:
          this->template build_nscbc_neumann<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffTag );
  //        this->template set_diffusive_flux_boundary<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffTag, -1 );
          break;
        case SpatialOps::YDIR::value:
          this->template build_nscbc_neumann<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffTag );
  //        this->template set_diffusive_flux_boundary<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffTag, -1 );
          break;
        case SpatialOps::ZDIR::value:
          this->template build_nscbc_neumann<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffTag );
  //        this->template set_diffusive_flux_boundary<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffTag, -1 );
          break;
        case SpatialOps::NODIR::value:
        {
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << std::endl
              << "No direction was given" << std::endl;
          throw std::runtime_error( msg.str() );
          break;
        }
      }
    }//End Switch
  }
  //Sets up convective fluxes
  this->build_nscbc_convflux( *gc[ADVANCE_SOLUTION]->exprFactory, nscbcVec );
}

//-----------------------------------------------------------------


#define INSTANTIATE( T )                \
template class SpeciesEquation<T>;      \
template void set_default_species_compositions<T>( GraphCategories&, const BundlePtr, const PoKiTTOptions& );


// Explicit template instantiation
INSTANTIATE( LBMS::XVolField )
INSTANTIATE( LBMS::YVolField )
INSTANTIATE( LBMS::ZVolField )
