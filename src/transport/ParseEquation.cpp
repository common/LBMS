/*
 * Copyright (c) 2014 The University of UYtah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ParseEquation.h"

#include <mpi/Environment.h>
#include <mpi/FieldBundleExchange.h>
#include <mpi/CoarseFieldPollWorker.h>
#include <fields/StringNames.h>
#include <fields/Fields.h>
#include <lbms/Bundle.h>
#include <parser/ParseTools.h>
#include <operators/Operators.h>

#include <yaml-cpp/yaml.h>

//-- Add headers for individual transport equations here --//
#include "MomentumEquation.h"
#include "TotalInternalEnergyEquation.h"

//-- includes for the expressions built here --//
#include <exprs/ConvectiveFlux.h>
#include <exprs/DiffusiveFlux.h>
#include <exprs/PrimVar.h>
#include <exprs/ScalarRHS.h>
#include <exprs/FieldInterp.h>

//-- Expression Library includes --//
#include <expression/ExprLib.h>
#include "TransportEquationBase.h"


#include <iostream>

using std::string;
using std::endl;

namespace LBMS{


  //==================================================================

  template<typename FieldT>
  string get_solnvar_name( const YAML::Node& parser )
  {
    return parser["SolutionVariable"].as<string>();
  }

  template<typename FieldT>
  string strip_field_suffix( std::string name )
  {
    std::string suff = suffix<FieldT>();
    const size_t n = name.find( suff );
    return name.substr( 0, n );
  }

  template<typename FieldT>
  string get_primvar_name( const YAML::Node& params )
  {
    return params["PrimitiveVariable"].as<string>() + suffix<FieldT>();
  }

  //==================================================================

  template< typename FieldT >
  void setup_field_exchange( const BundlePtr bundle,
                             const Expr::Tag fieldTag,
                             Expr::ExpressionFactory& factory,
                             GraphCategories& gc,
                             const LBMS::Direction srcBundleDir,
                             const LBMS::Direction targetBundleDir )
  {
    GraphHelper* const solnGraphHelper = gc[ADVANCE_SOLUTION];
    typedef typename FieldT::Location::FaceDir         DirT;
    typedef typename CoarseFieldSelector<FieldT>::type CoarseFieldT;
    const int fmlID = bundle->id();
    const Direction fieldDir = direction<DirT>();
    const bool isOnlinefield = fieldDir == LBMS::NODIR ? ( bundle->dir() == srcBundleDir ) : ( bundle->dir() == fieldDir );
    const string basefieldName = strip_field_suffix<FieldT>( fieldTag.name() );
    const Expr::Tag coarsefieldTag ( basefieldName + "_coarse" + get_bundle_name_suffix(bundle->dir()),
                                     fieldTag.context() );
    /*
     * If this processor owns the fine field field, then create the coarse field
     * expression. Otherwise, create a PlaceHolder with a Poller that will not
     * allow the "downstream" dependents of the PlaceHolder to execute until the
     * communication completes.
     */
    if( isOnlinefield ){
      // register the expression to compute the coarse field from the fine field on this bundle
      typedef typename FieldInterp< InterpFineToCoarse<FieldT> >::Builder Coarsefield;
      const Expr::ExpressionID id = factory.register_expression( new Coarsefield(coarsefieldTag,fieldTag), false, fmlID );
      solnGraphHelper->rootIDs.insert(id);
      communicate_expression_result<FieldT>(fieldTag, bundle, factory);
    }
    else{
      typedef typename Expr::PlaceHolder<CoarseFieldT>::Builder Coarsefield;
      factory.register_expression( new Coarsefield(coarsefieldTag), false, fmlID );
      /*
       * Register the expressions to inject back to the fine field for both
       * perpendicular directions. Note that if this processor does not own a
       * bundle in the given direction, the expression will not be instantiated
       * or loaded into the graph.
       */
      typedef typename FieldInterp< InterpCoarseToFine<FieldT> >::Builder C2F;
      factory.register_expression( new C2F(fieldTag,coarsefieldTag), false, fmlID );
    }

    // set up communication as needed to transfer the coarse field between bundles
    communicate_coarse_expression_result<CoarseFieldT>( factory, bundle, coarsefieldTag, srcBundleDir, targetBundleDir );

  } // setup_field_exchange

  //==================================================================

  template<typename FieldT>
  Expr::Tag
  get_rhs_expr_tag( const BundlePtr bundle,
                    const Expr::Tag densityTag,
                    Expr::ExpressionFactory& factory,
                    const YAML::Node& params,
                    GraphCategories& gc)
  {
    FieldTagInfo info;
    const int fmlID = bundle->id();
    const Direction dir = direction<typename FieldT::Location::Bundle>();

    //______________________________________________________________________
    // Set up the tags for solution variable and primitive variable. Also,
    // getting information about the equation format that we are solving (
    // strong form, weak form, Constant density or variable density) and
    // throwing errors with respect to input file definition.
    const std::string solnVarName = get_solnvar_name<FieldT>( params );

    const Expr::Tag solnVarTag = create_tag( solnVarName, Expr::STATE_N, dir );
    Expr::Tag primVarTag( solnVarTag );

    const bool densityWeighted = params["DensityWeighted"];
    if( densityWeighted ){
      primVarTag = Expr::Tag( get_primvar_name<FieldT>( params["DensityWeighted"] ),
                              Expr::STATE_NONE );
      factory.register_expression( new typename PrimVar<FieldT>::Builder( primVarTag, solnVarTag, densityTag ), false, fmlID );
    }

    //_________________
    // Diffusive Fluxes
    const YAML::Node dfExpr = params["DiffusiveFluxExpression"];
    for( YAML::Node::const_iterator diffFluxParams=dfExpr.begin(); diffFluxParams != dfExpr.end(); ++diffFluxParams ){
      setup_diffusive_flux_expression<FieldT>( bundle, *diffFluxParams, densityTag, primVarTag, factory, info, gc );
    }

    //__________________
    // Convective Fluxes
    const YAML::Node cfExpr = params["ConvectiveFluxExpression"];
    for( YAML::Node::const_iterator convFluxParams = cfExpr.begin(); convFluxParams != cfExpr.end(); ++convFluxParams ){
      setup_convective_flux_expression<FieldT>( bundle, *convFluxParams, solnVarTag, factory, info, gc );
    }

    //_____________
    // Source Terms
//    Expr::TagList srcTags;
//    for( ParseGroup::const_iterator sourceTermParams = params.begin("SourceTermExpression");
//         sourceTermParams != params.end("SourceTermExpression");
//         ++sourceTermParams )
//    {
//      srcTags.push_back( parse_nametag( sourceTermParams->get_child("NameTag") ) );
//    }
      if( params["SourceTermExpression"] )
        info[ SOURCE_TERM ] = parse_nametag( params["SourceTermExpression"]["NameTag"], dir );

    //_____________
    // Right Hand Side
    const Expr::Tag rhsTag( solnVarName+bundle->name()+"_rhs", Expr::STATE_NONE );
    factory.register_expression( new typename ScalarRHS<FieldT>::Builder(rhsTag, info), false, fmlID );
    return rhsTag;
  }

  //==================================================================

  template<typename T>
  TransportEquationBase<T>*
  parse_equation( const BundlePtr bundle,
                  const YAML::Node& params,
                  const Options& options,
                  const Expr::Tag& densityTag,
                  GraphCategories& gc,
                  const typename LBMS::NSCBCVecType<T>::NSCBCVec& nscbcVec )
  {
    GraphHelper* const solnGraphHelper = gc[ADVANCE_SOLUTION];

    if( params["EnergyEquation"] ){

      const YAML::Node paramSubset = params["EnergyEquation"];
      const bool doAdvection = paramSubset["Advection"];

      proc0cout << "Parsing energy equation ";
      if( doAdvection ){ proc0cout << "with Advection"    << endl; }
      else             { proc0cout << "without Advection" << endl; }

      typedef TotalInternalEnergyEquation<T> TotIntEngyEqn;
      TotIntEngyEqn* equation = new TotIntEngyEqn( *solnGraphHelper->exprFactory, bundle, options, doAdvection, gc, nscbcVec );
      return equation;
    }
    else if( params["SolutionVariable"] ){
      const string solnVariable = params["SolutionVariable"].as<string>();
      proc0cout << "Parsing " << solnVariable << " equation" << endl;
      //___________________________________________________________________________
      // resolve the transport equation to be solved and create the adaptor for it.
      //
      typedef TransportEquationBase<T> TransEqn;
      const Expr::Tag rhsTag = get_rhs_expr_tag<T>( bundle, densityTag, *solnGraphHelper->exprFactory, params, gc );
      TransEqn* transeqn = new TransEqn( bundle, get_solnvar_name<T>( params ), rhsTag );
      return transeqn;
    }
    else assert(false);
    return NULL;
  }

  //==================================================================

  template<typename T>
  std::vector<TransportEquationBase<T>*>
  parse_momentum_equations( const BundlePtr bundle,
                            const YAML::Node& params,
                            const Options& options,
                            const Expr::Tag& densityTag,
                            GraphCategories& gc,
                            const typename LBMS::NSCBCVecType<T>::NSCBCVec& nscbcVec )
  {
    proc0cout << "Parsing momentum equations" << endl;
    GraphHelper* const solnGraphHelper = gc[ADVANCE_SOLUTION];

    typedef TransportEquationBase<T>   TransEqn;
    typedef std::vector<TransportEquationBase<T>*> TransEqns;

    const std::vector<string> opts = params.as<std::vector<string> >();

    const bool doXMom      = std::find( opts.begin(), opts.end(), "X-Momentum" ) != opts.end();
    const bool doYMom      = std::find( opts.begin(), opts.end(), "Y-Momentum" ) != opts.end();
    const bool doZMom      = std::find( opts.begin(), opts.end(), "Z-Momentum" ) != opts.end();
    const bool doAdvection = std::find( opts.begin(), opts.end(), "Advection"  ) != opts.end();

    if( doAdvection ){ proc0cout << "with Advection"    << endl; }
    else             { proc0cout << "without Advection" << endl; }

    if( !(doXMom || doYMom || doZMom) ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "No Direction specified in input file for momentum equations." << endl;
      throw std::invalid_argument( msg.str() );
    }

    TransEqns equations;
    if( doXMom ){
      typedef MomentumEquation< T, SpatialOps::XDIR > XMomEqn;
      XMomEqn* equation = new XMomEqn( *solnGraphHelper->exprFactory, bundle, options, doAdvection, gc, nscbcVec );
      equations.push_back(equation);
    }

    if( doYMom ){
      typedef MomentumEquation< T, SpatialOps::YDIR > YMomEqn;
      YMomEqn* equation = new YMomEqn( *solnGraphHelper->exprFactory, bundle, options, doAdvection, gc, nscbcVec );
      equations.push_back(equation);
    }

    if( doZMom ){
      typedef MomentumEquation< T, SpatialOps::ZDIR > ZMomEqn;
      ZMomEqn* equation = new ZMomEqn( *solnGraphHelper->exprFactory, bundle, options, doAdvection, gc, nscbcVec );
      equations.push_back(equation);
    }
    return equations;
  }

  //==================================================================

  template< typename FieldT >
  void setup_convective_flux_expression( const BundlePtr bundle,
                                         const Direction fluxDir,
                                         const Expr::Tag& solnVarTag,
                                         Expr::Tag& convFluxTag,
                                         const Expr::Tag& advVelocityTag,
                                         Expr::ExpressionFactory& factory,
                                         FieldTagInfo& info,
                                         GraphCategories& gc )
  {
    typedef typename SpatialOps::FaceTypes<FieldT>::XFace XFaceT;
    typedef typename SpatialOps::FaceTypes<FieldT>::YFace YFaceT;
    typedef typename SpatialOps::FaceTypes<FieldT>::ZFace ZFaceT;

    if( advVelocityTag == Expr::Tag() ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "ERROR: no advective velocity set for transport equation '" << solnVarTag.name() << "'" << endl;
      throw std::invalid_argument( msg.str() );
    }

    if( convFluxTag == Expr::Tag() ){
      convFluxTag = Expr::Tag( get_dir_name(fluxDir)+ "_convective_flux_" + solnVarTag.name() , Expr::STATE_NONE );

      Expr::ExpressionBuilder* builder = NULL;

      typedef typename ConvectiveFlux<XFaceT>::Builder XCFlux;
      typedef typename ConvectiveFlux<YFaceT>::Builder YCFlux;
      typedef typename ConvectiveFlux<ZFaceT>::Builder ZCFlux;

      const int patchID = bundle->id();
      const bool onlineFlux  = ( bundle->dir() == fluxDir );
      switch( fluxDir ){
        case XDIR:{
          builder=new XCFlux( convFluxTag, advVelocityTag, solnVarTag );
          if( onlineFlux ) factory.register_expression( builder, false, patchID );
          setup_field_exchange<XFaceT>( bundle, convFluxTag, factory, gc, fluxDir, bundle->dir() );
          break;
        }
        case YDIR:{
          builder=new YCFlux( convFluxTag, advVelocityTag, solnVarTag );
          if( onlineFlux ) factory.register_expression( builder, false, patchID );
          setup_field_exchange<YFaceT>( bundle, convFluxTag, factory, gc, fluxDir, bundle->dir() );
          break;
        }
        case ZDIR:{
          builder=new ZCFlux( convFluxTag, advVelocityTag, solnVarTag );
          if( onlineFlux ) factory.register_expression( builder, false, patchID );
          setup_field_exchange<ZFaceT>( bundle, convFluxTag, factory, gc, fluxDir, bundle->dir() );
          break;
        }
        default:{
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << endl
              << "Invalid direction selection for convective flux expression on " << solnVarTag.name() << endl;
          throw std::invalid_argument( msg.str() );
        }
      } // switch( dir )
    } // if

    FieldSelector fs;
    switch( fluxDir ){
    case XDIR: fs=CONVECTIVE_FLUX_X; break;
    case YDIR: fs=CONVECTIVE_FLUX_Y; break;
    case ZDIR: fs=CONVECTIVE_FLUX_Z; break;
    default:{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "Invalid direction selection for convective flux expression on " << solnVarTag.name() << endl;
      throw std::invalid_argument( msg.str() );
    }
    }

    info[ fs ] = convFluxTag;
  }

  template< typename FieldT >
  void setup_convective_flux_expression( const BundlePtr bundle,
                                         const YAML::Node& convFluxParams,
                                         const Expr::Tag& solnVarTag,
                                         Expr::ExpressionFactory& factory,
                                         FieldTagInfo& info,
                                         GraphCategories& gc)
  {

    Expr::Tag convFluxTag, advVelocityTag;
    const Direction dir = direction( convFluxParams["Direction"].as<string>() );

    // get the tag for the advective velocity
    if( convFluxParams["AdvectiveVelocity"] ){
      advVelocityTag = parse_nametag( convFluxParams["AdvectiveVelocity"]["NameTag"], dir );
    }

    // see if we have an expression set for the advective flux.
    if( convFluxParams["NameTag"] ){
      convFluxTag = parse_nametag( convFluxParams["NameTag"], dir );
    }

    setup_convective_flux_expression<FieldT>( bundle, dir, solnVarTag,
                                              convFluxTag, advVelocityTag,
                                              factory, info, gc );
  }

  //=================================================================

  template< typename FluxT >
  Expr::ExpressionBuilder*
  build_diff_flux_expr( const YAML::Node& diffFluxParams,
                        const Expr::Tag& diffFluxTag,
                        const Expr::Tag& primVarTag,
                        const Expr::Tag& densityTag )
  {
    Expr::ExpressionBuilder* builder = NULL;

    std::cout << "Building diffusive flux expression for " << diffFluxTag.name() << endl;
    const Direction dir = direction<typename FluxT::Location::Bundle>();
    const YAML::Node dcoef = diffFluxParams["DiffusionCoefficient"];
    if( dcoef["ConstantDiffusivity"] ){
      const double coef = dcoef["ConstantDiffusivity"].as<double>();
      typedef typename DiffusiveFlux<FluxT>::Builder Flux;
      builder = new Flux( diffFluxTag, primVarTag, coef, densityTag );
    }
    else{
      const Expr::Tag coef = parse_nametag( dcoef["NameTag"], dir );
      typedef typename DiffusiveFlux2<FluxT>::Builder Flux;
      builder = new Flux( diffFluxTag, primVarTag, coef, densityTag );
    }
    return builder;
  }

  template< typename FieldT>
  void setup_diffusive_flux_expression( const BundlePtr bundle,
                                        const YAML::Node& diffFluxParams,
                                        const Expr::Tag& densityTag,
                                        const Expr::Tag& primVarTag,
                                        Expr::ExpressionFactory& factory,
                                        FieldTagInfo& info,
                                        GraphCategories& gc )
  {
    typedef typename SpatialOps::FaceTypes<FieldT>::XFace XFluxT;
    typedef typename SpatialOps::FaceTypes<FieldT>::YFace YFluxT;
    typedef typename SpatialOps::FaceTypes<FieldT>::ZFace ZFluxT;

    typedef typename FieldT::Location::Bundle DirT;
    const string fluxDirName = diffFluxParams["Direction"].as<string>();
    const Direction fluxDir = direction(fluxDirName);
    
    const int fmlID = bundle->id();
    const bool isOnlineFlux = (fluxDir == bundle->dir());
    const string& primVarName = primVarTag.name();

    const Expr::Tag diffFluxTag( fluxDirName+"_diffFlux_"+primVarName, Expr::STATE_NONE );

    // register an expression for the diffusive flux.
    Expr::ExpressionBuilder* builder = NULL;
    FieldSelector fs;
    switch( fluxDir ){
    case XDIR:{
      if( isOnlineFlux ) builder = build_diff_flux_expr<XFluxT>( diffFluxParams, diffFluxTag, primVarTag, densityTag );
      if( isOnlineFlux ) factory.register_expression( builder, false, fmlID );
      setup_field_exchange<XFluxT>( bundle, diffFluxTag, factory, gc, fluxDir, bundle->dir() );
      fs = DIFFUSIVE_FLUX_X;
      break;
    }
    case YDIR:{
      if( isOnlineFlux ) builder = build_diff_flux_expr<YFluxT>( diffFluxParams, diffFluxTag, primVarTag, densityTag );
      if( isOnlineFlux ) factory.register_expression( builder, false, fmlID );
      setup_field_exchange<YFluxT>( bundle, diffFluxTag, factory, gc, fluxDir, bundle->dir() );
      fs = DIFFUSIVE_FLUX_Y;
      break;
    }
    case ZDIR:{
      if( isOnlineFlux ) builder = build_diff_flux_expr<ZFluxT>( diffFluxParams, diffFluxTag, primVarTag, densityTag );
      if( isOnlineFlux ) factory.register_expression( builder, false, fmlID );
      setup_field_exchange<ZFluxT>( bundle, diffFluxTag, factory, gc, fluxDir, bundle->dir() );
      fs = DIFFUSIVE_FLUX_Z;
      break;
    }
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl
          << "Invalid direction selection for diffusive flux expression for '" << primVarName << "'" << endl;
      throw std::runtime_error( msg.str() );
    }

    info[ fs ] = diffFluxTag;
  }

  //==================================================================
  // explicit template instantiation

#define INSTANTIATE_FLUXES( FLUXT )                                                  \
  template void setup_field_exchange<FLUXT>( const BundlePtr bundle,                 \
                                             const Expr::Tag fieldTag,               \
                                             Expr::ExpressionFactory& factory,       \
                                             GraphCategories& gc,                    \
                                             const LBMS::Direction srcBundleDir,     \
                                             const LBMS::Direction targetBundleDir );
#define INSTANTIATE_VOLUMES( VOLT )                                                  \
  template void setup_field_exchange<VOLT>( const BundlePtr bundle,                  \
                                            const Expr::Tag fieldTag,                \
                                            Expr::ExpressionFactory& factory,        \
                                            GraphCategories& gc,                     \
                                            const LBMS::Direction srcBundleDir,      \
                                            const LBMS::Direction targetBundleDir ); \
                                                                                     \
  template TransportEquationBase<VOLT>* parse_equation(                              \
    const BundlePtr bundle,                                                          \
    const YAML::Node& params,                                                        \
    const Options& options,                                                          \
    const Expr::Tag& densityTag,                                                     \
    GraphCategories& gc,                                                             \
    const LBMS::NSCBCVecType<VOLT>::NSCBCVec& nscbcVec);                             \
                                                                                     \
  template std::vector<TransportEquationBase<VOLT>*> parse_momentum_equations(       \
    const BundlePtr bundle,                                                          \
    const YAML::Node& params,                                                        \
    const Options& options,                                                          \
    const Expr::Tag& densityTag,                                                     \
    GraphCategories& gc,                                                             \
    const LBMS::NSCBCVecType<VOLT>::NSCBCVec& nscbcVec );                            \
                                                                                     \
  template void setup_diffusive_flux_expression<VOLT>(                               \
     const BundlePtr bundle,                                                         \
     const YAML::Node& diffFluxParams,                                               \
     const Expr::Tag& densityTag,                                                    \
     const Expr::Tag& primVarTag,                                                    \
     Expr::ExpressionFactory& factory,                                               \
     FieldTagInfo& info,                                                             \
     GraphCategories& gc );                                                          \
                                                                                     \
  template void setup_convective_flux_expression<VOLT>(                              \
     const BundlePtr bundle,                                                         \
     const YAML::Node& convFluxParams,                                               \
     const Expr::Tag& solnVarName,                                                   \
     Expr::ExpressionFactory& factory,                                               \
     FieldTagInfo& info,                                                             \
     GraphCategories& gc );

#define INSTANTIATE( FIELDT )                                       \
     INSTANTIATE_FLUXES( SpatialOps::FaceTypes<FIELDT>::XFace )     \
     INSTANTIATE_FLUXES( SpatialOps::FaceTypes<FIELDT>::YFace )     \
     INSTANTIATE_FLUXES( SpatialOps::FaceTypes<FIELDT>::ZFace )     \
     INSTANTIATE_VOLUMES( FIELDT )

  INSTANTIATE( XVolField )
  INSTANTIATE( YVolField )
  INSTANTIATE( ZVolField )
  //=================================================================


} // namespace LBMS
