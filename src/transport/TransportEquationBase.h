/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/**
 *  \file   TransportEquationBase.cpp
 *  \author Derek Cline
 *  \brief  Replaces TransportEquation.h, which is a wrapper for the base class TransportEquation
 *          in ExprLib.  The reason for this is that the NSCBC causes the boundary condition methods
 *          in ExprLib TransportEquation to become deprecated.
 *  \date April 2015
 */

#ifndef LBMSTransportEquation_h
#define LBMSTransportEquation_h

#include <transport/EquationBase.h>  // Base class definition

#include <nscbc/CharacteristicBCBuilder.h>

#include <operators/Operators.h>
#include <operators/Extrapolation.h>

#include <lbms/Bundle_fwd.h>
#include <lbms/Direction.h>
#include <lbms/GraphHelperTools.h>
#include <lbms/BCOptions.h>

#include <fields/Fields.h>
#include <fields/StringNames.h>

#include <exprs/RHSTerms.h>
#include <exprs/OffLineFlux.h>

#include <vector>

namespace LBMS{

  /**
   *  @class TransportEquationBase
   *  @author Derek
   *  @brief Base class for transport equations in LBMS - wraps the LBMS::EquationBase class.
   *  @date April, 2015
   */
  template< typename FieldT >
  class TransportEquationBase
    : public LBMS::EquationBase
  {
  public:
    typedef typename FieldT::Location::Bundle                       BundleDirT; ///< Orientation of this bundle.

    typedef typename LBMS::GetPerpDirType<BundleDirT>::Perp1DirT    Perp1DirT;
    typedef typename LBMS::GetPerpDirType<BundleDirT>::Perp2DirT    Perp2DirT;

    typedef typename LBMS::FaceFieldSelector<BundleDirT>::ParallelT ParFluxT;   ///< Flux in direction 1
    typedef typename LBMS::FaceFieldSelector<BundleDirT>::Perp1T    Perp1FluxT; ///< Flux in direction 2
    typedef typename LBMS::FaceFieldSelector<BundleDirT>::Perp2T    Perp2FluxT; ///< Flux in direction 3

    typedef typename LBMS::CoarseFieldSelector<ParFluxT>::type      ParCoarseT;
    typedef typename LBMS::CoarseFieldSelector<Perp1FluxT>::type    Perp1CoarseT;   ///< Coarse Flux in direction 2
    typedef typename LBMS::CoarseFieldSelector<Perp2FluxT>::type    Perp2CoarseT;   ///< Coarse Flux in direction 3

    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Gradient,  FieldT,    ParFluxT >::type ParGradT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Gradient,  FieldT,  Perp1FluxT >::type Perp1GradT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Gradient,  FieldT,  Perp2FluxT >::type Perp2GradT;

    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence, ParFluxT,   FieldT >::type ParDivT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence, Perp1FluxT, FieldT >::type Perp1DivT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Divergence, Perp2FluxT, FieldT >::type Perp2DivT;

    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, FieldT, ParFluxT   >::type ParInterpT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, FieldT, Perp1FluxT >::type Perp1InterpT;
    typedef typename SpatialOps::OperatorTypeBuilder< SpatialOps::Interpolant, FieldT, Perp2FluxT >::type Perp2InterpT;

    template<typename BundleDirT, typename FieldDirT> struct FaceSelector;
    template<typename BundleDirT> struct FaceSelector<BundleDirT, SpatialOps::XDIR>{
                                  typedef typename LBMS::NativeFieldSelector<BundleDirT>::XFaceT type; };
    template<typename BundleDirT> struct FaceSelector<BundleDirT, SpatialOps::YDIR>{
                                  typedef typename LBMS::NativeFieldSelector<BundleDirT>::YFaceT type; };
    template<typename BundleDirT> struct FaceSelector<BundleDirT, SpatialOps::ZDIR>{
                                  typedef typename LBMS::NativeFieldSelector<BundleDirT>::ZFaceT type; };

    /**
     *  \brief Base class for LBMS transport equations.
     *
     *  \param bundle The LBMS bundle that this TransportEquation will
     *         be built on.
     *  \param varname The name for the solution variable that this
     *         transport equation is solving.
     *  \param rhsTag The Expr::Tag for the RHS expression associated with
     *         this TransportEquation.
     */
    TransportEquationBase( const BundlePtr bundle,
                           const std::string varname,
                           const Expr::Tag rhsTag );

    virtual ~TransportEquationBase();

//    virtual void setup_boundary_conditions( GraphCategories& gc,
//                                            std::vector< boost::shared_ptr< NSCBC::BCBuilder<FieldT> > >& bcBuilders ) = 0;

  protected:

    const bool doX_, ///< true if x-direction is active on this bundle
               doY_, ///< true if y-direction is active on this bundle.
               doZ_; ///< true if z-direction is active on this bundle.

    const BundlePtr bundle_;  ///< The bundle that this TransportEquation lives on.

    const std::string rawSolnVarName_;  ///< without the bundle direction appended

    void build_convective_fluxes( Expr::ExpressionFactory& factory,
                                  GraphCategories& gc,
                                  FieldTagInfo& rhsInfo );

    void build_nscbc_convflux( Expr::ExpressionFactory& factory, const typename LBMS::NSCBCVecType<FieldT>::NSCBCVec& nscbcVec );

    template< typename DirT >
    void build_nscbc_neumann( Expr::ExpressionFactory& factory,
                              const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                              const Expr::Tag& modTag,
                              const Expr::Tag& fieldTag );

    template< typename DirT >
    void build_one_sided_convection( Expr::ExpressionFactory& factory,
                                     const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                                     const Expr::Tag& modTag,
                                     const Expr::Tag& fieldTag );
    template< typename DirT >
    void set_diffusive_flux_boundary( Expr::ExpressionFactory& factory,
                                      const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                                      const Expr::Tag& modTag,
                                      const Expr::Tag& fieldTag,
                                      const int isNormalStress );

    template< typename DirT >
    void zero_convective_flux( Expr::ExpressionFactory& factory,
                               const std::pair< boost::shared_ptr< NSCBC::BCBuilder<FieldT> >, LBMS::PointCollection2D >& nscbcPair,
                               const Expr::Tag& modTag,
                               const Expr::Tag& fieldTag );




    const StringNames& sName_;

  };

} // namespace LBMS

#endif // LBMSTransportEquation_h
