/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <transport/DensityEquation.h>
#include <lbms/Bundle.h>
#include <transport/ParseEquation.h>
#include <mpi/FieldBundleExchange.h>
#include <operators/Operators.h>

//--- Expressions ---//
#include <exprs/ConvectiveFlux.h>
#include <exprs/ScalarRHS.h>

//--- Cantera Headers ---//
#include <pokitt/CanteraObjects.h>
#include <cantera/IdealGasMix.h>

// Boost Includes
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

using Expr::Tag;
using Expr::STATE_NONE;
using Expr::STATE_N;

//====================================================================

template< typename ScalarT >
DensityEquation<ScalarT>::
DensityEquation( Expr::ExpressionFactory& exprFactory,
                 const LBMS::BundlePtr bundle,
                 const LBMS::Options& options,
                 const bool doAdvection,
                 GraphCategories& gc,
                 const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
  : LBMS::TransportEquationBase<ScalarT>( bundle,
                                          StringNames::self().density,
                                          create_tag( StringNames::self().density+"_RHS", STATE_NONE, bundle->dir() ) ),
    options_( options      ),
    id_     ( bundle->id() )
{
  FieldTagInfo fieldTagInfo;
  if( doAdvection ) this->build_convective_fluxes( exprFactory, gc, fieldTagInfo );

  // RHS Expression
  typedef typename ScalarRHS<ScalarT>::Builder DensityRHS;
  exprFactory.register_expression( new DensityRHS( this->get_rhs_tag(), fieldTagInfo ), false, id_ );

  this->setup_boundary_conditions( gc, nscbcVec );
}

//--------------------------------------------------------------------

template< typename ScalarT >
DensityEquation<ScalarT>::
~DensityEquation()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionID
DensityEquation<ScalarT>::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  using namespace Expr;
  const LBMS::Direction dir = this->bundle_->dir();
  const Tag rhoTag   = create_tag( StringNames::self().density,        STATE_N   , dir );
  const Tag tempTag  = create_tag( options_.pokittOptions.temperature, STATE_NONE, dir );
  const Tag pressTag = create_tag( options_.pokittOptions.pressure,    STATE_NONE, dir );
  const TagList specTags = options_.pokittOptions.species_tags( dir,   STATE_NONE );
  return exprFactory.register_expression( new typename DensityICExpr::Builder( rhoTag, tempTag, pressTag, specTags ), false, id_ );
}

//--------------------------------------------------------------------

template< typename ScalarT >
void
DensityEquation<ScalarT>::DensityICExpr::
evaluate()
{
  //TODO: replace this by the PoKiTT version.
  ScalarT& dens = this->value();

  Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();

  std::vector<double> ptSpec( gas->nSpecies(), 0.0 );

  // pack a vector with species iterators.
  std::vector<typename ScalarT::const_iterator> specIVec;
  for( size_t i=0; i<species_.size(); ++i ){
    specIVec.push_back( species_[i]->field_ref().begin() );
  }
  typename ScalarT::const_iterator itemp = temp_ ->field_ref().begin();
  typename ScalarT::const_iterator ipres = press_->field_ref().begin();
  for( typename ScalarT::iterator idens = dens.begin(); idens!=dens.end(); ++idens, ++itemp, ++ipres ){
    // extract composition and pack it into a temporary buffer.
    typename std::vector<typename ScalarT::const_iterator>::iterator isp=specIVec.begin();
    const typename std::vector<typename ScalarT::const_iterator>::iterator ispe=specIVec.end();
    std::vector<double>::iterator ipt = ptSpec.begin();
    for( ; isp!=ispe; ++ipt, ++isp ){
      *ipt = **isp;
      ++(*isp);
    }
    gas->setState_TPY( *itemp, *ipres, &ptSpec[0] );
    *idens = gas->density();
  }

  CanteraObjects::restore_gasmix( gas );
}

//--------------------------------------------------------------------

template< typename ScalarT >
DensityEquation<ScalarT>::DensityICExpr::
DensityICExpr( const Expr::Tag tempTag,
               const Expr::Tag pressTag,
               const Expr::TagList& yiTag )
  : Expr::Expression<ScalarT>()
{
  temp_  = this->template create_field_request<ScalarT>( tempTag  );
  press_ = this->template create_field_request<ScalarT>( pressTag );
  this->template create_field_vector_request<ScalarT>( yiTag, species_ );
}

//-----------------------------------------------------------------

template<typename ScalarT>
void
DensityEquation<ScalarT>::
setup_boundary_conditions( GraphCategories& gc, const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
{
  //This avoids an issue with BOOST_FOREACH's macro, which will parse an std::pair template argument
  //as two arguments due to the comma in the template arguments.
  typedef const std::pair< boost::shared_ptr< NSCBC::BCBuilder<ScalarT> >, LBMS::PointCollection2D > NSCBCPairT;

  BOOST_FOREACH( const NSCBCPairT& bc, nscbcVec ){
    const NSCBC::BCType bT = bc.first->get_boundary_type();
    //if( bT == NSCBC::HARD_INFLOW || bT == NSCBC::NONREFLECTING ){
    //dac Currently, it is not apparent if the NSCBC nonreflecting hard inflows
    //    are working appropriately, "reflecting" Hard inflow, however, works fine.
    if( bT == NSCBC::NONREFLECTING ){
      (bc.first)->attach_rhs_modifier( *gc[ADVANCE_SOLUTION]->exprFactory, this->get_rhs_tag(), NSCBC::DENSITY, -1 );
    }
  }
  this->build_nscbc_convflux( *gc[ADVANCE_SOLUTION]->exprFactory, nscbcVec );
}

//--------------------------------------------------------------------

template class DensityEquation<LBMS::XVolField>;
template class DensityEquation<LBMS::YVolField>;
template class DensityEquation<LBMS::ZVolField>;
