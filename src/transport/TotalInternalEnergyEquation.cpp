/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <transport/TotalInternalEnergyEquation.h>
#include <transport/ParseEquation.h>
#include <lbms/Bundle.h>
#include <mpi/FieldBundleExchange.h>
#include <operators/Operators.h>

//--- Expressions that we build here ---//
#include <exprs/ScalarRHS.h>
#include <exprs/IntEnergyFlux.h>
#include <exprs/KineticEnergy.h>
#include <exprs/PrimVar.h>
#include <exprs/HeatFlux.h>
#include <exprs/ConvectiveFlux.h>
#include <exprs/TemperatureFromIntEnergy.h>

#include <pokitt/thermo/Temperature.h>
#include <pokitt/transport/ThermalCondMix.h>
#include <pokitt/thermo/HeatCapacity_Cp.h>
#include <pokitt/thermo/HeatCapacity_Cv.h>

//--- Cantera Headers ---//
#include <pokitt/CanteraObjects.h>
#include <cantera/IdealGasMix.h>

#include <boost/lexical_cast.hpp>

using Expr::STATE_NONE;
using Expr::STATE_N;
using Expr::Tag;
using namespace LBMS;

/**
 *  @class RhoE0ICExpr
 *  @brief Sets the initial condition for the total internal energy equation.
 */
template<typename ScalarT>
class RhoE0ICExpr : public Expr::Expression<ScalarT>
{
  RhoE0ICExpr( const Expr::Tag tempTag,
               const Expr::Tag pressTag,
               const Expr::Tag densTag,
               const Expr::TagList& yiTag,
               const Expr::Tag xvelTag,
               const Expr::Tag yvelTag,
               const Expr::Tag zvelTag )
  : Expr::Expression<ScalarT>(),
    haveX_( xvelTag != Expr::Tag() ),
    haveY_( yvelTag != Expr::Tag() ),
    haveZ_( zvelTag != Expr::Tag() )
  {
    temp_  = this->template create_field_request<ScalarT>( tempTag  );
    press_ = this->template create_field_request<ScalarT>( pressTag );
    dens_  = this->template create_field_request<ScalarT>( densTag  );

    if( haveX_ ) xvel_ = this->template create_field_request<ScalarT>( xvelTag );
    if( haveY_ ) yvel_ = this->template create_field_request<ScalarT>( yvelTag );
    if( haveZ_ ) zvel_ = this->template create_field_request<ScalarT>( zvelTag );

    this->template create_field_vector_request<ScalarT>( yiTag, species_ );
  }

  const bool haveX_, haveY_, haveZ_;
  DECLARE_FIELDS( ScalarT, temp_, press_, dens_, xvel_, yvel_, zvel_ )
  DECLARE_VECTOR_OF_FIELDS( ScalarT, species_ )

public:
  struct Builder : public Expr::ExpressionBuilder
  {
    Builder( const Expr::Tag result,
             const Expr::Tag tempTag,
             const Expr::Tag pressTag,
             const Expr::Tag densTag,
             const Expr::TagList& yiTag,
             const Expr::Tag xvelTag,
             const Expr::Tag yvelTag,
             const Expr::Tag zvelTag )
    : ExpressionBuilder(result),
      tT_   ( tempTag  ),
      pT_   ( pressTag ),
      dT_   ( densTag  ),
      xvelT_( xvelTag  ),
      yvelT_( yvelTag  ),
      zvelT_( zvelTag  ),
      yiT_  ( yiTag    )
    {}

    Expr::ExpressionBase* build() const{ return new RhoE0ICExpr( tT_, pT_, dT_, yiT_, xvelT_, yvelT_, zvelT_ ); }

  private:
    const Expr::Tag tT_, pT_, dT_, xvelT_, yvelT_, zvelT_;
    const Expr::TagList yiT_;
  };

  ~RhoE0ICExpr(){}

  void evaluate()
  {
    ScalarT& rhoE0 = this->value();

    Cantera::IdealGasMix* const gas = CanteraObjects::get_gasmix();
    std::vector<double> ptSpec( gas->nSpecies(), 0.0 );

    // pack a vector with species iterators.
    std::vector<typename ScalarT::const_iterator> specIVec;
    for( size_t i=0; i<species_.size(); ++i ){
      specIVec.push_back( species_[i]->field_ref().begin() );
    }
    typedef typename ScalarT::const_iterator ScalarConstIter;
    ScalarConstIter itemp = temp_ ->field_ref().begin();
    ScalarConstIter ipres = press_->field_ref().begin();
    ScalarConstIter idens = dens_ ->field_ref().begin();
    ScalarConstIter ixvel = haveX_ ? xvel_->field_ref().begin() : itemp;
    ScalarConstIter iyvel = haveY_ ? yvel_->field_ref().begin() : itemp;
    ScalarConstIter izvel = haveZ_ ? zvel_->field_ref().begin() : itemp;

    for( typename ScalarT::iterator ire0 = rhoE0.begin();
         ire0!=rhoE0.end();
         ++ire0, ++itemp, ++ipres, ++idens )
      {
        // calculate kinetic energy
        double ke = 0.0;
        if( haveX_ ){ ke += 0.5 * *ixvel**ixvel;  ++ixvel; }
        if( haveY_ ){ ke += 0.5 * *iyvel**iyvel;  ++iyvel; }
        if( haveZ_ ){ ke += 0.5 * *izvel**izvel;  ++izvel; }

        // extract composition and pack it into a temporary buffer.
        typedef typename std::vector<ScalarConstIter>::iterator VecScalarIter;
        VecScalarIter isp=specIVec.begin();
        const VecScalarIter ispe=specIVec.end();
        typename std::vector<double>::iterator ipt = ptSpec.begin();
        for( ; isp!=ispe; ++ipt, ++isp ){
          *ipt = **isp;
          ++(*isp);
        }
        gas->setState_TPY( *itemp, *ipres, &ptSpec[0] );

        const double e0 = gas->intEnergy_mass() + ke;
        *ire0 = *idens * e0;
      }
    CanteraObjects::restore_gasmix( gas );
  }

}; // class RhoE0ICExpr

//====================================================================

template< typename ScalarT >
TotalInternalEnergyEquation<ScalarT>::
TotalInternalEnergyEquation( Expr::ExpressionFactory& exprFactory,
                             const LBMS::BundlePtr bundle,
                             const LBMS::Options& options,
                             const bool doAdvection,
                             GraphCategories& gc,
                             const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
  : LBMS::TransportEquationBase<ScalarT>( bundle,
                                          StringNames::self().rhoE0,
                                          create_tag(StringNames::self().rhoE0 + "_RHS", STATE_NONE, bundle->dir() ) ),
  options_( options ),
  sName_( StringNames::self() ),

  id_( bundle->id() ),

  parBundleName_  ( bundle->name() ),
  perpBundleName1_( get_bundle_name_suffix( get_perp1( bundle->dir() ) ) ),
  perpBundleName2_( get_bundle_name_suffix( get_perp2( bundle->dir() ) ) ),
  coarseName_( sName_.coarse ),

  parDir_  (            bundle->dir()   ),
  perp1Dir_( get_perp1( bundle->dir() ) ),
  perp2Dir_( get_perp2( bundle->dir() ) ),

  do1_( bundle->npts(parDir_  ) > 1 ),
  do2_( bundle->npts(perp1Dir_) > 1 ),
  do3_( bundle->npts(perp2Dir_) > 1 )
{
  using Expr::STATE_NONE;
  using Expr::STATE_N;
  using Expr::STATE_NP1;
  using Expr::Tag;
  using namespace LBMS;

  const std::string vel1      = select_from_dir( parDir_  , sName_.xvel, sName_.yvel, sName_.zvel );
  const std::string vel2      = select_from_dir( perp1Dir_, sName_.xvel, sName_.yvel, sName_.zvel );
  const std::string vel3      = select_from_dir( perp2Dir_, sName_.xvel, sName_.yvel, sName_.zvel );

  const std::string parFace   = select_from_dir( parDir_  , sName_.xface, sName_.yface, sName_.zface );
  const std::string perp1Face = select_from_dir( perp1Dir_, sName_.xface, sName_.yface, sName_.zface );
  const std::string perp2Face = select_from_dir( perp2Dir_, sName_.xface, sName_.yface, sName_.zface );

  const Tag velt1 =        create_tag( vel1, STATE_NONE, parDir_ );
  const Tag velt2 = do2_ ? create_tag( vel2, STATE_NONE, parDir_ ) : Tag();
  const Tag velt3 = do3_ ? create_tag( vel3, STATE_NONE, parDir_ ) : Tag();

  const Tag taut11 =              create_tag( get_tau_name( parDir_,            parDir_   ), STATE_NONE, parDir_ );
  const Tag taut12 = this->do2_ ? create_tag( get_tau_name( parDir_,            perp1Dir_ ), STATE_NONE, parDir_ ) : Tag();
  const Tag taut13 = this->do3_ ? create_tag( get_tau_name( parDir_,            perp2Dir_ ), STATE_NONE, parDir_ ) : Tag();
  const Tag taut21 =              create_tag( get_tau_name( get_perp1(parDir_), parDir_   ) + sName_.fine, STATE_NONE, parDir_ );
  const Tag taut22 = this->do2_ ? create_tag( get_tau_name( get_perp1(parDir_), perp1Dir_ ) + sName_.fine, STATE_NONE, parDir_ ) : Tag();
  const Tag taut23 = this->do3_ ? create_tag( get_tau_name( get_perp1(parDir_), perp2Dir_ ) + sName_.fine, STATE_NONE, parDir_ ) : Tag();
  const Tag taut31 =              create_tag( get_tau_name( get_perp2(parDir_), parDir_   ) + sName_.fine, STATE_NONE, parDir_ );
  const Tag taut32 = this->do2_ ? create_tag( get_tau_name( get_perp2(parDir_), perp1Dir_ ) + sName_.fine, STATE_NONE, parDir_ ) : Tag();
  const Tag taut33 = this->do3_ ? create_tag( get_tau_name( get_perp2(parDir_), perp2Dir_ ) + sName_.fine, STATE_NONE, parDir_ ) : Tag();

  const Tag heatFlux1Tag = create_tag( sName_.heat_flux + parFace,   STATE_NONE, parDir_ );
  const Tag heatFlux2Tag = create_tag( sName_.heat_flux + perp1Face, STATE_NONE, parDir_ );
  const Tag heatFlux3Tag = create_tag( sName_.heat_flux + perp2Face, STATE_NONE, parDir_ );

  const Tag thermCondTag = create_tag( options.pokittOptions.thermCond,   STATE_NONE, parDir_ );

  const Tag tempTag = create_tag( options.pokittOptions.temperature, STATE_NONE, parDir_ );
  const Tag rhoE0t  = create_tag( sName_.rhoE0,   STATE_N,    parDir_ );
  const Tag e0Tag   = create_tag( sName_.e0,      STATE_NONE, parDir_ );
  const Tag denst   = create_tag( sName_.density, STATE_N,    parDir_ );
  const Tag keTag   = create_tag( sName_.ke,      STATE_NONE, parDir_ );
  const Tag mwTag   = create_tag( sName_.mixmw,   STATE_NONE, parDir_ );

  const Expr::TagList speciesTags = options.pokittOptions.species_tags( parDir_, STATE_NONE );

  FieldTagInfo fieldTagInfo;

  // jcs note that if we have body forces, these will need to be added
  // as appropriate source terms here.

  //-----------------------------------------------------------------------------
  // Total Energy Flux
  // total energy flux in on-line direction
  const std::string eDiFlux = sName_.rhoE0+"_diffusiveflux";
  if( do1_ ){
    typedef typename IntEnergyFlux<typename BaseT::ParInterpT>::Builder IntEnergyFlux1;
    const Expr::Tag dfluxTag = create_tag( eDiFlux+parFace, STATE_NONE, parDir_ );
    exprFactory.register_expression( new IntEnergyFlux1( dfluxTag,
                                                         create_tag( options_.pokittOptions.pressure, STATE_NONE, parDir_ ),
                                                         rhoE0t,
                                                         taut11, taut12, taut13,
                                                         velt1, velt2, velt3,
                                                         heatFlux1Tag ),
                                     false, id_ );

    LBMS::setup_field_exchange<typename BaseT::ParFluxT>( bundle, create_tag( eDiFlux+parFace, STATE_NONE, parDir_ ), exprFactory, gc, parDir_, parDir_ );
    fieldTagInfo[ select_from_dir(parDir_,DIFFUSIVE_FLUX_X,DIFFUSIVE_FLUX_Y,DIFFUSIVE_FLUX_Z) ] = dfluxTag;
  }

  if( do2_ ){
    const Tag dFineFluxTag = create_tag( eDiFlux+perp1Face + sName_.fine, STATE_NONE, parDir_ );
    typedef typename IntEnergyFlux<Perp1InterpT>::Builder IntEnergyFlux2;
    exprFactory.register_expression( new IntEnergyFlux2( dFineFluxTag,
                                                         create_tag( options_.pokittOptions.pressure, STATE_NONE, parDir_ ),
                                                         rhoE0t,
                                                         taut22, taut21, taut23,
                                                         velt2, velt1, velt3,
                                                         heatFlux2Tag ),
                                     false, id_ );

    const Expr::Tag dfluxTag       = create_tag( eDiFlux + perp1Face,                        STATE_NONE, parDir_ );
    const Expr::Tag dCoarseFluxTag = create_tag( eDiFlux + perp1Face + sName_.coarse,        STATE_NONE, parDir_ );
    const Expr::Tag reconsTag      = create_tag( eDiFlux + sName_.reconstructed + perp1Face, STATE_NONE, parDir_ );

    LBMS::setup_field_exchange<typename BaseT::Perp1FluxT>( bundle, dfluxTag, exprFactory, gc, perp1Dir_, parDir_ );

    typedef typename OffLineFlux< typename BaseT::Perp1FluxT, typename BaseT::Perp1CoarseT >::Builder OffLineFluxBuilder;
    exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                              dCoarseFluxTag,
                                                              dFineFluxTag,
                                                              bundle ),
                                 false, id_ );

    fieldTagInfo[ select_from_dir(perp1Dir_,DIFFUSIVE_FLUX_X,DIFFUSIVE_FLUX_Y,DIFFUSIVE_FLUX_Z) ] = reconsTag;
  }
  if( do3_ ){
    const Tag dFineFluxTag = create_tag( eDiFlux+perp2Face + sName_.fine, STATE_NONE, parDir_ );
    typedef typename IntEnergyFlux<Perp2InterpT>::Builder IntEnergyFlux3;
    exprFactory.register_expression( new IntEnergyFlux3( dFineFluxTag,
                                                         create_tag( options_.pokittOptions.pressure, STATE_NONE, parDir_ ),
                                                         rhoE0t,
                                                         taut33, taut31, taut32,
                                                         velt3, velt1, velt2,
                                                         heatFlux3Tag ),
                                     false, id_ );

    const Expr::Tag dfluxTag       = create_tag( eDiFlux + perp2Face,                        STATE_NONE, parDir_ );
    const Expr::Tag dCoarseFluxTag = create_tag( eDiFlux + perp2Face + sName_.coarse,        STATE_NONE, parDir_ );
    const Expr::Tag reconsTag      = create_tag( eDiFlux + sName_.reconstructed + perp2Face, STATE_NONE, parDir_ );

    LBMS::setup_field_exchange<typename BaseT::Perp2FluxT>( bundle, dfluxTag, exprFactory, gc, perp2Dir_, parDir_ );

    typedef typename OffLineFlux< typename BaseT::Perp2FluxT, typename BaseT::Perp2CoarseT >::Builder OffLineFluxBuilder;

    exprFactory.register_expression( new OffLineFluxBuilder ( reconsTag,
                                                              dCoarseFluxTag,
                                                              dFineFluxTag,
                                                              bundle ),
                                 false, id_ );

    fieldTagInfo[ select_from_dir(perp2Dir_,DIFFUSIVE_FLUX_X,DIFFUSIVE_FLUX_Y,DIFFUSIVE_FLUX_Z) ] = reconsTag;
  }

  //-------------------------------------------------------------
  // convective fluxes
  //
  if( doAdvection ) this->build_convective_fluxes( exprFactory, gc, fieldTagInfo );

  //-----------------------------------------------------------------------------
//   temperature calculation
  typedef typename TemperatureFromE0<ScalarT>::Builder TempBuilder;
  exprFactory.register_expression( new TempBuilder( tempTag, e0Tag, keTag, denst, speciesTags ), false, id_ );
  //Necessary to avoid temperature solves (and failures) in corner ghost cells
  communicate_expression_result<ScalarT>( tempTag, bundle, exprFactory );

  // kinetic energy calculation
  typedef typename KineticEnergy<ScalarT>::Builder KinEngBuilder;
  if( !do2_ && do3_ )
    exprFactory.register_expression( new KinEngBuilder( keTag, velt1, velt3, velt2 ), false, id_ );
  else
    exprFactory.register_expression( new KinEngBuilder( keTag, velt1, velt2, velt3 ), false, id_ );

  // extract e0 from rho*e0
  typedef typename PrimVar<ScalarT>::Builder E0Builder;
  exprFactory.register_expression( new E0Builder( create_tag( sName_.e0, STATE_NONE, parDir_ ),
                                                  rhoE0t,
                                                  denst ),
                                   false, id_);

  //-----------------------------------------------------------------------------
  // Heat flux calculation
  // Line parallel direction
  // For flux reconstruction, we will need to turn on the perpendicular calculations

  typedef typename HeatFlux< ScalarT, typename BaseT::ParFluxT   >::Builder HeatFlux1;

  typedef typename HeatFlux< ScalarT, typename BaseT::Perp2FluxT >::Builder HeatFlux3;
  const Expr::TagList specEnthTags = options.pokittOptions.species_tags( parDir_, STATE_NONE, sName_.enthalpy );
  const Expr::TagList spFluxTags   = options.pokittOptions.species_flux_tags( parDir_, parDir_, STATE_NONE  );
  if( options.pokittOptions.doSpeciesTransport ){
    exprFactory.register_expression( new HeatFlux1( heatFlux1Tag, thermCondTag, tempTag, spFluxTags, specEnthTags ), false, id_ );

    if( do2_ ){
      const Expr::TagList spFlux2Tags   = options.pokittOptions.species_flux_tags( perp1Dir_, parDir_, STATE_NONE  );

      typedef typename HeatFlux< ScalarT, typename BaseT::Perp1FluxT >::Builder HeatFlux2;
      exprFactory.register_expression( new HeatFlux2( heatFlux2Tag, thermCondTag, tempTag, spFlux2Tags, specEnthTags ), false, id_ );
    }
    if( do3_ ){
      const Expr::TagList spFlux3Tags   = options.pokittOptions.species_flux_tags( perp2Dir_, parDir_, STATE_NONE  );

      typedef typename HeatFlux< ScalarT, typename BaseT::Perp2FluxT >::Builder HeatFlux3;
      exprFactory.register_expression( new HeatFlux3( heatFlux3Tag, thermCondTag, tempTag, spFlux3Tags, specEnthTags ), false, id_ );
    }
  }
  else{
    exprFactory.register_expression( new HeatFlux1( heatFlux1Tag, thermCondTag, tempTag ), false, id_ );

    if( do2_ ){
      typedef typename HeatFlux< ScalarT, typename BaseT::Perp1FluxT >::Builder HeatFlux2;
      exprFactory.register_expression( new HeatFlux2( heatFlux2Tag, thermCondTag, tempTag ), false, id_ );
    }
    if( do3_ ){
      typedef typename HeatFlux< ScalarT, typename BaseT::Perp2FluxT >::Builder HeatFlux3;
      exprFactory.register_expression( new HeatFlux3( heatFlux3Tag, thermCondTag, tempTag ), false, id_ );
    }
  }

  //----------------------------------------------------------------
  // thermal conductivity
  if( !exprFactory.have_entry(thermCondTag) ){
    typedef typename pokitt::ThermalConductivity<ScalarT>::Builder ThermCond;
    exprFactory.register_expression( new ThermCond( thermCondTag, tempTag, speciesTags, mwTag ), false, id_ );
  }

  // RHS Expression
  typedef typename ScalarRHS<ScalarT>::Builder RHS;
  exprFactory.register_expression( new RHS( this->get_rhs_tag(), fieldTagInfo ), false, id_ );

  this->setup_boundary_conditions( gc, nscbcVec );
}

//--------------------------------------------------------------------

template< typename ScalarT >
TotalInternalEnergyEquation<ScalarT>::
~TotalInternalEnergyEquation()
{}

//--------------------------------------------------------------------

template< typename ScalarT >
Expr::ExpressionID
TotalInternalEnergyEquation<ScalarT>::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  using Expr::STATE_NONE;
  using Expr::STATE_N;
  using Expr::Tag;

  const Tag xvelTag = this->doX_ ? Tag( sName_.xvel + parBundleName_, STATE_NONE ) : Tag();
  const Tag yvelTag = this->doY_ ? Tag( sName_.yvel + parBundleName_, STATE_NONE ) : Tag();
  const Tag zvelTag = this->doZ_ ? Tag( sName_.zvel + parBundleName_, STATE_NONE ) : Tag();

  typedef typename RhoE0ICExpr<ScalarT>::Builder RhoIC;
  return exprFactory.register_expression( new RhoIC( Tag( this->solnVarName_,STATE_N),
                                                     create_tag( options_.pokittOptions.temperature, STATE_NONE, parDir_ ),
                                                     create_tag( options_.pokittOptions.pressure,    STATE_NONE, parDir_ ),
                                                     create_tag( sName_.density,                     STATE_N,    parDir_ ),
                                                     options_.pokittOptions.species_tags( parDir_, STATE_NONE ),
                                                     xvelTag, yvelTag, zvelTag ),
                                          false, id_ );
}

//--------------------------------------------------------------------

template< typename ScalarT >
void
TotalInternalEnergyEquation<ScalarT>::
setup_boundary_conditions( GraphCategories& gc, const typename LBMS::NSCBCVecType<ScalarT>::NSCBCVec& nscbcVec )
{
  //We check here to ensure that cp and cv are created
  const Expr::Tag cpTag   = create_tag( sName_.cp,                          STATE_NONE, parDir_ );
  const Expr::Tag cvTag   = create_tag( sName_.cv,                          STATE_NONE, parDir_ );
  const Expr::Tag tempTag = create_tag( options_.pokittOptions.temperature, STATE_NONE, parDir_ );

  GraphHelper* const solnGraphHelper = gc[ADVANCE_SOLUTION];
  Expr::ExpressionFactory& factory = *solnGraphHelper->exprFactory;

  //If they are not created, we create them
  if( !( factory.have_entry(cpTag) ) ){
    typedef typename pokitt::HeatCapacity_Cp<ScalarT>::Builder CP;
    factory.register_expression( new CP( cpTag, tempTag, options_.pokittOptions.species_tags( parDir_, STATE_NONE ) ), false, id_ );
  }
  if( !( factory.have_entry(cvTag) ) ){
    typedef typename pokitt::HeatCapacity_Cv<ScalarT>::Builder CV;
    factory.register_expression( new CV( cvTag, tempTag, options_.pokittOptions.species_tags( parDir_, STATE_NONE ) ), false, id_ );
  }

  //This avoids an issue with BOOST_FOREACH's macro, which will parse an std::pair template argument
  //as two arguments due to the comma in the template arguments.
  const typedef std::pair< boost::shared_ptr< NSCBC::BCBuilder<ScalarT> >, LBMS::PointCollection2D > NSCBCPairT;

  BOOST_FOREACH( const NSCBCPairT& bc, nscbcVec ){
    (bc.first)->attach_rhs_modifier( factory, this->get_rhs_tag(), NSCBC::ENERGY, -1 );

    const NSCBC::BCType bT = bc.first->get_boundary_type();
    if( bT == NSCBC::NONREFLECTING ){
      //Setting up diffusive boundary conditions
      //Note that 'direction((bc.first)->dir_)' converts from NSCBC::XDIR to LBMS::XDIR by
      //going through the integer values associated with the direction (i.e. SpatialOps::XDIR::value)
      //This should be safe as long as the NSCBC and LBMS direction types are both built upon SpatialOps
      //Also note that we are applying this to the total flux as opposed to the heat flux.  This should
      //not matter, however, as the stress tensor and pressure gradient will also be zero'd out.
      //Thus, the total internal energy diffusive flux is equivalent to the heat flux for NSCBC

      const std::string faceString = select_from_dir( direction(bc.first->get_boundary_direction()), sName_.xface, sName_.yface, sName_.zface );
      const Expr::Tag modTag1 = create_tag( sName_.rhoE0+"_diffusiveflux_mod1" + faceString
                                            + bc.first->type()
                                            + bc.first->side(),
                                            Expr::STATE_NONE,
                                            parDir_ );
      const Expr::Tag modTag2 = create_tag( sName_.rhoE0+"_diffusiveflux_mod2" + faceString
                                            + bc.first->type()
                                            + bc.first->side(),
                                            Expr::STATE_NONE,
                                            parDir_ );

      const Expr::Tag diffFluxTag = direction(bc.first->get_boundary_direction()) == parDir_ ?
                                    create_tag( sName_.rhoE0+"_diffusiveflux"+faceString, Expr::STATE_NONE, parDir_ ) :
                                    create_tag( sName_.rhoE0+"_diffusiveflux"+faceString+sName_.fine, Expr::STATE_NONE, parDir_ );

      switch( bc.first->get_boundary_direction() ){
        case SpatialOps::XDIR::value:
          this->template build_nscbc_neumann<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffFluxTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::XDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffFluxTag, -1 );
          break;
        case SpatialOps::YDIR::value:
          this->template build_nscbc_neumann<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffFluxTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::YDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffFluxTag, -1 );
          break;
        case SpatialOps::ZDIR::value:
          this->template build_nscbc_neumann<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag1, diffFluxTag );
//          this->template set_diffusive_flux_boundary<SpatialOps::ZDIR>( *gc[ADVANCE_SOLUTION]->exprFactory, bc, modTag2, diffFluxTag, -1 );
          break;
        case SpatialOps::NODIR::value:
        {
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << std::endl
              << "No direction was given" << std::endl;
          throw std::runtime_error( msg.str() );
          break;
        }
      }//End Switch
    }//End if
  }//End BOOST_FOREACH
  this->build_nscbc_convflux( factory, nscbcVec );
}

//====================================================================

template class TotalInternalEnergyEquation<LBMS::XVolField>;
template class TotalInternalEnergyEquation<LBMS::YVolField>;
template class TotalInternalEnergyEquation<LBMS::ZVolField>;
