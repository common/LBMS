/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   ParseTools.h
 *  \brief  General tools helpful when parsing
 *  \author James C. Sutherland
 */

#ifndef ParseTools_h
#define ParseTools_h

#include <string>
#include <map>

#include <lbms/Direction.h>
#include <lbms/Bundle_fwd.h>

#include <expression/ExprFwd.h>

#include <spatialops/structured/IntVec.h>
#include <lbms/Coordinate.h>

#include <yaml-cpp/yaml.h>

// forward declaration
namespace YAML{
  template<>
  struct convert<SpatialOps::IntVec> {
    static Node encode(const SpatialOps::IntVec& rhs) {
      Node node;
      node.push_back( rhs[0] );
      node.push_back( rhs[1] );
      node.push_back( rhs[2] );
      return node;
    }

    static bool decode(const Node& node, SpatialOps::IntVec& rhs) {
      if(!node.IsSequence() || node.size() != 3)
        return false;
      rhs[0] = node[0].as<int>();
      rhs[1] = node[1].as<int>();
      rhs[2] = node[2].as<int>();
      return true;
    }
  };

  template<>
  struct convert<LBMS::Coordinate> {
    static Node encode(const LBMS::Coordinate& rhs) {
      Node node;
      node.push_back( rhs[0] );
      node.push_back( rhs[1] );
      node.push_back( rhs[2] );
      return node;
    }

    static bool decode(const Node& node, LBMS::Coordinate& rhs) {
      if(!node.IsSequence() || node.size() != 3)
        return false;
      rhs[0] = node[0].as<double>();
      rhs[1] = node[1].as<double>();
      rhs[2] = node[2].as<double>();
      return true;
    }
  };

  template<>
  struct convert<LBMS::Faces> {
    static Node encode( const LBMS::Faces& face ){
      Node node;
      node.push_back( face_name(face) );
      return node;
    }
    static bool decode( const Node& node, LBMS::Faces& face ){
      if( node.IsSequence() ) return false;
      face = LBMS::get_face( node.as<std::string>() );
      return true;
    }
  };
}

namespace LBMS{

  /** \addtogroup Parser
   *  @{
   */

  /**
   *  \fn Expr::Tag parse_nametag(const ParseGroup&, const LBMS::Direction);
   *
   *  \brief Parses a name tag, comprised of a variable name and state. The
   *         bundle direction information is appended to the name.
   *
   *  \param param The parser block for this name tag.
   *  \param dir the Bundle direction.  If NODIR, then no direction information is appended.
   *  \return the Expr::Tag.
   */
  Expr::Tag parse_nametag( const YAML::Node& param, const LBMS::Direction dir );

  /**
   *  \fn Expr::TagList parse_taglist(const ParseGroup&, const LBMS::Direction);
   *
   *  \brief Parses a list of name tags. The bundle direction information is appended to the name.
   *
   *  \param param The parser block for this name tag.
   *  \param dir the Bundle direction.  If NODIR, then no direction information is appended.
   *  \return the Expr::TagList.
   */
  Expr::TagList
  parse_taglist( const YAML::Node& param, const LBMS::Direction dir );

  /**
   *  \enum FieldType
   *  \brief Enumerate the field types that can be handled by parsing.
   *  \sa get_field_type
   */
  enum FieldType{
    VOLUME,  ///< Volume fields (on each bundle) corresponds to string "VOLUME"
    XFACE,   ///< X-Face fields (on each bundle) corresponds to string "XFACE"
    YFACE,   ///< Y-Face fields (on each bundle) corresponds to string "YFACE"
    ZFACE    ///< Z-Face fields (on each bundle) corresponds to string "ZFACE"
  };

  /**
   *  \fn FieldType get_field_type( std::string );
   *  \brief Convert a (case-insensitive) string into a FieldType enum.
   *   \sa FieldType for allowed string names.
       Invalid strings result in an exception being thrown.
   *  \param key the string
   *  \return the eunum value
   */
  FieldType get_field_type( std::string key );

# ifdef HAVE_MPI
  class IDXWriters; // forward

  /**
   * \fn IDXWriters* parse_vis_fields( const ParseGroup&, const BundlePtrVec& );
   * @param param the main parser (so that we can include the job name), idx node is generated in function
   * @param bundles the set of bundles to extract fields from
   * @return a new IDXWriters object.
   */
  IDXWriters*
  parse_idx_vis_fields( const YAML::Node& parser, const BundlePtrVec& bundles );
# endif

/** @} */

} // namespace LBMS


#endif // ParseTools_h
