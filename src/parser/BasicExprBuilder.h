/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef BasicExprBuilder_h
#define BasicExprBuilder_h


#include <lbms/GraphHelperTools.h>
#include <lbms/Bundle_fwd.h>

/**
 *  \file BasicExprBuilder.h
 *  \brief parser support for creating some basic expressions.
 */

namespace Expr{ class ExpressionBuilder; }
namespace YAML{ class Node; }

namespace LBMS{
  /**
   *  \fn void create_expressions_from_input( const ParseGroup&, const BundlePtr, GraphCategories& );
   *
   *  \brief Creates expressions from the ones explicitly defined in the input file
   *
   *  \param parser - the ParseGroup block that contains \verbatim <BasicExpression> \endverbatim tags
   *  \param bundle - the bundle to create expressions on
   *  \param gc - the GraphCategories object that this expression should be associated with.
   *
   *  Note that the expression will be created on all bundle directions, so
   *  a generic field type "VOLUME" "XFACE", "YFACE" or "ZFACE" is expected.
   */
  void
  create_expressions_from_input( const YAML::Node& parser,
                                 const BundlePtr bundle,
                                 GraphCategories& gc );

  /**
   *  \fn   template<typename FieldT> void build_coord_expr( Expr::ExpressionFactory* const, const BundlePtr& );
   *  \brief build expressions to calculate the coordinates - may be required for other expressions in the graph.
   *  \param exprFactory the factory to register these on
   *  \param bundle the bundle associated with these expressions
   */
  template<typename FieldT>
  void
  build_coord_expr( Expr::ExpressionFactory* const exprFactory,
                    const BundlePtr bundle,
                    GraphHelper* const gHelper);

} // namespace LBMS

#endif // BasicExprBuilder_h
