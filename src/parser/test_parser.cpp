/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ParseGroup.h"
#include <test/TestHelper.h>

#include <boost/program_options.hpp>
namespace po=boost::program_options;

#include <spatialops/structured/IntVec.h>

#include <string>
#include <iostream>
#include <fstream>
using std::cout; using std::endl;
using std::string;

bool parse_file( const string& fname )
{
  try{
    cout << "Parsing '" << fname << "'" << endl;
    ParseGroup p(fname);

    std::ostringstream os; os << p;
    std::istringstream is(os.str());
    ParseGroup p2(is);

    std::ostringstream os2; os2 << p2;
    std::istringstream is2(os2.str());
    ParseGroup p3(is2);

    {
      std::ofstream f1( string("new_" +fname).c_str() );   f1 << p2;
      std::ofstream f2( string("new2_"+fname).c_str() );   f2 << p3;
    }

    TestHelper status(false);
    status( p2==p3, "p2==p3" );
    return status.ok();
  }
  catch( std::exception& err ){
    cout << err.what() << std::endl;
  }
  return false;
}

void
create_xml( const string& fname )
{
  std::ofstream xml( fname.c_str() );

  xml << "<ints> 1 2  3 </ints>" << std::endl
      << "<dblvec> 1.1 2.2  3.3 , 4.4</dblvec>" << std::endl
      << "<MyTag a1=\"Hi there\" a2=\"12345\">" << std::endl
      << "  <name>John Doe</name>" << std::endl
      << "  <email> John.Doe@gmail.com </email>" << std::endl
      << "  <ssn>123456789</ssn>" << std::endl
      << "</MyTag>" << std::endl
      << "<repeat>1</repeat>" << std::endl
      << "<repeat>2</repeat>" << std::endl
      << "<repeat>3</repeat>" << std::endl;
}

bool query_xml()
{
  using SpatialOps::IntVec;
  TestHelper status(true);

  try{
    create_xml( "test.xml" );

    status( parse_file("test.xml"), "parse & round trip on test.xml" );

    ParseGroup pg("test.xml");

    status( pg.has_child("ints"), "has_child" );
    status( !pg.has_child("invalid"), "has_child" );

    status( pg.get_child("ints").name() == "ints", "name" );

    status( pg.get_value<IntVec>("ints") == IntVec(1,2,3), "IntVec" );

    std::vector<double> dvec(4,0); dvec[0]=1.1; dvec[1]=2.2; dvec[2]=3.3; dvec[3]=4.4;
    status( pg.get_value_vec<double>("dblvec") == dvec, "vector<double>" );
    status( pg.get_child("dblvec").get_value_vec<double>() == dvec, "vector<double>" );

    const ParseGroup mtpg = pg.get_child("MyTag");
    status( mtpg.get_attribute<string>("a1") == "Hi there", "string attribute" );
    status( mtpg.get_attribute<int>("a2") == 12345, "int attribute" );
    status( mtpg.get_value<string>("email") == "John.Doe@gmail.com", "string value" );
    status( mtpg.get_value<int>("ssn") == 123456789, "int value" );
    status( mtpg.get_child("ssn").get_value<int>() == 123456789, "int value" );
    status( pg.get_child("MyTag").get_child("ssn").get_value<int>() == 123456789, "nested query" );

    unsigned i=1;
    for( ParseGroup::const_iterator ii=pg.begin("repeat"); ii!=pg.end("repeat"); ++i, ++ii ){
      status( ii->get_value<unsigned>() == i, "iterator" );
    }

    {
      std::map<string,string> mm = mtpg.get_children<string>();
      status( mm["name" ] == "John Doe",           "children map 1" );
      status( mm["email"] == "John.Doe@gmail.com", "children map 2" );
      status( mm["ssn"  ] == "123456789",          "children map 3" );
    }

    return status.ok();
  }
  catch( std::exception& err ){
    cout << err.what();
    return false;
  }
}

int main( int iarg, char* carg[] )
{
  string fname;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Usage: testparser [filename]");
    desc.add_options()
      ( "help", "print this message" );

    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("input-file", po::value<string>(&fname), "input file");

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    po::positional_options_description p;
    p.add("input-file", -1);

    po::variables_map args;
    po::store( po::command_line_parser(iarg,carg).
               options(cmdline_options).positional(p).run(), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return -1;
    }
  }

  TestHelper status(true);

  status( query_xml() );

  if( !fname.empty() ){
    status( parse_file(fname), "file parsing & round trip" );
  }

  if( status.ok() ){
    cout << "PASS" << endl;
    return 0;
  }
  cout << "FAIL" << endl;
  return -1;
}
