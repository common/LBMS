/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "BasicExprBuilder.h"
#include <yaml-cpp/yaml.h>
#include <parser/ParseTools.h>
#include <fields/Fields.h>
#include <exprs/Coordinate.h>

#include <expression/ExprLib.h>

// dac expressions for custom initializations
#include <exprs/TaylorVortex.h>
#include <parser/ReadInputFile.h>

#include <boost/foreach.hpp>

#include <string>
#include <vector>

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

using std::cout;
using std::endl;
using std::string;

namespace LBMS{

  //-----------------------------------------------------------------

  template<typename T> string coord_name();
  template<> string coord_name<  XVolField>(){ return "_Vol"  +suffix<  XVolField>(); }
  template<> string coord_name<XSurfXField>(){ return "_XSurf"+suffix<XSurfXField>(); }
  template<> string coord_name<XSurfYField>(){ return "_YSurf"+suffix<XSurfYField>(); }
  template<> string coord_name<XSurfZField>(){ return "_ZSurf"+suffix<XSurfZField>(); }
  template<> string coord_name<  YVolField>(){ return "_Vol"  +suffix<  YVolField>(); }
  template<> string coord_name<YSurfXField>(){ return "_XSurf"+suffix<YSurfXField>(); }
  template<> string coord_name<YSurfYField>(){ return "_YSurf"+suffix<YSurfYField>(); }
  template<> string coord_name<YSurfZField>(){ return "_ZSurf"+suffix<YSurfZField>(); }
  template<> string coord_name<  ZVolField>(){ return "_Vol"  +suffix<  ZVolField>(); }
  template<> string coord_name<ZSurfXField>(){ return "_XSurf"+suffix<ZSurfXField>(); }
  template<> string coord_name<ZSurfYField>(){ return "_YSurf"+suffix<ZSurfYField>(); }
  template<> string coord_name<ZSurfZField>(){ return "_ZSurf"+suffix<ZSurfZField>(); }

  //-----------------------------------------------------------------

  template<typename FieldT>
  void
  build_coord_expr( Expr::ExpressionFactory* const exprFactory,
                    const BundlePtr bundle,
                    GraphHelper* const gHelper )
  {
    Expr::TagList tags;
    tags.push_back( Expr::Tag( "X"+coord_name<FieldT>(), Expr::STATE_NONE) );
    tags.push_back( Expr::Tag( "Y"+coord_name<FieldT>(), Expr::STATE_NONE) );
    tags.push_back( Expr::Tag( "Z"+coord_name<FieldT>(), Expr::STATE_NONE) );
    const Expr::ExpressionID id = exprFactory->register_expression( new typename Coordinates<FieldT>::Builder(tags,bundle), false, bundle->id() );
    gHelper->rootIDs.insert( id );
  }
#define INSTANTIATE_COORD(T)          \
  template void build_coord_expr<T>( Expr::ExpressionFactory* const, const BundlePtr, GraphHelper* const );
#define INSTANTIATE_COORD_GROUP(VOLT)                                    \
    INSTANTIATE_COORD( VOLT )                                            \
    INSTANTIATE_COORD( SpatialOps::FaceTypes<VOLT>::XFace )  \
    INSTANTIATE_COORD( SpatialOps::FaceTypes<VOLT>::YFace )  \
    INSTANTIATE_COORD( SpatialOps::FaceTypes<VOLT>::ZFace )

  INSTANTIATE_COORD_GROUP(XVolField)
  INSTANTIATE_COORD_GROUP(YVolField)
  INSTANTIATE_COORD_GROUP(ZVolField)

  //-----------------------------------------------------------------

  template<typename FieldT>
  Expr::ExpressionBuilder*
  build_basic_expr( const YAML::Node& params, const BundlePtr bundle )
  {
    const Direction dir = direction<typename FieldT::Location::Bundle>();
    const Expr::Tag tag = parse_nametag( params["NameTag"], dir );
    Expr::ExpressionBuilder* builder = NULL;

    if( params["Constant"] ){
      typedef typename Expr::ConstantExpr<FieldT>::Builder Builder;
      builder = new Builder( tag, params["Constant"].as<double>() );
    }

    else if( params["LinearFunction"] ){
      const YAML::Node valParams = params["LinearFunction"];
      typedef typename Expr::LinearFunction<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["NameTag"], dir ),
                             valParams["slope"    ].as<double>(),
                             valParams["intercept"].as<double>() );
    }

    else if( params["SineFunction"] ) {
      const YAML::Node valParams = params["SineFunction"];
      typedef typename Expr::SinFunction<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["NameTag"], dir ),
                             valParams["amplitude"].as<double>(),
                             valParams["frequency"].as<double>(),
                             valParams["offset"   ].as<double>() );
    }

    else if( params["ParabolicFunction"] ) {
      const YAML::Node valParams = params["ParabolicFunction"];
      typedef typename Expr::ParabolicFunction<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["NameTag"], dir ),
                             valParams["a" ].as<double>(),
                             valParams["b" ].as<double>(),
                             valParams["c" ].as<double>(),
                             valParams["x0"].as<double>(0.0) );
    }

    else if( params["GaussianFunction"] ) {
      const YAML::Node valParams = params["GaussianFunction"];
      typedef typename Expr::GaussianFunction<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["IndependentVariable"], dir ),
                             valParams["amplitude"].as<double>(),
                             valParams["deviation"].as<double>(),
                             valParams["mean"     ].as<double>(),
                             valParams["baseline" ].as<double>() );
    }

    else if( params["2DGaussianFunction"] ) {
      const YAML::Node valParams = params["2DGaussianFunction"];
      typedef typename Expr::GaussianFunction2D<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["IndependentVariable1"], dir ),
                             parse_nametag( valParams["IndependentVariable2"], dir ),
                             valParams["amplitude" ].as<double>(),
                             valParams["deviation1"].as<double>(),
                             valParams["deviation2"].as<double>(),
                             valParams["mean1"     ].as<double>(),
                             valParams["mean2"     ].as<double>(),
                             valParams["baseline"  ].as<double>() );
    }

    else if( params["DoubleTanhFunction"] ) {
      const YAML::Node valParams = params["DoubleTanhFunction"];
      typedef typename Expr::DoubleTanhFunction<FieldT>::Builder Builder;
      builder = new Builder( tag,
                             parse_nametag( valParams["NameTag"], dir ),
                             valParams["midpointUp"  ].as<double>(),
                             valParams["midpointDown"].as<double>(),
                             valParams["width"       ].as<double>(),
                             valParams["amplitude"   ].as<double>() );
    }

    else if( params["TaylorGreenVel3D"] ){

      const Expr::Tag xCoord( "X"+coord_name<FieldT>(), Expr::STATE_NONE );
      const Expr::Tag yCoord( "Y"+coord_name<FieldT>(), Expr::STATE_NONE );
      const Expr::Tag zCoord( "Z"+coord_name<FieldT>(), Expr::STATE_NONE );
      const YAML::Node valParams = params["TaylorGreenVel3D"];
      double angle = valParams["angle"].as<double>();
      const string name = params["NameTag"]["name"].as<string>();

      typedef typename TaylorGreenVel3D<FieldT>::Builder Builder;

      if( name == "x_velocity" ) {
        angle += 2*PI/3.0;
        builder = new Builder( tag, xCoord, yCoord, zCoord, angle );
      }
      else if( name == "y_velocity" ) {
        angle -= 2*PI/3.0;
        builder = new Builder( tag, yCoord, xCoord, zCoord, angle );
      }
      else if ( name == "z_velocity" ) {
        builder = new Builder( tag, zCoord, xCoord, yCoord, angle );
      }
    }

    else if( params["FileName"] ){

      string fileName;
      double baseline;

      //Allows for different files and baselines per bundle
      //This is necessary for coarse cases
      switch( bundle->dir() ){

      case LBMS::XDIR: fileName = params["FileName"]["XBundle"].as<string>();
                       baseline = params["FileName"]["XBaseline"].as<double>();
      break;

      case LBMS::YDIR: fileName = params["FileName"]["YBundle"].as<string>();
                       baseline = params["FileName"]["YBaseline"].as<double>();
      break;

      case LBMS::ZDIR: fileName = params["FileName"]["ZBundle"].as<string>();
                       baseline = params["FileName"]["ZBaseline"].as<double>();
      break;

      case LBMS::NODIR: std::cout << "No bundle direction in BasicExprBuilder"
                                  << std::endl; break;

      }

      typedef typename ReadFileExpression<FieldT>::Builder Builder;
                       builder = new Builder( tag, fileName, bundle, baseline );
    }

    if( builder == NULL ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "ERROR: did not resolve a builder for " << params.begin()->first.as<string>() << std::endl;
      throw std::invalid_argument( msg.str() );
    }

    return builder;
  }

  //------------------------------------------------------------------

  void
  create_expressions_from_input( const YAML::Node& parser,
                                 const BundlePtr bundle,
                                 GraphCategories& gc )
  {
    //___________________________________
    // parse and build basic expressions
    const YAML::Node exprInfo( parser["BasicExpression"] );
    for( YAML::Node::const_iterator iparams = exprInfo.begin(); iparams != exprInfo.end(); ++iparams ){
      const YAML::Node& exprParams = *iparams;
      const string fieldType = exprParams["type"].as<string>();

      Expr::ExpressionBuilder* builder;

      switch( get_field_type(fieldType) ){
      case VOLUME:
        switch( bundle->dir() ){
        case XDIR: builder = build_basic_expr<XVolField>(exprParams, bundle); break;
        case YDIR: builder = build_basic_expr<YVolField>(exprParams, bundle); break;
        case ZDIR: builder = build_basic_expr<ZVolField>(exprParams, bundle); break;
        case NODIR: break;
        }
        break;
      case XFACE:
        switch( bundle->dir() ){
        case XDIR: builder = build_basic_expr<XSurfXField>(exprParams, bundle); break;
        case YDIR: builder = build_basic_expr<YSurfXField>(exprParams, bundle); break;
        case ZDIR: builder = build_basic_expr<ZSurfXField>(exprParams, bundle); break;
        case NODIR: break;
        }
        break;
      case YFACE:
        switch( bundle->dir() ){
        case XDIR: builder = build_basic_expr<XSurfYField>(exprParams, bundle); break;
        case YDIR: builder = build_basic_expr<YSurfYField>(exprParams, bundle); break;
        case ZDIR: builder = build_basic_expr<ZSurfYField>(exprParams, bundle); break;
        case NODIR: break;
        }
        break;
      case ZFACE:
        switch( bundle->dir() ){
        case XDIR: builder = build_basic_expr<XSurfZField>(exprParams, bundle); break;
        case YDIR: builder = build_basic_expr<YSurfZField>(exprParams, bundle); break;
        case ZDIR: builder = build_basic_expr<ZSurfZField>(exprParams, bundle); break;
        case NODIR: break;
        }
        break;
      }

      const string taskListName = exprParams["TaskList"].as<string>();

      if( taskListName ==  "initialization"  )  gc[INITIALIZATION  ]->exprFactory->register_expression( builder, false, bundle->id() );
      if( taskListName == "advance_solution" )  gc[ADVANCE_SOLUTION]->exprFactory->register_expression( builder, false, bundle->id() );

      if( taskListName != "advance_solution" && taskListName != "initialization" ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "ERROR: unsupported task list '" << taskListName << "'" << std::endl
            << "       encountered while parsing '"
            << exprParams["NameTag"]["name"].as<string>()
            << "'" << std::endl;
        throw std::invalid_argument( msg.str() );
      }
    } // basic expression loop

  }

  //-----------------------------------------------------------------

} // namespace LBMS
