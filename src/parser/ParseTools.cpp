/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ParseTools.h"
#include <yaml-cpp/yaml.h>

#include <lbms/Bundle.h>
#include <fields/Fields.h>
#include <fields/StringNames.h>
#ifdef HAVE_MPI
#include <pidx/IDXWriter.h>
#endif

#include <expression/Tag.h>

#include <boost/foreach.hpp>

#include <stdexcept>
#include <sstream>
using std::string;


namespace LBMS{

  //===================================================================

  Expr::Tag
  parse_nametag( const YAML::Node& param, const LBMS::Direction dir )
  {
    return create_tag( param["name"].as<string>(),
                       Expr::str2context( param["state"].as<string>() ),
                       dir );
  }

  //===================================================================

  Expr::TagList
  parse_taglist( const YAML::Node& param, const LBMS::Direction dir )
  {
    Expr::TagList tags;
    for( YAML::Node::const_iterator ipg=param.begin(); ipg!=param.end(); ++ipg ){
      tags.push_back( parse_nametag((*ipg)["NameTag"],dir) );
    }
    return tags;
  }

  //===================================================================

  typedef std::map<string,FieldType> StringMap;
  static StringMap validStrings;

  void set_string_map()
  {
    if( !validStrings.empty() ) return;

    validStrings["VOLUME"] = VOLUME;
    validStrings["XFACE" ] = XFACE;
    validStrings["YFACE" ] = YFACE;
    validStrings["ZFACE" ] = ZFACE;
  }

  FieldType
  get_field_type( std::string key )
  {
    set_string_map();
    std::transform( key.begin(), key.end(), key.begin(), ::toupper );

    StringMap::iterator i = validStrings.find( key );
    if( i == validStrings.end() ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid field type encountered ('" << key << "')" << std::endl
          << "  Valid field type names:" << std::endl;
      for( StringMap::const_iterator i=validStrings.begin(); i!=validStrings.end(); ++i ){
        msg << "    " << i->first << std::endl;
      }
      throw std::invalid_argument( msg.str() );
    }
    return i->second;
  }

  //===================================================================

# ifdef HAVE_MPI
  IDXWriters*
  parse_idx_vis_fields( const YAML::Node& parser, const BundlePtrVec& bundles )
  {
    IDXWriters* const writers = new IDXWriters();

    const YAML::Node& param = parser["IDX-Visualization"];

    BOOST_FOREACH( BundlePtr b, bundles ){

      const Direction dir = b->dir();
      const int fmlID = b->id();

      IDXWriter* const writer = new IDXWriter(b, parser["JobName"].as<string>() );

      for( YAML::Node::const_iterator ipg=param.begin(); ipg!=param.end(); ++ipg ){
        const YAML::Node field( (*ipg)["field"] );
        const FieldType fieldType = get_field_type( field["type"].as<string>() );
        const Expr::Tag tag = parse_nametag( field["NameTag"], dir );

        switch( fieldType ){
        case VOLUME:
          switch( dir ){
          case XDIR: writer->add_output_field<XVolField>( tag, fmlID, tag.name() ); break;
          case YDIR: writer->add_output_field<YVolField>( tag, fmlID, tag.name() ); break;
          case ZDIR: writer->add_output_field<ZVolField>( tag, fmlID, tag.name() ); break;
          case NODIR: break;
          }
          break;
        case XFACE:
          switch( dir ){
          case XDIR: writer->add_output_field<XSurfXField>( tag, fmlID, tag.name() ); break;
          case YDIR: writer->add_output_field<YSurfXField>( tag, fmlID, tag.name() ); break;
          case ZDIR: writer->add_output_field<ZSurfXField>( tag, fmlID, tag.name() ); break;
          case NODIR: break;
          }
          break;
        case YFACE:
          switch( dir ){
          case XDIR: writer->add_output_field<XSurfYField>( tag, fmlID, tag.name() ); break;
          case YDIR: writer->add_output_field<YSurfYField>( tag, fmlID, tag.name() ); break;
          case ZDIR: writer->add_output_field<ZSurfYField>( tag, fmlID, tag.name() ); break;
          case NODIR: break;
          }
          break;
        case ZFACE:
          switch( dir ){
          case XDIR: writer->add_output_field<XSurfZField>( tag, fmlID, tag.name() ); break;
          case YDIR: writer->add_output_field<YSurfZField>( tag, fmlID, tag.name() ); break;
          case ZDIR: writer->add_output_field<ZSurfZField>( tag, fmlID, tag.name() ); break;
          case NODIR: break;
          }
          break;
        } // switch( fieldType )

      } // loop over fields

      writers->add_writer( fmlID, writer );

    } // loop over bundles

    return writers;
  }
# endif

  //===================================================================

} // namespace LBMS
