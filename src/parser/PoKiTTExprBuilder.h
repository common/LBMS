/*
 * The MIT License
 *
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   PoKiTTExprBuilder.h
 *  \date   Jan 5, 2015
 *  \author "James C. Sutherland"
 *
 *  \brief creates expressions from PoKiTT
 *  \ingroup Parser
 */

#ifndef POKITTEXPRBUILDER_H_
#define POKITTEXPRBUILDER_H_

#include <lbms/Options.h>

namespace Expr{ class ExpressionFactory; }

namespace LBMS{

  void
  build_pokitt_expressions( const PoKiTTOptions& opts,
                            const BundlePtr bundle,
                            Expr::ExpressionFactory& factory );

} // namespace LBMS

#endif /* POKITTEXPRBUILDER_H_ */
