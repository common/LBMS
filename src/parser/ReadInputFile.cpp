/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "ReadInputFile.h"

#include <zlib.h>
#include <mpi/Environment.h>
#include <lbms/Direction.h>

std::string get_value( const gzFile & gzFp )
{
  std::string temp;
  while( true ) {
    char ch = gzgetc( gzFp );
    if( ch == '#' ) { // The input line is commented out.
      while( ch != '\n' ) { // Skip it.
        ch = gzgetc( gzFp );
      }
    }
    if( ch == -1 ) { // end of file reached
      break;
    }
    if( ch == '\n' || ch == '\t' || ch == '\r' || ch == ' ' ) { // done reading token
      if( temp.size() > 0 ) {
        break;
      }
      else {
        continue; // until we get a non-empty token.
      }
    }
    temp.push_back( ch );
  }
  return temp;
}

double get_double( gzFile & gzFp )
{
  double out;
  std::string result = get_value( gzFp );
  sscanf( result.c_str(), "%lf", &out );
  return out;
}


template<typename FieldT>
ReadFileExpression<FieldT>::
ReadFileExpression( const std::string fileName,
                    const LBMS::BundlePtr bundle,
                    const double baseline )
: Expr::Expression<FieldT>(),
  filename_( fileName ),
  bundle_  ( bundle   ),
  baseline_( baseline )
{}

//--------------------------------------------------------------------

template< typename FieldT >
void
ReadFileExpression<FieldT>::
evaluate()
{
  using namespace SpatialOps;
  using std::string;
  FieldT& phi = this->value();

  // gzFile utilities as they can handle both gzip and ascii files.
  gzFile inputFile = gzopen( filename_.c_str(), "r" );
  proc0cout << filename_.c_str() << std::endl << std::endl;

  if( inputFile == NULL ){
    std::cout << "ERROR: ReadInputFileExpresssion: \n Unable to open the given input file "
              << filename_ << std::endl;
    throw std::runtime_error("Problems opening the input file");
  }

  phi <<= baseline_;

  typename FieldT::iterator phiiter = phi.interior_begin();

  filesize_[0]=get_double(inputFile);
  filesize_[1]=get_double(inputFile);
  filesize_[2]=get_double(inputFile);

  const IntVec& extent = bundle_->glob_npts();  //extent of mesh
  const IntVec& range  = bundle_->npts();       //extent of processor
  const IntVec& filter = filesize_ / extent;    //ratio of the extent of the file to the extent of the mesh
  const IntVec& offset = (bundle_->global_mesh_offset()*filter)+IntVec(1,1,1); //offset of processor within file domain
  const IntVec& procEdge = filter * range + offset - IntVec(1,1,1);            //extent of processor within file domain

  if( filesize_[0] % extent[0] != 0 || filesize_[1] % extent[1] != 0 || filesize_[2] % extent[2] != 0 ) {
        std::cout << "ERROR: Coarse and fine mesh must be divisible by "
                  << filesize_ << std::endl;
        throw std::runtime_error("Problems with mesh size in ReadInputFile");
  }

  double val;

  for( int k = 1; k <= filesize_[2]; ++k  ){
    for( int j = 1; j <= filesize_[1]; ++j  ){
      for( int i = 1; i <= filesize_[0]; ++i  ){
        val = get_double(inputFile);
        //if on processor
        if( i>=offset[0] && i<=procEdge[0] && j>=offset[1] && j<=procEdge[1] && k>=offset[2] && k<=procEdge[2] ){
          *phiiter = val + baseline_;
          ++phiiter;
        }
      }
    }
  }
  //Check to ensure both the file and the field are at each end
//  const bool endOfField( phiiter == phi.interior_end() );
//  assert( gzeof(inputFile) == endOfField );
  gzclose( inputFile );
}

//--------------------------------------------------------------------

template< typename FieldT >
ReadFileExpression<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const std::string fileName,
         const LBMS::BundlePtr bundle,
         const double baseline )
: ExpressionBuilder(result),
  filename_( fileName ),
  bundle_  ( bundle   ),
  baseline_( baseline )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
ReadFileExpression<FieldT>::Builder::
build() const
{
  return new ReadFileExpression<FieldT>( filename_, bundle_, baseline_ );
}

#include <fields/Fields.h>
#define DECLARE( VOLT )                                                    \
  template class ReadFileExpression< VOLT >;                               \
  template class ReadFileExpression< SpatialOps::FaceTypes<VOLT>::XFace >; \
  template class ReadFileExpression< SpatialOps::FaceTypes<VOLT>::YFace >; \
  template class ReadFileExpression< SpatialOps::FaceTypes<VOLT>::ZFace >; 

DECLARE( LBMS::XVolField )
DECLARE( LBMS::YVolField )
DECLARE( LBMS::ZVolField )

