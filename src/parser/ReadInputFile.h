/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef Read_Input_File
#define Read_Input_File

#include <expression/Expression.h>
#include <spatialops/structured/IntVec.h>
#include <lbms/Bundle.h>

/**
 *  \class ReadFileExpression
 *  \author Derek Cline
 *  \date Nov, 2013
 *  \brief Implements an expression to read velocity field files
 *
 *  Note: The input file should be in the format of three integers which represent the
 *        number of coarse points in each direction, x, y and z followed by floating
 *        point numbers representing the values.  Each can be separated by either a line
 *        break or a space Thus, "int int int\n float\n float\n.... etc" will work.
 *
 *        The mesh size and each value should be indexed following Matlab's convention,
 *        that is, first we iterate in the x direction, then the y followed finally by the
 *        z direction.
 */

template< typename FieldT >
class ReadFileExpression : public Expr::Expression<FieldT>
{
public:

  struct Builder : public Expr::ExpressionBuilder
  {
    Builder(const Expr::Tag& result,
            const std::string fileName,
            const LBMS::BundlePtr bundle,
            const double baseline );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  private:
    const std::string filename_;
    const LBMS::BundlePtr bundle_;
    const double baseline_;
  };

  void evaluate();

private:
  ReadFileExpression( const std::string fileName,
                      const LBMS::BundlePtr bundle,
                      const double baseline );

  const std::string filename_;
  SpatialOps::IntVec filesize_;
  const LBMS::BundlePtr bundle_;
  const double baseline_;
};

//--------------------------------------------------------------------

#endif //Read_Input_File
