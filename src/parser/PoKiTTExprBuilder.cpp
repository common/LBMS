/*
 * The MIT License
 *
 * Copyright (c) 2015 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   PoKiTTExprBuilder.cpp
 *  \date   Jan 5, 2015
 *  \author "James C. Sutherland"
 */

#include "PoKiTTExprBuilder.h"

#include <parser/ParseTools.h>
#include <fields/StringNames.h>
#include <mpi/FieldBundleExchange.h>

#include <pokitt/transport/DiffusionCoeffMix.h>
#include <pokitt/MixtureMolWeight.h>
#include <pokitt/thermo/Enthalpy.h>


namespace LBMS{

//==============================================================================

template< typename FieldT >
void
setup_pokitt_expressions( const PoKiTTOptions& opts,
                          const BundlePtr bundle,
                          Expr::ExpressionFactory& factory )
{
  typedef typename pokitt::MixtureMolWeight<FieldT>::Builder    MixMW;
  typedef typename pokitt::SpeciesEnthalpy <FieldT>::Builder    SpEnthalpy;

  const YAML::Node& pg = opts.pokittParser;
  const Direction dir = direction<typename FieldT::Location::Bundle>();

  const StringNames& sName = StringNames::self();

  const Expr::TagList massFracs = opts.species_tags( dir, Expr::STATE_NONE );

  const Expr::Tag temperature = create_tag( opts.temperature, Expr::STATE_NONE, dir );
  const Expr::Tag pressure    = create_tag( opts.pressure,    Expr::STATE_NONE, dir );
  const Expr::Tag mw          = create_tag( sName.mixmw,      Expr::STATE_NONE, dir );

  factory.register_expression( new MixMW( mw, massFracs, pokitt::MASS ), false, bundle->id() );

  const Expr::TagList specEnthTags = opts.species_tags( dir, Expr::STATE_NONE, sName.enthalpy );
  for( size_t i=0; i<specEnthTags.size(); ++i ){
    factory.register_expression( new SpEnthalpy( specEnthTags[i], temperature, i ), false, bundle->id() );
  }

}

//==============================================================================

void build_pokitt_expressions( const PoKiTTOptions& opts,
                               const BundlePtr bundle,
                               Expr::ExpressionFactory& factory )
{
  switch( bundle->dir() ){
    case XDIR: setup_pokitt_expressions<XVolField>( opts, bundle, factory ); break;
    case YDIR: setup_pokitt_expressions<YVolField>( opts, bundle, factory ); break;
    case ZDIR: setup_pokitt_expressions<ZVolField>( opts, bundle, factory ); break;
    case NODIR: assert(false); // shouldn't get here.
  }
}

//==============================================================================

} // namespace LBMS
