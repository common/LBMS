#include <fields/Checkpoint.h>
#include <fields/Fields.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <test/TestHelper.h>
#include <mpi/Environment.h>

#include <expression/ExprLib.h>

#include <limits>
#include <iostream>
using std::cout;
using std::endl;

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
namespace po=boost::program_options;

#include <spatialops/Nebo.h>
#include <spatialops/structured/SpatialFieldStore.h>
using SpatialOps::operator<<=;
using SpatialOps::IntVec;

using namespace LBMS;

const Expr::Tag xtag("x",Expr::STATE_NONE), ytag("y",Expr::STATE_NONE), ztag("z",Expr::STATE_NONE);

//-------------------------------------------------------------------

template<typename FieldT>
void setup_fields( Expr::FieldManagerList& fml,
                   const bool initializeFields )
{
  const int nghost = 1;
  const BundlePtr bundle = Environment::bundle( direction<typename FieldT::Location::Bundle>() );
  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  fm.register_field( xtag, nghost );
  fm.register_field( ytag, nghost );
  fm.register_field( ztag, nghost );

  fm.allocate_fields( boost::cref( bundle->get_field_info() ) );

  if( initializeFields ){
    FieldT& x = fm.field_ref(xtag);
    FieldT& y = fm.field_ref(ytag);
    FieldT& z = fm.field_ref(ztag);
    set_coord_values(x,y,z,bundle);
  }
  else{
    fm.field_ref(xtag) <<= 0.0;
    fm.field_ref(ytag) <<= 0.0;
    fm.field_ref(ztag) <<= 0.0;
  }
}

//-------------------------------------------------------------------

template<typename FieldT>
bool check_fields( Expr::FieldManagerList& fml )
{
  const BundlePtr bundle = Environment::bundle( direction<typename FieldT::Location::Bundle>() );
  const typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  const FieldT& x = fm.field_ref(xtag);
  const FieldT& y = fm.field_ref(ytag);
  const FieldT& z = fm.field_ref(ztag);

  SpatialOps::SpatFldPtr<FieldT> xx = SpatialOps::SpatialFieldStore::get<FieldT>(x);
  SpatialOps::SpatFldPtr<FieldT> yy = SpatialOps::SpatialFieldStore::get<FieldT>(x);
  SpatialOps::SpatFldPtr<FieldT> zz = SpatialOps::SpatialFieldStore::get<FieldT>(x);

  set_coord_values(*xx,*yy,*zz,bundle);

  bool isOkay = true;

  int i=0;
  for( typename FieldT::const_iterator ix=x.interior_begin(), ixx=xx->interior_begin();
      ix!=x.interior_end();
      ++ix, ++ixx, ++i )
  {
    if( std::abs( *ix-*ixx ) > std::numeric_limits<double>::epsilon()*20 ){
      cout << i << " found: " << *ix << "\t expected: " << *ixx << endl;
      isOkay = false;
    }
  }
  i=0;
  for( typename FieldT::const_iterator ix=y.interior_begin(), ixx=yy->interior_begin();
      ix!=y.interior_end();
      ++ix, ++ixx, ++i )
  {
    if( std::abs( *ix-*ixx ) > std::numeric_limits<double>::epsilon()*20 ){
      cout << i << " found: " << *ix << "\t expected: " << *ixx << endl;
      isOkay = false;
    }
  }
  i=0;
  for( typename FieldT::const_iterator ix=z.interior_begin(), ixx=zz->interior_begin();
      ix!=z.interior_end();
      ++ix, ++ixx, ++i )
  {
    if( std::abs( *ix-*ixx ) > std::numeric_limits<double>::epsilon()*20 ){
      cout << i << " found: " << *ix << "\t expected: " << *ixx << endl;
      isOkay = false;
    }
  }
  return isOkay;
}

//-------------------------------------------------------------------

enum Mode{ WRITE, READ };

bool test_restart( const std::string dirPost,
                   const Mode mode )
{
  TestHelper status(true);

  const BundlePtr xbundle = Environment::bundle(XDIR);
  const BundlePtr ybundle = Environment::bundle(YDIR);
  const BundlePtr zbundle = Environment::bundle(ZDIR);

  cout << xbundle << endl << ybundle << endl << zbundle << endl;

  const bool doX = ( xbundle->npts(XDIR) > 1);
  const bool doY = ( ybundle->npts(YDIR) > 1);
  const bool doZ = ( zbundle->npts(ZDIR) > 1);

  BundlePtrVec bundles;
  if( doX ) bundles.push_back( xbundle );
  if( doY ) bundles.push_back( ybundle );
  if( doZ ) bundles.push_back( zbundle );

  Expr::FMLMap fmls;
  BOOST_FOREACH( BundlePtr b, bundles ){
    fmls[b->id()] = b->get_field_manager_list();
  }

  const bool writing = (mode == WRITE);

  if( doX ) setup_fields<XVolField>( *fmls[xbundle->id()], writing );
  if( doY ) setup_fields<YVolField>( *fmls[ybundle->id()], writing );
  if( doZ ) setup_fields<ZVolField>( *fmls[zbundle->id()], writing );
  const double testtime = 3.14159;
  Expr::TagSet tags; tags.insert(xtag); tags.insert(ytag); tags.insert(ztag);
  if( writing ){
    cout << "writing 'checkpoint" << dirPost << "'" << std::endl;
    Checkpoint checkpoint( bundles, tags, fmls, "./" );
    checkpoint.write_checkpoint("checkpoint"+dirPost, testtime);
  }
  else{
    cout << "reading 'checkpoint" << dirPost << "'" << std::endl;
    Checkpoint checkpoint( bundles, tags, fmls, "./" );
    double timecheck = checkpoint.read_checkpoint("checkpoint"+dirPost);
    status((timecheck == testtime), "Time");
    if( doX ) status( check_fields<XVolField>( *fmls[xbundle->id()] ), "X-bundle" );
    if( doY ) status( check_fields<YVolField>( *fmls[ybundle->id()] ), "Y-bundle" );
    if( doZ ) status( check_fields<ZVolField>( *fmls[zbundle->id()] ), "Z-bundle" );
  }

  return status.ok();
}

//-------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  IntVec nCoarse, nFine;
  Coordinate length;
  std::string name;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(12), "Fine grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(10), "Fine grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(10), "Fine grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(3),"Coarse grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(2),"Coarse grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(2),"Coarse grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "name", po::value<std::string>(&name)->default_value(""),"checkpoint dir tag")
      ( "perix", "periodic in x" )
      ( "periy", "periodic in y" )
      ( "periz", "periodic in z" );
      ;

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }

  try{
    Environment::setup(iarg,carg);
    const LBMS::MeshPtr mesh( new LBMS::Mesh( nCoarse, nFine, length) );
    TestHelper status;
    {
      Environment::set_topology(mesh);
      test_restart(name,WRITE);
    }
    {
      Environment::set_topology(mesh);
      status( test_restart(name,READ), "restart" );
    }

    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;
}
