#ifndef UT_StringNames_h
#define UT_StringNames_h

#include <string>

#include <expression/Tag.h>
#include <lbms/Direction.h>


/**
 *  \fn Expr::Tag create_tag(std::string, const Expr::Context, const LBMS::Direction);
 *
 *  \brief Create an Expr::Tag from the name, appending the bundle direction information to the name.
 *
 *  \param name The name of the field (will be appended by bundle direction)
 *  \param state the Expr::Context
 *  \param dir the Bundle direction.  If NODIR, then no direction information is appended.
 *  \return the Expr::Tag.
 *
 *  \ingroup Tools
 */
Expr::Tag create_tag( std::string name,
                      const Expr::Context state,
                      const LBMS::Direction dir );


/**
 *  \class  StringNames
 *  \author James C. Sutherland
 *
 *  \brief Holds names for variables.
 *
 *  \ingroup Tools
 */
class StringNames
{
public:
  static const StringNames& self();

  const std::string x, y, z;
  const std::string dx, dy, dz;
  const std::string coarse;
  const std::string fine;
  const std::string reconstructed;

  // for velocity gradients interpolated to alternate faces for use in stress expressions
  const std::string xface, yface, zface;

  const std::string xvel, yvel, zvel;
  const std::string advect;
  const std::string xmom, ymom, zmom;
  const std::string xcons, ycons, zcons;
  const std::string density;
  const std::string pressure, dpdx, dpdy, dpdz;
  const std::string temperature;
  const std::string enthalpy, rhoH;
  const std::string e0, rhoE0;
  const std::string ke;
  const std::string mixture_fraction, rhoF;
  const std::string species, rhoY, species_flux;
  const std::string tauxx, tauxy, tauxz;
  const std::string tauyx, tauyy, tauyz;
  const std::string tauzx, tauzy, tauzz;
  const std::string dilatation;
  const std::string heat_flux;
  const std::string massflux;
  const std::string filtered;
  const std::string boxFiltered;

  const std::string mixmw;

  // properties
  const std::string thermal_conductivity;
  const std::string cv, cp, c;
  const std::string viscosity;

  // scratch & work variables
  const std::string scratch1, scratch2, scratch3, scratch4;


private:
  StringNames();  // implemented as a singleton
  ~StringNames();
  StringNames(const StringNames&);            // no copying
  StringNames& operator=(const StringNames&); // no assignment
};

#endif
