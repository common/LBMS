#ifndef Checkfields_h
#define Checkfields_h
/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file  Checkfields.h
 *  \brief Functions for checking the interior of a field to tell if it is has bad values.
 *
 *  \date   2014
 *  \author John Hutchins
 */
#include <spatialops/structured/FieldHelper.h>
#include <fields/Fields.h>
#include <expression/Expression.h>

/**
 * @brief Checks the field interior for a given fail value, if the field contains greater (in absolute value) to the fail value then throw an error
 */
template<typename FieldT >
void check_field_interior(const FieldT& field, const std::string msg, const Expr::Tag& tag = Expr::Tag(), double limit = 19999999)
{
     bool output = false;
      const typename FieldT::const_iterator iend4 = field.interior_end();
      for( typename FieldT::const_iterator i=field.interior_begin(); i!=iend4; ++i){
        if(std::abs(*i) > limit){ output=true;}
      }
      if(output) std::cout<<msg<<tag<<std::endl;
      if(output) SpatialOps::interior_print_field(field, std::cout, true);
      if(output) throw std::runtime_error(" Field has bad interior, fix your code");
}


template<typename FieldT >
void check_field_interior(const FieldT* field, const std::string msg, const Expr::Tag& tag = Expr::Tag(), double limit = 19999999)
{
     bool output = false;
      const typename FieldT::const_iterator iend4 = field->interior_end();
      for( typename FieldT::const_iterator i=field->interior_begin(); i!=iend4; ++i){
        if(std::abs(*i) > limit){ output=true;}
      }
      if(output) std::cout<<msg<<tag<<std::endl;
      if(output) SpatialOps::interior_print_field(*field, std::cout, true);
      if(output) throw std::runtime_error(" Field has bad interior, fix your code");
}

/**
 * @brief Checks the field interior for a given fail value, return true if greater than the fail value is present in the field.
 */
template<typename FieldT>
bool field_interior_bad(const FieldT* field,  double limit = 19999999)
{
     const typename FieldT::const_iterator iend4 = field->interior_end();
     for( typename FieldT::const_iterator i=field->interior_begin(); i!=iend4; ++i){
       if(std::abs(*i) > limit){ return true;}
     }
    return false;
}

template<typename FieldT>
bool field_interior_bad(const FieldT& field,  double limit = 19999999)
{
     const typename FieldT::const_iterator iend4 = field.interior_end();
     for( typename FieldT::const_iterator i=field.interior_begin(); i!=iend4; ++i){
       if(std::abs(*i) > limit){ return true;}
     }
    return false;
}

/**
 * @brief Checks if a field contains a particular value, returns true if it does, within some value of epsilon set by precision.
 *  \param field   The field in question
 *  \param value   The value to searched for
 *  \param precision epsilon is created by 10^-precision
 *  \return  Whether the given value within epislon is contained in the field.
 */
template<typename FieldT>
bool field_interior_contains_value(const FieldT& field, double value, double precision=14)
{
    const typename FieldT::const_iterator iend4 = field.interior_end();
    for( typename FieldT::const_iterator i=field.interior_begin(); i!=iend4; ++i){
      if(*i >= value - std::pow(10, -precision) && *i<= value + std::pow(10, -precision)){ return true;}
    }
   return false;
}


#endif //checkfields
