/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   Checkpoint.h
 *  \date   May 30, 2012
 *  \author James C. Sutherland
 */

#ifndef CHECKPOINT_H_
#define CHECKPOINT_H_

#include <string>

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem/path.hpp>
#include <boost/property_tree/ptree_fwd.hpp>

#include <lbms/Bundle_fwd.h>
#include <lbms/Coordinate.h>

#include <expression/ExprFwd.h>
#include <expression/Tag.h>
#include <spatialops/structured/IntVec.h>

namespace Expr{
  class FieldManagerList;
}

namespace LBMS {

  /**
   *  \class Checkpoint
   *  \author James C. Sutherland
   *  \date   May 30, 2012
   *
   *  \brief Provides checkpoint IO for restart
   *
   *  Checkpoints are created with individual lines output as separate files.
   *  By using this level of granularity (lines as opposed to Bundles), restart
   *  can occur on a different processor topology without trouble.
   */
  class Checkpoint
  {
    const BundlePtrVec& bundles_;
    const Expr::TagSet fields_;
    Expr::FMLMap& fmls_;
    const boost::filesystem::path basePath_;

  public:

    struct BundleMetaData{
      std::string dir;
      Coordinate length;
      SpatialOps::IntVec npts, ncoarse, offset;
      BundleMetaData(){}
      BundleMetaData( const BundlePtr b );
      void read_ptree( const boost::property_tree::ptree& node );
      void write_ptree( boost::property_tree::ptree& node ) const;
      bool operator==(const BundleMetaData&) const;
      bool operator!=(const BundleMetaData&) const;
    };

    struct FieldMetaData{
      std::string fieldTypeName;
      Expr::Tag fieldTag;
      SpatialOps::IntVec npts;
      void read_ptree( const boost::property_tree::ptree& node );
      void write_ptree( boost::property_tree::ptree& node ) const;
      bool operator==(const FieldMetaData&) const;
      bool operator!=(const FieldMetaData&) const;
    };

    /**
     * \brief Create a Checkpoint
     */
    Checkpoint( const BundlePtrVec&,            ///< The set of bundles to include in the checkpoint
                const Expr::TagSet&,           ///< the tags for fields to include in the Checkpoint
                Expr::FMLMap& fmls,             ///< the FieldManagerList holding the fields for IO in the Checkpoint.
                const boost::filesystem::path="./"  ///< the path to locate the checkpoint file(s) at
              );

    ~Checkpoint();

    /**
     * \brief writes a checkpoint with the given label to disk, places the time into the metadata xml file.
     *
     * @param[in] cplabel the string which contains the complete folder name for this checkpoint
     * @param[in] simTime the simulation time for this checkpoint
     *
     */
    void write_checkpoint( const std::string& cplabel,  double simTime = 0.0 ) const;

    /**
     * \brief loads a checkpoint with the given label from disk, returns the simulation time.
     *
     * @param[in] label the string containing the checkpoint to be read in
     *
     * @return returns the simulation time that the checkpoint was written at, as a double.
     */
    double read_checkpoint( const std::string& label ) const;

  };

} /* namespace LBMS */
#endif /* CHECKPOINT_H_ */
