#include "StringNames.h"

using std::string;


Expr::Tag
create_tag( std::string name, const Expr::Context state, const LBMS::Direction dir )
{
  if( dir != LBMS::NODIR ) name += get_bundle_name_suffix(dir);
  return Expr::Tag( name, state );
}


const StringNames&
StringNames::self()
{
  static StringNames s;
  return s;
}

//--------------------------------------------------------------------

StringNames::StringNames()
  : x("X"), y("Y"), z("Z"),

    dx("_dx"), dy("_dy"), dz("_dz"),

    coarse       ("_coarse"       ),
    fine         ("_fine"         ),
    reconstructed("_reconstructed"),

    xface("_xface"), yface("_yface"), zface("_zface"),

    xvel( "x_velocity"), yvel("y_velocity"), zvel("z_velocity"),

    advect( "_advect"),

    xmom("x_momentum"), ymom("y_momentum"), zmom("z_momentum"),

    xcons("_xConservation"), ycons("_yConservation"), zcons("_zConservation"),

    density("density"),
    pressure("pressure"), dpdx("dpdx"), dpdy("dpdy"), dpdz("dpdz"),
    temperature("temperature"),
    enthalpy("enthalpy"),  rhoH("rhoH"),
    e0("e0"), rhoE0("rhoE0"),
    ke("ke"),

    mixture_fraction("mixture_fraction"),  rhoF("rhoF"),

    species("species"), rhoY("rhoY"), species_flux("species_flux"),

    tauxx("tauxx"), tauxy("tauxy"), tauxz("tauxz"),
    tauyx("tauyx"), tauyy("tauyy"), tauyz("tauyz"),
    tauzx("tauzx"), tauzy("tauzy"), tauzz("tauzz"),

    dilatation("dilatation"),

    heat_flux("heatflux"),

    massflux( "massflux"),

    filtered   (   "_filtered"),
    boxFiltered("_boxFiltered"),

    mixmw("mixmw"),

    thermal_conductivity("thermal_conductivity"),
    cp("cp"),
    cv("cv"),
    c("c"),
    viscosity("viscosity"),

    scratch1("scratch1"), scratch2("scratch2"), scratch3("scratch3"), scratch4("scratch4")
{
}

//--------------------------------------------------------------------

StringNames::~StringNames()
{
}

//--------------------------------------------------------------------
