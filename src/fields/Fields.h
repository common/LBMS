/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file  Fields.h
 *  \brief Defines fields for use in LBMS.
 *
 *  \date   2012
 *  \author James C. Sutherland
 */
#ifndef LBMSFields_h
#define LBMSFields_h

#include <spatialops/SpatialOpsConfigure.h>
#include <spatialops/SpatialOpsDefs.h>
#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/structured/SpatialField.h>
#include <spatialops/structured/IndexTriplet.h>

#include <lbms/Bundle_fwd.h>
#include <lbms/Direction.h>
#include <set>
namespace LBMS{

  typedef SpatialOps::XDIR  XBundle;  ///< type for the XBundle
  typedef SpatialOps::YDIR  YBundle;  ///< type for the YBundle
  typedef SpatialOps::ZDIR  ZBundle;  ///< type for the ZBundle

  // FaceDir: The direction relative to its volume field that this field is staggered.
  // StagLoc: The direction relative to the scalar volume field that this field's volume field is staggered.

  /** \addtogroup XBundle
   *  @{
   */
  /** \struct VolX
   *  \brief type traits for a cell   field on an X bundle
   */
  /** \struct XSurfX
   *   \brief type traits for a x-face field on an X bundle
   */
  /** \struct XSurfY
   *   \brief type traits for a y-face field on an X bundle
   */
  /** \struct XSurfZ
   *   \brief type traits for a z-face field on an X bundle
   */
  /**@}*/
  struct XVol  { typedef SpatialOps::NODIR FaceDir;  typedef SpatialOps::IndexTriplet< 0, 0, 0> Offset; typedef SpatialOps::IndexTriplet<0,0,0> BCExtra;  typedef XBundle Bundle; };
  struct XSurfX{ typedef SpatialOps::XDIR  FaceDir;  typedef SpatialOps::IndexTriplet<-1, 0, 0> Offset; typedef SpatialOps::IndexTriplet<1,0,0> BCExtra;  typedef XBundle Bundle; };
  struct XSurfY{ typedef SpatialOps::YDIR  FaceDir;  typedef SpatialOps::IndexTriplet< 0,-1, 0> Offset; typedef SpatialOps::IndexTriplet<0,1,0> BCExtra;  typedef XBundle Bundle; };
  struct XSurfZ{ typedef SpatialOps::ZDIR  FaceDir;  typedef SpatialOps::IndexTriplet< 0, 0,-1> Offset; typedef SpatialOps::IndexTriplet<0,0,1> BCExtra;  typedef XBundle Bundle; };


  /** \addtogroup YBundle
   *  @{*/
  /** \struct YVol
   *  \brief type traits for a cell   field on an Y bundle
   */
  /** \struct YSurfX
   *  \brief type traits for a x-face field on an Y bundle
   */
  /** \struct YSurfY
   *  \brief type traits for a y-face field on an Y bundle
   */
  /** \struct YSurfZ
   *  \brief type traits for a z-face field on an Y bundle
   */
  /**@}*/
  struct YVol  { typedef XVol  ::FaceDir FaceDir;  typedef XVol  ::Offset Offset;  typedef XVol  ::BCExtra  BCExtra;  typedef YBundle Bundle; };
  struct YSurfX{ typedef XSurfX::FaceDir FaceDir;  typedef XSurfX::Offset Offset;  typedef XSurfX::BCExtra  BCExtra;  typedef YBundle Bundle; };
  struct YSurfY{ typedef XSurfY::FaceDir FaceDir;  typedef XSurfY::Offset Offset;  typedef XSurfY::BCExtra  BCExtra;  typedef YBundle Bundle; };
  struct YSurfZ{ typedef XSurfZ::FaceDir FaceDir;  typedef XSurfZ::Offset Offset;  typedef XSurfZ::BCExtra  BCExtra;  typedef YBundle Bundle; };


  /** \addtogroup ZBundle
   *  @{*/
  /** \struct ZVol
   *  \brief type traits for a cell   field on an Z bundle
   */
  /** \struct ZSurfX
   *  \brief type traits for a x-face field on an Z bundle
   */
  /** \struct YSurfZ
   *  \brief type traits for a y-face field on an Z bundle
   */
  /** \struct ZSurfZ
   *  \brief type traits for a z-face field on an Z bundle
   */
  /**@}*/
  struct ZVol  { typedef XVol  ::FaceDir FaceDir;  typedef XVol  ::Offset Offset;  typedef XVol  ::BCExtra BCExtra;  typedef ZBundle Bundle; };
  struct ZSurfX{ typedef XSurfX::FaceDir FaceDir;  typedef XSurfX::Offset Offset;  typedef XSurfX::BCExtra BCExtra;  typedef ZBundle Bundle; };
  struct ZSurfY{ typedef XSurfY::FaceDir FaceDir;  typedef XSurfY::Offset Offset;  typedef XSurfY::BCExtra BCExtra;  typedef ZBundle Bundle; };
  struct ZSurfZ{ typedef XSurfZ::FaceDir FaceDir;  typedef XSurfZ::Offset Offset;  typedef XSurfZ::BCExtra BCExtra;  typedef ZBundle Bundle; };

  /** \addtogroup XBundle
   * @{
   */
  typedef SpatialOps::SpatialField<   XVol, double > XVolField;    ///< cell field on X-bundle
  typedef SpatialOps::SpatialField< XSurfX, double > XSurfXField;  ///< X-surface field on X-bundle
  typedef SpatialOps::SpatialField< XSurfY, double > XSurfYField;  ///< Y-surface field on X-bundle
  typedef SpatialOps::SpatialField< XSurfZ, double > XSurfZField;  ///< Z-surface field on X-bundle
  /**@}*/

  /** \addtogroup YBundle
   * @{
   */
  typedef SpatialOps::SpatialField<   YVol, double > YVolField;    ///< cell field on Y-bundle
  typedef SpatialOps::SpatialField< YSurfX, double > YSurfXField;  ///< X-surface field on Y-bundle
  typedef SpatialOps::SpatialField< YSurfY, double > YSurfYField;  ///< Y-surface field on Y-bundle
  typedef SpatialOps::SpatialField< YSurfZ, double > YSurfZField;  ///< Z-surface field on Y-bundle
  /**@}*/

  /** \addtogroup ZBundle
   * @{*/
  typedef SpatialOps::SpatialField<   ZVol, double > ZVolField;    ///< cell field on Z-bundle
  typedef SpatialOps::SpatialField< ZSurfX, double > ZSurfXField;  ///< X-surface field on Z-bundle
  typedef SpatialOps::SpatialField< ZSurfY, double > ZSurfYField;  ///< Y-surface field on Z-bundle
  typedef SpatialOps::SpatialField< ZSurfZ, double > ZSurfZField;  ///< Z-surface field on Z-bundle
  /**@}*/

} // namespace LBMS

namespace SpatialOps{

  /** \ingroup Tools
   *  \struct FaceTypes
   *  \brief Given the cell centered field type, this returns a struct
   *         with the corresponding face types.
   *
   *  <ul>
   *  <li> \b XFaceT The field type for the native direction on this bundle.
   *  <li> \b YFaceT The field type for the first non-native direction on this bundle.
   *  <li> \b ZFaceT The field type for the second non-native direction on this bundle.
   *  </ul>
   *
   *  Example:
   *  \code{.cpp}
   *   typdef FaceTypes<VolT>::XFace MyXFaceT;
   *   typdef FaceTypes<VolT>::YFace MyYFaceT;
   *   typdef FaceTypes<VolT>::ZFace MyZFaceT;
   *  \endcode
   */
  template< typename VolT >
  struct FaceTypes;

  template<>
  struct FaceTypes<LBMS::XVolField>
  {
    typedef LBMS::XSurfXField  XFace;
    typedef LBMS::XSurfYField  YFace;
    typedef LBMS::XSurfZField  ZFace;
  };
  template<>
  struct FaceTypes<LBMS::YVolField>
  {
    typedef LBMS::YSurfXField  XFace;
    typedef LBMS::YSurfYField  YFace;
    typedef LBMS::YSurfZField  ZFace;
  };
  template<>
  struct FaceTypes<LBMS::ZVolField>
  {
    typedef LBMS::ZSurfXField  XFace;
    typedef LBMS::ZSurfYField  YFace;
    typedef LBMS::ZSurfZField  ZFace;
  };

  /** \ingroup Tools
   *  \struct VolType
   *  \brief Given the face type, infer the corresponding volume field type
   *
   *  Example:
   *  \code{.cpp}
   *  typedef VolType<FaceT>::VolField MyVolT;
   *  \endcode
   *
   *  Note that these are specializations of the VolType struct declared in SpatialOps.
   */
  template<> struct VolType<LBMS::XSurfXField>{ typedef LBMS::XVolField VolField; };
  template<> struct VolType<LBMS::XSurfYField>{ typedef LBMS::XVolField VolField; };
  template<> struct VolType<LBMS::XSurfZField>{ typedef LBMS::XVolField VolField; };
  template<> struct VolType<LBMS::YSurfXField>{ typedef LBMS::YVolField VolField; };
  template<> struct VolType<LBMS::YSurfYField>{ typedef LBMS::YVolField VolField; };
  template<> struct VolType<LBMS::YSurfZField>{ typedef LBMS::YVolField VolField; };
  template<> struct VolType<LBMS::ZSurfXField>{ typedef LBMS::ZVolField VolField; };
  template<> struct VolType<LBMS::ZSurfYField>{ typedef LBMS::ZVolField VolField; };
  template<> struct VolType<LBMS::ZSurfZField>{ typedef LBMS::ZVolField VolField; };

} // SpatialOps

namespace LBMS{

  /**
   * \ingroup Tools
   * \struct NativeFieldSelector
   * \brief Given the bundle direction, this defines the types of the
   *        associated field types.
   *
   * The following typedefs are supplied by a specialization of this struct:
   *  - \c VolT the cell (volume) field type
   *  - \c Face1T the type of field in the "1" direction
   *  - \c Face2T the type of field in the "2" direction
   *  - \c Face3T the type of field in the "3" direction
   *
   * Example:
   * \code{.cpp}
   * typedef NativeFieldSelector<SpatialOps::XDIR>::XFaceT;  // xface field type on a X-bundle
   * typedef NativeFieldSelector<SpatialOps::XDIR>::YFaceT;  // yface field type on a X-bundle
   * typedef NativeFieldSelector<SpatialOps::XDIR>::ZFaceT;  // zface field type on a X-bundle
   *
   * typedef NativeFieldSelector<SpatialOps::YDIR>::XFaceT;  // xface field type on a Y-bundle
   * typedef NativeFieldSelector<SpatialOps::YDIR>::YFaceT;  // yface field type on a Y-bundle
   * typedef NativeFieldSelector<SpatialOps::YDIR>::ZFaceT;  // zface field type on a Y-bundle
   *
   * typedef NativeFieldSelector<SpatialOps::ZDIR>::XFaceT;  // xface field type on a Z-bundle
   * typedef NativeFieldSelector<SpatialOps::ZDIR>::YFaceT;  // yface field type on a Z-bundle
   * typedef NativeFieldSelector<SpatialOps::ZDIR>::ZFaceT;  // zface field type on a Z-bundle
   * \endcode
   */
  template<typename DirT> struct NativeFieldSelector;

  template<> struct NativeFieldSelector<SpatialOps::XDIR>
  {
    typedef XVolField   VolT;
    typedef SpatialOps::FaceTypes<VolT>::XFace XFaceT;
    typedef SpatialOps::FaceTypes<VolT>::YFace YFaceT;
    typedef SpatialOps::FaceTypes<VolT>::ZFace ZFaceT;
  };
  template<> struct NativeFieldSelector<SpatialOps::YDIR>
  {
    typedef YVolField   VolT;
    typedef SpatialOps::FaceTypes<VolT>::XFace XFaceT;
    typedef SpatialOps::FaceTypes<VolT>::YFace YFaceT;
    typedef SpatialOps::FaceTypes<VolT>::ZFace ZFaceT;
  };
  template<> struct NativeFieldSelector<SpatialOps::ZDIR>
  {
    typedef ZVolField   VolT;
    typedef SpatialOps::FaceTypes<VolT>::XFace XFaceT;
    typedef SpatialOps::FaceTypes<VolT>::YFace YFaceT;
    typedef SpatialOps::FaceTypes<VolT>::ZFace ZFaceT;
  };

  /**
   * \ingroup Tools
   * \struct FaceFieldSeclector
   * \brief Given the bundle direction, obtain surface field types.
   *
   * Example
   * \code{.cpp}
   * // Field type for faces in this bundle direction
   * // (e.g. x-face on an x-bundle)
   * typdef FaceFieldSeclector<DirT>::ParallelT ParFieldT;

   * // Field type for faces in in the first perpendicular bundle direction
   * // (e.g. y-face on an x-bundle)
   * typdef FaceFieldSeclector<DirT>::Perp1T    Perp1FieldT;

   * // Field type for faces in in the second perpendicular bundle direction
   * // (e.g. z-face on an x-bundle)
   * typdef FaceFieldSeclector<DirT>::Perp2T    Perp2TFieldT;
   * \endcode
   */
  template<typename DirT> struct FaceFieldSelector;

  template<> struct FaceFieldSelector<SpatialOps::XDIR>{
    typedef NativeFieldSelector<SpatialOps::XDIR>::XFaceT ParallelT;
    typedef NativeFieldSelector<SpatialOps::XDIR>::YFaceT Perp1T;
    typedef NativeFieldSelector<SpatialOps::XDIR>::ZFaceT Perp2T;
  };
  template<> struct FaceFieldSelector<SpatialOps::YDIR>{
    typedef NativeFieldSelector<SpatialOps::YDIR>::YFaceT ParallelT;
    typedef NativeFieldSelector<SpatialOps::YDIR>::XFaceT Perp1T;
    typedef NativeFieldSelector<SpatialOps::YDIR>::ZFaceT Perp2T;
  };
  template<> struct FaceFieldSelector<SpatialOps::ZDIR>{
    typedef NativeFieldSelector<SpatialOps::ZDIR>::ZFaceT ParallelT;
    typedef NativeFieldSelector<SpatialOps::ZDIR>::XFaceT Perp1T;
    typedef NativeFieldSelector<SpatialOps::ZDIR>::YFaceT Perp2T;
  };

  /**
   * \ingroup Tools
   * \struct CoarseFieldSelector
   * \brief Given a field type on a bundle, this translates to the corresponding "coarse" field type.
   * \tparam FineT the type for the fine field that we want the corresponding coarse field type for.
   * Example:
   * \code{.cpp}
   *   typedef CoarseFieldSelector<MyFieldT>::type  CoarseFieldT;
   * \endcode
   */
  template< typename FineT > struct CoarseFieldSelector;

  template<> struct CoarseFieldSelector<XVolField  >{ typedef SpatialOps::SVolField   type; };
  template<> struct CoarseFieldSelector<XSurfXField>{ typedef SpatialOps::SSurfXField type; };
  template<> struct CoarseFieldSelector<XSurfYField>{ typedef SpatialOps::SSurfYField type; };
  template<> struct CoarseFieldSelector<XSurfZField>{ typedef SpatialOps::SSurfZField type; };

  template<> struct CoarseFieldSelector<YVolField  >{ typedef SpatialOps::SVolField   type; };
  template<> struct CoarseFieldSelector<YSurfXField>{ typedef SpatialOps::SSurfXField type; };
  template<> struct CoarseFieldSelector<YSurfYField>{ typedef SpatialOps::SSurfYField type; };
  template<> struct CoarseFieldSelector<YSurfZField>{ typedef SpatialOps::SSurfZField type; };

  template<> struct CoarseFieldSelector<ZVolField  >{ typedef SpatialOps::SVolField   type; };
  template<> struct CoarseFieldSelector<ZSurfXField>{ typedef SpatialOps::SSurfXField type; };
  template<> struct CoarseFieldSelector<ZSurfYField>{ typedef SpatialOps::SSurfYField type; };
  template<> struct CoarseFieldSelector<ZSurfZField>{ typedef SpatialOps::SSurfZField type; };

  /**
   * \ingroup Tools
   * \fn template<typename FieldT> std::string suffix()
   * \return the suffix for the given type of field
   * \brief Use this to generate an appropriate suffix for a field
   *   to distinguish it from other fields of the same name on
   *   different bundles.
   */
  template<typename FieldT> std::string suffix();

  /**
   * \ingroup Tools
   *
   * \fn template<typename FieldT> FieldT get_line_field( FieldT&, const SpatialOps::IntVec )
   *
   * \brief Obtain a line on this FieldT with the origin of the line at loc.  Used for fields on a bundle. NOTE: this excludes ghost cells!
   *
   * \param parent the field to extract a line from
   * \param loc the position for the origin of the line
   *
   * \return the the field on the desired line
   *
   * \tparam FieldT the type of field that we desire to obtain the line window for.
   */
  template< typename FieldT >
  FieldT
  get_line_field( FieldT& parent,
                  const SpatialOps::IntVec loc );

  /**
   * \ingroup Tools
   *
   * \fn template<typename FieldT> const FieldT get_line_field( const FieldT&, const SpatialOps::IntVec )
   *
   * \brief Obtain a line on this FieldT with the origin of the line at loc.  Used for fields on a bundle. NOTE: this excludes ghost cells!
   *
   * \param parent the field to extract a line from
   * \param loc the position for the origin of the line
   *
   * \return the the field on the desired line
   *
   * \tparam FieldT the type of field that we desire to obtain the line window for.
   */
  template< typename FieldT >
  const FieldT
  get_line_field( const FieldT& parent,
                  const SpatialOps::IntVec loc );

  /**
   * \ingroup Tools
   *
   * \fn template<typename FieldT> FieldT get_line_field( FieldT&, const SpatialOps::IntVec, const Direction )
   *
   * \brief obtain a line on this FieldT with the origin of the line at loc.  Used for coarse fields.  NOTE: this excludes ghost cells!
   *
   * \param parent the field to extract a line from
   * \param loc the position for the origin of the line
   * \param dir the direction to orient the line in (on the coarse mesh)
   *
   * \return the FieldT for the desired line
   *
   * \tparam FieldT the type of field that we desire to obtain the line window for.
   */
  template< typename FieldT >
  FieldT
  get_line_field( FieldT& parent,
                  const SpatialOps::IntVec loc,
                  const Direction dir );

  /**
   * \ingroup Tools
   *
   * \fn template<typename FieldT> const FieldT get_line_field( const FieldT&, const SpatialOps::IntVec, const Direction )
   *
   * \brief obtain a line on this FieldT with the origin of the line at loc.  Used for coarse fields.  NOTE: this excludes ghost cells!
   *
   * \param parent the field to extract a line from
   * \param loc the position for the origin of the line
   * \param dir the direction to orient the line in (on the coarse mesh)
   *
   * \return the FieldT for the desired line
   *
   * \tparam FieldT the type of field that we desire to obtain the line window for.
   */
  template< typename FieldT >
  const FieldT
  get_line_field( const FieldT& parent,
                  const SpatialOps::IntVec loc,
                  const Direction dir );

  /**
   * \ingroup Tools
   * \fn template<typename FieldT> FieldT get_face_field( const FieldT&, const Faces, const int, const int );
   *
   * \brief Obtain a field on a face
   *
   * \param parent the parent field
   * \param faceDir the face of interest
   * \param loc the index of the plane (in the perpendicular direction)
   * \param numPlanes (default 1) can be used to get a slab rather than a face.
   *        The slab grows in the (+) direction by this amount.
   *
   * \return the field on the desired face
   */
  template< typename FieldT >
  const FieldT
  get_face_field( const FieldT& parent,
                  const Faces faceDir,
                  const int loc,
                  const int numPlanes = 1 );

} // namespace LBMS

#endif // LBMSFields_h
