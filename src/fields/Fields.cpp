/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file Fields.cpp
 */

#include "Fields.h"

#include <spatialops/structured/MemoryWindow.h>

using SpatialOps::MemoryWindow;
using SpatialOps::IntVec;
namespace so=SpatialOps;
using std::string;

namespace LBMS{

  //------------------------------------------------------------------

  template<typename FieldT>
  string suffix()
  {
    return get_bundle_name_suffix( direction<typename FieldT::Location::Bundle>() );
  }

  // explicit template instantiation for supported field types
  template string suffix<  XVolField>();
  template string suffix<XSurfXField>();
  template string suffix<YSurfXField>();
  template string suffix<ZSurfXField>();
  template string suffix<  YVolField>();
  template string suffix<XSurfYField>();
  template string suffix<YSurfYField>();
  template string suffix<ZSurfYField>();
  template string suffix<  ZVolField>();
  template string suffix<XSurfZField>();
  template string suffix<YSurfZField>();
  template string suffix<ZSurfZField>();

  //------------------------------------------------------------------

  template< typename FieldT >
  FieldT
  inline get_line_field_internal( const FieldT& f,
                                  const Direction dir0,
                                  const IntVec loc )
  {
    const Direction dir1 = get_perp1(dir0);
    const Direction dir2 = get_perp2(dir0);

    assert( loc[dir0] == 0 );


    const MemoryWindow& parentWindow = f.window_without_ghost();
    IntVec extent = parentWindow.extent();
    // only one cell in the bundle-perpendicular directions
    extent[dir1] = 1;
    extent[dir2] = 1;

    const so::IntVec& poffset = parentWindow.offset();
    const MemoryWindow mw( parentWindow.glob_dim(), poffset+loc, extent );

    return FieldT( mw, f );
  }

  template< typename FieldT >
  const FieldT
  get_line_field( const FieldT& parent, const IntVec loc )
  {
    return get_line_field_internal<FieldT>( parent,
                                            direction<typename FieldT::Location::Bundle>(),
                                            loc );
  }

  template< typename FieldT >
  FieldT
  get_line_field( FieldT& parent, const IntVec loc )
  {
    return get_line_field_internal<FieldT>( parent,
                                            direction<typename FieldT::Location::Bundle>(),
                                            loc );
  }

  template< typename FieldT >
  FieldT
  get_line_field( FieldT& parent, const IntVec loc, const Direction dir0 )
  {
    return get_line_field_internal<FieldT>( parent, dir0, loc );
  }

  template< typename FieldT >
  const FieldT
  get_line_field( const FieldT& parent, const IntVec loc, const Direction dir0 )
  {
    return get_line_field_internal<FieldT>( parent, dir0, loc );
  }

  //-----------------------------------------------------------------

  template< typename FieldT >
  FieldT
  get_face_field( FieldT& parent,
                  const Faces faceDir,
                  const int loc,
                  const int numPlanes )
  {
    const MemoryWindow& parentWindow = parent.window_with_ghost();
    IntVec offset=parentWindow.offset(), extent=parentWindow.extent();
    
    switch( faceDir ){
    case XMINUS:
      offset[0] += loc;
      extent[0] = numPlanes;
      break;
    case XPLUS:
      offset[0] += std::max(0,extent[0]-loc);
      extent[0] = numPlanes;
      break;
    case YMINUS:
      offset[1] += loc;
      extent[1] = numPlanes;
      break;
    case YPLUS:
      offset[1] += std::max(0,extent[1]-loc);
      extent[1] = numPlanes;
      break;
    case ZMINUS:
      offset[2] += loc;
      extent[2] = numPlanes;
      break;
    case ZPLUS:
      offset[2] += std::max(0,extent[2]-loc);
      extent[2] = numPlanes;
      break;
    }
    const MemoryWindow mw( parentWindow.glob_dim(), offset, extent );
    return FieldT( mw, parent);
  }

  template< typename FieldT >
  const FieldT
  get_face_field( const FieldT& parent,
                  const Faces faceDir,
                  const int loc,
                  const int numPlanes )
  {
    const MemoryWindow& parentWindow = parent.window_with_ghost();
    IntVec offset = parentWindow.offset(), extent=parentWindow.glob_dim();

    switch( faceDir ){
    case XMINUS:
      offset[0] += loc;
      extent[0] = numPlanes;
      break;
    case XPLUS:
      offset[0] += std::max(0,extent[0]-loc);
      extent[0] = numPlanes;
      break;
    case YMINUS:
      offset[1] += loc;
      extent[1] = numPlanes;
      break;
    case YPLUS:
      offset[1] += std::max(0,extent[1]-loc);
      extent[1] = numPlanes;
      break;
    case ZMINUS:
      offset[2] += loc;
      extent[2] = numPlanes;
      break;
    case ZPLUS:
      offset[2] += std::max(0,extent[2]-loc);
      extent[2] = numPlanes;
      break;
    }
    const MemoryWindow mw( parentWindow.glob_dim(), offset, extent );
    return FieldT( mw, parent);
  }

  //-----------------------------------------------------------------
  // Explicit template instantiations:

#define INSTANTIATE_LINE_FIELD_1( T )                           \
  template       T get_line_field<T>(       T&, const IntVec ); \
  template const T get_line_field<T>( const T&, const IntVec );

#define INSTANTIATE_LINE_FIELD_2( T )                                                   \
  template       T get_line_field<T>(       T&, const IntVec, const Direction );        \
  template const T get_line_field<T>( const T&, const IntVec, const Direction );

#define INSTANTIATE_FACE_FIELD( T )                                                     \
  template       T get_face_field<T>(       T&, const Faces, const int, const int );    \
  template const T get_face_field<T>( const T&, const Faces, const int, const int );

#define INSTANTIATE_GROUP( VOLT )                                   \
    INSTANTIATE_LINE_FIELD_1( VOLT )                                \
    INSTANTIATE_LINE_FIELD_1( SpatialOps::FaceTypes<VOLT>::XFace )  \
    INSTANTIATE_LINE_FIELD_1( SpatialOps::FaceTypes<VOLT>::YFace )  \
    INSTANTIATE_LINE_FIELD_1( SpatialOps::FaceTypes<VOLT>::ZFace )  \
    INSTANTIATE_LINE_FIELD_2( VOLT )                                \
    INSTANTIATE_LINE_FIELD_2( SpatialOps::FaceTypes<VOLT>::XFace )  \
    INSTANTIATE_LINE_FIELD_2( SpatialOps::FaceTypes<VOLT>::YFace )  \
    INSTANTIATE_LINE_FIELD_2( SpatialOps::FaceTypes<VOLT>::ZFace )  \
    INSTANTIATE_FACE_FIELD( VOLT )                                  \
    INSTANTIATE_FACE_FIELD( SpatialOps::FaceTypes<VOLT>::YFace )    \
    INSTANTIATE_FACE_FIELD( SpatialOps::FaceTypes<VOLT>::XFace )    \
    INSTANTIATE_FACE_FIELD( SpatialOps::FaceTypes<VOLT>::ZFace )

  INSTANTIATE_GROUP( XVolField )
  INSTANTIATE_GROUP( YVolField )
  INSTANTIATE_GROUP( ZVolField )

  // note that coarse fields can only use the second, not the first, implementation of get_line_field
  INSTANTIATE_LINE_FIELD_2( SpatialOps::SVolField   )
  INSTANTIATE_LINE_FIELD_2( SpatialOps::SSurfXField )
  INSTANTIATE_LINE_FIELD_2( SpatialOps::SSurfYField )
  INSTANTIATE_LINE_FIELD_2( SpatialOps::SSurfZField )

} // namespace LBMS
