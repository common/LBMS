/**
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Checkpoint.h"
#include <fields/Fields.h>
#include <lbms/Bundle.h>
#include <mpi/FieldBundleExchange.h>
#include <test/TestHelper.h>

#include <expression/ExprLib.h>

#include <iostream>
#include <iomanip>

#include <boost/property_tree/ptree.hpp>
namespace bpt=boost::property_tree;
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace bfs=boost::filesystem;
using SpatialOps::IntVec;

#ifdef HAVE_MPI
#include <mpi/Environment.h>
#endif

#define TYPE_NAME(FT) #FT

#define WRITER( FT )                                                                     \
{                                                                                        \
  typedef typename Expr::FieldMgrSelector<FT>::type FieldMgr;                            \
  const FieldMgr& fm = fml.field_manager<FT>();                                          \
  if( fm.has_field(*ifld) ){                                                             \
    const FT& field = fm.field_ref(*ifld);                                               \
    /* re-window field to this line */                                                   \
    const FT f( get_line_field( field, locOffset ) );                                    \
    for( typename FT::const_iterator i=f.begin(); i!=f.end(); ++i ){                     \
      fout << std::setprecision( std::numeric_limits<double>::digits10 ) << *i << " ";   \
    }                                                                                    \
    fout << std::endl;                                                                   \
    if( isFirstLine ){                                                                   \
      Checkpoint::FieldMetaData fmd;                                                     \
      fmd.fieldTypeName = TYPE_NAME(FT);                                                 \
      fmd.fieldTag = Expr::Tag( *ifld );                                                 \
      fmd.npts = f.window_with_ghost().extent();                                         \
      bpt::ptree& fData = metadata.add("field","");                                      \
      fmd.write_ptree( fData );                                                          \
    }                                                                                    \
  }                                                                                      \
}

#define READER( FT )                                                                     \
{                                                                                        \
  typedef typename Expr::FieldMgrSelector<FT>::type FieldMgr;                            \
  FieldMgr& fm = fml.field_manager<FT>();                                                \
  if( fm.has_field(*ifld) ){                                                             \
    FT& field = fm.field_ref(*ifld);                                                     \
    /* re-window field to this line */                                                   \
    FT f( get_line_field( field, locOffset ) );                                          \
    for( typename FT::iterator i=f.begin(); i!=f.end(); ++i ){                           \
      fin >> *i;                                                                         \
    }                                                                                    \
    std::string tmp;                                                                     \
    std::getline(fin,tmp);                                                               \
  }                                                                                      \
}


namespace LBMS {

  //jcs TODO
  //  look into boost::iostreams for a possible way to achieve on-the-fly compression.

  //-----------------------------------------------------------------

  /**
   *\brief writes the data from the given bundle
   *
   *@param[in] bundle   the bundle being written
   *@param[in] fml      the field manageer list
   *@param[in] path     the path to the folder being written
   *@param[in] fields   the feilds which to write
   *@param[in] metadata the metadata which will be written in an xml file
   */
  template< typename DirT >
  void
  write_bundle_data( const BundlePtr& bundle,
                     const Expr::FieldManagerList& fml,
                     const bfs::path& path,
                     const Expr::TagSet& fields,
                     bpt::ptree& metadata )
  {
    const IntVec& npts = bundle->npts();
    const Direction dir0 = bundle->dir();
    const Direction dir1 = get_perp1(dir0);
    const Direction dir2 = get_perp2(dir0);

    const Checkpoint::BundleMetaData bmd( bundle );
    bpt::ptree& bundleTree = metadata.add( "bundle", "" );
    bmd.write_ptree( bundleTree );

    bool isFirstLine = true;

    IntVec locOffset(0,0,0);
    for( size_t idim1=0; idim1!=npts[dir1]; ++idim1 ){
      locOffset[dir1] = idim1;
      for( size_t idim2=0; idim2!=npts[dir2]; ++idim2 ){
        locOffset[dir2] = idim2;
        const IntVec lineIndex = bundle->global_mesh_offset() + locOffset;

        std::string fname( get_dir_name(dir0) + "_" +
                           boost::lexical_cast<std::string>(lineIndex[dir1]) + "_" +
                           boost::lexical_cast<std::string>(lineIndex[dir2]) );

        bfs::ofstream fout( path/fname );
        assert( fout.good()    );
        assert( fout.is_open() );

        // dump fields on this line into the file
        for( typename Expr::TagSet::const_iterator ifld = fields.begin(); ifld!=fields.end(); ++ifld ){
          typedef typename NativeFieldSelector<DirT>::VolT    VolT;
          typedef typename NativeFieldSelector<DirT>::XFaceT  XFaceT;
          typedef typename NativeFieldSelector<DirT>::YFaceT  YFaceT;
          typedef typename NativeFieldSelector<DirT>::ZFaceT  ZFaceT;
          WRITER( VolT   )
          WRITER( XFaceT )
          WRITER( YFaceT )
          WRITER( ZFaceT )
        } // field loop

        isFirstLine = false;

      } // lattice 2 loop
    } // lattice 1 loop
  }

  //---------------------------------------------------------------

  /**
   * \brief given the path and the direction opens the xml file and reads out the time variable.
   * @param[in] path the boost filesystem path for the folder of the checkpoint being read
   * @param[in] dir  the Direction of the xml file being written.
   *
   * @return the simulation time as a double.
   */
  double get_time( const bfs::path& path, const Direction dir )
  {
    const std::string filename = get_dir_name(dir) + "_FieldInfo.xml";
    bfs::ifstream fin(path/filename);
    bpt::ptree pt;
    bpt::read_xml(fin , pt );
    bpt::ptree& tTree = pt.get_child("info");
    double ret = tTree.get<double>("time");
    return ret;
  }

  /**
   * \brief reads in from a file the data for the given bundle
   * @param[in, out]    bundle          the bundle which is being read
   * @param[in]         fml             the Field Manager List
   * @param[in]         path            the path to the folder containing the file
   * @param[in]         fields          the list of fields
   *
   * @return    returns the time as a double.
   */
  template<typename DirT>
  double
  read_bundle_data( const BundlePtr bundle,
                    Expr::FieldManagerList& fml,
                    const bfs::path& path,
                    const Expr::TagSet& fields)
  {
    const IntVec& npts = bundle->npts();
    const Direction dir0 = bundle->dir();
    const Direction dir1 = get_perp1(dir0);
    const Direction dir2 = get_perp2(dir0);

    IntVec locOffset(0,0,0);
    for( size_t idim1=0; idim1!=npts[dir1]; ++idim1 ){
      locOffset[dir1] = idim1;
      for( size_t idim2=0; idim2!=npts[dir2]; ++idim2 ){
        locOffset[dir2] = idim2;
        const IntVec lineIndex = bundle->global_mesh_offset() + locOffset;

        const std::string fname( get_dir_name(dir0) + "_" +
                                 boost::lexical_cast<std::string>(lineIndex[dir1]) + "_" +
                                 boost::lexical_cast<std::string>(lineIndex[dir2]) );

        bfs::ifstream fin( path/fname );

        if( !fin.good() ){
          std::cout << std::endl
                    << "Checkpoint file is not good"            << std::endl
                    << "Path to Checkpoint file:  "    << path  << std::endl
                    << "Checkpoint file:  "            << fname << std::endl << std::endl;
          throw;
        }

        if( !fin.is_open() ){
          std::cout << std::endl
                    << "Checkpoint file is not open"            << std::endl
                    << "Path to Checkpoint file:  "    << path  << std::endl
                    << "Checkpoint file:  "            << fname << std::endl << std::endl;
          throw;
        }

        // read fields on this line from the file
        for( typename Expr::TagSet::const_iterator ifld = fields.begin(); ifld!=fields.end(); ++ifld ){
          typedef typename NativeFieldSelector<DirT>::VolT    VolT;
          typedef typename NativeFieldSelector<DirT>::XFaceT  XFaceT;
          typedef typename NativeFieldSelector<DirT>::YFaceT  YFaceT;
          typedef typename NativeFieldSelector<DirT>::ZFaceT  ZFaceT;
          READER( VolT )
          READER( XFaceT )
          READER( YFaceT )
          READER( ZFaceT )
        } // field loop
      } // lattice 2 loop
    } // lattice 1 loop

    return get_time( path, dir0 ); //returns the time.
  }

  //---------------------------------------------------------------

  Checkpoint::Checkpoint( const BundlePtrVec& bv,
                          const Expr::TagSet& fields,
                          Expr::FMLMap& fmls,
                          const bfs::path basePath )
  : bundles_( bv ),
    fields_( fields ),
    fmls_( fmls ),
    basePath_( basePath )
  {}

  //-----------------------------------------------------------------

  Checkpoint::~Checkpoint()
  {}

  //-----------------------------------------------------------------

  void
  write_metadata( const Direction dir,
                  const boost::filesystem::path& path,
                  const bpt::ptree& tree )
  {
    const std::string fname = get_dir_name(dir)+"_FieldInfo.xml";
    bfs::ofstream fout( path/fname );
    // boost 1.56 introduced a breaking change.
#   if Boost_MAJOR_VERSION == 1 && Boost_MINOR_VERSION < 56
    const bpt::xml_writer_settings<char> settings(' ',2u);
#   else
    const bpt::xml_writer_settings<std::string> settings(' ',2u);
#   endif
    bpt::write_xml( fout, tree, settings );
  }

  //-----------------------------------------------------------------

  void
  Checkpoint::write_checkpoint( const std::string& cplabel,  double simTime ) const
  {
    //Try/catch will attempt to write up to 10 times if there is a failure.
    //This is due to seemingly random errors with dropping checkpoints on Ash.
    int tries = 0;
    bool failed = true;
    while( failed && tries < 10 ){
      failed = false;
      try{
        const bfs::path path = basePath_ / cplabel;
        bfs::create_directory(path);

        bool xfirst=true, yfirst=true, zfirst=true;
    #   ifdef HAVE_MPI
        if( Environment::xbundle_group().rank() != 0 ) xfirst = false;
        if( Environment::ybundle_group().rank() != 0 ) yfirst = false;
        if( Environment::zbundle_group().rank() != 0 ) zfirst = false;
    #   endif
        for( BundlePtrVec::const_iterator ib=bundles_.begin(); ib!=bundles_.end(); ++ib ){
          const BundlePtr b = *ib;
          const Direction bdir = b->dir();
          bpt::ptree metadata;
          bpt::ptree& childMetaData = metadata.add("info","");
          std::stringstream ss;
          ss << simTime;
          childMetaData.add("time", ss.str());
          const Expr::FMLMap& fmls = fmls_;
          const Expr::FieldManagerList* fml = Expr::extract_field_manager_list(fmls,b->id());


          ++tries;
          switch( bdir ){
                case XDIR:
                  write_bundle_data<SpatialOps::XDIR>( b, *fml, path, fields_, childMetaData );
                  if( xfirst ) write_metadata( bdir, path, metadata );
                  xfirst=false;
                  break;
                case YDIR:
                  write_bundle_data<SpatialOps::YDIR>( b, *fml, path, fields_, childMetaData );
                  if( yfirst ) write_metadata( bdir, path, metadata );
                  yfirst=false;
                  break;
                case ZDIR:
                  write_bundle_data<SpatialOps::ZDIR>( b, *fml, path, fields_, childMetaData );
                  if( zfirst ) write_metadata( bdir, path, metadata );
                  zfirst=false;
                break;
                case NODIR: break;
          }
        }
      }
      catch(...){
        failed = true;
      }
    } // loop on bundles
  }

  //-----------------------------------------------------------------

  double
  Checkpoint::read_checkpoint( const std::string& label ) const
  {
    const bfs::path path = label;
    double rettime = 0.0;
    for( BundlePtrVec::const_iterator ib=bundles_.begin(); ib!=bundles_.end(); ++ib ){
      const BundlePtr b = *ib;
      if( b->npts()[b->dir()] > 1){
        switch( b->dir() ){
        case XDIR: rettime = read_bundle_data<SpatialOps::XDIR>( b, *fmls_[b->id()], path, fields_); break;
        case YDIR: rettime = read_bundle_data<SpatialOps::YDIR>( b, *fmls_[b->id()], path, fields_); break;
        case ZDIR: rettime = read_bundle_data<SpatialOps::ZDIR>( b, *fmls_[b->id()], path, fields_); break;
        case NODIR: break;
        }
      }
    }
    return rettime;
  }

  //-----------------------------------------------------------------

  Checkpoint::BundleMetaData::BundleMetaData( const BundlePtr b )
  : dir    ( get_dir_name( b->dir() ) ),
    length ( b->glob_length()  ),
    npts   ( b->glob_npts()    ),
    ncoarse( b->glob_ncoarse() ),
    offset ( b->glob_global_mesh_offset()  )
  {}

  void
  Checkpoint::BundleMetaData::read_ptree( const bpt::ptree& node )
  {
    dir     = node.get<std::string>("dir");
    npts    = SpatialOps::IntVec( node.get<int>("nX"),       node.get<int>("nY"),       node.get<int>("nZ")       );
    ncoarse = SpatialOps::IntVec( node.get<int>("ncoarseX"), node.get<int>("ncoarseY"), node.get<int>("ncoarseZ") );
    offset  = SpatialOps::IntVec( node.get<int>("offsetX"),  node.get<int>("offsetY"),  node.get<int>("offsetZ")  );
    length  = Coordinate( node.get<double>("LX"), node.get<double>("LY"), node.get<double>("LZ") );
  }

  void
  Checkpoint::BundleMetaData::write_ptree( bpt::ptree& node ) const
  {
    node.add( "dir", dir );
    node.add( "nX", npts[0] );
    node.add( "nY", npts[1] );
    node.add( "nZ", npts[2] );
    node.add( "ncoarseX", ncoarse[0] );
    node.add( "ncoarseY", ncoarse[1] );
    node.add( "ncoarseZ", ncoarse[2] );
    node.add( "offsetX", offset[0] );
    node.add( "offsetY", offset[1] );
    node.add( "offsetZ", offset[2] );
    node.add( "LX", length[0] );
    node.add( "LY", length[1] );
    node.add( "LZ", length[2] );
  }

  bool
  Checkpoint::BundleMetaData::operator !=( const Checkpoint::BundleMetaData& md ) const
  {
    return !(*this==md);
  }

  bool
  Checkpoint::BundleMetaData::operator ==( const Checkpoint::BundleMetaData& md ) const
  {
    TestHelper status(false);
    status( dir     == md.dir     );
    status( npts    == md.npts    );
    status( ncoarse == md.ncoarse );
    status( offset  == md.offset  );
    status( length  == md.length  );
    return status.ok();
  }

  //-----------------------------------------------------------------

  void
  Checkpoint::FieldMetaData::read_ptree( const bpt::ptree& node )
  {
    fieldTag = Expr::Tag( node.get<std::string>("name"),
                          Expr::str2context( node.get<std::string>("context") ) );
    fieldTypeName = node.get<std::string>("type");
    npts = SpatialOps::IntVec( node.get<int>("nx"), node.get<int>("ny"), node.get<int>("nz") );
  }

  void
  Checkpoint::FieldMetaData::write_ptree( bpt::ptree& node ) const
  {
    node.add( "name",    fieldTag.name()    );
    node.add( "context", fieldTag.context() );
    node.add( "type",    fieldTypeName      );
    node.add( "nx",      npts[0]            );
    node.add( "ny",      npts[1]            );
    node.add( "nz",      npts[2]            );
  }

  bool
  Checkpoint::FieldMetaData::operator!=( const Checkpoint::FieldMetaData& md ) const
  {
    return !(*this==md);
  }

  bool
  Checkpoint::FieldMetaData::operator==( const Checkpoint::FieldMetaData& md ) const
  {
    TestHelper status(false);
    status( fieldTypeName == md.fieldTypeName );
    status( fieldTag == md.fieldTag );
    status( npts == md.npts );
    return status.ok();
  }

  //-----------------------------------------------------------------

} /* namespace LBMS */
