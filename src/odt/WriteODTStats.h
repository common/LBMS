#ifndef WriteODTStats_H_
#define WriteODTStats_H_

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem/path.hpp>

#include <spatialops/structured/IntVec.h>
#include <lbms/Direction.h>
#include <string>

namespace LBMS{
  /**
   * \class WriteODTStats
   * \author Derek Cline
   * \brief drops triplet mapping statistics
   */
  class WriteODTStats
  {
  public:

    struct ODTStats
    {
      int sindex;
      int eindex;
      double eddystart;
      double eddylife;
      SpatialOps::IntVec location;
    };

    /**
     * @brief drops triplet mapping statistics
     * @param dir the direction of interest
     */
    WriteODTStats( const LBMS::Direction dir, const std::string jobName );

    ~WriteODTStats();

    /**
     * \brief writes stats to the file
     */
    void write_stats( const ODTStats, const int ) const;

  private:
    const LBMS::Direction dir_;
    boost::filesystem::path path_;
    const std::string jobName_;
  };


}//LBMS namespace

#endif /* WriteODTStats_H_ */
