#include <odt/WriteODTStats.h>
#include <fstream>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace LBMS{

  //-----------------------------------------------------------------
  WriteODTStats::WriteODTStats( const Direction dir, const std::string jobName )
  : dir_(dir),
    jobName_(jobName)
  {
    std::string dname("./eddystats_"+jobName_);
    path_ = dname;
    boost::filesystem::create_directory(path_);
  }

  //-----------------------------------------------------------------
  WriteODTStats::~WriteODTStats()
  {}

  //-----------------------------------------------------------------
  void
  WriteODTStats::write_stats( const ODTStats stats, const int eddycount ) const
  {
    std::string fname(boost::lexical_cast<std::string>(dir_));

    boost::filesystem::ofstream fout;

    //Will write over information for the first eddy,
    //otherwise will seek the end of the file
    if( eddycount == 1 ) fout.open(path_/fname);
    else                 fout.open(path_/fname, std::ios::app );

    assert( fout.good()    );
    assert( fout.is_open() );

    fout << stats.location << " " << stats.sindex << " " << stats.eindex << " "
         << stats.eddystart << " " << stats.eddylife << std::endl;
  //      std::setprecision( std::numeric_limits<double>::digits10 ) << << " ";
  }

} // namespace LBMS
