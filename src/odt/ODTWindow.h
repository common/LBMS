/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 *  \file ODTWindow.h
 */

#include <spatialops/structured/MemoryWindow.h>
#include <fields/Fields.h>
#include <fields/StringNames.h>
#include <lbms/Bundle_fwd.h>
#include <lbms/Direction.h>
#include <expression/ExprFwd.h>
#include <odt/ODT.h>
#include <odt/WriteODTStats.h>
#include <ctime>        // For time()
#include <cstdlib>      // For srand() and rand()

#include <odt/Conservation.h>

#ifndef ODTWindow_H_
#define ODTWindow_H_

namespace YAML{ class Node; }



namespace LBMS{

  /**
   *  @struct ConservationInfo
   *  @brief Contains send helper, field tag and coarse field tag
   *         for use in ODTWindow.cpp
   */
  struct ConservationInfo
  {
    Expr::Tag conservationTag;                            ///< Coarse field tag
    Expr::Tag transformTag;                               ///< Tag of field to be transformed by ODT
    SpatialOps::SVolField* coarseField;                       ///< Coarse field to be filled
    CoarseFieldSendHelper<SpatialOps::SVolField>* sendHelper; ///< Coarse field send helper
  };


  /**
   * \class ODTWindow
   * \author Derek Cline
   * \brief Creates line windows for each line in each direction
   *        and attempts to apply the triplet mapping procedure
   *        to each line window
   */
  template< typename DirT >
  class ODTWindow
  {
  public:
    /**
     * @brief Constructs the ODTWindow
     * @param bundle
     * @param fml Field Manager List
     * @param odtparams Structure of parameters required for triplet mapping:
     *        alpha, viscous cutoff, beta, eddy rate constant, stress tolerance,
     *        reynolds number and integral length scale
     */
    ODTWindow( const BundlePtr bundle,
               const ODTParameters odtparams,
               const std::vector<std::string>& varNames );

    ~ODTWindow();

    /**
     * \brief Loop to apply ODT transformation to each line
     */
    void loop_through( const double simulationTime, LBMS::Conservation<DirT>& conservation ) const;

    void clear_conservation_send_buffers() const;

  protected:
    /**
     * @brief Creates line window and fills ODT
     * @param location The IntVec location of the line window in the domain
     * @param simulationTime the current time
     * @param writer the writer to use
     */
    void create_window( const SpatialOps::IntVec& location,
                        const double simulationTime,
                        const WriteODTStats writer,
                        LBMS::Conservation<DirT>& conservation ) const;

    ODTParameters odtparams;

  private:

    typedef typename LBMS::NativeFieldSelector<DirT>::VolT      VFieldT;
    typedef typename LBMS::CoarseFieldSelector< VFieldT >::type SFieldT;

    const BundlePtr bundle_;
    const StringNames& sName_;
    const Direction dir_;
    Expr::FieldManagerList& fml_;
    const ODTParameters odtparams_;
    const SpatialOps::IntVec extent_;
    mutable int eddyCount_;
    double cutoff_;

    std::vector<ConservationInfo> conservationInfoVec_;

    void clear_coarse_fields() const;
  };

  // jcs these should be moved to their own file, as should the declaration of the ODTParameters struct.
  /**
   *  \fn ODTParameters fill_ODT_params( const YAML::Node& );
   *  \brief Obtains ODT Parameters from input file
   */
  ODTParameters fill_ODT_params( const YAML::Node& );
  ODTParameters fill_ODT_params();
}//LBMS namespace

#endif /* ODTWindow_H_ */
