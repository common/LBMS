/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 * \file ODT.h
 */

#ifndef ODT_h
#define ODT_h

#include "LagrangePoly.h"

#include <fields/Fields.h>
#include <lbms/Direction.h>

#include <expression/Tag.h>

#include <vector>
#include <iostream>
#include <cmath>
#include <cstdlib>      // For srand() and rand()


namespace LBMS{

  // jcs consider moving this into its own header file and then providing a "parse" function to populate it...
  /**
   * \struct ODTParameters
   * \brief holds parameters that influence the selection and implementation of eddies
   */
  struct ODTParameters
  {
    double alpha;        ///< The velocity redistribution parameter [0,1]
    double z;            ///< The viscous cutoff parameter
    double beta;         ///< Parameter for Large eddy suppression
    double c;            ///< The eddy rate parameter
    double cCoefA;       ///< A coefficient for the eddy rate parameter model
    double cCoefB;       ///< A coefficient for the eddy rate parameter model
    double stresstol;    ///< The threshold for the shear stress in considering eddy locations
    double Re;           ///< Reynold's number of the flow
    double integLS;      ///< Integral length scale
    double timestep;     ///< The time step for the time integrator (assumed constant)
    unsigned int seed;   ///< Integer to seed random variable.  Note that 0 defaults to time(NULL) as a seed
    int    number_spec;  ///< Number of species
    bool   implementODT; ///< Boolean to implement ODT
    bool   test;         ///< Boolean to remove randomness from Eddies for testing purposes
    bool   statDump;     ///< Boolean to print out eddy statistics
    std::string jobName; ///< Job name for dropping eddy stats
    std::vector<Expr::Tag> fieldTags;
  };


  /**
   *  @class ODT
   *  @brief Applies Triplet Mapping to a given Field
   *
   *  Currently, this will (should it?) be hard coded and
   *  applied after the RHS is fully resolved.
   */
  template< typename DirT >
  class ODT
  {
    public:

      typedef typename LBMS::NativeFieldSelector<DirT>::VolT VFieldT;
      typedef typename LBMS::NativeFieldSelector<DirT>::XFaceT XFieldT;
      typedef typename LBMS::NativeFieldSelector<DirT>::YFaceT YFieldT;
      typedef typename LBMS::NativeFieldSelector<DirT>::ZFaceT ZFieldT;

      /**
       * \enum KernelSelector
       */
      enum KernelSelector
      {
        NO_KERNEL,            ///< NO_KERNEL
        KINETIC_ENERGY_KERNEL ///< KINETIC_ENERGY_KERNEL
      };

      // jcs need to finish documentation of constructor
      /**
       * @brief Create an ODT object to facilitate eddy selection and implementation.
       *        Note: that seeding of the random number occurs in ODTWindow.cpp
       *
       * @param xmom Volume Field of the momentum in the x direction
       * @param ymom Volume Field of the momentum in the y direction
       * @param zmom Volume Field of the momentum in the z direction
       * @param rho Volume Field of the density
       * @param coord Coordinate Volume Field
       * @param visc Volume Field of the viscosity
       * @param xFlux Face Field of the stress tensor compared to the stress tolerance
       *        in order to suppress eddies that occur with low energy
       * @param yFlux Face Field of the stress tensor compared to the stress tolerance
       *        in order to suppress eddies that occur with low energy
       * @param zFlux Face Field of the stress tensor compared to the stress tolerance
       *        in order to suppress eddies that occur with low energy
       * @param coord_vec Vector<double> of the coordinate field necessary for
       *        calculating Lagrange coefficients
       * @param params Struct containing parameters required for triplet mapping
       */
      ODT( VFieldT& xmom,
           VFieldT& ymom,
           VFieldT& zmom,
           VFieldT& rho,
           const VFieldT& coord,
           const VFieldT& visc,
           const XFieldT& xFlux,
           const YFieldT& yFlux,
           const ZFieldT& zFlux,
           const std::vector<double>& coord_vec,
           const ODTParameters& params,
           const double cutoff,
           const bool periodic );

      /**
       * @brief Overwrites field with temporary field values containing triplet mapping
       *        NOTE: transform should never be called without a check for has_eddy()
       * @param field field to have triplet mapping applied
       * @param op For kinetic energy kernel,  default is NO_KERNEL
       */
      void transform( VFieldT& field, const KernelSelector op = NO_KERNEL );

      /**
       * @return true if an eddy is going to be implemented
       */
      bool has_eddy() const { return implementEddy_; }

      /**
       * @return an integer representing the index of coordinate points from the beginning
       *         of the line window to the start of an eddy
       */
      int return_sindex() const{ return sindex_;}

      /**
       * @return an integer representing the index of coordinate points from the beginning
       *         of the line window to the start of an eddy for eddies crossing the boundary
       */
      int return_sindexPrior() const{ return sindexPrior_;}

      /**
       * @return an integer representing the index of coordinate points from the beginning
       *         of the line window to the end of an eddy
       */
      int return_eindex() const{ return eindex_;}

      /**
       * @return a double representing the length of an eddy
       */
      double return_length() const{ return eddyl_; }

      /**
       * @return the eddy turnover time to compare to the simulation time
       *         in order to suppress large eddies
       */
      double return_eddylifetime() const{ return eddylifetime_;}

    protected:
      /** \brief determine the location of the eddy (if it occurs) */
      void eddy_select();

      void calculate_kernel();
      void triplet_map_kernel( const VFieldT& );
      double trap_integral( const double* , const double* const );
      double field_trap_integral( typename VFieldT::const_iterator, typename VFieldT::const_iterator );
      double custom_integral( const std::vector<double>& field );
      double custom_integral( const std::vector<double>& field, const std::vector<double>& lesrho, const std::vector<double>& Kles );

      /** \brief apply the triplet map the the given field */
      void triplet_map( VFieldT& );

      /** \brief triplet mapping temporary storage */
      void triplet_map( VFieldT&, std::vector<double>& tempfield );

      /** \brief supports continuous triplet mapping procedure */
      double interpolate( const double, const VFieldT& );

      /** \brief Shifts a field for instances where an eddy crosses a periodic domain */
      void shift( VFieldT& );

      /** brief Shifts the field back */
      void shift_back( VFieldT& );

    private:

      VFieldT &umom_, &vmom_,&wmom_, &rho_;
      const VFieldT &coord_, &kinvisc_;
      const XFieldT xStress_;
      const YFieldT yStress_;
      const ZFieldT zStress_;
      const ODTParameters params_;
      const LagrangeCoefficients lagrangeCoefs_;
      bool implementEddy_;
      double y0_,eddyl_;                ///< Eddy location and length
      double c1_,c2_,c3_,gamma_;        ///< kernel coefficients
      double eddylifetime_;
      double Lp_, maxeddy_, mineddy_;
      double factorc_,time_;
      std::vector< double > newloc_, gvec_, phif_;
      std::vector< double > temprho_,tempumom_,tempvmom_,tempwmom_,Kvec_;
      int sindex_, eindex_, sindexPrior_;     ///< Starting and ending locations for eddy on window, prior shifts if over periodic boundary
      int tmpoints_;
      const double cutoff_;
      const bool periodic_;
      const double gridSpacing_;
      double reynolds_;

      //Statistics for periodic conditions
      double y0Prior_; ///< double y0_ before the shift
      bool acrossTheBoundary_, cleanup_; ///< does the eddy cross the boundary? did we shift?

  };
} // LBMS namespace

#endif /* ODT_h */
