/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 * \file Conservation.h
 * \author Derek Cline
 */
#include <fields/Fields.h>
#include <lbms/Direction.h>
#include <expression/ExprLib.h>
#include <mpi/Environment.h>
#include <mpi/FieldBundleExchange.h>
#include <mpi/FieldExchangeInfo.h>
#include <cmath>

#ifndef Conservation_H_
#define Conservation_H_

namespace LBMS{

  /**
   *  @class Conservation
   *  @brief Applies Coarse Volume Field changes due to Triplet Mapping from one bundle to the others
   *
   *  Currently, this class will need to be constructed within the ODT windowing class
   *  in order to ensure that only the Coarse Volumes which are affected by a Triplet
   *  Map are checked.  Not doing so will cause the code to break (or increase computational
   *  costs otherwise)
   */
  template< typename DirT >
  class Conservation
  {
  public:

    /**
     * @brief Create a Conservation object to set extents and domains for
     *  memory windows around Coarse Volumes.
     */
    Conservation( const std::vector<std::string>& varNames );
    ~Conservation();

    typedef typename LBMS::NativeFieldSelector<DirT>::VolT    VFieldT;
    typedef typename LBMS::CoarseFieldSelector<VFieldT>::type CoarseFieldT;

    /**
     * @brief integrates over each coarse cell in which an eddy lives
     * @param field line windowed field in which an eddy event has occurred
     * @param coord coordinate field associated with field
     * @param sindex starting location of the eddy event on the fine mesh
     * @param eindex ending location of the eddy event on the fine mesh
     */
    std::vector<double> integrate_over_coarse( const VFieldT& field,
                                               const VFieldT& coord,
                                               const int sindex,
                                               const int eindex ) const;

    /**
     * @brief Exchanges information on a field across bundles
     * @param coarseField the field to fill
     * @param location IntVec of the eddy event analogous to ODTwindow's Location parameter
     * @param before Vector of Coarse Volume Integrals before triplet mapping is applied
     * @param after Vector of Coarse Volume Integrals after triplet mapping is applied
     * @param sindex starting location
     */
    void fill_coarse_field( CoarseFieldT* coarseField,
                            const Expr::Tag tag,
                            const SpatialOps::IntVec location,
                            const std::vector<double> before,
                            const std::vector<double> after,
                            const int sindex ) const;

    void apply_conservation( const Expr::Tag perp1CoarseTag,
                             const Expr::Tag perp2CoarseTag,
                             const Expr::Tag fieldTag ) const;


    void set_conservation_fields( const std::vector<std::string>& varNames );

  private:

    const Direction dir_, perp1Dir_, perp2Dir_; ///< Direction determined from template parameter
    const BundlePtr bundle_;                    ///< Bundle pointer
    const int nFineInCoarse_;                   ///< number of points in a coarse control volume
  };

}//namespace LBMS

#endif /* Conservation_H_ */
