/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 * \file ODTWindow.cpp
 */

#include <odt/ODTWindow.h>
#include <fields/Fields.h>
#include <lbms/Bundle.h>
#include <fields/StringNames.h>
#include <lbms/Direction.h>
#include <mpi/Environment.h>

#include <yaml-cpp/yaml.h>

#include <expression/FieldManagerList.h>
#include <expression/FieldManager.h>

using SpatialOps::IntVec;
using LBMS::Direction;

namespace LBMS{


  /** \todo this should be moved to its own implementation file */
  ODTParameters fill_ODT_params( const YAML::Node& parser )
  {
    const YAML::Node odtParser( parser["TripletMapping"] );
    const YAML::Node paramPG = odtParser["Parameters"];

    ODTParameters odtparams;
    odtparams.implementODT = true;

    odtparams.alpha     = paramPG["Alpha"              ].as<double>( 0.5     );
    odtparams.z         = paramPG["ViscousCutoff"      ].as<double>( 1.0e-5  );
    odtparams.beta      = paramPG["Beta"               ].as<double>( 0.0     );
    odtparams.c         = paramPG["EddyRateConstant"   ].as<double>( 60.0    );

    odtparams.stresstol = paramPG["StressTol"          ].as<double>( 1.0     );
    odtparams.Re        = paramPG["Reynolds"           ].as<double>( 2.0e4   );
    odtparams.integLS   = paramPG["Integ_LS"           ].as<double>( 0.01    );

    odtparams.seed      = paramPG["Random_Number_Seed" ].as<unsigned int>( 0 );

    odtparams.test      = paramPG["Run_As_Test"        ].as<bool>( false );
    odtparams.statDump  = paramPG["Drop_Statistics"    ].as<bool>( false );

    odtparams.cCoefA    =          paramPG["EddyRateACoefficient"].as<double>( -1.69 );
    odtparams.cCoefB    = std::exp(paramPG["EddyRateBCoefficient"].as<double>( 17.4  ));

    if( odtparams.statDump ) odtparams.jobName = parser["JobName"].as<std::string>("test");

    //___________________________________
    // Grab fields that require triplet mapping from input file
    // Implementation of kernel choice on a per field basis will (need to) go here
    // Note that here we stash away tags, but they are not yet augmented with the
    // bundle direction information.  This will be done later.
    const YAML::Node tags( odtParser["Fields"] );

    for( YAML::Node::const_iterator ipg=tags.begin(); ipg!=tags.end(); ++ipg ){
      const  YAML::Node nt( (*ipg)["NameTag"] );
      proc0cout << nt << std::endl;
      odtparams.fieldTags.push_back( Expr::Tag( nt["name"].as<std::string>(),
                                                Expr::str2context( nt["state"].as<std::string>()) ) );
    }

    return odtparams;
  }

  //--------------------------------------------------------------------------------------------

  // jcs this should be moved to its own implementation file
  ODTParameters fill_ODT_params( )
  {
    //Fills ODT param struct for test case
    std::cout << std::endl;
    std::cout << "No ParseGroup fed to ODTparameters, using test values" << std::endl;
    std::cout << std::endl;

    ODTParameters odtparams;
    odtparams.implementODT = true;

    odtparams.alpha	= 0.5;
    odtparams.z		= 1.0e-8;
    odtparams.beta	= 0.0;
    odtparams.c		= 60.0;
    odtparams.stresstol	= 0.0;
    odtparams.Re	= 2.0e4;
    odtparams.integLS	= 0.5;
    odtparams.timestep  = 0.0002;
    odtparams.seed      = 1; //seeds random variables, zero will seed with time(NULL)

    odtparams.test      = true; //removes randomness to ODT for testing purposes
    odtparams.statDump  = true; //drops eddy statistics

    if( odtparams.statDump ) odtparams.jobName = "test";

    return odtparams;
  }

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  ODTWindow<DirT>::ODTWindow( const BundlePtr bundle,
                              const ODTParameters odtparams,
                              const std::vector<std::string>& varNames )
    : bundle_   ( bundle ),
      fml_      ( *bundle_->get_field_manager_list() ),
      odtparams_( odtparams           ),
      extent_   ( bundle_->npts()      ),
      dir_      ( direction<DirT>()   ),
      sName_    ( StringNames::self() )
  {
    cutoff_ = 3.5 * (bundle_->npts(dir_)) / (bundle_->ncoarse(dir_ ));

    //Seeding rand() requires an unsigned int.  In the case of 0, we default to a time(NULL) seed.
    //We should never get into the final else statement.
    if     ( odtparams_.seed == 0 ) srand( (LBMS::Environment::self().rank() * time(NULL))     );
    else if( odtparams_.seed >  0 ) srand(  LBMS::Environment::self().rank() * odtparams_.seed );
    else{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Random number seed should be an unsigned int." << std::endl;
      throw std::runtime_error( msg.str() );
    }
    eddyCount_ = 0;

    //Coarse field exchange setup
    typedef typename LBMS::CoarseFieldSelector<VFieldT>::type SFieldT;

    std::string consString = select_from_dir( direction<DirT>(), sName_.xcons, sName_.ycons, sName_.zcons );
    std::string bundleName = select_from_dir( direction<DirT>(), "_Xbundle", "_Ybundle", "_Zbundle" );

    {
      using Expr::Tag;
      ConservationInfo info;
      BOOST_FOREACH( const std::string& name, varNames ){
        const std::string strippedName = name.substr( 0, name.size()-8 );
        //Check to see if the variable is on bundle
        if( name == (strippedName + bundleName) ){
          const Tag tag = Tag( strippedName + consString + bundleName, Expr::STATE_NONE );
          info.conservationTag = tag;
          info.coarseField = &fml_.field_ref<SFieldT>( tag );
          info.sendHelper = new CoarseFieldSendHelper<SFieldT>( bundle_, tag, bundle_->dir(), bundle_->dir() );
          info.transformTag = Tag( name, Expr::STATE_N );
          conservationInfoVec_.push_back( info );
        }
      }
    }
  }

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  ODTWindow<DirT>::~ODTWindow()
  {}

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  void ODTWindow<DirT>::create_window( const IntVec& location,
                                       const double simulationTime,
                                       const WriteODTStats writer,
                                       LBMS::Conservation<DirT>& conservation ) const
  {
    typedef typename LBMS::NativeFieldSelector<DirT>::XFaceT XFFieldT;
    typedef typename LBMS::NativeFieldSelector<DirT>::YFaceT YFFieldT;
    typedef typename LBMS::NativeFieldSelector<DirT>::ZFaceT ZFFieldT;

    const StringNames& sName = StringNames::self();

    /** \todo these names should not be hard-coded here.
     *     We need to get tags for these from the constructor, or perhaps
     *     through the ODTParams object.
     * dac temporary fix, we check if the tags exist and set em up based
     *     on the bool has_child
     */

    std::string bundleName = select_from_dir( dir_, "_Xbundle", "_Ybundle", "_Zbundle" );

    //pulls fields from FML
    const VFieldT*  xmom = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.xmom      + bundleName, Expr::STATE_N    ) );
    const VFieldT*  ymom = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.ymom      + bundleName, Expr::STATE_N    ) );
    const VFieldT*  zmom = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.zmom      + bundleName, Expr::STATE_N    ) );
    const VFieldT*  ener = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.rhoE0     + bundleName, Expr::STATE_N    ) );
    const VFieldT*  rho  = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.density   + bundleName, Expr::STATE_N    ) );
    const VFieldT*  visc = &fml_.field_ref<VFieldT>(  Expr::Tag(sName_.viscosity + bundleName, Expr::STATE_NONE ) );
    const XFFieldT* taux = ( dir_ == LBMS::XDIR ) ?
        &fml_.field_ref<XFFieldT>( Expr::Tag(sName_.tauxx                       + bundleName, Expr::STATE_NONE ) ) :
        &fml_.field_ref<XFFieldT>( Expr::Tag(sName_.tauxx + sName.reconstructed + bundleName, Expr::STATE_NONE ) ) ;
    const YFFieldT* tauy = ( dir_ == LBMS::YDIR ) ?
        &fml_.field_ref<YFFieldT>( Expr::Tag(sName_.tauyy                       + bundleName, Expr::STATE_NONE ) ) :
        &fml_.field_ref<YFFieldT>( Expr::Tag(sName_.tauyy + sName.reconstructed + bundleName, Expr::STATE_NONE ) ) ;
    const ZFFieldT* tauz = ( dir_ == LBMS::ZDIR ) ?
        &fml_.field_ref<ZFFieldT>( Expr::Tag(sName_.tauzz                       + bundleName, Expr::STATE_NONE ) ) :
        &fml_.field_ref<ZFFieldT>( Expr::Tag(sName_.tauzz + sName.reconstructed + bundleName, Expr::STATE_NONE ) ) ;

    VFieldT* coord = NULL;
    switch(dir_){
    case XDIR: coord = &fml_.field_ref<VFieldT>( Expr::Tag( "X_Vol" + suffix<VFieldT>(), Expr::STATE_NONE ) ); break;
    case YDIR: coord = &fml_.field_ref<VFieldT>( Expr::Tag( "Y_Vol" + suffix<VFieldT>(), Expr::STATE_NONE ) ); break;
    case ZDIR: coord = &fml_.field_ref<VFieldT>( Expr::Tag( "Z_Vol" + suffix<VFieldT>(), Expr::STATE_NONE ) ); break;
      default:{
        std::ostringstream msg;
        msg << std::endl << "Error creating coordinate window for triplet mapping in " << dir_ << std::endl;
        throw std::runtime_error( msg.str() );
      }
    } // switch

    typedef typename LBMS::CoarseFieldSelector<VFieldT>::type SFieldT;

    // create fields associated with line window. Note that these are simply
    // "views" into existing fields, not "new" fields.
    VFieldT  xmomLine       ( get_line_field( *xmom,  location ) );
    VFieldT  ymomLine       ( get_line_field( *ymom,  location ) );
    VFieldT  zmomLine       ( get_line_field( *zmom,  location ) );
    VFieldT  rhoLine        ( get_line_field( *rho,   location ) );
    VFieldT  energyLine     ( get_line_field( *ener,  location ) );
    const VFieldT  coordLine( get_line_field( *coord, location ) );
    const VFieldT  viscLine ( get_line_field( *visc,  location ) );
    const XFFieldT tauxLine ( get_line_field( *taux,  location ) );
    const YFFieldT tauyLine ( get_line_field( *tauy,  location ) );
    const ZFFieldT tauzLine ( get_line_field( *tauz,  location ) );

    std::vector<double> coord_vec( coordLine.begin(), coordLine.end() );

    bool periodic;
    switch(dir_){
    case XDIR: periodic = bundle_->get_boundary_type(XMINUS) == PeriodicBoundary ? true : false; break;
    case YDIR: periodic = bundle_->get_boundary_type(YMINUS) == PeriodicBoundary ? true : false; break;
    case ZDIR: periodic = bundle_->get_boundary_type(ZMINUS) == PeriodicBoundary ? true : false; break;
      default:{
        std::ostringstream msg;
        msg << std::endl << "Error creating coordinate window for triplet mapping in " << dir_ << std::endl;
        throw std::runtime_error( msg.str() );
      }
    } // switch

    //Somewhat redundant if statement (ODT objects aren't created without
    //ODT turned on, so we should never get here), possibly remove to improve speed
    if( odtparams_.implementODT ){

      ODT<DirT> ODTobj( xmomLine, ymomLine, zmomLine, rhoLine, coordLine,
                        viscLine, tauxLine, tauyLine, tauzLine,
                        coord_vec, odtparams_, cutoff_, periodic );
      const double eddylife = ODTobj.return_eddylifetime();

      if( ODTobj.has_eddy() && (eddylife * odtparams_.beta) <= simulationTime ){

        const double eddyRadius = ODTobj.return_length() / 2.0;

        const bool perp1Periodic = Environment::is_periodic( get_perp1(dir_) );

        const double perp1Distance = ( bundle_->glob_spacing( get_perp1(dir_) ) ) * ( location[get_perp1(dir_)] + (bundle_->global_mesh_offset())[get_perp1(dir_)]  + 0.5);
        const double perp1Length = bundle_->glob_length( get_perp1(dir_) );

        if( perp1Periodic || ( (perp1Distance - eddyRadius) > 0.0 && (perp1Distance + eddyRadius) < perp1Length) )
        {
          const bool perp2Periodic = Environment::is_periodic( get_perp2(dir_) );

          const double perp2Distance = ( bundle_->glob_spacing( get_perp2(dir_) ) ) * ( location[get_perp2(dir_)] + (bundle_->global_mesh_offset())[get_perp2(dir_)] + 0.5 );
          const double perp2Length = bundle_->glob_length( get_perp2(dir_) );

          if( perp2Periodic || ((perp2Distance - eddyRadius) > 0.0 && (perp2Distance + eddyRadius) < perp2Length ))
          {
            eddyCount_++;

            const int sindex      = ODTobj.return_sindex();
            const int sindexPrior = ODTobj.return_sindexPrior();
            const int eindex      = ODTobj.return_eindex();

            BOOST_FOREACH( const ConservationInfo& info, conservationInfoVec_ ){
              const VFieldT* field = &fml_.field_ref<VFieldT>( info.transformTag );
              VFieldT lineField ( get_line_field( *field, location ) );


              std::vector<double> before = conservation.integrate_over_coarse( lineField, coordLine, sindexPrior, eindex );
              ODTobj.transform( lineField );
              std::vector<double> after  = conservation.integrate_over_coarse( lineField, coordLine, sindexPrior, eindex );

              //Must use sindex_, not sindexPrior_
              conservation.fill_coarse_field( info.coarseField, info.conservationTag, location, before, after, sindex );
            }

            if( odtparams_.statDump ){
              //Dropping ODT statistics
              WriteODTStats::ODTStats tmpstats;
              tmpstats.sindex    = ODTobj.return_sindex();
              tmpstats.eindex    = ODTobj.return_eindex();
              tmpstats.eddystart = simulationTime;
              tmpstats.eddylife  = ODTobj.return_eddylifetime();
              tmpstats.location  = location + bundle_->global_mesh_offset();
              writer.write_stats(tmpstats, eddyCount_);
            }
          }//end perp2 if
        }//end perp1 if
      }//end if for transformation
    }//end if for ODT object

  }

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  void ODTWindow<DirT>::loop_through( const double simulationTime, LBMS::Conservation<DirT>& conservation ) const
  {
    //iterates through coarse points through non-DirT dimensions and creates line windows
    // NOTE: evaluation and application of triplet mapping occurs in create_window
    IntVec location(0,0,0);

    const Direction dir1 = get_perp1( dir_ );
    const Direction dir2 = get_perp2( dir_ );

    //for dropping statistics
    WriteODTStats statwriter( dir_, odtparams_.jobName );

    for ( size_t idim1=0; idim1!=(extent_[dir1]); ++idim1 ){
      for ( size_t idim2=0; idim2!=(extent_[dir2]); ++idim2 ){
        location[dir1] = idim1;
        location[dir2] = idim2;
        create_window( location, simulationTime, statwriter, conservation );
      } // Perpendicular Lattice Loop 1
    } // Perpendicular Lattice Loop 2

    clear_coarse_fields();

    //field bundle exchange
    BOOST_FOREACH( const ConservationInfo& info, conservationInfoVec_ ){
      ( *(info.sendHelper) )( *(info.coarseField) );
    }
  }

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  void ODTWindow<DirT>::clear_conservation_send_buffers() const
  {
    bool isDoneSending = false;
    while( !isDoneSending ){
      isDoneSending = true;
      BOOST_FOREACH( const ConservationInfo& info, conservationInfoVec_ ){
        if( !(*(info.sendHelper))() ) isDoneSending = false;
      }
    }
  }

  //--------------------------------------------------------------------------------------------

  template< typename DirT >
  void ODTWindow<DirT>::clear_coarse_fields() const
  {
    BOOST_FOREACH( const ConservationInfo& info, conservationInfoVec_ ){
      *(info.coarseField) <<= 0.0;
    }
  }

  //--------------------------------------------------------------------------------------------

  //=================================================
  template class ODTWindow<SpatialOps::XDIR>;
  template class ODTWindow<SpatialOps::YDIR>;
  template class ODTWindow<SpatialOps::ZDIR>;
  //=================================================

} // namespace LBMS
