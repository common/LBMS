/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 * \file Conservation.cpp
 * \date Nov 28, 2012
 * \author Derek Cline
 */

#include "Conservation.h"
#include <mpi/Environment.h>
#include <lbms/Bundle.h>
#include <spatialops/Nebo.h>
#include <operators/InterpCoarseToFine.h>
#include <fields/StringNames.h>

#include <expression/FieldManagerList.h>
#include <expression/FieldManager.h>

using SpatialOps::IntVec;

namespace LBMS{

  //------------------------------Constructor----------------------------------------
  template< typename DirT >
  Conservation<DirT>::Conservation( const std::vector<std::string>& varNames )
  : dir_     ( direction<DirT>()         ),
    bundle_  ( Environment::bundle(dir_) ),
    perp1Dir_( get_perp1(dir_)           ),
    perp2Dir_( get_perp2(dir_)           ),
    nFineInCoarse_( bundle_->npts(dir_) /bundle_->ncoarse(dir_) )
  {
    set_conservation_fields( varNames );
  }


  //-------------------------------Destructor----------------------------------------
  template< typename DirT >
  Conservation<DirT>::~Conservation()
  {}

  //---------------------------------------------------------------------------------

  template< typename DirT >
  std::vector<double> Conservation<DirT>::
  integrate_over_coarse( const VFieldT& field,
                         const VFieldT& coord,
                         const int sindex,
                         const int eindex ) const
  {
    typedef typename VFieldT::const_iterator  VolIter;

    //Finds the first/last fine mesh point within the coarse volume in which the eddy starting/ending location lives
    VolIter coordIter;
    VolIter fieldIter;
    VolIter coarseEddyEnd;

    if( sindex < eindex ){
      coordIter     = coord.begin() + ( floor( sindex / nFineInCoarse_ ) * nFineInCoarse_ );
      fieldIter     = field.begin() + ( floor( sindex / nFineInCoarse_ ) * nFineInCoarse_ );
      coarseEddyEnd = coord.begin() + ( ceil(  ( 1.0 * eindex ) / nFineInCoarse_ ) * nFineInCoarse_ );
    }
    else{
      coordIter     = coord.begin();
      fieldIter     = field.begin();
      coarseEddyEnd = coord.end();
    }
    std::vector<double> integral;

    //Creates an integral on the line field around a triplet mapping
    while( coordIter < coarseEddyEnd){
      double tempIntegral = 0.0;
      for(int j = 0; j != nFineInCoarse_; ++j, ++coordIter, ++fieldIter ){
        tempIntegral += ( (*fieldIter + *(fieldIter+1)) * ( *(coordIter+1) - *coordIter))/2;
      }//Loop across fine points
      integral.push_back(tempIntegral);
    }//Loop across coarse points associated with coarse control volumes

    return integral;
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  void Conservation<DirT>::fill_coarse_field( CoarseFieldT* coarseField,
                                              const Expr::Tag tag,
                                              const IntVec location,
                                              const std::vector<double> before,
                                              const std::vector<double> after,
                                              const int sindex ) const
  {
    //Sanity check
    assert(before.size() == after.size());

    //----- FIX THE LOCATION -----
    //----------------------------
    IntVec tempLocation = location;
    tempLocation[dir_] = sindex; //where the eddy starts

    const IntVec coarseLocation = bundle_->fine_to_coarse( tempLocation );

    CoarseFieldT coarseLine( get_line_field( *coarseField,  location, dir_ ) );

    typename CoarseFieldT::iterator coarseIter = coarseLine.begin();
    for( int i = 0; i != before.size(); ++i, ++coarseIter ){
      *( coarseIter + coarseLocation[dir_] ) = after[i] - before[i];
    }
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  void Conservation<DirT>::apply_conservation( const Expr::Tag perp1CoarseTag,
                                               const Expr::Tag perp2CoarseTag,
                                               const Expr::Tag fieldTag ) const
  {
    Expr::FieldManagerList* fml = bundle_->get_field_manager_list();

    CoarseFieldT* coarsePerp1Field = &fml->field_ref<CoarseFieldT>( perp1CoarseTag );
    CoarseFieldT* coarsePerp2Field = &fml->field_ref<CoarseFieldT>( perp2CoarseTag );

    VFieldT* field = &fml->field_ref<VFieldT>( fieldTag );

    {
      LBMS::CoarseFieldRecvHelper<CoarseFieldT> recv( bundle_, perp1CoarseTag, perp1Dir_ );
      while(!recv.is_ready(*coarsePerp1Field)){}
      recv( *coarsePerp1Field );
    }
    {
      LBMS::CoarseFieldRecvHelper<CoarseFieldT> recv( bundle_, perp2CoarseTag, perp2Dir_ );
      while(!recv.is_ready(*coarsePerp2Field)){}
      recv( *coarsePerp2Field );
    }

    //Scratch field
    SpatialOps::SpatFldPtr<VFieldT> tempField = SpatialOps::SpatialFieldStore::get<VFieldT>( *field );

    const SpatialOps::IntVec extent = bundle_->npts();
          SpatialOps::IntVec location(0,0,0);

    *tempField <<= 0.0;

    using namespace SpatialOps;

    for ( size_t idim1=0; idim1!=( extent[perp1Dir_]); idim1++ ){
      for ( size_t idim2=0; idim2!=( extent[perp2Dir_]); idim2++ ){

        //location for creation of line windows
        location[perp1Dir_] = idim1;
        location[perp2Dir_] = idim2;

        //Line window for fine (high wave number) field and constructed field
        VFieldT fieldLine( get_line_field( *field, location ) );

        //Line window for coarse fields
        const CoarseFieldT coarsePerp1Line( get_line_field( *coarsePerp1Field, location, dir_ ) );
        const CoarseFieldT coarsePerp2Line( get_line_field( *coarsePerp2Field, location, dir_ ) );

        typename CoarseFieldT::const_iterator coarseP1Iter = coarsePerp1Line.begin();
        typename CoarseFieldT::const_iterator coarseP2Iter = coarsePerp2Line.begin();
        typename VFieldT::iterator            fieldIter    =       fieldLine.begin();

        for( ; fieldIter != fieldLine.end(); ++coarseP1Iter, ++coarseP2Iter ){

          for( int k=0 ; k != nFineInCoarse_; ++k, ++fieldIter ){
            *fieldIter = *fieldIter + (*coarseP1Iter + *coarseP2Iter) * ( nFineInCoarse_/2.0 - std::abs( k + 1.0 - (nFineInCoarse_+1.0)/2.0 ) ) / ( (1.0/4.0)*(1.0/nFineInCoarse_ + nFineInCoarse_ ) );
          } //Loop over coarse volume to create a constructed flux
        } //Loop over line
      } //Loop over perp1
    }//Loop over perp2
  }

  template< typename DirT >
  void Conservation<DirT>::set_conservation_fields( const std::vector<std::string>& varNames )
  {
    //Function to register coarse fields required for the conservation class
    //(which live outside of the expression tree) to the required field managers
    using namespace SpatialOps;
    const int nghosts = 1;

    Expr::FieldManagerList* fml = bundle_->get_field_manager_list();

    typedef typename Expr::FieldMgrSelector<CoarseFieldT>::type FieldMgr;

    FieldMgr& fm = fml->template field_manager<CoarseFieldT>();

    const Direction d = direction<DirT>();

    const StringNames& sName = StringNames::self();
    BOOST_FOREACH( const std::string& name, varNames ){
      const Expr::Tag xConservationTag( name.substr( 0, name.size()-8 ) + sName.xcons + get_bundle_name_suffix( d ), Expr::STATE_NONE );
      const Expr::Tag yConservationTag( name.substr( 0, name.size()-8 ) + sName.ycons + get_bundle_name_suffix( d ), Expr::STATE_NONE );
      const Expr::Tag zConservationTag( name.substr( 0, name.size()-8 ) + sName.zcons + get_bundle_name_suffix( d ), Expr::STATE_NONE );

      fm.register_field( xConservationTag, nghosts );
      fm.register_field( yConservationTag, nghosts );
      fm.register_field( zConservationTag, nghosts );
    }
  }

  //=================================================
  template class Conservation<SpatialOps::XDIR>;
  template class Conservation<SpatialOps::YDIR>;
  template class Conservation<SpatialOps::ZDIR>;
  //=================================================

} //namespace LBMS
