/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

/**
 * \file ODT.cpp
 * \date Jul 11, 2012
 * \author Derek Cline
 */

#include "ODT.h"

#include <expression/Expression.h>

namespace LBMS{

  //------------------------------Constructor----------------------------------------
  template< typename DirT >
  ODT<DirT>::ODT( VFieldT& xmom,
                  VFieldT& ymom,
                  VFieldT& zmom,
                  VFieldT& rho,
                  const VFieldT& coord,
                  const VFieldT& visc,
                  const XFieldT& xFlux,
                  const YFieldT& yFlux,
                  const ZFieldT& zFlux,
                  const std::vector<double>& coord_vec,
                  const ODTParameters& params,
                  const double cutoff,
                  const bool periodic )
    : umom_(xmom),
      vmom_(ymom),
      wmom_(zmom),
      rho_(rho),
      coord_(coord),
      kinvisc_(visc),
      xStress_(xFlux),
      yStress_(yFlux),
      zStress_(zFlux),
      lagrangeCoefs_(coord_vec),
      params_(params),
      cutoff_(cutoff),
      periodic_( periodic ),
      gridSpacing_(*(coord_.begin()+1) - *(coord_.begin()) )
  {
    eddy_select();

    if( implementEddy_ ){
      triplet_map( rho_, temprho_ );
      triplet_map( umom_,tempumom_);
      triplet_map( vmom_,tempvmom_);
      triplet_map( wmom_,tempwmom_);
    }

    if( implementEddy_ ){
      assert( sindex_ < coord_.window_without_ghost().glob_npts() );
      assert( eindex_ < coord_.window_without_ghost().glob_npts() );
      assert( sindex_ >= 0 );
      assert( eindex_ >= 0 );
      assert( eindex_ > sindex_ );
      assert( newloc_.size() == size_t(eindex_ - sindex_+1) );

      Kvec_.clear();

      typename VFieldT::const_iterator coorditer = coord_.begin() + sindex_;
      typename VFieldT::const_iterator coorditerend = coord_.begin() + eindex_;
      std::vector<double>::const_iterator newcoorditer = newloc_.begin();

      for( ; coorditer != coorditerend; ++coorditer, ++newcoorditer ){
        Kvec_.push_back(*coorditer - *newcoorditer);
      }
      Kvec_.push_back(0.0);  // jcs why?
    }

    if( implementEddy_ ){
      calculate_kernel();
    }
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  void ODT<DirT>::transform( VFieldT& field, const KernelSelector op )
  {
    if( !implementEddy_ && !params_.test ){
      return;
    }

    if( periodic_ && acrossTheBoundary_ ){
      shift( field );
    }

    typename VFieldT::iterator j = field.begin() +  sindex_;

    if( &field[0] == &rho_[0] ){
      for( size_t i = 0; i < temprho_.size() - 1; i++, j++ ){
        *j = temprho_[i];
      }
    }
    else if( &field[0] == &umom_[0] ){
      for( size_t i = 0; i < tempumom_.size() - 1; i++, j++ ){
        *j = tempumom_[i]+c1_*Kvec_[i];
      }
    }
    else if( &field[0] == &vmom_[0] ){
      for( size_t i = 0; i < tempvmom_.size() - 1; i++, j++ ){
        *j = tempvmom_[i]+c2_*Kvec_[i];
      }
    }
    else if( &field[0] == &wmom_[0] ){
      for( size_t i = 0; i < tempvmom_.size() - 1; i++, j++ ){
        *j = tempwmom_[i]+c3_*Kvec_[i];
      }
    }
    else{
      triplet_map( field );
    }

    if( periodic_ && acrossTheBoundary_ ){
      shift_back( field );
    }
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  void ODT<DirT>::eddy_select()
  {
    implementEddy_ = true;

    typename XFieldT::const_iterator Xstriterst = xStress_.begin();
    typename YFieldT::const_iterator Ystriterst = yStress_.begin();
    typename ZFieldT::const_iterator Zstriterst = zStress_.begin();

    std::vector<int> stressindices;
    int count = 0;

    const Direction d = LBMS::direction<DirT>();

    switch(d)
    {
    case LBMS::XDIR :
      for( int i = 0; Xstriterst != xStress_.end(); ++i, ++Xstriterst){
        if( fabs( *Xstriterst ) >= params_.stresstol ){
          stressindices.push_back( i );
          count++;
        }
      }
    break;
    case LBMS::YDIR :
    for( int i = 0; Ystriterst != yStress_.end(); ++i, ++Ystriterst ){
      if( fabs( *Ystriterst ) >= params_.stresstol ){
        stressindices.push_back( i );
        count++;
      }
    }
    break;
    case LBMS::ZDIR :
    for( int i = 0; Zstriterst != zStress_.end(); ++i, ++Zstriterst ){
      if( fabs( *Zstriterst ) >= params_.stresstol ){
        stressindices.push_back( i );
        count++;
      }
    }
    break;
    case LBMS::NODIR :
      throw std::runtime_error("No direction for ODT window");
    break;
    }

    if( count <= 0 ){
      implementEddy_ = false;
      return;
    }

    const double r1 = double(rand())/double(RAND_MAX);
    const int ran_loc = int(r1*double(stressindices.size()-1));
    y0_ = *(coord_.begin()+stressindices[ran_loc]);

    //scaling analysis is used to determine the eddy length
    const double r2 = double(rand())/double(RAND_MAX);

    const double maxu = field_max(umom_);
    const double maxv = field_max(vmom_);
    const double maxw = field_max(wmom_);
    const double minu = field_min(umom_);
    const double minv = field_min(vmom_);
    const double minw = field_min(wmom_);

    const double maxVel = std::max( std::max( maxu, maxv ), maxw );
    const double minVel = std::min( std::min( minu, minv ), minw );

    const double vel = std::max( maxVel, std::abs(minVel) );

    const double maxvisc = field_max(kinvisc_);
    const double minvisc = field_min(kinvisc_);

    const double viscAvg = (maxvisc+minvisc)/2.0;

    reynolds_ = vel * params_.integLS / viscAvg;

    const double kol_LS = params_.integLS / (pow(reynolds_,0.75));
    maxeddy_ = params_.integLS;
    mineddy_ = 6*kol_LS;
    Lp_ = exp( ( log(params_.integLS ) + log(kol_LS) )/2 );
    factorc_ = 2*Lp_/(exp(-2*Lp_/maxeddy_) - exp(-2*Lp_/mineddy_));
    eddyl_ = -2*Lp_/log((2*Lp_*r2/factorc_) + exp(-2*Lp_/mineddy_));

    //hard codes eddy length and location for testing purposes
    if( params_.test == true ){
      eddyl_ = 0.50;
      y0_ = 0.50;
    }

    // eddy starting location will be the center of the eddy
    y0_ = y0_ - eddyl_/2.0;

    typename VFieldT::const_iterator coord_iterbegin = coord_.begin();
    acrossTheBoundary_ = false;
    y0Prior_ = 0.0;

    if( (y0_+eddyl_) >= (*(coord_.end()-3)) ){
      if( !periodic_ ){
        implementEddy_ = false;
        return;
      }
      else{
         //This is more of a sanity check and to avoid further calculations
         //as our coarse to fine ratio check will always be less than the domain length
        if( (y0_+eddyl_) >= 2*(*(coord_.end()-3)) ){
          implementEddy_ = false;
          return;
        }
        acrossTheBoundary_ = true;
        y0Prior_ = y0_;
        y0_ = 0.0;
      }
    }

    //sets starting index sindex_ to eddy starting location
    for(int i=0;coord_iterbegin!=coord_.end();++coord_iterbegin,i++){
      if(y0_ <= *coord_iterbegin){
        y0_ = *coord_iterbegin;
        sindex_ = i;
        break;
      }
    }

    if( acrossTheBoundary_ ){
      for(int i=0;coord_iterbegin!=coord_.end();++coord_iterbegin,i++){
        if(y0Prior_ <= *coord_iterbegin){
          y0Prior_ = *coord_iterbegin;
          sindexPrior_ = i;
          break;
        }
      }
    }
    else{
      sindexPrior_ = sindex_;
      y0Prior_     = y0_;
    }

    coord_iterbegin = coord_.begin() + sindex_;

    //sets end point index eindex_ to eddy ending location
    for(int i=sindex_; coord_iterbegin!=coord_.end();++coord_iterbegin,++i){
      if((y0_+eddyl_)<= *coord_iterbegin){
        eddyl_ = (*coord_iterbegin)-y0_;
        eindex_ = i;
        tmpoints_ = i+1-sindex_;
        break;
      }
    }

    if(tmpoints_ % 3 ==1){
      eindex_ = eindex_ + 1;
      tmpoints_ += 1;
      eddyl_ = eddyl_ + gridSpacing_;
    }

    if(tmpoints_<6 ){
      implementEddy_ = false;
      return;
    }

    if( std::abs(eindex_ - sindex_) > cutoff_ && !params_.test ){
      implementEddy_ = false;
    }

    if( eindex_ < 1 ){
      implementEddy_ = false;
    }
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  void ODT<DirT>::calculate_kernel()
  {
    const double integA  = custom_integral( Kvec_    );
    const double integB1 = custom_integral( tempumom_);
    const double integB2 = custom_integral( tempvmom_);
    const double integB3 = custom_integral( tempwmom_);

    const double Q1 = pow(integB1,2)/(2*integA);
    const double Q2 = pow(integB2,2)/(2*integA);
    const double Q3 = pow(integB3,2)/(2*integA);

    double visc_sum = 0.0, rho_sum = 0.0;

    typename VFieldT::const_iterator rhostart = rho_.begin() + sindex_ ;
    typename VFieldT::const_iterator rhonext = rho_.begin() + sindex_ + 1 ;
    typename VFieldT::const_iterator rhostop = rho_.begin() + eindex_ ;
    typename VFieldT::const_iterator visstart = kinvisc_.begin() + sindex_ ;
    typename VFieldT::const_iterator visnext = kinvisc_.begin() + sindex_ + 1;

    for( ; rhostart!=rhostop; ++rhostart,++rhonext,++visstart,++visnext){
      visc_sum += 0.5*gridSpacing_*( *visstart +  *visnext );
      rho_sum  += 0.5*gridSpacing_*(*rhostart + *rhonext);
    }

    const double visc_avg = visc_sum/eddyl_;
    const double rho_avg = rho_sum/eddyl_;

    const double sqrtterm = (Q3 + Q2 + Q1)/(rho_avg*eddyl_) - params_.z *pow((visc_avg/eddyl_),2);

    if( sqrtterm < 0 ){
      implementEddy_ = false;
      return;
    }

    //Dynamic Eddy Rate constant
    const double cModel    = params_.cCoefB * pow(reynolds_,params_.cCoefA);
    //C = b Re ^ a, where b = exp(16.9), a = -1.69 and Re = 2000. Below this, we
    //should be laminar, so it does not make sense for C to continue to grow.
    const double cModelMax = params_.cCoefB * pow(2000.0, params_.cCoefA);
    const double cMax      = std::max(cModel,params_.c);
    const double cDynamic  = std::min(cMax,cModelMax);

    const double eddy_rate = (cDynamic/(pow(eddyl_,3)))*sqrt(sqrtterm);
    const double f_l = (factorc_/(eddyl_*eddyl_))*exp(-2*Lp_/eddyl_);
    const double g_l = 1.0/(maxeddy_-mineddy_);
    const double probability = eddy_rate*params_.timestep/(f_l*g_l);

    if(probability>1.0){
      implementEddy_ = false;
      return;
    }

    const double r3 = double(rand())/double(RAND_MAX);
    if(probability < r3 && params_.test == false ){
      implementEddy_ = false;
      return;
    }

    const double sqrtterm1 = sqrt( pow((integB1/integA),2) * (1-params_.alpha)
                                 + pow((integB2/integA),2) * params_.alpha/2
                                 + pow((integB3/integA),2) * params_.alpha/2 );
    const double sqrtterm2 = sqrt( pow((integB2/integA),2) * (1-params_.alpha)
                                 + pow((integB1/integA),2) * params_.alpha/2
                                 + pow((integB3/integA),2) * params_.alpha/2 );
    const double sqrtterm3 = sqrt( pow((integB3/integA),2) * (1-params_.alpha)
                                 + pow((integB1/integA),2) * params_.alpha/2
                                 + pow((integB2/integA),2) * params_.alpha/2 );

    if( std::isnan(sqrtterm1) || std::isnan(sqrtterm2) || std::isnan(sqrtterm3) ){
      implementEddy_ = false;
      return;
    }

    double signsym = (integB1<0.0) ? -1.0 : 1.0;
    c1_ = -(integB1/integA) + signsym* sqrtterm1;
    signsym = (integB2>0)  ? 1.0 : -1.0;
    c2_ = -(integB2/integA) + signsym* sqrtterm2;
    signsym = (integB3>0)  ? 1.0 : -1.0;
    c3_ = -(integB3/integA) + signsym* sqrtterm3;

    eddylifetime_ = eddyl_/sqrt(sqrtterm);
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  double ODT<DirT>::trap_integral( const double* start,
                                   const double* const stop )
  {
    double sum = 0.0;
    const double* fa = start;
    const double* fb = ++start;
    for( ; fa!=stop; ++fa, ++fb ){
      sum += 0.5*gridSpacing_*( *fa+*fb );
    }
    return sum;
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  double ODT<DirT>::field_trap_integral( typename VFieldT::const_iterator start,
                                         typename VFieldT::const_iterator stop )
  {
    double sum = 0.0;
    typename VFieldT::const_iterator fa = start;
    typename VFieldT::const_iterator fb = ++start;
    for( ; fa!=stop; ++fa, ++fb ){
      sum += 0.5*gridSpacing_*( *fa+*fb );
    }
    return sum;
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  double ODT<DirT>::custom_integral( const std::vector<double>& field,
                                     const std::vector<double>& lesrho,
                                     const std::vector<double>& Kles )
  {
    //This form was not implemented in Naveen's code.  Thus, this is never used
    //and remains as an artifact of the original ODT code

    const double* rhostart   = &lesrho[0];
    const double* rhonext    = &lesrho[1];
    const double* rhoend     = &lesrho[lesrho.size()-1];
    const double* Kstart     = &Kles[0];
    const double* Knext      = &Kles[1];
    const double* fieldstart = &field[0];
    const double* fieldnext  = &field[1];

    double integval = 0.0;
    for(;rhostart!=rhoend;++rhostart,++rhonext,++Kstart,++Knext,++fieldstart,++fieldnext){
      integval += gridSpacing_*((*Kstart * *fieldstart/ *rhostart) + (*Knext * *fieldnext/ *rhonext));
    }
    return (0.5*integval);
  }

  //---------------------------------------------------------------------------------

  template< typename DirT >
  double ODT<DirT>::custom_integral( const std::vector<double>& field )
  {
    const double* rhostart = &temprho_[0];
    const double* rhonext  = &temprho_[1];
    const double* rhoend   = &temprho_[temprho_.size()-1];

    const double* Kstart  = &Kvec_[0];
    const double* Knext   = &Kvec_[1];

    const double* fieldstart = &field[0];
    const double* fieldnext  = &field[1];

    double integval = 0.0;
    for(;rhostart!=rhoend;++rhostart,++rhonext,++Kstart,++Knext,++fieldstart,++fieldnext){
      integval += gridSpacing_*((*Kstart * *fieldstart/ *rhostart) + (*Knext * *fieldnext/ *rhonext));
    }
    return (0.5*integval);
  }
  //------------------------------------------------------------------------------



  //-------------------------------calculates kernel for triplet map----------------------------
  template< typename DirT >
  void ODT<DirT>::triplet_map_kernel( const VFieldT& field )
  {
    phif_.clear();
    newloc_.clear();
    phif_.push_back(*(field.begin() + sindex_ ));
    newloc_.push_back(*(coord_.begin() + sindex_));

    typename VFieldT::const_iterator i = (coord_.begin() + sindex_ + 1);

    for( ; i < (coord_.begin() + eindex_);  ++i ){
      double newlocation=0.0;
      if( (*i) >= y0_ && (*i) < (y0_ + eddyl_/3) ){
        newlocation = 3*(*i) - 2*y0_;
        newloc_.push_back(newlocation);
        phif_.push_back(interpolate(newlocation,field));
      }
      else if( (*i) >= (y0_ + eddyl_/3) && (*i) < (y0_ + (2 *eddyl_)/3) ){
        newlocation = 4*y0_ + 2*eddyl_ - 3*(*i);
        newloc_.push_back(newlocation);
        phif_.push_back(interpolate(newlocation,field));
      }
      else if( (*i) >= (y0_ + (2*eddyl_)/3) && ( *i ) <= (y0_ + eddyl_) ){
        newlocation = 3*(*i) - 2*y0_ - 2*eddyl_;
        newloc_.push_back(newlocation);
        phif_.push_back(interpolate(newlocation,field));
      }
    }
    phif_.push_back( *(field.begin() + eindex_) );
    newloc_.push_back( *(coord_.begin() + eindex_) );

    //---------evaluates G vector for triplet map kernel--------
    gvec_.clear();
    for( i = (coord_.begin() + sindex_); i != (coord_.begin() + eindex_); ++i ){
      if( (*i) >= y0_ && (*i) < (y0_ +eddyl_/3) ){
        gvec_.push_back( (3/eddyl_) * ( (*i) - y0_) );
      }
      else if( (*i) >= (y0_ + eddyl_/3) && (*i) < (y0_ + (2*eddyl_)/3) ){
        gvec_.push_back( (-3/eddyl_) * ( (*i) - y0_ - eddyl_/3) + 1 );
      }
      else if( (*i) >= (y0_ + (2*eddyl_)/3 ) && (*i) <= (y0_+eddyl_) ){
        gvec_.push_back( (3/eddyl_) * ( (*i) - y0_ - 2*eddyl_/3) );
      }
    }
    gvec_.push_back(0.0);

    typename VFieldT::const_iterator field_end_iter   = field.begin()  + eindex_;
    typename VFieldT::const_iterator field_begin_iter = field.begin()  + sindex_;

    //------To evaluate the triplet map kernel coefficient----------
    const double phi_integ = field_trap_integral(field_begin_iter, field_end_iter);
    const double phif_integ = trap_integral(&phif_[0],&phif_[phif_.size()-1]);
    const double G_integ = trap_integral(&gvec_[0],&gvec_[gvec_.size()-1]);
    gamma_ = (phi_integ - phif_integ)/G_integ;
  }

  //----------------------------------------------------------------------------------------

  template< typename DirT >
  double ODT<DirT>::interpolate( const double newlocation, const VFieldT& field )
  {
    std::vector<int> indices;
    std::vector<double> coefs;
    lagrangeCoefs_.get_interp_coefs_indices( newlocation, 2, coefs, indices );
    double sum = 0.0;
    typename VFieldT::const_iterator i = field.begin();
    for(size_t j = 0 ; j < coefs.size(); ++j ){
      sum += coefs[j] * *(i + indices[j]);
    }
    return sum;
  }

  //----------------------------------------------------------------------------------------

  template< typename DirT >
  void ODT<DirT>::triplet_map( VFieldT& field, std::vector<double>& tempfield )
  {
    if( periodic_ && acrossTheBoundary_ ) shift( field );
    triplet_map_kernel( field );
    for(size_t i = 0; i < phif_.size(); i++ ){
      tempfield.push_back( phif_[i] + gvec_[i] * gamma_ );
    }
    if( periodic_ && acrossTheBoundary_ ) shift_back( field );
  }

  //---------------------------------------------------------------------------------------

  template< typename DirT >
  void ODT<DirT>::triplet_map( VFieldT& field )
  {
    triplet_map_kernel( field );
    typename VFieldT::iterator j = field.begin() + sindex_;
    for( size_t i = 0; i < ( phif_.size()); ++j, ++i ){
      *j = phif_[i] + gvec_[i] * gamma_;
    }
  }

  //---------------------------------------------------------------------------------------

  //---------------------------------------------------------------------------------------
  template< typename DirT >
  void ODT<DirT>::shift( VFieldT& field )
  {
    assert( y0Prior_ > gridSpacing_ );

    std::vector<double> tempField;
    for( typename VFieldT::iterator j=field.begin(); j!=field.end(); j++ ){
      tempField.push_back(*j);
    }

    std::vector<double>::iterator tempIter = tempField.begin();
    typename VFieldT::iterator   fieldIter = field.begin();
    const int distFromBoundary = coord_.window_with_ghost().local_npts() - sindexPrior_;

    for( size_t i = 0; i!=distFromBoundary; ++i, ++fieldIter, ++tempIter ){
      *fieldIter = *(tempIter+sindexPrior_);
    }
    //reset temp field
    tempIter = tempField.begin();
    for( size_t i = distFromBoundary; fieldIter!=field.end(); ++i, ++fieldIter, ++tempIter ){
      *fieldIter = *tempIter;
    }
  }
  //---------------------------------------------------------------------------------------

  //---------------------------------------------------------------------------------------
  template< typename DirT >
  void ODT<DirT>::shift_back( VFieldT& field )
  {
    assert( y0Prior_ > gridSpacing_ );

    std::vector<double> tempField;
    for( typename VFieldT::iterator j=field.begin(); j!=field.end(); j++ ){
      tempField.push_back(*j);
    }

    std::vector<double>::iterator tempIter = tempField.begin();
    typename VFieldT::iterator   fieldIter = field.begin();
    const int distFromBoundary = coord_.window_with_ghost().local_npts() - sindexPrior_;

    for( size_t i = 0; i!=sindexPrior_; ++i, ++fieldIter, ++tempIter ){
      *fieldIter = *(tempIter+distFromBoundary);
    }
    //reset temp field
    tempIter = tempField.begin();
    for( size_t i = sindexPrior_; fieldIter!=field.end(); ++i, ++fieldIter, ++tempIter ){
      *fieldIter = *tempIter;
    }
  }
  //---------------------------------------------------------------------------------------

  //=================================================
  template class ODT<SpatialOps::XDIR>;
  template class ODT<SpatialOps::YDIR>;
  template class ODT<SpatialOps::ZDIR>;
  //=================================================

} // namespace LBMS
