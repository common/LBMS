/*
 * ODT_test.cpp
 *
 *  Created on: Jun 27, 2012
 *      Author: Derek Cline
 */

// dcs TODO

#include <fields/Checkpoint.h>
#include <fields/Fields.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <mpi/Environment.h>
#include <odt/ODTWindow.h>
//#include <odt/ODTconservation.h>

#include <test/TestHelper.h>
#include <boost/program_options.hpp>
#include <spatialops/FieldFunctions.h>
#include <expression/ExprLib.h>

namespace po=boost::program_options;
using namespace LBMS;
using namespace std;
using SpatialOps::IntVec;

//used for comparing integral values before and after triplet mapping
bool compare_field_integrals( std::vector<double> before, std::vector<double> after ){
  for(int i=0; i!=before.size()-1; i++){
    //1.0e-10 check for machine precision + integral error
    if( abs(before[i] - after[i]) > 1.0e-10 ) return 0;
  }
  return 1;
}

//--------------------------------------------------------------------------------

template< typename VolFieldT>
bool check_differences( const VolFieldT& f,
                        IntVec extent,
                        int Direc,
                        double slope,
                        double intercept )
{
  //Checks to ensure that the field underwent a triplet mapping
  //i.e. something actually changed the field
  IntVec location(0,0,0);

  Direction dir0 = Direction(Direc);
  Direction dir1 = get_perp1(dir0);
  Direction dir2 = get_perp2(dir0);

  double temp = 0.0;
  bool change = false;

  for ( size_t idim1=0; idim1!=(extent[dir1]); ++idim1 ){
    for ( size_t idim2=0; idim2!=(extent[dir2]); ++idim2 ){
      location[dir1] = idim1;
      location[dir2] = idim2;

      const VolFieldT fWindow( get_line_field( f, location ));

      typename VolFieldT::const_iterator fi = fWindow.begin();

      for( int i=1; fi !=fWindow.end(); fi++, i++ ){
        temp = intercept + i*slope;
        if( (temp - *fi) > 2.0e-4 ) change = true;
      } //Loop across memory window
    } // Perpendicular Lattice Loop 1
  } // Perpendicular Lattice Loop 2

  if( change ) return 1;
  else         return 0;
}

//--------------------------------------------------------------------------------

template< typename VolFieldT>
std::vector<double>
calculate_integral( const VolFieldT& field,
                    const VolFieldT& coord,
                    IntVec extent,
                    int Direc )
{
  //-----------
  //integral over each field before the triplet mapping procedure
  std::vector<double> integral;
  IntVec location(0,0,0);

  Direction dir0 = Direction(Direc);
  Direction dir1 = get_perp1(dir0);
  Direction dir2 = get_perp2(dir0);

  for ( size_t idim1=0; idim1!=(extent[dir1]); ++idim1 ){
    for ( size_t idim2=0; idim2!=(extent[dir2]); ++idim2 ){
      location[dir1] = idim1;
      location[dir2] = idim2;

      const VolFieldT fieldWindow( get_line_field( field, location ));
      const VolFieldT coordWindow( get_line_field( coord, location ));

      typename VolFieldT::const_iterator coord_iter = coordWindow.begin();
      typename VolFieldT::const_iterator field_iter = fieldWindow.begin();
      double tempinteg = 0.0;

      for(; coord_iter !=coordWindow.end()-1; coord_iter++,field_iter++){
        tempinteg += ( (*field_iter + *(field_iter+1)) * ( *(coord_iter+1) - *coord_iter))/2;
      } //Loop across memory window
      integral.push_back(tempinteg);
    } // Perpendicular Lattice Loop 1
  } // Perpendicular Lattice Loop 2

  return integral;
}

//--------------------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  const int nghost = 1;
  TestHelper status;
  IntVec nCoarse(2,2,2);
  IntVec nFine(5,5,5);
  Coordinate length(1,1,1);
  std::string name;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nFine[0])->default_value(60), "Fine grid in x" )
      ( "ny", po::value<int>(&nFine[1])->default_value(50), "Fine grid in y" )
      ( "nz", po::value<int>(&nFine[2])->default_value(50), "Fine grid in z" )
      ( "Nx", po::value<int>(&nCoarse[0])->default_value(15),"Coarse grid in x")
      ( "Ny", po::value<int>(&nCoarse[1])->default_value(10),"Coarse grid in y")
      ( "Nz", po::value<int>(&nCoarse[2])->default_value(10),"Coarse grid in z")
      ( "Lx", po::value<double>(&length[0])->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1),"Domain length in z")
      ( "name", po::value<std::string>(&name)->default_value(""),"checkpoint dir tag");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }// end scope

  ODTParameters odtparams = fill_ODT_params();

  const double simutime = 500.0;
  {
    const LBMS::MeshPtr mesh( new LBMS::Mesh( nCoarse, nFine, length ) );
    Environment::setup(iarg,carg);
    Environment::set_topology(mesh);
  }
  const BundlePtr xbundle = Environment::bundle(XDIR);
  const BundlePtr ybundle = Environment::bundle(YDIR);
  const BundlePtr zbundle = Environment::bundle(ZDIR);

  BundlePtrVec bundles;
  bundles.push_back( xbundle );
  bundles.push_back( ybundle );
  bundles.push_back( zbundle );

  Expr::FMLMap fmls;

  try{
    BOOST_FOREACH( BundlePtr b, bundles ){ fmls[b->id()] = new Expr::FieldManagerList(); }

    Expr::FieldManagerList& fmlx = *(bundles[0])->get_field_manager_list();
    Expr::FieldManagerList& fmly = *(bundles[1])->get_field_manager_list();
    Expr::FieldManagerList& fmlz = *(bundles[2])->get_field_manager_list();

    typedef Expr::FieldManagerList::FMPtr FMPtr;
    FMPtr coarseVFM = FMPtr( new Expr::LBMSFieldManager<SpatialOps::SVolField>() );

    fmlx.set_field_manager<SpatialOps::SVolField>(coarseVFM);
    fmly.set_field_manager<SpatialOps::SVolField>(coarseVFM);
    fmlz.set_field_manager<SpatialOps::SVolField>(coarseVFM);

    Expr::TagList xtags, ytags, ztags;
    Expr::TagSet tags;

    Expr::FieldMgrSelector<XVolField>::type& xvolfm = fmlx.field_manager<XVolField>();
    Expr::FieldMgrSelector<YVolField>::type& yvolfm = fmly.field_manager<YVolField>();
    Expr::FieldMgrSelector<ZVolField>::type& zvolfm = fmlz.field_manager<ZVolField>();

    Expr::FieldMgrSelector<XSurfXField>::type& xsurfxfm = fmlx.field_manager<XSurfXField>();
    Expr::FieldMgrSelector<XSurfYField>::type& xsurfyfm = fmlx.field_manager<XSurfYField>();
    Expr::FieldMgrSelector<XSurfZField>::type& xsurfzfm = fmlx.field_manager<XSurfZField>();

    Expr::FieldMgrSelector<YSurfXField>::type& ysurfxfm = fmly.field_manager<YSurfXField>();
    Expr::FieldMgrSelector<YSurfYField>::type& ysurfyfm = fmly.field_manager<YSurfYField>();
    Expr::FieldMgrSelector<YSurfZField>::type& ysurfzfm = fmly.field_manager<YSurfZField>();

    Expr::FieldMgrSelector<ZSurfXField>::type& zsurfxfm = fmlz.field_manager<ZSurfXField>();
    Expr::FieldMgrSelector<ZSurfYField>::type& zsurfyfm = fmlz.field_manager<ZSurfYField>();
    Expr::FieldMgrSelector<ZSurfZField>::type& zsurfzfm = fmlz.field_manager<ZSurfZField>();

    tags.clear(); xtags.clear(); ytags.clear(); ztags.clear();

    const Expr::Tag xxmomt   ( "x_momentum_Xbundle",          Expr::STATE_N );
    const Expr::Tag xymomt   ( "y_momentum_Xbundle",          Expr::STATE_N );
    const Expr::Tag xzmomt   ( "z_momentum_Xbundle",          Expr::STATE_N );
    const Expr::Tag xrhot    ( "density_Xbundle",             Expr::STATE_N );
    const Expr::Tag xenergyt ( "rhoE0_Xbundle",               Expr::STATE_N );
    const Expr::Tag xcoordxt ( "X_Vol" + suffix<XVolField>(), Expr::STATE_NONE );
    const Expr::Tag xcoordyt ( "Y_Vol" + suffix<XVolField>(), Expr::STATE_NONE );
    const Expr::Tag xcoordzt ( "Z_Vol" + suffix<XVolField>(), Expr::STATE_NONE );
    const Expr::Tag xvisct   ( "viscosity_Xbundle",           Expr::STATE_NONE );
    const Expr::Tag xtaux    ( "tauxx_Xbundle",               Expr::STATE_NONE );
    const Expr::Tag xtauy    ( "tauyy_reconstructed_Xbundle", Expr::STATE_NONE );
    const Expr::Tag xtauz    ( "tauzz_reconstructed_Xbundle", Expr::STATE_NONE );

    const Expr::Tag yxmomt   ( "x_momentum_Ybundle",          Expr::STATE_N );
    const Expr::Tag yymomt   ( "y_momentum_Ybundle",          Expr::STATE_N );
    const Expr::Tag yzmomt   ( "z_momentum_Ybundle",          Expr::STATE_N );
    const Expr::Tag yrhot    ( "density_Ybundle",             Expr::STATE_N );
    const Expr::Tag yenergyt ( "rhoE0_Ybundle",               Expr::STATE_N );
    const Expr::Tag ycoordxt ( "X_Vol" + suffix<YVolField>(), Expr::STATE_NONE );
    const Expr::Tag ycoordyt ( "Y_Vol" + suffix<YVolField>(), Expr::STATE_NONE );
    const Expr::Tag ycoordzt ( "Z_Vol" + suffix<YVolField>(), Expr::STATE_NONE );
    const Expr::Tag yvisct   ( "viscosity_Ybundle",           Expr::STATE_NONE );
    const Expr::Tag ytaux    ( "tauxx_reconstructed_Ybundle", Expr::STATE_NONE );
    const Expr::Tag ytauy    ( "tauyy_Ybundle",               Expr::STATE_NONE );
    const Expr::Tag ytauz    ( "tauzz_reconstructed_Ybundle", Expr::STATE_NONE );

    const Expr::Tag zxmomt   ( "x_momentum_Zbundle",          Expr::STATE_N );
    const Expr::Tag zymomt   ( "y_momentum_Zbundle",          Expr::STATE_N );
    const Expr::Tag zzmomt   ( "z_momentum_Zbundle",          Expr::STATE_N );
    const Expr::Tag zrhot    ( "density_Zbundle",             Expr::STATE_N );
    const Expr::Tag zenergyt ( "rhoE0_Zbundle",               Expr::STATE_N );
    const Expr::Tag zcoordxt ( "X_Vol" + suffix<ZVolField>(), Expr::STATE_NONE );
    const Expr::Tag zcoordyt ( "Y_Vol" + suffix<ZVolField>(), Expr::STATE_NONE );
    const Expr::Tag zcoordzt ( "Z_Vol" + suffix<ZVolField>(), Expr::STATE_NONE );
    const Expr::Tag zvisct   ( "viscosity_Zbundle",           Expr::STATE_NONE );
    const Expr::Tag ztaux    ( "tauxx_reconstructed_Zbundle", Expr::STATE_NONE );
    const Expr::Tag ztauy    ( "tauyy_reconstructed_Zbundle", Expr::STATE_NONE );
    const Expr::Tag ztauz    ( "tauzz_Zbundle",               Expr::STATE_NONE );

    xtags.push_back(xxmomt);   xtags.push_back(xymomt);   xtags.push_back(xzmomt);
    xtags.push_back(xrhot);    xtags.push_back(xenergyt); xtags.push_back(xcoordxt);
    xtags.push_back(xcoordyt); xtags.push_back(xcoordzt); xtags.push_back(xvisct);

    ytags.push_back(yxmomt);   ytags.push_back(yymomt);   ytags.push_back(yzmomt);
    ytags.push_back(yrhot);    ytags.push_back(yenergyt); ytags.push_back(ycoordxt);
    ytags.push_back(ycoordyt); ytags.push_back(ycoordzt); ytags.push_back(yvisct);

    ztags.push_back(zxmomt);   ztags.push_back(zymomt);   ztags.push_back(zzmomt);
    ztags.push_back(zrhot);    ztags.push_back(zenergyt); ztags.push_back(zcoordxt);
    ztags.push_back(zcoordyt); ztags.push_back(zcoordzt); ztags.push_back(zvisct);

    // registers all fields that are pushed onto the voltag list
    for( Expr::TagList::const_iterator it=xtags.begin(); it!=xtags.end(); ++it ){
      xvolfm.register_field( *it, nghost );
      tags.insert( *it );
    }
    for( Expr::TagList::const_iterator it=ytags.begin(); it!=ytags.end(); ++it ){
      yvolfm.register_field( *it, nghost );
      tags.insert( *it );
    }
    for( Expr::TagList::const_iterator it=ztags.begin(); it!=ztags.end(); ++it ){
      zvolfm.register_field( *it, nghost );
      tags.insert( *it );
    }

    xsurfxfm.register_field( xtaux, nghost );
    xsurfyfm.register_field( xtauy, nghost );
    xsurfzfm.register_field( xtauz, nghost );

    ysurfxfm.register_field( ytaux, nghost );
    ysurfyfm.register_field( ytauy, nghost );
    ysurfzfm.register_field( ytauz, nghost );

    zsurfxfm.register_field( ztaux, nghost );
    zsurfyfm.register_field( ztauy, nghost );
    zsurfzfm.register_field( ztauz, nghost );

    xvolfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    yvolfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    zvolfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );

    xsurfxfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    xsurfyfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    xsurfzfm.allocate_fields( boost::cref( xbundle->get_field_info() ) );

    ysurfxfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    ysurfyfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    ysurfzfm.allocate_fields( boost::cref( ybundle->get_field_info() ) );

    zsurfxfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );
    zsurfyfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );
    zsurfzfm.allocate_fields( boost::cref( zbundle->get_field_info() ) );

    std::vector<std::string> varNames;
    varNames.push_back("x_momentum_Xbundle"); varNames.push_back("x_momentum_Ybundle"); varNames.push_back("x_momentum_Zbundle");
    varNames.push_back("y_momentum_Xbundle"); varNames.push_back("y_momentum_Ybundle"); varNames.push_back("y_momentum_Zbundle");
    varNames.push_back("z_momentum_Xbundle"); varNames.push_back("z_momentum_Ybundle"); varNames.push_back("z_momentum_Zbundle");
    varNames.push_back("density_Xbundle"   ); varNames.push_back("density_Ybundle"   ); varNames.push_back("density_Zbundle"   );
    varNames.push_back("rhoE0_Xbundle"     ); varNames.push_back("rhoE0_Ybundle"     ); varNames.push_back("rhoE0_Zbundle"     );

    Conservation<SpatialOps::XDIR> xCons( varNames );
    Conservation<SpatialOps::YDIR> yCons( varNames );
    Conservation<SpatialOps::ZDIR> zCons( varNames );

    typedef Expr::FieldMgrSelector<SpatialOps::SVolField>::type FieldMgr;

    FieldMgr& fmXS = fmlx.field_manager<SpatialOps::SVolField>();
    FieldMgr& fmYS = fmly.field_manager<SpatialOps::SVolField>();
    FieldMgr& fmZS = fmlz.field_manager<SpatialOps::SVolField>();
    fmXS.allocate_fields( boost::cref( xbundle->get_field_info() ) );
    fmYS.allocate_fields( boost::cref( ybundle->get_field_info() ) );
    fmZS.allocate_fields( boost::cref( zbundle->get_field_info() ) );

    XVolField& xxmom = xvolfm.field_ref(xxmomt);
    XVolField& xymom = xvolfm.field_ref(xymomt);
    XVolField& xzmom = xvolfm.field_ref(xzmomt);
    XVolField& xener = xvolfm.field_ref(xenergyt);
    XVolField& xrho  = xvolfm.field_ref(xrhot);
    XVolField& xvisc = xvolfm.field_ref(xvisct);

    YVolField& yxmom = yvolfm.field_ref(yxmomt);
    YVolField& yymom = yvolfm.field_ref(yymomt);
    YVolField& yzmom = yvolfm.field_ref(yzmomt);
    YVolField& yener = yvolfm.field_ref(yenergyt);
    YVolField& yrho  = yvolfm.field_ref(yrhot);
    YVolField& yvisc = yvolfm.field_ref(yvisct);

    ZVolField& zxmom = zvolfm.field_ref(zxmomt);
    ZVolField& zymom = zvolfm.field_ref(zymomt);
    ZVolField& zzmom = zvolfm.field_ref(zzmomt);
    ZVolField& zener = zvolfm.field_ref(zenergyt);
    ZVolField& zrho  = zvolfm.field_ref(zrhot);
    ZVolField& zvisc = zvolfm.field_ref(zvisct);

    XVolField& xcoordonx = xvolfm.field_ref(xcoordxt);
    XVolField& ycoordonx = xvolfm.field_ref(xcoordyt);
    XVolField& zcoordonx = xvolfm.field_ref(xcoordzt);

    YVolField& xcoordony = yvolfm.field_ref(ycoordxt);
    YVolField& ycoordony = yvolfm.field_ref(ycoordyt);
    YVolField& zcoordony = yvolfm.field_ref(ycoordzt);

    ZVolField& xcoordonz = zvolfm.field_ref(zcoordxt);
    ZVolField& ycoordonz = zvolfm.field_ref(zcoordyt);
    ZVolField& zcoordonz = zvolfm.field_ref(zcoordzt);

    LBMS::set_coord_values( xcoordonx, ycoordonx, zcoordonx, xbundle );
    LBMS::set_coord_values( xcoordony, ycoordony, zcoordony, ybundle );
    LBMS::set_coord_values( xcoordonz, ycoordonz, zcoordonz, zbundle );

    SpatialOps::FieldFunction1D<XVolField>* xlinear_function;
    xlinear_function = new SpatialOps::LinearFunction1D<XVolField>( xcoordonx, 12.0, 20.0 );
    xlinear_function->evaluate( xxmom );
    xlinear_function->evaluate( xymom );
    xlinear_function->evaluate( xzmom );
    xlinear_function->evaluate( xener );
    xlinear_function->evaluate( xvisc );

    //Necessary or the kernel will return garbage, see Sutherland, Punati 2009, B.63
    //if rho == rhomom then rhomom/rho = 1, which causes the integral to return zero,
    //giving us garbage.  Thus, a different linear function is necessary for rho.
    SpatialOps::FieldFunction1D<XVolField>* twoxlinear_function;
    twoxlinear_function = new SpatialOps::LinearFunction1D<XVolField>( xcoordonx, 14.0, 10.0 );
    twoxlinear_function->evaluate( xrho );

    IntVec xextent (nFine[0], nCoarse[1], nCoarse[2]);

    SpatialOps::FieldFunction1D<YVolField>* ylinear_function;
    ylinear_function = new SpatialOps::LinearFunction1D<YVolField>( ycoordony, 10.0, 20.0 );
    ylinear_function->evaluate( yxmom );
    ylinear_function->evaluate( yymom );
    ylinear_function->evaluate( yzmom );
    ylinear_function->evaluate( yener );
    ylinear_function->evaluate( yvisc );

    // see above
    SpatialOps::FieldFunction1D<YVolField>* twoylinear_function;
    twoylinear_function = new SpatialOps::LinearFunction1D<YVolField>( ycoordony, 14.0, 10.0 );
    twoylinear_function->evaluate( yrho );

    IntVec yextent (nCoarse[0], nFine[1], nCoarse[2]);

    SpatialOps::FieldFunction1D<ZVolField>* zlinear_function;
    zlinear_function = new SpatialOps::LinearFunction1D<ZVolField>( zcoordonz, 10.0, 20.0 );
    zlinear_function->evaluate( zxmom );
    zlinear_function->evaluate( zymom );
    zlinear_function->evaluate( zzmom );
    zlinear_function->evaluate( zener );
    zlinear_function->evaluate( zvisc );

    // see above
    SpatialOps::FieldFunction1D<ZVolField>* twozlinear_function;
    twozlinear_function = new SpatialOps::LinearFunction1D<ZVolField>( zcoordonz, 14.0, 10.0 );
    twozlinear_function->evaluate( zrho );

    IntVec zextent (nCoarse[0], nCoarse[1], nFine[2]);

    //-----------
    //integral over each field before the triplet mapping procedure

    std::vector<double> xxmomintegbefore, xymomintegbefore, xzmomintegbefore, xenerintegbefore, xrhointegbefore;
    std::vector<double> yxmomintegbefore, yymomintegbefore, yzmomintegbefore, yenerintegbefore, yrhointegbefore;
    std::vector<double> zxmomintegbefore, zymomintegbefore, zzmomintegbefore, zenerintegbefore, zrhointegbefore;

    xxmomintegbefore = calculate_integral<XVolField>(xxmom, xcoordonx, xextent, 0);
    xymomintegbefore = calculate_integral<XVolField>(xymom, xcoordonx, xextent, 0);
    xzmomintegbefore = calculate_integral<XVolField>(xzmom, xcoordonx, xextent, 0);
    xenerintegbefore = calculate_integral<XVolField>(xener, xcoordonx, xextent, 0);
    xrhointegbefore  = calculate_integral<XVolField>(xrho,  xcoordonx, xextent, 0);

    yxmomintegbefore = calculate_integral<YVolField>(yxmom, ycoordony, yextent, 1);
    yymomintegbefore = calculate_integral<YVolField>(yymom, ycoordony, yextent, 1);
    yzmomintegbefore = calculate_integral<YVolField>(yzmom, ycoordony, yextent, 1);
    yenerintegbefore = calculate_integral<YVolField>(yener, ycoordony, yextent, 1);
    yrhointegbefore  = calculate_integral<YVolField>(yrho,  ycoordony, yextent, 1);

    zxmomintegbefore = calculate_integral<ZVolField>(zxmom, zcoordonz, zextent, 2);
    zymomintegbefore = calculate_integral<ZVolField>(zymom, zcoordonz, zextent, 2);
    zzmomintegbefore = calculate_integral<ZVolField>(zzmom, zcoordonz, zextent, 2);
    zenerintegbefore = calculate_integral<ZVolField>(zener, zcoordonz, zextent, 2);
    zrhointegbefore  = calculate_integral<ZVolField>(zrho,  zcoordonz, zextent, 2);

    //______________
    // visualization
    LBMS::Checkpoint visualization( bundles, tags, fmls );
    visualization.write_checkpoint( "odt_triplet_before" );

    ODTWindow<SpatialOps::XDIR> XODT( bundles[0], odtparams, varNames );
    ODTWindow<SpatialOps::YDIR> YODT( bundles[1], odtparams, varNames );
    ODTWindow<SpatialOps::ZDIR> ZODT( bundles[2], odtparams, varNames );

    //----------------------------------
    //Actual triplet mapping and coarse volume conservation occurs here
    //----------------------------------
    XODT.loop_through( simutime, xCons );
    YODT.loop_through( simutime, yCons );
    ZODT.loop_through( simutime, zCons );
    //----------------------------------

    std::vector<double> xxmomintegafter, xymomintegafter, xzmomintegafter, xenerintegafter, xrhointegafter;
    std::vector<double> yxmomintegafter, yymomintegafter, yzmomintegafter, yenerintegafter, yrhointegafter;
    std::vector<double> zxmomintegafter, zymomintegafter, zzmomintegafter, zenerintegafter, zrhointegafter;

    //-----------
    //integration over each field after the triplet mapping procedure
    xxmomintegafter = calculate_integral<XVolField>(xxmom, xcoordonx, xextent, 0);
    xymomintegafter = calculate_integral<XVolField>(xymom, xcoordonx, xextent, 0);
    xzmomintegafter = calculate_integral<XVolField>(xzmom, xcoordonx, xextent, 0);
    xenerintegafter = calculate_integral<XVolField>(xener, xcoordonx, xextent, 0);
    xrhointegafter  = calculate_integral<XVolField>(xrho,  xcoordonx, xextent, 0);

    yxmomintegafter = calculate_integral<YVolField>(yxmom, ycoordony, yextent, 1);
    yymomintegafter = calculate_integral<YVolField>(yymom, ycoordony, yextent, 1);
    yzmomintegafter = calculate_integral<YVolField>(yzmom, ycoordony, yextent, 1);
    yenerintegafter = calculate_integral<YVolField>(yener, ycoordony, yextent, 1);
    yrhointegafter  = calculate_integral<YVolField>(yrho,  ycoordony, yextent, 1);

    zxmomintegafter = calculate_integral<ZVolField>(zxmom, zcoordonz, zextent, 2);
    zymomintegafter = calculate_integral<ZVolField>(zymom, zcoordonz, zextent, 2);
    zzmomintegafter = calculate_integral<ZVolField>(zzmom, zcoordonz, zextent, 2);
    zenerintegafter = calculate_integral<ZVolField>(zener, zcoordonz, zextent, 2);
    zrhointegafter  = calculate_integral<ZVolField>(zrho,  zcoordonz, zextent, 2);

    //Test
    status( compare_field_integrals(xxmomintegbefore, xxmomintegafter) );
    status( compare_field_integrals(xymomintegbefore, xymomintegafter) );
    status( compare_field_integrals(xzmomintegbefore, xzmomintegafter) );
    status( compare_field_integrals(xenerintegbefore, xenerintegafter) );
    status( compare_field_integrals(xrhointegbefore, xrhointegafter) );

    status( compare_field_integrals(yxmomintegbefore, yxmomintegafter) );
    status( compare_field_integrals(yymomintegbefore, yymomintegafter) );
    status( compare_field_integrals(yzmomintegbefore, yzmomintegafter) );
    status( compare_field_integrals(yenerintegbefore, yenerintegafter) );
    status( compare_field_integrals(yrhointegbefore, yrhointegafter) );

    status( compare_field_integrals(zxmomintegbefore, zxmomintegafter) );
    status( compare_field_integrals(zymomintegbefore, zymomintegafter) );
    status( compare_field_integrals(zzmomintegbefore, zzmomintegafter) );
    status( compare_field_integrals(zenerintegbefore, zenerintegafter) );
    status( compare_field_integrals(zrhointegbefore, zrhointegafter) );

    status( check_differences<XVolField>(xxmom, xextent, 0, 0.1,           20) );
    status( check_differences<XVolField>(xymom, xextent, 0, 0.1,           20) );
    status( check_differences<XVolField>(xzmom, xextent, 0, 0.1,           20) );
    status( check_differences<XVolField>(xener, xextent, 0, 0.1,           20) );
    status( check_differences<XVolField>(xrho,  xextent, 0, 0.11666666667, 10) );

    status( check_differences<YVolField>(yxmom, yextent, 1, 0.1,  20) );
    status( check_differences<YVolField>(yymom, yextent, 1, 0.1,  20) );
    status( check_differences<YVolField>(yzmom, yextent, 1, 0.1,  20) );
    status( check_differences<YVolField>(yener, yextent, 1, 0.1,  20) );
    status( check_differences<YVolField>(yrho,  yextent, 1, 0.14, 10) );

    status( check_differences<ZVolField>(zxmom, zextent, 2, 0.1,  20) );
    status( check_differences<ZVolField>(zymom, zextent, 2, 0.1,  20) );
    status( check_differences<ZVolField>(zzmom, zextent, 2, 0.1,  20) );
    status( check_differences<ZVolField>(zener, zextent, 2, 0.1,  20) );
    status( check_differences<ZVolField>(zrho,  zextent, 2, 0.14, 10) );

    //Check to ensure conservation is upheld
    const double precision = 1000 * std::numeric_limits<double>::epsilon();

    double beforeCons = SpatialOps::nebo_sum_interior( xxmom );
    xCons.apply_conservation( Expr::Tag("x_momentum_yConservation_Xbundle", Expr::STATE_NONE),
                              Expr::Tag("x_momentum_zConservation_Xbundle", Expr::STATE_NONE),
                              xxmomt );
    double afterCons = SpatialOps::nebo_sum_interior( xxmom );
    status( abs(beforeCons - afterCons) < precision, "X Conservation" );

    beforeCons = SpatialOps::nebo_sum_interior( yymom );
    yCons.apply_conservation( Expr::Tag("y_momentum_xConservation_Ybundle", Expr::STATE_NONE),
                              Expr::Tag("y_momentum_zConservation_Ybundle", Expr::STATE_NONE),
                              yymomt );
    afterCons = SpatialOps::nebo_sum_interior( yymom );
    status( abs(beforeCons - afterCons) < precision, "Y Conservation" );

    beforeCons = SpatialOps::nebo_sum_interior( zzmom );
    zCons.apply_conservation( Expr::Tag("z_momentum_xConservation_Zbundle", Expr::STATE_NONE),
                              Expr::Tag("z_momentum_yConservation_Zbundle", Expr::STATE_NONE),
                              zzmomt );
    afterCons = SpatialOps::nebo_sum_interior( zzmom );
    status( abs(beforeCons - afterCons) < precision, "Z Conservation" );


    visualization.write_checkpoint( "odt_triplet_after" );
    if( status.ok() ){
      return 0; // for successful exit.
    }
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  return -1;
}
