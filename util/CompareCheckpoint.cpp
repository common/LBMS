/*
 * The MIT License
 *
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   CompareCheckpoint.cpp
 *  \date   May 30, 2013
 *  \author "James C. Sutherland"
 */

#include <lbms/Mesh.h>
#include <fields/Checkpoint.h>
#include <lbms/Bundle.h>
#include <test/TestHelper.h>
#include <parser/ParseGroup.h>
using namespace LBMS;

#include <expression/ExprLib.h>

#include <vector>
#include <string>
#include <stdexcept>
#include <sstream>
#include <limits>
#include <cmath>
using std::vector;
using std::string;
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po=boost::program_options;

#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/math/special_functions/round.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
namespace bpt=boost::property_tree;

#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace bfs = boost::filesystem;

#include <boost/foreach.hpp>

typedef vector<Checkpoint::FieldMetaData > FMDs;
typedef vector<Checkpoint::BundleMetaData> BMDs;

using SpatialOps::IntVec;

//-------------------------------------------------------------------
inline double fround( const double n, const unsigned d )
{
  if(std::abs(n)<=std::numeric_limits<double>::epsilon()) return 0.0;
  double factor =  std::pow(10.,(int) d - std::ceil(std::log10(std::fabs(n))));
  return (boost::math::round(n*factor)/factor);
}

template<typename DirT>
bool
extract_metadata( const string& fnam,
                  Checkpoint::BundleMetaData& bundleData,
                  FMDs& fieldData )
{
  fieldData.clear();

  const Direction dir = direction<DirT>();
  const string dirName = get_dir_name(dir);
  const string filename = fnam + "/" + dirName + "_FieldInfo.xml";
  bpt::ptree pt;

  if(boost::filesystem::exists( filename )){
    bpt::read_xml( filename, pt );
    bpt::ptree& bundleTree = pt.get_child("info.bundle");
    bundleData.read_ptree( bundleTree );

    if( direction(bundleData.dir) != dir ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << endl << "inconsistent directions encountered." << endl;
      throw std::runtime_error( msg.str() );
    }
    BOOST_FOREACH( const bpt::ptree::value_type& node, pt.get_child("info") ){
      if( node.first.compare("field") ) continue;
      const bpt::ptree& field = node.second;
      Checkpoint::FieldMetaData fmd;
      fmd.read_ptree( field );
      fieldData.push_back( fmd );
    }
    return true;
  }
  else
    return false;
}

//-------------------------------------------------------------------

std::vector<double>
parse_field_values( const std::string& valstr )
{
  std::vector<double> values;
  typedef boost::tokenizer< boost::char_separator<char> > Token;
  boost::char_separator<char> sep(" \n");
  const Token token( valstr, sep );
  for( Token::const_iterator ival=token.begin(); ival!=token.end(); ++ival ){
    try{
      values.push_back( boost::lexical_cast<double>(*ival) );
    }
    catch( boost::bad_lexical_cast& err ){
//      cout << err.what() << " (" << *ival << ")" << endl;
      values.push_back(-0.0);
    }
  }
  return values;
}

//-------------------------------------------------------------------

bool compare_fields( const Checkpoint::BundleMetaData& bmd,
                     const FMDs& mds1, const string& f1,
                     const FMDs& mds2, const string& f2,
                     const unsigned precision )
{
  TestHelper status(false);

  if( mds1.size() != mds2.size() ){
    cout << "FAILED metadata comparison\n";
    return false;
  }

  // check field metadata
  for( FMDs::const_iterator imd1 = mds1.begin(); imd1 != mds1.end(); ++imd1 ){
    const FMDs::const_iterator imd2 = std::find( mds2.begin(), mds2.end(), *imd1 );
    const Checkpoint::FieldMetaData& md1 = *imd1;
    const Checkpoint::FieldMetaData& md2 = *imd2;
    status( md1 == md2, "comparison on metadata for field " + md1.fieldTag.name() );
    if( md1 != md2 )
      cout << "FAILED metadata comparison between \n\t" << md1.fieldTag.name()
           << " and \n\t" << md2.fieldTag.name()
           << "\nSkipping comparison of field values.\n\n";
  }
  if( !status.ok() ) return false;

  const Direction dir0 = direction(bmd.dir);
  const Direction dir1 = get_perp1(dir0);
  const Direction dir2 = get_perp2(dir0);

  const int n1 = bmd.ncoarse[dir1];
  const int n2 = bmd.ncoarse[dir2];

  SpatialOps::IntVec locOffset(0,0,0);
  for( size_t idim1=0; idim1!=n1; ++idim1 ){
    locOffset[dir1] = idim1;
    for( size_t idim2=0; idim2!=n2; ++idim2 ){
      locOffset[dir2] = idim2;

      const SpatialOps::IntVec lineIndex = bmd.offset + locOffset;

      const std::string fname1( f1 + "/" + get_dir_name(dir0) + "_" +
                                boost::lexical_cast<std::string>(lineIndex[dir1]) + "_" +
                                boost::lexical_cast<std::string>(lineIndex[dir2]) );
      const std::string fname2( f2 + "/" + get_dir_name(dir0) + "_" +
                                boost::lexical_cast<std::string>(lineIndex[dir1]) + "_" +
                                boost::lexical_cast<std::string>(lineIndex[dir2]) );

      cout << "   Comparing files: " << fname1 << " and " << fname2 << endl;

      bfs::ifstream fin1( fname1 );
      bfs::ifstream fin2( fname2 );
      assert( fin1.good()    );
      assert( fin1.is_open() );
      assert( fin2.good()    );
      assert( fin2.is_open() );

      // loop over each field
      for( size_t i=0; i<mds1.size(); ++i ){

        const Checkpoint::FieldMetaData& md1 = mds1[i];
        {
          const FMDs::const_iterator imd2 = std::find( mds2.begin(), mds2.end(), md1 );
          const Checkpoint::FieldMetaData& md2 = *imd2;
          // determine the line on the data file where this entry occurs and then seek to that line
          const size_t fieldOffset = imd2 - mds2.begin();
          fin2.seekg(0); // rewind to beginning of file2
          fin2.clear();  // clear eof flags, error flags, etc. on file2
          std::string stmp;
          // seek to the position where the field of interest begins in file2
          for( size_t iskip=0; iskip<fieldOffset; ++iskip ) std::getline(fin2,stmp);
//          std::cout << "Examining " << md1.fieldTag << " (entry " << i << " in f1 and " << fieldOffset << " in f2)" << std::endl;
        }

        string stmp1, stmp2;
        std::getline(fin1,stmp1);
        std::getline(fin2,stmp2);
        const std::vector<double> vals1 = parse_field_values(stmp1);
        const std::vector<double> vals2 = parse_field_values(stmp2);
        assert( vals1.size() == vals2.size() );
        for( auto ipt=0; ipt<vals1.size(); ++ipt ){
          const bool ok = (std::abs(fround(vals1[ipt],precision) - fround(vals2[ipt], precision)) <= std::numeric_limits<double>::epsilon());
          status( ok, md1.fieldTag );
          if( !ok ){
            cout << endl << "*** Fields differ at point " << ipt+1 << " on entry " << i+1 << " (" << md1.fieldTag.name() << ")"
                 << ".  Values: (" << vals1[ipt] << "," << vals2[ipt] << ")" << endl;
          }
        }
      }
    }
  }
  return status.ok();
}

//-------------------------------------------------------------------

bool compare_files( const string f1, const string f2, unsigned precision )
{
  FMDs xb1fmd, yb1fmd, zb1fmd, xb2fmd, yb2fmd, zb2fmd;
  Checkpoint::BundleMetaData xbmd1, ybmd1, zbmd1, xbmd2, ybmd2, zbmd2;

  if( !boost::filesystem::is_directory( f1 ) )
    throw std::invalid_argument( "Error: " + f1 + " does not exist!" );

  if( !boost::filesystem::is_directory( f2 ) )
    throw std::invalid_argument( "Error: " + f2 + " does not exist!" );


  const bool xexist1 = extract_metadata<SpatialOps::XDIR>(f1,xbmd1,xb1fmd);
  const bool yexist1 = extract_metadata<SpatialOps::YDIR>(f1,ybmd1,yb1fmd);
  const bool zexist1 = extract_metadata<SpatialOps::ZDIR>(f1,zbmd1,zb1fmd);

  const bool xexist2 = extract_metadata<SpatialOps::XDIR>(f2,xbmd2,xb2fmd);
  const bool yexist2 = extract_metadata<SpatialOps::YDIR>(f2,ybmd2,yb2fmd);
  const bool zexist2 = extract_metadata<SpatialOps::ZDIR>(f2,zbmd2,zb2fmd);

  TestHelper status(true);

  if(xexist1) status( xbmd1   == xbmd2, "X-bundle metadata" );
  else        std::cout << "X-bundle does not exist\n";
  if(yexist1) status( ybmd1   == ybmd2, "Y-bundle metadata" );
  else        std::cout << "Y-bundle does not exist\n";
  if(zexist1) status( zbmd1   == zbmd2, "Z-bundle metadata" );
  else        std::cout << "Z-bundle does not exist\n";
  cout << endl;

  if( status.isfailed() ) return false;

  status( compare_fields( xbmd1, xb1fmd, f1, xb2fmd, f2, precision ), "Field data\n" );
  status( compare_fields( ybmd1, yb1fmd, f1, yb2fmd, f2, precision ), "Field data\n" );
  status( compare_fields( zbmd1, zb1fmd, f1, zb2fmd, f2, precision ), "Field data\n" );

  return status.ok();
}

//-------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  try{
    vector<string> files;
    unsigned precision;

    // parse the command line options input describing the problem
    {
      po::options_description desc("Options");
      desc.add_options()
            ("help", "produce help message")
            ("precision", po::value<unsigned int>(&precision)->default_value(16), "comparison precision (# digits of accuracy)")
            ;

      // Hidden options, will be allowed both on command line and
      // in config file, but will not be shown to the user.
      po::options_description hidden("Hidden options");
      hidden.add_options()
            ("files", po::value< vector<string> >(), "input file")
            ;

      po::options_description cmdline_options;
      cmdline_options.add(desc).add(hidden);

      po::positional_options_description p;
      p.add( "files", -1 );

      po::variables_map vm;
      po::store( po::command_line_parser(iarg,carg).options(cmdline_options).positional(p).run(), vm );
      po::notify(vm);

      const string description = "Usage:  compare_files <file1> <file2>\n";

      if( vm.count("help") ) {
        cout << description << desc << endl;
        return 1;
      }

      files = vm["files"].as< vector<string> >();

      if( files.size() != 2 ){
        throw std::invalid_argument( "\nYou must specify TWO files for comparison!\n\n" + description );
      }
      cout << "files to be compared: " << endl;
      BOOST_FOREACH( string s, files ){
        cout << "\t" << s << endl;
      }

      cout << endl;
    }

    if( compare_files( files[0], files[1], precision ) ){
      cout << endl << "Checkpoints are equal!" << endl << endl;
      return 0;
    }
    else{
      cout << endl << "Checkpoints are DIFFERENT!" << endl << endl;
      return -1;
    }
  }
  catch( std::exception& err ){
    cout << "ERROR encountered during file comparison.  Details follow:" << endl
         << err.what() << endl << endl;
  }
  return -1;
}
