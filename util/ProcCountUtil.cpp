/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * \file ProcCountUtil.cpp
 * \date Oct 2, 2013
 * \author John Hutchins
 * \brief A utility reading in a input file and outputting the processor counts based on the input file, including with the weighting.
 * \ingroup Tools
 */

#include <mpi/PartitionHelper.h>
#include <mpi/Sieve.h>

#include <parser/ParseGroup.h>
#include <parser/ParseTools.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <string>
#include <iostream>
#include <cmath>
#include <utility>
#include <algorithm>
#include <set>

typedef  SpatialOps::Numeric3Vec<size_t> ProcVec;
typedef std::pair<size_t, size_t> P;
typedef  SpatialOps::Numeric3Vec<float> WeightVec;

/**
 * @brief From a list of prime factors this generates the set of numbers.
 */
std::set<size_t> generate_numbers( const std::vector<P>& vec)
{
  std::set<size_t> ret;
  for( size_t i=0; i<vec.size(); ++i){
    size_t result = 1;
    for( size_t j=0; j<vec.size(); ++j ){
      if( i != j ){
        for( size_t k=0; k<vec[j].first; ++k){
          result *= vec[j].second;
          ret.insert(result);
        }
      }
    }
    for( size_t j=1; j<vec[i].first; ++j ){
      result *= vec[i].second;
      ret.insert(result);
    }
  }
  return ret;
}

//Something with templates could be used here and GetValid, but I don't think it would actually be better.
std::set<size_t>
direction_numbers( const std::set<size_t>& one, const std::set<size_t>& two )
{
  std::set<size_t> ret;
  for( std::set<size_t>::const_iterator it1 = one.begin(); it1 != one.end(); ++it1 ){
    for( std::set<size_t>::const_iterator it2 = two.begin(); it2 != two.end(); ++it2 ){
      ret.insert( (*it1) * (*it2) );
    }
  }
  return ret;
}


int main( int iarg, char* carg[] )
{
  using SpatialOps::IntVec;

  std::string inputFileName, restartDBName;
  try{
     po::options_description desc("Supported Options");
     desc.add_options()
       ( "help", "print help message" );

     po::options_description hidden("Hidden options");
     hidden.add_options()
       ("input-file", po::value<std::string>(&inputFileName), "input file");

     po::positional_options_description p;
     p.add("input-file", -1);

     po::options_description cmdline_options;
     cmdline_options.add(desc).add(hidden);

     po::variables_map args;
     po::store( po::command_line_parser(iarg,carg).
                options(cmdline_options).positional(p).run(), args );
     po::notify(args);

     if( inputFileName.empty() ){
       std::cout << "Must provide an input file!" << std::endl << desc << std::endl;
       return 1;
     }
     if( args.count("help") ){
       std::cout<< "USAGE:  ProcCountUtil [optional args] [input file]" << desc << "\n";
       return 1;
     }
   }
   catch( std::exception& err ){
     std::cout << err.what() << std::endl;
     return -1;
   }

   std::cout << "Loading input file '" << inputFileName << "'" << std::endl<<std::endl;
   const ParseGroup p0( inputFileName );
   const ParseGroup parser = p0.get_child("LBMS");
   const ParseGroup meshPG = parser.get_child("Mesh");

   IntVec nCoarse = meshPG.get_value<IntVec>("Coarse");
   const IntVec nFine = meshPG.get_value<IntVec>("Fine");
   Sieve<size_t> sieve;

   //Check for primality
   const bool xprime = sieve.is_prime( (size_t) nCoarse[0] );
   const bool yprime = sieve.is_prime( (size_t) nCoarse[1] );
   const bool zprime = sieve.is_prime( (size_t) nCoarse[2] );

   //Note if it is prime and readjust to unevenness.
   if( xprime || yprime || zprime ){
     if( xprime ){
       std::cout << nCoarse[0] << " is prime, only a single process can be evenly placed on this process" << std::endl;
       std::cout << "What follows includes uneven distributions based one " <<  (nCoarse[0]-1) << std::endl << std::endl;
       --nCoarse[0];
     }
     if( yprime ){
       std::cout << nCoarse[1] << " is prime, only a single process can be evenly placed on this process" << std::endl;
       std::cout << "What follows includes uneven distributions based one " << (nCoarse[1]-1) << std::endl << std::endl;
       --nCoarse[1];
     }
     if( zprime ){
       std::cout << nCoarse[2] << " is prime, only a single process can be evenly placed on this process" << std::endl;
       std::cout << "What follows includes uneven distributions based one " << (nCoarse[2]-1) << std::endl << std::endl;
       --nCoarse[2];
     }
     if( xprime && yprime && zprime ){
       std::cout << "All directions are prime, only 3 processes can be assigned to this problem evenly." << std::endl << std::endl;
     }
   }

   const ProcVec maxs = ProcVec(std::floor(nCoarse[0]/2.), std::floor(nCoarse[1]/2.), std::floor(nCoarse[2]/2.));

   const LBMS::PartitionHelper helper( nCoarse, nFine );
   //The problem with this is that if nCoarse is not divisble by two, but by three (or five, or seven) then this gives the wrong amount...
   const std::set<size_t> xvalidnumbers = helper.get_valid_nums(maxs[1], maxs[2]);
   const std::set<size_t> yvalidnumbers = helper.get_valid_nums(maxs[0], maxs[2]);
   const std::set<size_t> zvalidnumbers = helper.get_valid_nums(maxs[0], maxs[1]);

   //This always gives the right amount in terms of primality. However, more procs can validly be used in many cases...
   const std::vector<P> xfactors = sieve.get_prime_factors( (size_t) nCoarse[0] );
   const std::vector<P> yfactors = sieve.get_prime_factors( (size_t) nCoarse[1] );
   const std::vector<P> zfactors = sieve.get_prime_factors( (size_t) nCoarse[2] );

   const std::set<size_t> xnums = generate_numbers(xfactors);
   const std::set<size_t> ynums = generate_numbers(yfactors);
   const std::set<size_t> znums = generate_numbers(zfactors);

   const std::set<size_t> xnum = direction_numbers(ynums, znums);
   const std::set<size_t> ynum = direction_numbers(xnums, znums);
   const std::set<size_t> znum = direction_numbers(xnums, ynums);

   //xnum, ynum, znum contain valid even proc counts for each direction.
   //xvalidnumbers, yvalidnumbers, zvalidnumbers contain valid proc counts which may not be even
   //Priority given to xnum, ynum, znum.
   std::cout << "Evenly split processor counts follow " << std::endl << std::endl;
   for( std::set<size_t>::iterator i = xnum.begin(); i != xnum.end(); ++i ){
     for( std::set<size_t>::iterator j = ynum.begin(); j != ynum.end(); ++j ){
       for( std::set<size_t>::iterator k = znum.begin(); k != znum.end(); ++k ){
         const ProcVec ijk(*i, *j, *k);
         if( ijk ==  helper.check_proc_number(ijk.sum(), false) ){
            std::cout << ijk << " " << ijk.sum() << std::endl;
         }
       }
     }
   }
   std::cout << std::endl << "All processor counts which have a single direction per process follow " << std::endl << std::endl;
   for( std::set<size_t>::iterator i = xvalidnumbers.begin(); i != xvalidnumbers.end(); ++i ){
     for( std::set<size_t>::iterator j = yvalidnumbers.begin(); j != yvalidnumbers.end(); ++j ){
       for( std::set<size_t>::iterator k = zvalidnumbers.begin(); k !=zvalidnumbers.end(); ++k ){
         const ProcVec ijk(*i, *j, *k);
         if( ijk ==  helper.check_proc_number(ijk.sum(), false) ){
            std::cout << ijk << " " << ijk.sum() << std::endl;
         }
       }
     }
   }
}

