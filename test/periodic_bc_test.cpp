#include "TestHelper.h"

#include <iostream>
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

//--- LBMS Includes ---//
#include <lbms/bcs/PeriodicBC.h>
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>
#include <mpi/Environment.h>

#include <expression/Tag.h>

namespace so = SpatialOps;
using so::IntVec;

//====================================================================

template< typename FieldT >
bool
test_periodic( const LBMS::Direction dir )
{
  const LBMS::BundlePtr b = Environment::bundle( LBMS::direction<typename FieldT::Location::Bundle>() );

  const bool bcFlagX = ( b->get_boundary_type(LBMS::XPLUS) == LBMS::DomainBoundary );
  const bool bcFlagY = ( b->get_boundary_type(LBMS::YPLUS) == LBMS::DomainBoundary );
  const bool bcFlagZ = ( b->get_boundary_type(LBMS::ZPLUS) == LBMS::DomainBoundary );

  const so::GhostData ghost(1);
  const so::BoundaryCellInfo bcInfo = so::BoundaryCellInfo::build<FieldT>( ( b->get_boundary_type(LBMS::XMINUS) == LBMS::DomainBoundary )
                                                                           ( b->get_boundary_type(LBMS::XPLUS ) == LBMS::DomainBoundary )
                                                                           ( b->get_boundary_type(LBMS::YMINUS) == LBMS::DomainBoundary )
                                                                           ( b->get_boundary_type(LBMS::YPLUS ) == LBMS::DomainBoundary )
                                                                           ( b->get_boundary_type(LBMS::ZMINUS) == LBMS::DomainBoundary )
                                                                           ( b->get_boundary_type(LBMS::ZPLUS ) == LBMS::DomainBoundary )
  );

  const SpatialOps::IntVec& dim = b->npts();
  const SpatialOps::MemoryWindow w = so::get_window_with_ghost( dim, ghost, bcInfo );

  FieldT x( w, bcInfo, ghost, NULL );
  FieldT y( w, bcInfo, ghost, NULL );
  FieldT z( w, bcInfo, ghost, NULL );
  Expr::Tag tx = Expr::Tag("X", Expr::STATE_NONE );
  Expr::Tag ty = Expr::Tag("Y", Expr::STATE_NONE );
  Expr::Tag tz = Expr::Tag("Z", Expr::STATE_NONE );

  LBMS::set_coord_values( x, y, z, b );

  const LBMS::PeriodicBC<FieldT> bcx( LBMS::XDIR, b, tx );
  const LBMS::PeriodicBC<FieldT> bcy( LBMS::YDIR, b, ty );
  const LBMS::PeriodicBC<FieldT> bcz( LBMS::ZDIR, b, tz );

  TestHelper status(false);

  const size_t nx = LBMS::npts<FieldT>( LBMS::XDIR, ghost, b );
  const size_t ny = LBMS::npts<FieldT>( LBMS::YDIR, ghost, b );
  const size_t nz = LBMS::npts<FieldT>( LBMS::ZDIR, ghost, b );

  const int ngx = ghost.get_minus(LBMS::XDIR);
  const int ngy = ghost.get_minus(LBMS::YDIR);
  const int ngz = ghost.get_minus(LBMS::ZDIR);

  // x faces
  if( nx>1 ){
    bcx(x);
    bcx(y);
    bcx(z);
    for( size_t k=0; k<nz; ++k ){
      for( size_t j=0; j<ny; ++j ){
        size_t ix1 = LBMS::flat<FieldT>( IntVec( 0,        j, k ), ghost, b );
        size_t ix2 = LBMS::flat<FieldT>( IntVec( nx-ngx-1, j, k ), ghost, b );
        status( x[ix1] == x[ix2], "x: -x" );
        status( y[ix1] == y[ix2], "y: -x" );
        status( z[ix1] == z[ix2], "z: -x" );
        ix1 = LBMS::flat<FieldT>( IntVec( nx-1, j, k ), ghost, b );
        ix2 = LBMS::flat<FieldT>( IntVec( ngx,  j, k ), ghost, b );
        status( x[ix1] == x[ix2], "x: +x" );
        status( y[ix1] == y[ix2], "y: +x" );
        status( z[ix1] == z[ix2], "z: +x" );
      }
    }
  }

  // y faces
  if( ny>1 ){
    bcy(x); bcy(y); bcy(z);
    for( size_t k=0; k<nz; ++k ){
      for( size_t i=0; i<nx; ++i ){
        size_t ix1 = LBMS::flat<FieldT>( IntVec( i, 0,        k ), ghost, b );
        size_t ix2 = LBMS::flat<FieldT>( IntVec( i, ny-ngy-1, k ), ghost, b );
        status( x[ix1] == x[ix2], "x: -y" );
        status( y[ix1] == y[ix2], "y: -y" );
        status( z[ix1] == z[ix2], "z: -y" );
        ix1 = LBMS::flat<FieldT>( IntVec( i, ny-1, k ), ghost, b );
        ix2 = LBMS::flat<FieldT>( IntVec( i, ngy,  k ), ghost, b );
        status( x[ix1] == x[ix2], "x: +y" );
        status( y[ix1] == y[ix2], "y: +y" );
        status( z[ix1] == z[ix2], "z: +y" );
      }
    }
  }

  // z faces
  if( nz>1 ){
    bcz(x); bcz(y); bcz(z);
    for( size_t j=0; j<ny; ++j ){
      for( size_t i=0; i<nx; ++i ){
        size_t ix1 = LBMS::flat<FieldT>( IntVec( i, j, 0       ), ghost, b );
        size_t ix2 = LBMS::flat<FieldT>( IntVec( i, j, nz-ngz-1), ghost, b );
        status( x[ix1] == x[ix2], "x: -z" );
        status( y[ix1] == y[ix2], "y: -z" );
        status( z[ix1] == z[ix2], "z: -z" );
        ix1 = LBMS::flat<FieldT>( IntVec( i, j, nz-1 ), ghost, b );
        ix2 = LBMS::flat<FieldT>( IntVec( i, j, ngz  ), ghost, b );
        status( x[ix1] == x[ix2], "x: +z" );
        status( y[ix1] == y[ix2], "y: +z" );
        status( z[ix1] == z[ix2], "z: +z" );
      }
    }
  }

  return status.ok();
}

//====================================================================

int main( int iarg, char* carg[] )
{
  LBMS::Environment::setup( iarg, carg );
  int nx, ny, nz;
  int Nx, Ny, Nz;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nx)->default_value(20), "Fine (ODT) grid in x" )
      ( "ny", po::value<int>(&ny)->default_value(20), "Fine (ODT) grid in y" )
      ( "nz", po::value<int>(&nz)->default_value(20), "Fine (ODT) grid in z" )
      ( "Nx", po::value<int>(&Nx)->default_value(4),"Coarse (LES) grid in x")
      ( "Ny", po::value<int>(&Ny)->default_value(4),"Coarse (LES) grid in y")
      ( "Nz", po::value<int>(&Nz)->default_value(4),"Coarse (LES) grid in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << endl;
      return 1;
    }
  }

  {
    LBMS::Environment::set_topology( IntVec(Nx, Ny, Nz),
                                     IntVec(nx, ny, nz),
                                     LBMS::Coordinate(1,1,1) );
  }
  LBMS::BoundaryType bcs[6];
  for( unsigned i=0; i<6; ++i ) bcs[i] = LBMS::PeriodicBoundary;

  if( nx>1 ) Environment::bundle(LBMS::XDIR)->set_boundary_types(bcs);
  if( ny>1 ) Environment::bundle(LBMS::YDIR)->set_boundary_types(bcs);
  if( nz>1 ) Environment::bundle(LBMS::ZDIR)->set_boundary_types(bcs);

  TestHelper status(true);

  try{
    if( nx>1 ){
                 status( test_periodic<LBMS::XVolField  >( LBMS::XDIR ), "XBundle : vol"   );
                 status( test_periodic<LBMS::XSurfXField>( LBMS::XDIR ), "XBundle : xsurf" );
      if( Ny>1 ) status( test_periodic<LBMS::YSurfXField>( LBMS::XDIR ), "XBundle : ysurf" );
      if( Nz>1 ) status( test_periodic<LBMS::ZSurfXField>( LBMS::XDIR ), "XBundle : zsurf" );
    }
    if( ny>1 ){
                 status( test_periodic<LBMS::YVolField  >( LBMS::YDIR ), "XBundle : vol"   );
      if( Nx>1 ) status( test_periodic<LBMS::XSurfYField>( LBMS::YDIR ), "YBundle : xsurf" );
                 status( test_periodic<LBMS::YSurfYField>( LBMS::YDIR ), "YBundle : ysurf" );
      if( Nz>1 ) status( test_periodic<LBMS::ZSurfYField>( LBMS::YDIR ), "YBundle : zsurf" );
    }
    if( nz>1 ){
                 status( test_periodic<LBMS::ZVolField  >( LBMS::ZDIR ), "XBundle : vol"   );
      if( Nx>1 ) status( test_periodic<LBMS::XSurfZField>( LBMS::ZDIR ), "ZBundle : xsurf" );
      if( Ny>1 ) status( test_periodic<LBMS::YSurfZField>( LBMS::ZDIR ), "ZBundle : ysurf" );
                 status( test_periodic<LBMS::ZSurfZField>( LBMS::ZDIR ), "ZBundle : zsurf" );
    }
    if( status.ok() ) return 0;
  }
  catch( std::exception& err ){
    cout << endl << err.what() << endl;
  }
  return -1;
}

//====================================================================
