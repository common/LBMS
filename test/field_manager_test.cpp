#include <string>
#include <iostream>
#include <iomanip>

//--- LBMS Includes ---//
#include <lbms/Mesh.h>
#include <lbms/Bundle.h>
#include <fields/Fields.h>
#include <test/TestHelper.h>

//--- Expression Includes ---//
#include <expression/ExprLib.h>

using namespace std;

template< typename FieldT >
bool test_field_manager( const LBMS::BundlePtr b,
                         Expr::FieldManagerList& fml,
                         const std::string fieldName )
{
  typename Expr::FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();

  const int nghost = 1;
  const Expr::Tag tag(fieldName, Expr::STATE_NONE);
  fm.register_field( tag, nghost );
  fm.allocate_fields( boost::cref( b->get_field_info() ) );
  FieldT& fld = fm.field_ref( tag );

  return true;
}

//====================================================================

int main()
{
  using SpatialOps::IntVec;

  try{

    const IntVec nFine  (50, 50, 2);
    const IntVec nCoarse(10,  5, 1);

    const LBMS::Coordinate length(1,1,1);
    const LBMS::Mesh mesh( nCoarse, nFine, length );

    TestHelper status(true);

    Expr::FieldManagerList fml;

    //
    // make sure that we can build field managers and deal with fields on them.
    //
    {
      status( test_field_manager<LBMS::XVolField  >( mesh.bundle(LBMS::XDIR), fml, "VolX"   ), "VolX Fields" );
      status( test_field_manager<LBMS::XSurfXField>( mesh.bundle(LBMS::XDIR), fml, "XSurfX" ), "XSurfX Fields" );
      status( test_field_manager<LBMS::YSurfXField>( mesh.bundle(LBMS::XDIR), fml, "YSurfX" ), "YSurfX Fields" );
      status( test_field_manager<LBMS::ZSurfXField>( mesh.bundle(LBMS::XDIR), fml, "ZSurfX" ), "ZSurfX Fields" );
    }
    {
      status( test_field_manager<LBMS::YVolField  >( mesh.bundle(LBMS::YDIR), fml, "VolY"   ), "VolY Fields" );  ;
      status( test_field_manager<LBMS::XSurfYField>( mesh.bundle(LBMS::YDIR), fml, "XSurfY" ), "XSurfY Fields" );;
      status( test_field_manager<LBMS::YSurfYField>( mesh.bundle(LBMS::YDIR), fml, "YSurfY" ), "YSurfY Fields" );;
      status( test_field_manager<LBMS::ZSurfYField>( mesh.bundle(LBMS::YDIR), fml, "ZSurfY" ), "ZSurfY Fields" );;
    }
    {
      status( test_field_manager<LBMS::ZVolField  >( mesh.bundle(LBMS::ZDIR), fml, "VolZ"   ), "VolZ Fields" );  ;
      status( test_field_manager<LBMS::XSurfZField>( mesh.bundle(LBMS::ZDIR), fml, "XSurfZ" ), "XSurfZ Fields" );;
      status( test_field_manager<LBMS::YSurfZField>( mesh.bundle(LBMS::ZDIR), fml, "YSurfZ" ), "YSurfZ Fields" );;
      status( test_field_manager<LBMS::ZSurfZField>( mesh.bundle(LBMS::ZDIR), fml, "ZSurfZ" ), "ZSurfZ Fields" );;
    }
    if( status.ok() ){
      std::cout << "PASS" << std::endl;
      return 0;
    }
  }
  catch(std::exception& err){
    std::cout << err.what() << std::endl;
  }
  return -1;
}
