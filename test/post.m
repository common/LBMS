clear; clc;

% ndx = 50 +0 %+2;
% ndy = 10 +1 %+2;
% 
% nsx = 10 +0 %+2;
% nsy = 50 +1 %+2;

ndx = 10 +1 %+2;
ndy = 50 +0 %+2;

nsx = 50 +1 %+2;
nsy = 10 +0 %+2;

xs = reshape( load_xs , nsx, nsy );
ys = reshape( load_ys , nsx, nsy );
s  = reshape( load_src, nsx, nsy );

xd = reshape( load_xd  , ndx, ndy );
yd = reshape( load_yd  , ndx, ndy );
d  = reshape( load_dest, ndx, ndy );
d2 = reshape( load_d2  , ndx, ndy );

c = [min(min(min(d2)),min(min(s))), max(max(max(d2)),max(max(s)))];

subplot(2,2,1); surf(xs,ys,s ); xlabel('x'); caxis(c); title('src');
subplot(2,2,2); surf(xd,yd,d2); xlabel('x'); caxis(c); title('dest');
subplot(2,2,3); surf(xd,yd,d ); xlabel('x'); caxis(c); title('restrict');
subplot(2,2,4); surf(xd,yd,d2-d ); xlabel('x'); title('err'); shading flat;

% figure;
% R=load_R;
% dd = R*load_src;
% surf(xd,yd,reshape(dd,ndx,ndy)); caxis(c); xlabel('x');