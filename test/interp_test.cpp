#include <spatialops/OperatorDatabase.h>
#include <spatialops/structured/FieldHelper.h>  // write_matlab

#include <operators/Operators.h>
#include <lbms/Mesh.h>
#include <lbms/Patch.h>

#include "TestHelper.h"

#include <limits>
#include <iostream>
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po=boost::program_options;


template< typename OpT >
bool
test_interp( const SpatialOps::OperatorDatabase& opDB,
             const LBMS::Mesh& mesh,
             LBMS::Patch& patch,
             const bool writeDiagnostics = false )
{
  using namespace SpatialOps;

  typedef typename OpT::SrcFieldType  SrcT;
  typedef typename OpT::DestFieldType DestT;

  const bool bcFlagX=true, bcFlagY=true, bcFlagZ=true;

  const SpatialOps::IntVec srcDim ( patch.dim<SrcT >() );
  const SpatialOps::IntVec destDim( patch.dim<DestT>() );

  const SpatialOps::MemoryWindow srcWindow ( SpatialOps::get_window_with_ghost<SrcT >(  srcDim, bcFlagX, bcFlagY, bcFlagZ ) );
  const SpatialOps::MemoryWindow destWindow( SpatialOps::get_window_with_ghost<DestT>( destDim, bcFlagX, bcFlagY, bcFlagZ ) );

  SrcT xs( srcWindow, NULL );
  SrcT ys( srcWindow, NULL );
  SrcT zs( srcWindow, NULL );

  DestT xd( destWindow, NULL );
  DestT yd( destWindow, NULL );
  DestT zd( destWindow, NULL );

  DestT tmp( destWindow, NULL );

  set_coord_values( xs, ys, zs, mesh );
  set_coord_values( xd, yd, zd, mesh );

  const OpT* const op = opDB.retrieve_operator<OpT>();

  const bool writeGhost = true;
  if( writeDiagnostics ){
    //op->write_matlab("R");
    SpatialOps::write_matlab(xs,"xs",writeGhost);
    SpatialOps::write_matlab(xd,"xd",writeGhost);
    SpatialOps::write_matlab(ys,"ys",writeGhost);
    SpatialOps::write_matlab(yd,"yd",writeGhost);
    SpatialOps::write_matlab(zs,"zs",writeGhost);
    SpatialOps::write_matlab(zd,"zd",writeGhost);
  }

  typename DestT::iterator
    itmp=tmp.interior_begin(),
    is=xd.interior_begin();

  TestHelper status(false);

  const double tol = 10*std::numeric_limits<double>::epsilon();

  op->apply_to_field( xs, tmp );
  if( writeDiagnostics ) SpatialOps::write_matlab(tmp,"xdr",writeGhost);

  int i=0;
  is=xd.interior_begin(); itmp=tmp.interior_begin();
  const typename DestT::iterator ise=xd.interior_end();
  for( ; is!=ise; ++is, ++itmp, ++i )
    {
      const double diff = fabs(*itmp-*is);
      status( diff < tol, "x" );
    }

  op->apply_to_field( ys, tmp );
  if( writeDiagnostics ) SpatialOps::write_matlab(tmp,"ydr",writeGhost);

  for( is=yd.interior_begin(), itmp=tmp.interior_begin();
       is!=yd.interior_end();
       ++is, ++itmp )
    {
      status( fabs(*itmp-*is) < tol, "y" );
    }

  op->apply_to_field( zs, tmp );
  if( writeDiagnostics ) SpatialOps::write_matlab(tmp,"zdr",writeGhost);

  for( is=zd.interior_begin(), itmp=tmp.interior_begin();
       is!=zd.interior_end();
       ++is, ++itmp )
    {
      status( fabs(*itmp-*is) < tol, "z" );
    }
  return status.ok();
}

//--------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  int Nx, Ny, Nz, nx, ny, nz;
  double Lx, Ly, Lz;

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "nx", po::value<int>(&nx)->default_value(12), "Fine (ODT) grid in x" )
      ( "ny", po::value<int>(&ny)->default_value(12), "Fine (ODT) grid in y" )
      ( "nz", po::value<int>(&nz)->default_value(12), "Fine (ODT) grid in z" )
      ( "Nx", po::value<int>(&Nx)->default_value(4),"Coarse (LES) grid in x")
      ( "Ny", po::value<int>(&Ny)->default_value(3),"Coarse (LES) grid in y")
      ( "Nz", po::value<int>(&Nz)->default_value(2),"Coarse (LES) grid in z")
      ( "Lx", po::value<double>(&Lx)->default_value(1),"Domain length in x")
      ( "Ly", po::value<double>(&Ly)->default_value(1),"Domain length in y")
      ( "Lz", po::value<double>(&Lz)->default_value(1),"Domain length in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }

  TestHelper status;

  try{
    using SpatialOps::IntVec;
    LBMS::Mesh mesh( IntVec(Nx, Ny, Nz),
                     IntVec(nx, ny, nz),
                     Coordinate(Lx, Ly, Lz) );

    LBMS::Patch patch( mesh );

    const LBMS::BundlePtr xBundle = mesh.bundle(LBMS::XDIR);
    const LBMS::BundlePtr yBundle = mesh.bundle(LBMS::YDIR);
    const LBMS::BundlePtr zBundle = mesh.bundle(LBMS::ZDIR);

    cout << xBundle << endl
         << yBundle << endl
         << zBundle << endl;

    SpatialOps::OperatorDatabase opDB;

    const bool bcplus[] = { true, true, true };

    if( nx>1 ){
      // x-interpolant on x bundle
      opDB.register_new_operator<LBMS::XInterpX>( new LBMS::XInterpX( xBundle ) );
      status( test_interp<LBMS::XInterpX>( opDB, mesh, patch ), "XInterpX" );
    }
    if( ny>1 ){
      // y-interpolant on a y-bundle
      opDB.register_new_operator<LBMS::YInterpY>( new LBMS::YInterpY( yBundle ) );
      status( test_interp<LBMS::YInterpY>( opDB, mesh, patch ), "YInterpY" );
    }
    if( nz>1 ){
      opDB.register_new_operator<LBMS::ZInterpZ>( new LBMS::ZInterpZ( zBundle ) );
      status( test_interp<LBMS::ZInterpZ>( opDB, mesh, patch ), "ZInterpZ" );
    }

    //
    //  Test 2D interpolants (on a given bundle)
    //
    if( nx>1 ){
      if( Ny>1 ){
        // x-face on x bundle to y-face on x bundle
        opDB.register_new_operator<LBMS::XInterpXY>( LBMS::XInterpXY::Assembler( xBundle ) );
        status( test_interp<LBMS::XInterpXY>( opDB, mesh, patch ), "XInterpXY" );
      }
      if( Nz>1 ){
        // x-face on x bundle to z-face on x bundle
        opDB.register_new_operator<LBMS::XInterpXZ>( LBMS::XInterpXZ::Assembler( xBundle ) );
        status( test_interp<LBMS::XInterpXZ>( opDB, mesh, patch ), "XInterpXZ" );
      }
    }

    if( ny>1 ){
      if( Nx>1 ){
        // x-face on y bundle to y-face on y bundle
        opDB.register_new_operator<LBMS::YInterpYX>( LBMS::YInterpYX::Assembler( yBundle ) );  // jcs broken.
        status( test_interp<LBMS::YInterpYX>( opDB, mesh, patch ), "YInterpYX" );
      }
      if( Nz>1 ){
        // x-face on y bundle to z-face on y bundle
        opDB.register_new_operator<LBMS::YInterpYZ>( LBMS::YInterpYZ::Assembler( yBundle ) );  // jcs broken.
        status( test_interp<LBMS::YInterpYZ>( opDB, mesh, patch ), "YInterpYZ" );
      }
    }

    if( nz>1 ){
      if( Nx>1 ){
        // z-face on z bundle to x-face on z bundle
        opDB.register_new_operator<LBMS::ZInterpZX>( LBMS::ZInterpZX::Assembler( zBundle ) );
        status( test_interp<LBMS::ZInterpZX>( opDB, mesh, patch ), "ZInterpZX" );
      }
      if( Ny>1 ){
        // z-face on z bundle to y-face on z bundle
        opDB.register_new_operator<LBMS::ZInterpZY>( LBMS::ZInterpZY::Assembler( zBundle ) );  // jcs broken.
        status( test_interp<LBMS::ZInterpZY>( opDB, mesh, patch ), "ZInterpZY" );
      }
    }

    //
    //  Test 2D interpolants among lines in a bundle.
    //
    if( Nx>1 ){
      if( Ny>1 ){
        opDB.register_new_operator<LBMS::X2YInterpX>( LBMS::X2YInterpX::Assembler( xBundle, yBundle ) );
        status( test_interp<LBMS::X2YInterpX>( opDB, mesh, patch ), "X2YInterpX" );
      }
      if( Nz>1 ){
        opDB.register_new_operator<LBMS::X2ZInterpX>( LBMS::X2ZInterpX::Assembler( xBundle, zBundle ) );
        status( test_interp<LBMS::X2ZInterpX>( opDB, mesh, patch ), "X2ZInterpX" );
      }
    }

    if( Ny>1 ){
      if( Nx>1 ){
        opDB.register_new_operator<LBMS::Y2XInterpY>( LBMS::Y2XInterpY::Assembler( yBundle, xBundle ) );
        status( test_interp<LBMS::Y2XInterpY>( opDB, mesh, patch ), "Y2XInterpY" );
      }
      if( Nz>1 ){
        opDB.register_new_operator<LBMS::Y2ZInterpY>( LBMS::Y2ZInterpY::Assembler( yBundle, zBundle ) );
        status( test_interp<LBMS::Y2ZInterpY>( opDB, mesh, patch ), "Y2ZInterpY" );
      }
    }

    if( Nz>1 ){
      if( Nx>1 ){
        opDB.register_new_operator<LBMS::Z2XInterpZ>( LBMS::Z2XInterpZ::Assembler( zBundle, xBundle ) );
        status( test_interp<LBMS::Z2XInterpZ>( opDB, mesh, patch ), "Z2XInterpZ" );
      }
      if( Ny>1 ){
        opDB.register_new_operator<LBMS::Z2YInterpZ>( LBMS::Z2YInterpZ::Assembler( zBundle, xBundle ) );
        status( test_interp<LBMS::Z2YInterpZ>( opDB, mesh, patch ), "Z2YInterpZ" );
      }
    }

    //
    // Test 2D interpolants among different bundles
    //
    if( nx>1 ){
      if( ny>1 && Ny>1 ){
        opDB.register_new_operator<LBMS::X2YInterpXY>( LBMS::X2YInterpXY::Assembler( xBundle, yBundle ) );
        status( test_interp<LBMS::X2YInterpXY>( opDB, mesh, patch ), "X2YInterpXY" );
      }
      if( nz>1 && Nz>1 ){
        opDB.register_new_operator<LBMS::X2ZInterpXZ>( LBMS::X2ZInterpXZ::Assembler( xBundle, zBundle ) );
        status( test_interp<LBMS::X2ZInterpXZ>( opDB, mesh, patch ), "X2ZInterpXZ" );
      }
    }

    if( ny>1 ){
      if( Nx>1 && nx>1 ){
        opDB.register_new_operator<LBMS::Y2XInterpYX>( LBMS::Y2XInterpYX::Assembler( yBundle, xBundle ) );
        status( test_interp<LBMS::Y2XInterpYX>( opDB, mesh, patch ), "Y2XInterpYX" );
      }
      if( Nz>1 && nz>1 ){
        opDB.register_new_operator<LBMS::Y2ZInterpYZ>( LBMS::Y2ZInterpYZ::Assembler( yBundle, zBundle ) );
        status( test_interp<LBMS::Y2ZInterpYZ>( opDB, mesh, patch ), "Y2ZInterpYZ" );
      }
    }

    if( nz>1 ){
      if( Nx>1 && nx>1 ){
        opDB.register_new_operator<LBMS::Z2XInterpZX>( LBMS::Z2XInterpZX::Assembler( zBundle, xBundle ) );
        status( test_interp<LBMS::Z2XInterpZX>( opDB, mesh, patch ), "Z2XInterpZX" );
      }
      if( Ny>1 && ny>1 ){
        opDB.register_new_operator<LBMS::Z2YInterpZY>( LBMS::Z2YInterpZY::Assembler( zBundle, yBundle ) );
        status( test_interp<LBMS::Z2YInterpZY>( opDB, mesh, patch ), "Z2YInterpZY" );
      }
    }

  }
  catch( std::runtime_error& e ){
    cout << e.what() << endl;
    return -1;
  }
  catch( ... ){
    cout << "Unknown error caught. test FAILED" << endl;
    return -1;
  }

  if( status.ok() ) return 0;
  return -1;
}
