
#
# run_test( test_name myinputFile.yaml )
#   performs a test without GS comparison
#
# run_test( test_name myinputFile.yaml TestCheckpoint_0.1 )
#   performs a test with GS comparison
#
function( run_test testName inputFile )

  if( (NOT ${ARGC} EQUAL 2)  AND  (NOT ${ARGC} EQUAL 3) )
    message( SEND_ERROR "Invalid use of 'run_test' with ${ARGC} arguments.\n"
             "\trun_test( testName inputFile )\n"
             "\trun_test( testName inputFile outputName )\n" )
  endif()

  add_test( NAME ${testName} 
            COMMAND lbms_exe
            ${inputFile}
          )
  set_property( TEST ${testName} APPEND PROPERTY TIMEOUT 180 )

  if( ${HAVE_GS} )
    if( ${ARGC} EQUAL 3 )
      list( GET ARGN 0 fname )
      # note that this will only run when configuring, not each time the test is run...
      file( REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/${fname} )
      add_test( NAME ${testName}_compare
                COMMAND compare_checkpoints
                ${CMAKE_CURRENT_BINARY_DIR}/${fname}
                ${GOLD_STANDARDS_DIR}/${fname}
              )
      set_property( TEST ${testName}_compare APPEND PROPERTY DEPENDS ${testName} )
    endif( ${ARGC} EQUAL 3 )
  endif( ${HAVE_GS} )

endfunction()

# Example Usage:
#
#   run_mpi_test( "1;2;4" myMPITest myMPITest.yaml )
#     runs the myMPITest.yaml input file on 1, 2 and 4 MPI processes.
#     These tests will be run sequentially to avoid IO clashes
#   
#   run_mpi_test( "1;2;4" myMPITest myMPITest.yaml myMPITest_Checkpoint_1.0e-04 )
#     Runs the test on 1 2 and 4 processes and compares the result of each run
#     with the specified gold standard
#
function( run_mpi_test nprocs testName inputFile )
  
  if( (NOT ${ARGC} EQUAL 3) AND (NOT ${ARGC} EQUAL 4) )
    message( SEND_ERROR "Invalid use of 'run_mpi_test' with ${ARGC} arguments.\n"
           "\trun_test( nproclist testName inputFile )\n"
           "\trun_test( nproclist testName inputFile outputName )\n" )
  endif() 

  set( LBMS_EXE ${CMAKE_BINARY_DIR}/src/lbms_exe )
  
  if( ENABLE_MPI )
  
    set( DEPS "" )
    
    foreach( np ${nprocs} )
     
      set( testname "${testName}_${np}" )
    
      add_test( NAME ${testname} 
                COMMAND ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${np} ${LBMS_EXE}
                ${inputFile}
              )
      set_property( TEST ${testname} APPEND PROPERTY TIMEOUT 180 )
      
      # this test depends on all previous.  This forces an effective serialization
      # of these tests to prevent clashes on output since they use the same input
      # and output files.
      set_property( TEST ${testname} APPEND PROPERTY DEPENDS ${DEPS} )
      set( DEPS ${DEPS} ${testname} ) 
     
      if( ${HAVE_GS} )
        if( ${ARGC} EQUAL 4 )
          list( GET ARGN 0 fname )
          add_test( NAME ${testname}_compare
                    COMMAND compare_checkpoints
                    ${CMAKE_CURRENT_BINARY_DIR}/${fname}
                    ${GOLD_STANDARDS_DIR}/${fname}
                  )
          # This test depends on all previous to prevent IO clashes
          set_property( TEST ${testname}_compare APPEND PROPERTY DEPENDS ${DEPS} )
          set( DEPS ${DEPS} ${testname}_compare )
        endif( ${ARGC} EQUAL 4 )
      endif( ${HAVE_GS} )
    
    endforeach()
  
  else ( ENABLE_MPI )
    if( ${ARGC} EQUAL 4 )
      list( GET ARGN 0 fname )
      run_test( ${testName} ${inputFile} ${fname} )
    else()
      run_test( ${testName} ${inputFile} )
    endif()
  endif( ENABLE_MPI )
  
endfunction()
